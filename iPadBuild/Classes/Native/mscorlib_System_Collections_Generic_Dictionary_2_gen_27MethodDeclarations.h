﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3920;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t3923;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t3928;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3761;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t4430;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t4431;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__20.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m22214_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2__ctor_m22214(__this, method) (( void (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2__ctor_m22214_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22216_gshared (Dictionary_2_t3920 * __this, Object_t* ___comparer, MethodInfo* method);
#define Dictionary_2__ctor_m22216(__this, ___comparer, method) (( void (*) (Dictionary_2_t3920 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m22216_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22218_gshared (Dictionary_2_t3920 * __this, int32_t ___capacity, MethodInfo* method);
#define Dictionary_2__ctor_m22218(__this, ___capacity, method) (( void (*) (Dictionary_2_t3920 *, int32_t, MethodInfo*))Dictionary_2__ctor_m22218_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22220_gshared (Dictionary_2_t3920 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2__ctor_m22220(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3920 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m22220_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m22222_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m22222(__this, method) (( Object_t * (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m22222_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22224_gshared (Dictionary_2_t3920 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22224(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3920 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22224_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22226_gshared (Dictionary_2_t3920 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22226(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3920 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22226_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22228_gshared (Dictionary_2_t3920 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22228(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3920 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22228_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22230_gshared (Dictionary_2_t3920 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22230(__this, ___key, method) (( void (*) (Dictionary_2_t3920 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22230_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22232_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22232(__this, method) (( bool (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22232_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22234_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22234(__this, method) (( Object_t * (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22236_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22236(__this, method) (( bool (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22236_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22238_gshared (Dictionary_2_t3920 * __this, KeyValuePair_2_t3921  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22238(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3920 *, KeyValuePair_2_t3921 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22238_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22240_gshared (Dictionary_2_t3920 * __this, KeyValuePair_2_t3921  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22240(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3920 *, KeyValuePair_2_t3921 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22240_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22242_gshared (Dictionary_2_t3920 * __this, KeyValuePair_2U5BU5D_t4430* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22242(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3920 *, KeyValuePair_2U5BU5D_t4430*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22242_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22244_gshared (Dictionary_2_t3920 * __this, KeyValuePair_2_t3921  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22244(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3920 *, KeyValuePair_2_t3921 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22244_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22246_gshared (Dictionary_2_t3920 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22246(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3920 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22246_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22248_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22248(__this, method) (( Object_t * (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22248_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22250_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22250(__this, method) (( Object_t* (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22250_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22252_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22252(__this, method) (( Object_t * (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22252_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22254_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_get_Count_m22254(__this, method) (( int32_t (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_get_Count_m22254_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m22256_gshared (Dictionary_2_t3920 * __this, int32_t ___key, MethodInfo* method);
#define Dictionary_2_get_Item_m22256(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3920 *, int32_t, MethodInfo*))Dictionary_2_get_Item_m22256_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22258_gshared (Dictionary_2_t3920 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_set_Item_m22258(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3920 *, int32_t, Object_t *, MethodInfo*))Dictionary_2_set_Item_m22258_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22260_gshared (Dictionary_2_t3920 * __this, int32_t ___capacity, Object_t* ___hcp, MethodInfo* method);
#define Dictionary_2_Init_m22260(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3920 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m22260_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22262_gshared (Dictionary_2_t3920 * __this, int32_t ___size, MethodInfo* method);
#define Dictionary_2_InitArrays_m22262(__this, ___size, method) (( void (*) (Dictionary_2_t3920 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m22262_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22264_gshared (Dictionary_2_t3920 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22264(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3920 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m22264_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3921  Dictionary_2_make_pair_m22266_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_make_pair_m22266(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3921  (*) (Object_t * /* static, unused */, int32_t, Object_t *, MethodInfo*))Dictionary_2_make_pair_m22266_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m22268_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_pick_key_m22268(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, MethodInfo*))Dictionary_2_pick_key_m22268_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m22270_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_pick_value_m22270(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, MethodInfo*))Dictionary_2_pick_value_m22270_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22272_gshared (Dictionary_2_t3920 * __this, KeyValuePair_2U5BU5D_t4430* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyTo_m22272(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3920 *, KeyValuePair_2U5BU5D_t4430*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m22272_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m22274_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_Resize_m22274(__this, method) (( void (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_Resize_m22274_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22276_gshared (Dictionary_2_t3920 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_Add_m22276(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3920 *, int32_t, Object_t *, MethodInfo*))Dictionary_2_Add_m22276_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m22278_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_Clear_m22278(__this, method) (( void (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_Clear_m22278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22280_gshared (Dictionary_2_t3920 * __this, int32_t ___key, MethodInfo* method);
#define Dictionary_2_ContainsKey_m22280(__this, ___key, method) (( bool (*) (Dictionary_2_t3920 *, int32_t, MethodInfo*))Dictionary_2_ContainsKey_m22280_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22282_gshared (Dictionary_2_t3920 * __this, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_ContainsValue_m22282(__this, ___value, method) (( bool (*) (Dictionary_2_t3920 *, Object_t *, MethodInfo*))Dictionary_2_ContainsValue_m22282_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22284_gshared (Dictionary_2_t3920 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2_GetObjectData_m22284(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3920 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m22284_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22286_gshared (Dictionary_2_t3920 * __this, Object_t * ___sender, MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22286(__this, ___sender, method) (( void (*) (Dictionary_2_t3920 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m22286_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22288_gshared (Dictionary_2_t3920 * __this, int32_t ___key, MethodInfo* method);
#define Dictionary_2_Remove_m22288(__this, ___key, method) (( bool (*) (Dictionary_2_t3920 *, int32_t, MethodInfo*))Dictionary_2_Remove_m22288_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22290_gshared (Dictionary_2_t3920 * __this, int32_t ___key, Object_t ** ___value, MethodInfo* method);
#define Dictionary_2_TryGetValue_m22290(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3920 *, int32_t, Object_t **, MethodInfo*))Dictionary_2_TryGetValue_m22290_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Keys()
extern "C" KeyCollection_t3923 * Dictionary_2_get_Keys_m22292_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_get_Keys_m22292(__this, method) (( KeyCollection_t3923 * (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_get_Keys_m22292_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t3928 * Dictionary_2_get_Values_m22293_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_get_Values_m22293(__this, method) (( ValueCollection_t3928 * (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_get_Values_m22293_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m22295_gshared (Dictionary_2_t3920 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ToTKey_m22295(__this, ___key, method) (( int32_t (*) (Dictionary_2_t3920 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m22295_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m22297_gshared (Dictionary_2_t3920 * __this, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_ToTValue_m22297(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t3920 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m22297_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22299_gshared (Dictionary_2_t3920 * __this, KeyValuePair_2_t3921  ___pair, MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22299(__this, ___pair, method) (( bool (*) (Dictionary_2_t3920 *, KeyValuePair_2_t3921 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22299_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t3925  Dictionary_2_GetEnumerator_m22300_gshared (Dictionary_2_t3920 * __this, MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22300(__this, method) (( Enumerator_t3925  (*) (Dictionary_2_t3920 *, MethodInfo*))Dictionary_2_GetEnumerator_m22300_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2128  Dictionary_2_U3CCopyToU3Em__0_m22302_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22302(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, int32_t, Object_t *, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22302_gshared)(__this /* static, unused */, ___key, ___value, method)
