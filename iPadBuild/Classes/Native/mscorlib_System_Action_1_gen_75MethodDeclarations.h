﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.Vector3>
struct Action_1_t3857;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Action`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m21422_gshared (Action_1_t3857 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m21422(__this, ___object, ___method, method) (( void (*) (Action_1_t3857 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m21422_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
extern "C" void Action_1_Invoke_m21423_gshared (Action_1_t3857 * __this, Vector3_t758  ___obj, MethodInfo* method);
#define Action_1_Invoke_m21423(__this, ___obj, method) (( void (*) (Action_1_t3857 *, Vector3_t758 , MethodInfo*))Action_1_Invoke_m21423_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m21424_gshared (Action_1_t3857 * __this, Vector3_t758  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m21424(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t3857 *, Vector3_t758 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m21424_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m21425_gshared (Action_1_t3857 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m21425(__this, ___result, method) (( void (*) (Action_1_t3857 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m21425_gshared)(__this, ___result, method)
