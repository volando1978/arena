﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>
struct InternalEnumerator_1_t4129;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25165_gshared (InternalEnumerator_1_t4129 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25165(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4129 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25165_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25166_gshared (InternalEnumerator_1_t4129 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25166(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4129 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25166_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25167_gshared (InternalEnumerator_1_t4129 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25167(__this, method) (( void (*) (InternalEnumerator_1_t4129 *, MethodInfo*))InternalEnumerator_1_Dispose_m25167_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25168_gshared (InternalEnumerator_1_t4129 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25168(__this, method) (( bool (*) (InternalEnumerator_1_t4129 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25168_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C" ParameterModifier_t2550  InternalEnumerator_1_get_Current_m25169_gshared (InternalEnumerator_1_t4129 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25169(__this, method) (( ParameterModifier_t2550  (*) (InternalEnumerator_1_t4129 *, MethodInfo*))InternalEnumerator_1_get_Current_m25169_gshared)(__this, method)
