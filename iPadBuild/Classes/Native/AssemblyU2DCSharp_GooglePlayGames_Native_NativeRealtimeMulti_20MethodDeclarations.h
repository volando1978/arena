﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C
struct U3CCreateQuickGameU3Ec__AnonStorey2C_t595;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::.ctor()
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2C__ctor_m2450 (U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::<>m__23()
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2C_U3CU3Em__23_m2451 (U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
