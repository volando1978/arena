﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3719;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Object>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19730_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3719 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19730(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3719 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19730_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Object>::<>m__E(T)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19731_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3719 * __this, Object_t * ___result, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19731(__this, ___result, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3719 *, Object_t *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19731_gshared)(__this, ___result, method)
