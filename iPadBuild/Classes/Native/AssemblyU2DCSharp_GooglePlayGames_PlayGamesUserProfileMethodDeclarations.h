﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.PlayGamesUserProfile
struct PlayGamesUserProfile_t386;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t384;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"

// System.Void GooglePlayGames.PlayGamesUserProfile::.ctor()
extern "C" void PlayGamesUserProfile__ctor_m1579 (PlayGamesUserProfile_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesUserProfile::get_userName()
extern "C" String_t* PlayGamesUserProfile_get_userName_m1580 (PlayGamesUserProfile_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesUserProfile::get_id()
extern "C" String_t* PlayGamesUserProfile_get_id_m1581 (PlayGamesUserProfile_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesUserProfile::get_isFriend()
extern "C" bool PlayGamesUserProfile_get_isFriend_m1582 (PlayGamesUserProfile_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.UserState GooglePlayGames.PlayGamesUserProfile::get_state()
extern "C" int32_t PlayGamesUserProfile_get_state_m1583 (PlayGamesUserProfile_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GooglePlayGames.PlayGamesUserProfile::get_image()
extern "C" Texture2D_t384 * PlayGamesUserProfile_get_image_m1584 (PlayGamesUserProfile_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
