﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// explosionGenerator
struct explosionGenerator_t791;

// System.Void explosionGenerator::.ctor()
extern "C" void explosionGenerator__ctor_m3408 (explosionGenerator_t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void explosionGenerator::Start()
extern "C" void explosionGenerator_Start_m3409 (explosionGenerator_t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
