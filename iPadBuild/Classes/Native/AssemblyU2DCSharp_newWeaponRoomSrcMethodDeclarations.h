﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// newWeaponRoomSrc
struct newWeaponRoomSrc_t821;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void newWeaponRoomSrc::.ctor()
extern "C" void newWeaponRoomSrc__ctor_m3605 (newWeaponRoomSrc_t821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc::Start()
extern "C" void newWeaponRoomSrc_Start_m3606 (newWeaponRoomSrc_t821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc::Update()
extern "C" void newWeaponRoomSrc_Update_m3607 (newWeaponRoomSrc_t821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator newWeaponRoomSrc::animSpin()
extern "C" Object_t * newWeaponRoomSrc_animSpin_m3608 (newWeaponRoomSrc_t821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator newWeaponRoomSrc::waiting()
extern "C" Object_t * newWeaponRoomSrc_waiting_m3609 (newWeaponRoomSrc_t821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean newWeaponRoomSrc::isReady()
extern "C" bool newWeaponRoomSrc_isReady_m3610 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc::setReady(System.Boolean)
extern "C" void newWeaponRoomSrc_setReady_m3611 (Object_t * __this /* static, unused */, bool ___readyState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc::OnGUI()
extern "C" void newWeaponRoomSrc_OnGUI_m3612 (newWeaponRoomSrc_t821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc::OnApplicationQuit()
extern "C" void newWeaponRoomSrc_OnApplicationQuit_m3613 (newWeaponRoomSrc_t821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
