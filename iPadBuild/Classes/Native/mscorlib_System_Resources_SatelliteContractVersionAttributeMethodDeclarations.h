﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t1807;
// System.String
struct String_t;

// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
extern "C" void SatelliteContractVersionAttribute__ctor_m7689 (SatelliteContractVersionAttribute_t1807 * __this, String_t* ___version, MethodInfo* method) IL2CPP_METHOD_ATTR;
