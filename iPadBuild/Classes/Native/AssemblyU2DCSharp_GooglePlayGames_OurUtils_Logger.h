﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.OurUtils.Logger
struct  Logger_t390  : public Object_t
{
};
struct Logger_t390_StaticFields{
	// System.Boolean GooglePlayGames.OurUtils.Logger::debugLogEnabled
	bool ___debugLogEnabled_0;
	// System.Boolean GooglePlayGames.OurUtils.Logger::warningLogEnabled
	bool ___warningLogEnabled_1;
};
