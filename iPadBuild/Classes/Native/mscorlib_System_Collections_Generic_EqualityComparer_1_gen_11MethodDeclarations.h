﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t4182;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.Byte>::.ctor()
extern "C" void EqualityComparer_1__ctor_m25700_gshared (EqualityComparer_1_t4182 * __this, MethodInfo* method);
#define EqualityComparer_1__ctor_m25700(__this, method) (( void (*) (EqualityComparer_1_t4182 *, MethodInfo*))EqualityComparer_1__ctor_m25700_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Byte>::.cctor()
extern "C" void EqualityComparer_1__cctor_m25701_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1__cctor_m25701(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1__cctor_m25701_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25702_gshared (EqualityComparer_1_t4182 * __this, Object_t * ___obj, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25702(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t4182 *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25702_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25703_gshared (EqualityComparer_1_t4182 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25703(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t4182 *, Object_t *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25703_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::get_Default()
extern "C" EqualityComparer_1_t4182 * EqualityComparer_1_get_Default_m25704_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1_get_Default_m25704(__this /* static, unused */, method) (( EqualityComparer_1_t4182 * (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1_get_Default_m25704_gshared)(__this /* static, unused */, method)
