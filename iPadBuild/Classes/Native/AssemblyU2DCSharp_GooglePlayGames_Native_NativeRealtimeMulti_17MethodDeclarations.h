﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState
struct AbortingRoomCreationState_t590;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t695;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" void AbortingRoomCreationState__ctor_m2444 (AbortingRoomCreationState_t590 * __this, RoomSession_t566 * ___session, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::IsActive()
extern "C" bool AbortingRoomCreationState_IsActive_m2445 (AbortingRoomCreationState_t590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern "C" void AbortingRoomCreationState_HandleRoomResponse_m2446 (AbortingRoomCreationState_t590 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::<HandleRoomResponse>m__47()
extern "C" void AbortingRoomCreationState_U3CHandleRoomResponseU3Em__47_m2447 (AbortingRoomCreationState_t590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
