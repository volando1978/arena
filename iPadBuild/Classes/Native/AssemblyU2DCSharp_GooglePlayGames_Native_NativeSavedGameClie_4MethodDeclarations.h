﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46
struct U3CInternalManualOpenU3Ec__AnonStorey46_t623;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::.ctor()
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey46__ctor_m2511 (U3CInternalManualOpenU3Ec__AnonStorey46_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::<>m__51()
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__51_m2512 (U3CInternalManualOpenU3Ec__AnonStorey46_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::<>m__52(System.Byte[],System.Byte[])
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__52_m2513 (U3CInternalManualOpenU3Ec__AnonStorey46_t623 * __this, ByteU5BU5D_t350* ___originalData, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method) IL2CPP_METHOD_ATTR;
