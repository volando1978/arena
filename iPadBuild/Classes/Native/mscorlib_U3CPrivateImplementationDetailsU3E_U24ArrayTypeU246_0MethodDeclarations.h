﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$640
struct U24ArrayTypeU24640_t2869;
struct U24ArrayTypeU24640_t2869_marshaled;

void U24ArrayTypeU24640_t2869_marshal(const U24ArrayTypeU24640_t2869& unmarshaled, U24ArrayTypeU24640_t2869_marshaled& marshaled);
void U24ArrayTypeU24640_t2869_marshal_back(const U24ArrayTypeU24640_t2869_marshaled& marshaled, U24ArrayTypeU24640_t2869& unmarshaled);
void U24ArrayTypeU24640_t2869_marshal_cleanup(U24ArrayTypeU24640_t2869_marshaled& marshaled);
