﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ObjectDictionary
struct ObjectDictionary_t12;
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct List_1_t163;
// System.String
struct String_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// ObjectKvp
struct ObjectKvp_t6;

// System.Void ObjectDictionary::.ctor()
extern "C" void ObjectDictionary__ctor_m15 (ObjectDictionary_t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>> ObjectDictionary::get_KeyValuePairs()
extern "C" List_1_t163 * ObjectDictionary_get_KeyValuePairs_m16 (ObjectDictionary_t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectDictionary::set_KeyValuePairs(System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>)
extern "C" void ObjectDictionary_set_KeyValuePairs_m17 (ObjectDictionary_t12 * __this, List_1_t163 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectDictionary::SetKeyValuePair(System.String,System.String)
extern "C" void ObjectDictionary_SetKeyValuePair_m18 (ObjectDictionary_t12 * __this, String_t* ___k, String_t* ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String> ObjectDictionary::<get_KeyValuePairs>m__7(ObjectKvp)
extern "C" UnityKeyValuePair_2_t164 * ObjectDictionary_U3Cget_KeyValuePairsU3Em__7_m19 (Object_t * __this /* static, unused */, ObjectKvp_t6 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// ObjectKvp ObjectDictionary::<set_KeyValuePairs>m__8(UnityEngine.UnityKeyValuePair`2<System.String,System.String>)
extern "C" ObjectKvp_t6 * ObjectDictionary_U3Cset_KeyValuePairsU3Em__8_m20 (Object_t * __this /* static, unused */, UnityKeyValuePair_2_t164 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
