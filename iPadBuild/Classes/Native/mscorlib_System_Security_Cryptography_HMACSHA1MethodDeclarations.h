﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t2313;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void System.Security.Cryptography.HMACSHA1::.ctor()
extern "C" void HMACSHA1__ctor_m12981 (HMACSHA1_t2313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[])
extern "C" void HMACSHA1__ctor_m12982 (HMACSHA1_t2313 * __this, ByteU5BU5D_t350* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
