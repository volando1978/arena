﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient
struct DummyNearbyConnectionClient_t359;
// System.Collections.Generic.List`1<System.String>
struct List_1_t43;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>
struct Action_1_t845;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>
struct Action_1_t846;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>
struct Action_1_t847;
// GooglePlayGames.BasicApi.Nearby.IMessageListener
struct IMessageListener_t848;
// GooglePlayGames.BasicApi.Nearby.IDiscoveryListener
struct IDiscoveryListener_t849;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"

// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::.ctor()
extern "C" void DummyNearbyConnectionClient__ctor_m1439 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::MaxUnreliableMessagePayloadLength()
extern "C" int32_t DummyNearbyConnectionClient_MaxUnreliableMessagePayloadLength_m1440 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::MaxReliableMessagePayloadLength()
extern "C" int32_t DummyNearbyConnectionClient_MaxReliableMessagePayloadLength_m1441 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::SendReliable(System.Collections.Generic.List`1<System.String>,System.Byte[])
extern "C" void DummyNearbyConnectionClient_SendReliable_m1442 (DummyNearbyConnectionClient_t359 * __this, List_1_t43 * ___recipientEndpointIds, ByteU5BU5D_t350* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::SendUnreliable(System.Collections.Generic.List`1<System.String>,System.Byte[])
extern "C" void DummyNearbyConnectionClient_SendUnreliable_m1443 (DummyNearbyConnectionClient_t359 * __this, List_1_t43 * ___recipientEndpointIds, ByteU5BU5D_t350* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StartAdvertising(System.String,System.Collections.Generic.List`1<System.String>,System.Nullable`1<System.TimeSpan>,System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>,System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>)
extern "C" void DummyNearbyConnectionClient_StartAdvertising_m1444 (DummyNearbyConnectionClient_t359 * __this, String_t* ___name, List_1_t43 * ___appIdentifiers, Nullable_1_t377  ___advertisingDuration, Action_1_t845 * ___resultCallback, Action_1_t846 * ___connectionRequestCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StopAdvertising()
extern "C" void DummyNearbyConnectionClient_StopAdvertising_m1445 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::SendConnectionRequest(System.String,System.String,System.Byte[],System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>,GooglePlayGames.BasicApi.Nearby.IMessageListener)
extern "C" void DummyNearbyConnectionClient_SendConnectionRequest_m1446 (DummyNearbyConnectionClient_t359 * __this, String_t* ___name, String_t* ___remoteEndpointId, ByteU5BU5D_t350* ___payload, Action_1_t847 * ___responseCallback, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::AcceptConnectionRequest(System.String,System.Byte[],GooglePlayGames.BasicApi.Nearby.IMessageListener)
extern "C" void DummyNearbyConnectionClient_AcceptConnectionRequest_m1447 (DummyNearbyConnectionClient_t359 * __this, String_t* ___remoteEndpointId, ByteU5BU5D_t350* ___payload, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StartDiscovery(System.String,System.Nullable`1<System.TimeSpan>,GooglePlayGames.BasicApi.Nearby.IDiscoveryListener)
extern "C" void DummyNearbyConnectionClient_StartDiscovery_m1448 (DummyNearbyConnectionClient_t359 * __this, String_t* ___serviceId, Nullable_1_t377  ___advertisingTimeout, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StopDiscovery(System.String)
extern "C" void DummyNearbyConnectionClient_StopDiscovery_m1449 (DummyNearbyConnectionClient_t359 * __this, String_t* ___serviceId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::RejectConnectionRequest(System.String)
extern "C" void DummyNearbyConnectionClient_RejectConnectionRequest_m1450 (DummyNearbyConnectionClient_t359 * __this, String_t* ___requestingEndpointId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::DisconnectFromEndpoint(System.String)
extern "C" void DummyNearbyConnectionClient_DisconnectFromEndpoint_m1451 (DummyNearbyConnectionClient_t359 * __this, String_t* ___remoteEndpointId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StopAllConnections()
extern "C" void DummyNearbyConnectionClient_StopAllConnections_m1452 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::LocalEndpointId()
extern "C" String_t* DummyNearbyConnectionClient_LocalEndpointId_m1453 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::LocalDeviceId()
extern "C" String_t* DummyNearbyConnectionClient_LocalDeviceId_m1454 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::GetAppBundleId()
extern "C" String_t* DummyNearbyConnectionClient_GetAppBundleId_m1455 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::GetServiceId()
extern "C" String_t* DummyNearbyConnectionClient_GetServiceId_m1456 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
