﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t2373;
struct Escape_t2373_marshaled;

void Escape_t2373_marshal(const Escape_t2373& unmarshaled, Escape_t2373_marshaled& marshaled);
void Escape_t2373_marshal_back(const Escape_t2373_marshaled& marshaled, Escape_t2373& unmarshaled);
void Escape_t2373_marshal_cleanup(Escape_t2373_marshaled& marshaled);
