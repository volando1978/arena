﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// JSONObject/<StringifyAsync>c__Iterator2
struct U3CStringifyAsyncU3Ec__Iterator2_t40;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;

// System.Void JSONObject/<StringifyAsync>c__Iterator2::.ctor()
extern "C" void U3CStringifyAsyncU3Ec__Iterator2__ctor_m90 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m91 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m92 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator2::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m93 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<StringifyAsync>c__Iterator2::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C" Object_t* U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m94 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<StringifyAsync>c__Iterator2::MoveNext()
extern "C" bool U3CStringifyAsyncU3Ec__Iterator2_MoveNext_m95 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator2::Dispose()
extern "C" void U3CStringifyAsyncU3Ec__Iterator2_Dispose_m96 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator2::Reset()
extern "C" void U3CStringifyAsyncU3Ec__Iterator2_Reset_m97 (U3CStringifyAsyncU3Ec__Iterator2_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
