﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_4.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_4MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_7.h"
#include "mscorlib_ArrayTypes.h"
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E::.ctor()
extern "C" void U3CPeersDisconnectedU3Ec__AnonStorey3E__ctor_m2358 (U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E::<>m__34()
extern TypeInfo* RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var;
extern "C" void U3CPeersDisconnectedU3Ec__AnonStorey3E_U3CU3Em__34_m2359 (U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGameThreadForwardingListener_t563 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		Object_t * L_1 = (L_0->___mListener_0);
		StringU5BU5D_t169* L_2 = (__this->___participantIds_0);
		NullCheck(L_1);
		InterfaceActionInvoker1< StringU5BU5D_t169* >::Invoke(5 /* System.Void GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener::OnPeersDisconnected(System.String[]) */, RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var, L_1, L_2);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey3F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_5.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey3F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_5MethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Byte
#include "mscorlib_System_Byte.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey3F::.ctor()
extern "C" void U3CRealTimeMessageReceivedU3Ec__AnonStorey3F__ctor_m2360 (U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey3F::<>m__35()
extern TypeInfo* RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var;
extern "C" void U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_U3CU3Em__35_m2361 (U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGameThreadForwardingListener_t563 * L_0 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_0);
		Object_t * L_1 = (L_0->___mListener_0);
		bool L_2 = (__this->___isReliable_0);
		String_t* L_3 = (__this->___senderId_1);
		ByteU5BU5D_t350* L_4 = (__this->___data_2);
		NullCheck(L_1);
		InterfaceActionInvoker3< bool, String_t*, ByteU5BU5D_t350* >::Invoke(6 /* System.Void GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener::OnRealTimeMessageReceived(System.Boolean,System.String,System.Byte[]) */, RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var, L_1, L_2, L_3, L_4);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_6.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_6MethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.Participant
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti_0.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40::.ctor()
extern "C" void U3CParticipantLeftU3Ec__AnonStorey40__ctor_m2362 (U3CParticipantLeftU3Ec__AnonStorey40_t572 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40::<>m__36()
extern TypeInfo* RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var;
extern "C" void U3CParticipantLeftU3Ec__AnonStorey40_U3CU3Em__36_m2363 (U3CParticipantLeftU3Ec__AnonStorey40_t572 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGameThreadForwardingListener_t563 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		Object_t * L_1 = (L_0->___mListener_0);
		Participant_t340 * L_2 = (__this->___participant_0);
		NullCheck(L_1);
		InterfaceActionInvoker1< Participant_t340 * >::Invoke(3 /* System.Void GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener::OnParticipantLeft(GooglePlayGames.BasicApi.Multiplayer.Participant) */, RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var, L_1, L_2);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_7MethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_1.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_2.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_3.h"
// GooglePlayGames.OurUtils.Misc
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_MiscMethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_1MethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// GooglePlayGames.OurUtils.PlayGamesHelperObject
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGamesHelperObMethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_2MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_3MethodDeclarations.h"
struct Misc_t391;
struct RealTimeMultiplayerListener_t573;
// GooglePlayGames.OurUtils.Misc
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Misc.h"
struct Misc_t391;
struct Object_t;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(!!0)
extern "C" Object_t * Misc_CheckNotNull_TisObject_t_m3706_gshared (Object_t * __this /* static, unused */, Object_t * p0, MethodInfo* method);
#define Misc_CheckNotNull_TisObject_t_m3706(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener>(!!0)
#define Misc_CheckNotNull_TisRealTimeMultiplayerListener_t573_m3795(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::.ctor(GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern MethodInfo* Misc_CheckNotNull_TisRealTimeMultiplayerListener_t573_m3795_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener__ctor_m2364 (OnGameThreadForwardingListener_t563 * __this, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisRealTimeMultiplayerListener_t573_m3795_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483909);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___listener;
		Object_t * L_1 = Misc_CheckNotNull_TisRealTimeMultiplayerListener_t573_m3795(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisRealTimeMultiplayerListener_t573_m3795_MethodInfo_var);
		__this->___mListener_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::RoomSetupProgress(System.Single)
extern TypeInfo* U3CRoomSetupProgressU3Ec__AnonStorey3B_t567_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* U3CRoomSetupProgressU3Ec__AnonStorey3B_U3CU3Em__30_m2353_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener_RoomSetupProgress_m2365 (OnGameThreadForwardingListener_t563 * __this, float ___percent, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRoomSetupProgressU3Ec__AnonStorey3B_t567_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(741);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		U3CRoomSetupProgressU3Ec__AnonStorey3B_U3CU3Em__30_m2353_MethodInfo_var = il2cpp_codegen_method_info_from_index(262);
		s_Il2CppMethodIntialized = true;
	}
	U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * V_0 = {0};
	{
		U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * L_0 = (U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 *)il2cpp_codegen_object_new (U3CRoomSetupProgressU3Ec__AnonStorey3B_t567_il2cpp_TypeInfo_var);
		U3CRoomSetupProgressU3Ec__AnonStorey3B__ctor_m2352(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * L_1 = V_0;
		float L_2 = ___percent;
		NullCheck(L_1);
		L_1->___percent_0 = L_2;
		U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * L_4 = V_0;
		IntPtr_t L_5 = { U3CRoomSetupProgressU3Ec__AnonStorey3B_U3CU3Em__30_m2353_MethodInfo_var };
		Action_t588 * L_6 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::RoomConnected(System.Boolean)
extern TypeInfo* U3CRoomConnectedU3Ec__AnonStorey3C_t568_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* U3CRoomConnectedU3Ec__AnonStorey3C_U3CU3Em__31_m2355_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener_RoomConnected_m2366 (OnGameThreadForwardingListener_t563 * __this, bool ___success, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRoomConnectedU3Ec__AnonStorey3C_t568_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(742);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		U3CRoomConnectedU3Ec__AnonStorey3C_U3CU3Em__31_m2355_MethodInfo_var = il2cpp_codegen_method_info_from_index(263);
		s_Il2CppMethodIntialized = true;
	}
	U3CRoomConnectedU3Ec__AnonStorey3C_t568 * V_0 = {0};
	{
		U3CRoomConnectedU3Ec__AnonStorey3C_t568 * L_0 = (U3CRoomConnectedU3Ec__AnonStorey3C_t568 *)il2cpp_codegen_object_new (U3CRoomConnectedU3Ec__AnonStorey3C_t568_il2cpp_TypeInfo_var);
		U3CRoomConnectedU3Ec__AnonStorey3C__ctor_m2354(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRoomConnectedU3Ec__AnonStorey3C_t568 * L_1 = V_0;
		bool L_2 = ___success;
		NullCheck(L_1);
		L_1->___success_0 = L_2;
		U3CRoomConnectedU3Ec__AnonStorey3C_t568 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CRoomConnectedU3Ec__AnonStorey3C_t568 * L_4 = V_0;
		IntPtr_t L_5 = { U3CRoomConnectedU3Ec__AnonStorey3C_U3CU3Em__31_m2355_MethodInfo_var };
		Action_t588 * L_6 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::LeftRoom()
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* OnGameThreadForwardingListener_U3CLeftRoomU3Em__32_m2372_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener_LeftRoom_m2367 (OnGameThreadForwardingListener_t563 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		OnGameThreadForwardingListener_U3CLeftRoomU3Em__32_m2372_MethodInfo_var = il2cpp_codegen_method_info_from_index(264);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { OnGameThreadForwardingListener_U3CLeftRoomU3Em__32_m2372_MethodInfo_var };
		Action_t588 * L_1 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::PeersConnected(System.String[])
extern TypeInfo* U3CPeersConnectedU3Ec__AnonStorey3D_t569_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* U3CPeersConnectedU3Ec__AnonStorey3D_U3CU3Em__33_m2357_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener_PeersConnected_m2368 (OnGameThreadForwardingListener_t563 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CPeersConnectedU3Ec__AnonStorey3D_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(743);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		U3CPeersConnectedU3Ec__AnonStorey3D_U3CU3Em__33_m2357_MethodInfo_var = il2cpp_codegen_method_info_from_index(265);
		s_Il2CppMethodIntialized = true;
	}
	U3CPeersConnectedU3Ec__AnonStorey3D_t569 * V_0 = {0};
	{
		U3CPeersConnectedU3Ec__AnonStorey3D_t569 * L_0 = (U3CPeersConnectedU3Ec__AnonStorey3D_t569 *)il2cpp_codegen_object_new (U3CPeersConnectedU3Ec__AnonStorey3D_t569_il2cpp_TypeInfo_var);
		U3CPeersConnectedU3Ec__AnonStorey3D__ctor_m2356(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPeersConnectedU3Ec__AnonStorey3D_t569 * L_1 = V_0;
		StringU5BU5D_t169* L_2 = ___participantIds;
		NullCheck(L_1);
		L_1->___participantIds_0 = L_2;
		U3CPeersConnectedU3Ec__AnonStorey3D_t569 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CPeersConnectedU3Ec__AnonStorey3D_t569 * L_4 = V_0;
		IntPtr_t L_5 = { U3CPeersConnectedU3Ec__AnonStorey3D_U3CU3Em__33_m2357_MethodInfo_var };
		Action_t588 * L_6 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::PeersDisconnected(System.String[])
extern TypeInfo* U3CPeersDisconnectedU3Ec__AnonStorey3E_t570_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* U3CPeersDisconnectedU3Ec__AnonStorey3E_U3CU3Em__34_m2359_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener_PeersDisconnected_m2369 (OnGameThreadForwardingListener_t563 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CPeersDisconnectedU3Ec__AnonStorey3E_t570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(744);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		U3CPeersDisconnectedU3Ec__AnonStorey3E_U3CU3Em__34_m2359_MethodInfo_var = il2cpp_codegen_method_info_from_index(266);
		s_Il2CppMethodIntialized = true;
	}
	U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * V_0 = {0};
	{
		U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * L_0 = (U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 *)il2cpp_codegen_object_new (U3CPeersDisconnectedU3Ec__AnonStorey3E_t570_il2cpp_TypeInfo_var);
		U3CPeersDisconnectedU3Ec__AnonStorey3E__ctor_m2358(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * L_1 = V_0;
		StringU5BU5D_t169* L_2 = ___participantIds;
		NullCheck(L_1);
		L_1->___participantIds_0 = L_2;
		U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * L_4 = V_0;
		IntPtr_t L_5 = { U3CPeersDisconnectedU3Ec__AnonStorey3E_U3CU3Em__34_m2359_MethodInfo_var };
		Action_t588 * L_6 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::RealTimeMessageReceived(System.Boolean,System.String,System.Byte[])
extern TypeInfo* U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_U3CU3Em__35_m2361_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener_RealTimeMessageReceived_m2370 (OnGameThreadForwardingListener_t563 * __this, bool ___isReliable, String_t* ___senderId, ByteU5BU5D_t350* ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(745);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_U3CU3Em__35_m2361_MethodInfo_var = il2cpp_codegen_method_info_from_index(267);
		s_Il2CppMethodIntialized = true;
	}
	U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * V_0 = {0};
	{
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * L_0 = (U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 *)il2cpp_codegen_object_new (U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571_il2cpp_TypeInfo_var);
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F__ctor_m2360(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * L_1 = V_0;
		bool L_2 = ___isReliable;
		NullCheck(L_1);
		L_1->___isReliable_0 = L_2;
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * L_3 = V_0;
		String_t* L_4 = ___senderId;
		NullCheck(L_3);
		L_3->___senderId_1 = L_4;
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * L_5 = V_0;
		ByteU5BU5D_t350* L_6 = ___data;
		NullCheck(L_5);
		L_5->___data_2 = L_6;
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * L_7 = V_0;
		NullCheck(L_7);
		L_7->___U3CU3Ef__this_3 = __this;
		U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * L_8 = V_0;
		IntPtr_t L_9 = { U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_U3CU3Em__35_m2361_MethodInfo_var };
		Action_t588 * L_10 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_10, L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::ParticipantLeft(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern TypeInfo* U3CParticipantLeftU3Ec__AnonStorey40_t572_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* U3CParticipantLeftU3Ec__AnonStorey40_U3CU3Em__36_m2363_MethodInfo_var;
extern "C" void OnGameThreadForwardingListener_ParticipantLeft_m2371 (OnGameThreadForwardingListener_t563 * __this, Participant_t340 * ___participant, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CParticipantLeftU3Ec__AnonStorey40_t572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(746);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		U3CParticipantLeftU3Ec__AnonStorey40_U3CU3Em__36_m2363_MethodInfo_var = il2cpp_codegen_method_info_from_index(268);
		s_Il2CppMethodIntialized = true;
	}
	U3CParticipantLeftU3Ec__AnonStorey40_t572 * V_0 = {0};
	{
		U3CParticipantLeftU3Ec__AnonStorey40_t572 * L_0 = (U3CParticipantLeftU3Ec__AnonStorey40_t572 *)il2cpp_codegen_object_new (U3CParticipantLeftU3Ec__AnonStorey40_t572_il2cpp_TypeInfo_var);
		U3CParticipantLeftU3Ec__AnonStorey40__ctor_m2362(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CParticipantLeftU3Ec__AnonStorey40_t572 * L_1 = V_0;
		Participant_t340 * L_2 = ___participant;
		NullCheck(L_1);
		L_1->___participant_0 = L_2;
		U3CParticipantLeftU3Ec__AnonStorey40_t572 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CParticipantLeftU3Ec__AnonStorey40_t572 * L_4 = V_0;
		IntPtr_t L_5 = { U3CParticipantLeftU3Ec__AnonStorey40_U3CU3Em__36_m2363_MethodInfo_var };
		Action_t588 * L_6 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::<LeftRoom>m__32()
extern TypeInfo* RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var;
extern "C" void OnGameThreadForwardingListener_U3CLeftRoomU3Em__32_m2372 (OnGameThreadForwardingListener_t563 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mListener_0);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener::OnLeftRoom() */, RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_8.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_8MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_NativeRealT.h"
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Multiplayer_0.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_gen_14.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// GooglePlayGames.OurUtils.Logger
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_LoggerMethodDeclarations.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_gen_14MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::.ctor()
extern "C" void State__ctor_m2373 (State_t565 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_HandleRoomResponse_m2374 (State_t565 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral492, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::IsActive()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" bool State_IsActive_m2375 (State_t565 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral493, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::LeaveRoom()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_LeaveRoom_m2376 (State_t565 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral494, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::ShowWaitingRoomUI(System.UInt32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_ShowWaitingRoomUI_m2377 (State_t565 * __this, uint32_t ___minimumParticipantsBeforeStarting, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral495, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnStateEntered()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_OnStateEntered_m2378 (State_t565 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral496, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_OnRoomStatusChanged_m2379 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral497, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_OnConnectedSetChanged_m2380 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral498, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_OnParticipantStatusChanged_m2381 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral499, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnDataReceived(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_OnDataReceived_m2382 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___sender, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral500, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::SendToSpecificRecipient(System.String,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_SendToSpecificRecipient_m2383 (State_t565 * __this, String_t* ___recipientId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral501, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::SendToAll(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void State_SendToAll_m2384 (State_t565 * __this, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral502, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetConnectedParticipants()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t351_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3797_MethodInfo_var;
extern "C" List_1_t351 * State_GetConnectedParticipants_m2385 (State_t565 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		List_1_t351_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(747);
		List_1__ctor_m3797_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483917);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral503, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t351_il2cpp_TypeInfo_var);
		List_1_t351 * L_3 = (List_1_t351 *)il2cpp_codegen_object_new (List_1_t351_il2cpp_TypeInfo_var);
		List_1__ctor_m3797(L_3, /*hidden argument*/List_1__ctor_m3797_MethodInfo_var);
		return L_3;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetSelf()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" Participant_t340 * State_GetSelf_m2386 (State_t565 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral504, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (Participant_t340 *)NULL;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetParticipant(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" Participant_t340 * State_GetParticipant_m2387 (State_t565 * __this, String_t* ___participantId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (Participant_t340 *)NULL;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::IsRoomConnected()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" bool State_IsRoomConnected_m2388 (State_t565 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m932(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral506, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return 0;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_9.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_9MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_0.h"
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_BaseReferen.h"
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>
#include "System_Core_System_Func_2_gen_1.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1.h"
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "System_Core_System_Func_2_gen_2.h"
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>
#include "System_Core_System_Func_2_gen_0.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_13.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2.h"
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.Boolean>
#include "System_Core_System_Func_2_gen_3.h"
// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_List_1_gen_19.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_3.h"
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>
#include "mscorlib_System_Action_1_gen_29.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_0.h"
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_BaseReferenMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_NativeRealTMethodDeclarations.h"
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>
#include "System_Core_System_Func_2_gen_1MethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12MethodDeclarations.h"
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "System_Core_System_Func_2_gen_2MethodDeclarations.h"
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>
#include "System_Core_System_Func_2_gen_0MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_13MethodDeclarations.h"
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.Boolean>
#include "System_Core_System_Func_2_gen_3MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_0MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_3MethodDeclarations.h"
// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_List_1_gen_19MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_0MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollectiMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Multiplayer_0MethodDeclarations.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti_0MethodDeclarations.h"
struct Misc_t391;
struct RoomSession_t566;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession>(!!0)
#define Misc_CheckNotNull_TisRoomSession_t566_m3798(__this /* static, unused */, p0, method) (( RoomSession_t566 * (*) (Object_t * /* static, unused */, RoomSession_t566 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct NativeRealTimeRoom_t574;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>(!!0)
#define Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799(__this /* static, unused */, p0, method) (( NativeRealTimeRoom_t574 * (*) (Object_t * /* static, unused */, NativeRealTimeRoom_t574 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t219;
struct Dictionary_2_t575;
struct IEnumerable_1_t874;
struct Func_2_t577;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t219;
struct Dictionary_2_t920;
struct IEnumerable_1_t221;
struct Func_2_t903;
// Declaration System.Collections.Generic.Dictionary`2<!!1,!!0> System.Linq.Enumerable::ToDictionary<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.Dictionary`2<!!1,!!0> System.Linq.Enumerable::ToDictionary<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C" Dictionary_2_t920 * Enumerable_ToDictionary_TisObject_t_TisObject_t_m3801_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t903 * p1, MethodInfo* method);
#define Enumerable_ToDictionary_TisObject_t_TisObject_t_m3801(__this /* static, unused */, p0, p1, method) (( Dictionary_2_t920 * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t903 *, MethodInfo*))Enumerable_ToDictionary_TisObject_t_TisObject_t_m3801_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.Dictionary`2<!!1,!!0> System.Linq.Enumerable::ToDictionary<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.Dictionary`2<!!1,!!0> System.Linq.Enumerable::ToDictionary<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800(__this /* static, unused */, p0, p1, method) (( Dictionary_2_t575 * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t577 *, MethodInfo*))Enumerable_ToDictionary_TisObject_t_TisObject_t_m3801_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct IEnumerable_1_t902;
struct IEnumerable_1_t874;
struct Func_2_t578;
struct Enumerable_t219;
struct IEnumerable_1_t221;
struct Func_2_t903;
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C" Object_t* Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t903 * p1, MethodInfo* method);
#define Enumerable_Select_TisObject_t_TisObject_t_m3696(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t903 *, MethodInfo*))Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t578 *, MethodInfo*))Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct Dictionary_2_t576;
struct IEnumerable_1_t902;
struct Func_2_t352;
// Declaration System.Collections.Generic.Dictionary`2<!!1,!!0> System.Linq.Enumerable::ToDictionary<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.Dictionary`2<!!1,!!0> System.Linq.Enumerable::ToDictionary<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803(__this /* static, unused */, p0, p1, method) (( Dictionary_2_t576 * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t352 *, MethodInfo*))Enumerable_ToDictionary_TisObject_t_TisObject_t_m3801_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct IEnumerable_1_t902;
struct Func_2_t579;
struct Enumerable_t219;
struct IEnumerable_1_t221;
struct Func_2_t222;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" Object_t* Enumerable_Where_TisObject_t_m3805_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t222 * p1, MethodInfo* method);
#define Enumerable_Where_TisObject_t_m3805(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t222 *, MethodInfo*))Enumerable_Where_TisObject_t_m3805_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<GooglePlayGames.BasicApi.Multiplayer.Participant>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<GooglePlayGames.BasicApi.Multiplayer.Participant>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisParticipant_t340_m3804(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t579 *, MethodInfo*))Enumerable_Where_TisObject_t_m3805_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct List_1_t351;
struct IEnumerable_1_t902;
struct Enumerable_t219;
struct List_1_t181;
struct IEnumerable_1_t221;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" List_1_t181 * Enumerable_ToList_TisObject_t_m3763_gshared (Object_t * __this /* static, unused */, Object_t* p0, MethodInfo* method);
#define Enumerable_ToList_TisObject_t_m3763(__this /* static, unused */, p0, method) (( List_1_t181 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.Multiplayer.Participant>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.Multiplayer.Participant>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisParticipant_t340_m3806(__this /* static, unused */, p0, method) (( List_1_t351 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession,GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern MethodInfo* Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var;
extern "C" void MessagingEnabledState__ctor_m2389 (MessagingEnabledState_t580 * __this, RoomSession_t566 * ___session, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		State__ctor_m2373(__this, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = ___session;
		RoomSession_t566 * L_1 = Misc_CheckNotNull_TisRoomSession_t566_m3798(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var);
		__this->___mSession_0 = L_1;
		NativeRealTimeRoom_t574 * L_2 = ___room;
		MessagingEnabledState_UpdateCurrentRoom_m2390(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::UpdateCurrentRoom(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern TypeInfo* MessagingEnabledState_t580_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t577_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t578_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t352_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799_MethodInfo_var;
extern MethodInfo* MessagingEnabledState_U3CUpdateCurrentRoomU3Em__37_m2401_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3807_MethodInfo_var;
extern MethodInfo* Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800_MethodInfo_var;
extern MethodInfo* Dictionary_2_get_Values_m3808_MethodInfo_var;
extern MethodInfo* MessagingEnabledState_U3CUpdateCurrentRoomU3Em__38_m2402_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3809_MethodInfo_var;
extern MethodInfo* Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802_MethodInfo_var;
extern MethodInfo* MessagingEnabledState_U3CUpdateCurrentRoomU3Em__39_m2403_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3703_MethodInfo_var;
extern MethodInfo* Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803_MethodInfo_var;
extern "C" void MessagingEnabledState_UpdateCurrentRoom_m2390 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MessagingEnabledState_t580_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(751);
		Func_2_t577_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(752);
		Func_2_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(753);
		Func_2_t352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483919);
		MessagingEnabledState_U3CUpdateCurrentRoomU3Em__37_m2401_MethodInfo_var = il2cpp_codegen_method_info_from_index(272);
		Func_2__ctor_m3807_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483921);
		Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483922);
		Dictionary_2_get_Values_m3808_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483923);
		MessagingEnabledState_U3CUpdateCurrentRoomU3Em__38_m2402_MethodInfo_var = il2cpp_codegen_method_info_from_index(276);
		Func_2__ctor_m3809_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483925);
		Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483926);
		MessagingEnabledState_U3CUpdateCurrentRoomU3Em__39_m2403_MethodInfo_var = il2cpp_codegen_method_info_from_index(279);
		Func_2__ctor_m3703_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483822);
		Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483928);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* G_B4_0 = {0};
	MessagingEnabledState_t580 * G_B4_1 = {0};
	Object_t* G_B3_0 = {0};
	MessagingEnabledState_t580 * G_B3_1 = {0};
	ValueCollection_t921 * G_B6_0 = {0};
	MessagingEnabledState_t580 * G_B6_1 = {0};
	ValueCollection_t921 * G_B5_0 = {0};
	MessagingEnabledState_t580 * G_B5_1 = {0};
	Object_t* G_B8_0 = {0};
	MessagingEnabledState_t580 * G_B8_1 = {0};
	Object_t* G_B7_0 = {0};
	MessagingEnabledState_t580 * G_B7_1 = {0};
	{
		NativeRealTimeRoom_t574 * L_0 = (__this->___mRoom_1);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NativeRealTimeRoom_t574 * L_1 = (__this->___mRoom_1);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_1);
	}

IL_0016:
	{
		NativeRealTimeRoom_t574 * L_2 = ___room;
		NativeRealTimeRoom_t574 * L_3 = Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799_MethodInfo_var);
		__this->___mRoom_1 = L_3;
		NativeRealTimeRoom_t574 * L_4 = (__this->___mRoom_1);
		NullCheck(L_4);
		Object_t* L_5 = NativeRealTimeRoom_Participants_m2798(L_4, /*hidden argument*/NULL);
		Func_2_t577 * L_6 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B3_0 = L_5;
		G_B3_1 = __this;
		if (L_6)
		{
			G_B4_0 = L_5;
			G_B4_1 = __this;
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_7 = { MessagingEnabledState_U3CUpdateCurrentRoomU3Em__37_m2401_MethodInfo_var };
		Func_2_t577 * L_8 = (Func_2_t577 *)il2cpp_codegen_object_new (Func_2_t577_il2cpp_TypeInfo_var);
		Func_2__ctor_m3807(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m3807_MethodInfo_var);
		((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_8;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0046:
	{
		Func_2_t577 * L_9 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		Dictionary_2_t575 * L_10 = Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800(NULL /*static, unused*/, G_B4_0, L_9, /*hidden argument*/Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800_MethodInfo_var);
		NullCheck(G_B4_1);
		G_B4_1->___mNativeParticipants_2 = L_10;
		Dictionary_2_t575 * L_11 = (__this->___mNativeParticipants_2);
		NullCheck(L_11);
		ValueCollection_t921 * L_12 = Dictionary_2_get_Values_m3808(L_11, /*hidden argument*/Dictionary_2_get_Values_m3808_MethodInfo_var);
		Func_2_t578 * L_13 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B5_0 = L_12;
		G_B5_1 = __this;
		if (L_13)
		{
			G_B6_0 = L_12;
			G_B6_1 = __this;
			goto IL_0079;
		}
	}
	{
		IntPtr_t L_14 = { MessagingEnabledState_U3CUpdateCurrentRoomU3Em__38_m2402_MethodInfo_var };
		Func_2_t578 * L_15 = (Func_2_t578 *)il2cpp_codegen_object_new (Func_2_t578_il2cpp_TypeInfo_var);
		Func_2__ctor_m3809(L_15, NULL, L_14, /*hidden argument*/Func_2__ctor_m3809_MethodInfo_var);
		((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_15;
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0079:
	{
		Func_2_t578 * L_16 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		Object_t* L_17 = Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802(NULL /*static, unused*/, G_B6_0, L_16, /*hidden argument*/Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802_MethodInfo_var);
		Func_2_t352 * L_18 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		G_B7_0 = L_17;
		G_B7_1 = G_B6_1;
		if (L_18)
		{
			G_B8_0 = L_17;
			G_B8_1 = G_B6_1;
			goto IL_009b;
		}
	}
	{
		IntPtr_t L_19 = { MessagingEnabledState_U3CUpdateCurrentRoomU3Em__39_m2403_MethodInfo_var };
		Func_2_t352 * L_20 = (Func_2_t352 *)il2cpp_codegen_object_new (Func_2_t352_il2cpp_TypeInfo_var);
		Func_2__ctor_m3703(L_20, NULL, L_19, /*hidden argument*/Func_2__ctor_m3703_MethodInfo_var);
		((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6 = L_20;
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_009b:
	{
		Func_2_t352 * L_21 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		Dictionary_2_t576 * L_22 = Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803(NULL /*static, unused*/, G_B8_0, L_21, /*hidden argument*/Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803_MethodInfo_var);
		NullCheck(G_B8_1);
		G_B8_1->___mParticipants_3 = L_22;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_OnRoomStatusChanged_m2391 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	{
		NativeRealTimeRoom_t574 * L_0 = ___room;
		VirtActionInvoker1< NativeRealTimeRoom_t574 * >::Invoke(19 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom) */, __this, L_0);
		NativeRealTimeRoom_t574 * L_1 = ___room;
		MessagingEnabledState_UpdateCurrentRoom_m2390(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_HandleRoomStatusChanged_m2392 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_OnConnectedSetChanged_m2393 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	{
		NativeRealTimeRoom_t574 * L_0 = ___room;
		VirtActionInvoker1< NativeRealTimeRoom_t574 * >::Invoke(20 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom) */, __this, L_0);
		NativeRealTimeRoom_t574 * L_1 = ___room;
		MessagingEnabledState_UpdateCurrentRoom_m2390(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_HandleConnectedSetChanged_m2394 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void MessagingEnabledState_OnParticipantStatusChanged_m2395 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method)
{
	{
		NativeRealTimeRoom_t574 * L_0 = ___room;
		MultiplayerParticipant_t672 * L_1 = ___participant;
		VirtActionInvoker2< NativeRealTimeRoom_t574 *, MultiplayerParticipant_t672 * >::Invoke(21 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant) */, __this, L_0, L_1);
		NativeRealTimeRoom_t574 * L_2 = ___room;
		MessagingEnabledState_UpdateCurrentRoom_m2390(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void MessagingEnabledState_HandleParticipantStatusChanged_m2396 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::GetConnectedParticipants()
extern TypeInfo* MessagingEnabledState_t580_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t579_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2_get_Values_m3810_MethodInfo_var;
extern MethodInfo* MessagingEnabledState_U3CGetConnectedParticipantsU3Em__3A_m2404_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3811_MethodInfo_var;
extern MethodInfo* Enumerable_Where_TisParticipant_t340_m3804_MethodInfo_var;
extern MethodInfo* Enumerable_ToList_TisParticipant_t340_m3806_MethodInfo_var;
extern MethodInfo* List_1_Sort_m3699_MethodInfo_var;
extern "C" List_1_t351 * MessagingEnabledState_GetConnectedParticipants_m2397 (MessagingEnabledState_t580 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MessagingEnabledState_t580_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(751);
		Func_2_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(754);
		Dictionary_2_get_Values_m3810_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483929);
		MessagingEnabledState_U3CGetConnectedParticipantsU3Em__3A_m2404_MethodInfo_var = il2cpp_codegen_method_info_from_index(282);
		Func_2__ctor_m3811_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483931);
		Enumerable_Where_TisParticipant_t340_m3804_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		Enumerable_ToList_TisParticipant_t340_m3806_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		List_1_Sort_m3699_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483817);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t351 * V_0 = {0};
	ValueCollection_t922 * G_B2_0 = {0};
	ValueCollection_t922 * G_B1_0 = {0};
	{
		Dictionary_2_t576 * L_0 = (__this->___mParticipants_3);
		NullCheck(L_0);
		ValueCollection_t922 * L_1 = Dictionary_2_get_Values_m3810(L_0, /*hidden argument*/Dictionary_2_get_Values_m3810_MethodInfo_var);
		Func_2_t579 * L_2 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_0023;
		}
	}
	{
		IntPtr_t L_3 = { MessagingEnabledState_U3CGetConnectedParticipantsU3Em__3A_m2404_MethodInfo_var };
		Func_2_t579 * L_4 = (Func_2_t579 *)il2cpp_codegen_object_new (Func_2_t579_il2cpp_TypeInfo_var);
		Func_2__ctor_m3811(L_4, NULL, L_3, /*hidden argument*/Func_2__ctor_m3811_MethodInfo_var);
		((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7 = L_4;
		G_B2_0 = G_B1_0;
	}

IL_0023:
	{
		Func_2_t579 * L_5 = ((MessagingEnabledState_t580_StaticFields*)MessagingEnabledState_t580_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		Object_t* L_6 = Enumerable_Where_TisParticipant_t340_m3804(NULL /*static, unused*/, G_B2_0, L_5, /*hidden argument*/Enumerable_Where_TisParticipant_t340_m3804_MethodInfo_var);
		List_1_t351 * L_7 = Enumerable_ToList_TisParticipant_t340_m3806(NULL /*static, unused*/, L_6, /*hidden argument*/Enumerable_ToList_TisParticipant_t340_m3806_MethodInfo_var);
		V_0 = L_7;
		List_1_t351 * L_8 = V_0;
		NullCheck(L_8);
		List_1_Sort_m3699(L_8, /*hidden argument*/List_1_Sort_m3699_MethodInfo_var);
		List_1_t351 * L_9 = V_0;
		return L_9;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::SendToSpecificRecipient(System.String,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t891_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3812_MethodInfo_var;
extern "C" void MessagingEnabledState_SendToSpecificRecipient_m2398 (MessagingEnabledState_t580 * __this, String_t* ___recipientId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		List_1_t891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(755);
		List_1__ctor_m3812_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t891 * V_0 = {0};
	{
		Dictionary_2_t575 * L_0 = (__this->___mNativeParticipants_2);
		String_t* L_1 = ___recipientId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___recipientId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral507, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		bool L_5 = ___isReliable;
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		RoomSession_t566 * L_6 = (__this->___mSession_0);
		NullCheck(L_6);
		RealtimeManager_t564 * L_7 = RoomSession_Manager_m2331(L_6, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_8 = (__this->___mRoom_1);
		Dictionary_2_t575 * L_9 = (__this->___mNativeParticipants_2);
		String_t* L_10 = ___recipientId;
		NullCheck(L_9);
		MultiplayerParticipant_t672 * L_11 = (MultiplayerParticipant_t672 *)VirtFuncInvoker1< MultiplayerParticipant_t672 *, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Item(!0) */, L_9, L_10);
		ByteU5BU5D_t350* L_12 = ___data;
		int32_t L_13 = ___offset;
		int32_t L_14 = ___length;
		ByteU5BU5D_t350* L_15 = Misc_GetSubsetBytes_m1597(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_7);
		RealtimeManager_SendReliableMessage_m2991(L_7, L_8, L_11, L_15, (Action_1_t890 *)NULL, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_005a:
	{
		RoomSession_t566 * L_16 = (__this->___mSession_0);
		NullCheck(L_16);
		RealtimeManager_t564 * L_17 = RoomSession_Manager_m2331(L_16, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_18 = (__this->___mRoom_1);
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t891_il2cpp_TypeInfo_var);
		List_1_t891 * L_19 = (List_1_t891 *)il2cpp_codegen_object_new (List_1_t891_il2cpp_TypeInfo_var);
		List_1__ctor_m3812(L_19, /*hidden argument*/List_1__ctor_m3812_MethodInfo_var);
		V_0 = L_19;
		List_1_t891 * L_20 = V_0;
		Dictionary_2_t575 * L_21 = (__this->___mNativeParticipants_2);
		String_t* L_22 = ___recipientId;
		NullCheck(L_21);
		MultiplayerParticipant_t672 * L_23 = (MultiplayerParticipant_t672 *)VirtFuncInvoker1< MultiplayerParticipant_t672 *, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Item(!0) */, L_21, L_22);
		NullCheck(L_20);
		VirtActionInvoker1< MultiplayerParticipant_t672 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Add(!0) */, L_20, L_23);
		List_1_t891 * L_24 = V_0;
		ByteU5BU5D_t350* L_25 = ___data;
		int32_t L_26 = ___offset;
		int32_t L_27 = ___length;
		ByteU5BU5D_t350* L_28 = Misc_GetSubsetBytes_m1597(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/NULL);
		NullCheck(L_17);
		RealtimeManager_SendUnreliableMessageToSpecificParticipants_m2994(L_17, L_18, L_24, L_28, /*hidden argument*/NULL);
	}

IL_0092:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::SendToAll(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern TypeInfo* Enumerator_t923_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2_get_Keys_m3813_MethodInfo_var;
extern MethodInfo* KeyCollection_GetEnumerator_m3814_MethodInfo_var;
extern MethodInfo* Enumerator_get_Current_m3815_MethodInfo_var;
extern MethodInfo* Enumerator_MoveNext_m3816_MethodInfo_var;
extern "C" void MessagingEnabledState_SendToAll_m2399 (MessagingEnabledState_t580 * __this, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t923_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(756);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Dictionary_2_get_Keys_m3813_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		KeyCollection_GetEnumerator_m3814_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		Enumerator_get_Current_m3815_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		Enumerator_MoveNext_m3816_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t350* V_0 = {0};
	String_t* V_1 = {0};
	Enumerator_t923  V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t350* L_0 = ___data;
		int32_t L_1 = ___offset;
		int32_t L_2 = ___length;
		ByteU5BU5D_t350* L_3 = Misc_GetSubsetBytes_m1597(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = ___isReliable;
		if (!L_4)
		{
			goto IL_005d;
		}
	}
	{
		Dictionary_2_t575 * L_5 = (__this->___mNativeParticipants_2);
		NullCheck(L_5);
		KeyCollection_t924 * L_6 = Dictionary_2_get_Keys_m3813(L_5, /*hidden argument*/Dictionary_2_get_Keys_m3813_MethodInfo_var);
		NullCheck(L_6);
		Enumerator_t923  L_7 = KeyCollection_GetEnumerator_m3814(L_6, /*hidden argument*/KeyCollection_GetEnumerator_m3814_MethodInfo_var);
		V_2 = L_7;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003b;
		}

IL_0026:
		{
			String_t* L_8 = Enumerator_get_Current_m3815((&V_2), /*hidden argument*/Enumerator_get_Current_m3815_MethodInfo_var);
			V_1 = L_8;
			String_t* L_9 = V_1;
			ByteU5BU5D_t350* L_10 = V_0;
			ByteU5BU5D_t350* L_11 = V_0;
			NullCheck(L_11);
			VirtActionInvoker5< String_t*, ByteU5BU5D_t350*, int32_t, int32_t, bool >::Invoke(13 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::SendToSpecificRecipient(System.String,System.Byte[],System.Int32,System.Int32,System.Boolean) */, __this, L_9, L_10, 0, (((int32_t)(((Array_t *)L_11)->max_length))), 1);
		}

IL_003b:
		{
			bool L_12 = Enumerator_MoveNext_m3816((&V_2), /*hidden argument*/Enumerator_MoveNext_m3816_MethodInfo_var);
			if (L_12)
			{
				goto IL_0026;
			}
		}

IL_0047:
		{
			IL2CPP_LEAVE(0x58, FINALLY_004c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_004c;
	}

FINALLY_004c:
	{ // begin finally (depth: 1)
		Enumerator_t923  L_13 = V_2;
		Enumerator_t923  L_14 = L_13;
		Object_t * L_15 = Box(Enumerator_t923_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_15);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_15);
		IL2CPP_END_FINALLY(76)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(76)
	{
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0058:
	{
		goto IL_0074;
	}

IL_005d:
	{
		RoomSession_t566 * L_16 = (__this->___mSession_0);
		NullCheck(L_16);
		RealtimeManager_t564 * L_17 = RoomSession_Manager_m2331(L_16, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_18 = (__this->___mRoom_1);
		ByteU5BU5D_t350* L_19 = V_0;
		NullCheck(L_17);
		RealtimeManager_SendUnreliableMessageToAll_m2993(L_17, L_18, L_19, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnDataReceived(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C" void MessagingEnabledState_OnDataReceived_m2400 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___sender, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		OnGameThreadForwardingListener_t563 * L_1 = RoomSession_OnGameThreadListener_m2334(L_0, /*hidden argument*/NULL);
		bool L_2 = ___isReliable;
		MultiplayerParticipant_t672 * L_3 = ___sender;
		NullCheck(L_3);
		String_t* L_4 = MultiplayerParticipant_Id_m2717(L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_5 = ___data;
		NullCheck(L_1);
		OnGameThreadForwardingListener_RealTimeMessageReceived_m2370(L_1, L_2, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<UpdateCurrentRoom>m__37(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" String_t* MessagingEnabledState_U3CUpdateCurrentRoomU3Em__37_m2401 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method)
{
	{
		MultiplayerParticipant_t672 * L_0 = ___p;
		NullCheck(L_0);
		String_t* L_1 = MultiplayerParticipant_Id_m2717(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<UpdateCurrentRoom>m__38(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" Participant_t340 * MessagingEnabledState_U3CUpdateCurrentRoomU3Em__38_m2402 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method)
{
	{
		MultiplayerParticipant_t672 * L_0 = ___p;
		NullCheck(L_0);
		Participant_t340 * L_1 = MultiplayerParticipant_AsParticipant_m2720(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<UpdateCurrentRoom>m__39(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* MessagingEnabledState_U3CUpdateCurrentRoomU3Em__39_m2403 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method)
{
	{
		Participant_t340 * L_0 = ___p;
		NullCheck(L_0);
		String_t* L_1 = Participant_get_ParticipantId_m1387(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<GetConnectedParticipants>m__3A(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" bool MessagingEnabledState_U3CGetConnectedParticipantsU3Em__3A_m2404 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method)
{
	{
		Participant_t340 * L_0 = ___p;
		NullCheck(L_0);
		bool L_1 = Participant_get_IsConnectedToRoom_m1390(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_10.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_10MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_15.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_15MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern MethodInfo* Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var;
extern "C" void BeforeRoomCreateStartedState__ctor_m2405 (BeforeRoomCreateStartedState_t581 * __this, RoomSession_t566 * ___session, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		State__ctor_m2373(__this, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = ___session;
		RoomSession_t566 * L_1 = Misc_CheckNotNull_TisRoomSession_t566_m3798(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var);
		__this->___mContainingSession_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState::LeaveRoom()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ShutdownState_t587_il2cpp_TypeInfo_var;
extern "C" void BeforeRoomCreateStartedState_LeaveRoom_m2406 (BeforeRoomCreateStartedState_t581 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ShutdownState_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(757);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral508, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = (__this->___mContainingSession_0);
		NullCheck(L_0);
		OnGameThreadForwardingListener_t563 * L_1 = RoomSession_OnGameThreadListener_m2334(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_1, 0, /*hidden argument*/NULL);
		RoomSession_t566 * L_2 = (__this->___mContainingSession_0);
		RoomSession_t566 * L_3 = (__this->___mContainingSession_0);
		ShutdownState_t587 * L_4 = (ShutdownState_t587 *)il2cpp_codegen_object_new (ShutdownState_t587_il2cpp_TypeInfo_var);
		ShutdownState__ctor_m2437(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		RoomSession_EnterState_m2335(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_11.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_11MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_12.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_17.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeManMethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_12MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_17MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern MethodInfo* Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var;
extern "C" void RoomCreationPendingState__ctor_m2407 (RoomCreationPendingState_t582 * __this, RoomSession_t566 * ___session, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		State__ctor_m2373(__this, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = ___session;
		RoomSession_t566 * L_1 = Misc_CheckNotNull_TisRoomSession_t566_m3798(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var);
		__this->___mContainingSession_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern TypeInfo* ShutdownState_t587_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectingState_t584_il2cpp_TypeInfo_var;
extern "C" void RoomCreationPendingState_HandleRoomResponse_m2408 (RoomCreationPendingState_t582 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShutdownState_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(757);
		ConnectingState_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(758);
		s_Il2CppMethodIntialized = true;
	}
	{
		RealTimeRoomResponse_t695 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = RealTimeRoomResponse_RequestSucceeded_m2955(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		RoomSession_t566 * L_2 = (__this->___mContainingSession_0);
		RoomSession_t566 * L_3 = (__this->___mContainingSession_0);
		ShutdownState_t587 * L_4 = (ShutdownState_t587 *)il2cpp_codegen_object_new (ShutdownState_t587_il2cpp_TypeInfo_var);
		ShutdownState__ctor_m2437(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		RoomSession_EnterState_m2335(L_2, L_4, /*hidden argument*/NULL);
		RoomSession_t566 * L_5 = (__this->___mContainingSession_0);
		NullCheck(L_5);
		OnGameThreadForwardingListener_t563 * L_6 = RoomSession_OnGameThreadListener_m2334(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_6, 0, /*hidden argument*/NULL);
		return;
	}

IL_0033:
	{
		RoomSession_t566 * L_7 = (__this->___mContainingSession_0);
		RealTimeRoomResponse_t695 * L_8 = ___response;
		NullCheck(L_8);
		NativeRealTimeRoom_t574 * L_9 = RealTimeRoomResponse_Room_m2956(L_8, /*hidden argument*/NULL);
		RoomSession_t566 * L_10 = (__this->___mContainingSession_0);
		IL2CPP_RUNTIME_CLASS_INIT(ConnectingState_t584_il2cpp_TypeInfo_var);
		ConnectingState_t584 * L_11 = (ConnectingState_t584 *)il2cpp_codegen_object_new (ConnectingState_t584_il2cpp_TypeInfo_var);
		ConnectingState__ctor_m2411(L_11, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		RoomSession_EnterState_m2335(L_7, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::IsActive()
extern "C" bool RoomCreationPendingState_IsActive_m2409 (RoomCreationPendingState_t582 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::LeaveRoom()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* AbortingRoomCreationState_t590_il2cpp_TypeInfo_var;
extern "C" void RoomCreationPendingState_LeaveRoom_m2410 (RoomCreationPendingState_t582 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		AbortingRoomCreationState_t590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(759);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral509, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = (__this->___mContainingSession_0);
		RoomSession_t566 * L_1 = (__this->___mContainingSession_0);
		AbortingRoomCreationState_t590 * L_2 = (AbortingRoomCreationState_t590 *)il2cpp_codegen_object_new (AbortingRoomCreationState_t590_il2cpp_TypeInfo_var);
		AbortingRoomCreationState__ctor_m2444(L_2, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RoomSession_EnterState_m2335(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.HashSet`1<System.String>
#include "System_Core_System_Collections_Generic_HashSet_1_gen.h"
// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
#include "System_Core_System_Collections_Generic_HashSet_1_gen_0.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Real.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_14.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_16.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_1.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse>
#include "mscorlib_System_Action_1_gen_30.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// System.Collections.Generic.HashSet`1<System.String>
#include "System_Core_System_Collections_Generic_HashSet_1_genMethodDeclarations.h"
// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
#include "System_Core_System_Collections_Generic_HashSet_1_gen_0MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_14MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_16MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse>
#include "mscorlib_System_Action_1_gen_30MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_1MethodDeclarations.h"
struct Enumerable_t219;
struct IEnumerable_1_t170;
struct Enumerable_t219;
struct IEnumerable_1_t221;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Except<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Except<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" Object_t* Enumerable_Except_TisObject_t_m3818_gshared (Object_t * __this /* static, unused */, Object_t* p0, Object_t* p1, MethodInfo* method);
#define Enumerable_Except_TisObject_t_m3818(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Object_t*, MethodInfo*))Enumerable_Except_TisObject_t_m3818_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Except<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Except<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Except_TisString_t_m3817(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Object_t*, MethodInfo*))Enumerable_Except_TisObject_t_m3818_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct StringU5BU5D_t169;
struct IEnumerable_1_t170;
struct Enumerable_t219;
struct ObjectU5BU5D_t208;
struct IEnumerable_1_t221;
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" ObjectU5BU5D_t208* Enumerable_ToArray_TisObject_t_m3698_gshared (Object_t * __this /* static, unused */, Object_t* p0, MethodInfo* method);
#define Enumerable_ToArray_TisObject_t_m3698(__this /* static, unused */, p0, method) (( ObjectU5BU5D_t208* (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToArray_TisObject_t_m3698_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m3697(__this /* static, unused */, p0, method) (( StringU5BU5D_t169* (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToArray_TisObject_t_m3698_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t219;
struct IEnumerable_1_t170;
struct Enumerable_t219;
struct IEnumerable_1_t221;
// Declaration System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" int32_t Enumerable_Count_TisObject_t_m1002_gshared (Object_t * __this /* static, unused */, Object_t* p0, MethodInfo* method);
#define Enumerable_Count_TisObject_t_m1002(__this /* static, unused */, p0, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_Count_TisObject_t_m1002_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Int32 System.Linq.Enumerable::Count<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Int32 System.Linq.Enumerable::Count<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Count_TisString_t_m3819(__this /* static, unused */, p0, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_Count_TisObject_t_m1002_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::.ctor(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern TypeInfo* HashSet_1_t103_il2cpp_TypeInfo_var;
extern MethodInfo* HashSet_1__ctor_m983_MethodInfo_var;
extern "C" void ConnectingState__ctor_m2411 (ConnectingState_t584 * __this, NativeRealTimeRoom_t574 * ___room, RoomSession_t566 * ___session, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HashSet_1_t103_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(121);
		HashSet_1__ctor_m983_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483766);
		s_Il2CppMethodIntialized = true;
	}
	{
		HashSet_1_t103 * L_0 = (HashSet_1_t103 *)il2cpp_codegen_object_new (HashSet_1_t103_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m983(L_0, /*hidden argument*/HashSet_1__ctor_m983_MethodInfo_var);
		__this->___mConnectedParticipants_10 = L_0;
		__this->___mPercentComplete_11 = (20.0f);
		RoomSession_t566 * L_1 = ___session;
		NativeRealTimeRoom_t574 * L_2 = ___room;
		MessagingEnabledState__ctor_m2389(__this, L_1, L_2, /*hidden argument*/NULL);
		RoomSession_t566 * L_3 = ___session;
		NullCheck(L_3);
		uint32_t L_4 = RoomSession_get_MinPlayersToStart_m2329(L_3, /*hidden argument*/NULL);
		__this->___mPercentPerParticipant_12 = ((float)((float)(80.0f)/(float)(((float)(((double)L_4))))));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::.cctor()
extern TypeInfo* HashSet_1_t583_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectingState_t584_il2cpp_TypeInfo_var;
extern MethodInfo* HashSet_1__ctor_m3820_MethodInfo_var;
extern MethodInfo* HashSet_1_Add_m3821_MethodInfo_var;
extern "C" void ConnectingState__cctor_m2412 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HashSet_1_t583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(761);
		ConnectingState_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(758);
		HashSet_1__ctor_m3820_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		HashSet_1_Add_m3821_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		s_Il2CppMethodIntialized = true;
	}
	HashSet_1_t583 * V_0 = {0};
	{
		HashSet_1_t583 * L_0 = (HashSet_1_t583 *)il2cpp_codegen_object_new (HashSet_1_t583_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3820(L_0, /*hidden argument*/HashSet_1__ctor_m3820_MethodInfo_var);
		V_0 = L_0;
		HashSet_1_t583 * L_1 = V_0;
		NullCheck(L_1);
		HashSet_1_Add_m3821(L_1, 3, /*hidden argument*/HashSet_1_Add_m3821_MethodInfo_var);
		HashSet_1_t583 * L_2 = V_0;
		NullCheck(L_2);
		HashSet_1_Add_m3821(L_2, 4, /*hidden argument*/HashSet_1_Add_m3821_MethodInfo_var);
		HashSet_1_t583 * L_3 = V_0;
		((ConnectingState_t584_StaticFields*)ConnectingState_t584_il2cpp_TypeInfo_var->static_fields)->___FailedStatuses_9 = L_3;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::OnStateEntered()
extern "C" void ConnectingState_OnStateEntered_m2413 (ConnectingState_t584 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		OnGameThreadForwardingListener_t563 * L_1 = RoomSession_OnGameThreadListener_m2334(L_0, /*hidden argument*/NULL);
		float L_2 = (__this->___mPercentComplete_11);
		NullCheck(L_1);
		OnGameThreadForwardingListener_RoomSetupProgress_m2365(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::HandleConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern TypeInfo* HashSet_1_t103_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t874_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t925_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ShutdownState_t587_il2cpp_TypeInfo_var;
extern TypeInfo* ActiveState_t586_il2cpp_TypeInfo_var;
extern MethodInfo* HashSet_1__ctor_m983_MethodInfo_var;
extern MethodInfo* HashSet_1_Add_m989_MethodInfo_var;
extern MethodInfo* Enumerable_Except_TisString_t_m3817_MethodInfo_var;
extern MethodInfo* Enumerable_ToArray_TisString_t_m3697_MethodInfo_var;
extern MethodInfo* Enumerable_Count_TisString_t_m3819_MethodInfo_var;
extern "C" void ConnectingState_HandleConnectedSetChanged_m2414 (ConnectingState_t584 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HashSet_1_t103_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(121);
		IEnumerable_1_t874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(762);
		IEnumerator_1_t925_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(763);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ShutdownState_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(757);
		ActiveState_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(764);
		HashSet_1__ctor_m983_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483766);
		HashSet_1_Add_m989_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483773);
		Enumerable_Except_TisString_t_m3817_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		Enumerable_ToArray_TisString_t_m3697_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483824);
		Enumerable_Count_TisString_t_m3819_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		s_Il2CppMethodIntialized = true;
	}
	HashSet_1_t103 * V_0 = {0};
	MultiplayerParticipant_t672 * V_1 = {0};
	Object_t* V_2 = {0};
	MultiplayerParticipant_t672 * V_3 = {0};
	Object_t* V_4 = {0};
	Object_t* V_5 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HashSet_1_t103 * L_0 = (HashSet_1_t103 *)il2cpp_codegen_object_new (HashSet_1_t103_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m983(L_0, /*hidden argument*/HashSet_1__ctor_m983_MethodInfo_var);
		V_0 = L_0;
		NativeRealTimeRoom_t574 * L_1 = ___room;
		NullCheck(L_1);
		int32_t L_2 = NativeRealTimeRoom_Status_m2800(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_001e;
		}
	}
	{
		NativeRealTimeRoom_t574 * L_3 = ___room;
		NullCheck(L_3);
		int32_t L_4 = NativeRealTimeRoom_Status_m2800(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_006a;
		}
	}

IL_001e:
	{
		RoomSession_t566 * L_5 = (__this->___mSession_0);
		NullCheck(L_5);
		uint32_t L_6 = RoomSession_get_MinPlayersToStart_m2329(L_5, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_7 = ___room;
		NullCheck(L_7);
		uint32_t L_8 = NativeRealTimeRoom_ParticipantCount_m2799(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) <= ((uint32_t)L_8))))
		{
			goto IL_006a;
		}
	}
	{
		RoomSession_t566 * L_9 = (__this->___mSession_0);
		RoomSession_t566 * L_10 = (__this->___mSession_0);
		NullCheck(L_10);
		uint32_t L_11 = RoomSession_get_MinPlayersToStart_m2329(L_10, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_12 = ___room;
		NullCheck(L_12);
		uint32_t L_13 = NativeRealTimeRoom_ParticipantCount_m2799(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		RoomSession_set_MinPlayersToStart_m2330(L_9, ((int32_t)((int32_t)L_11+(int32_t)L_13)), /*hidden argument*/NULL);
		RoomSession_t566 * L_14 = (__this->___mSession_0);
		NullCheck(L_14);
		uint32_t L_15 = RoomSession_get_MinPlayersToStart_m2329(L_14, /*hidden argument*/NULL);
		__this->___mPercentPerParticipant_12 = ((float)((float)(80.0f)/(float)(((float)(((double)L_15))))));
	}

IL_006a:
	{
		NativeRealTimeRoom_t574 * L_16 = ___room;
		NullCheck(L_16);
		Object_t* L_17 = NativeRealTimeRoom_Participants_m2798(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Object_t* L_18 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GetEnumerator() */, IEnumerable_1_t874_il2cpp_TypeInfo_var, L_17);
		V_2 = L_18;
	}

IL_0076:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ae;
		}

IL_007b:
		{
			Object_t* L_19 = V_2;
			NullCheck(L_19);
			MultiplayerParticipant_t672 * L_20 = (MultiplayerParticipant_t672 *)InterfaceFuncInvoker0< MultiplayerParticipant_t672 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Current() */, IEnumerator_1_t925_il2cpp_TypeInfo_var, L_19);
			V_1 = L_20;
			MultiplayerParticipant_t672 * L_21 = V_1;
			V_3 = L_21;
		}

IL_0084:
		try
		{ // begin try (depth: 2)
			{
				MultiplayerParticipant_t672 * L_22 = V_1;
				NullCheck(L_22);
				bool L_23 = MultiplayerParticipant_IsConnectedToRoom_m2714(L_22, /*hidden argument*/NULL);
				if (!L_23)
				{
					goto IL_009c;
				}
			}

IL_008f:
			{
				HashSet_1_t103 * L_24 = V_0;
				MultiplayerParticipant_t672 * L_25 = V_1;
				NullCheck(L_25);
				String_t* L_26 = MultiplayerParticipant_Id_m2717(L_25, /*hidden argument*/NULL);
				NullCheck(L_24);
				HashSet_1_Add_m989(L_24, L_26, /*hidden argument*/HashSet_1_Add_m989_MethodInfo_var);
			}

IL_009c:
			{
				IL2CPP_LEAVE(0xAE, FINALLY_00a1);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_00a1;
		}

FINALLY_00a1:
		{ // begin finally (depth: 2)
			{
				MultiplayerParticipant_t672 * L_27 = V_3;
				if (!L_27)
				{
					goto IL_00ad;
				}
			}

IL_00a7:
			{
				MultiplayerParticipant_t672 * L_28 = V_3;
				NullCheck(L_28);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_28);
			}

IL_00ad:
			{
				IL2CPP_END_FINALLY(161)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(161)
		{
			IL2CPP_JUMP_TBL(0xAE, IL_00ae)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_00ae:
		{
			Object_t* L_29 = V_2;
			NullCheck(L_29);
			bool L_30 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_29);
			if (L_30)
			{
				goto IL_007b;
			}
		}

IL_00b9:
		{
			IL2CPP_LEAVE(0xC9, FINALLY_00be);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_00be;
	}

FINALLY_00be:
	{ // begin finally (depth: 1)
		{
			Object_t* L_31 = V_2;
			if (L_31)
			{
				goto IL_00c2;
			}
		}

IL_00c1:
		{
			IL2CPP_END_FINALLY(190)
		}

IL_00c2:
		{
			Object_t* L_32 = V_2;
			NullCheck(L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_32);
			IL2CPP_END_FINALLY(190)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(190)
	{
		IL2CPP_JUMP_TBL(0xC9, IL_00c9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00c9:
	{
		HashSet_1_t103 * L_33 = (__this->___mConnectedParticipants_10);
		HashSet_1_t103 * L_34 = V_0;
		NullCheck(L_33);
		bool L_35 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_33, L_34);
		if (!L_35)
		{
			goto IL_00e5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral510, /*hidden argument*/NULL);
		return;
	}

IL_00e5:
	{
		HashSet_1_t103 * L_36 = (__this->___mConnectedParticipants_10);
		HashSet_1_t103 * L_37 = V_0;
		Object_t* L_38 = Enumerable_Except_TisString_t_m3817(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/Enumerable_Except_TisString_t_m3817_MethodInfo_var);
		V_4 = L_38;
		NativeRealTimeRoom_t574 * L_39 = ___room;
		NullCheck(L_39);
		int32_t L_40 = NativeRealTimeRoom_Status_m2800(L_39, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_40) == ((uint32_t)5))))
		{
			goto IL_0147;
		}
	}
	{
		Object_t* L_41 = V_4;
		StringU5BU5D_t169* L_42 = Enumerable_ToArray_TisString_t_m3697(NULL /*static, unused*/, L_41, /*hidden argument*/Enumerable_ToArray_TisString_t_m3697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Join_m3704(NULL /*static, unused*/, (String_t*) &_stringLiteral41, L_42, /*hidden argument*/NULL);
		String_t* L_44 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral511, L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		RoomSession_t566 * L_45 = (__this->___mSession_0);
		NullCheck(L_45);
		OnGameThreadForwardingListener_t563 * L_46 = RoomSession_OnGameThreadListener_m2334(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_46, 0, /*hidden argument*/NULL);
		RoomSession_t566 * L_47 = (__this->___mSession_0);
		RoomSession_t566 * L_48 = (__this->___mSession_0);
		ShutdownState_t587 * L_49 = (ShutdownState_t587 *)il2cpp_codegen_object_new (ShutdownState_t587_il2cpp_TypeInfo_var);
		ShutdownState__ctor_m2437(L_49, L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		RoomSession_EnterState_m2335(L_47, L_49, /*hidden argument*/NULL);
		return;
	}

IL_0147:
	{
		HashSet_1_t103 * L_50 = V_0;
		HashSet_1_t103 * L_51 = (__this->___mConnectedParticipants_10);
		Object_t* L_52 = Enumerable_Except_TisString_t_m3817(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/Enumerable_Except_TisString_t_m3817_MethodInfo_var);
		V_5 = L_52;
		Object_t* L_53 = V_5;
		StringU5BU5D_t169* L_54 = Enumerable_ToArray_TisString_t_m3697(NULL /*static, unused*/, L_53, /*hidden argument*/Enumerable_ToArray_TisString_t_m3697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = String_Join_m3704(NULL /*static, unused*/, (String_t*) &_stringLiteral41, L_54, /*hidden argument*/NULL);
		String_t* L_56 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral512, L_55, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_57 = ___room;
		NullCheck(L_57);
		int32_t L_58 = NativeRealTimeRoom_Status_m2800(L_57, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_58) == ((uint32_t)4))))
		{
			goto IL_01b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral513, /*hidden argument*/NULL);
		RoomSession_t566 * L_59 = (__this->___mSession_0);
		NativeRealTimeRoom_t574 * L_60 = ___room;
		RoomSession_t566 * L_61 = (__this->___mSession_0);
		ActiveState_t586 * L_62 = (ActiveState_t586 *)il2cpp_codegen_object_new (ActiveState_t586_il2cpp_TypeInfo_var);
		ActiveState__ctor_m2425(L_62, L_60, L_61, /*hidden argument*/NULL);
		NullCheck(L_59);
		RoomSession_EnterState_m2335(L_59, L_62, /*hidden argument*/NULL);
		RoomSession_t566 * L_63 = (__this->___mSession_0);
		NullCheck(L_63);
		OnGameThreadForwardingListener_t563 * L_64 = RoomSession_OnGameThreadListener_m2334(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_64, 1, /*hidden argument*/NULL);
		return;
	}

IL_01b4:
	{
		float L_65 = (__this->___mPercentComplete_11);
		float L_66 = (__this->___mPercentPerParticipant_12);
		Object_t* L_67 = V_5;
		int32_t L_68 = Enumerable_Count_TisString_t_m3819(NULL /*static, unused*/, L_67, /*hidden argument*/Enumerable_Count_TisString_t_m3819_MethodInfo_var);
		__this->___mPercentComplete_11 = ((float)((float)L_65+(float)((float)((float)L_66*(float)(((float)L_68))))));
		HashSet_1_t103 * L_69 = V_0;
		__this->___mConnectedParticipants_10 = L_69;
		RoomSession_t566 * L_70 = (__this->___mSession_0);
		NullCheck(L_70);
		OnGameThreadForwardingListener_t563 * L_71 = RoomSession_OnGameThreadListener_m2334(L_70, /*hidden argument*/NULL);
		float L_72 = (__this->___mPercentComplete_11);
		NullCheck(L_71);
		OnGameThreadForwardingListener_RoomSetupProgress_m2365(L_71, L_72, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::HandleParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern TypeInfo* ConnectingState_t584_il2cpp_TypeInfo_var;
extern "C" void ConnectingState_HandleParticipantStatusChanged_m2415 (ConnectingState_t584 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConnectingState_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(758);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConnectingState_t584_il2cpp_TypeInfo_var);
		HashSet_1_t583 * L_0 = ((ConnectingState_t584_StaticFields*)ConnectingState_t584_il2cpp_TypeInfo_var->static_fields)->___FailedStatuses_9;
		MultiplayerParticipant_t672 * L_1 = ___participant;
		NullCheck(L_1);
		int32_t L_2 = MultiplayerParticipant_Status_m2713(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(8 /* System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Contains(!0) */, L_0, L_2);
		if (L_3)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		RoomSession_t566 * L_4 = (__this->___mSession_0);
		NullCheck(L_4);
		OnGameThreadForwardingListener_t563 * L_5 = RoomSession_OnGameThreadListener_m2334(L_4, /*hidden argument*/NULL);
		MultiplayerParticipant_t672 * L_6 = ___participant;
		NullCheck(L_6);
		Participant_t340 * L_7 = MultiplayerParticipant_AsParticipant_m2720(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		OnGameThreadForwardingListener_ParticipantLeft_m2371(L_5, L_7, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_8 = ___room;
		NullCheck(L_8);
		int32_t L_9 = NativeRealTimeRoom_Status_m2800(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)2)))
		{
			goto IL_004a;
		}
	}
	{
		NativeRealTimeRoom_t574 * L_10 = ___room;
		NullCheck(L_10);
		int32_t L_11 = NativeRealTimeRoom_Status_m2800(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)3)))
		{
			goto IL_004a;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::LeaveRoom() */, __this);
	}

IL_004a:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::LeaveRoom()
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* LeavingRoom_t589_il2cpp_TypeInfo_var;
extern MethodInfo* ConnectingState_U3CLeaveRoomU3Em__3B_m2418_MethodInfo_var;
extern "C" void ConnectingState_LeaveRoom_m2416 (ConnectingState_t584 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		LeavingRoom_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(765);
		ConnectingState_U3CLeaveRoomU3Em__3B_m2418_MethodInfo_var = il2cpp_codegen_method_info_from_index(295);
		s_Il2CppMethodIntialized = true;
	}
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		RoomSession_t566 * L_1 = (__this->___mSession_0);
		NativeRealTimeRoom_t574 * L_2 = (__this->___mRoom_1);
		IntPtr_t L_3 = { ConnectingState_U3CLeaveRoomU3Em__3B_m2418_MethodInfo_var };
		Action_t588 * L_4 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_4, __this, L_3, /*hidden argument*/NULL);
		LeavingRoom_t589 * L_5 = (LeavingRoom_t589 *)il2cpp_codegen_object_new (LeavingRoom_t589_il2cpp_TypeInfo_var);
		LeavingRoom__ctor_m2440(L_5, L_1, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		RoomSession_EnterState_m2335(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::ShowWaitingRoomUI(System.UInt32)
extern TypeInfo* Action_1_t887_il2cpp_TypeInfo_var;
extern MethodInfo* ConnectingState_U3CShowWaitingRoomUIU3Em__3C_m2419_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3822_MethodInfo_var;
extern "C" void ConnectingState_ShowWaitingRoomUI_m2417 (ConnectingState_t584 * __this, uint32_t ___minimumParticipantsBeforeStarting, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t887_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(767);
		ConnectingState_U3CShowWaitingRoomUIU3Em__3C_m2419_MethodInfo_var = il2cpp_codegen_method_info_from_index(296);
		Action_1__ctor_m3822_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		s_Il2CppMethodIntialized = true;
	}
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		RealtimeManager_t564 * L_1 = RoomSession_Manager_m2331(L_0, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_2 = (__this->___mRoom_1);
		uint32_t L_3 = ___minimumParticipantsBeforeStarting;
		IntPtr_t L_4 = { ConnectingState_U3CShowWaitingRoomUIU3Em__3C_m2419_MethodInfo_var };
		Action_1_t887 * L_5 = (Action_1_t887 *)il2cpp_codegen_object_new (Action_1_t887_il2cpp_TypeInfo_var);
		Action_1__ctor_m3822(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m3822_MethodInfo_var);
		NullCheck(L_1);
		RealtimeManager_ShowWaitingRoomUI_m2983(L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::<LeaveRoom>m__3B()
extern "C" void ConnectingState_U3CLeaveRoomU3Em__3B_m2418 (ConnectingState_t584 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		OnGameThreadForwardingListener_t563 * L_1 = RoomSession_OnGameThreadListener_m2334(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::<ShowWaitingRoomUI>m__3C(GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t235_il2cpp_TypeInfo_var;
extern TypeInfo* RealTimeRoomStatus_t520_il2cpp_TypeInfo_var;
extern TypeInfo* ActiveState_t586_il2cpp_TypeInfo_var;
extern "C" void ConnectingState_U3CShowWaitingRoomUIU3Em__3C_m2419 (ConnectingState_t584 * __this, WaitingRoomUIResponse_t697 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		UInt32_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(170);
		RealTimeRoomStatus_t520_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(768);
		ActiveState_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(764);
		s_Il2CppMethodIntialized = true;
	}
	{
		WaitingRoomUIResponse_t697 * L_0 = ___response;
		NullCheck(L_0);
		int32_t L_1 = WaitingRoomUIResponse_ResponseStatus_m2965(L_0, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(UIStatus_t412_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral514, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		WaitingRoomUIResponse_t697 * L_5 = ___response;
		NullCheck(L_5);
		int32_t L_6 = WaitingRoomUIResponse_ResponseStatus_m2965(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_00af;
		}
	}
	{
		ObjectU5BU5D_t208* L_7 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, (String_t*) &_stringLiteral515);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 0)) = (Object_t *)(String_t*) &_stringLiteral515;
		ObjectU5BU5D_t208* L_8 = L_7;
		WaitingRoomUIResponse_t697 * L_9 = ___response;
		NullCheck(L_9);
		NativeRealTimeRoom_t574 * L_10 = WaitingRoomUIResponse_Room_m2966(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		uint32_t L_11 = NativeRealTimeRoom_ParticipantCount_m2799(L_10, /*hidden argument*/NULL);
		uint32_t L_12 = L_11;
		Object_t * L_13 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 1)) = (Object_t *)L_13;
		ObjectU5BU5D_t208* L_14 = L_8;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		ArrayElementTypeCheck (L_14, (String_t*) &_stringLiteral461);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 2)) = (Object_t *)(String_t*) &_stringLiteral461;
		ObjectU5BU5D_t208* L_15 = L_14;
		WaitingRoomUIResponse_t697 * L_16 = ___response;
		NullCheck(L_16);
		NativeRealTimeRoom_t574 * L_17 = WaitingRoomUIResponse_Room_m2966(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = NativeRealTimeRoom_Status_m2800(L_17, /*hidden argument*/NULL);
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(RealTimeRoomStatus_t520_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
		ArrayElementTypeCheck (L_15, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 3)) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m976(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		WaitingRoomUIResponse_t697 * L_22 = ___response;
		NullCheck(L_22);
		NativeRealTimeRoom_t574 * L_23 = WaitingRoomUIResponse_Room_m2966(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = NativeRealTimeRoom_Status_m2800(L_23, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)4))))
		{
			goto IL_00aa;
		}
	}
	{
		RoomSession_t566 * L_25 = (__this->___mSession_0);
		WaitingRoomUIResponse_t697 * L_26 = ___response;
		NullCheck(L_26);
		NativeRealTimeRoom_t574 * L_27 = WaitingRoomUIResponse_Room_m2966(L_26, /*hidden argument*/NULL);
		RoomSession_t566 * L_28 = (__this->___mSession_0);
		ActiveState_t586 * L_29 = (ActiveState_t586 *)il2cpp_codegen_object_new (ActiveState_t586_il2cpp_TypeInfo_var);
		ActiveState__ctor_m2425(L_29, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		RoomSession_EnterState_m2335(L_25, L_29, /*hidden argument*/NULL);
		RoomSession_t566 * L_30 = (__this->___mSession_0);
		NullCheck(L_30);
		OnGameThreadForwardingListener_t563 * L_31 = RoomSession_OnGameThreadListener_m2334(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_31, 1, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		goto IL_00dd;
	}

IL_00af:
	{
		WaitingRoomUIResponse_t697 * L_32 = ___response;
		NullCheck(L_32);
		int32_t L_33 = WaitingRoomUIResponse_ResponseStatus_m2965(L_32, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)-18)))))
		{
			goto IL_00c7;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::LeaveRoom() */, __this);
		goto IL_00dd;
	}

IL_00c7:
	{
		RoomSession_t566 * L_34 = (__this->___mSession_0);
		NullCheck(L_34);
		OnGameThreadForwardingListener_t563 * L_35 = RoomSession_OnGameThreadListener_m2334(L_34, /*hidden argument*/NULL);
		float L_36 = (__this->___mPercentComplete_11);
		NullCheck(L_35);
		OnGameThreadForwardingListener_RoomSetupProgress_m2365(L_35, L_36, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_13.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_13MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::.ctor()
extern "C" void U3CHandleConnectedSetChangedU3Ec__AnonStorey41__ctor_m2420 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__41(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__41_m2421 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peerId, MethodInfo* method)
{
	{
		String_t* L_0 = ___peerId;
		String_t* L_1 = (__this->___selfId_0);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_0, L_1);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__42(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__42_m2422 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peerId, MethodInfo* method)
{
	{
		String_t* L_0 = ___peerId;
		String_t* L_1 = (__this->___selfId_0);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_0, L_1);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__43(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__43_m2423 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peer, MethodInfo* method)
{
	{
		String_t* L_0 = ___peer;
		String_t* L_1 = (__this->___selfId_0);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_0, L_1);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__44(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__44_m2424 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peer, MethodInfo* method)
{
	{
		String_t* L_0 = ___peer;
		String_t* L_1 = (__this->___selfId_0);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_0, L_1);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3.h"
// GooglePlayGames.BasicApi.Multiplayer.Player
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Playe.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_3.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"
// System.Func`2<System.String,System.Boolean>
#include "System_Core_System_Func_2_gen_4.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3MethodDeclarations.h"
// GooglePlayGames.BasicApi.Multiplayer.Player
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_PlayeMethodDeclarations.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_3MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4MethodDeclarations.h"
// System.Func`2<System.String,System.Boolean>
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"
struct Enumerable_t219;
struct IEnumerable_1_t170;
struct IEnumerable_1_t902;
struct Func_2_t352;
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisParticipant_t340_TisString_t_m3695(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t352 *, MethodInfo*))Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct IEnumerable_1_t170;
struct Func_2_t926;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisString_t_m3823(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t926 *, MethodInfo*))Enumerable_Where_TisObject_t_m3805_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct List_1_t43;
struct IEnumerable_1_t170;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisString_t_m3824(__this /* static, unused */, p0, method) (( List_1_t43 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::.ctor(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" void ActiveState__ctor_m2425 (ActiveState_t586 * __this, NativeRealTimeRoom_t574 * ___room, RoomSession_t566 * ___session, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = ___session;
		NativeRealTimeRoom_t574 * L_1 = ___room;
		MessagingEnabledState__ctor_m2389(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::OnStateEntered()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void ActiveState_OnStateEntered_m2426 (ActiveState_t586 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Participant_t340 * L_0 = (Participant_t340 *)VirtFuncInvoker0< Participant_t340 * >::Invoke(16 /* GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::GetSelf() */, __this);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral516, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(6 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::LeaveRoom() */, __this);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::IsRoomConnected()
extern "C" bool ActiveState_IsRoomConnected_m2427 (ActiveState_t586 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::GetParticipant(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" Participant_t340 * ActiveState_GetParticipant_m2428 (ActiveState_t586 * __this, String_t* ___participantId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t576 * L_0 = (__this->___mParticipants_3);
		String_t* L_1 = ___participantId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_3 = ___participantId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral517, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (Participant_t340 *)NULL;
	}

IL_0023:
	{
		Dictionary_2_t576 * L_5 = (__this->___mParticipants_3);
		String_t* L_6 = ___participantId;
		NullCheck(L_5);
		Participant_t340 * L_7 = (Participant_t340 *)VirtFuncInvoker1< Participant_t340 *, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Item(!0) */, L_5, L_6);
		return L_7;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::GetSelf()
extern TypeInfo* Enumerator_t927_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2_get_Values_m3810_MethodInfo_var;
extern MethodInfo* ValueCollection_GetEnumerator_m3825_MethodInfo_var;
extern MethodInfo* Enumerator_get_Current_m3826_MethodInfo_var;
extern MethodInfo* Enumerator_MoveNext_m3827_MethodInfo_var;
extern "C" Participant_t340 * ActiveState_GetSelf_m2429 (ActiveState_t586 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(769);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Dictionary_2_get_Values_m3810_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483929);
		ValueCollection_GetEnumerator_m3825_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483946);
		Enumerator_get_Current_m3826_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483947);
		Enumerator_MoveNext_m3827_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483948);
		s_Il2CppMethodIntialized = true;
	}
	Participant_t340 * V_0 = {0};
	Enumerator_t927  V_1 = {0};
	Participant_t340 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t576 * L_0 = (__this->___mParticipants_3);
		NullCheck(L_0);
		ValueCollection_t922 * L_1 = Dictionary_2_get_Values_m3810(L_0, /*hidden argument*/Dictionary_2_get_Values_m3810_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t927  L_2 = ValueCollection_GetEnumerator_m3825(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m3825_MethodInfo_var);
		V_1 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0016:
		{
			Participant_t340 * L_3 = Enumerator_get_Current_m3826((&V_1), /*hidden argument*/Enumerator_get_Current_m3826_MethodInfo_var);
			V_0 = L_3;
			Participant_t340 * L_4 = V_0;
			NullCheck(L_4);
			Player_t347 * L_5 = Participant_get_Player_m1389(L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0050;
			}
		}

IL_0029:
		{
			Participant_t340 * L_6 = V_0;
			NullCheck(L_6);
			Player_t347 * L_7 = Participant_get_Player_m1389(L_6, /*hidden argument*/NULL);
			NullCheck(L_7);
			String_t* L_8 = Player_get_PlayerId_m1398(L_7, /*hidden argument*/NULL);
			RoomSession_t566 * L_9 = (__this->___mSession_0);
			NullCheck(L_9);
			String_t* L_10 = RoomSession_SelfPlayerId_m2333(L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			bool L_11 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_8, L_10);
			if (!L_11)
			{
				goto IL_0050;
			}
		}

IL_0049:
		{
			Participant_t340 * L_12 = V_0;
			V_2 = L_12;
			IL2CPP_LEAVE(0x6F, FINALLY_0061);
		}

IL_0050:
		{
			bool L_13 = Enumerator_MoveNext_m3827((&V_1), /*hidden argument*/Enumerator_MoveNext_m3827_MethodInfo_var);
			if (L_13)
			{
				goto IL_0016;
			}
		}

IL_005c:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		Enumerator_t927  L_14 = V_1;
		Enumerator_t927  L_15 = L_14;
		Object_t * L_16 = Box(Enumerator_t927_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_16);
		IL2CPP_END_FINALLY(97)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_006d:
	{
		return (Participant_t340 *)NULL;
	}

IL_006f:
	{
		Participant_t340 * L_17 = V_2;
		return L_17;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::HandleConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern TypeInfo* U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t43_il2cpp_TypeInfo_var;
extern TypeInfo* ActiveState_t586_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t577_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t923_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t928_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t578_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t352_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t926_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m883_MethodInfo_var;
extern MethodInfo* ActiveState_U3CHandleConnectedSetChangedU3Em__3D_m2432_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3807_MethodInfo_var;
extern MethodInfo* Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800_MethodInfo_var;
extern MethodInfo* Dictionary_2_get_Keys_m3813_MethodInfo_var;
extern MethodInfo* KeyCollection_GetEnumerator_m3814_MethodInfo_var;
extern MethodInfo* Enumerator_get_Current_m3815_MethodInfo_var;
extern MethodInfo* Enumerator_MoveNext_m3816_MethodInfo_var;
extern MethodInfo* Dictionary_2_get_Values_m3808_MethodInfo_var;
extern MethodInfo* ValueCollection_GetEnumerator_m3828_MethodInfo_var;
extern MethodInfo* Enumerator_get_Current_m3829_MethodInfo_var;
extern MethodInfo* Enumerator_MoveNext_m3830_MethodInfo_var;
extern MethodInfo* ActiveState_U3CHandleConnectedSetChangedU3Em__3E_m2433_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3809_MethodInfo_var;
extern MethodInfo* Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802_MethodInfo_var;
extern MethodInfo* ActiveState_U3CHandleConnectedSetChangedU3Em__3F_m2434_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3703_MethodInfo_var;
extern MethodInfo* Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803_MethodInfo_var;
extern MethodInfo* Dictionary_2_get_Values_m3810_MethodInfo_var;
extern MethodInfo* ActiveState_U3CHandleConnectedSetChangedU3Em__40_m2435_MethodInfo_var;
extern MethodInfo* Enumerable_Select_TisParticipant_t340_TisString_t_m3695_MethodInfo_var;
extern MethodInfo* Enumerable_ToArray_TisString_t_m3697_MethodInfo_var;
extern MethodInfo* U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__41_m2421_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3831_MethodInfo_var;
extern MethodInfo* Enumerable_Where_TisString_t_m3823_MethodInfo_var;
extern MethodInfo* Enumerable_ToList_TisString_t_m3824_MethodInfo_var;
extern MethodInfo* U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__42_m2422_MethodInfo_var;
extern MethodInfo* List_1_Sort_m3832_MethodInfo_var;
extern MethodInfo* U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__43_m2423_MethodInfo_var;
extern MethodInfo* U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__44_m2424_MethodInfo_var;
extern "C" void ActiveState_HandleConnectedSetChanged_m2430 (ActiveState_t586 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		List_1_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		ActiveState_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(764);
		Func_2_t577_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(752);
		Enumerator_t923_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(756);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Enumerator_t928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(771);
		Func_2_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(753);
		Func_2_t352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Func_2_t926_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(772);
		List_1__ctor_m883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		ActiveState_U3CHandleConnectedSetChangedU3Em__3D_m2432_MethodInfo_var = il2cpp_codegen_method_info_from_index(301);
		Func_2__ctor_m3807_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483921);
		Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483922);
		Dictionary_2_get_Keys_m3813_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		KeyCollection_GetEnumerator_m3814_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		Enumerator_get_Current_m3815_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		Enumerator_MoveNext_m3816_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		Dictionary_2_get_Values_m3808_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483923);
		ValueCollection_GetEnumerator_m3828_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483950);
		Enumerator_get_Current_m3829_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483951);
		Enumerator_MoveNext_m3830_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483952);
		ActiveState_U3CHandleConnectedSetChangedU3Em__3E_m2433_MethodInfo_var = il2cpp_codegen_method_info_from_index(305);
		Func_2__ctor_m3809_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483925);
		Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483926);
		ActiveState_U3CHandleConnectedSetChangedU3Em__3F_m2434_MethodInfo_var = il2cpp_codegen_method_info_from_index(306);
		Func_2__ctor_m3703_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483822);
		Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483928);
		Dictionary_2_get_Values_m3810_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483929);
		ActiveState_U3CHandleConnectedSetChangedU3Em__40_m2435_MethodInfo_var = il2cpp_codegen_method_info_from_index(307);
		Enumerable_Select_TisParticipant_t340_TisString_t_m3695_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483823);
		Enumerable_ToArray_TisString_t_m3697_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483824);
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__41_m2421_MethodInfo_var = il2cpp_codegen_method_info_from_index(308);
		Func_2__ctor_m3831_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483957);
		Enumerable_Where_TisString_t_m3823_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483958);
		Enumerable_ToList_TisString_t_m3824_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483959);
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__42_m2422_MethodInfo_var = il2cpp_codegen_method_info_from_index(312);
		List_1_Sort_m3832_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483961);
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__43_m2423_MethodInfo_var = il2cpp_codegen_method_info_from_index(314);
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__44_m2424_MethodInfo_var = il2cpp_codegen_method_info_from_index(315);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t43 * V_0 = {0};
	List_1_t43 * V_1 = {0};
	Dictionary_2_t575 * V_2 = {0};
	String_t* V_3 = {0};
	Enumerator_t923  V_4 = {0};
	MultiplayerParticipant_t672 * V_5 = {0};
	MultiplayerParticipant_t672 * V_6 = {0};
	MultiplayerParticipant_t672 * V_7 = {0};
	Enumerator_t928  V_8 = {0};
	U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * V_9 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Object_t* G_B2_0 = {0};
	Object_t* G_B1_0 = {0};
	ValueCollection_t921 * G_B20_0 = {0};
	ActiveState_t586 * G_B20_1 = {0};
	ValueCollection_t921 * G_B19_0 = {0};
	ActiveState_t586 * G_B19_1 = {0};
	Object_t* G_B22_0 = {0};
	ActiveState_t586 * G_B22_1 = {0};
	Object_t* G_B21_0 = {0};
	ActiveState_t586 * G_B21_1 = {0};
	ValueCollection_t922 * G_B24_0 = {0};
	String_t* G_B24_1 = {0};
	String_t* G_B24_2 = {0};
	ValueCollection_t922 * G_B23_0 = {0};
	String_t* G_B23_1 = {0};
	String_t* G_B23_2 = {0};
	{
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * L_0 = (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 *)il2cpp_codegen_object_new (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585_il2cpp_TypeInfo_var);
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41__ctor_m2420(L_0, /*hidden argument*/NULL);
		V_9 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t43_il2cpp_TypeInfo_var);
		List_1_t43 * L_1 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m883(L_1, /*hidden argument*/List_1__ctor_m883_MethodInfo_var);
		V_0 = L_1;
		List_1_t43 * L_2 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m883(L_2, /*hidden argument*/List_1__ctor_m883_MethodInfo_var);
		V_1 = L_2;
		NativeRealTimeRoom_t574 * L_3 = ___room;
		NullCheck(L_3);
		Object_t* L_4 = NativeRealTimeRoom_Participants_m2798(L_3, /*hidden argument*/NULL);
		Func_2_t577 * L_5 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_8;
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_0031;
		}
	}
	{
		IntPtr_t L_6 = { ActiveState_U3CHandleConnectedSetChangedU3Em__3D_m2432_MethodInfo_var };
		Func_2_t577 * L_7 = (Func_2_t577 *)il2cpp_codegen_object_new (Func_2_t577_il2cpp_TypeInfo_var);
		Func_2__ctor_m3807(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m3807_MethodInfo_var);
		((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_8 = L_7;
		G_B2_0 = G_B1_0;
	}

IL_0031:
	{
		Func_2_t577 * L_8 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_8;
		Dictionary_2_t575 * L_9 = Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800(NULL /*static, unused*/, G_B2_0, L_8, /*hidden argument*/Enumerable_ToDictionary_TisMultiplayerParticipant_t672_TisString_t_m3800_MethodInfo_var);
		V_2 = L_9;
		Dictionary_2_t575 * L_10 = (__this->___mNativeParticipants_2);
		NullCheck(L_10);
		KeyCollection_t924 * L_11 = Dictionary_2_get_Keys_m3813(L_10, /*hidden argument*/Dictionary_2_get_Keys_m3813_MethodInfo_var);
		NullCheck(L_11);
		Enumerator_t923  L_12 = KeyCollection_GetEnumerator_m3814(L_11, /*hidden argument*/KeyCollection_GetEnumerator_m3814_MethodInfo_var);
		V_4 = L_12;
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a4;
		}

IL_0053:
		{
			String_t* L_13 = Enumerator_get_Current_m3815((&V_4), /*hidden argument*/Enumerator_get_Current_m3815_MethodInfo_var);
			V_3 = L_13;
			Dictionary_2_t575 * L_14 = V_2;
			String_t* L_15 = V_3;
			NullCheck(L_14);
			MultiplayerParticipant_t672 * L_16 = (MultiplayerParticipant_t672 *)VirtFuncInvoker1< MultiplayerParticipant_t672 *, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Item(!0) */, L_14, L_15);
			V_5 = L_16;
			Dictionary_2_t575 * L_17 = (__this->___mNativeParticipants_2);
			String_t* L_18 = V_3;
			NullCheck(L_17);
			MultiplayerParticipant_t672 * L_19 = (MultiplayerParticipant_t672 *)VirtFuncInvoker1< MultiplayerParticipant_t672 *, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Item(!0) */, L_17, L_18);
			V_6 = L_19;
			MultiplayerParticipant_t672 * L_20 = V_5;
			NullCheck(L_20);
			bool L_21 = MultiplayerParticipant_IsConnectedToRoom_m2714(L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_0085;
			}
		}

IL_007e:
		{
			List_1_t43 * L_22 = V_1;
			String_t* L_23 = V_3;
			NullCheck(L_22);
			VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_22, L_23);
		}

IL_0085:
		{
			MultiplayerParticipant_t672 * L_24 = V_6;
			NullCheck(L_24);
			bool L_25 = MultiplayerParticipant_IsConnectedToRoom_m2714(L_24, /*hidden argument*/NULL);
			if (L_25)
			{
				goto IL_00a4;
			}
		}

IL_0091:
		{
			MultiplayerParticipant_t672 * L_26 = V_5;
			NullCheck(L_26);
			bool L_27 = MultiplayerParticipant_IsConnectedToRoom_m2714(L_26, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_00a4;
			}
		}

IL_009d:
		{
			List_1_t43 * L_28 = V_0;
			String_t* L_29 = V_3;
			NullCheck(L_28);
			VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_28, L_29);
		}

IL_00a4:
		{
			bool L_30 = Enumerator_MoveNext_m3816((&V_4), /*hidden argument*/Enumerator_MoveNext_m3816_MethodInfo_var);
			if (L_30)
			{
				goto IL_0053;
			}
		}

IL_00b0:
		{
			IL2CPP_LEAVE(0xC2, FINALLY_00b5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_00b5;
	}

FINALLY_00b5:
	{ // begin finally (depth: 1)
		Enumerator_t923  L_31 = V_4;
		Enumerator_t923  L_32 = L_31;
		Object_t * L_33 = Box(Enumerator_t923_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_33);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_33);
		IL2CPP_END_FINALLY(181)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(181)
	{
		IL2CPP_JUMP_TBL(0xC2, IL_00c2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00c2:
	{
		Dictionary_2_t575 * L_34 = (__this->___mNativeParticipants_2);
		NullCheck(L_34);
		ValueCollection_t921 * L_35 = Dictionary_2_get_Values_m3808(L_34, /*hidden argument*/Dictionary_2_get_Values_m3808_MethodInfo_var);
		NullCheck(L_35);
		Enumerator_t928  L_36 = ValueCollection_GetEnumerator_m3828(L_35, /*hidden argument*/ValueCollection_GetEnumerator_m3828_MethodInfo_var);
		V_8 = L_36;
	}

IL_00d4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e9;
		}

IL_00d9:
		{
			MultiplayerParticipant_t672 * L_37 = Enumerator_get_Current_m3829((&V_8), /*hidden argument*/Enumerator_get_Current_m3829_MethodInfo_var);
			V_7 = L_37;
			MultiplayerParticipant_t672 * L_38 = V_7;
			NullCheck(L_38);
			VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_38);
		}

IL_00e9:
		{
			bool L_39 = Enumerator_MoveNext_m3830((&V_8), /*hidden argument*/Enumerator_MoveNext_m3830_MethodInfo_var);
			if (L_39)
			{
				goto IL_00d9;
			}
		}

IL_00f5:
		{
			IL2CPP_LEAVE(0x107, FINALLY_00fa);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_00fa;
	}

FINALLY_00fa:
	{ // begin finally (depth: 1)
		Enumerator_t928  L_40 = V_8;
		Enumerator_t928  L_41 = L_40;
		Object_t * L_42 = Box(Enumerator_t928_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_42);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_42);
		IL2CPP_END_FINALLY(250)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(250)
	{
		IL2CPP_JUMP_TBL(0x107, IL_0107)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0107:
	{
		Dictionary_2_t575 * L_43 = V_2;
		__this->___mNativeParticipants_2 = L_43;
		Dictionary_2_t575 * L_44 = (__this->___mNativeParticipants_2);
		NullCheck(L_44);
		ValueCollection_t921 * L_45 = Dictionary_2_get_Values_m3808(L_44, /*hidden argument*/Dictionary_2_get_Values_m3808_MethodInfo_var);
		Func_2_t578 * L_46 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_9;
		G_B19_0 = L_45;
		G_B19_1 = __this;
		if (L_46)
		{
			G_B20_0 = L_45;
			G_B20_1 = __this;
			goto IL_0132;
		}
	}
	{
		IntPtr_t L_47 = { ActiveState_U3CHandleConnectedSetChangedU3Em__3E_m2433_MethodInfo_var };
		Func_2_t578 * L_48 = (Func_2_t578 *)il2cpp_codegen_object_new (Func_2_t578_il2cpp_TypeInfo_var);
		Func_2__ctor_m3809(L_48, NULL, L_47, /*hidden argument*/Func_2__ctor_m3809_MethodInfo_var);
		((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_9 = L_48;
		G_B20_0 = G_B19_0;
		G_B20_1 = G_B19_1;
	}

IL_0132:
	{
		Func_2_t578 * L_49 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_9;
		Object_t* L_50 = Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802(NULL /*static, unused*/, G_B20_0, L_49, /*hidden argument*/Enumerable_Select_TisMultiplayerParticipant_t672_TisParticipant_t340_m3802_MethodInfo_var);
		Func_2_t352 * L_51 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_10;
		G_B21_0 = L_50;
		G_B21_1 = G_B20_1;
		if (L_51)
		{
			G_B22_0 = L_50;
			G_B22_1 = G_B20_1;
			goto IL_0154;
		}
	}
	{
		IntPtr_t L_52 = { ActiveState_U3CHandleConnectedSetChangedU3Em__3F_m2434_MethodInfo_var };
		Func_2_t352 * L_53 = (Func_2_t352 *)il2cpp_codegen_object_new (Func_2_t352_il2cpp_TypeInfo_var);
		Func_2__ctor_m3703(L_53, NULL, L_52, /*hidden argument*/Func_2__ctor_m3703_MethodInfo_var);
		((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_10 = L_53;
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
	}

IL_0154:
	{
		Func_2_t352 * L_54 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_10;
		Dictionary_2_t576 * L_55 = Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803(NULL /*static, unused*/, G_B22_0, L_54, /*hidden argument*/Enumerable_ToDictionary_TisParticipant_t340_TisString_t_m3803_MethodInfo_var);
		NullCheck(G_B22_1);
		G_B22_1->___mParticipants_3 = L_55;
		Dictionary_2_t576 * L_56 = (__this->___mParticipants_3);
		NullCheck(L_56);
		ValueCollection_t922 * L_57 = Dictionary_2_get_Values_m3810(L_56, /*hidden argument*/Dictionary_2_get_Values_m3810_MethodInfo_var);
		Func_2_t352 * L_58 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_11;
		G_B23_0 = L_57;
		G_B23_1 = (String_t*) &_stringLiteral41;
		G_B23_2 = (String_t*) &_stringLiteral518;
		if (L_58)
		{
			G_B24_0 = L_57;
			G_B24_1 = (String_t*) &_stringLiteral41;
			G_B24_2 = (String_t*) &_stringLiteral518;
			goto IL_0190;
		}
	}
	{
		IntPtr_t L_59 = { ActiveState_U3CHandleConnectedSetChangedU3Em__40_m2435_MethodInfo_var };
		Func_2_t352 * L_60 = (Func_2_t352 *)il2cpp_codegen_object_new (Func_2_t352_il2cpp_TypeInfo_var);
		Func_2__ctor_m3703(L_60, NULL, L_59, /*hidden argument*/Func_2__ctor_m3703_MethodInfo_var);
		((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_11 = L_60;
		G_B24_0 = G_B23_0;
		G_B24_1 = G_B23_1;
		G_B24_2 = G_B23_2;
	}

IL_0190:
	{
		Func_2_t352 * L_61 = ((ActiveState_t586_StaticFields*)ActiveState_t586_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_11;
		Object_t* L_62 = Enumerable_Select_TisParticipant_t340_TisString_t_m3695(NULL /*static, unused*/, G_B24_0, L_61, /*hidden argument*/Enumerable_Select_TisParticipant_t340_TisString_t_m3695_MethodInfo_var);
		StringU5BU5D_t169* L_63 = Enumerable_ToArray_TisString_t_m3697(NULL /*static, unused*/, L_62, /*hidden argument*/Enumerable_ToArray_TisString_t_m3697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = String_Join_m3704(NULL /*static, unused*/, G_B24_1, L_63, /*hidden argument*/NULL);
		String_t* L_65 = String_Concat_m856(NULL /*static, unused*/, G_B24_2, L_64, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		List_1_t43 * L_66 = V_1;
		Participant_t340 * L_67 = (Participant_t340 *)VirtFuncInvoker0< Participant_t340 * >::Invoke(16 /* GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::GetSelf() */, __this);
		NullCheck(L_67);
		String_t* L_68 = Participant_get_ParticipantId_m1387(L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		bool L_69 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0) */, L_66, L_68);
		if (!L_69)
		{
			goto IL_01ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral519, /*hidden argument*/NULL);
	}

IL_01ce:
	{
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * L_70 = V_9;
		Participant_t340 * L_71 = (Participant_t340 *)VirtFuncInvoker0< Participant_t340 * >::Invoke(16 /* GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::GetSelf() */, __this);
		NullCheck(L_71);
		String_t* L_72 = Participant_get_ParticipantId_m1387(L_71, /*hidden argument*/NULL);
		NullCheck(L_70);
		L_70->___selfId_0 = L_72;
		List_1_t43 * L_73 = V_0;
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * L_74 = V_9;
		IntPtr_t L_75 = { U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__41_m2421_MethodInfo_var };
		Func_2_t926 * L_76 = (Func_2_t926 *)il2cpp_codegen_object_new (Func_2_t926_il2cpp_TypeInfo_var);
		Func_2__ctor_m3831(L_76, L_74, L_75, /*hidden argument*/Func_2__ctor_m3831_MethodInfo_var);
		Object_t* L_77 = Enumerable_Where_TisString_t_m3823(NULL /*static, unused*/, L_73, L_76, /*hidden argument*/Enumerable_Where_TisString_t_m3823_MethodInfo_var);
		List_1_t43 * L_78 = Enumerable_ToList_TisString_t_m3824(NULL /*static, unused*/, L_77, /*hidden argument*/Enumerable_ToList_TisString_t_m3824_MethodInfo_var);
		V_0 = L_78;
		List_1_t43 * L_79 = V_1;
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * L_80 = V_9;
		IntPtr_t L_81 = { U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__42_m2422_MethodInfo_var };
		Func_2_t926 * L_82 = (Func_2_t926 *)il2cpp_codegen_object_new (Func_2_t926_il2cpp_TypeInfo_var);
		Func_2__ctor_m3831(L_82, L_80, L_81, /*hidden argument*/Func_2__ctor_m3831_MethodInfo_var);
		Object_t* L_83 = Enumerable_Where_TisString_t_m3823(NULL /*static, unused*/, L_79, L_82, /*hidden argument*/Enumerable_Where_TisString_t_m3823_MethodInfo_var);
		List_1_t43 * L_84 = Enumerable_ToList_TisString_t_m3824(NULL /*static, unused*/, L_83, /*hidden argument*/Enumerable_ToList_TisString_t_m3824_MethodInfo_var);
		V_1 = L_84;
		List_1_t43 * L_85 = V_0;
		NullCheck(L_85);
		int32_t L_86 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_85);
		if ((((int32_t)L_86) <= ((int32_t)0)))
		{
			goto IL_024c;
		}
	}
	{
		List_1_t43 * L_87 = V_0;
		NullCheck(L_87);
		List_1_Sort_m3832(L_87, /*hidden argument*/List_1_Sort_m3832_MethodInfo_var);
		RoomSession_t566 * L_88 = (__this->___mSession_0);
		NullCheck(L_88);
		OnGameThreadForwardingListener_t563 * L_89 = RoomSession_OnGameThreadListener_m2334(L_88, /*hidden argument*/NULL);
		List_1_t43 * L_90 = V_0;
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * L_91 = V_9;
		IntPtr_t L_92 = { U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__43_m2423_MethodInfo_var };
		Func_2_t926 * L_93 = (Func_2_t926 *)il2cpp_codegen_object_new (Func_2_t926_il2cpp_TypeInfo_var);
		Func_2__ctor_m3831(L_93, L_91, L_92, /*hidden argument*/Func_2__ctor_m3831_MethodInfo_var);
		Object_t* L_94 = Enumerable_Where_TisString_t_m3823(NULL /*static, unused*/, L_90, L_93, /*hidden argument*/Enumerable_Where_TisString_t_m3823_MethodInfo_var);
		StringU5BU5D_t169* L_95 = Enumerable_ToArray_TisString_t_m3697(NULL /*static, unused*/, L_94, /*hidden argument*/Enumerable_ToArray_TisString_t_m3697_MethodInfo_var);
		NullCheck(L_89);
		OnGameThreadForwardingListener_PeersConnected_m2368(L_89, L_95, /*hidden argument*/NULL);
	}

IL_024c:
	{
		List_1_t43 * L_96 = V_1;
		NullCheck(L_96);
		int32_t L_97 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_96);
		if ((((int32_t)L_97) <= ((int32_t)0)))
		{
			goto IL_0286;
		}
	}
	{
		List_1_t43 * L_98 = V_1;
		NullCheck(L_98);
		List_1_Sort_m3832(L_98, /*hidden argument*/List_1_Sort_m3832_MethodInfo_var);
		RoomSession_t566 * L_99 = (__this->___mSession_0);
		NullCheck(L_99);
		OnGameThreadForwardingListener_t563 * L_100 = RoomSession_OnGameThreadListener_m2334(L_99, /*hidden argument*/NULL);
		List_1_t43 * L_101 = V_1;
		U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * L_102 = V_9;
		IntPtr_t L_103 = { U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__44_m2424_MethodInfo_var };
		Func_2_t926 * L_104 = (Func_2_t926 *)il2cpp_codegen_object_new (Func_2_t926_il2cpp_TypeInfo_var);
		Func_2__ctor_m3831(L_104, L_102, L_103, /*hidden argument*/Func_2__ctor_m3831_MethodInfo_var);
		Object_t* L_105 = Enumerable_Where_TisString_t_m3823(NULL /*static, unused*/, L_101, L_104, /*hidden argument*/Enumerable_Where_TisString_t_m3823_MethodInfo_var);
		StringU5BU5D_t169* L_106 = Enumerable_ToArray_TisString_t_m3697(NULL /*static, unused*/, L_105, /*hidden argument*/Enumerable_ToArray_TisString_t_m3697_MethodInfo_var);
		NullCheck(L_100);
		OnGameThreadForwardingListener_PeersDisconnected_m2369(L_100, L_106, /*hidden argument*/NULL);
	}

IL_0286:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::LeaveRoom()
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* LeavingRoom_t589_il2cpp_TypeInfo_var;
extern MethodInfo* ActiveState_U3CLeaveRoomU3Em__45_m2436_MethodInfo_var;
extern "C" void ActiveState_LeaveRoom_m2431 (ActiveState_t586 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		LeavingRoom_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(765);
		ActiveState_U3CLeaveRoomU3Em__45_m2436_MethodInfo_var = il2cpp_codegen_method_info_from_index(316);
		s_Il2CppMethodIntialized = true;
	}
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		RoomSession_t566 * L_1 = (__this->___mSession_0);
		NativeRealTimeRoom_t574 * L_2 = (__this->___mRoom_1);
		IntPtr_t L_3 = { ActiveState_U3CLeaveRoomU3Em__45_m2436_MethodInfo_var };
		Action_t588 * L_4 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_4, __this, L_3, /*hidden argument*/NULL);
		LeavingRoom_t589 * L_5 = (LeavingRoom_t589 *)il2cpp_codegen_object_new (LeavingRoom_t589_il2cpp_TypeInfo_var);
		LeavingRoom__ctor_m2440(L_5, L_1, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		RoomSession_EnterState_m2335(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__3D(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" String_t* ActiveState_U3CHandleConnectedSetChangedU3Em__3D_m2432 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method)
{
	{
		MultiplayerParticipant_t672 * L_0 = ___p;
		NullCheck(L_0);
		String_t* L_1 = MultiplayerParticipant_Id_m2717(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__3E(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" Participant_t340 * ActiveState_U3CHandleConnectedSetChangedU3Em__3E_m2433 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method)
{
	{
		MultiplayerParticipant_t672 * L_0 = ___p;
		NullCheck(L_0);
		Participant_t340 * L_1 = MultiplayerParticipant_AsParticipant_m2720(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__3F(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* ActiveState_U3CHandleConnectedSetChangedU3Em__3F_m2434 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method)
{
	{
		Participant_t340 * L_0 = ___p;
		NullCheck(L_0);
		String_t* L_1 = Participant_get_ParticipantId_m1387(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__40(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* ActiveState_U3CHandleConnectedSetChangedU3Em__40_m2435 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method)
{
	{
		Participant_t340 * L_0 = ___p;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String GooglePlayGames.BasicApi.Multiplayer.Participant::ToString() */, L_0);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<LeaveRoom>m__45()
extern "C" void ActiveState_U3CLeaveRoomU3Em__45_m2436 (ActiveState_t586 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		OnGameThreadForwardingListener_t563 * L_1 = RoomSession_OnGameThreadListener_m2334(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		OnGameThreadForwardingListener_LeftRoom_m2367(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern MethodInfo* Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var;
extern "C" void ShutdownState__ctor_m2437 (ShutdownState_t587 * __this, RoomSession_t566 * ___session, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		State__ctor_m2373(__this, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = ___session;
		RoomSession_t566 * L_1 = Misc_CheckNotNull_TisRoomSession_t566_m3798(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var);
		__this->___mSession_0 = L_1;
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::IsActive()
extern "C" bool ShutdownState_IsActive_m2438 (ShutdownState_t587 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::LeaveRoom()
extern "C" void ShutdownState_LeaveRoom_m2439 (ShutdownState_t587 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		OnGameThreadForwardingListener_t563 * L_1 = RoomSession_OnGameThreadListener_m2334(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		OnGameThreadForwardingListener_LeftRoom_m2367(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>
#include "mscorlib_System_Action_1_gen_31.h"
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>
#include "mscorlib_System_Action_1_gen_31MethodDeclarations.h"
struct Misc_t391;
struct Action_t588;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action>(!!0)
#define Misc_CheckNotNull_TisAction_t588_m3833(__this /* static, unused */, p0, method) (( Action_t588 * (*) (Object_t * /* static, unused */, Action_t588 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession,GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,System.Action)
extern MethodInfo* Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_t588_m3833_MethodInfo_var;
extern "C" void LeavingRoom__ctor_m2440 (LeavingRoom_t589 * __this, RoomSession_t566 * ___session, NativeRealTimeRoom_t574 * ___room, Action_t588 * ___leavingCompleteCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483919);
		Misc_CheckNotNull_TisAction_t588_m3833_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		s_Il2CppMethodIntialized = true;
	}
	{
		State__ctor_m2373(__this, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = ___session;
		RoomSession_t566 * L_1 = Misc_CheckNotNull_TisRoomSession_t566_m3798(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var);
		__this->___mSession_0 = L_1;
		NativeRealTimeRoom_t574 * L_2 = ___room;
		NativeRealTimeRoom_t574 * L_3 = Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisNativeRealTimeRoom_t574_m3799_MethodInfo_var);
		__this->___mRoomToLeave_1 = L_3;
		Action_t588 * L_4 = ___leavingCompleteCallback;
		Action_t588 * L_5 = Misc_CheckNotNull_TisAction_t588_m3833(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisAction_t588_m3833_MethodInfo_var);
		__this->___mLeavingCompleteCallback_2 = L_5;
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::IsActive()
extern "C" bool LeavingRoom_IsActive_m2441 (LeavingRoom_t589 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::OnStateEntered()
extern TypeInfo* Action_1_t889_il2cpp_TypeInfo_var;
extern MethodInfo* LeavingRoom_U3COnStateEnteredU3Em__46_m2443_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3834_MethodInfo_var;
extern "C" void LeavingRoom_OnStateEntered_m2442 (LeavingRoom_t589 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t889_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(773);
		LeavingRoom_U3COnStateEnteredU3Em__46_m2443_MethodInfo_var = il2cpp_codegen_method_info_from_index(318);
		Action_1__ctor_m3834_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		s_Il2CppMethodIntialized = true;
	}
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		RealtimeManager_t564 * L_1 = RoomSession_Manager_m2331(L_0, /*hidden argument*/NULL);
		NativeRealTimeRoom_t574 * L_2 = (__this->___mRoomToLeave_1);
		IntPtr_t L_3 = { LeavingRoom_U3COnStateEnteredU3Em__46_m2443_MethodInfo_var };
		Action_1_t889 * L_4 = (Action_1_t889 *)il2cpp_codegen_object_new (Action_1_t889_il2cpp_TypeInfo_var);
		Action_1__ctor_m3834(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m3834_MethodInfo_var);
		NullCheck(L_1);
		RealtimeManager_LeaveRoom_m2988(L_1, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::<OnStateEntered>m__46(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern TypeInfo* ShutdownState_t587_il2cpp_TypeInfo_var;
extern "C" void LeavingRoom_U3COnStateEnteredU3Em__46_m2443 (LeavingRoom_t589 * __this, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShutdownState_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(757);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t588 * L_0 = (__this->___mLeavingCompleteCallback_2);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_0);
		RoomSession_t566 * L_1 = (__this->___mSession_0);
		RoomSession_t566 * L_2 = (__this->___mSession_0);
		ShutdownState_t587 * L_3 = (ShutdownState_t587 *)il2cpp_codegen_object_new (ShutdownState_t587_il2cpp_TypeInfo_var);
		ShutdownState__ctor_m2437(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		RoomSession_EnterState_m2335(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern MethodInfo* Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var;
extern "C" void AbortingRoomCreationState__ctor_m2444 (AbortingRoomCreationState_t590 * __this, RoomSession_t566 * ___session, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		State__ctor_m2373(__this, /*hidden argument*/NULL);
		RoomSession_t566 * L_0 = ___session;
		RoomSession_t566 * L_1 = Misc_CheckNotNull_TisRoomSession_t566_m3798(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisRoomSession_t566_m3798_MethodInfo_var);
		__this->___mSession_0 = L_1;
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::IsActive()
extern "C" bool AbortingRoomCreationState_IsActive_m2445 (AbortingRoomCreationState_t590 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern TypeInfo* ShutdownState_t587_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* LeavingRoom_t589_il2cpp_TypeInfo_var;
extern MethodInfo* AbortingRoomCreationState_U3CHandleRoomResponseU3Em__47_m2447_MethodInfo_var;
extern "C" void AbortingRoomCreationState_HandleRoomResponse_m2446 (AbortingRoomCreationState_t590 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShutdownState_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(757);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		LeavingRoom_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(765);
		AbortingRoomCreationState_U3CHandleRoomResponseU3Em__47_m2447_MethodInfo_var = il2cpp_codegen_method_info_from_index(320);
		s_Il2CppMethodIntialized = true;
	}
	{
		RealTimeRoomResponse_t695 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = RealTimeRoomResponse_RequestSucceeded_m2955(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		RoomSession_t566 * L_2 = (__this->___mSession_0);
		RoomSession_t566 * L_3 = (__this->___mSession_0);
		ShutdownState_t587 * L_4 = (ShutdownState_t587 *)il2cpp_codegen_object_new (ShutdownState_t587_il2cpp_TypeInfo_var);
		ShutdownState__ctor_m2437(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		RoomSession_EnterState_m2335(L_2, L_4, /*hidden argument*/NULL);
		RoomSession_t566 * L_5 = (__this->___mSession_0);
		NullCheck(L_5);
		OnGameThreadForwardingListener_t563 * L_6 = RoomSession_OnGameThreadListener_m2334(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_6, 0, /*hidden argument*/NULL);
		return;
	}

IL_0033:
	{
		RoomSession_t566 * L_7 = (__this->___mSession_0);
		RoomSession_t566 * L_8 = (__this->___mSession_0);
		RealTimeRoomResponse_t695 * L_9 = ___response;
		NullCheck(L_9);
		NativeRealTimeRoom_t574 * L_10 = RealTimeRoomResponse_Room_m2956(L_9, /*hidden argument*/NULL);
		IntPtr_t L_11 = { AbortingRoomCreationState_U3CHandleRoomResponseU3Em__47_m2447_MethodInfo_var };
		Action_t588 * L_12 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_12, __this, L_11, /*hidden argument*/NULL);
		LeavingRoom_t589 * L_13 = (LeavingRoom_t589 *)il2cpp_codegen_object_new (LeavingRoom_t589_il2cpp_TypeInfo_var);
		LeavingRoom__ctor_m2440(L_13, L_8, L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_7);
		RoomSession_EnterState_m2335(L_7, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/AbortingRoomCreationState::<HandleRoomResponse>m__47()
extern "C" void AbortingRoomCreationState_U3CHandleRoomResponseU3Em__47_m2447 (AbortingRoomCreationState_t590 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mSession_0);
		NullCheck(L_0);
		OnGameThreadForwardingListener_t563 * L_1 = RoomSession_OnGameThreadListener_m2334(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		OnGameThreadForwardingListener_RoomConnected_m2366(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_18.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_18MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2D::.ctor()
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2D__ctor_m2448 (U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_19.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_19MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B::.ctor()
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2B__ctor_m2449 (U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_20.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_20MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_34.h"
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeRoo.h"
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealTimeEve_0.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse>
#include "mscorlib_System_Action_1_gen_32.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse>
#include "mscorlib_System_Action_1_gen_32MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::.ctor()
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2C__ctor_m2450 (U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::<>m__23()
extern TypeInfo* Action_1_t884_il2cpp_TypeInfo_var;
extern MethodInfo* RoomSession_HandleRoomResponse_m2342_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3835_MethodInfo_var;
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2C_U3CU3Em__23_m2451 (U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(775);
		RoomSession_HandleRoomResponse_m2342_MethodInfo_var = il2cpp_codegen_method_info_from_index(321);
		Action_1__ctor_m3835_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483970);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeRealtimeMultiplayerClient_t536 * L_0 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_0);
		RealtimeManager_t564 * L_1 = (L_0->___mRealtimeManager_2);
		U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * L_2 = (__this->___U3CU3Ef__refU2443_2);
		NullCheck(L_2);
		RealtimeRoomConfig_t592 * L_3 = (L_2->___config_0);
		RealTimeEventListenerHelper_t594 * L_4 = (__this->___helper_0);
		U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_5 = (__this->___U3CU3Ef__refU2445_1);
		NullCheck(L_5);
		RoomSession_t566 * L_6 = (L_5->___newSession_0);
		IntPtr_t L_7 = { RoomSession_HandleRoomResponse_m2342_MethodInfo_var };
		Action_1_t884 * L_8 = (Action_1_t884 *)il2cpp_codegen_object_new (Action_1_t884_il2cpp_TypeInfo_var);
		Action_1__ctor_m3835(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m3835_MethodInfo_var);
		NullCheck(L_1);
		RealtimeManager_CreateRoom_m2977(L_1, L_3, L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_21.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_21MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::.ctor()
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E__ctor_m2452 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__24(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__24_m2453 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___session_0);
		NativeRealTimeRoom_t574 * L_1 = ___room;
		MultiplayerParticipant_t672 * L_2 = ___participant;
		ByteU5BU5D_t350* L_3 = ___data;
		bool L_4 = ___isReliable;
		NullCheck(L_0);
		RoomSession_OnDataReceived_m2343(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__25(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__25_m2454 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___session_0);
		NativeRealTimeRoom_t574 * L_1 = ___room;
		MultiplayerParticipant_t672 * L_2 = ___participant;
		NullCheck(L_0);
		RoomSession_OnParticipantStatusChanged_m2341(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__26(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__26_m2455 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___session_0);
		NativeRealTimeRoom_t574 * L_1 = ___room;
		NullCheck(L_0);
		RoomSession_OnConnectedSetChanged_m2340(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__27(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__27_m2456 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___session_0);
		NativeRealTimeRoom_t574 * L_1 = ___room;
		NullCheck(L_0);
		RoomSession_OnRoomStatusChanged_m2339(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey30
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_22.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey30
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_22MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey30::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey30__ctor_m2457 (U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_23.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_23MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey31__ctor_m2458 (U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey32
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_24.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey32
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_24MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_25.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey32::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey32__ctor_m2459 (U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey32::<>m__2C()
extern TypeInfo* Action_1_t884_il2cpp_TypeInfo_var;
extern MethodInfo* RoomSession_HandleRoomResponse_m2342_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3835_MethodInfo_var;
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey32_U3CU3Em__2C_m2460 (U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(775);
		RoomSession_HandleRoomResponse_m2342_MethodInfo_var = il2cpp_codegen_method_info_from_index(321);
		Action_1__ctor_m3835_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483970);
		s_Il2CppMethodIntialized = true;
	}
	{
		U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_0 = (__this->___U3CU3Ef__refU2447_1);
		NullCheck(L_0);
		NativeRealtimeMultiplayerClient_t536 * L_1 = (L_0->___U3CU3Ef__this_2);
		NullCheck(L_1);
		RealtimeManager_t564 * L_2 = (L_1->___mRealtimeManager_2);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * L_3 = (__this->___U3CU3Ef__refU2449_2);
		NullCheck(L_3);
		RealtimeRoomConfig_t592 * L_4 = (L_3->___config_0);
		RealTimeEventListenerHelper_t594 * L_5 = (__this->___helper_0);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_6 = (__this->___U3CU3Ef__refU2447_1);
		NullCheck(L_6);
		RoomSession_t566 * L_7 = (L_6->___newRoom_0);
		IntPtr_t L_8 = { RoomSession_HandleRoomResponse_m2342_MethodInfo_var };
		Action_1_t884 * L_9 = (Action_1_t884 *)il2cpp_codegen_object_new (Action_1_t884_il2cpp_TypeInfo_var);
		Action_1__ctor_m3835(L_9, L_7, L_8, /*hidden argument*/Action_1__ctor_m3835_MethodInfo_var);
		NullCheck(L_2);
		RealtimeManager_CreateRoom_m2977(L_2, L_4, L_5, L_9, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_25MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PlayerSelec_0.h"
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeRoo_0.h"
// GooglePlayGames.Native.NativeClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient.h"
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PlayerSelec_0MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeRoo_0MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_34MethodDeclarations.h"
// GooglePlayGames.Native.NativeClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClientMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey2F__ctor_m2461 (U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F::<>m__28(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599_il2cpp_TypeInfo_var;
extern TypeInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* Enumerable_Count_TisString_t_m3819_MethodInfo_var;
extern MethodInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey32_U3CU3Em__2C_m2460_MethodInfo_var;
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_U3CU3Em__28_m2462 (U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * __this, PlayerSelectUIResponse_t686 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(776);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(777);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Enumerable_Count_TisString_t_m3819_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey32_U3CU3Em__2C_m2460_MethodInfo_var = il2cpp_codegen_method_info_from_index(323);
		s_Il2CppMethodIntialized = true;
	}
	RealtimeRoomConfigBuilder_t700 * V_0 = {0};
	U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * V_1 = {0};
	U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PlayerSelectUIResponse_t686 * L_0 = ___response;
		NullCheck(L_0);
		int32_t L_1 = PlayerSelectUIResponse_Status_m2882(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral520, /*hidden argument*/NULL);
		RoomSession_t566 * L_2 = (__this->___newRoom_0);
		NullCheck(L_2);
		RoomSession_LeaveRoom_m2336(L_2, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		NativeRealtimeMultiplayerClient_t536 * L_3 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_3);
		RoomSession_t566 * L_4 = (L_3->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		PlayerSelectUIResponse_t686 * L_5 = ___response;
		NullCheck(L_5);
		uint32_t L_6 = PlayerSelectUIResponse_MinimumAutomatchingPlayers_m2885(L_5, /*hidden argument*/NULL);
		PlayerSelectUIResponse_t686 * L_7 = ___response;
		int32_t L_8 = Enumerable_Count_TisString_t_m3819(NULL /*static, unused*/, L_7, /*hidden argument*/Enumerable_Count_TisString_t_m3819_MethodInfo_var);
		NullCheck(L_4);
		RoomSession_set_MinPlayersToStart_m2330(L_4, ((int32_t)((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_8))+(int32_t)1)), /*hidden argument*/NULL);
		RealtimeRoomConfigBuilder_t700 * L_9 = RealtimeRoomConfigBuilder_Create_m3009(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			RealtimeRoomConfigBuilder_t700 * L_10 = V_0;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * L_11 = (__this->___U3CU3Ef__refU2448_1);
			NullCheck(L_11);
			uint32_t L_12 = (L_11->___variant_0);
			NullCheck(L_10);
			RealtimeRoomConfigBuilder_SetVariant_m3002(L_10, L_12, /*hidden argument*/NULL);
			RealtimeRoomConfigBuilder_t700 * L_13 = V_0;
			PlayerSelectUIResponse_t686 * L_14 = ___response;
			NullCheck(L_13);
			RealtimeRoomConfigBuilder_PopulateFromUIResponse_m3001(L_13, L_14, /*hidden argument*/NULL);
			U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * L_15 = (U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 *)il2cpp_codegen_object_new (U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599_il2cpp_TypeInfo_var);
			U3CCreateWithInvitationScreenU3Ec__AnonStorey31__ctor_m2458(L_15, /*hidden argument*/NULL);
			V_1 = L_15;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * L_16 = V_1;
			NullCheck(L_16);
			L_16->___U3CU3Ef__refU2447_1 = __this;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * L_17 = V_1;
			RealtimeRoomConfigBuilder_t700 * L_18 = V_0;
			NullCheck(L_18);
			RealtimeRoomConfig_t592 * L_19 = RealtimeRoomConfigBuilder_Build_m3007(L_18, /*hidden argument*/NULL);
			NullCheck(L_17);
			L_17->___config_0 = L_19;
		}

IL_007c:
		try
		{ // begin try (depth: 2)
			{
				U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * L_20 = (U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 *)il2cpp_codegen_object_new (U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600_il2cpp_TypeInfo_var);
				U3CCreateWithInvitationScreenU3Ec__AnonStorey32__ctor_m2459(L_20, /*hidden argument*/NULL);
				V_2 = L_20;
				U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * L_21 = V_2;
				NullCheck(L_21);
				L_21->___U3CU3Ef__refU2447_1 = __this;
				U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * L_22 = V_2;
				U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * L_23 = V_1;
				NullCheck(L_22);
				L_22->___U3CU3Ef__refU2449_2 = L_23;
				U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * L_24 = V_2;
				RoomSession_t566 * L_25 = (__this->___newRoom_0);
				RealTimeEventListenerHelper_t594 * L_26 = NativeRealtimeMultiplayerClient_HelperForSession_m2481(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
				NullCheck(L_24);
				L_24->___helper_0 = L_26;
			}

IL_00a1:
			try
			{ // begin try (depth: 3)
				RoomSession_t566 * L_27 = (__this->___newRoom_0);
				NativeRealtimeMultiplayerClient_t536 * L_28 = (__this->___U3CU3Ef__this_2);
				NullCheck(L_28);
				NativeClient_t524 * L_29 = (L_28->___mNativeClient_1);
				NullCheck(L_29);
				String_t* L_30 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeClient::GetUserId() */, L_29);
				U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * L_31 = V_2;
				IntPtr_t L_32 = { U3CCreateWithInvitationScreenU3Ec__AnonStorey32_U3CU3Em__2C_m2460_MethodInfo_var };
				Action_t588 * L_33 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
				Action__ctor_m3796(L_33, L_31, L_32, /*hidden argument*/NULL);
				NullCheck(L_27);
				RoomSession_StartRoomCreation_m2338(L_27, L_30, L_33, /*hidden argument*/NULL);
				IL2CPP_LEAVE(0xE4, FINALLY_00cd);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t135 *)e.ex;
				goto FINALLY_00cd;
			}

FINALLY_00cd:
			{ // begin finally (depth: 3)
				{
					U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * L_34 = V_2;
					NullCheck(L_34);
					RealTimeEventListenerHelper_t594 * L_35 = (L_34->___helper_0);
					if (!L_35)
					{
						goto IL_00e3;
					}
				}

IL_00d8:
				{
					U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * L_36 = V_2;
					NullCheck(L_36);
					RealTimeEventListenerHelper_t594 * L_37 = (L_36->___helper_0);
					NullCheck(L_37);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_37);
				}

IL_00e3:
				{
					IL2CPP_END_FINALLY(205)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(205)
			{
				IL2CPP_JUMP_TBL(0xE4, IL_00e4)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
			}

IL_00e4:
			{
				IL2CPP_LEAVE(0x100, FINALLY_00e9);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_00e9;
		}

FINALLY_00e9:
		{ // begin finally (depth: 2)
			{
				U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * L_38 = V_1;
				NullCheck(L_38);
				RealtimeRoomConfig_t592 * L_39 = (L_38->___config_0);
				if (!L_39)
				{
					goto IL_00ff;
				}
			}

IL_00f4:
			{
				U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * L_40 = V_1;
				NullCheck(L_40);
				RealtimeRoomConfig_t592 * L_41 = (L_40->___config_0);
				NullCheck(L_41);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_41);
			}

IL_00ff:
			{
				IL2CPP_END_FINALLY(233)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(233)
		{
			IL2CPP_JUMP_TBL(0x100, IL_0100)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_0100:
		{
			IL2CPP_LEAVE(0x112, FINALLY_0105);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0105;
	}

FINALLY_0105:
	{ // begin finally (depth: 1)
		{
			RealtimeRoomConfigBuilder_t700 * L_42 = V_0;
			if (!L_42)
			{
				goto IL_0111;
			}
		}

IL_010b:
		{
			RealtimeRoomConfigBuilder_t700 * L_43 = V_0;
			NullCheck(L_43);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_43);
		}

IL_0111:
		{
			IL2CPP_END_FINALLY(261)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(261)
	{
		IL2CPP_JUMP_TBL(0x112, IL_0112)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0112:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_26.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_26MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey34__ctor_m2463 (U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_27.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_27MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_28.h"
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Multiplayer.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey35__ctor_m2464 (U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::<>m__2D()
extern TypeInfo* Action_1_t884_il2cpp_TypeInfo_var;
extern MethodInfo* U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2E_m2466_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3835_MethodInfo_var;
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2D_m2465 (U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(775);
		U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2E_m2466_MethodInfo_var = il2cpp_codegen_method_info_from_index(324);
		Action_1__ctor_m3835_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483970);
		s_Il2CppMethodIntialized = true;
	}
	{
		U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_0 = (__this->___U3CU3Ef__refU2451_1);
		NullCheck(L_0);
		NativeRealtimeMultiplayerClient_t536 * L_1 = (L_0->___U3CU3Ef__this_1);
		NullCheck(L_1);
		RealtimeManager_t564 * L_2 = (L_1->___mRealtimeManager_2);
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * L_3 = (__this->___U3CU3Ef__refU2452_2);
		NullCheck(L_3);
		MultiplayerInvitation_t601 * L_4 = (L_3->___invitation_0);
		RealTimeEventListenerHelper_t594 * L_5 = (__this->___helper_0);
		IntPtr_t L_6 = { U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2E_m2466_MethodInfo_var };
		Action_1_t884 * L_7 = (Action_1_t884 *)il2cpp_codegen_object_new (Action_1_t884_il2cpp_TypeInfo_var);
		Action_1__ctor_m3835(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m3835_MethodInfo_var);
		NullCheck(L_2);
		RealtimeManager_AcceptInvitation_m2989(L_2, L_4, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::<>m__2E(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2E_m2466 (U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * __this, RealTimeRoomResponse_t695 * ___acceptResponse, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	MultiplayerInvitation_t601 * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * L_0 = (__this->___U3CU3Ef__refU2452_2);
		NullCheck(L_0);
		MultiplayerInvitation_t601 * L_1 = (L_0->___invitation_0);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_2 = (__this->___U3CU3Ef__refU2451_1);
		NullCheck(L_2);
		RoomSession_t566 * L_3 = (L_2->___newRoom_0);
		RealTimeRoomResponse_t695 * L_4 = ___acceptResponse;
		NullCheck(L_3);
		RoomSession_HandleRoomResponse_m2342(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x2F, FINALLY_0022);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0022;
	}

FINALLY_0022:
	{ // begin finally (depth: 1)
		{
			MultiplayerInvitation_t601 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_002e;
			}
		}

IL_0028:
		{
			MultiplayerInvitation_t601 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_6);
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(34)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(34)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_002f:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_28MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_0.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_0MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_MultiplayerMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey33__ctor_m2467 (U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33::<>m__29(GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse)
extern TypeInfo* U3CAcceptFromInboxU3Ec__AnonStorey34_t603_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* U3CAcceptFromInboxU3Ec__AnonStorey35_t604_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2D_m2465_MethodInfo_var;
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey33_U3CU3Em__29_m2468 (U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * __this, RoomInboxUIResponse_t696 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(778);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		U3CAcceptFromInboxU3Ec__AnonStorey35_t604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(779);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2D_m2465_MethodInfo_var = il2cpp_codegen_method_info_from_index(325);
		s_Il2CppMethodIntialized = true;
	}
	U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * V_0 = {0};
	U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * L_0 = (U3CAcceptFromInboxU3Ec__AnonStorey34_t603 *)il2cpp_codegen_object_new (U3CAcceptFromInboxU3Ec__AnonStorey34_t603_il2cpp_TypeInfo_var);
		U3CAcceptFromInboxU3Ec__AnonStorey34__ctor_m2463(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__refU2451_1 = __this;
		RoomInboxUIResponse_t696 * L_2 = ___response;
		NullCheck(L_2);
		int32_t L_3 = RoomInboxUIResponse_ResponseStatus_m2960(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral520, /*hidden argument*/NULL);
		RoomSession_t566 * L_4 = (__this->___newRoom_0);
		NullCheck(L_4);
		RoomSession_LeaveRoom_m2336(L_4, /*hidden argument*/NULL);
		return;
	}

IL_002f:
	{
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * L_5 = V_0;
		RoomInboxUIResponse_t696 * L_6 = ___response;
		NullCheck(L_6);
		MultiplayerInvitation_t601 * L_7 = RoomInboxUIResponse_Invitation_m2961(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->___invitation_0 = L_7;
		U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * L_8 = (U3CAcceptFromInboxU3Ec__AnonStorey35_t604 *)il2cpp_codegen_object_new (U3CAcceptFromInboxU3Ec__AnonStorey35_t604_il2cpp_TypeInfo_var);
		U3CAcceptFromInboxU3Ec__AnonStorey35__ctor_m2464(L_8, /*hidden argument*/NULL);
		V_1 = L_8;
		U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * L_9 = V_1;
		NullCheck(L_9);
		L_9->___U3CU3Ef__refU2451_1 = __this;
		U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * L_10 = V_1;
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * L_11 = V_0;
		NullCheck(L_10);
		L_10->___U3CU3Ef__refU2452_2 = L_11;
		U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * L_12 = V_1;
		RoomSession_t566 * L_13 = (__this->___newRoom_0);
		RealTimeEventListenerHelper_t594 * L_14 = NativeRealtimeMultiplayerClient_HelperForSession_m2481(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->___helper_0 = L_14;
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * L_15 = V_0;
		NullCheck(L_15);
		MultiplayerInvitation_t601 * L_16 = (L_15->___invitation_0);
		NullCheck(L_16);
		String_t* L_17 = MultiplayerInvitation_Id_m2703(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral521, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		RoomSession_t566 * L_19 = (__this->___newRoom_0);
		NativeRealtimeMultiplayerClient_t536 * L_20 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_20);
		NativeClient_t524 * L_21 = (L_20->___mNativeClient_1);
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeClient::GetUserId() */, L_21);
		U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * L_23 = V_1;
		IntPtr_t L_24 = { U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2D_m2465_MethodInfo_var };
		Action_t588 * L_25 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_25, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_19);
		RoomSession_StartRoomCreation_m2338(L_19, L_22, L_25, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0xBD, FINALLY_00a6);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_00a6;
	}

FINALLY_00a6:
	{ // begin finally (depth: 1)
		{
			U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * L_26 = V_1;
			NullCheck(L_26);
			RealTimeEventListenerHelper_t594 * L_27 = (L_26->___helper_0);
			if (!L_27)
			{
				goto IL_00bc;
			}
		}

IL_00b1:
		{
			U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * L_28 = V_1;
			NullCheck(L_28);
			RealTimeEventListenerHelper_t594 * L_29 = (L_28->___helper_0);
			NullCheck(L_29);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_29);
		}

IL_00bc:
		{
			IL2CPP_END_FINALLY(166)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(166)
	{
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00bd:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_29.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_29MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey37__ctor_m2469 (U3CAcceptInvitationU3Ec__AnonStorey37_t605 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_30.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_30MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey38__ctor_m2470 (U3CAcceptInvitationU3Ec__AnonStorey38_t607 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_31.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_31MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_32.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey39__ctor_m2471 (U3CAcceptInvitationU3Ec__AnonStorey39_t608 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39::<>m__2F()
extern TypeInfo* Action_1_t884_il2cpp_TypeInfo_var;
extern MethodInfo* RoomSession_HandleRoomResponse_m2342_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3835_MethodInfo_var;
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey39_U3CU3Em__2F_m2472 (U3CAcceptInvitationU3Ec__AnonStorey39_t608 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(775);
		RoomSession_HandleRoomResponse_m2342_MethodInfo_var = il2cpp_codegen_method_info_from_index(321);
		Action_1__ctor_m3835_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483970);
		s_Il2CppMethodIntialized = true;
	}
	{
		U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_0 = (__this->___U3CU3Ef__refU2454_1);
		NullCheck(L_0);
		NativeRealtimeMultiplayerClient_t536 * L_1 = (L_0->___U3CU3Ef__this_2);
		NullCheck(L_1);
		RealtimeManager_t564 * L_2 = (L_1->___mRealtimeManager_2);
		U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_3 = (__this->___U3CU3Ef__refU2456_2);
		NullCheck(L_3);
		MultiplayerInvitation_t601 * L_4 = (L_3->___invitation_0);
		RealTimeEventListenerHelper_t594 * L_5 = (__this->___helper_0);
		U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_6 = (__this->___U3CU3Ef__refU2454_1);
		NullCheck(L_6);
		RoomSession_t566 * L_7 = (L_6->___newRoom_0);
		IntPtr_t L_8 = { RoomSession_HandleRoomResponse_m2342_MethodInfo_var };
		Action_1_t884 * L_9 = (Action_1_t884 *)il2cpp_codegen_object_new (Action_1_t884_il2cpp_TypeInfo_var);
		Action_1__ctor_m3835(L_9, L_7, L_8, /*hidden argument*/Action_1__ctor_m3835_MethodInfo_var);
		NullCheck(L_2);
		RealtimeManager_AcceptInvitation_m2989(L_2, L_4, L_5, L_9, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_32MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_2.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_2MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey36__ctor_m2473 (U3CAcceptInvitationU3Ec__AnonStorey36_t606 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36::<>m__2A(GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* U3CAcceptInvitationU3Ec__AnonStorey38_t607_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t883_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t929_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t235_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* U3CAcceptInvitationU3Ec__AnonStorey39_t608_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern MethodInfo* U3CAcceptInvitationU3Ec__AnonStorey39_U3CU3Em__2F_m2472_MethodInfo_var;
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey36_U3CU3Em__2A_m2474 (U3CAcceptInvitationU3Ec__AnonStorey36_t606 * __this, FetchInvitationsResponse_t698 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		U3CAcceptInvitationU3Ec__AnonStorey38_t607_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(780);
		IEnumerable_1_t883_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(781);
		IEnumerator_1_t929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(782);
		UInt32_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(170);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		U3CAcceptInvitationU3Ec__AnonStorey39_t608_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(783);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		U3CAcceptInvitationU3Ec__AnonStorey39_U3CU3Em__2F_m2472_MethodInfo_var = il2cpp_codegen_method_info_from_index(326);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	MultiplayerInvitation_t601 * V_1 = {0};
	U3CAcceptInvitationU3Ec__AnonStorey38_t607 * V_2 = {0};
	U3CAcceptInvitationU3Ec__AnonStorey39_t608 * V_3 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FetchInvitationsResponse_t698 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = FetchInvitationsResponse_RequestSucceeded_m2970(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral522, /*hidden argument*/NULL);
		RoomSession_t566 * L_2 = (__this->___newRoom_0);
		NullCheck(L_2);
		RoomSession_LeaveRoom_m2336(L_2, /*hidden argument*/NULL);
		return;
	}

IL_0021:
	{
		U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_3 = (U3CAcceptInvitationU3Ec__AnonStorey38_t607 *)il2cpp_codegen_object_new (U3CAcceptInvitationU3Ec__AnonStorey38_t607_il2cpp_TypeInfo_var);
		U3CAcceptInvitationU3Ec__AnonStorey38__ctor_m2470(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
		U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_4 = V_2;
		U3CAcceptInvitationU3Ec__AnonStorey37_t605 * L_5 = (__this->___U3CU3Ef__refU2455_1);
		NullCheck(L_4);
		L_4->___U3CU3Ef__refU2455_1 = L_5;
		U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_6 = V_2;
		NullCheck(L_6);
		L_6->___U3CU3Ef__refU2454_2 = __this;
		FetchInvitationsResponse_t698 * L_7 = ___response;
		NullCheck(L_7);
		Object_t* L_8 = FetchInvitationsResponse_Invitations_m2972(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::GetEnumerator() */, IEnumerable_1_t883_il2cpp_TypeInfo_var, L_8);
		V_0 = L_9;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014c;
		}

IL_004b:
		{
			U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_10 = V_2;
			Object_t* L_11 = V_0;
			NullCheck(L_11);
			MultiplayerInvitation_t601 * L_12 = (MultiplayerInvitation_t601 *)InterfaceFuncInvoker0< MultiplayerInvitation_t601 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::get_Current() */, IEnumerator_1_t929_il2cpp_TypeInfo_var, L_11);
			NullCheck(L_10);
			L_10->___invitation_0 = L_12;
			U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_13 = V_2;
			NullCheck(L_13);
			MultiplayerInvitation_t601 * L_14 = (L_13->___invitation_0);
			V_1 = L_14;
		}

IL_005e:
		try
		{ // begin try (depth: 2)
			{
				U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_15 = V_2;
				NullCheck(L_15);
				MultiplayerInvitation_t601 * L_16 = (L_15->___invitation_0);
				NullCheck(L_16);
				String_t* L_17 = MultiplayerInvitation_Id_m2703(L_16, /*hidden argument*/NULL);
				U3CAcceptInvitationU3Ec__AnonStorey37_t605 * L_18 = (__this->___U3CU3Ef__refU2455_1);
				NullCheck(L_18);
				String_t* L_19 = (L_18->___invitationId_0);
				NullCheck(L_17);
				bool L_20 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_17, L_19);
				if (!L_20)
				{
					goto IL_013a;
				}
			}

IL_007e:
			{
				NativeRealtimeMultiplayerClient_t536 * L_21 = (__this->___U3CU3Ef__this_2);
				NullCheck(L_21);
				RoomSession_t566 * L_22 = (L_21->___mCurrentSession_3);
				il2cpp_codegen_memory_barrier();
				U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_23 = V_2;
				NullCheck(L_23);
				MultiplayerInvitation_t601 * L_24 = (L_23->___invitation_0);
				NullCheck(L_24);
				uint32_t L_25 = MultiplayerInvitation_AutomatchingSlots_m2705(L_24, /*hidden argument*/NULL);
				U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_26 = V_2;
				NullCheck(L_26);
				MultiplayerInvitation_t601 * L_27 = (L_26->___invitation_0);
				NullCheck(L_27);
				uint32_t L_28 = MultiplayerInvitation_ParticipantCount_m2706(L_27, /*hidden argument*/NULL);
				NullCheck(L_22);
				RoomSession_set_MinPlayersToStart_m2330(L_22, ((int32_t)((int32_t)L_25+(int32_t)L_28)), /*hidden argument*/NULL);
				NativeRealtimeMultiplayerClient_t536 * L_29 = (__this->___U3CU3Ef__this_2);
				NullCheck(L_29);
				RoomSession_t566 * L_30 = (L_29->___mCurrentSession_3);
				il2cpp_codegen_memory_barrier();
				NullCheck(L_30);
				uint32_t L_31 = RoomSession_get_MinPlayersToStart_m2329(L_30, /*hidden argument*/NULL);
				uint32_t L_32 = L_31;
				Object_t * L_33 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_32);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_34 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral523, L_33, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
				Logger_d_m1591(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
				U3CAcceptInvitationU3Ec__AnonStorey39_t608 * L_35 = (U3CAcceptInvitationU3Ec__AnonStorey39_t608 *)il2cpp_codegen_object_new (U3CAcceptInvitationU3Ec__AnonStorey39_t608_il2cpp_TypeInfo_var);
				U3CAcceptInvitationU3Ec__AnonStorey39__ctor_m2471(L_35, /*hidden argument*/NULL);
				V_3 = L_35;
				U3CAcceptInvitationU3Ec__AnonStorey39_t608 * L_36 = V_3;
				NullCheck(L_36);
				L_36->___U3CU3Ef__refU2454_1 = __this;
				U3CAcceptInvitationU3Ec__AnonStorey39_t608 * L_37 = V_3;
				U3CAcceptInvitationU3Ec__AnonStorey38_t607 * L_38 = V_2;
				NullCheck(L_37);
				L_37->___U3CU3Ef__refU2456_2 = L_38;
				U3CAcceptInvitationU3Ec__AnonStorey39_t608 * L_39 = V_3;
				RoomSession_t566 * L_40 = (__this->___newRoom_0);
				RealTimeEventListenerHelper_t594 * L_41 = NativeRealtimeMultiplayerClient_HelperForSession_m2481(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
				NullCheck(L_39);
				L_39->___helper_0 = L_41;
			}

IL_00f2:
			try
			{ // begin try (depth: 3)
				{
					RoomSession_t566 * L_42 = (__this->___newRoom_0);
					NativeRealtimeMultiplayerClient_t536 * L_43 = (__this->___U3CU3Ef__this_2);
					NullCheck(L_43);
					NativeClient_t524 * L_44 = (L_43->___mNativeClient_1);
					NullCheck(L_44);
					String_t* L_45 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeClient::GetUserId() */, L_44);
					U3CAcceptInvitationU3Ec__AnonStorey39_t608 * L_46 = V_3;
					IntPtr_t L_47 = { U3CAcceptInvitationU3Ec__AnonStorey39_U3CU3Em__2F_m2472_MethodInfo_var };
					Action_t588 * L_48 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
					Action__ctor_m3796(L_48, L_46, L_47, /*hidden argument*/NULL);
					NullCheck(L_42);
					RoomSession_StartRoomCreation_m2338(L_42, L_45, L_48, /*hidden argument*/NULL);
					IL2CPP_LEAVE(0x18C, FINALLY_0123);
				}

IL_011e:
				{
					; // IL_011e: leave IL_013a
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t135 *)e.ex;
				goto FINALLY_0123;
			}

FINALLY_0123:
			{ // begin finally (depth: 3)
				{
					U3CAcceptInvitationU3Ec__AnonStorey39_t608 * L_49 = V_3;
					NullCheck(L_49);
					RealTimeEventListenerHelper_t594 * L_50 = (L_49->___helper_0);
					if (!L_50)
					{
						goto IL_0139;
					}
				}

IL_012e:
				{
					U3CAcceptInvitationU3Ec__AnonStorey39_t608 * L_51 = V_3;
					NullCheck(L_51);
					RealTimeEventListenerHelper_t594 * L_52 = (L_51->___helper_0);
					NullCheck(L_52);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_52);
				}

IL_0139:
				{
					IL2CPP_END_FINALLY(291)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(291)
			{
				IL2CPP_END_CLEANUP(0x18C, FINALLY_013f);
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
			}

IL_013a:
			{
				IL2CPP_LEAVE(0x14C, FINALLY_013f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_013f;
		}

FINALLY_013f:
		{ // begin finally (depth: 2)
			{
				MultiplayerInvitation_t601 * L_53 = V_1;
				if (!L_53)
				{
					goto IL_014b;
				}
			}

IL_0145:
			{
				MultiplayerInvitation_t601 * L_54 = V_1;
				NullCheck(L_54);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_54);
			}

IL_014b:
			{
				IL2CPP_END_FINALLY(319)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(319)
		{
			IL2CPP_END_CLEANUP(0x18C, FINALLY_015c);
			IL2CPP_JUMP_TBL(0x14C, IL_014c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_014c:
		{
			Object_t* L_55 = V_0;
			NullCheck(L_55);
			bool L_56 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_55);
			if (L_56)
			{
				goto IL_004b;
			}
		}

IL_0157:
		{
			IL2CPP_LEAVE(0x167, FINALLY_015c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_015c;
	}

FINALLY_015c:
	{ // begin finally (depth: 1)
		{
			Object_t* L_57 = V_0;
			if (L_57)
			{
				goto IL_0160;
			}
		}

IL_015f:
		{
			IL2CPP_END_FINALLY(348)
		}

IL_0160:
		{
			Object_t* L_58 = V_0;
			NullCheck(L_58);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_58);
			IL2CPP_END_FINALLY(348)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(348)
	{
		IL2CPP_JUMP_TBL(0x18C, IL_018c)
		IL2CPP_JUMP_TBL(0x167, IL_0167)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0167:
	{
		U3CAcceptInvitationU3Ec__AnonStorey37_t605 * L_59 = (__this->___U3CU3Ef__refU2455_1);
		NullCheck(L_59);
		String_t* L_60 = (L_59->___invitationId_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral524, L_60, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		RoomSession_t566 * L_62 = (__this->___newRoom_0);
		NullCheck(L_62);
		RoomSession_LeaveRoom_m2336(L_62, /*hidden argument*/NULL);
	}

IL_018c:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_33.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_33MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A::.ctor()
extern "C" void U3CDeclineInvitationU3Ec__AnonStorey3A__ctor_m2475 (U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A::<>m__2B(GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t883_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t929_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern "C" void U3CDeclineInvitationU3Ec__AnonStorey3A_U3CU3Em__2B_m2476 (U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * __this, FetchInvitationsResponse_t698 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IEnumerable_1_t883_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(781);
		IEnumerator_1_t929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(782);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	MultiplayerInvitation_t601 * V_0 = {0};
	Object_t* V_1 = {0};
	MultiplayerInvitation_t601 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FetchInvitationsResponse_t698 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = FetchInvitationsResponse_RequestSucceeded_m2970(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral522, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		FetchInvitationsResponse_t698 * L_2 = ___response;
		NullCheck(L_2);
		Object_t* L_3 = FetchInvitationsResponse_Invitations_m2972(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Object_t* L_4 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::GetEnumerator() */, IEnumerable_1_t883_il2cpp_TypeInfo_var, L_3);
		V_1 = L_4;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0069;
		}

IL_0027:
		{
			Object_t* L_5 = V_1;
			NullCheck(L_5);
			MultiplayerInvitation_t601 * L_6 = (MultiplayerInvitation_t601 *)InterfaceFuncInvoker0< MultiplayerInvitation_t601 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::get_Current() */, IEnumerator_1_t929_il2cpp_TypeInfo_var, L_5);
			V_0 = L_6;
			MultiplayerInvitation_t601 * L_7 = V_0;
			V_2 = L_7;
		}

IL_0030:
		try
		{ // begin try (depth: 2)
			{
				MultiplayerInvitation_t601 * L_8 = V_0;
				NullCheck(L_8);
				String_t* L_9 = MultiplayerInvitation_Id_m2703(L_8, /*hidden argument*/NULL);
				String_t* L_10 = (__this->___invitationId_0);
				NullCheck(L_9);
				bool L_11 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_9, L_10);
				if (!L_11)
				{
					goto IL_0057;
				}
			}

IL_0046:
			{
				NativeRealtimeMultiplayerClient_t536 * L_12 = (__this->___U3CU3Ef__this_1);
				NullCheck(L_12);
				RealtimeManager_t564 * L_13 = (L_12->___mRealtimeManager_2);
				MultiplayerInvitation_t601 * L_14 = V_0;
				NullCheck(L_13);
				RealtimeManager_DeclineInvitation_m2990(L_13, L_14, /*hidden argument*/NULL);
			}

IL_0057:
			{
				IL2CPP_LEAVE(0x69, FINALLY_005c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_005c;
		}

FINALLY_005c:
		{ // begin finally (depth: 2)
			{
				MultiplayerInvitation_t601 * L_15 = V_2;
				if (!L_15)
				{
					goto IL_0068;
				}
			}

IL_0062:
			{
				MultiplayerInvitation_t601 * L_16 = V_2;
				NullCheck(L_16);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_16);
			}

IL_0068:
			{
				IL2CPP_END_FINALLY(92)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(92)
		{
			IL2CPP_JUMP_TBL(0x69, IL_0069)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_0069:
		{
			Object_t* L_17 = V_1;
			NullCheck(L_17);
			bool L_18 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0027;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x84, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			Object_t* L_19 = V_1;
			if (L_19)
			{
				goto IL_007d;
			}
		}

IL_007c:
		{
			IL2CPP_END_FINALLY(121)
		}

IL_007d:
		{
			Object_t* L_20 = V_1;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0084:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
// System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>
#include "System_Core_System_Action_4_gen.h"
// System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "System_Core_System_Action_2_gen_8.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>
#include "mscorlib_System_Action_1_gen_33.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>
#include "mscorlib_System_Action_1_gen_34.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse>
#include "mscorlib_System_Action_1_gen_35.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse>
#include "mscorlib_System_Action_1_gen_36.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMultiMethodDeclarations.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealTimeEve_0MethodDeclarations.h"
// System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>
#include "System_Core_System_Action_4_genMethodDeclarations.h"
// System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "System_Core_System_Action_2_gen_8MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>
#include "mscorlib_System_Action_1_gen_33MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>
#include "mscorlib_System_Action_1_gen_34MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse>
#include "mscorlib_System_Action_1_gen_35MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse>
#include "mscorlib_System_Action_1_gen_36MethodDeclarations.h"
struct Misc_t391;
struct NativeClient_t524;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeClient>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeClient>(!!0)
#define Misc_CheckNotNull_TisNativeClient_t524_m3836(__this /* static, unused */, p0, method) (( NativeClient_t524 * (*) (Object_t * /* static, unused */, NativeClient_t524 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct RealtimeManager_t564;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.RealtimeManager>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.RealtimeManager>(!!0)
#define Misc_CheckNotNull_TisRealtimeManager_t564_m3793(__this /* static, unused */, p0, method) (( RealtimeManager_t564 * (*) (Object_t * /* static, unused */, RealtimeManager_t564 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::.ctor(GooglePlayGames.Native.NativeClient,GooglePlayGames.Native.PInvoke.RealtimeManager)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisNativeClient_t524_m3836_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisRealtimeManager_t564_m3793_MethodInfo_var;
extern "C" void NativeRealtimeMultiplayerClient__ctor_m2477 (NativeRealtimeMultiplayerClient_t536 * __this, NativeClient_t524 * ___nativeClient, RealtimeManager_t564 * ___manager, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Misc_CheckNotNull_TisNativeClient_t524_m3836_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483975);
		Misc_CheckNotNull_TisRealtimeManager_t564_m3793_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483907);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m830(L_0, /*hidden argument*/NULL);
		__this->___mSessionLock_0 = L_0;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		NativeClient_t524 * L_1 = ___nativeClient;
		NativeClient_t524 * L_2 = Misc_CheckNotNull_TisNativeClient_t524_m3836(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisNativeClient_t524_m3836_MethodInfo_var);
		__this->___mNativeClient_1 = L_2;
		RealtimeManager_t564 * L_3 = ___manager;
		RealtimeManager_t564 * L_4 = Misc_CheckNotNull_TisRealtimeManager_t564_m3793(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisRealtimeManager_t564_m3793_MethodInfo_var);
		__this->___mRealtimeManager_2 = L_4;
		RoomSession_t566 * L_5 = NativeRealtimeMultiplayerClient_GetTerminatedSession_m2478(__this, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->___mCurrentSession_3 = L_5;
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetTerminatedSession()
extern TypeInfo* NoopListener_t562_il2cpp_TypeInfo_var;
extern TypeInfo* RoomSession_t566_il2cpp_TypeInfo_var;
extern TypeInfo* ShutdownState_t587_il2cpp_TypeInfo_var;
extern "C" RoomSession_t566 * NativeRealtimeMultiplayerClient_GetTerminatedSession_m2478 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoopListener_t562_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(784);
		RoomSession_t566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(748);
		ShutdownState_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(757);
		s_Il2CppMethodIntialized = true;
	}
	RoomSession_t566 * V_0 = {0};
	{
		RealtimeManager_t564 * L_0 = (__this->___mRealtimeManager_2);
		NoopListener_t562 * L_1 = (NoopListener_t562 *)il2cpp_codegen_object_new (NoopListener_t562_il2cpp_TypeInfo_var);
		NoopListener__ctor_m2320(L_1, /*hidden argument*/NULL);
		RoomSession_t566 * L_2 = (RoomSession_t566 *)il2cpp_codegen_object_new (RoomSession_t566_il2cpp_TypeInfo_var);
		RoomSession__ctor_m2328(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		RoomSession_t566 * L_3 = V_0;
		RoomSession_t566 * L_4 = V_0;
		ShutdownState_t587 * L_5 = (ShutdownState_t587 *)il2cpp_codegen_object_new (ShutdownState_t587_il2cpp_TypeInfo_var);
		ShutdownState__ctor_m2437(L_5, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RoomSession_EnterState_m2335(L_3, L_5, /*hidden argument*/NULL);
		RoomSession_t566 * L_6 = V_0;
		return L_6;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::CreateQuickGame(System.UInt32,System.UInt32,System.UInt32,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void NativeRealtimeMultiplayerClient_CreateQuickGame_m2479 (NativeRealtimeMultiplayerClient_t536 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, Object_t * ___listener, MethodInfo* method)
{
	{
		uint32_t L_0 = ___minOpponents;
		uint32_t L_1 = ___maxOpponents;
		uint32_t L_2 = ___variant;
		Object_t * L_3 = ___listener;
		VirtActionInvoker5< uint32_t, uint32_t, uint32_t, uint64_t, Object_t * >::Invoke(5 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::CreateQuickGame(System.UInt32,System.UInt32,System.UInt32,System.UInt64,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener) */, __this, L_0, L_1, L_2, (((int64_t)0)), L_3);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::CreateQuickGame(System.UInt32,System.UInt32,System.UInt32,System.UInt64,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern TypeInfo* U3CCreateQuickGameU3Ec__AnonStorey2D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* RoomSession_t566_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t235_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* U3CCreateQuickGameU3Ec__AnonStorey2B_t593_il2cpp_TypeInfo_var;
extern TypeInfo* U3CCreateQuickGameU3Ec__AnonStorey2C_t595_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* U3CCreateQuickGameU3Ec__AnonStorey2C_U3CU3Em__23_m2451_MethodInfo_var;
extern "C" void NativeRealtimeMultiplayerClient_CreateQuickGame_m2480 (NativeRealtimeMultiplayerClient_t536 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, uint64_t ___exclusiveBitMask, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCreateQuickGameU3Ec__AnonStorey2D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(785);
		RoomSession_t566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(748);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		UInt32_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(170);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		U3CCreateQuickGameU3Ec__AnonStorey2B_t593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(786);
		U3CCreateQuickGameU3Ec__AnonStorey2C_t595_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(787);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		U3CCreateQuickGameU3Ec__AnonStorey2C_U3CU3Em__23_m2451_MethodInfo_var = il2cpp_codegen_method_info_from_index(328);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	RealtimeRoomConfigBuilder_t700 * V_1 = {0};
	RealtimeRoomConfig_t592 * V_2 = {0};
	U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * V_3 = {0};
	U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * V_4 = {0};
	U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * V_5 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mSessionLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_2 = (U3CCreateQuickGameU3Ec__AnonStorey2D_t591 *)il2cpp_codegen_object_new (U3CCreateQuickGameU3Ec__AnonStorey2D_t591_il2cpp_TypeInfo_var);
			U3CCreateQuickGameU3Ec__AnonStorey2D__ctor_m2448(L_2, /*hidden argument*/NULL);
			V_3 = L_2;
			U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_3 = V_3;
			NullCheck(L_3);
			L_3->___U3CU3Ef__this_1 = __this;
			U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_4 = V_3;
			RealtimeManager_t564 * L_5 = (__this->___mRealtimeManager_2);
			Object_t * L_6 = ___listener;
			RoomSession_t566 * L_7 = (RoomSession_t566 *)il2cpp_codegen_object_new (RoomSession_t566_il2cpp_TypeInfo_var);
			RoomSession__ctor_m2328(L_7, L_5, L_6, /*hidden argument*/NULL);
			NullCheck(L_4);
			L_4->___newSession_0 = L_7;
			RoomSession_t566 * L_8 = (__this->___mCurrentSession_3);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_8);
			bool L_9 = RoomSession_IsActive_m2332(L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_0059;
			}
		}

IL_003f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral486, /*hidden argument*/NULL);
			U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_10 = V_3;
			NullCheck(L_10);
			RoomSession_t566 * L_11 = (L_10->___newSession_0);
			NullCheck(L_11);
			RoomSession_LeaveRoom_m2336(L_11, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x170, FINALLY_0169);
		}

IL_0059:
		{
			U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_12 = V_3;
			NullCheck(L_12);
			RoomSession_t566 * L_13 = (L_12->___newSession_0);
			il2cpp_codegen_memory_barrier();
			__this->___mCurrentSession_3 = L_13;
			uint32_t L_14 = ___minOpponents;
			uint32_t L_15 = L_14;
			Object_t * L_16 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_15);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_17 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral487, L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
			RoomSession_t566 * L_18 = (__this->___mCurrentSession_3);
			il2cpp_codegen_memory_barrier();
			uint32_t L_19 = ___minOpponents;
			NullCheck(L_18);
			RoomSession_set_MinPlayersToStart_m2330(L_18, L_19, /*hidden argument*/NULL);
			RealtimeRoomConfigBuilder_t700 * L_20 = RealtimeRoomConfigBuilder_Create_m3009(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_20;
		}

IL_0090:
		try
		{ // begin try (depth: 2)
			{
				U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * L_21 = (U3CCreateQuickGameU3Ec__AnonStorey2B_t593 *)il2cpp_codegen_object_new (U3CCreateQuickGameU3Ec__AnonStorey2B_t593_il2cpp_TypeInfo_var);
				U3CCreateQuickGameU3Ec__AnonStorey2B__ctor_m2449(L_21, /*hidden argument*/NULL);
				V_4 = L_21;
				U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * L_22 = V_4;
				NullCheck(L_22);
				L_22->___U3CU3Ef__this_1 = __this;
				U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * L_23 = V_4;
				RealtimeRoomConfigBuilder_t700 * L_24 = V_1;
				uint32_t L_25 = ___minOpponents;
				NullCheck(L_24);
				RealtimeRoomConfigBuilder_t700 * L_26 = RealtimeRoomConfigBuilder_SetMinimumAutomatchingPlayers_m3005(L_24, L_25, /*hidden argument*/NULL);
				uint32_t L_27 = ___maxOpponents;
				NullCheck(L_26);
				RealtimeRoomConfigBuilder_t700 * L_28 = RealtimeRoomConfigBuilder_SetMaximumAutomatchingPlayers_m3006(L_26, L_27, /*hidden argument*/NULL);
				uint32_t L_29 = ___variant;
				NullCheck(L_28);
				RealtimeRoomConfigBuilder_t700 * L_30 = RealtimeRoomConfigBuilder_SetVariant_m3002(L_28, L_29, /*hidden argument*/NULL);
				uint64_t L_31 = ___exclusiveBitMask;
				NullCheck(L_30);
				RealtimeRoomConfigBuilder_t700 * L_32 = RealtimeRoomConfigBuilder_SetExclusiveBitMask_m3004(L_30, L_31, /*hidden argument*/NULL);
				NullCheck(L_32);
				RealtimeRoomConfig_t592 * L_33 = RealtimeRoomConfigBuilder_Build_m3007(L_32, /*hidden argument*/NULL);
				NullCheck(L_23);
				L_23->___config_0 = L_33;
				U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * L_34 = V_4;
				NullCheck(L_34);
				RealtimeRoomConfig_t592 * L_35 = (L_34->___config_0);
				V_2 = L_35;
			}

IL_00cd:
			try
			{ // begin try (depth: 3)
				{
					U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_36 = (U3CCreateQuickGameU3Ec__AnonStorey2C_t595 *)il2cpp_codegen_object_new (U3CCreateQuickGameU3Ec__AnonStorey2C_t595_il2cpp_TypeInfo_var);
					U3CCreateQuickGameU3Ec__AnonStorey2C__ctor_m2450(L_36, /*hidden argument*/NULL);
					V_5 = L_36;
					U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_37 = V_5;
					U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_38 = V_3;
					NullCheck(L_37);
					L_37->___U3CU3Ef__refU2445_1 = L_38;
					U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_39 = V_5;
					U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * L_40 = V_4;
					NullCheck(L_39);
					L_39->___U3CU3Ef__refU2443_2 = L_40;
					U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_41 = V_5;
					NullCheck(L_41);
					L_41->___U3CU3Ef__this_3 = __this;
					U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_42 = V_5;
					U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_43 = V_3;
					NullCheck(L_43);
					RoomSession_t566 * L_44 = (L_43->___newSession_0);
					RealTimeEventListenerHelper_t594 * L_45 = NativeRealtimeMultiplayerClient_HelperForSession_m2481(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
					NullCheck(L_42);
					L_42->___helper_0 = L_45;
				}

IL_00ff:
				try
				{ // begin try (depth: 4)
					U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * L_46 = V_3;
					NullCheck(L_46);
					RoomSession_t566 * L_47 = (L_46->___newSession_0);
					NativeClient_t524 * L_48 = (__this->___mNativeClient_1);
					NullCheck(L_48);
					String_t* L_49 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeClient::GetUserId() */, L_48);
					U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_50 = V_5;
					IntPtr_t L_51 = { U3CCreateQuickGameU3Ec__AnonStorey2C_U3CU3Em__23_m2451_MethodInfo_var };
					Action_t588 * L_52 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
					Action__ctor_m3796(L_52, L_50, L_51, /*hidden argument*/NULL);
					NullCheck(L_47);
					RoomSession_StartRoomCreation_m2338(L_47, L_49, L_52, /*hidden argument*/NULL);
					IL2CPP_LEAVE(0x140, FINALLY_0127);
				} // end try (depth: 4)
				catch(Il2CppExceptionWrapper& e)
				{
					__last_unhandled_exception = (Exception_t135 *)e.ex;
					goto FINALLY_0127;
				}

FINALLY_0127:
				{ // begin finally (depth: 4)
					{
						U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_53 = V_5;
						NullCheck(L_53);
						RealTimeEventListenerHelper_t594 * L_54 = (L_53->___helper_0);
						if (!L_54)
						{
							goto IL_013f;
						}
					}

IL_0133:
					{
						U3CCreateQuickGameU3Ec__AnonStorey2C_t595 * L_55 = V_5;
						NullCheck(L_55);
						RealTimeEventListenerHelper_t594 * L_56 = (L_55->___helper_0);
						NullCheck(L_56);
						InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_56);
					}

IL_013f:
					{
						IL2CPP_END_FINALLY(295)
					}
				} // end finally (depth: 4)
				IL2CPP_CLEANUP(295)
				{
					IL2CPP_JUMP_TBL(0x140, IL_0140)
					IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
				}

IL_0140:
				{
					IL2CPP_LEAVE(0x152, FINALLY_0145);
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t135 *)e.ex;
				goto FINALLY_0145;
			}

FINALLY_0145:
			{ // begin finally (depth: 3)
				{
					RealtimeRoomConfig_t592 * L_57 = V_2;
					if (!L_57)
					{
						goto IL_0151;
					}
				}

IL_014b:
				{
					RealtimeRoomConfig_t592 * L_58 = V_2;
					NullCheck(L_58);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_58);
				}

IL_0151:
				{
					IL2CPP_END_FINALLY(325)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(325)
			{
				IL2CPP_JUMP_TBL(0x152, IL_0152)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
			}

IL_0152:
			{
				IL2CPP_LEAVE(0x164, FINALLY_0157);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_0157;
		}

FINALLY_0157:
		{ // begin finally (depth: 2)
			{
				RealtimeRoomConfigBuilder_t700 * L_59 = V_1;
				if (!L_59)
				{
					goto IL_0163;
				}
			}

IL_015d:
			{
				RealtimeRoomConfigBuilder_t700 * L_60 = V_1;
				NullCheck(L_60);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_60);
			}

IL_0163:
			{
				IL2CPP_END_FINALLY(343)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(343)
		{
			IL2CPP_JUMP_TBL(0x164, IL_0164)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_0164:
		{
			IL2CPP_LEAVE(0x170, FINALLY_0169);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0169;
	}

FINALLY_0169:
	{ // begin finally (depth: 1)
		Object_t * L_61 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(361)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(361)
	{
		IL2CPP_JUMP_TBL(0x170, IL_0170)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0170:
	{
		return;
	}
}
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient::HelperForSession(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern TypeInfo* U3CHelperForSessionU3Ec__AnonStorey2E_t596_il2cpp_TypeInfo_var;
extern TypeInfo* Action_4_t882_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t881_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t693_il2cpp_TypeInfo_var;
extern MethodInfo* U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__24_m2453_MethodInfo_var;
extern MethodInfo* Action_4__ctor_m3837_MethodInfo_var;
extern MethodInfo* U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__25_m2454_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3838_MethodInfo_var;
extern MethodInfo* U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__26_m2455_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3839_MethodInfo_var;
extern MethodInfo* U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__27_m2456_MethodInfo_var;
extern "C" RealTimeEventListenerHelper_t594 * NativeRealtimeMultiplayerClient_HelperForSession_m2481 (Object_t * __this /* static, unused */, RoomSession_t566 * ___session, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CHelperForSessionU3Ec__AnonStorey2E_t596_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(788);
		Action_4_t882_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(789);
		Action_2_t881_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(790);
		Action_1_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(791);
		U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__24_m2453_MethodInfo_var = il2cpp_codegen_method_info_from_index(329);
		Action_4__ctor_m3837_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483978);
		U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__25_m2454_MethodInfo_var = il2cpp_codegen_method_info_from_index(331);
		Action_2__ctor_m3838_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483980);
		U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__26_m2455_MethodInfo_var = il2cpp_codegen_method_info_from_index(333);
		Action_1__ctor_m3839_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483982);
		U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__27_m2456_MethodInfo_var = il2cpp_codegen_method_info_from_index(335);
		s_Il2CppMethodIntialized = true;
	}
	U3CHelperForSessionU3Ec__AnonStorey2E_t596 * V_0 = {0};
	{
		U3CHelperForSessionU3Ec__AnonStorey2E_t596 * L_0 = (U3CHelperForSessionU3Ec__AnonStorey2E_t596 *)il2cpp_codegen_object_new (U3CHelperForSessionU3Ec__AnonStorey2E_t596_il2cpp_TypeInfo_var);
		U3CHelperForSessionU3Ec__AnonStorey2E__ctor_m2452(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CHelperForSessionU3Ec__AnonStorey2E_t596 * L_1 = V_0;
		RoomSession_t566 * L_2 = ___session;
		NullCheck(L_1);
		L_1->___session_0 = L_2;
		RealTimeEventListenerHelper_t594 * L_3 = RealTimeEventListenerHelper_Create_m2952(NULL /*static, unused*/, /*hidden argument*/NULL);
		U3CHelperForSessionU3Ec__AnonStorey2E_t596 * L_4 = V_0;
		IntPtr_t L_5 = { U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__24_m2453_MethodInfo_var };
		Action_4_t882 * L_6 = (Action_4_t882 *)il2cpp_codegen_object_new (Action_4_t882_il2cpp_TypeInfo_var);
		Action_4__ctor_m3837(L_6, L_4, L_5, /*hidden argument*/Action_4__ctor_m3837_MethodInfo_var);
		NullCheck(L_3);
		RealTimeEventListenerHelper_t594 * L_7 = RealTimeEventListenerHelper_SetOnDataReceivedCallback_m2949(L_3, L_6, /*hidden argument*/NULL);
		U3CHelperForSessionU3Ec__AnonStorey2E_t596 * L_8 = V_0;
		IntPtr_t L_9 = { U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__25_m2454_MethodInfo_var };
		Action_2_t881 * L_10 = (Action_2_t881 *)il2cpp_codegen_object_new (Action_2_t881_il2cpp_TypeInfo_var);
		Action_2__ctor_m3838(L_10, L_8, L_9, /*hidden argument*/Action_2__ctor_m3838_MethodInfo_var);
		NullCheck(L_7);
		RealTimeEventListenerHelper_t594 * L_11 = RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback_m2946(L_7, L_10, /*hidden argument*/NULL);
		U3CHelperForSessionU3Ec__AnonStorey2E_t596 * L_12 = V_0;
		IntPtr_t L_13 = { U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__26_m2455_MethodInfo_var };
		Action_1_t693 * L_14 = (Action_1_t693 *)il2cpp_codegen_object_new (Action_1_t693_il2cpp_TypeInfo_var);
		Action_1__ctor_m3839(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m3839_MethodInfo_var);
		NullCheck(L_11);
		RealTimeEventListenerHelper_t594 * L_15 = RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback_m2940(L_11, L_14, /*hidden argument*/NULL);
		U3CHelperForSessionU3Ec__AnonStorey2E_t596 * L_16 = V_0;
		IntPtr_t L_17 = { U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__27_m2456_MethodInfo_var };
		Action_1_t693 * L_18 = (Action_1_t693 *)il2cpp_codegen_object_new (Action_1_t693_il2cpp_TypeInfo_var);
		Action_1__ctor_m3839(L_18, L_16, L_17, /*hidden argument*/Action_1__ctor_m3839_MethodInfo_var);
		NullCheck(L_15);
		RealTimeEventListenerHelper_t594 * L_19 = RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback_m2938(L_15, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::CreateWithInvitationScreen(System.UInt32,System.UInt32,System.UInt32,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern TypeInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597_il2cpp_TypeInfo_var;
extern TypeInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598_il2cpp_TypeInfo_var;
extern TypeInfo* RoomSession_t566_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t885_il2cpp_TypeInfo_var;
extern MethodInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_U3CU3Em__28_m2462_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3840_MethodInfo_var;
extern "C" void NativeRealtimeMultiplayerClient_CreateWithInvitationScreen_m2482 (NativeRealtimeMultiplayerClient_t536 * __this, uint32_t ___minOpponents, uint32_t ___maxOppponents, uint32_t ___variant, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(793);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(794);
		RoomSession_t566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(748);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t885_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(795);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_U3CU3Em__28_m2462_MethodInfo_var = il2cpp_codegen_method_info_from_index(336);
		Action_1__ctor_m3840_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * V_1 = {0};
	U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * L_0 = (U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 *)il2cpp_codegen_object_new (U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597_il2cpp_TypeInfo_var);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey30__ctor_m2457(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * L_1 = V_1;
		uint32_t L_2 = ___variant;
		NullCheck(L_1);
		L_1->___variant_0 = L_2;
		U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * L_3 = V_1;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		Object_t * L_4 = (__this->___mSessionLock_0);
		V_0 = L_4;
		Object_t * L_5 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_6 = (U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 *)il2cpp_codegen_object_new (U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598_il2cpp_TypeInfo_var);
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F__ctor_m2461(L_6, /*hidden argument*/NULL);
			V_2 = L_6;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_7 = V_2;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * L_8 = V_1;
			NullCheck(L_7);
			L_7->___U3CU3Ef__refU2448_1 = L_8;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_9 = V_2;
			NullCheck(L_9);
			L_9->___U3CU3Ef__this_2 = __this;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_10 = V_2;
			RealtimeManager_t564 * L_11 = (__this->___mRealtimeManager_2);
			Object_t * L_12 = ___listener;
			RoomSession_t566 * L_13 = (RoomSession_t566 *)il2cpp_codegen_object_new (RoomSession_t566_il2cpp_TypeInfo_var);
			RoomSession__ctor_m2328(L_13, L_11, L_12, /*hidden argument*/NULL);
			NullCheck(L_10);
			L_10->___newRoom_0 = L_13;
			RoomSession_t566 * L_14 = (__this->___mCurrentSession_3);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_14);
			bool L_15 = RoomSession_IsActive_m2332(L_14, /*hidden argument*/NULL);
			if (!L_15)
			{
				goto IL_0074;
			}
		}

IL_005a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral486, /*hidden argument*/NULL);
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_16 = V_2;
			NullCheck(L_16);
			RoomSession_t566 * L_17 = (L_16->___newRoom_0);
			NullCheck(L_17);
			RoomSession_LeaveRoom_m2336(L_17, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xA8, FINALLY_00a1);
		}

IL_0074:
		{
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_18 = V_2;
			NullCheck(L_18);
			RoomSession_t566 * L_19 = (L_18->___newRoom_0);
			il2cpp_codegen_memory_barrier();
			__this->___mCurrentSession_3 = L_19;
			RealtimeManager_t564 * L_20 = (__this->___mRealtimeManager_2);
			uint32_t L_21 = ___minOpponents;
			uint32_t L_22 = ___maxOppponents;
			U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * L_23 = V_2;
			IntPtr_t L_24 = { U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_U3CU3Em__28_m2462_MethodInfo_var };
			Action_1_t885 * L_25 = (Action_1_t885 *)il2cpp_codegen_object_new (Action_1_t885_il2cpp_TypeInfo_var);
			Action_1__ctor_m3840(L_25, L_23, L_24, /*hidden argument*/Action_1__ctor_m3840_MethodInfo_var);
			NullCheck(L_20);
			RealtimeManager_ShowPlayerSelectUI_m2978(L_20, L_21, L_22, 1, L_25, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xA8, FINALLY_00a1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_00a1;
	}

FINALLY_00a1:
	{ // begin finally (depth: 1)
		Object_t * L_26 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(161)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(161)
	{
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00a8:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::ShowWaitingRoomUI()
extern "C" void NativeRealtimeMultiplayerClient_ShowWaitingRoomUI_m2483 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mSessionLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		RoomSession_t566 * L_2 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_2);
		RoomSession_ShowWaitingRoomUI_m2337(L_2, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::AcceptFromInbox(GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern TypeInfo* U3CAcceptFromInboxU3Ec__AnonStorey33_t602_il2cpp_TypeInfo_var;
extern TypeInfo* RoomSession_t566_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t886_il2cpp_TypeInfo_var;
extern MethodInfo* U3CAcceptFromInboxU3Ec__AnonStorey33_U3CU3Em__29_m2468_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3841_MethodInfo_var;
extern "C" void NativeRealtimeMultiplayerClient_AcceptFromInbox_m2484 (NativeRealtimeMultiplayerClient_t536 * __this, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAcceptFromInboxU3Ec__AnonStorey33_t602_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(797);
		RoomSession_t566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(748);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t886_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(798);
		U3CAcceptFromInboxU3Ec__AnonStorey33_U3CU3Em__29_m2468_MethodInfo_var = il2cpp_codegen_method_info_from_index(338);
		Action_1__ctor_m3841_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483987);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mSessionLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_2 = (U3CAcceptFromInboxU3Ec__AnonStorey33_t602 *)il2cpp_codegen_object_new (U3CAcceptFromInboxU3Ec__AnonStorey33_t602_il2cpp_TypeInfo_var);
			U3CAcceptFromInboxU3Ec__AnonStorey33__ctor_m2467(L_2, /*hidden argument*/NULL);
			V_1 = L_2;
			U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_3 = V_1;
			NullCheck(L_3);
			L_3->___U3CU3Ef__this_1 = __this;
			U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_4 = V_1;
			RealtimeManager_t564 * L_5 = (__this->___mRealtimeManager_2);
			Object_t * L_6 = ___listener;
			RoomSession_t566 * L_7 = (RoomSession_t566 *)il2cpp_codegen_object_new (RoomSession_t566_il2cpp_TypeInfo_var);
			RoomSession__ctor_m2328(L_7, L_5, L_6, /*hidden argument*/NULL);
			NullCheck(L_4);
			L_4->___newRoom_0 = L_7;
			RoomSession_t566 * L_8 = (__this->___mCurrentSession_3);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_8);
			bool L_9 = RoomSession_IsActive_m2332(L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_0058;
			}
		}

IL_003e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral488, /*hidden argument*/NULL);
			U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_10 = V_1;
			NullCheck(L_10);
			RoomSession_t566 * L_11 = (L_10->___newRoom_0);
			NullCheck(L_11);
			RoomSession_LeaveRoom_m2336(L_11, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x89, FINALLY_0082);
		}

IL_0058:
		{
			U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_12 = V_1;
			NullCheck(L_12);
			RoomSession_t566 * L_13 = (L_12->___newRoom_0);
			il2cpp_codegen_memory_barrier();
			__this->___mCurrentSession_3 = L_13;
			RealtimeManager_t564 * L_14 = (__this->___mRealtimeManager_2);
			U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * L_15 = V_1;
			IntPtr_t L_16 = { U3CAcceptFromInboxU3Ec__AnonStorey33_U3CU3Em__29_m2468_MethodInfo_var };
			Action_1_t886 * L_17 = (Action_1_t886 *)il2cpp_codegen_object_new (Action_1_t886_il2cpp_TypeInfo_var);
			Action_1__ctor_m3841(L_17, L_15, L_16, /*hidden argument*/Action_1__ctor_m3841_MethodInfo_var);
			NullCheck(L_14);
			RealtimeManager_ShowRoomInboxUI_m2982(L_14, L_17, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x89, FINALLY_0082);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		Object_t * L_18 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(130)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0089:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::AcceptInvitation(System.String,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern TypeInfo* U3CAcceptInvitationU3Ec__AnonStorey37_t605_il2cpp_TypeInfo_var;
extern TypeInfo* U3CAcceptInvitationU3Ec__AnonStorey36_t606_il2cpp_TypeInfo_var;
extern TypeInfo* RoomSession_t566_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t888_il2cpp_TypeInfo_var;
extern MethodInfo* U3CAcceptInvitationU3Ec__AnonStorey36_U3CU3Em__2A_m2474_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3842_MethodInfo_var;
extern "C" void NativeRealtimeMultiplayerClient_AcceptInvitation_m2485 (NativeRealtimeMultiplayerClient_t536 * __this, String_t* ___invitationId, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAcceptInvitationU3Ec__AnonStorey37_t605_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(800);
		U3CAcceptInvitationU3Ec__AnonStorey36_t606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(801);
		RoomSession_t566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(748);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(802);
		U3CAcceptInvitationU3Ec__AnonStorey36_U3CU3Em__2A_m2474_MethodInfo_var = il2cpp_codegen_method_info_from_index(340);
		Action_1__ctor_m3842_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483989);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	U3CAcceptInvitationU3Ec__AnonStorey37_t605 * V_1 = {0};
	U3CAcceptInvitationU3Ec__AnonStorey36_t606 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CAcceptInvitationU3Ec__AnonStorey37_t605 * L_0 = (U3CAcceptInvitationU3Ec__AnonStorey37_t605 *)il2cpp_codegen_object_new (U3CAcceptInvitationU3Ec__AnonStorey37_t605_il2cpp_TypeInfo_var);
		U3CAcceptInvitationU3Ec__AnonStorey37__ctor_m2469(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CAcceptInvitationU3Ec__AnonStorey37_t605 * L_1 = V_1;
		String_t* L_2 = ___invitationId;
		NullCheck(L_1);
		L_1->___invitationId_0 = L_2;
		U3CAcceptInvitationU3Ec__AnonStorey37_t605 * L_3 = V_1;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		Object_t * L_4 = (__this->___mSessionLock_0);
		V_0 = L_4;
		Object_t * L_5 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_6 = (U3CAcceptInvitationU3Ec__AnonStorey36_t606 *)il2cpp_codegen_object_new (U3CAcceptInvitationU3Ec__AnonStorey36_t606_il2cpp_TypeInfo_var);
			U3CAcceptInvitationU3Ec__AnonStorey36__ctor_m2473(L_6, /*hidden argument*/NULL);
			V_2 = L_6;
			U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_7 = V_2;
			U3CAcceptInvitationU3Ec__AnonStorey37_t605 * L_8 = V_1;
			NullCheck(L_7);
			L_7->___U3CU3Ef__refU2455_1 = L_8;
			U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_9 = V_2;
			NullCheck(L_9);
			L_9->___U3CU3Ef__this_2 = __this;
			U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_10 = V_2;
			RealtimeManager_t564 * L_11 = (__this->___mRealtimeManager_2);
			Object_t * L_12 = ___listener;
			RoomSession_t566 * L_13 = (RoomSession_t566 *)il2cpp_codegen_object_new (RoomSession_t566_il2cpp_TypeInfo_var);
			RoomSession__ctor_m2328(L_13, L_11, L_12, /*hidden argument*/NULL);
			NullCheck(L_10);
			L_10->___newRoom_0 = L_13;
			RoomSession_t566 * L_14 = (__this->___mCurrentSession_3);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_14);
			bool L_15 = RoomSession_IsActive_m2332(L_14, /*hidden argument*/NULL);
			if (!L_15)
			{
				goto IL_0073;
			}
		}

IL_0059:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral488, /*hidden argument*/NULL);
			U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_16 = V_2;
			NullCheck(L_16);
			RoomSession_t566 * L_17 = (L_16->___newRoom_0);
			NullCheck(L_17);
			RoomSession_LeaveRoom_m2336(L_17, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xA4, FINALLY_009d);
		}

IL_0073:
		{
			U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_18 = V_2;
			NullCheck(L_18);
			RoomSession_t566 * L_19 = (L_18->___newRoom_0);
			il2cpp_codegen_memory_barrier();
			__this->___mCurrentSession_3 = L_19;
			RealtimeManager_t564 * L_20 = (__this->___mRealtimeManager_2);
			U3CAcceptInvitationU3Ec__AnonStorey36_t606 * L_21 = V_2;
			IntPtr_t L_22 = { U3CAcceptInvitationU3Ec__AnonStorey36_U3CU3Em__2A_m2474_MethodInfo_var };
			Action_1_t888 * L_23 = (Action_1_t888 *)il2cpp_codegen_object_new (Action_1_t888_il2cpp_TypeInfo_var);
			Action_1__ctor_m3842(L_23, L_21, L_22, /*hidden argument*/Action_1__ctor_m3842_MethodInfo_var);
			NullCheck(L_20);
			RealtimeManager_FetchInvitations_m2986(L_20, L_23, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xA4, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		Object_t * L_24 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(157)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_JUMP_TBL(0xA4, IL_00a4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00a4:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::LeaveRoom()
extern "C" void NativeRealtimeMultiplayerClient_LeaveRoom_m2486 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		RoomSession_LeaveRoom_m2336(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessageToAll(System.Boolean,System.Byte[])
extern "C" void NativeRealtimeMultiplayerClient_SendMessageToAll_m2487 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		bool L_1 = ___reliable;
		ByteU5BU5D_t350* L_2 = ___data;
		NullCheck(L_0);
		RoomSession_SendMessageToAll_m2344(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessageToAll(System.Boolean,System.Byte[],System.Int32,System.Int32)
extern "C" void NativeRealtimeMultiplayerClient_SendMessageToAll_m2488 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		bool L_1 = ___reliable;
		ByteU5BU5D_t350* L_2 = ___data;
		int32_t L_3 = ___offset;
		int32_t L_4 = ___length;
		NullCheck(L_0);
		RoomSession_SendMessageToAll_m2345(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessage(System.Boolean,System.String,System.Byte[])
extern "C" void NativeRealtimeMultiplayerClient_SendMessage_m2489 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		bool L_1 = ___reliable;
		String_t* L_2 = ___participantId;
		ByteU5BU5D_t350* L_3 = ___data;
		NullCheck(L_0);
		RoomSession_SendMessage_m2346(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessage(System.Boolean,System.String,System.Byte[],System.Int32,System.Int32)
extern "C" void NativeRealtimeMultiplayerClient_SendMessage_m2490 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		bool L_1 = ___reliable;
		String_t* L_2 = ___participantId;
		ByteU5BU5D_t350* L_3 = ___data;
		int32_t L_4 = ___offset;
		int32_t L_5 = ___length;
		NullCheck(L_0);
		RoomSession_SendMessage_m2347(L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetConnectedParticipants()
extern "C" List_1_t351 * NativeRealtimeMultiplayerClient_GetConnectedParticipants_m2491 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		List_1_t351 * L_1 = RoomSession_GetConnectedParticipants_m2348(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetSelf()
extern "C" Participant_t340 * NativeRealtimeMultiplayerClient_GetSelf_m2492 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		Participant_t340 * L_1 = (Participant_t340 *)VirtFuncInvoker0< Participant_t340 * >::Invoke(4 /* GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetSelf() */, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetParticipant(System.String)
extern "C" Participant_t340 * NativeRealtimeMultiplayerClient_GetParticipant_m2493 (NativeRealtimeMultiplayerClient_t536 * __this, String_t* ___participantId, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = ___participantId;
		NullCheck(L_0);
		Participant_t340 * L_2 = (Participant_t340 *)VirtFuncInvoker1< Participant_t340 *, String_t* >::Invoke(5 /* GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetParticipant(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient::IsRoomConnected()
extern "C" bool NativeRealtimeMultiplayerClient_IsRoomConnected_m2494 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method)
{
	{
		RoomSession_t566 * L_0 = (__this->___mCurrentSession_3);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::IsRoomConnected() */, L_0);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::DeclineInvitation(System.String)
extern TypeInfo* U3CDeclineInvitationU3Ec__AnonStorey3A_t609_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t888_il2cpp_TypeInfo_var;
extern MethodInfo* U3CDeclineInvitationU3Ec__AnonStorey3A_U3CU3Em__2B_m2476_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3842_MethodInfo_var;
extern "C" void NativeRealtimeMultiplayerClient_DeclineInvitation_m2495 (NativeRealtimeMultiplayerClient_t536 * __this, String_t* ___invitationId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDeclineInvitationU3Ec__AnonStorey3A_t609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(803);
		Action_1_t888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(802);
		U3CDeclineInvitationU3Ec__AnonStorey3A_U3CU3Em__2B_m2476_MethodInfo_var = il2cpp_codegen_method_info_from_index(342);
		Action_1__ctor_m3842_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483989);
		s_Il2CppMethodIntialized = true;
	}
	U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * V_0 = {0};
	{
		U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * L_0 = (U3CDeclineInvitationU3Ec__AnonStorey3A_t609 *)il2cpp_codegen_object_new (U3CDeclineInvitationU3Ec__AnonStorey3A_t609_il2cpp_TypeInfo_var);
		U3CDeclineInvitationU3Ec__AnonStorey3A__ctor_m2475(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * L_1 = V_0;
		String_t* L_2 = ___invitationId;
		NullCheck(L_1);
		L_1->___invitationId_0 = L_2;
		U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		RealtimeManager_t564 * L_4 = (__this->___mRealtimeManager_2);
		U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * L_5 = V_0;
		IntPtr_t L_6 = { U3CDeclineInvitationU3Ec__AnonStorey3A_U3CU3Em__2B_m2476_MethodInfo_var };
		Action_1_t888 * L_7 = (Action_1_t888 *)il2cpp_codegen_object_new (Action_1_t888_il2cpp_TypeInfo_var);
		Action_1__ctor_m3842(L_7, L_5, L_6, /*hidden argument*/Action_1__ctor_m3842_MethodInfo_var);
		NullCheck(L_4);
		RealtimeManager_FetchInvitations_m2986(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClieMethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_4.h"
// GooglePlayGames.Native.NativeSnapshotMetadata
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSnapshotMetad.h"
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
#include "System_Core_System_Action_2_gen_9.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa.h"
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSnapshotMetad_0.h"
// GooglePlayGames.Native.NativeSnapshotMetadataChange
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSnapshotMetad_1.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_1.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>
#include "mscorlib_System_Action_1_gen_37.h"
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
#include "System_Core_System_Action_2_gen_9MethodDeclarations.h"
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSnapshotMetad_0MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>
#include "mscorlib_System_Action_1_gen_37MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_4MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_1MethodDeclarations.h"
// GooglePlayGames.Native.NativeSavedGameClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_10MethodDeclarations.h"
struct Misc_t391;
struct SnapshotManager_t610;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.SnapshotManager>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.SnapshotManager>(!!0)
#define Misc_CheckNotNull_TisSnapshotManager_t610_m3843(__this /* static, unused */, p0, method) (( SnapshotManager_t610 * (*) (Object_t * /* static, unused */, SnapshotManager_t610 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct String_t;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(!!0)
#define Misc_CheckNotNull_TisString_t_m3705(__this /* static, unused */, p0, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct NativeSnapshotMetadata_t611;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeSnapshotMetadata>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeSnapshotMetadata>(!!0)
#define Misc_CheckNotNull_TisNativeSnapshotMetadata_t611_m3844(__this /* static, unused */, p0, method) (( NativeSnapshotMetadata_t611 * (*) (Object_t * /* static, unused */, NativeSnapshotMetadata_t611 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t612;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t612_m3845(__this /* static, unused */, p0, method) (( Action_2_t612 * (*) (Object_t * /* static, unused */, Action_2_t612 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::.ctor(GooglePlayGames.Native.PInvoke.SnapshotManager,System.String,GooglePlayGames.Native.NativeSnapshotMetadata,GooglePlayGames.Native.NativeSnapshotMetadata,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>,System.Action)
extern MethodInfo* Misc_CheckNotNull_TisSnapshotManager_t610_m3843_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisNativeSnapshotMetadata_t611_m3844_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_t588_m3833_MethodInfo_var;
extern "C" void NativeConflictResolver__ctor_m2496 (NativeConflictResolver_t613 * __this, SnapshotManager_t610 * ___manager, String_t* ___conflictId, NativeSnapshotMetadata_t611 * ___original, NativeSnapshotMetadata_t611 * ___unmerged, Action_2_t612 * ___completeCallback, Action_t588 * ___retryOpen, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisSnapshotManager_t610_m3843_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483991);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisNativeSnapshotMetadata_t611_m3844_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483992);
		Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		Misc_CheckNotNull_TisAction_t588_m3833_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		SnapshotManager_t610 * L_0 = ___manager;
		SnapshotManager_t610 * L_1 = Misc_CheckNotNull_TisSnapshotManager_t610_m3843(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisSnapshotManager_t610_m3843_MethodInfo_var);
		__this->___mManager_0 = L_1;
		String_t* L_2 = ___conflictId;
		String_t* L_3 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mConflictId_1 = L_3;
		NativeSnapshotMetadata_t611 * L_4 = ___original;
		NativeSnapshotMetadata_t611 * L_5 = Misc_CheckNotNull_TisNativeSnapshotMetadata_t611_m3844(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadata_t611_m3844_MethodInfo_var);
		__this->___mOriginal_2 = L_5;
		NativeSnapshotMetadata_t611 * L_6 = ___unmerged;
		NativeSnapshotMetadata_t611 * L_7 = Misc_CheckNotNull_TisNativeSnapshotMetadata_t611_m3844(NULL /*static, unused*/, L_6, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadata_t611_m3844_MethodInfo_var);
		__this->___mUnmerged_3 = L_7;
		Action_2_t612 * L_8 = ___completeCallback;
		Action_2_t612 * L_9 = Misc_CheckNotNull_TisAction_2_t612_m3845(NULL /*static, unused*/, L_8, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var);
		__this->___mCompleteCallback_4 = L_9;
		Action_t588 * L_10 = ___retryOpen;
		Action_t588 * L_11 = Misc_CheckNotNull_TisAction_t588_m3833(NULL /*static, unused*/, L_10, /*hidden argument*/Misc_CheckNotNull_TisAction_t588_m3833_MethodInfo_var);
		__this->___mRetryFileOpen_5 = L_11;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::ChooseMetadata(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern TypeInfo* NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Builder_t678_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t896_il2cpp_TypeInfo_var;
extern MethodInfo* NativeConflictResolver_U3CChooseMetadataU3Em__54_m2498_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3846_MethodInfo_var;
extern "C" void NativeConflictResolver_ChooseMetadata_m2497 (NativeConflictResolver_t613 * __this, Object_t * ___chosenMetadata, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(804);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Builder_t678_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(809);
		Action_1_t896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(810);
		NativeConflictResolver_U3CChooseMetadataU3Em__54_m2498_MethodInfo_var = il2cpp_codegen_method_info_from_index(346);
		Action_1__ctor_m3846_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483995);
		s_Il2CppMethodIntialized = true;
	}
	NativeSnapshotMetadata_t611 * V_0 = {0};
	{
		Object_t * L_0 = ___chosenMetadata;
		V_0 = ((NativeSnapshotMetadata_t611 *)IsInst(L_0, NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var));
		NativeSnapshotMetadata_t611 * L_1 = V_0;
		NativeSnapshotMetadata_t611 * L_2 = (__this->___mOriginal_2);
		if ((((Object_t*)(NativeSnapshotMetadata_t611 *)L_1) == ((Object_t*)(NativeSnapshotMetadata_t611 *)L_2)))
		{
			goto IL_0038;
		}
	}
	{
		NativeSnapshotMetadata_t611 * L_3 = V_0;
		NativeSnapshotMetadata_t611 * L_4 = (__this->___mUnmerged_3);
		if ((((Object_t*)(NativeSnapshotMetadata_t611 *)L_3) == ((Object_t*)(NativeSnapshotMetadata_t611 *)L_4)))
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral534, /*hidden argument*/NULL);
		Action_2_t612 * L_5 = (__this->___mCompleteCallback_4);
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_5, ((int32_t)-4), (Object_t *)NULL);
		return;
	}

IL_0038:
	{
		SnapshotManager_t610 * L_6 = (__this->___mManager_0);
		NativeSnapshotMetadata_t611 * L_7 = V_0;
		Builder_t678 * L_8 = (Builder_t678 *)il2cpp_codegen_object_new (Builder_t678_il2cpp_TypeInfo_var);
		Builder__ctor_m2817(L_8, /*hidden argument*/NULL);
		NullCheck(L_8);
		NativeSnapshotMetadataChange_t679 * L_9 = Builder_Build_m2822(L_8, /*hidden argument*/NULL);
		String_t* L_10 = (__this->___mConflictId_1);
		IntPtr_t L_11 = { NativeConflictResolver_U3CChooseMetadataU3Em__54_m2498_MethodInfo_var };
		Action_1_t896 * L_12 = (Action_1_t896 *)il2cpp_codegen_object_new (Action_1_t896_il2cpp_TypeInfo_var);
		Action_1__ctor_m3846(L_12, __this, L_11, /*hidden argument*/Action_1__ctor_m3846_MethodInfo_var);
		NullCheck(L_6);
		SnapshotManager_Resolve_m3054(L_6, L_7, L_9, L_10, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::<ChooseMetadata>m__54(GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern "C" void NativeConflictResolver_U3CChooseMetadataU3Em__54_m2498 (NativeConflictResolver_t613 * __this, CommitResponse_t703 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	{
		CommitResponse_t703 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = CommitResponse_RequestSucceeded_m3029(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		Action_2_t612 * L_2 = (__this->___mCompleteCallback_4);
		CommitResponse_t703 * L_3 = ___response;
		NullCheck(L_3);
		int32_t L_4 = CommitResponse_ResponseStatus_m3028(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		int32_t L_5 = NativeSavedGameClient_AsRequestStatus_m2539(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_2, L_5, (Object_t *)NULL);
		return;
	}

IL_0023:
	{
		Action_t588 * L_6 = (__this->___mRetryFileOpen_5);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_6);
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/Prefetcher
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/Prefetcher
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_0MethodDeclarations.h"

// System.Action`2<System.Byte[],System.Byte[]>
#include "System_Core_System_Action_2_gen_10.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_2.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_2MethodDeclarations.h"
// System.Action`2<System.Byte[],System.Byte[]>
#include "System_Core_System_Action_2_gen_10MethodDeclarations.h"
struct Misc_t391;
struct Action_2_t614;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<System.Byte[],System.Byte[]>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<System.Byte[],System.Byte[]>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t614_m3847(__this /* static, unused */, p0, method) (( Action_2_t614 * (*) (Object_t * /* static, unused */, Action_2_t614 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::.ctor(System.Action`2<System.Byte[],System.Byte[]>,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t614_m3847_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var;
extern "C" void Prefetcher__ctor_m2499 (Prefetcher_t615 * __this, Action_2_t614 * ___dataFetchedCallback, Action_2_t612 * ___completedCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Misc_CheckNotNull_TisAction_2_t614_m3847_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483996);
		Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m830(L_0, /*hidden argument*/NULL);
		__this->___mLock_0 = L_0;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		Action_2_t614 * L_1 = ___dataFetchedCallback;
		Action_2_t614 * L_2 = Misc_CheckNotNull_TisAction_2_t614_m3847(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t614_m3847_MethodInfo_var);
		__this->___mDataFetchedCallback_6 = L_2;
		Action_2_t612 * L_3 = ___completedCallback;
		Action_2_t612 * L_4 = Misc_CheckNotNull_TisAction_2_t612_m3845(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var);
		__this->___completedCallback_5 = L_4;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::OnOriginalDataRead(GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* Prefetcher_t615_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t612_il2cpp_TypeInfo_var;
extern MethodInfo* Prefetcher_U3COnOriginalDataReadU3Em__55_m2503_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3848_MethodInfo_var;
extern "C" void Prefetcher_OnOriginalDataRead_m2500 (Prefetcher_t615 * __this, ReadResponse_t704 * ___readResponse, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		Prefetcher_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(812);
		Action_2_t612_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(805);
		Prefetcher_U3COnOriginalDataReadU3Em__55_m2503_MethodInfo_var = il2cpp_codegen_method_info_from_index(349);
		Action_2__ctor_m3848_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483998);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Prefetcher_t615 * G_B4_0 = {0};
	Prefetcher_t615 * G_B3_0 = {0};
	{
		Object_t * L_0 = (__this->___mLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			ReadResponse_t704 * L_2 = ___readResponse;
			NullCheck(L_2);
			bool L_3 = ReadResponse_RequestSucceeded_m3035(L_2, /*hidden argument*/NULL);
			if (L_3)
			{
				goto IL_0061;
			}
		}

IL_0018:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral535, /*hidden argument*/NULL);
			Action_2_t612 * L_4 = (__this->___completedCallback_5);
			ReadResponse_t704 * L_5 = ___readResponse;
			NullCheck(L_5);
			int32_t L_6 = ReadResponse_ResponseStatus_m3034(L_5, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
			int32_t L_7 = NativeSavedGameClient_AsRequestStatus_m2539(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			NullCheck(L_4);
			VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_4, L_7, (Object_t *)NULL);
			Action_2_t612 * L_8 = ((Prefetcher_t615_StaticFields*)Prefetcher_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
			G_B3_0 = __this;
			if (L_8)
			{
				G_B4_0 = __this;
				goto IL_0052;
			}
		}

IL_0041:
		{
			IntPtr_t L_9 = { Prefetcher_U3COnOriginalDataReadU3Em__55_m2503_MethodInfo_var };
			Action_2_t612 * L_10 = (Action_2_t612 *)il2cpp_codegen_object_new (Action_2_t612_il2cpp_TypeInfo_var);
			Action_2__ctor_m3848(L_10, NULL, L_9, /*hidden argument*/Action_2__ctor_m3848_MethodInfo_var);
			((Prefetcher_t615_StaticFields*)Prefetcher_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7 = L_10;
			G_B4_0 = G_B3_0;
		}

IL_0052:
		{
			Action_2_t612 * L_11 = ((Prefetcher_t615_StaticFields*)Prefetcher_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
			NullCheck(G_B4_0);
			G_B4_0->___completedCallback_5 = L_11;
			goto IL_0084;
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral536, /*hidden argument*/NULL);
			__this->___mOriginalDataFetched_1 = 1;
			ReadResponse_t704 * L_12 = ___readResponse;
			NullCheck(L_12);
			ByteU5BU5D_t350* L_13 = ReadResponse_Data_m3036(L_12, /*hidden argument*/NULL);
			__this->___mOriginalData_2 = L_13;
			Prefetcher_MaybeProceed_m2502(__this, /*hidden argument*/NULL);
		}

IL_0084:
		{
			IL2CPP_LEAVE(0x90, FINALLY_0089);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0089;
	}

FINALLY_0089:
	{ // begin finally (depth: 1)
		Object_t * L_14 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(137)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(137)
	{
		IL2CPP_JUMP_TBL(0x90, IL_0090)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0090:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::OnUnmergedDataRead(GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* Prefetcher_t615_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t612_il2cpp_TypeInfo_var;
extern MethodInfo* Prefetcher_U3COnUnmergedDataReadU3Em__56_m2504_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3848_MethodInfo_var;
extern "C" void Prefetcher_OnUnmergedDataRead_m2501 (Prefetcher_t615 * __this, ReadResponse_t704 * ___readResponse, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		Prefetcher_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(812);
		Action_2_t612_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(805);
		Prefetcher_U3COnUnmergedDataReadU3Em__56_m2504_MethodInfo_var = il2cpp_codegen_method_info_from_index(351);
		Action_2__ctor_m3848_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483998);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Prefetcher_t615 * G_B4_0 = {0};
	Prefetcher_t615 * G_B3_0 = {0};
	{
		Object_t * L_0 = (__this->___mLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			ReadResponse_t704 * L_2 = ___readResponse;
			NullCheck(L_2);
			bool L_3 = ReadResponse_RequestSucceeded_m3035(L_2, /*hidden argument*/NULL);
			if (L_3)
			{
				goto IL_0061;
			}
		}

IL_0018:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral537, /*hidden argument*/NULL);
			Action_2_t612 * L_4 = (__this->___completedCallback_5);
			ReadResponse_t704 * L_5 = ___readResponse;
			NullCheck(L_5);
			int32_t L_6 = ReadResponse_ResponseStatus_m3034(L_5, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
			int32_t L_7 = NativeSavedGameClient_AsRequestStatus_m2539(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			NullCheck(L_4);
			VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_4, L_7, (Object_t *)NULL);
			Action_2_t612 * L_8 = ((Prefetcher_t615_StaticFields*)Prefetcher_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
			G_B3_0 = __this;
			if (L_8)
			{
				G_B4_0 = __this;
				goto IL_0052;
			}
		}

IL_0041:
		{
			IntPtr_t L_9 = { Prefetcher_U3COnUnmergedDataReadU3Em__56_m2504_MethodInfo_var };
			Action_2_t612 * L_10 = (Action_2_t612 *)il2cpp_codegen_object_new (Action_2_t612_il2cpp_TypeInfo_var);
			Action_2__ctor_m3848(L_10, NULL, L_9, /*hidden argument*/Action_2__ctor_m3848_MethodInfo_var);
			((Prefetcher_t615_StaticFields*)Prefetcher_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8 = L_10;
			G_B4_0 = G_B3_0;
		}

IL_0052:
		{
			Action_2_t612 * L_11 = ((Prefetcher_t615_StaticFields*)Prefetcher_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
			NullCheck(G_B4_0);
			G_B4_0->___completedCallback_5 = L_11;
			goto IL_0084;
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral538, /*hidden argument*/NULL);
			__this->___mUnmergedDataFetched_3 = 1;
			ReadResponse_t704 * L_12 = ___readResponse;
			NullCheck(L_12);
			ByteU5BU5D_t350* L_13 = ReadResponse_Data_m3036(L_12, /*hidden argument*/NULL);
			__this->___mUnmergedData_4 = L_13;
			Prefetcher_MaybeProceed_m2502(__this, /*hidden argument*/NULL);
		}

IL_0084:
		{
			IL2CPP_LEAVE(0x90, FINALLY_0089);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0089;
	}

FINALLY_0089:
	{ // begin finally (depth: 1)
		Object_t * L_14 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(137)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(137)
	{
		IL2CPP_JUMP_TBL(0x90, IL_0090)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0090:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::MaybeProceed()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Prefetcher_MaybeProceed_m2502 (Prefetcher_t615 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mOriginalDataFetched_1);
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		bool L_1 = (__this->___mUnmergedDataFetched_3);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral539, /*hidden argument*/NULL);
		Action_2_t614 * L_2 = (__this->___mDataFetchedCallback_6);
		ByteU5BU5D_t350* L_3 = (__this->___mOriginalData_2);
		ByteU5BU5D_t350* L_4 = (__this->___mUnmergedData_4);
		NullCheck(L_2);
		VirtActionInvoker2< ByteU5BU5D_t350*, ByteU5BU5D_t350* >::Invoke(10 /* System.Void System.Action`2<System.Byte[],System.Byte[]>::Invoke(!0,!1) */, L_2, L_3, L_4);
		goto IL_0078;
	}

IL_003c:
	{
		ObjectU5BU5D_t208* L_5 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, (String_t*) &_stringLiteral540);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 0)) = (Object_t *)(String_t*) &_stringLiteral540;
		ObjectU5BU5D_t208* L_6 = L_5;
		bool L_7 = (__this->___mOriginalDataFetched_1);
		bool L_8 = L_7;
		Object_t * L_9 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1)) = (Object_t *)L_9;
		ObjectU5BU5D_t208* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, (String_t*) &_stringLiteral541);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 2)) = (Object_t *)(String_t*) &_stringLiteral541;
		ObjectU5BU5D_t208* L_11 = L_10;
		bool L_12 = (__this->___mUnmergedDataFetched_3);
		bool L_13 = L_12;
		Object_t * L_14 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 3)) = (Object_t *)L_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m976(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::<OnOriginalDataRead>m__55(GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern "C" void Prefetcher_U3COnOriginalDataReadU3Em__55_m2503 (Object_t * __this /* static, unused */, int32_t p0, Object_t * p1, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::<OnUnmergedDataRead>m__56(GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern "C" void Prefetcher_U3COnUnmergedDataReadU3Em__56_m2504 (Object_t * __this /* static, unused */, int32_t p0, Object_t * p1, MethodInfo* method)
{
	{
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_1MethodDeclarations.h"

// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42::.ctor()
extern "C" void U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42__ctor_m2505 (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42::<>m__48(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern TypeInfo* IConflictResolver_t617_il2cpp_TypeInfo_var;
extern TypeInfo* ISavedGameMetadata_t618_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t190_il2cpp_TypeInfo_var;
extern TypeInfo* ConflictResolutionStrategy_t373_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_U3CU3Em__48_m2506 (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IConflictResolver_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(813);
		ISavedGameMetadata_t618_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(807);
		TimeSpan_t190_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		ConflictResolutionStrategy_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(814);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = (__this->___resolutionStrategy_0);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002f;
		}
		if (L_1 == 1)
		{
			goto IL_001e;
		}
		if (L_1 == 2)
		{
			goto IL_0026;
		}
	}
	{
		goto IL_005b;
	}

IL_001e:
	{
		Object_t * L_2 = ___resolver;
		Object_t * L_3 = ___original;
		NullCheck(L_2);
		InterfaceActionInvoker1< Object_t * >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.SavedGame.IConflictResolver::ChooseMetadata(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata) */, IConflictResolver_t617_il2cpp_TypeInfo_var, L_2, L_3);
		return;
	}

IL_0026:
	{
		Object_t * L_4 = ___resolver;
		Object_t * L_5 = ___unmerged;
		NullCheck(L_4);
		InterfaceActionInvoker1< Object_t * >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.SavedGame.IConflictResolver::ChooseMetadata(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata) */, IConflictResolver_t617_il2cpp_TypeInfo_var, L_4, L_5);
		return;
	}

IL_002f:
	{
		Object_t * L_6 = ___original;
		NullCheck(L_6);
		TimeSpan_t190  L_7 = (TimeSpan_t190 )InterfaceFuncInvoker0< TimeSpan_t190  >::Invoke(4 /* System.TimeSpan GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_TotalTimePlayed() */, ISavedGameMetadata_t618_il2cpp_TypeInfo_var, L_6);
		Object_t * L_8 = ___unmerged;
		NullCheck(L_8);
		TimeSpan_t190  L_9 = (TimeSpan_t190 )InterfaceFuncInvoker0< TimeSpan_t190  >::Invoke(4 /* System.TimeSpan GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_TotalTimePlayed() */, ISavedGameMetadata_t618_il2cpp_TypeInfo_var, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t190_il2cpp_TypeInfo_var);
		bool L_10 = TimeSpan_op_GreaterThanOrEqual_m3849(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0052;
		}
	}
	{
		Object_t * L_11 = ___resolver;
		Object_t * L_12 = ___original;
		NullCheck(L_11);
		InterfaceActionInvoker1< Object_t * >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.SavedGame.IConflictResolver::ChooseMetadata(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata) */, IConflictResolver_t617_il2cpp_TypeInfo_var, L_11, L_12);
		goto IL_005a;
	}

IL_0052:
	{
		Object_t * L_13 = ___resolver;
		Object_t * L_14 = ___unmerged;
		NullCheck(L_13);
		InterfaceActionInvoker1< Object_t * >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.SavedGame.IConflictResolver::ChooseMetadata(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata) */, IConflictResolver_t617_il2cpp_TypeInfo_var, L_13, L_14);
	}

IL_005a:
	{
		return;
	}

IL_005b:
	{
		int32_t L_15 = (__this->___resolutionStrategy_0);
		int32_t L_16 = L_15;
		Object_t * L_17 = Box(ConflictResolutionStrategy_t373_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral542, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Action_2_t612 * L_19 = (__this->___callback_1);
		NullCheck(L_19);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_19, ((int32_t)-2), (Object_t *)NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_2MethodDeclarations.h"

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_3.h"
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic_0.h"
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic_0MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey44__ctor_m2507 (U3CToOnGameThreadU3Ec__AnonStorey44_t620 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::<>m__50()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey44_U3CU3Em__50_m2508 (U3CToOnGameThreadU3Ec__AnonStorey44_t620 * __this, MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey43_t619 * L_0 = (__this->___U3CU3Ef__refU2467_5);
		NullCheck(L_0);
		ConflictCallback_t621 * L_1 = (L_0->___conflictCallback_0);
		Object_t * L_2 = (__this->___resolver_0);
		Object_t * L_3 = (__this->___original_1);
		ByteU5BU5D_t350* L_4 = (__this->___originalData_2);
		Object_t * L_5 = (__this->___unmerged_3);
		ByteU5BU5D_t350* L_6 = (__this->___unmergedData_4);
		NullCheck(L_1);
		VirtActionInvoker5< Object_t *, Object_t *, ByteU5BU5D_t350*, Object_t *, ByteU5BU5D_t350* >::Invoke(10 /* System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[]) */, L_1, L_2, L_3, L_4, L_5, L_6);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey43__ctor_m2509 (U3CToOnGameThreadU3Ec__AnonStorey43_t619 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43::<>m__49(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern TypeInfo* U3CToOnGameThreadU3Ec__AnonStorey44_t620_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* U3CToOnGameThreadU3Ec__AnonStorey44_U3CU3Em__50_m2508_MethodInfo_var;
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey43_U3CU3Em__49_m2510 (U3CToOnGameThreadU3Ec__AnonStorey43_t619 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CToOnGameThreadU3Ec__AnonStorey44_t620_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(815);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		U3CToOnGameThreadU3Ec__AnonStorey44_U3CU3Em__50_m2508_MethodInfo_var = il2cpp_codegen_method_info_from_index(352);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey44_t620 * V_0 = {0};
	{
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey44_t620 *)il2cpp_codegen_object_new (U3CToOnGameThreadU3Ec__AnonStorey44_t620_il2cpp_TypeInfo_var);
		U3CToOnGameThreadU3Ec__AnonStorey44__ctor_m2507(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__refU2467_5 = __this;
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_2 = V_0;
		Object_t * L_3 = ___resolver;
		NullCheck(L_2);
		L_2->___resolver_0 = L_3;
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_4 = V_0;
		Object_t * L_5 = ___original;
		NullCheck(L_4);
		L_4->___original_1 = L_5;
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_6 = V_0;
		ByteU5BU5D_t350* L_7 = ___originalData;
		NullCheck(L_6);
		L_6->___originalData_2 = L_7;
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_8 = V_0;
		Object_t * L_9 = ___unmerged;
		NullCheck(L_8);
		L_8->___unmerged_3 = L_9;
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_10 = V_0;
		ByteU5BU5D_t350* L_11 = ___unmergedData;
		NullCheck(L_10);
		L_10->___unmergedData_4 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral543, /*hidden argument*/NULL);
		U3CToOnGameThreadU3Ec__AnonStorey44_t620 * L_12 = V_0;
		IntPtr_t L_13 = { U3CToOnGameThreadU3Ec__AnonStorey44_U3CU3Em__50_m2508_MethodInfo_var };
		Action_t588 * L_14 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_14, L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1603(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_4MethodDeclarations.h"

// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_5.h"
// GooglePlayGames.Native.NativeSavedGameClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_10.h"
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"


// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::.ctor()
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey46__ctor_m2511 (U3CInternalManualOpenU3Ec__AnonStorey46_t623 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::<>m__51()
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__51_m2512 (U3CInternalManualOpenU3Ec__AnonStorey46_t623 * __this, MethodInfo* method)
{
	{
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_0 = (__this->___U3CU3Ef__refU2469_3);
		NullCheck(L_0);
		NativeSavedGameClient_t624 * L_1 = (L_0->___U3CU3Ef__this_5);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_2 = (__this->___U3CU3Ef__refU2469_3);
		NullCheck(L_2);
		String_t* L_3 = (L_2->___filename_1);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_4 = (__this->___U3CU3Ef__refU2469_3);
		NullCheck(L_4);
		int32_t L_5 = (L_4->___source_2);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_6 = (__this->___U3CU3Ef__refU2469_3);
		NullCheck(L_6);
		bool L_7 = (L_6->___prefetchDataOnConflict_3);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_8 = (__this->___U3CU3Ef__refU2469_3);
		NullCheck(L_8);
		ConflictCallback_t621 * L_9 = (L_8->___conflictCallback_4);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_10 = (__this->___U3CU3Ef__refU2469_3);
		NullCheck(L_10);
		Action_2_t612 * L_11 = (L_10->___completedCallback_0);
		NullCheck(L_1);
		NativeSavedGameClient_InternalManualOpen_m2529(L_1, L_3, L_5, L_7, L_9, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::<>m__52(System.Byte[],System.Byte[])
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__52_m2513 (U3CInternalManualOpenU3Ec__AnonStorey46_t623 * __this, ByteU5BU5D_t350* ___originalData, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method)
{
	{
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_0 = (__this->___U3CU3Ef__refU2469_3);
		NullCheck(L_0);
		ConflictCallback_t621 * L_1 = (L_0->___conflictCallback_4);
		NativeConflictResolver_t613 * L_2 = (__this->___resolver_0);
		NativeSnapshotMetadata_t611 * L_3 = (__this->___original_1);
		ByteU5BU5D_t350* L_4 = ___originalData;
		NativeSnapshotMetadata_t611 * L_5 = (__this->___unmerged_2);
		ByteU5BU5D_t350* L_6 = ___unmergedData;
		NullCheck(L_1);
		VirtActionInvoker5< Object_t *, Object_t *, ByteU5BU5D_t350*, Object_t *, ByteU5BU5D_t350* >::Invoke(10 /* System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[]) */, L_1, L_2, L_3, L_4, L_5, L_6);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_5MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_6.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>
#include "mscorlib_System_Action_1_gen_38.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotManMethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>
#include "mscorlib_System_Action_1_gen_38MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::.ctor()
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey45__ctor_m2514 (U3CInternalManualOpenU3Ec__AnonStorey45_t622 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::<>m__4A(GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* U3CInternalManualOpenU3Ec__AnonStorey46_t623_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t588_il2cpp_TypeInfo_var;
extern TypeInfo* NativeConflictResolver_t613_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t614_il2cpp_TypeInfo_var;
extern TypeInfo* Prefetcher_t615_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t897_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__51_m2512_MethodInfo_var;
extern MethodInfo* U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__52_m2513_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3850_MethodInfo_var;
extern MethodInfo* Prefetcher_OnOriginalDataRead_m2500_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3851_MethodInfo_var;
extern MethodInfo* Prefetcher_OnUnmergedDataRead_m2501_MethodInfo_var;
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey45_U3CU3Em__4A_m2515 (U3CInternalManualOpenU3Ec__AnonStorey45_t622 * __this, OpenResponse_t701 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(817);
		Action_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		NativeConflictResolver_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(818);
		Action_2_t614_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(811);
		Prefetcher_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(812);
		Action_1_t897_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(819);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__51_m2512_MethodInfo_var = il2cpp_codegen_method_info_from_index(353);
		U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__52_m2513_MethodInfo_var = il2cpp_codegen_method_info_from_index(354);
		Action_2__ctor_m3850_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484003);
		Prefetcher_OnOriginalDataRead_m2500_MethodInfo_var = il2cpp_codegen_method_info_from_index(356);
		Action_1__ctor_m3851_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484005);
		Prefetcher_OnUnmergedDataRead_m2501_MethodInfo_var = il2cpp_codegen_method_info_from_index(358);
		s_Il2CppMethodIntialized = true;
	}
	Prefetcher_t615 * V_0 = {0};
	U3CInternalManualOpenU3Ec__AnonStorey46_t623 * V_1 = {0};
	{
		OpenResponse_t701 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = OpenResponse_RequestSucceeded_m3011(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Action_2_t612 * L_2 = (__this->___completedCallback_0);
		OpenResponse_t701 * L_3 = ___response;
		NullCheck(L_3);
		int32_t L_4 = OpenResponse_ResponseStatus_m3012(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		int32_t L_5 = NativeSavedGameClient_AsRequestStatus_m2537(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_2, L_5, (Object_t *)NULL);
		goto IL_0159;
	}

IL_0027:
	{
		OpenResponse_t701 * L_6 = ___response;
		NullCheck(L_6);
		int32_t L_7 = OpenResponse_ResponseStatus_m3012(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_004a;
		}
	}
	{
		Action_2_t612 * L_8 = (__this->___completedCallback_0);
		OpenResponse_t701 * L_9 = ___response;
		NullCheck(L_9);
		NativeSnapshotMetadata_t611 * L_10 = OpenResponse_Data_m3014(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_8, 1, L_10);
		goto IL_0159;
	}

IL_004a:
	{
		OpenResponse_t701 * L_11 = ___response;
		NullCheck(L_11);
		int32_t L_12 = OpenResponse_ResponseStatus_m3012(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_0141;
		}
	}
	{
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_13 = (U3CInternalManualOpenU3Ec__AnonStorey46_t623 *)il2cpp_codegen_object_new (U3CInternalManualOpenU3Ec__AnonStorey46_t623_il2cpp_TypeInfo_var);
		U3CInternalManualOpenU3Ec__AnonStorey46__ctor_m2511(L_13, /*hidden argument*/NULL);
		V_1 = L_13;
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_14 = V_1;
		NullCheck(L_14);
		L_14->___U3CU3Ef__refU2469_3 = __this;
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_15 = V_1;
		OpenResponse_t701 * L_16 = ___response;
		NullCheck(L_16);
		NativeSnapshotMetadata_t611 * L_17 = OpenResponse_ConflictOriginal_m3015(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->___original_1 = L_17;
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_18 = V_1;
		OpenResponse_t701 * L_19 = ___response;
		NullCheck(L_19);
		NativeSnapshotMetadata_t611 * L_20 = OpenResponse_ConflictUnmerged_m3016(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		L_18->___unmerged_2 = L_20;
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_21 = V_1;
		NativeSavedGameClient_t624 * L_22 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_22);
		SnapshotManager_t610 * L_23 = (L_22->___mSnapshotManager_1);
		OpenResponse_t701 * L_24 = ___response;
		NullCheck(L_24);
		String_t* L_25 = OpenResponse_ConflictId_m3013(L_24, /*hidden argument*/NULL);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_26 = V_1;
		NullCheck(L_26);
		NativeSnapshotMetadata_t611 * L_27 = (L_26->___original_1);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_28 = V_1;
		NullCheck(L_28);
		NativeSnapshotMetadata_t611 * L_29 = (L_28->___unmerged_2);
		Action_2_t612 * L_30 = (__this->___completedCallback_0);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_31 = V_1;
		IntPtr_t L_32 = { U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__51_m2512_MethodInfo_var };
		Action_t588 * L_33 = (Action_t588 *)il2cpp_codegen_object_new (Action_t588_il2cpp_TypeInfo_var);
		Action__ctor_m3796(L_33, L_31, L_32, /*hidden argument*/NULL);
		NativeConflictResolver_t613 * L_34 = (NativeConflictResolver_t613 *)il2cpp_codegen_object_new (NativeConflictResolver_t613_il2cpp_TypeInfo_var);
		NativeConflictResolver__ctor_m2496(L_34, L_23, L_25, L_27, L_29, L_30, L_33, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->___resolver_0 = L_34;
		bool L_35 = (__this->___prefetchDataOnConflict_3);
		if (L_35)
		{
			goto IL_00e0;
		}
	}
	{
		ConflictCallback_t621 * L_36 = (__this->___conflictCallback_4);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_37 = V_1;
		NullCheck(L_37);
		NativeConflictResolver_t613 * L_38 = (L_37->___resolver_0);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_39 = V_1;
		NullCheck(L_39);
		NativeSnapshotMetadata_t611 * L_40 = (L_39->___original_1);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_41 = V_1;
		NullCheck(L_41);
		NativeSnapshotMetadata_t611 * L_42 = (L_41->___unmerged_2);
		NullCheck(L_36);
		VirtActionInvoker5< Object_t *, Object_t *, ByteU5BU5D_t350*, Object_t *, ByteU5BU5D_t350* >::Invoke(10 /* System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[]) */, L_36, L_38, L_40, (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL, L_42, (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL);
		return;
	}

IL_00e0:
	{
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_43 = V_1;
		IntPtr_t L_44 = { U3CInternalManualOpenU3Ec__AnonStorey46_U3CU3Em__52_m2513_MethodInfo_var };
		Action_2_t614 * L_45 = (Action_2_t614 *)il2cpp_codegen_object_new (Action_2_t614_il2cpp_TypeInfo_var);
		Action_2__ctor_m3850(L_45, L_43, L_44, /*hidden argument*/Action_2__ctor_m3850_MethodInfo_var);
		Action_2_t612 * L_46 = (__this->___completedCallback_0);
		Prefetcher_t615 * L_47 = (Prefetcher_t615 *)il2cpp_codegen_object_new (Prefetcher_t615_il2cpp_TypeInfo_var);
		Prefetcher__ctor_m2499(L_47, L_45, L_46, /*hidden argument*/NULL);
		V_0 = L_47;
		NativeSavedGameClient_t624 * L_48 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_48);
		SnapshotManager_t610 * L_49 = (L_48->___mSnapshotManager_1);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_50 = V_1;
		NullCheck(L_50);
		NativeSnapshotMetadata_t611 * L_51 = (L_50->___original_1);
		Prefetcher_t615 * L_52 = V_0;
		IntPtr_t L_53 = { Prefetcher_OnOriginalDataRead_m2500_MethodInfo_var };
		Action_1_t897 * L_54 = (Action_1_t897 *)il2cpp_codegen_object_new (Action_1_t897_il2cpp_TypeInfo_var);
		Action_1__ctor_m3851(L_54, L_52, L_53, /*hidden argument*/Action_1__ctor_m3851_MethodInfo_var);
		NullCheck(L_49);
		SnapshotManager_Read_m3057(L_49, L_51, L_54, /*hidden argument*/NULL);
		NativeSavedGameClient_t624 * L_55 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_55);
		SnapshotManager_t610 * L_56 = (L_55->___mSnapshotManager_1);
		U3CInternalManualOpenU3Ec__AnonStorey46_t623 * L_57 = V_1;
		NullCheck(L_57);
		NativeSnapshotMetadata_t611 * L_58 = (L_57->___unmerged_2);
		Prefetcher_t615 * L_59 = V_0;
		IntPtr_t L_60 = { Prefetcher_OnUnmergedDataRead_m2501_MethodInfo_var };
		Action_1_t897 * L_61 = (Action_1_t897 *)il2cpp_codegen_object_new (Action_1_t897_il2cpp_TypeInfo_var);
		Action_1__ctor_m3851(L_61, L_59, L_60, /*hidden argument*/Action_1__ctor_m3851_MethodInfo_var);
		NullCheck(L_56);
		SnapshotManager_Read_m3057(L_56, L_58, L_61, /*hidden argument*/NULL);
		goto IL_0159;
	}

IL_0141:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral544, /*hidden argument*/NULL);
		Action_2_t612 * L_62 = (__this->___completedCallback_0);
		NullCheck(L_62);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_62, ((int32_t)-2), (Object_t *)NULL);
	}

IL_0159:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey47
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_6.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey47
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_6MethodDeclarations.h"

// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>
#include "System_Core_System_Action_2_gen_11.h"
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>
#include "System_Core_System_Action_2_gen_11MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey47::.ctor()
extern "C" void U3CReadBinaryDataU3Ec__AnonStorey47__ctor_m2516 (U3CReadBinaryDataU3Ec__AnonStorey47_t626 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey47::<>m__4B(GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern "C" void U3CReadBinaryDataU3Ec__AnonStorey47_U3CU3Em__4B_m2517 (U3CReadBinaryDataU3Ec__AnonStorey47_t626 * __this, ReadResponse_t704 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReadResponse_t704 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = ReadResponse_RequestSucceeded_m3035(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Action_2_t625 * L_2 = (__this->___completedCallback_0);
		ReadResponse_t704 * L_3 = ___response;
		NullCheck(L_3);
		int32_t L_4 = ReadResponse_ResponseStatus_m3034(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		int32_t L_5 = NativeSavedGameClient_AsRequestStatus_m2539(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, ByteU5BU5D_t350* >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::Invoke(!0,!1) */, L_2, L_5, (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL);
		goto IL_0039;
	}

IL_0027:
	{
		Action_2_t625 * L_6 = (__this->___completedCallback_0);
		ReadResponse_t704 * L_7 = ___response;
		NullCheck(L_7);
		ByteU5BU5D_t350* L_8 = ReadResponse_Data_m3036(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker2< int32_t, ByteU5BU5D_t350* >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::Invoke(!0,!1) */, L_6, 1, L_8);
	}

IL_0039:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_7.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_7MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_3.h"
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
#include "System_Core_System_Action_2_gen_12.h"
// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SelectU.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_3MethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
#include "System_Core_System_Action_2_gen_12MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48::.ctor()
extern "C" void U3CShowSelectSavedGameUIU3Ec__AnonStorey48__ctor_m2518 (U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48::<>m__4C(GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern "C" void U3CShowSelectSavedGameUIU3Ec__AnonStorey48_U3CU3Em__4C_m2519 (U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * __this, SnapshotSelectUIResponse_t705 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = {0};
	Action_2_t627 * G_B2_1 = {0};
	int32_t G_B1_0 = {0};
	Action_2_t627 * G_B1_1 = {0};
	NativeSnapshotMetadata_t611 * G_B3_0 = {0};
	int32_t G_B3_1 = {0};
	Action_2_t627 * G_B3_2 = {0};
	{
		Action_2_t627 * L_0 = (__this->___callback_0);
		SnapshotSelectUIResponse_t705 * L_1 = ___response;
		NullCheck(L_1);
		int32_t L_2 = SnapshotSelectUIResponse_RequestStatus_m3041(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		int32_t L_3 = NativeSavedGameClient_AsUIStatus_m2540(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		SnapshotSelectUIResponse_t705 * L_4 = ___response;
		NullCheck(L_4);
		bool L_5 = SnapshotSelectUIResponse_RequestSucceeded_m3042(L_4, /*hidden argument*/NULL);
		G_B1_0 = L_3;
		G_B1_1 = L_0;
		if (!L_5)
		{
			G_B2_0 = L_3;
			G_B2_1 = L_0;
			goto IL_0027;
		}
	}
	{
		SnapshotSelectUIResponse_t705 * L_6 = ___response;
		NullCheck(L_6);
		NativeSnapshotMetadata_t611 * L_7 = SnapshotSelectUIResponse_Data_m3043(L_6, /*hidden argument*/NULL);
		G_B3_0 = L_7;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = ((NativeSnapshotMetadata_t611 *)(NULL));
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0028:
	{
		NullCheck(G_B3_2);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, G_B3_2, G_B3_1, G_B3_0);
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_8.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_8MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49::.ctor()
extern "C" void U3CCommitUpdateU3Ec__AnonStorey49__ctor_m2520 (U3CCommitUpdateU3Ec__AnonStorey49_t629 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49::<>m__4D(GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern "C" void U3CCommitUpdateU3Ec__AnonStorey49_U3CU3Em__4D_m2521 (U3CCommitUpdateU3Ec__AnonStorey49_t629 * __this, CommitResponse_t703 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	{
		CommitResponse_t703 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = CommitResponse_RequestSucceeded_m3029(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Action_2_t612 * L_2 = (__this->___callback_0);
		CommitResponse_t703 * L_3 = ___response;
		NullCheck(L_3);
		int32_t L_4 = CommitResponse_ResponseStatus_m3028(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		int32_t L_5 = NativeSavedGameClient_AsRequestStatus_m2539(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_2, L_5, (Object_t *)NULL);
		goto IL_0039;
	}

IL_0027:
	{
		Action_2_t612 * L_6 = (__this->___callback_0);
		CommitResponse_t703 * L_7 = ___response;
		NullCheck(L_7);
		NativeSnapshotMetadata_t611 * L_8 = CommitResponse_Data_m3030(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_6, 1, L_8);
	}

IL_0039:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_9.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_9MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_0.h"
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>
#include "System_Core_System_Action_2_gen_13.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
#include "mscorlib_System_Collections_Generic_List_1_gen_20.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_0MethodDeclarations.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>
#include "System_Core_System_Action_2_gen_13MethodDeclarations.h"
struct Enumerable_t219;
struct IEnumerable_1_t930;
struct IEnumerable_t38;
struct Enumerable_t219;
struct IEnumerable_1_t221;
struct IEnumerable_t38;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C" Object_t* Enumerable_Cast_TisObject_t_m3761_gshared (Object_t * __this /* static, unused */, Object_t * p0, MethodInfo* method);
#define Enumerable_Cast_TisObject_t_m3761(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Enumerable_Cast_TisObject_t_m3761_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisISavedGameMetadata_t618_m3852(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Enumerable_Cast_TisObject_t_m3761_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t219;
struct List_1_t931;
struct IEnumerable_1_t930;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisISavedGameMetadata_t618_m3853(__this /* static, unused */, p0, method) (( List_1_t931 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A::.ctor()
extern "C" void U3CFetchAllSavedGamesU3Ec__AnonStorey4A__ctor_m2522 (U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A::<>m__4E(GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t931_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3854_MethodInfo_var;
extern MethodInfo* Enumerable_Cast_TisISavedGameMetadata_t618_m3852_MethodInfo_var;
extern MethodInfo* Enumerable_ToList_TisISavedGameMetadata_t618_m3853_MethodInfo_var;
extern "C" void U3CFetchAllSavedGamesU3Ec__AnonStorey4A_U3CU3Em__4E_m2523 (U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * __this, FetchAllResponse_t702 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		List_1_t931_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(820);
		List_1__ctor_m3854_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484007);
		Enumerable_Cast_TisISavedGameMetadata_t618_m3852_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484008);
		Enumerable_ToList_TisISavedGameMetadata_t618_m3853_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484009);
		s_Il2CppMethodIntialized = true;
	}
	{
		FetchAllResponse_t702 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = FetchAllResponse_RequestSucceeded_m3022(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		Action_2_t630 * L_2 = (__this->___callback_0);
		FetchAllResponse_t702 * L_3 = ___response;
		NullCheck(L_3);
		int32_t L_4 = FetchAllResponse_ResponseStatus_m3021(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		int32_t L_5 = NativeSavedGameClient_AsRequestStatus_m2539(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t931_il2cpp_TypeInfo_var);
		List_1_t931 * L_6 = (List_1_t931 *)il2cpp_codegen_object_new (List_1_t931_il2cpp_TypeInfo_var);
		List_1__ctor_m3854(L_6, /*hidden argument*/List_1__ctor_m3854_MethodInfo_var);
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, List_1_t931 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>::Invoke(!0,!1) */, L_2, L_5, L_6);
		goto IL_0047;
	}

IL_002b:
	{
		Action_2_t630 * L_7 = (__this->___callback_0);
		FetchAllResponse_t702 * L_8 = ___response;
		NullCheck(L_8);
		Object_t* L_9 = FetchAllResponse_Data_m3023(L_8, /*hidden argument*/NULL);
		Object_t* L_10 = Enumerable_Cast_TisISavedGameMetadata_t618_m3852(NULL /*static, unused*/, L_9, /*hidden argument*/Enumerable_Cast_TisISavedGameMetadata_t618_m3852_MethodInfo_var);
		List_1_t931 * L_11 = Enumerable_ToList_TisISavedGameMetadata_t618_m3853(NULL /*static, unused*/, L_10, /*hidden argument*/Enumerable_ToList_TisISavedGameMetadata_t618_m3853_MethodInfo_var);
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, List_1_t931 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>::Invoke(!0,!1) */, L_7, 1, L_11);
	}

IL_0047:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_Regex.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>
#include "mscorlib_System_Action_1_gen_39.h"
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Snap.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>
#include "mscorlib_System_Action_1_gen_40.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_41.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>
#include "mscorlib_System_Action_1_gen_39MethodDeclarations.h"
// GooglePlayGames.Native.NativeSnapshotMetadata
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSnapshotMetadMethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>
#include "mscorlib_System_Action_1_gen_40MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_41MethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1MethodDeclarations.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_genMethodDeclarations.h"
struct NativeSavedGameClient_t624;
struct Action_2_t612;
struct NativeSavedGameClient_t624;
struct Action_2_t916;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<System.Int32,System.Object>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<System.Int32,System.Object>(System.Action`2<!!0,!!1>)
extern "C" Action_2_t916 * NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared (Object_t * __this /* static, unused */, Action_2_t916 * p0, MethodInfo* method);
#define NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856(__this /* static, unused */, p0, method) (( Action_2_t916 * (*) (Object_t * /* static, unused */, Action_2_t916 *, MethodInfo*))NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Action`2<!!0,!!1>)
#define NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855(__this /* static, unused */, p0, method) (( Action_2_t612 * (*) (Object_t * /* static, unused */, Action_2_t612 *, MethodInfo*))NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct ConflictCallback_t621;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.SavedGame.ConflictCallback>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.SavedGame.ConflictCallback>(!!0)
#define Misc_CheckNotNull_TisConflictCallback_t621_m3857(__this /* static, unused */, p0, method) (( ConflictCallback_t621 * (*) (Object_t * /* static, unused */, ConflictCallback_t621 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct ISavedGameMetadata_t618;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(!!0)
#define Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t625;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t625_m3859(__this /* static, unused */, p0, method) (( Action_2_t625 * (*) (Object_t * /* static, unused */, Action_2_t625 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct NativeSavedGameClient_t624;
struct Action_2_t625;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>(System.Action`2<!!0,!!1>)
#define NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisByteU5BU5D_t350_m3860(__this /* static, unused */, p0, method) (( Action_2_t625 * (*) (Object_t * /* static, unused */, Action_2_t625 *, MethodInfo*))NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t627;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t627_m3861(__this /* static, unused */, p0, method) (( Action_2_t627 * (*) (Object_t * /* static, unused */, Action_2_t627 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct NativeSavedGameClient_t624;
struct Action_2_t627;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>(System.Action`2<!!0,!!1>)
#define NativeSavedGameClient_ToOnGameThread_TisSelectUIStatus_t375_TisISavedGameMetadata_t618_m3862(__this /* static, unused */, p0, method) (( Action_2_t627 * (*) (Object_t * /* static, unused */, Action_2_t627 *, MethodInfo*))NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct ByteU5BU5D_t350;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Byte[]>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Byte[]>(!!0)
#define Misc_CheckNotNull_TisByteU5BU5D_t350_m3707(__this /* static, unused */, p0, method) (( ByteU5BU5D_t350* (*) (Object_t * /* static, unused */, ByteU5BU5D_t350*, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t630;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t630_m3863(__this /* static, unused */, p0, method) (( Action_2_t630 * (*) (Object_t * /* static, unused */, Action_2_t630 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct NativeSavedGameClient_t624;
struct Action_2_t630;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>(System.Action`2<!!0,!!1>)
#define NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisList_1_t931_m3864(__this /* static, unused */, p0, method) (( Action_2_t630 * (*) (Object_t * /* static, unused */, Action_2_t630 *, MethodInfo*))NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeSavedGameClient::.ctor(GooglePlayGames.Native.PInvoke.SnapshotManager)
extern MethodInfo* Misc_CheckNotNull_TisSnapshotManager_t610_m3843_MethodInfo_var;
extern "C" void NativeSavedGameClient__ctor_m2524 (NativeSavedGameClient_t624 * __this, SnapshotManager_t610 * ___manager, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisSnapshotManager_t610_m3843_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483991);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		SnapshotManager_t610 * L_0 = ___manager;
		SnapshotManager_t610 * L_1 = Misc_CheckNotNull_TisSnapshotManager_t610_m3843(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisSnapshotManager_t610_m3843_MethodInfo_var);
		__this->___mSnapshotManager_1 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::.cctor()
extern TypeInfo* Regex_t200_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern "C" void NativeSavedGameClient__cctor_m2525 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Regex_t200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t200_il2cpp_TypeInfo_var);
		Regex_t200 * L_0 = (Regex_t200 *)il2cpp_codegen_object_new (Regex_t200_il2cpp_TypeInfo_var);
		Regex__ctor_m3865(L_0, (String_t*) &_stringLiteral525, /*hidden argument*/NULL);
		((NativeSavedGameClient_t624_StaticFields*)NativeSavedGameClient_t624_il2cpp_TypeInfo_var->static_fields)->___ValidFilenameRegex_0 = L_0;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::OpenWithAutomaticConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern TypeInfo* U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ConflictCallback_t621_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var;
extern MethodInfo* NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var;
extern MethodInfo* U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_U3CU3Em__48_m2506_MethodInfo_var;
extern "C" void NativeSavedGameClient_OpenWithAutomaticConflictResolution_m2526 (NativeSavedGameClient_t624 * __this, String_t* ___filename, int32_t ___source, int32_t ___resolutionStrategy, Action_2_t612 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(821);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ConflictCallback_t621_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(822);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484010);
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_U3CU3Em__48_m2506_MethodInfo_var = il2cpp_codegen_method_info_from_index(363);
		s_Il2CppMethodIntialized = true;
	}
	U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * V_0 = {0};
	{
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_0 = (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 *)il2cpp_codegen_object_new (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616_il2cpp_TypeInfo_var);
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42__ctor_m2505(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_1 = V_0;
		int32_t L_2 = ___resolutionStrategy;
		NullCheck(L_1);
		L_1->___resolutionStrategy_0 = L_2;
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_3 = V_0;
		Action_2_t612 * L_4 = ___callback;
		NullCheck(L_3);
		L_3->___callback_1 = L_4;
		String_t* L_5 = ___filename;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_6 = V_0;
		NullCheck(L_6);
		Action_2_t612 * L_7 = (L_6->___callback_1);
		Misc_CheckNotNull_TisAction_2_t612_m3845(NULL /*static, unused*/, L_7, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var);
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_8 = V_0;
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_9 = V_0;
		NullCheck(L_9);
		Action_2_t612 * L_10 = (L_9->___callback_1);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		Action_2_t612 * L_11 = NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855(NULL /*static, unused*/, L_10, /*hidden argument*/NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var);
		NullCheck(L_8);
		L_8->___callback_1 = L_11;
		String_t* L_12 = ___filename;
		bool L_13 = NativeSavedGameClient_IsValidFilename_m2535(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_14 = ___filename;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral526, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_16 = V_0;
		NullCheck(L_16);
		Action_2_t612 * L_17 = (L_16->___callback_1);
		NullCheck(L_17);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_17, ((int32_t)-4), (Object_t *)NULL);
		return;
	}

IL_0063:
	{
		String_t* L_18 = ___filename;
		int32_t L_19 = ___source;
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_20 = V_0;
		IntPtr_t L_21 = { U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_U3CU3Em__48_m2506_MethodInfo_var };
		ConflictCallback_t621 * L_22 = (ConflictCallback_t621 *)il2cpp_codegen_object_new (ConflictCallback_t621_il2cpp_TypeInfo_var);
		ConflictCallback__ctor_m3680(L_22, L_20, L_21, /*hidden argument*/NULL);
		U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * L_23 = V_0;
		NullCheck(L_23);
		Action_2_t612 * L_24 = (L_23->___callback_1);
		VirtActionInvoker5< String_t*, int32_t, bool, ConflictCallback_t621 *, Action_2_t612 * >::Invoke(5 /* System.Void GooglePlayGames.Native.NativeSavedGameClient::OpenWithManualConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>) */, __this, L_18, L_19, 0, L_22, L_24);
		return;
	}
}
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread(GooglePlayGames.BasicApi.SavedGame.ConflictCallback)
extern TypeInfo* U3CToOnGameThreadU3Ec__AnonStorey43_t619_il2cpp_TypeInfo_var;
extern TypeInfo* ConflictCallback_t621_il2cpp_TypeInfo_var;
extern MethodInfo* U3CToOnGameThreadU3Ec__AnonStorey43_U3CU3Em__49_m2510_MethodInfo_var;
extern "C" ConflictCallback_t621 * NativeSavedGameClient_ToOnGameThread_m2527 (NativeSavedGameClient_t624 * __this, ConflictCallback_t621 * ___conflictCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CToOnGameThreadU3Ec__AnonStorey43_t619_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(823);
		ConflictCallback_t621_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(822);
		U3CToOnGameThreadU3Ec__AnonStorey43_U3CU3Em__49_m2510_MethodInfo_var = il2cpp_codegen_method_info_from_index(364);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey43_t619 * V_0 = {0};
	{
		U3CToOnGameThreadU3Ec__AnonStorey43_t619 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey43_t619 *)il2cpp_codegen_object_new (U3CToOnGameThreadU3Ec__AnonStorey43_t619_il2cpp_TypeInfo_var);
		U3CToOnGameThreadU3Ec__AnonStorey43__ctor_m2509(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CToOnGameThreadU3Ec__AnonStorey43_t619 * L_1 = V_0;
		ConflictCallback_t621 * L_2 = ___conflictCallback;
		NullCheck(L_1);
		L_1->___conflictCallback_0 = L_2;
		U3CToOnGameThreadU3Ec__AnonStorey43_t619 * L_3 = V_0;
		IntPtr_t L_4 = { U3CToOnGameThreadU3Ec__AnonStorey43_U3CU3Em__49_m2510_MethodInfo_var };
		ConflictCallback_t621 * L_5 = (ConflictCallback_t621 *)il2cpp_codegen_object_new (ConflictCallback_t621_il2cpp_TypeInfo_var);
		ConflictCallback__ctor_m3680(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::OpenWithManualConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisConflictCallback_t621_m3857_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var;
extern MethodInfo* NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var;
extern "C" void NativeSavedGameClient_OpenWithManualConflictResolution_m2528 (NativeSavedGameClient_t624 * __this, String_t* ___filename, int32_t ___source, bool ___prefetchDataOnConflict, ConflictCallback_t621 * ___conflictCallback, Action_2_t612 * ___completedCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisConflictCallback_t621_m3857_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484013);
		Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484010);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___filename;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		ConflictCallback_t621 * L_1 = ___conflictCallback;
		Misc_CheckNotNull_TisConflictCallback_t621_m3857(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisConflictCallback_t621_m3857_MethodInfo_var);
		Action_2_t612 * L_2 = ___completedCallback;
		Misc_CheckNotNull_TisAction_2_t612_m3845(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var);
		ConflictCallback_t621 * L_3 = ___conflictCallback;
		ConflictCallback_t621 * L_4 = NativeSavedGameClient_ToOnGameThread_m2527(__this, L_3, /*hidden argument*/NULL);
		___conflictCallback = L_4;
		Action_2_t612 * L_5 = ___completedCallback;
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		Action_2_t612 * L_6 = NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855(NULL /*static, unused*/, L_5, /*hidden argument*/NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var);
		___completedCallback = L_6;
		String_t* L_7 = ___filename;
		bool L_8 = NativeSavedGameClient_IsValidFilename_m2535(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0050;
		}
	}
	{
		String_t* L_9 = ___filename;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral526, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Action_2_t612 * L_11 = ___completedCallback;
		NullCheck(L_11);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_11, ((int32_t)-4), (Object_t *)NULL);
		return;
	}

IL_0050:
	{
		String_t* L_12 = ___filename;
		int32_t L_13 = ___source;
		bool L_14 = ___prefetchDataOnConflict;
		ConflictCallback_t621 * L_15 = ___conflictCallback;
		Action_2_t612 * L_16 = ___completedCallback;
		NativeSavedGameClient_InternalManualOpen_m2529(__this, L_12, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::InternalManualOpen(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern TypeInfo* U3CInternalManualOpenU3Ec__AnonStorey45_t622_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t895_il2cpp_TypeInfo_var;
extern MethodInfo* U3CInternalManualOpenU3Ec__AnonStorey45_U3CU3Em__4A_m2515_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3866_MethodInfo_var;
extern "C" void NativeSavedGameClient_InternalManualOpen_m2529 (NativeSavedGameClient_t624 * __this, String_t* ___filename, int32_t ___source, bool ___prefetchDataOnConflict, ConflictCallback_t621 * ___conflictCallback, Action_2_t612 * ___completedCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CInternalManualOpenU3Ec__AnonStorey45_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(825);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		Action_1_t895_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(826);
		U3CInternalManualOpenU3Ec__AnonStorey45_U3CU3Em__4A_m2515_MethodInfo_var = il2cpp_codegen_method_info_from_index(366);
		Action_1__ctor_m3866_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484015);
		s_Il2CppMethodIntialized = true;
	}
	U3CInternalManualOpenU3Ec__AnonStorey45_t622 * V_0 = {0};
	{
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_0 = (U3CInternalManualOpenU3Ec__AnonStorey45_t622 *)il2cpp_codegen_object_new (U3CInternalManualOpenU3Ec__AnonStorey45_t622_il2cpp_TypeInfo_var);
		U3CInternalManualOpenU3Ec__AnonStorey45__ctor_m2514(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_1 = V_0;
		Action_2_t612 * L_2 = ___completedCallback;
		NullCheck(L_1);
		L_1->___completedCallback_0 = L_2;
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_3 = V_0;
		String_t* L_4 = ___filename;
		NullCheck(L_3);
		L_3->___filename_1 = L_4;
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_5 = V_0;
		int32_t L_6 = ___source;
		NullCheck(L_5);
		L_5->___source_2 = L_6;
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_7 = V_0;
		bool L_8 = ___prefetchDataOnConflict;
		NullCheck(L_7);
		L_7->___prefetchDataOnConflict_3 = L_8;
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_9 = V_0;
		ConflictCallback_t621 * L_10 = ___conflictCallback;
		NullCheck(L_9);
		L_9->___conflictCallback_4 = L_10;
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_11 = V_0;
		NullCheck(L_11);
		L_11->___U3CU3Ef__this_5 = __this;
		SnapshotManager_t610 * L_12 = (__this->___mSnapshotManager_1);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = (L_13->___filename_1);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = (L_15->___source_2);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		int32_t L_17 = NativeSavedGameClient_AsDataSource_m2538(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		U3CInternalManualOpenU3Ec__AnonStorey45_t622 * L_18 = V_0;
		IntPtr_t L_19 = { U3CInternalManualOpenU3Ec__AnonStorey45_U3CU3Em__4A_m2515_MethodInfo_var };
		Action_1_t895 * L_20 = (Action_1_t895 *)il2cpp_codegen_object_new (Action_1_t895_il2cpp_TypeInfo_var);
		Action_1__ctor_m3866(L_20, L_18, L_19, /*hidden argument*/Action_1__ctor_m3866_MethodInfo_var);
		NullCheck(L_12);
		SnapshotManager_Open_m3051(L_12, L_14, L_17, 1, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::ReadBinaryData(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>)
extern TypeInfo* U3CReadBinaryDataU3Ec__AnonStorey47_t626_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t897_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t625_m3859_MethodInfo_var;
extern MethodInfo* NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisByteU5BU5D_t350_m3860_MethodInfo_var;
extern MethodInfo* U3CReadBinaryDataU3Ec__AnonStorey47_U3CU3Em__4B_m2517_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3851_MethodInfo_var;
extern "C" void NativeSavedGameClient_ReadBinaryData_m2530 (NativeSavedGameClient_t624 * __this, Object_t * ___metadata, Action_2_t625 * ___completedCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CReadBinaryDataU3Ec__AnonStorey47_t626_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(828);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(804);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t897_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(819);
		Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484016);
		Misc_CheckNotNull_TisAction_2_t625_m3859_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484017);
		NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisByteU5BU5D_t350_m3860_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484018);
		U3CReadBinaryDataU3Ec__AnonStorey47_U3CU3Em__4B_m2517_MethodInfo_var = il2cpp_codegen_method_info_from_index(371);
		Action_1__ctor_m3851_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484005);
		s_Il2CppMethodIntialized = true;
	}
	NativeSnapshotMetadata_t611 * V_0 = {0};
	U3CReadBinaryDataU3Ec__AnonStorey47_t626 * V_1 = {0};
	{
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_0 = (U3CReadBinaryDataU3Ec__AnonStorey47_t626 *)il2cpp_codegen_object_new (U3CReadBinaryDataU3Ec__AnonStorey47_t626_il2cpp_TypeInfo_var);
		U3CReadBinaryDataU3Ec__AnonStorey47__ctor_m2516(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_1 = V_1;
		Action_2_t625 * L_2 = ___completedCallback;
		NullCheck(L_1);
		L_1->___completedCallback_0 = L_2;
		Object_t * L_3 = ___metadata;
		Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var);
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_4 = V_1;
		NullCheck(L_4);
		Action_2_t625 * L_5 = (L_4->___completedCallback_0);
		Misc_CheckNotNull_TisAction_2_t625_m3859(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t625_m3859_MethodInfo_var);
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_6 = V_1;
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_7 = V_1;
		NullCheck(L_7);
		Action_2_t625 * L_8 = (L_7->___completedCallback_0);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		Action_2_t625 * L_9 = NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisByteU5BU5D_t350_m3860(NULL /*static, unused*/, L_8, /*hidden argument*/NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisByteU5BU5D_t350_m3860_MethodInfo_var);
		NullCheck(L_6);
		L_6->___completedCallback_0 = L_9;
		Object_t * L_10 = ___metadata;
		V_0 = ((NativeSnapshotMetadata_t611 *)IsInst(L_10, NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var));
		NativeSnapshotMetadata_t611 * L_11 = V_0;
		if (L_11)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral527, /*hidden argument*/NULL);
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_12 = V_1;
		NullCheck(L_12);
		Action_2_t625 * L_13 = (L_12->___completedCallback_0);
		NullCheck(L_13);
		VirtActionInvoker2< int32_t, ByteU5BU5D_t350* >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::Invoke(!0,!1) */, L_13, ((int32_t)-4), (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL);
		return;
	}

IL_0057:
	{
		NativeSnapshotMetadata_t611 * L_14 = V_0;
		NullCheck(L_14);
		bool L_15 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean GooglePlayGames.Native.NativeSnapshotMetadata::get_IsOpen() */, L_14);
		if (L_15)
		{
			goto IL_007b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral528, /*hidden argument*/NULL);
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_16 = V_1;
		NullCheck(L_16);
		Action_2_t625 * L_17 = (L_16->___completedCallback_0);
		NullCheck(L_17);
		VirtActionInvoker2< int32_t, ByteU5BU5D_t350* >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::Invoke(!0,!1) */, L_17, ((int32_t)-4), (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL);
		return;
	}

IL_007b:
	{
		SnapshotManager_t610 * L_18 = (__this->___mSnapshotManager_1);
		NativeSnapshotMetadata_t611 * L_19 = V_0;
		U3CReadBinaryDataU3Ec__AnonStorey47_t626 * L_20 = V_1;
		IntPtr_t L_21 = { U3CReadBinaryDataU3Ec__AnonStorey47_U3CU3Em__4B_m2517_MethodInfo_var };
		Action_1_t897 * L_22 = (Action_1_t897 *)il2cpp_codegen_object_new (Action_1_t897_il2cpp_TypeInfo_var);
		Action_1__ctor_m3851(L_22, L_20, L_21, /*hidden argument*/Action_1__ctor_m3851_MethodInfo_var);
		NullCheck(L_18);
		SnapshotManager_Read_m3057(L_18, L_19, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::ShowSelectSavedGameUI(System.String,System.UInt32,System.Boolean,System.Boolean,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern TypeInfo* U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t894_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t627_m3861_MethodInfo_var;
extern MethodInfo* NativeSavedGameClient_ToOnGameThread_TisSelectUIStatus_t375_TisISavedGameMetadata_t618_m3862_MethodInfo_var;
extern MethodInfo* U3CShowSelectSavedGameUIU3Ec__AnonStorey48_U3CU3Em__4C_m2519_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3867_MethodInfo_var;
extern "C" void NativeSavedGameClient_ShowSelectSavedGameUI_m2531 (NativeSavedGameClient_t624 * __this, String_t* ___uiTitle, uint32_t ___maxDisplayedSavedGames, bool ___showCreateSaveUI, bool ___showDeleteSaveUI, Action_2_t627 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(832);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(833);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisAction_2_t627_m3861_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484020);
		NativeSavedGameClient_ToOnGameThread_TisSelectUIStatus_t375_TisISavedGameMetadata_t618_m3862_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484021);
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_U3CU3Em__4C_m2519_MethodInfo_var = il2cpp_codegen_method_info_from_index(374);
		Action_1__ctor_m3867_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484023);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * V_0 = {0};
	{
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * L_0 = (U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 *)il2cpp_codegen_object_new (U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628_il2cpp_TypeInfo_var);
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48__ctor_m2518(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * L_1 = V_0;
		Action_2_t627 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		String_t* L_3 = ___uiTitle;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * L_4 = V_0;
		NullCheck(L_4);
		Action_2_t627 * L_5 = (L_4->___callback_0);
		Misc_CheckNotNull_TisAction_2_t627_m3861(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t627_m3861_MethodInfo_var);
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * L_6 = V_0;
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * L_7 = V_0;
		NullCheck(L_7);
		Action_2_t627 * L_8 = (L_7->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		Action_2_t627 * L_9 = NativeSavedGameClient_ToOnGameThread_TisSelectUIStatus_t375_TisISavedGameMetadata_t618_m3862(NULL /*static, unused*/, L_8, /*hidden argument*/NativeSavedGameClient_ToOnGameThread_TisSelectUIStatus_t375_TisISavedGameMetadata_t618_m3862_MethodInfo_var);
		NullCheck(L_6);
		L_6->___callback_0 = L_9;
		uint32_t L_10 = ___maxDisplayedSavedGames;
		if ((!(((uint32_t)L_10) <= ((uint32_t)0))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral529, /*hidden argument*/NULL);
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * L_11 = V_0;
		NullCheck(L_11);
		Action_2_t627 * L_12 = (L_11->___callback_0);
		NullCheck(L_12);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_12, ((int32_t)-4), (Object_t *)NULL);
		return;
	}

IL_0052:
	{
		SnapshotManager_t610 * L_13 = (__this->___mSnapshotManager_1);
		bool L_14 = ___showCreateSaveUI;
		bool L_15 = ___showDeleteSaveUI;
		uint32_t L_16 = ___maxDisplayedSavedGames;
		String_t* L_17 = ___uiTitle;
		U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * L_18 = V_0;
		IntPtr_t L_19 = { U3CShowSelectSavedGameUIU3Ec__AnonStorey48_U3CU3Em__4C_m2519_MethodInfo_var };
		Action_1_t894 * L_20 = (Action_1_t894 *)il2cpp_codegen_object_new (Action_1_t894_il2cpp_TypeInfo_var);
		Action_1__ctor_m3867(L_20, L_18, L_19, /*hidden argument*/Action_1__ctor_m3867_MethodInfo_var);
		NullCheck(L_13);
		SnapshotManager_SnapshotSelectUI_m3049(L_13, L_14, L_15, L_16, L_17, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::CommitUpdate(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate,System.Byte[],System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern TypeInfo* U3CCommitUpdateU3Ec__AnonStorey49_t629_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t896_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var;
extern MethodInfo* NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var;
extern MethodInfo* U3CCommitUpdateU3Ec__AnonStorey49_U3CU3Em__4D_m2521_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3846_MethodInfo_var;
extern "C" void NativeSavedGameClient_CommitUpdate_m2532 (NativeSavedGameClient_t624 * __this, Object_t * ___metadata, SavedGameMetadataUpdate_t378  ___updateForMetadata, ByteU5BU5D_t350* ___updatedBinaryData, Action_2_t612 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCommitUpdateU3Ec__AnonStorey49_t629_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(834);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(804);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(810);
		Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484016);
		Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483826);
		Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484010);
		U3CCommitUpdateU3Ec__AnonStorey49_U3CU3Em__4D_m2521_MethodInfo_var = il2cpp_codegen_method_info_from_index(376);
		Action_1__ctor_m3846_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483995);
		s_Il2CppMethodIntialized = true;
	}
	NativeSnapshotMetadata_t611 * V_0 = {0};
	U3CCommitUpdateU3Ec__AnonStorey49_t629 * V_1 = {0};
	{
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_0 = (U3CCommitUpdateU3Ec__AnonStorey49_t629 *)il2cpp_codegen_object_new (U3CCommitUpdateU3Ec__AnonStorey49_t629_il2cpp_TypeInfo_var);
		U3CCommitUpdateU3Ec__AnonStorey49__ctor_m2520(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_1 = V_1;
		Action_2_t612 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		Object_t * L_3 = ___metadata;
		Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var);
		ByteU5BU5D_t350* L_4 = ___updatedBinaryData;
		Misc_CheckNotNull_TisByteU5BU5D_t350_m3707(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var);
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_5 = V_1;
		NullCheck(L_5);
		Action_2_t612 * L_6 = (L_5->___callback_0);
		Misc_CheckNotNull_TisAction_2_t612_m3845(NULL /*static, unused*/, L_6, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t612_m3845_MethodInfo_var);
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_7 = V_1;
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_8 = V_1;
		NullCheck(L_8);
		Action_2_t612 * L_9 = (L_8->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		Action_2_t612 * L_10 = NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855(NULL /*static, unused*/, L_9, /*hidden argument*/NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisISavedGameMetadata_t618_m3855_MethodInfo_var);
		NullCheck(L_7);
		L_7->___callback_0 = L_10;
		Object_t * L_11 = ___metadata;
		V_0 = ((NativeSnapshotMetadata_t611 *)IsInst(L_11, NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var));
		NativeSnapshotMetadata_t611 * L_12 = V_0;
		if (L_12)
		{
			goto IL_005f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral527, /*hidden argument*/NULL);
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_13 = V_1;
		NullCheck(L_13);
		Action_2_t612 * L_14 = (L_13->___callback_0);
		NullCheck(L_14);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_14, ((int32_t)-4), (Object_t *)NULL);
		return;
	}

IL_005f:
	{
		NativeSnapshotMetadata_t611 * L_15 = V_0;
		NullCheck(L_15);
		bool L_16 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean GooglePlayGames.Native.NativeSnapshotMetadata::get_IsOpen() */, L_15);
		if (L_16)
		{
			goto IL_0083;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral528, /*hidden argument*/NULL);
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_17 = V_1;
		NullCheck(L_17);
		Action_2_t612 * L_18 = (L_17->___callback_0);
		NullCheck(L_18);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(!0,!1) */, L_18, ((int32_t)-4), (Object_t *)NULL);
		return;
	}

IL_0083:
	{
		SnapshotManager_t610 * L_19 = (__this->___mSnapshotManager_1);
		NativeSnapshotMetadata_t611 * L_20 = V_0;
		SavedGameMetadataUpdate_t378  L_21 = ___updateForMetadata;
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		NativeSnapshotMetadataChange_t679 * L_22 = NativeSavedGameClient_AsMetadataChange_m2541(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_23 = ___updatedBinaryData;
		U3CCommitUpdateU3Ec__AnonStorey49_t629 * L_24 = V_1;
		IntPtr_t L_25 = { U3CCommitUpdateU3Ec__AnonStorey49_U3CU3Em__4D_m2521_MethodInfo_var };
		Action_1_t896 * L_26 = (Action_1_t896 *)il2cpp_codegen_object_new (Action_1_t896_il2cpp_TypeInfo_var);
		Action_1__ctor_m3846(L_26, L_24, L_25, /*hidden argument*/Action_1__ctor_m3846_MethodInfo_var);
		NullCheck(L_19);
		SnapshotManager_Commit_m3053(L_19, L_20, L_22, L_23, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::FetchAllSavedGames(GooglePlayGames.BasicApi.DataSource,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>)
extern TypeInfo* U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t893_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t630_m3863_MethodInfo_var;
extern MethodInfo* NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisList_1_t931_m3864_MethodInfo_var;
extern MethodInfo* U3CFetchAllSavedGamesU3Ec__AnonStorey4A_U3CU3Em__4E_m2523_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3868_MethodInfo_var;
extern "C" void NativeSavedGameClient_FetchAllSavedGames_m2533 (NativeSavedGameClient_t624 * __this, int32_t ___source, Action_2_t630 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(837);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		Action_1_t893_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		Misc_CheckNotNull_TisAction_2_t630_m3863_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484025);
		NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisList_1_t931_m3864_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484026);
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_U3CU3Em__4E_m2523_MethodInfo_var = il2cpp_codegen_method_info_from_index(379);
		Action_1__ctor_m3868_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484028);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * V_0 = {0};
	{
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * L_0 = (U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 *)il2cpp_codegen_object_new (U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631_il2cpp_TypeInfo_var);
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A__ctor_m2522(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * L_1 = V_0;
		Action_2_t630 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * L_3 = V_0;
		NullCheck(L_3);
		Action_2_t630 * L_4 = (L_3->___callback_0);
		Misc_CheckNotNull_TisAction_2_t630_m3863(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t630_m3863_MethodInfo_var);
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * L_5 = V_0;
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * L_6 = V_0;
		NullCheck(L_6);
		Action_2_t630 * L_7 = (L_6->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		Action_2_t630 * L_8 = NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisList_1_t931_m3864(NULL /*static, unused*/, L_7, /*hidden argument*/NativeSavedGameClient_ToOnGameThread_TisSavedGameRequestStatus_t374_TisList_1_t931_m3864_MethodInfo_var);
		NullCheck(L_5);
		L_5->___callback_0 = L_8;
		SnapshotManager_t610 * L_9 = (__this->___mSnapshotManager_1);
		int32_t L_10 = ___source;
		int32_t L_11 = NativeSavedGameClient_AsDataSource_m2538(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * L_12 = V_0;
		IntPtr_t L_13 = { U3CFetchAllSavedGamesU3Ec__AnonStorey4A_U3CU3Em__4E_m2523_MethodInfo_var };
		Action_1_t893 * L_14 = (Action_1_t893 *)il2cpp_codegen_object_new (Action_1_t893_il2cpp_TypeInfo_var);
		Action_1__ctor_m3868(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m3868_MethodInfo_var);
		NullCheck(L_9);
		SnapshotManager_FetchAll_m3047(L_9, L_11, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient::Delete(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern TypeInfo* NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var;
extern "C" void NativeSavedGameClient_Delete_m2534 (NativeSavedGameClient_t624 * __this, Object_t * ___metadata, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(804);
		Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484016);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___metadata;
		Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisISavedGameMetadata_t618_m3858_MethodInfo_var);
		SnapshotManager_t610 * L_1 = (__this->___mSnapshotManager_1);
		Object_t * L_2 = ___metadata;
		NullCheck(L_1);
		SnapshotManager_Delete_m3056(L_1, ((NativeSnapshotMetadata_t611 *)Castclass(L_2, NativeSnapshotMetadata_t611_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeSavedGameClient::IsValidFilename(System.String)
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern "C" bool NativeSavedGameClient_IsValidFilename_m2535 (Object_t * __this /* static, unused */, String_t* ___filename, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___filename;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
		Regex_t200 * L_1 = ((NativeSavedGameClient_t624_StaticFields*)NativeSavedGameClient_t624_il2cpp_TypeInfo_var->static_fields)->___ValidFilenameRegex_0;
		String_t* L_2 = ___filename;
		NullCheck(L_1);
		bool L_3 = Regex_IsMatch_m3869(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy GooglePlayGames.Native.NativeSavedGameClient::AsConflictPolicy(GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy)
extern TypeInfo* ConflictResolutionStrategy_t373_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t NativeSavedGameClient_AsConflictPolicy_m2536 (Object_t * __this /* static, unused */, int32_t ___strategy, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConflictResolutionStrategy_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(814);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___strategy;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_001b;
		}
		if (L_1 == 2)
		{
			goto IL_001d;
		}
	}
	{
		goto IL_001f;
	}

IL_0019:
	{
		return (int32_t)(2);
	}

IL_001b:
	{
		return (int32_t)(3);
	}

IL_001d:
	{
		return (int32_t)(4);
	}

IL_001f:
	{
		int32_t L_2 = ___strategy;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(ConflictResolutionStrategy_t373_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral530, L_4, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_6 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_6, L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus GooglePlayGames.Native.NativeSavedGameClient::AsRequestStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus)
extern TypeInfo* SnapshotOpenStatus_t416_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" int32_t NativeSavedGameClient_AsRequestStatus_m2537 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SnapshotOpenStatus_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(839);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 0)
		{
			goto IL_0027;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 1)
		{
			goto IL_0016;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 2)
		{
			goto IL_0024;
		}
	}

IL_0016:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		goto IL_0029;
	}

IL_0022:
	{
		return (int32_t)(1);
	}

IL_0024:
	{
		return (int32_t)(((int32_t)-3));
	}

IL_0027:
	{
		return (int32_t)((-1));
	}

IL_0029:
	{
		int32_t L_3 = ___status;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(SnapshotOpenStatus_t416_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral483, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return (int32_t)(((int32_t)-2));
	}
}
// GooglePlayGames.Native.Cwrapper.Types/DataSource GooglePlayGames.Native.NativeSavedGameClient::AsDataSource(GooglePlayGames.BasicApi.DataSource)
extern TypeInfo* DataSource_t334_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t NativeSavedGameClient_AsDataSource_m2538 (Object_t * __this /* static, unused */, int32_t ___source, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSource_t334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___source;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0018;
	}

IL_0014:
	{
		return (int32_t)(1);
	}

IL_0016:
	{
		return (int32_t)(2);
	}

IL_0018:
	{
		int32_t L_3 = ___source;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(DataSource_t334_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral437, L_5, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_7 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_7);
	}
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus GooglePlayGames.Native.NativeSavedGameClient::AsRequestStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ResponseStatus_t409_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" int32_t NativeSavedGameClient_AsRequestStatus_m2539 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ResponseStatus_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 0)
		{
			goto IL_004c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 1)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 2)
		{
			goto IL_003f;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 3)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 4)
		{
			goto IL_0032;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 5)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 6)
		{
			goto IL_004e;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 7)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0050;
	}

IL_002f:
	{
		return (int32_t)(((int32_t)-2));
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral531, /*hidden argument*/NULL);
		return (int32_t)(((int32_t)-3));
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral532, /*hidden argument*/NULL);
		return (int32_t)(((int32_t)-3));
	}

IL_004c:
	{
		return (int32_t)((-1));
	}

IL_004e:
	{
		return (int32_t)(1);
	}

IL_0050:
	{
		int32_t L_2 = ___status;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(ResponseStatus_t409_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral436, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (int32_t)(((int32_t)-2));
	}
}
// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus GooglePlayGames.Native.NativeSavedGameClient::AsUIStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" int32_t NativeSavedGameClient_AsUIStatus_m2540 (Object_t * __this /* static, unused */, int32_t ___uiStatus, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___uiStatus;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 0)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 1)
		{
			goto IL_0038;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 2)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 3)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 4)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 5)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 6)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)6)) == 7)
		{
			goto IL_002f;
		}
	}
	{
		goto IL_003b;
	}

IL_002f:
	{
		return (int32_t)(1);
	}

IL_0031:
	{
		return (int32_t)(2);
	}

IL_0033:
	{
		return (int32_t)((-1));
	}

IL_0035:
	{
		return (int32_t)(((int32_t)-3));
	}

IL_0038:
	{
		return (int32_t)(((int32_t)-2));
	}

IL_003b:
	{
		int32_t L_2 = ___uiStatus;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(UIStatus_t412_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral533, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (int32_t)((-1));
	}
}
// GooglePlayGames.Native.NativeSnapshotMetadataChange GooglePlayGames.Native.NativeSavedGameClient::AsMetadataChange(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate)
extern TypeInfo* Builder_t678_il2cpp_TypeInfo_var;
extern MethodInfo* Nullable_1_get_Value_m3870_MethodInfo_var;
extern "C" NativeSnapshotMetadataChange_t679 * NativeSavedGameClient_AsMetadataChange_m2541 (Object_t * __this /* static, unused */, SavedGameMetadataUpdate_t378  ___update, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Builder_t678_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(809);
		Nullable_1_get_Value_m3870_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484029);
		s_Il2CppMethodIntialized = true;
	}
	Builder_t678 * V_0 = {0};
	Nullable_1_t377  V_1 = {0};
	TimeSpan_t190  V_2 = {0};
	{
		Builder_t678 * L_0 = (Builder_t678 *)il2cpp_codegen_object_new (Builder_t678_il2cpp_TypeInfo_var);
		Builder__ctor_m2817(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = SavedGameMetadataUpdate_get_IsCoverImageUpdated_m1490((&___update), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Builder_t678 * L_2 = V_0;
		ByteU5BU5D_t350* L_3 = SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m1491((&___update), /*hidden argument*/NULL);
		NullCheck(L_2);
		Builder_SetCoverImageFromPngData_m2821(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		bool L_4 = SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1488((&___update), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		Builder_t678 * L_5 = V_0;
		String_t* L_6 = SavedGameMetadataUpdate_get_UpdatedDescription_m1489((&___update), /*hidden argument*/NULL);
		NullCheck(L_5);
		Builder_SetDescription_m2819(L_5, L_6, /*hidden argument*/NULL);
	}

IL_003a:
	{
		bool L_7 = SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_m1492((&___update), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		Builder_t678 * L_8 = V_0;
		Nullable_1_t377  L_9 = SavedGameMetadataUpdate_get_UpdatedPlayedTime_m1493((&___update), /*hidden argument*/NULL);
		V_1 = L_9;
		TimeSpan_t190  L_10 = Nullable_1_get_Value_m3870((&V_1), /*hidden argument*/Nullable_1_get_Value_m3870_MethodInfo_var);
		V_2 = L_10;
		double L_11 = TimeSpan_get_TotalMilliseconds_m3711((&V_2), /*hidden argument*/NULL);
		NullCheck(L_8);
		Builder_SetPlayedTime_m2820(L_8, (((uint64_t)L_11)), /*hidden argument*/NULL);
	}

IL_0065:
	{
		Builder_t678 * L_12 = V_0;
		NullCheck(L_12);
		NativeSnapshotMetadataChange_t679 * L_13 = Builder_Build_m2822(L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMultMethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_6.h"
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_5.h"
// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>
#include "System_Core_System_Action_2_gen_14.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_1.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_14.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_3.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>
#include "mscorlib_System_Action_1_gen_42.h"
// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>
#include "System_Core_System_Action_2_gen_14MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_6MethodDeclarations.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_14MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_3MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey4D__ctor_m2542 (U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D::<>m__57(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_U3CU3Em__57_m2543 (U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * __this, PlayerSelectUIResponse_t686 * ___result, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	TurnBasedMatchConfigBuilder_t712 * V_0 = {0};
	TurnBasedMatchConfig_t710 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PlayerSelectUIResponse_t686 * L_0 = ___result;
		NullCheck(L_0);
		int32_t L_1 = PlayerSelectUIResponse_Status_m2882(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		Action_2_t632 * L_2 = (__this->___callback_0);
		NullCheck(L_2);
		VirtActionInvoker2< bool, TurnBasedMatch_t353 * >::Invoke(10 /* System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(!0,!1) */, L_2, 0, (TurnBasedMatch_t353 *)NULL);
	}

IL_0019:
	{
		TurnBasedMatchConfigBuilder_t712 * L_3 = TurnBasedMatchConfigBuilder_Create_m3120(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			TurnBasedMatchConfigBuilder_t712 * L_4 = V_0;
			PlayerSelectUIResponse_t686 * L_5 = ___result;
			NullCheck(L_4);
			TurnBasedMatchConfigBuilder_PopulateFromUIResponse_m3112(L_4, L_5, /*hidden argument*/NULL);
			TurnBasedMatchConfigBuilder_t712 * L_6 = V_0;
			NullCheck(L_6);
			TurnBasedMatchConfig_t710 * L_7 = TurnBasedMatchConfigBuilder_Build_m3118(L_6, /*hidden argument*/NULL);
			V_1 = L_7;
		}

IL_002e:
		try
		{ // begin try (depth: 2)
			NativeTurnBasedMultiplayerClient_t535 * L_8 = (__this->___U3CU3Ef__this_1);
			NullCheck(L_8);
			TurnBasedManager_t651 * L_9 = (L_8->___mTurnBasedManager_0);
			TurnBasedMatchConfig_t710 * L_10 = V_1;
			NativeTurnBasedMultiplayerClient_t535 * L_11 = (__this->___U3CU3Ef__this_1);
			Action_2_t632 * L_12 = (__this->___callback_0);
			NullCheck(L_11);
			Action_1_t864 * L_13 = NativeTurnBasedMultiplayerClient_BridgeMatchToUserCallback_m2582(L_11, L_12, /*hidden argument*/NULL);
			NullCheck(L_9);
			TurnBasedManager_CreateMatch_m3083(L_9, L_10, L_13, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x62, FINALLY_0055);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_0055;
		}

FINALLY_0055:
		{ // begin finally (depth: 2)
			{
				TurnBasedMatchConfig_t710 * L_14 = V_1;
				if (!L_14)
				{
					goto IL_0061;
				}
			}

IL_005b:
			{
				TurnBasedMatchConfig_t710 * L_15 = V_1;
				NullCheck(L_15);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_15);
			}

IL_0061:
			{
				IL2CPP_END_FINALLY(85)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(85)
		{
			IL2CPP_JUMP_TBL(0x62, IL_0062)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_0062:
		{
			IL2CPP_LEAVE(0x74, FINALLY_0067);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0067;
	}

FINALLY_0067:
	{ // begin finally (depth: 1)
		{
			TurnBasedMatchConfigBuilder_t712 * L_16 = V_0;
			if (!L_16)
			{
				goto IL_0073;
			}
		}

IL_006d:
		{
			TurnBasedMatchConfigBuilder_t712 * L_17 = V_0;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_17);
		}

IL_0073:
		{
			IL2CPP_END_FINALLY(103)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(103)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0074:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_0MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_0.h"
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_NativeTurnB.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_0MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_NativeTurnBMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E::.ctor()
extern "C" void U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E__ctor_m2544 (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E::<>m__58(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" void U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_U3CU3Em__58_m2545 (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * __this, TurnBasedMatchResponse_t707 * ___callbackResult, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	NativeTurnBasedMatch_t680 * V_0 = {0};
	TurnBasedMatch_t353 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TurnBasedMatchResponse_t707 * L_0 = ___callbackResult;
		NullCheck(L_0);
		NativeTurnBasedMatch_t680 * L_1 = TurnBasedMatchResponse_Match_m3067(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			NativeTurnBasedMatch_t680 * L_2 = V_0;
			if (L_2)
			{
				goto IL_001f;
			}
		}

IL_000d:
		{
			Action_2_t632 * L_3 = (__this->___userCallback_0);
			NullCheck(L_3);
			VirtActionInvoker2< bool, TurnBasedMatch_t353 * >::Invoke(10 /* System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(!0,!1) */, L_3, 0, (TurnBasedMatch_t353 *)NULL);
			goto IL_0053;
		}

IL_001f:
		{
			NativeTurnBasedMatch_t680 * L_4 = V_0;
			NativeTurnBasedMultiplayerClient_t535 * L_5 = (__this->___U3CU3Ef__this_1);
			NullCheck(L_5);
			NativeClient_t524 * L_6 = (L_5->___mNativeClient_1);
			NullCheck(L_6);
			String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeClient::GetUserId() */, L_6);
			NullCheck(L_4);
			TurnBasedMatch_t353 * L_8 = NativeTurnBasedMatch_AsTurnBasedMatch_m2842(L_4, L_7, /*hidden argument*/NULL);
			V_1 = L_8;
			TurnBasedMatch_t353 * L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral547, L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			Action_2_t632 * L_11 = (__this->___userCallback_0);
			TurnBasedMatch_t353 * L_12 = V_1;
			NullCheck(L_11);
			VirtActionInvoker2< bool, TurnBasedMatch_t353 * >::Invoke(10 /* System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(!0,!1) */, L_11, 1, L_12);
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		{
			NativeTurnBasedMatch_t680 * L_13 = V_0;
			if (!L_13)
			{
				goto IL_0064;
			}
		}

IL_005e:
		{
			NativeTurnBasedMatch_t680 * L_14 = V_0;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_14);
		}

IL_0064:
		{
			IL2CPP_END_FINALLY(88)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0065:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey4F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey4F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_1MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMaMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey4F::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey4F__ctor_m2546 (U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey4F::<>m__59(GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey4F_U3CU3Em__59_m2547 (U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * __this, MatchInboxUIResponse_t706 * ___callbackResult, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	NativeTurnBasedMatch_t680 * V_0 = {0};
	TurnBasedMatch_t353 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MatchInboxUIResponse_t706 * L_0 = ___callbackResult;
		NullCheck(L_0);
		NativeTurnBasedMatch_t680 * L_1 = MatchInboxUIResponse_Match_m3061(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			NativeTurnBasedMatch_t680 * L_2 = V_0;
			if (L_2)
			{
				goto IL_001f;
			}
		}

IL_000d:
		{
			Action_2_t632 * L_3 = (__this->___callback_0);
			NullCheck(L_3);
			VirtActionInvoker2< bool, TurnBasedMatch_t353 * >::Invoke(10 /* System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(!0,!1) */, L_3, 0, (TurnBasedMatch_t353 *)NULL);
			goto IL_0053;
		}

IL_001f:
		{
			NativeTurnBasedMatch_t680 * L_4 = V_0;
			NativeTurnBasedMultiplayerClient_t535 * L_5 = (__this->___U3CU3Ef__this_1);
			NullCheck(L_5);
			NativeClient_t524 * L_6 = (L_5->___mNativeClient_1);
			NullCheck(L_6);
			String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeClient::GetUserId() */, L_6);
			NullCheck(L_4);
			TurnBasedMatch_t353 * L_8 = NativeTurnBasedMatch_AsTurnBasedMatch_m2842(L_4, L_7, /*hidden argument*/NULL);
			V_1 = L_8;
			TurnBasedMatch_t353 * L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral547, L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			Action_2_t632 * L_11 = (__this->___callback_0);
			TurnBasedMatch_t353 * L_12 = V_1;
			NullCheck(L_11);
			VirtActionInvoker2< bool, TurnBasedMatch_t353 * >::Invoke(10 /* System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(!0,!1) */, L_11, 1, L_12);
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		{
			NativeTurnBasedMatch_t680 * L_13 = V_0;
			if (!L_13)
			{
				goto IL_0064;
			}
		}

IL_005e:
		{
			NativeTurnBasedMatch_t680 * L_14 = V_0;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_14);
		}

IL_0064:
		{
			IL2CPP_END_FINALLY(88)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0065:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey50
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey50
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey50::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey50__ctor_m2548 (U3CAcceptInvitationU3Ec__AnonStorey50_t636 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey50::<>m__5A(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey50_U3CU3Em__5A_m2549 (U3CAcceptInvitationU3Ec__AnonStorey50_t636 * __this, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		MultiplayerInvitation_t601 * L_0 = ___invitation;
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_1 = (__this->___invitationId_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral548, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Action_2_t632 * L_3 = (__this->___callback_1);
		NullCheck(L_3);
		VirtActionInvoker2< bool, TurnBasedMatch_t353 * >::Invoke(10 /* System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(!0,!1) */, L_3, 0, (TurnBasedMatch_t353 *)NULL);
		return;
	}

IL_0029:
	{
		NativeTurnBasedMultiplayerClient_t535 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		TurnBasedManager_t651 * L_5 = (L_4->___mTurnBasedManager_0);
		MultiplayerInvitation_t601 * L_6 = ___invitation;
		NativeTurnBasedMultiplayerClient_t535 * L_7 = (__this->___U3CU3Ef__this_2);
		Action_2_t632 * L_8 = (__this->___callback_1);
		NullCheck(L_7);
		Action_1_t864 * L_9 = NativeTurnBasedMultiplayerClient_BridgeMatchToUserCallback_m2582(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		TurnBasedManager_AcceptInvitation_m3088(L_5, L_6, L_9, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_3MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_1.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
#include "mscorlib_System_Action_1_gen_43.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_1MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
#include "mscorlib_System_Action_1_gen_43MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51::.ctor()
extern "C" void U3CFindInvitationWithIdU3Ec__AnonStorey51__ctor_m2550 (U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51::<>m__5B(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse)
extern TypeInfo* IEnumerable_1_t883_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t929_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern "C" void U3CFindInvitationWithIdU3Ec__AnonStorey51_U3CU3Em__5B_m2551 (U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * __this, TurnBasedMatchesResponse_t708 * ___allMatches, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t883_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(781);
		IEnumerator_1_t929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(782);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	MultiplayerInvitation_t601 * V_0 = {0};
	Object_t* V_1 = {0};
	MultiplayerInvitation_t601 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TurnBasedMatchesResponse_t708 * L_0 = ___allMatches;
		NullCheck(L_0);
		int32_t L_1 = TurnBasedMatchesResponse_Status_m3072(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		Action_1_t637 * L_2 = (__this->___callback_0);
		NullCheck(L_2);
		VirtActionInvoker1< MultiplayerInvitation_t601 * >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::Invoke(!0) */, L_2, (MultiplayerInvitation_t601 *)NULL);
		return;
	}

IL_0019:
	{
		TurnBasedMatchesResponse_t708 * L_3 = ___allMatches;
		NullCheck(L_3);
		Object_t* L_4 = TurnBasedMatchesResponse_Invitations_m3073(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_t* L_5 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::GetEnumerator() */, IEnumerable_1_t883_il2cpp_TypeInfo_var, L_4);
		V_1 = L_5;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006c;
		}

IL_002a:
		{
			Object_t* L_6 = V_1;
			NullCheck(L_6);
			MultiplayerInvitation_t601 * L_7 = (MultiplayerInvitation_t601 *)InterfaceFuncInvoker0< MultiplayerInvitation_t601 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::get_Current() */, IEnumerator_1_t929_il2cpp_TypeInfo_var, L_6);
			V_0 = L_7;
			MultiplayerInvitation_t601 * L_8 = V_0;
			V_2 = L_8;
		}

IL_0033:
		try
		{ // begin try (depth: 2)
			{
				MultiplayerInvitation_t601 * L_9 = V_0;
				NullCheck(L_9);
				String_t* L_10 = MultiplayerInvitation_Id_m2703(L_9, /*hidden argument*/NULL);
				String_t* L_11 = (__this->___invitationId_1);
				NullCheck(L_10);
				bool L_12 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_10, L_11);
				if (!L_12)
				{
					goto IL_005a;
				}
			}

IL_0049:
			{
				Action_1_t637 * L_13 = (__this->___callback_0);
				MultiplayerInvitation_t601 * L_14 = V_0;
				NullCheck(L_13);
				VirtActionInvoker1< MultiplayerInvitation_t601 * >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::Invoke(!0) */, L_13, L_14);
				IL2CPP_LEAVE(0x93, FINALLY_005f);
			}

IL_005a:
			{
				IL2CPP_LEAVE(0x6C, FINALLY_005f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_005f;
		}

FINALLY_005f:
		{ // begin finally (depth: 2)
			{
				MultiplayerInvitation_t601 * L_15 = V_2;
				if (!L_15)
				{
					goto IL_006b;
				}
			}

IL_0065:
			{
				MultiplayerInvitation_t601 * L_16 = V_2;
				NullCheck(L_16);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_16);
			}

IL_006b:
			{
				IL2CPP_END_FINALLY(95)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(95)
		{
			IL2CPP_END_CLEANUP(0x93, FINALLY_007c);
			IL2CPP_JUMP_TBL(0x6C, IL_006c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_006c:
		{
			Object_t* L_17 = V_1;
			NullCheck(L_17);
			bool L_18 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_002a;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x87, FINALLY_007c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		{
			Object_t* L_19 = V_1;
			if (L_19)
			{
				goto IL_0080;
			}
		}

IL_007f:
		{
			IL2CPP_END_FINALLY(124)
		}

IL_0080:
		{
			Object_t* L_20 = V_1;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(124)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0087:
	{
		Action_1_t637 * L_21 = (__this->___callback_0);
		NullCheck(L_21);
		VirtActionInvoker1< MultiplayerInvitation_t601 * >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::Invoke(!0) */, L_21, (MultiplayerInvitation_t601 *)NULL);
	}

IL_0093:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_4MethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52::.ctor()
extern "C" void U3CRegisterMatchDelegateU3Ec__AnonStorey52__ctor_m2552 (U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52::<>m__5C(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C" void U3CRegisterMatchDelegateU3Ec__AnonStorey52_U3CU3Em__5C_m2553 (U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * __this, TurnBasedMatch_t353 * ___match, bool ___autoLaunch, MethodInfo* method)
{
	{
		MatchDelegate_t364 * L_0 = (__this->___del_0);
		TurnBasedMatch_t353 * L_1 = ___match;
		bool L_2 = ___autoLaunch;
		NullCheck(L_0);
		VirtActionInvoker2< TurnBasedMatch_t353 *, bool >::Invoke(10 /* System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean) */, L_0, L_1, L_2);
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_5.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_5MethodDeclarations.h"

// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>
#include "mscorlib_System_Action_1_gen_42MethodDeclarations.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::.ctor()
extern "C" void U3CTakeTurnU3Ec__AnonStorey53__ctor_m2554 (U3CTakeTurnU3Ec__AnonStorey53_t640 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::<>m__5D(GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* Action_1_t864_il2cpp_TypeInfo_var;
extern MethodInfo* U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__68_m2556_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3871_MethodInfo_var;
extern "C" void U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__5D_m2555 (U3CTakeTurnU3Ec__AnonStorey53_t640 * __this, MultiplayerParticipant_t672 * ___pendingParticipant, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t864_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__68_m2556_MethodInfo_var = il2cpp_codegen_method_info_from_index(382);
		Action_1__ctor_m3871_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484031);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeTurnBasedMultiplayerClient_t535 * L_0 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_0);
		TurnBasedManager_t651 * L_1 = (L_0->___mTurnBasedManager_0);
		NativeTurnBasedMatch_t680 * L_2 = ___foundMatch;
		ByteU5BU5D_t350* L_3 = (__this->___data_0);
		MultiplayerParticipant_t672 * L_4 = ___pendingParticipant;
		IntPtr_t L_5 = { U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__68_m2556_MethodInfo_var };
		Action_1_t864 * L_6 = (Action_1_t864 *)il2cpp_codegen_object_new (Action_1_t864_il2cpp_TypeInfo_var);
		Action_1__ctor_m3871(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m3871_MethodInfo_var);
		NullCheck(L_1);
		TurnBasedManager_TakeTurn_m3090(L_1, L_2, L_3, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::<>m__68(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern TypeInfo* MultiplayerStatus_t413_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__68_m2556 (U3CTakeTurnU3Ec__AnonStorey53_t640 * __this, TurnBasedMatchResponse_t707 * ___result, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerStatus_t413_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		TurnBasedMatchResponse_t707 * L_0 = ___result;
		NullCheck(L_0);
		bool L_1 = TurnBasedMatchResponse_RequestSucceeded_m3066(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Action_1_t98 * L_2 = (__this->___callback_1);
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_2, 1);
		goto IL_0042;
	}

IL_001c:
	{
		TurnBasedMatchResponse_t707 * L_3 = ___result;
		NullCheck(L_3);
		int32_t L_4 = TurnBasedMatchResponse_ResponseStatus_m3065(L_3, /*hidden argument*/NULL);
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(MultiplayerStatus_t413_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral549, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Action_1_t98 * L_8 = (__this->___callback_1);
		NullCheck(L_8);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_8, 0);
	}

IL_0042:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_6.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_6MethodDeclarations.h"

// System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "mscorlib_System_Action_1_gen_44.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_1MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "mscorlib_System_Action_1_gen_44MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54::.ctor()
extern "C" void U3CFindEqualVersionMatchU3Ec__AnonStorey54__ctor_m2557 (U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54::<>m__5E(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t235_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" void U3CFindEqualVersionMatchU3Ec__AnonStorey54_U3CU3Em__5E_m2558 (U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * __this, TurnBasedMatchResponse_t707 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		UInt32_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(170);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	NativeTurnBasedMatch_t680 * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TurnBasedMatchResponse_t707 * L_0 = ___response;
		NullCheck(L_0);
		NativeTurnBasedMatch_t680 * L_1 = TurnBasedMatchResponse_Match_m3067(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			NativeTurnBasedMatch_t680 * L_2 = V_0;
			if (L_2)
			{
				goto IL_0038;
			}
		}

IL_000d:
		{
			TurnBasedMatch_t353 * L_3 = (__this->___match_0);
			NullCheck(L_3);
			String_t* L_4 = TurnBasedMatch_get_MatchId_m1404(L_3, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_5 = String_Format_m3715(NULL /*static, unused*/, (String_t*) &_stringLiteral550, L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			Action_1_t98 * L_6 = (__this->___onFailure_1);
			NullCheck(L_6);
			VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_6, 0);
			IL2CPP_LEAVE(0xA7, FINALLY_009a);
		}

IL_0038:
		{
			NativeTurnBasedMatch_t680 * L_7 = V_0;
			NullCheck(L_7);
			uint32_t L_8 = NativeTurnBasedMatch_Version_m2830(L_7, /*hidden argument*/NULL);
			TurnBasedMatch_t353 * L_9 = (__this->___match_0);
			NullCheck(L_9);
			uint32_t L_10 = TurnBasedMatch_get_Version_m1416(L_9, /*hidden argument*/NULL);
			if ((((int32_t)L_8) == ((int32_t)L_10)))
			{
				goto IL_0089;
			}
		}

IL_004e:
		{
			TurnBasedMatch_t353 * L_11 = (__this->___match_0);
			NullCheck(L_11);
			uint32_t L_12 = TurnBasedMatch_get_Version_m1416(L_11, /*hidden argument*/NULL);
			uint32_t L_13 = L_12;
			Object_t * L_14 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_13);
			NativeTurnBasedMatch_t680 * L_15 = V_0;
			NullCheck(L_15);
			uint32_t L_16 = NativeTurnBasedMatch_Version_m2830(L_15, /*hidden argument*/NULL);
			uint32_t L_17 = L_16;
			Object_t * L_18 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_17);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_19 = String_Format_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral551, L_14, L_18, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
			Action_1_t98 * L_20 = (__this->___onFailure_1);
			NullCheck(L_20);
			VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_20, 0);
			IL2CPP_LEAVE(0xA7, FINALLY_009a);
		}

IL_0089:
		{
			Action_1_t641 * L_21 = (__this->___onVersionMatch_2);
			NativeTurnBasedMatch_t680 * L_22 = V_0;
			NullCheck(L_21);
			VirtActionInvoker1< NativeTurnBasedMatch_t680 * >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::Invoke(!0) */, L_21, L_22);
			IL2CPP_LEAVE(0xA7, FINALLY_009a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_009a;
	}

FINALLY_009a:
	{ // begin finally (depth: 1)
		{
			NativeTurnBasedMatch_t680 * L_23 = V_0;
			if (!L_23)
			{
				goto IL_00a6;
			}
		}

IL_00a0:
		{
			NativeTurnBasedMatch_t680 * L_24 = V_0;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_24);
		}

IL_00a6:
		{
			IL2CPP_END_FINALLY(154)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(154)
	{
		IL2CPP_JUMP_TBL(0xA7, IL_00a7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00a7:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_7.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_7MethodDeclarations.h"

// System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "System_Core_System_Action_2_gen_15.h"
// System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "System_Core_System_Action_2_gen_15MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::.ctor()
extern "C" void U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55__ctor_m2559 (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::<>m__5F(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_U3CU3Em__5F_m2560 (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	MultiplayerParticipant_t672 * V_0 = {0};
	MultiplayerParticipant_t672 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = (__this->___participantId_0);
		if (L_0)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant_t672 * L_1 = MultiplayerParticipant_AutomatchingSentinel_m2722(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			Action_2_t643 * L_2 = (__this->___onFoundParticipantAndMatch_1);
			MultiplayerParticipant_t672 * L_3 = V_0;
			NativeTurnBasedMatch_t680 * L_4 = ___foundMatch;
			NullCheck(L_2);
			VirtActionInvoker2< MultiplayerParticipant_t672 *, NativeTurnBasedMatch_t680 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::Invoke(!0,!1) */, L_2, L_3, L_4);
			IL2CPP_LEAVE(0x98, FINALLY_0028);
		}

IL_0023:
		{
			; // IL_0023: leave IL_0035
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		{
			MultiplayerParticipant_t672 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0034;
			}
		}

IL_002e:
		{
			MultiplayerParticipant_t672 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_6);
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(40)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x98, IL_0098)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0035:
	{
		NativeTurnBasedMatch_t680 * L_7 = ___foundMatch;
		String_t* L_8 = (__this->___participantId_0);
		NullCheck(L_7);
		MultiplayerParticipant_t672 * L_9 = NativeTurnBasedMatch_ParticipantWithId_m2833(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0042:
	try
	{ // begin try (depth: 1)
		{
			MultiplayerParticipant_t672 * L_10 = V_1;
			if (L_10)
			{
				goto IL_0079;
			}
		}

IL_0048:
		{
			TurnBasedMatch_t353 * L_11 = (__this->___match_2);
			NullCheck(L_11);
			String_t* L_12 = TurnBasedMatch_get_MatchId_m1404(L_11, /*hidden argument*/NULL);
			String_t* L_13 = (__this->___participantId_0);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Format_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral552, L_12, L_13, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			Action_1_t98 * L_15 = (__this->___onFailure_3);
			NullCheck(L_15);
			VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_15, 0);
			IL2CPP_LEAVE(0x98, FINALLY_008b);
		}

IL_0079:
		{
			Action_2_t643 * L_16 = (__this->___onFoundParticipantAndMatch_1);
			MultiplayerParticipant_t672 * L_17 = V_1;
			NativeTurnBasedMatch_t680 * L_18 = ___foundMatch;
			NullCheck(L_16);
			VirtActionInvoker2< MultiplayerParticipant_t672 *, NativeTurnBasedMatch_t680 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::Invoke(!0,!1) */, L_16, L_17, L_18);
			IL2CPP_LEAVE(0x98, FINALLY_008b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_008b;
	}

FINALLY_008b:
	{ // begin finally (depth: 1)
		{
			MultiplayerParticipant_t672 * L_19 = V_1;
			if (!L_19)
			{
				goto IL_0097;
			}
		}

IL_0091:
		{
			MultiplayerParticipant_t672 * L_20 = V_1;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_20);
		}

IL_0097:
		{
			IL2CPP_END_FINALLY(139)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(139)
	{
		IL2CPP_JUMP_TBL(0x98, IL_0098)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0098:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_8.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_8MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.ParticipantResults
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Participant.h"
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_0.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_0MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.ParticipantResults
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_ParticipantMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::.ctor()
extern "C" void U3CFinishU3Ec__AnonStorey56__ctor_m2561 (U3CFinishU3Ec__AnonStorey56_t645 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::<>m__60(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* UInt32_t235_il2cpp_TypeInfo_var;
extern TypeInfo* MatchResult_t514_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t214_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t864_il2cpp_TypeInfo_var;
extern MethodInfo* List_1_GetEnumerator_m969_MethodInfo_var;
extern MethodInfo* Enumerator_get_Current_m970_MethodInfo_var;
extern MethodInfo* Enumerator_MoveNext_m971_MethodInfo_var;
extern MethodInfo* U3CFinishU3Ec__AnonStorey56_U3CU3Em__69_m2563_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3871_MethodInfo_var;
extern "C" void U3CFinishU3Ec__AnonStorey56_U3CU3Em__60_m2562 (U3CFinishU3Ec__AnonStorey56_t645 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(170);
		MatchResult_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(842);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Enumerator_t214_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Action_1_t864_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		List_1_GetEnumerator_m969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483755);
		Enumerator_get_Current_m970_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483756);
		Enumerator_MoveNext_m971_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483757);
		U3CFinishU3Ec__AnonStorey56_U3CU3Em__69_m2563_MethodInfo_var = il2cpp_codegen_method_info_from_index(384);
		Action_1__ctor_m3871_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484031);
		s_Il2CppMethodIntialized = true;
	}
	ParticipantResults_t683 * V_0 = {0};
	String_t* V_1 = {0};
	Enumerator_t214  V_2 = {0};
	int32_t V_3 = {0};
	uint32_t V_4 = 0;
	int32_t V_5 = {0};
	uint32_t V_6 = 0;
	ParticipantResults_t683 * V_7 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NativeTurnBasedMatch_t680 * L_0 = ___foundMatch;
		NullCheck(L_0);
		ParticipantResults_t683 * L_1 = NativeTurnBasedMatch_Results_m2832(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		MatchOutcome_t345 * L_2 = (__this->___outcome_0);
		NullCheck(L_2);
		List_1_t43 * L_3 = MatchOutcome_get_ParticipantIds_m1381(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Enumerator_t214  L_4 = List_1_GetEnumerator_m969(L_3, /*hidden argument*/List_1_GetEnumerator_m969_MethodInfo_var);
		V_2 = L_4;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00be;
		}

IL_001d:
		{
			String_t* L_5 = Enumerator_get_Current_m970((&V_2), /*hidden argument*/Enumerator_get_Current_m970_MethodInfo_var);
			V_1 = L_5;
			MatchOutcome_t345 * L_6 = (__this->___outcome_0);
			String_t* L_7 = V_1;
			NullCheck(L_6);
			int32_t L_8 = MatchOutcome_GetResultFor_m1382(L_6, L_7, /*hidden argument*/NULL);
			int32_t L_9 = NativeTurnBasedMultiplayerClient_ResultToMatchResult_m2593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_3 = L_9;
			MatchOutcome_t345 * L_10 = (__this->___outcome_0);
			String_t* L_11 = V_1;
			NullCheck(L_10);
			uint32_t L_12 = MatchOutcome_GetPlacementFor_m1383(L_10, L_11, /*hidden argument*/NULL);
			V_4 = L_12;
			ParticipantResults_t683 * L_13 = V_0;
			String_t* L_14 = V_1;
			NullCheck(L_13);
			bool L_15 = ParticipantResults_HasResultsForParticipant_m2863(L_13, L_14, /*hidden argument*/NULL);
			if (!L_15)
			{
				goto IL_00a8;
			}
		}

IL_0051:
		{
			ParticipantResults_t683 * L_16 = V_0;
			String_t* L_17 = V_1;
			NullCheck(L_16);
			int32_t L_18 = ParticipantResults_ResultsForParticipant_m2865(L_16, L_17, /*hidden argument*/NULL);
			V_5 = L_18;
			ParticipantResults_t683 * L_19 = V_0;
			String_t* L_20 = V_1;
			NullCheck(L_19);
			uint32_t L_21 = ParticipantResults_PlacingForParticipant_m2864(L_19, L_20, /*hidden argument*/NULL);
			V_6 = L_21;
			int32_t L_22 = V_3;
			int32_t L_23 = V_5;
			if ((!(((uint32_t)L_22) == ((uint32_t)L_23))))
			{
				goto IL_0074;
			}
		}

IL_006b:
		{
			uint32_t L_24 = V_4;
			uint32_t L_25 = V_6;
			if ((((int32_t)L_24) == ((int32_t)L_25)))
			{
				goto IL_00a3;
			}
		}

IL_0074:
		{
			String_t* L_26 = V_1;
			uint32_t L_27 = V_6;
			uint32_t L_28 = L_27;
			Object_t * L_29 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_28);
			int32_t L_30 = V_5;
			int32_t L_31 = L_30;
			Object_t * L_32 = Box(MatchResult_t514_il2cpp_TypeInfo_var, &L_31);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_33 = String_Format_m3693(NULL /*static, unused*/, (String_t*) &_stringLiteral553, L_26, L_29, L_32, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
			Action_1_t98 * L_34 = (__this->___callback_1);
			NullCheck(L_34);
			VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_34, 0);
			IL2CPP_LEAVE(0xFF, FINALLY_00cf);
		}

IL_00a3:
		{
			goto IL_00be;
		}

IL_00a8:
		{
			ParticipantResults_t683 * L_35 = V_0;
			V_7 = L_35;
			ParticipantResults_t683 * L_36 = V_7;
			String_t* L_37 = V_1;
			uint32_t L_38 = V_4;
			int32_t L_39 = V_3;
			NullCheck(L_36);
			ParticipantResults_t683 * L_40 = ParticipantResults_WithResult_m2866(L_36, L_37, L_38, L_39, /*hidden argument*/NULL);
			V_0 = L_40;
			ParticipantResults_t683 * L_41 = V_7;
			NullCheck(L_41);
			VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_41);
		}

IL_00be:
		{
			bool L_42 = Enumerator_MoveNext_m971((&V_2), /*hidden argument*/Enumerator_MoveNext_m971_MethodInfo_var);
			if (L_42)
			{
				goto IL_001d;
			}
		}

IL_00ca:
		{
			IL2CPP_LEAVE(0xDB, FINALLY_00cf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_00cf;
	}

FINALLY_00cf:
	{ // begin finally (depth: 1)
		Enumerator_t214  L_43 = V_2;
		Enumerator_t214  L_44 = L_43;
		Object_t * L_45 = Box(Enumerator_t214_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_45);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_45);
		IL2CPP_END_FINALLY(207)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(207)
	{
		IL2CPP_JUMP_TBL(0xFF, IL_00ff)
		IL2CPP_JUMP_TBL(0xDB, IL_00db)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00db:
	{
		NativeTurnBasedMultiplayerClient_t535 * L_46 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_46);
		TurnBasedManager_t651 * L_47 = (L_46->___mTurnBasedManager_0);
		NativeTurnBasedMatch_t680 * L_48 = ___foundMatch;
		ByteU5BU5D_t350* L_49 = (__this->___data_2);
		ParticipantResults_t683 * L_50 = V_0;
		IntPtr_t L_51 = { U3CFinishU3Ec__AnonStorey56_U3CU3Em__69_m2563_MethodInfo_var };
		Action_1_t864 * L_52 = (Action_1_t864 *)il2cpp_codegen_object_new (Action_1_t864_il2cpp_TypeInfo_var);
		Action_1__ctor_m3871(L_52, __this, L_51, /*hidden argument*/Action_1__ctor_m3871_MethodInfo_var);
		NullCheck(L_47);
		TurnBasedManager_FinishMatchDuringMyTurn_m3095(L_47, L_48, L_49, L_50, L_52, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::<>m__69(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void U3CFinishU3Ec__AnonStorey56_U3CU3Em__69_m2563 (U3CFinishU3Ec__AnonStorey56_t645 * __this, TurnBasedMatchResponse_t707 * ___response, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = (__this->___callback_1);
		TurnBasedMatchResponse_t707 * L_1 = ___response;
		NullCheck(L_1);
		bool L_2 = TurnBasedMatchResponse_RequestSucceeded_m3066(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_0, L_2);
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_9.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_9MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::.ctor()
extern "C" void U3CAcknowledgeFinishedU3Ec__AnonStorey57__ctor_m2564 (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::<>m__61(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* Action_1_t864_il2cpp_TypeInfo_var;
extern MethodInfo* U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__6A_m2566_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3871_MethodInfo_var;
extern "C" void U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__61_m2565 (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t864_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__6A_m2566_MethodInfo_var = il2cpp_codegen_method_info_from_index(385);
		Action_1__ctor_m3871_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484031);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeTurnBasedMultiplayerClient_t535 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		TurnBasedManager_t651 * L_1 = (L_0->___mTurnBasedManager_0);
		NativeTurnBasedMatch_t680 * L_2 = ___foundMatch;
		IntPtr_t L_3 = { U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__6A_m2566_MethodInfo_var };
		Action_1_t864 * L_4 = (Action_1_t864 *)il2cpp_codegen_object_new (Action_1_t864_il2cpp_TypeInfo_var);
		Action_1__ctor_m3871(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m3871_MethodInfo_var);
		NullCheck(L_1);
		TurnBasedManager_ConfirmPendingCompletion_m3096(L_1, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::<>m__6A(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__6A_m2566 (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * __this, TurnBasedMatchResponse_t707 * ___response, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = (__this->___callback_0);
		TurnBasedMatchResponse_t707 * L_1 = ___response;
		NullCheck(L_1);
		bool L_2 = TurnBasedMatchResponse_RequestSucceeded_m3066(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_0, L_2);
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_10.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_10MethodDeclarations.h"

// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>
#include "mscorlib_System_Action_1_gen_29MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58::.ctor()
extern "C" void U3CLeaveU3Ec__AnonStorey58__ctor_m2567 (U3CLeaveU3Ec__AnonStorey58_t647 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58::<>m__62(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* Action_1_t890_il2cpp_TypeInfo_var;
extern MethodInfo* U3CLeaveU3Ec__AnonStorey58_U3CU3Em__6B_m2569_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3872_MethodInfo_var;
extern "C" void U3CLeaveU3Ec__AnonStorey58_U3CU3Em__62_m2568 (U3CLeaveU3Ec__AnonStorey58_t647 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t890_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(843);
		U3CLeaveU3Ec__AnonStorey58_U3CU3Em__6B_m2569_MethodInfo_var = il2cpp_codegen_method_info_from_index(386);
		Action_1__ctor_m3872_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484035);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeTurnBasedMultiplayerClient_t535 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		TurnBasedManager_t651 * L_1 = (L_0->___mTurnBasedManager_0);
		NativeTurnBasedMatch_t680 * L_2 = ___foundMatch;
		IntPtr_t L_3 = { U3CLeaveU3Ec__AnonStorey58_U3CU3Em__6B_m2569_MethodInfo_var };
		Action_1_t890 * L_4 = (Action_1_t890 *)il2cpp_codegen_object_new (Action_1_t890_il2cpp_TypeInfo_var);
		Action_1__ctor_m3872(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m3872_MethodInfo_var);
		NullCheck(L_1);
		TurnBasedManager_LeaveMatchDuringTheirTurn_m3097(L_1, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58::<>m__6B(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C" void U3CLeaveU3Ec__AnonStorey58_U3CU3Em__6B_m2569 (U3CLeaveU3Ec__AnonStorey58_t647 * __this, int32_t ___status, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = (__this->___callback_0);
		int32_t L_1 = ___status;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_0, ((((int32_t)L_1) > ((int32_t)0))? 1 : 0));
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_11.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_11MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59::.ctor()
extern "C" void U3CLeaveDuringTurnU3Ec__AnonStorey59__ctor_m2570 (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59::<>m__63(GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* Action_1_t890_il2cpp_TypeInfo_var;
extern MethodInfo* U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__6C_m2572_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3872_MethodInfo_var;
extern "C" void U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__63_m2571 (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * __this, MultiplayerParticipant_t672 * ___pendingParticipant, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t890_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(843);
		U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__6C_m2572_MethodInfo_var = il2cpp_codegen_method_info_from_index(388);
		Action_1__ctor_m3872_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484035);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeTurnBasedMultiplayerClient_t535 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		TurnBasedManager_t651 * L_1 = (L_0->___mTurnBasedManager_0);
		NativeTurnBasedMatch_t680 * L_2 = ___foundMatch;
		MultiplayerParticipant_t672 * L_3 = ___pendingParticipant;
		IntPtr_t L_4 = { U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__6C_m2572_MethodInfo_var };
		Action_1_t890 * L_5 = (Action_1_t890 *)il2cpp_codegen_object_new (Action_1_t890_il2cpp_TypeInfo_var);
		Action_1__ctor_m3872(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m3872_MethodInfo_var);
		NullCheck(L_1);
		TurnBasedManager_LeaveDuringMyTurn_m3094(L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59::<>m__6C(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C" void U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__6C_m2572 (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * __this, int32_t ___status, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = (__this->___callback_0);
		int32_t L_1 = ___status;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_0, ((((int32_t)L_1) > ((int32_t)0))? 1 : 0));
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_12.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_12MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::.ctor()
extern "C" void U3CCancelU3Ec__AnonStorey5A__ctor_m2573 (U3CCancelU3Ec__AnonStorey5A_t649 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::<>m__64(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* Action_1_t890_il2cpp_TypeInfo_var;
extern MethodInfo* U3CCancelU3Ec__AnonStorey5A_U3CU3Em__6D_m2575_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3872_MethodInfo_var;
extern "C" void U3CCancelU3Ec__AnonStorey5A_U3CU3Em__64_m2574 (U3CCancelU3Ec__AnonStorey5A_t649 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t890_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(843);
		U3CCancelU3Ec__AnonStorey5A_U3CU3Em__6D_m2575_MethodInfo_var = il2cpp_codegen_method_info_from_index(389);
		Action_1__ctor_m3872_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484035);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeTurnBasedMultiplayerClient_t535 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		TurnBasedManager_t651 * L_1 = (L_0->___mTurnBasedManager_0);
		NativeTurnBasedMatch_t680 * L_2 = ___foundMatch;
		IntPtr_t L_3 = { U3CCancelU3Ec__AnonStorey5A_U3CU3Em__6D_m2575_MethodInfo_var };
		Action_1_t890 * L_4 = (Action_1_t890 *)il2cpp_codegen_object_new (Action_1_t890_il2cpp_TypeInfo_var);
		Action_1__ctor_m3872(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m3872_MethodInfo_var);
		NullCheck(L_1);
		TurnBasedManager_CancelMatch_m3098(L_1, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::<>m__6D(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C" void U3CCancelU3Ec__AnonStorey5A_U3CU3Em__6D_m2575 (U3CCancelU3Ec__AnonStorey5A_t649 * __this, int32_t ___status, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = (__this->___callback_0);
		int32_t L_1 = ___status;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_0, ((((int32_t)L_1) > ((int32_t)0))? 1 : 0));
		return;
	}
}
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_13.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_13MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::.ctor()
extern "C" void U3CRematchU3Ec__AnonStorey5B__ctor_m2576 (U3CRematchU3Ec__AnonStorey5B_t650 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::<>m__65(System.Boolean)
extern "C" void U3CRematchU3Ec__AnonStorey5B_U3CU3Em__65_m2577 (U3CRematchU3Ec__AnonStorey5B_t650 * __this, bool ___failed, MethodInfo* method)
{
	{
		Action_2_t632 * L_0 = (__this->___callback_0);
		NullCheck(L_0);
		VirtActionInvoker2< bool, TurnBasedMatch_t353 * >::Invoke(10 /* System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(!0,!1) */, L_0, 0, (TurnBasedMatch_t353 *)NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::<>m__66(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CRematchU3Ec__AnonStorey5B_U3CU3Em__66_m2578 (U3CRematchU3Ec__AnonStorey5B_t650 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method)
{
	{
		NativeTurnBasedMultiplayerClient_t535 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		TurnBasedManager_t651 * L_1 = (L_0->___mTurnBasedManager_0);
		NativeTurnBasedMatch_t680 * L_2 = ___foundMatch;
		NativeTurnBasedMultiplayerClient_t535 * L_3 = (__this->___U3CU3Ef__this_1);
		Action_2_t632 * L_4 = (__this->___callback_0);
		NullCheck(L_3);
		Action_1_t864 * L_5 = NativeTurnBasedMultiplayerClient_BridgeMatchToUserCallback_m2582(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		TurnBasedManager_Rematch_m3099(L_1, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>
#include "mscorlib_System_Action_1_gen_45.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>
#include "mscorlib_System_Action_1_gen_46.h"
// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>
#include "System_Core_System_Action_2_gen_16.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// GooglePlayGames.Native.PInvoke.Callbacks
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_CallbacksMethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>
#include "mscorlib_System_Action_1_gen_45MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>
#include "mscorlib_System_Action_1_gen_46MethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>
#include "System_Core_System_Action_2_gen_16MethodDeclarations.h"
// System.NotImplementedException
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
struct Callbacks_t661;
struct Action_2_t632;
// GooglePlayGames.Native.PInvoke.Callbacks
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks.h"
struct Callbacks_t661;
struct Action_2_t932;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Byte,System.Object>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Byte,System.Object>(System.Action`2<!!0,!!1>)
extern "C" Action_2_t932 * Callbacks_AsOnGameThreadCallback_TisByte_t237_TisObject_t_m3874_gshared (Object_t * __this /* static, unused */, Action_2_t932 * p0, MethodInfo* method);
#define Callbacks_AsOnGameThreadCallback_TisByte_t237_TisObject_t_m3874(__this /* static, unused */, p0, method) (( Action_2_t932 * (*) (Object_t * /* static, unused */, Action_2_t932 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisByte_t237_TisObject_t_m3874_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>(System.Action`2<!!0,!!1>)
#define Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873(__this /* static, unused */, p0, method) (( Action_2_t632 * (*) (Object_t * /* static, unused */, Action_2_t632 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisByte_t237_TisObject_t_m3874_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_2_t652;
struct Callbacks_t661;
struct Action_2_t912;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Object,System.Byte>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Object,System.Byte>(System.Action`2<!!0,!!1>)
extern "C" Action_2_t912 * Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared (Object_t * __this /* static, unused */, Action_2_t912 * p0, MethodInfo* method);
#define Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748(__this /* static, unused */, p0, method) (( Action_2_t912 * (*) (Object_t * /* static, unused */, Action_2_t912 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>(System.Action`2<!!0,!!1>)
#define Callbacks_AsOnGameThreadCallback_TisTurnBasedMatch_t353_TisBoolean_t203_m3875(__this /* static, unused */, p0, method) (( Action_2_t652 * (*) (Object_t * /* static, unused */, Action_2_t652 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_1_t98;
struct Callbacks_t661;
struct Action_1_t910;
// Declaration System.Action`1<!!0> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Byte>(System.Action`1<!!0>)
// System.Action`1<!!0> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Byte>(System.Action`1<!!0>)
extern "C" Action_1_t910 * Callbacks_AsOnGameThreadCallback_TisByte_t237_m3877_gshared (Object_t * __this /* static, unused */, Action_1_t910 * p0, MethodInfo* method);
#define Callbacks_AsOnGameThreadCallback_TisByte_t237_m3877(__this /* static, unused */, p0, method) (( Action_1_t910 * (*) (Object_t * /* static, unused */, Action_1_t910 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisByte_t237_m3877_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`1<!!0> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Boolean>(System.Action`1<!!0>)
// System.Action`1<!!0> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Boolean>(System.Action`1<!!0>)
#define Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876(__this /* static, unused */, p0, method) (( Action_1_t98 * (*) (Object_t * /* static, unused */, Action_1_t98 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisByte_t237_m3877_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::.ctor(GooglePlayGames.Native.NativeClient,GooglePlayGames.Native.PInvoke.TurnBasedManager)
extern "C" void NativeTurnBasedMultiplayerClient__ctor_m2579 (NativeTurnBasedMultiplayerClient_t535 * __this, NativeClient_t524 * ___nativeClient, TurnBasedManager_t651 * ___manager, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		TurnBasedManager_t651 * L_0 = ___manager;
		__this->___mTurnBasedManager_0 = L_0;
		NativeClient_t524 * L_1 = ___nativeClient;
		__this->___mNativeClient_1 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::CreateQuickMatch(System.UInt32,System.UInt32,System.UInt32,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_CreateQuickMatch_m2580 (NativeTurnBasedMultiplayerClient_t535 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, Action_2_t632 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484038);
		s_Il2CppMethodIntialized = true;
	}
	TurnBasedMatchConfigBuilder_t712 * V_0 = {0};
	TurnBasedMatchConfig_t710 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Action_2_t632 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_2_t632 * L_1 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873(NULL /*static, unused*/, L_0, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var);
		___callback = L_1;
		TurnBasedMatchConfigBuilder_t712 * L_2 = TurnBasedMatchConfigBuilder_Create_m3120(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			TurnBasedMatchConfigBuilder_t712 * L_3 = V_0;
			uint32_t L_4 = ___variant;
			NullCheck(L_3);
			TurnBasedMatchConfigBuilder_SetVariant_m3113(L_3, L_4, /*hidden argument*/NULL);
			TurnBasedMatchConfigBuilder_t712 * L_5 = V_0;
			uint32_t L_6 = ___minOpponents;
			NullCheck(L_5);
			TurnBasedMatchConfigBuilder_SetMinimumAutomatchingPlayers_m3116(L_5, L_6, /*hidden argument*/NULL);
			TurnBasedMatchConfigBuilder_t712 * L_7 = V_0;
			uint32_t L_8 = ___maxOpponents;
			NullCheck(L_7);
			TurnBasedMatchConfigBuilder_SetMaximumAutomatchingPlayers_m3117(L_7, L_8, /*hidden argument*/NULL);
			TurnBasedMatchConfigBuilder_t712 * L_9 = V_0;
			NullCheck(L_9);
			TurnBasedMatchConfig_t710 * L_10 = TurnBasedMatchConfigBuilder_Build_m3118(L_9, /*hidden argument*/NULL);
			V_1 = L_10;
		}

IL_002e:
		try
		{ // begin try (depth: 2)
			TurnBasedManager_t651 * L_11 = (__this->___mTurnBasedManager_0);
			TurnBasedMatchConfig_t710 * L_12 = V_1;
			Action_2_t632 * L_13 = ___callback;
			Action_1_t864 * L_14 = NativeTurnBasedMultiplayerClient_BridgeMatchToUserCallback_m2582(__this, L_13, /*hidden argument*/NULL);
			NullCheck(L_11);
			TurnBasedManager_CreateMatch_m3083(L_11, L_12, L_14, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x54, FINALLY_0047);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_0047;
		}

FINALLY_0047:
		{ // begin finally (depth: 2)
			{
				TurnBasedMatchConfig_t710 * L_15 = V_1;
				if (!L_15)
				{
					goto IL_0053;
				}
			}

IL_004d:
			{
				TurnBasedMatchConfig_t710 * L_16 = V_1;
				NullCheck(L_16);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_16);
			}

IL_0053:
			{
				IL2CPP_END_FINALLY(71)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(71)
		{
			IL2CPP_JUMP_TBL(0x54, IL_0054)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		{
			TurnBasedMatchConfigBuilder_t712 * L_17 = V_0;
			if (!L_17)
			{
				goto IL_0065;
			}
		}

IL_005f:
		{
			TurnBasedMatchConfigBuilder_t712 * L_18 = V_0;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_18);
		}

IL_0065:
		{
			IL2CPP_END_FINALLY(89)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0066:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::CreateWithInvitationScreen(System.UInt32,System.UInt32,System.UInt32,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern TypeInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t885_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var;
extern MethodInfo* U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_U3CU3Em__57_m2543_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3840_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_CreateWithInvitationScreen_m2581 (NativeTurnBasedMultiplayerClient_t535 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, Action_2_t632 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(844);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t885_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(795);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484038);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_U3CU3Em__57_m2543_MethodInfo_var = il2cpp_codegen_method_info_from_index(391);
		Action_1__ctor_m3840_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		s_Il2CppMethodIntialized = true;
	}
	U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * V_0 = {0};
	{
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * L_0 = (U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 *)il2cpp_codegen_object_new (U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633_il2cpp_TypeInfo_var);
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D__ctor_m2542(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * L_1 = V_0;
		Action_2_t632 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * L_4 = V_0;
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * L_5 = V_0;
		NullCheck(L_5);
		Action_2_t632 * L_6 = (L_5->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_2_t632 * L_7 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873(NULL /*static, unused*/, L_6, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var);
		NullCheck(L_4);
		L_4->___callback_0 = L_7;
		TurnBasedManager_t651 * L_8 = (__this->___mTurnBasedManager_0);
		uint32_t L_9 = ___minOpponents;
		uint32_t L_10 = ___maxOpponents;
		U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * L_11 = V_0;
		IntPtr_t L_12 = { U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_U3CU3Em__57_m2543_MethodInfo_var };
		Action_1_t885 * L_13 = (Action_1_t885 *)il2cpp_codegen_object_new (Action_1_t885_il2cpp_TypeInfo_var);
		Action_1__ctor_m3840(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3840_MethodInfo_var);
		NullCheck(L_8);
		TurnBasedManager_ShowPlayerSelectUI_m3084(L_8, L_9, L_10, 1, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::BridgeMatchToUserCallback(System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern TypeInfo* U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t864_il2cpp_TypeInfo_var;
extern MethodInfo* U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_U3CU3Em__58_m2545_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3871_MethodInfo_var;
extern "C" Action_1_t864 * NativeTurnBasedMultiplayerClient_BridgeMatchToUserCallback_m2582 (NativeTurnBasedMultiplayerClient_t535 * __this, Action_2_t632 * ___userCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(845);
		Action_1_t864_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_U3CU3Em__58_m2545_MethodInfo_var = il2cpp_codegen_method_info_from_index(392);
		Action_1__ctor_m3871_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484031);
		s_Il2CppMethodIntialized = true;
	}
	U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * V_0 = {0};
	{
		U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * L_0 = (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 *)il2cpp_codegen_object_new (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634_il2cpp_TypeInfo_var);
		U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E__ctor_m2544(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * L_1 = V_0;
		Action_2_t632 * L_2 = ___userCallback;
		NullCheck(L_1);
		L_1->___userCallback_0 = L_2;
		U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * L_4 = V_0;
		IntPtr_t L_5 = { U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_U3CU3Em__58_m2545_MethodInfo_var };
		Action_1_t864 * L_6 = (Action_1_t864 *)il2cpp_codegen_object_new (Action_1_t864_il2cpp_TypeInfo_var);
		Action_1__ctor_m3871(L_6, L_4, L_5, /*hidden argument*/Action_1__ctor_m3871_MethodInfo_var);
		return L_6;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::AcceptFromInbox(System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern TypeInfo* U3CAcceptFromInboxU3Ec__AnonStorey4F_t635_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t899_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var;
extern MethodInfo* U3CAcceptFromInboxU3Ec__AnonStorey4F_U3CU3Em__59_m2547_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3878_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_AcceptFromInbox_m2583 (NativeTurnBasedMultiplayerClient_t535 * __this, Action_2_t632 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAcceptFromInboxU3Ec__AnonStorey4F_t635_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(847);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(848);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484038);
		U3CAcceptFromInboxU3Ec__AnonStorey4F_U3CU3Em__59_m2547_MethodInfo_var = il2cpp_codegen_method_info_from_index(393);
		Action_1__ctor_m3878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484042);
		s_Il2CppMethodIntialized = true;
	}
	U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * V_0 = {0};
	{
		U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * L_0 = (U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 *)il2cpp_codegen_object_new (U3CAcceptFromInboxU3Ec__AnonStorey4F_t635_il2cpp_TypeInfo_var);
		U3CAcceptFromInboxU3Ec__AnonStorey4F__ctor_m2546(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * L_1 = V_0;
		Action_2_t632 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * L_4 = V_0;
		U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * L_5 = V_0;
		NullCheck(L_5);
		Action_2_t632 * L_6 = (L_5->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_2_t632 * L_7 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873(NULL /*static, unused*/, L_6, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var);
		NullCheck(L_4);
		L_4->___callback_0 = L_7;
		TurnBasedManager_t651 * L_8 = (__this->___mTurnBasedManager_0);
		U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * L_9 = V_0;
		IntPtr_t L_10 = { U3CAcceptFromInboxU3Ec__AnonStorey4F_U3CU3Em__59_m2547_MethodInfo_var };
		Action_1_t899 * L_11 = (Action_1_t899 *)il2cpp_codegen_object_new (Action_1_t899_il2cpp_TypeInfo_var);
		Action_1__ctor_m3878(L_11, L_9, L_10, /*hidden argument*/Action_1__ctor_m3878_MethodInfo_var);
		NullCheck(L_8);
		TurnBasedManager_ShowInboxUI_m3092(L_8, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::AcceptInvitation(System.String,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern TypeInfo* U3CAcceptInvitationU3Ec__AnonStorey50_t636_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t637_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var;
extern MethodInfo* U3CAcceptInvitationU3Ec__AnonStorey50_U3CU3Em__5A_m2549_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3879_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_AcceptInvitation_m2584 (NativeTurnBasedMultiplayerClient_t535 * __this, String_t* ___invitationId, Action_2_t632 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAcceptInvitationU3Ec__AnonStorey50_t636_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(849);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t637_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(850);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484038);
		U3CAcceptInvitationU3Ec__AnonStorey50_U3CU3Em__5A_m2549_MethodInfo_var = il2cpp_codegen_method_info_from_index(395);
		Action_1__ctor_m3879_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484044);
		s_Il2CppMethodIntialized = true;
	}
	U3CAcceptInvitationU3Ec__AnonStorey50_t636 * V_0 = {0};
	{
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_0 = (U3CAcceptInvitationU3Ec__AnonStorey50_t636 *)il2cpp_codegen_object_new (U3CAcceptInvitationU3Ec__AnonStorey50_t636_il2cpp_TypeInfo_var);
		U3CAcceptInvitationU3Ec__AnonStorey50__ctor_m2548(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_1 = V_0;
		String_t* L_2 = ___invitationId;
		NullCheck(L_1);
		L_1->___invitationId_0 = L_2;
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_3 = V_0;
		Action_2_t632 * L_4 = ___callback;
		NullCheck(L_3);
		L_3->___callback_1 = L_4;
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_2 = __this;
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_6 = V_0;
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_7 = V_0;
		NullCheck(L_7);
		Action_2_t632 * L_8 = (L_7->___callback_1);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_2_t632 * L_9 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873(NULL /*static, unused*/, L_8, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var);
		NullCheck(L_6);
		L_6->___callback_1 = L_9;
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = (L_10->___invitationId_0);
		U3CAcceptInvitationU3Ec__AnonStorey50_t636 * L_12 = V_0;
		IntPtr_t L_13 = { U3CAcceptInvitationU3Ec__AnonStorey50_U3CU3Em__5A_m2549_MethodInfo_var };
		Action_1_t637 * L_14 = (Action_1_t637 *)il2cpp_codegen_object_new (Action_1_t637_il2cpp_TypeInfo_var);
		Action_1__ctor_m3879(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m3879_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindInvitationWithId_m2585(__this, L_11, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::FindInvitationWithId(System.String,System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>)
extern TypeInfo* U3CFindInvitationWithIdU3Ec__AnonStorey51_t638_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t898_il2cpp_TypeInfo_var;
extern MethodInfo* U3CFindInvitationWithIdU3Ec__AnonStorey51_U3CU3Em__5B_m2551_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3880_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_FindInvitationWithId_m2585 (NativeTurnBasedMultiplayerClient_t535 * __this, String_t* ___invitationId, Action_1_t637 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFindInvitationWithIdU3Ec__AnonStorey51_t638_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(852);
		Action_1_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(853);
		U3CFindInvitationWithIdU3Ec__AnonStorey51_U3CU3Em__5B_m2551_MethodInfo_var = il2cpp_codegen_method_info_from_index(397);
		Action_1__ctor_m3880_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484046);
		s_Il2CppMethodIntialized = true;
	}
	U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * V_0 = {0};
	{
		U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * L_0 = (U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 *)il2cpp_codegen_object_new (U3CFindInvitationWithIdU3Ec__AnonStorey51_t638_il2cpp_TypeInfo_var);
		U3CFindInvitationWithIdU3Ec__AnonStorey51__ctor_m2550(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * L_1 = V_0;
		Action_1_t637 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * L_3 = V_0;
		String_t* L_4 = ___invitationId;
		NullCheck(L_3);
		L_3->___invitationId_1 = L_4;
		TurnBasedManager_t651 * L_5 = (__this->___mTurnBasedManager_0);
		U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * L_6 = V_0;
		IntPtr_t L_7 = { U3CFindInvitationWithIdU3Ec__AnonStorey51_U3CU3Em__5B_m2551_MethodInfo_var };
		Action_1_t898 * L_8 = (Action_1_t898 *)il2cpp_codegen_object_new (Action_1_t898_il2cpp_TypeInfo_var);
		Action_1__ctor_m3880(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m3880_MethodInfo_var);
		NullCheck(L_5);
		TurnBasedManager_GetAllTurnbasedMatches_m3086(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::RegisterMatchDelegate(GooglePlayGames.BasicApi.Multiplayer.MatchDelegate)
extern TypeInfo* U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t652_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* U3CRegisterMatchDelegateU3Ec__AnonStorey52_U3CU3Em__5C_m2553_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3881_MethodInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisTurnBasedMatch_t353_TisBoolean_t203_m3875_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_RegisterMatchDelegate_m2586 (NativeTurnBasedMultiplayerClient_t535 * __this, MatchDelegate_t364 * ___del, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(854);
		Action_2_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(855);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		U3CRegisterMatchDelegateU3Ec__AnonStorey52_U3CU3Em__5C_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(399);
		Action_2__ctor_m3881_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484048);
		Callbacks_AsOnGameThreadCallback_TisTurnBasedMatch_t353_TisBoolean_t203_m3875_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484049);
		s_Il2CppMethodIntialized = true;
	}
	U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * V_0 = {0};
	{
		U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * L_0 = (U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 *)il2cpp_codegen_object_new (U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639_il2cpp_TypeInfo_var);
		U3CRegisterMatchDelegateU3Ec__AnonStorey52__ctor_m2552(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * L_1 = V_0;
		MatchDelegate_t364 * L_2 = ___del;
		NullCheck(L_1);
		L_1->___del_0 = L_2;
		U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * L_3 = V_0;
		NullCheck(L_3);
		MatchDelegate_t364 * L_4 = (L_3->___del_0);
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		il2cpp_codegen_memory_barrier();
		__this->___mMatchDelegate_2 = (Action_2_t652 *)NULL;
		goto IL_003f;
	}

IL_0026:
	{
		U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * L_5 = V_0;
		IntPtr_t L_6 = { U3CRegisterMatchDelegateU3Ec__AnonStorey52_U3CU3Em__5C_m2553_MethodInfo_var };
		Action_2_t652 * L_7 = (Action_2_t652 *)il2cpp_codegen_object_new (Action_2_t652_il2cpp_TypeInfo_var);
		Action_2__ctor_m3881(L_7, L_5, L_6, /*hidden argument*/Action_2__ctor_m3881_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_2_t652 * L_8 = Callbacks_AsOnGameThreadCallback_TisTurnBasedMatch_t353_TisBoolean_t203_m3875(NULL /*static, unused*/, L_7, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisTurnBasedMatch_t353_TisBoolean_t203_m3875_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		__this->___mMatchDelegate_2 = L_8;
	}

IL_003f:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::HandleMatchEvent(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_HandleMatchEvent_m2587 (NativeTurnBasedMultiplayerClient_t535 * __this, int32_t ___eventType, String_t* ___matchId, NativeTurnBasedMatch_t680 * ___match, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	Action_2_t652 * V_0 = {0};
	bool V_1 = false;
	{
		Action_2_t652 * L_0 = (__this->___mMatchDelegate_2);
		il2cpp_codegen_memory_barrier();
		V_0 = L_0;
		Action_2_t652 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		int32_t L_2 = ___eventType;
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_3 = ___matchId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral545, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		int32_t L_5 = ___eventType;
		V_1 = ((((int32_t)L_5) == ((int32_t)2))? 1 : 0);
		Action_2_t652 * L_6 = V_0;
		NativeTurnBasedMatch_t680 * L_7 = ___match;
		NativeClient_t524 * L_8 = (__this->___mNativeClient_1);
		NullCheck(L_8);
		String_t* L_9 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeClient::GetUserId() */, L_8);
		NullCheck(L_7);
		TurnBasedMatch_t353 * L_10 = NativeTurnBasedMatch_AsTurnBasedMatch_m2842(L_7, L_9, /*hidden argument*/NULL);
		bool L_11 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< TurnBasedMatch_t353 *, bool >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::Invoke(!0,!1) */, L_6, L_10, L_11);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::TakeTurn(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Byte[],System.String,System.Action`1<System.Boolean>)
extern TypeInfo* U3CTakeTurnU3Ec__AnonStorey53_t640_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t643_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var;
extern MethodInfo* U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__5D_m2555_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3882_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_TakeTurn_m2588 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, ByteU5BU5D_t350* ___data, String_t* ___pendingParticipantId, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CTakeTurnU3Ec__AnonStorey53_t640_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(856);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_2_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484050);
		U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__5D_m2555_MethodInfo_var = il2cpp_codegen_method_info_from_index(403);
		Action_2__ctor_m3882_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484052);
		s_Il2CppMethodIntialized = true;
	}
	U3CTakeTurnU3Ec__AnonStorey53_t640 * V_0 = {0};
	{
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_0 = (U3CTakeTurnU3Ec__AnonStorey53_t640 *)il2cpp_codegen_object_new (U3CTakeTurnU3Ec__AnonStorey53_t640_il2cpp_TypeInfo_var);
		U3CTakeTurnU3Ec__AnonStorey53__ctor_m2554(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_1 = V_0;
		ByteU5BU5D_t350* L_2 = ___data;
		NullCheck(L_1);
		L_1->___data_0 = L_2;
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_3 = V_0;
		Action_1_t98 * L_4 = ___callback;
		NullCheck(L_3);
		L_3->___callback_1 = L_4;
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_2 = __this;
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_6 = V_0;
		NullCheck(L_6);
		ByteU5BU5D_t350* L_7 = (L_6->___data_0);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_describe_m1594(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_8 = V_0;
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t98 * L_10 = (L_9->___callback_1);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t98 * L_11 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876(NULL /*static, unused*/, L_10, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var);
		NullCheck(L_8);
		L_8->___callback_1 = L_11;
		TurnBasedMatch_t353 * L_12 = ___match;
		String_t* L_13 = ___pendingParticipantId;
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_14 = V_0;
		NullCheck(L_14);
		Action_1_t98 * L_15 = (L_14->___callback_1);
		U3CTakeTurnU3Ec__AnonStorey53_t640 * L_16 = V_0;
		IntPtr_t L_17 = { U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__5D_m2555_MethodInfo_var };
		Action_2_t643 * L_18 = (Action_2_t643 *)il2cpp_codegen_object_new (Action_2_t643_il2cpp_TypeInfo_var);
		Action_2__ctor_m3882(L_18, L_16, L_17, /*hidden argument*/Action_2__ctor_m3882_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatchWithParticipant_m2590(__this, L_12, L_13, L_15, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::FindEqualVersionMatch(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>,System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>)
extern TypeInfo* U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t864_il2cpp_TypeInfo_var;
extern MethodInfo* U3CFindEqualVersionMatchU3Ec__AnonStorey54_U3CU3Em__5E_m2558_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3871_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___onFailure, Action_1_t641 * ___onVersionMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(858);
		Action_1_t864_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_U3CU3Em__5E_m2558_MethodInfo_var = il2cpp_codegen_method_info_from_index(405);
		Action_1__ctor_m3871_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484031);
		s_Il2CppMethodIntialized = true;
	}
	U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * V_0 = {0};
	{
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * L_0 = (U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 *)il2cpp_codegen_object_new (U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642_il2cpp_TypeInfo_var);
		U3CFindEqualVersionMatchU3Ec__AnonStorey54__ctor_m2557(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * L_1 = V_0;
		TurnBasedMatch_t353 * L_2 = ___match;
		NullCheck(L_1);
		L_1->___match_0 = L_2;
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * L_3 = V_0;
		Action_1_t98 * L_4 = ___onFailure;
		NullCheck(L_3);
		L_3->___onFailure_1 = L_4;
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * L_5 = V_0;
		Action_1_t641 * L_6 = ___onVersionMatch;
		NullCheck(L_5);
		L_5->___onVersionMatch_2 = L_6;
		TurnBasedManager_t651 * L_7 = (__this->___mTurnBasedManager_0);
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * L_8 = V_0;
		NullCheck(L_8);
		TurnBasedMatch_t353 * L_9 = (L_8->___match_0);
		NullCheck(L_9);
		String_t* L_10 = TurnBasedMatch_get_MatchId_m1404(L_9, /*hidden argument*/NULL);
		U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * L_11 = V_0;
		IntPtr_t L_12 = { U3CFindEqualVersionMatchU3Ec__AnonStorey54_U3CU3Em__5E_m2558_MethodInfo_var };
		Action_1_t864 * L_13 = (Action_1_t864 *)il2cpp_codegen_object_new (Action_1_t864_il2cpp_TypeInfo_var);
		Action_1__ctor_m3871(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3871_MethodInfo_var);
		NullCheck(L_7);
		TurnBasedManager_GetMatch_m3081(L_7, L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::FindEqualVersionMatchWithParticipant(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.String,System.Action`1<System.Boolean>,System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>)
extern TypeInfo* U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t641_il2cpp_TypeInfo_var;
extern MethodInfo* U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_U3CU3Em__5F_m2560_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3883_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_FindEqualVersionMatchWithParticipant_m2590 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, String_t* ___participantId, Action_1_t98 * ___onFailure, Action_2_t643 * ___onFoundParticipantAndMatch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(859);
		Action_1_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(860);
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_U3CU3Em__5F_m2560_MethodInfo_var = il2cpp_codegen_method_info_from_index(406);
		Action_1__ctor_m3883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484055);
		s_Il2CppMethodIntialized = true;
	}
	U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * V_0 = {0};
	{
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_0 = (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 *)il2cpp_codegen_object_new (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644_il2cpp_TypeInfo_var);
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55__ctor_m2559(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_1 = V_0;
		String_t* L_2 = ___participantId;
		NullCheck(L_1);
		L_1->___participantId_0 = L_2;
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_3 = V_0;
		Action_2_t643 * L_4 = ___onFoundParticipantAndMatch;
		NullCheck(L_3);
		L_3->___onFoundParticipantAndMatch_1 = L_4;
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_5 = V_0;
		TurnBasedMatch_t353 * L_6 = ___match;
		NullCheck(L_5);
		L_5->___match_2 = L_6;
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_7 = V_0;
		Action_1_t98 * L_8 = ___onFailure;
		NullCheck(L_7);
		L_7->___onFailure_3 = L_8;
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_9 = V_0;
		NullCheck(L_9);
		TurnBasedMatch_t353 * L_10 = (L_9->___match_2);
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_11 = V_0;
		NullCheck(L_11);
		Action_1_t98 * L_12 = (L_11->___onFailure_3);
		U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * L_13 = V_0;
		IntPtr_t L_14 = { U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_U3CU3Em__5F_m2560_MethodInfo_var };
		Action_1_t641 * L_15 = (Action_1_t641 *)il2cpp_codegen_object_new (Action_1_t641_il2cpp_TypeInfo_var);
		Action_1__ctor_m3883(L_15, L_13, L_14, /*hidden argument*/Action_1__ctor_m3883_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589(__this, L_10, L_12, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::GetMaxMatchDataSize()
extern TypeInfo* NotImplementedException_t933_il2cpp_TypeInfo_var;
extern "C" int32_t NativeTurnBasedMultiplayerClient_GetMaxMatchDataSize_m2591 (NativeTurnBasedMultiplayerClient_t535 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t933_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t933 * L_0 = (NotImplementedException_t933 *)il2cpp_codegen_object_new (NotImplementedException_t933_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3884(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Finish(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Byte[],GooglePlayGames.BasicApi.Multiplayer.MatchOutcome,System.Action`1<System.Boolean>)
extern TypeInfo* U3CFinishU3Ec__AnonStorey56_t645_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t641_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var;
extern MethodInfo* U3CFinishU3Ec__AnonStorey56_U3CU3Em__60_m2562_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3883_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_Finish_m2592 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, ByteU5BU5D_t350* ___data, MatchOutcome_t345 * ___outcome, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFinishU3Ec__AnonStorey56_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(862);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(860);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484050);
		U3CFinishU3Ec__AnonStorey56_U3CU3Em__60_m2562_MethodInfo_var = il2cpp_codegen_method_info_from_index(408);
		Action_1__ctor_m3883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484055);
		s_Il2CppMethodIntialized = true;
	}
	U3CFinishU3Ec__AnonStorey56_t645 * V_0 = {0};
	{
		U3CFinishU3Ec__AnonStorey56_t645 * L_0 = (U3CFinishU3Ec__AnonStorey56_t645 *)il2cpp_codegen_object_new (U3CFinishU3Ec__AnonStorey56_t645_il2cpp_TypeInfo_var);
		U3CFinishU3Ec__AnonStorey56__ctor_m2561(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFinishU3Ec__AnonStorey56_t645 * L_1 = V_0;
		MatchOutcome_t345 * L_2 = ___outcome;
		NullCheck(L_1);
		L_1->___outcome_0 = L_2;
		U3CFinishU3Ec__AnonStorey56_t645 * L_3 = V_0;
		Action_1_t98 * L_4 = ___callback;
		NullCheck(L_3);
		L_3->___callback_1 = L_4;
		U3CFinishU3Ec__AnonStorey56_t645 * L_5 = V_0;
		ByteU5BU5D_t350* L_6 = ___data;
		NullCheck(L_5);
		L_5->___data_2 = L_6;
		U3CFinishU3Ec__AnonStorey56_t645 * L_7 = V_0;
		NullCheck(L_7);
		L_7->___U3CU3Ef__this_3 = __this;
		U3CFinishU3Ec__AnonStorey56_t645 * L_8 = V_0;
		U3CFinishU3Ec__AnonStorey56_t645 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t98 * L_10 = (L_9->___callback_1);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t98 * L_11 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876(NULL /*static, unused*/, L_10, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var);
		NullCheck(L_8);
		L_8->___callback_1 = L_11;
		TurnBasedMatch_t353 * L_12 = ___match;
		U3CFinishU3Ec__AnonStorey56_t645 * L_13 = V_0;
		NullCheck(L_13);
		Action_1_t98 * L_14 = (L_13->___callback_1);
		U3CFinishU3Ec__AnonStorey56_t645 * L_15 = V_0;
		IntPtr_t L_16 = { U3CFinishU3Ec__AnonStorey56_U3CU3Em__60_m2562_MethodInfo_var };
		Action_1_t641 * L_17 = (Action_1_t641 *)il2cpp_codegen_object_new (Action_1_t641_il2cpp_TypeInfo_var);
		Action_1__ctor_m3883(L_17, L_15, L_16, /*hidden argument*/Action_1__ctor_m3883_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589(__this, L_12, L_14, L_17, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::ResultToMatchResult(GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult)
extern TypeInfo* ParticipantResult_t342_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" int32_t NativeTurnBasedMultiplayerClient_ResultToMatchResult_m2593 (Object_t * __this /* static, unused */, int32_t ___result, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticipantResult_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___result;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001f;
		}
		if (L_1 == 1)
		{
			goto IL_0023;
		}
		if (L_1 == 2)
		{
			goto IL_001d;
		}
		if (L_1 == 3)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0025;
	}

IL_001d:
	{
		return (int32_t)(3);
	}

IL_001f:
	{
		return (int32_t)(4);
	}

IL_0021:
	{
		return (int32_t)(5);
	}

IL_0023:
	{
		return (int32_t)(6);
	}

IL_0025:
	{
		int32_t L_2 = ___result;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(ParticipantResult_t342_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral546, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (int32_t)(4);
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::AcknowledgeFinished(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>)
extern TypeInfo* U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t641_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var;
extern MethodInfo* U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__61_m2565_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3883_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_AcknowledgeFinished_m2594 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(863);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(860);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484050);
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__61_m2565_MethodInfo_var = il2cpp_codegen_method_info_from_index(409);
		Action_1__ctor_m3883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484055);
		s_Il2CppMethodIntialized = true;
	}
	U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * V_0 = {0};
	{
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * L_0 = (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 *)il2cpp_codegen_object_new (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646_il2cpp_TypeInfo_var);
		U3CAcknowledgeFinishedU3Ec__AnonStorey57__ctor_m2564(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * L_1 = V_0;
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * L_4 = V_0;
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * L_5 = V_0;
		NullCheck(L_5);
		Action_1_t98 * L_6 = (L_5->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t98 * L_7 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876(NULL /*static, unused*/, L_6, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var);
		NullCheck(L_4);
		L_4->___callback_0 = L_7;
		TurnBasedMatch_t353 * L_8 = ___match;
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t98 * L_10 = (L_9->___callback_0);
		U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * L_11 = V_0;
		IntPtr_t L_12 = { U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__61_m2565_MethodInfo_var };
		Action_1_t641 * L_13 = (Action_1_t641 *)il2cpp_codegen_object_new (Action_1_t641_il2cpp_TypeInfo_var);
		Action_1__ctor_m3883(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3883_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589(__this, L_8, L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Leave(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>)
extern TypeInfo* U3CLeaveU3Ec__AnonStorey58_t647_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t641_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var;
extern MethodInfo* U3CLeaveU3Ec__AnonStorey58_U3CU3Em__62_m2568_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3883_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_Leave_m2595 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLeaveU3Ec__AnonStorey58_t647_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(864);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(860);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484050);
		U3CLeaveU3Ec__AnonStorey58_U3CU3Em__62_m2568_MethodInfo_var = il2cpp_codegen_method_info_from_index(410);
		Action_1__ctor_m3883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484055);
		s_Il2CppMethodIntialized = true;
	}
	U3CLeaveU3Ec__AnonStorey58_t647 * V_0 = {0};
	{
		U3CLeaveU3Ec__AnonStorey58_t647 * L_0 = (U3CLeaveU3Ec__AnonStorey58_t647 *)il2cpp_codegen_object_new (U3CLeaveU3Ec__AnonStorey58_t647_il2cpp_TypeInfo_var);
		U3CLeaveU3Ec__AnonStorey58__ctor_m2567(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLeaveU3Ec__AnonStorey58_t647 * L_1 = V_0;
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CLeaveU3Ec__AnonStorey58_t647 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CLeaveU3Ec__AnonStorey58_t647 * L_4 = V_0;
		U3CLeaveU3Ec__AnonStorey58_t647 * L_5 = V_0;
		NullCheck(L_5);
		Action_1_t98 * L_6 = (L_5->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t98 * L_7 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876(NULL /*static, unused*/, L_6, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var);
		NullCheck(L_4);
		L_4->___callback_0 = L_7;
		TurnBasedMatch_t353 * L_8 = ___match;
		U3CLeaveU3Ec__AnonStorey58_t647 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t98 * L_10 = (L_9->___callback_0);
		U3CLeaveU3Ec__AnonStorey58_t647 * L_11 = V_0;
		IntPtr_t L_12 = { U3CLeaveU3Ec__AnonStorey58_U3CU3Em__62_m2568_MethodInfo_var };
		Action_1_t641 * L_13 = (Action_1_t641 *)il2cpp_codegen_object_new (Action_1_t641_il2cpp_TypeInfo_var);
		Action_1__ctor_m3883(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3883_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589(__this, L_8, L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::LeaveDuringTurn(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.String,System.Action`1<System.Boolean>)
extern TypeInfo* U3CLeaveDuringTurnU3Ec__AnonStorey59_t648_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t643_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var;
extern MethodInfo* U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__63_m2571_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3882_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_LeaveDuringTurn_m2596 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, String_t* ___pendingParticipantId, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(865);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_2_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484050);
		U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__63_m2571_MethodInfo_var = il2cpp_codegen_method_info_from_index(411);
		Action_2__ctor_m3882_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484052);
		s_Il2CppMethodIntialized = true;
	}
	U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * V_0 = {0};
	{
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * L_0 = (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 *)il2cpp_codegen_object_new (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648_il2cpp_TypeInfo_var);
		U3CLeaveDuringTurnU3Ec__AnonStorey59__ctor_m2570(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * L_1 = V_0;
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * L_4 = V_0;
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * L_5 = V_0;
		NullCheck(L_5);
		Action_1_t98 * L_6 = (L_5->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t98 * L_7 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876(NULL /*static, unused*/, L_6, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var);
		NullCheck(L_4);
		L_4->___callback_0 = L_7;
		TurnBasedMatch_t353 * L_8 = ___match;
		String_t* L_9 = ___pendingParticipantId;
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * L_10 = V_0;
		NullCheck(L_10);
		Action_1_t98 * L_11 = (L_10->___callback_0);
		U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * L_12 = V_0;
		IntPtr_t L_13 = { U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__63_m2571_MethodInfo_var };
		Action_2_t643 * L_14 = (Action_2_t643 *)il2cpp_codegen_object_new (Action_2_t643_il2cpp_TypeInfo_var);
		Action_2__ctor_m3882(L_14, L_12, L_13, /*hidden argument*/Action_2__ctor_m3882_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatchWithParticipant_m2590(__this, L_8, L_9, L_11, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Cancel(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>)
extern TypeInfo* U3CCancelU3Ec__AnonStorey5A_t649_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t641_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var;
extern MethodInfo* U3CCancelU3Ec__AnonStorey5A_U3CU3Em__64_m2574_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3883_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_Cancel_m2597 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCancelU3Ec__AnonStorey5A_t649_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(866);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(860);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484050);
		U3CCancelU3Ec__AnonStorey5A_U3CU3Em__64_m2574_MethodInfo_var = il2cpp_codegen_method_info_from_index(412);
		Action_1__ctor_m3883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484055);
		s_Il2CppMethodIntialized = true;
	}
	U3CCancelU3Ec__AnonStorey5A_t649 * V_0 = {0};
	{
		U3CCancelU3Ec__AnonStorey5A_t649 * L_0 = (U3CCancelU3Ec__AnonStorey5A_t649 *)il2cpp_codegen_object_new (U3CCancelU3Ec__AnonStorey5A_t649_il2cpp_TypeInfo_var);
		U3CCancelU3Ec__AnonStorey5A__ctor_m2573(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCancelU3Ec__AnonStorey5A_t649 * L_1 = V_0;
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CCancelU3Ec__AnonStorey5A_t649 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CCancelU3Ec__AnonStorey5A_t649 * L_4 = V_0;
		U3CCancelU3Ec__AnonStorey5A_t649 * L_5 = V_0;
		NullCheck(L_5);
		Action_1_t98 * L_6 = (L_5->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t98 * L_7 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876(NULL /*static, unused*/, L_6, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_m3876_MethodInfo_var);
		NullCheck(L_4);
		L_4->___callback_0 = L_7;
		TurnBasedMatch_t353 * L_8 = ___match;
		U3CCancelU3Ec__AnonStorey5A_t649 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t98 * L_10 = (L_9->___callback_0);
		U3CCancelU3Ec__AnonStorey5A_t649 * L_11 = V_0;
		IntPtr_t L_12 = { U3CCancelU3Ec__AnonStorey5A_U3CU3Em__64_m2574_MethodInfo_var };
		Action_1_t641 * L_13 = (Action_1_t641 *)il2cpp_codegen_object_new (Action_1_t641_il2cpp_TypeInfo_var);
		Action_1__ctor_m3883(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3883_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589(__this, L_8, L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Rematch(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern TypeInfo* U3CRematchU3Ec__AnonStorey5B_t650_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t98_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t641_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var;
extern MethodInfo* U3CRematchU3Ec__AnonStorey5B_U3CU3Em__65_m2577_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m959_MethodInfo_var;
extern MethodInfo* U3CRematchU3Ec__AnonStorey5B_U3CU3Em__66_m2578_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3883_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_Rematch_m2598 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_2_t632 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRematchU3Ec__AnonStorey5B_t650_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(867);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		Action_1_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(860);
		Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484038);
		U3CRematchU3Ec__AnonStorey5B_U3CU3Em__65_m2577_MethodInfo_var = il2cpp_codegen_method_info_from_index(413);
		Action_1__ctor_m959_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		U3CRematchU3Ec__AnonStorey5B_U3CU3Em__66_m2578_MethodInfo_var = il2cpp_codegen_method_info_from_index(414);
		Action_1__ctor_m3883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484055);
		s_Il2CppMethodIntialized = true;
	}
	U3CRematchU3Ec__AnonStorey5B_t650 * V_0 = {0};
	{
		U3CRematchU3Ec__AnonStorey5B_t650 * L_0 = (U3CRematchU3Ec__AnonStorey5B_t650 *)il2cpp_codegen_object_new (U3CRematchU3Ec__AnonStorey5B_t650_il2cpp_TypeInfo_var);
		U3CRematchU3Ec__AnonStorey5B__ctor_m2576(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRematchU3Ec__AnonStorey5B_t650 * L_1 = V_0;
		Action_2_t632 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CRematchU3Ec__AnonStorey5B_t650 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CRematchU3Ec__AnonStorey5B_t650 * L_4 = V_0;
		U3CRematchU3Ec__AnonStorey5B_t650 * L_5 = V_0;
		NullCheck(L_5);
		Action_2_t632 * L_6 = (L_5->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_2_t632 * L_7 = Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873(NULL /*static, unused*/, L_6, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisBoolean_t203_TisTurnBasedMatch_t353_m3873_MethodInfo_var);
		NullCheck(L_4);
		L_4->___callback_0 = L_7;
		TurnBasedMatch_t353 * L_8 = ___match;
		U3CRematchU3Ec__AnonStorey5B_t650 * L_9 = V_0;
		IntPtr_t L_10 = { U3CRematchU3Ec__AnonStorey5B_U3CU3Em__65_m2577_MethodInfo_var };
		Action_1_t98 * L_11 = (Action_1_t98 *)il2cpp_codegen_object_new (Action_1_t98_il2cpp_TypeInfo_var);
		Action_1__ctor_m959(L_11, L_9, L_10, /*hidden argument*/Action_1__ctor_m959_MethodInfo_var);
		U3CRematchU3Ec__AnonStorey5B_t650 * L_12 = V_0;
		IntPtr_t L_13 = { U3CRematchU3Ec__AnonStorey5B_U3CU3Em__66_m2578_MethodInfo_var };
		Action_1_t641 * L_14 = (Action_1_t641 *)il2cpp_codegen_object_new (Action_1_t641_il2cpp_TypeInfo_var);
		Action_1__ctor_m3883(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m3883_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589(__this, L_8, L_11, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::DeclineInvitation(System.String)
extern TypeInfo* Action_1_t637_il2cpp_TypeInfo_var;
extern MethodInfo* NativeTurnBasedMultiplayerClient_U3CDeclineInvitationU3Em__67_m2600_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3879_MethodInfo_var;
extern "C" void NativeTurnBasedMultiplayerClient_DeclineInvitation_m2599 (NativeTurnBasedMultiplayerClient_t535 * __this, String_t* ___invitationId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t637_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(850);
		NativeTurnBasedMultiplayerClient_U3CDeclineInvitationU3Em__67_m2600_MethodInfo_var = il2cpp_codegen_method_info_from_index(415);
		Action_1__ctor_m3879_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484044);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___invitationId;
		IntPtr_t L_1 = { NativeTurnBasedMultiplayerClient_U3CDeclineInvitationU3Em__67_m2600_MethodInfo_var };
		Action_1_t637 * L_2 = (Action_1_t637 *)il2cpp_codegen_object_new (Action_1_t637_il2cpp_TypeInfo_var);
		Action_1__ctor_m3879(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m3879_MethodInfo_var);
		NativeTurnBasedMultiplayerClient_FindInvitationWithId_m2585(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::<DeclineInvitation>m__67(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C" void NativeTurnBasedMultiplayerClient_U3CDeclineInvitationU3Em__67_m2600 (NativeTurnBasedMultiplayerClient_t535 * __this, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method)
{
	{
		MultiplayerInvitation_t601 * L_0 = ___invitation;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		TurnBasedManager_t651 * L_1 = (__this->___mTurnBasedManager_0);
		MultiplayerInvitation_t601 * L_2 = ___invitation;
		NullCheck(L_1);
		TurnBasedManager_DeclineInvitation_m3089(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_AchievementMethodDeclarations.h"

// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.NativeAchievement
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeAchievement.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_3MethodDeclarations.h"
// GooglePlayGames.Native.NativeAchievement
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeAchievementMethodDeclarations.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::.ctor(System.IntPtr)
extern "C" void FetchResponse__ctor_m2601 (FetchResponse_t653 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::Status()
extern "C" int32_t FetchResponse_Status_m2602 (FetchResponse_t653 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = AchievementManager_AchievementManager_FetchResponse_GetStatus_m1651(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::Achievement()
extern TypeInfo* NativeAchievement_t673_il2cpp_TypeInfo_var;
extern "C" NativeAchievement_t673 * FetchResponse_Achievement_m2603 (FetchResponse_t653 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeAchievement_t673_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = AchievementManager_AchievementManager_FetchResponse_GetData_m1652(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		NativeAchievement_t673 * L_3 = (NativeAchievement_t673 *)il2cpp_codegen_object_new (NativeAchievement_t673_il2cpp_TypeInfo_var);
		NativeAchievement__ctor_m2725(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchResponse_CallDispose_m2604 (FetchResponse_t653 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		AchievementManager_AchievementManager_FetchResponse_Dispose_m1650(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* FetchResponse_t653_il2cpp_TypeInfo_var;
extern "C" FetchResponse_t653 * FetchResponse_FromPointer_m2605 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		FetchResponse_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(683);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchResponse_t653 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		FetchResponse_t653 * L_5 = (FetchResponse_t653 *)il2cpp_codegen_object_new (FetchResponse_t653_il2cpp_TypeInfo_var);
		FetchResponse__ctor_m2601(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_0MethodDeclarations.h"

// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>
#include "System_Core_System_Func_2_gen_5.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtrMethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>
#include "System_Core_System_Func_2_gen_5MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtil_0MethodDeclarations.h"
struct PInvokeUtilities_t682;
struct IEnumerator_1_t865;
struct Func_2_t934;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtil_0.h"
struct PInvokeUtilities_t682;
struct IEnumerator_1_t166;
struct Func_2_t935;
// Declaration System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<System.Object>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
// System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<System.Object>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
extern "C" Object_t* PInvokeUtilities_ToEnumerator_TisObject_t_m3887_gshared (Object_t * __this /* static, unused */, UIntPtr_t  p0, Func_2_t935 * p1, MethodInfo* method);
#define PInvokeUtilities_ToEnumerator_TisObject_t_m3887(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, UIntPtr_t , Func_2_t935 *, MethodInfo*))PInvokeUtilities_ToEnumerator_TisObject_t_m3887_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<GooglePlayGames.Native.NativeAchievement>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
// System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<GooglePlayGames.Native.NativeAchievement>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerator_TisNativeAchievement_t673_m3886(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, UIntPtr_t , Func_2_t934 *, MethodInfo*))PInvokeUtilities_ToEnumerator_TisObject_t_m3887_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C" void FetchAllResponse__ctor_m2606 (FetchAllResponse_t655 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * FetchAllResponse_System_Collections_IEnumerable_GetEnumerator_m2607 (FetchAllResponse_t655 * __this, MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(7 /* System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.NativeAchievement> GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetEnumerator() */, __this);
		return L_0;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::Status()
extern "C" int32_t FetchAllResponse_Status_m2608 (FetchAllResponse_t655 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = AchievementManager_AchievementManager_FetchAllResponse_GetStatus_m1647(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::Length()
extern "C" UIntPtr_t  FetchAllResponse_Length_m2609 (FetchAllResponse_t655 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = AchievementManager_AchievementManager_FetchAllResponse_GetData_Length_m1648(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetElement(System.UIntPtr)
extern TypeInfo* ArgumentOutOfRangeException_t909_il2cpp_TypeInfo_var;
extern TypeInfo* NativeAchievement_t673_il2cpp_TypeInfo_var;
extern "C" NativeAchievement_t673 * FetchAllResponse_GetElement_m2610 (FetchAllResponse_t655 * __this, UIntPtr_t  ___index, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(638);
		NativeAchievement_t673_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	UIntPtr_t  V_0 = {0};
	{
		uint64_t L_0 = UIntPtr_ToUInt64_m3888((&___index), /*hidden argument*/NULL);
		UIntPtr_t  L_1 = FetchAllResponse_Length_m2609(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		uint64_t L_2 = UIntPtr_ToUInt64_m3888((&V_0), /*hidden argument*/NULL);
		if ((!(((uint64_t)L_0) >= ((uint64_t)L_2))))
		{
			goto IL_0020;
		}
	}
	{
		ArgumentOutOfRangeException_t909 * L_3 = (ArgumentOutOfRangeException_t909 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t909_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3889(L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0020:
	{
		HandleRef_t657  L_4 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_5 = ___index;
		IntPtr_t L_6 = AchievementManager_AchievementManager_FetchAllResponse_GetData_GetElement_m1649(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NativeAchievement_t673 * L_7 = (NativeAchievement_t673 *)il2cpp_codegen_object_new (NativeAchievement_t673_il2cpp_TypeInfo_var);
		NativeAchievement__ctor_m2725(L_7, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.NativeAchievement> GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetEnumerator()
extern TypeInfo* Func_2_t934_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* FetchAllResponse_U3CGetEnumeratorU3Em__71_m2614_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3890_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_ToEnumerator_TisNativeAchievement_t673_m3886_MethodInfo_var;
extern "C" Object_t* FetchAllResponse_GetEnumerator_m2611 (FetchAllResponse_t655 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Func_2_t934_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(868);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		FetchAllResponse_U3CGetEnumeratorU3Em__71_m2614_MethodInfo_var = il2cpp_codegen_method_info_from_index(416);
		Func_2__ctor_m3890_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484065);
		PInvokeUtilities_ToEnumerator_TisNativeAchievement_t673_m3886_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484066);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = AchievementManager_AchievementManager_FetchAllResponse_GetData_Length_m1648(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = { FetchAllResponse_U3CGetEnumeratorU3Em__71_m2614_MethodInfo_var };
		Func_2_t934 * L_3 = (Func_2_t934 *)il2cpp_codegen_object_new (Func_2_t934_il2cpp_TypeInfo_var);
		Func_2__ctor_m3890(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m3890_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		Object_t* L_4 = PInvokeUtilities_ToEnumerator_TisNativeAchievement_t673_m3886(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerator_TisNativeAchievement_t673_m3886_MethodInfo_var);
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchAllResponse_CallDispose_m2612 (FetchAllResponse_t655 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		AchievementManager_AchievementManager_FetchAllResponse_Dispose_m1646(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* FetchAllResponse_t655_il2cpp_TypeInfo_var;
extern "C" FetchAllResponse_t655 * FetchAllResponse_FromPointer_m2613 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		FetchAllResponse_t655_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(673);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchAllResponse_t655 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		FetchAllResponse_t655 * L_5 = (FetchAllResponse_t655 *)il2cpp_codegen_object_new (FetchAllResponse_t655_il2cpp_TypeInfo_var);
		FetchAllResponse__ctor_m2606(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::<GetEnumerator>m__71(System.UIntPtr)
extern "C" NativeAchievement_t673 * FetchAllResponse_U3CGetEnumeratorU3Em__71_m2614 (FetchAllResponse_t655 * __this, UIntPtr_t  ___index, MethodInfo* method)
{
	{
		UIntPtr_t  L_0 = ___index;
		NativeAchievement_t673 * L_1 = FetchAllResponse_GetElement_m2610(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.PInvoke.AchievementManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.AchievementManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_1MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.GameServices
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService.h"
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
#include "mscorlib_System_Action_1_gen_21.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_2.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_17.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_0.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>
#include "System_Core_System_Func_2_gen_6.h"
// GooglePlayGames.Native.PInvoke.Callbacks/Type
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks_T.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_20.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_1.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>
#include "System_Core_System_Func_2_gen_7.h"
// GooglePlayGames.Native.PInvoke.GameServices
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameServiceMethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_2MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_0MethodDeclarations.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>
#include "System_Core_System_Func_2_gen_6MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_1MethodDeclarations.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>
#include "System_Core_System_Func_2_gen_7MethodDeclarations.h"
struct Misc_t391;
struct GameServices_t534;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.GameServices>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.GameServices>(!!0)
#define Misc_CheckNotNull_TisGameServices_t534_m3891(__this /* static, unused */, p0, method) (( GameServices_t534 * (*) (Object_t * /* static, unused */, GameServices_t534 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_1_t660;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>>(!!0)
#define Misc_CheckNotNull_TisAction_1_t660_m3892(__this /* static, unused */, p0, method) (( Action_1_t660 * (*) (Object_t * /* static, unused */, Action_1_t660 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_1_t866;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>>(!!0)
#define Misc_CheckNotNull_TisAction_1_t866_m3893(__this /* static, unused */, p0, method) (( Action_1_t866 * (*) (Object_t * /* static, unused */, Action_1_t866 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_1_t866;
struct Func_2_t936;
struct Callbacks_t661;
struct Action_1_t937;
struct Func_2_t938;
// Declaration System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
extern "C" IntPtr_t Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared (Object_t * __this /* static, unused */, Action_1_t937 * p0, Func_2_t938 * p1, MethodInfo* method);
#define Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895(__this /* static, unused */, p0, p1, method) (( IntPtr_t (*) (Object_t * /* static, unused */, Action_1_t937 *, Func_2_t938 *, MethodInfo*))Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisFetchAllResponse_t655_m3894(__this /* static, unused */, p0, p1, method) (( IntPtr_t (*) (Object_t * /* static, unused */, Action_1_t866 *, Func_2_t936 *, MethodInfo*))Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared)(__this /* static, unused */, p0, p1, method)
struct Misc_t391;
struct Action_1_t867;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>>(!!0)
#define Misc_CheckNotNull_TisAction_1_t867_m3896(__this /* static, unused */, p0, method) (( Action_1_t867 * (*) (Object_t * /* static, unused */, Action_1_t867 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_1_t867;
struct Func_2_t939;
// Declaration System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisFetchResponse_t653_m3897(__this /* static, unused */, p0, p1, method) (( IntPtr_t (*) (Object_t * /* static, unused */, Action_1_t867 *, Func_2_t939 *, MethodInfo*))Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern MethodInfo* Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var;
extern "C" void AchievementManager__ctor_m2615 (AchievementManager_t656 * __this, GameServices_t534 * ___services, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484067);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		GameServices_t534 * L_0 = ___services;
		GameServices_t534 * L_1 = Misc_CheckNotNull_TisGameServices_t534_m3891(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var);
		__this->___mServices_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::ShowAllUI(System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>)
extern TypeInfo* ShowAllUICallback_t400_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var;
extern MethodInfo* Callbacks_InternalShowUICallback_m2637_MethodInfo_var;
extern "C" void AchievementManager_ShowAllUI_m2616 (AchievementManager_t656 * __this, Action_1_t660 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShowAllUICallback_t400_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(871);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484068);
		Callbacks_InternalShowUICallback_m2637_MethodInfo_var = il2cpp_codegen_method_info_from_index(421);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t660 * L_0 = ___callback;
		Misc_CheckNotNull_TisAction_1_t660_m3892(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var);
		GameServices_t534 * L_1 = (__this->___mServices_0);
		NullCheck(L_1);
		HandleRef_t657  L_2 = GameServices_AsHandle_m2668(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = { Callbacks_InternalShowUICallback_m2637_MethodInfo_var };
		ShowAllUICallback_t400 * L_4 = (ShowAllUICallback_t400 *)il2cpp_codegen_object_new (ShowAllUICallback_t400_il2cpp_TypeInfo_var);
		ShowAllUICallback__ctor_m1635(L_4, NULL, L_3, /*hidden argument*/NULL);
		Action_1_t660 * L_5 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_6 = Callbacks_ToIntPtr_m2636(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		AchievementManager_AchievementManager_ShowAllUI_m1642(NULL /*static, unused*/, L_2, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::FetchAll(System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>)
extern TypeInfo* FetchAllCallback_t398_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t936_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_1_t866_m3893_MethodInfo_var;
extern MethodInfo* AchievementManager_InternalFetchAllCallback_m2618_MethodInfo_var;
extern MethodInfo* FetchAllResponse_FromPointer_m2613_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3898_MethodInfo_var;
extern MethodInfo* Callbacks_ToIntPtr_TisFetchAllResponse_t655_m3894_MethodInfo_var;
extern "C" void AchievementManager_FetchAll_m2617 (AchievementManager_t656 * __this, Action_1_t866 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchAllCallback_t398_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(872);
		Func_2_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(873);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Misc_CheckNotNull_TisAction_1_t866_m3893_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484070);
		AchievementManager_InternalFetchAllCallback_m2618_MethodInfo_var = il2cpp_codegen_method_info_from_index(423);
		FetchAllResponse_FromPointer_m2613_MethodInfo_var = il2cpp_codegen_method_info_from_index(424);
		Func_2__ctor_m3898_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484073);
		Callbacks_ToIntPtr_TisFetchAllResponse_t655_m3894_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484074);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t866 * L_0 = ___callback;
		Misc_CheckNotNull_TisAction_1_t866_m3893(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t866_m3893_MethodInfo_var);
		GameServices_t534 * L_1 = (__this->___mServices_0);
		NullCheck(L_1);
		HandleRef_t657  L_2 = GameServices_AsHandle_m2668(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = { AchievementManager_InternalFetchAllCallback_m2618_MethodInfo_var };
		FetchAllCallback_t398 * L_4 = (FetchAllCallback_t398 *)il2cpp_codegen_object_new (FetchAllCallback_t398_il2cpp_TypeInfo_var);
		FetchAllCallback__ctor_m1627(L_4, NULL, L_3, /*hidden argument*/NULL);
		Action_1_t866 * L_5 = ___callback;
		IntPtr_t L_6 = { FetchAllResponse_FromPointer_m2613_MethodInfo_var };
		Func_2_t936 * L_7 = (Func_2_t936 *)il2cpp_codegen_object_new (Func_2_t936_il2cpp_TypeInfo_var);
		Func_2__ctor_m3898(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m3898_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_8 = Callbacks_ToIntPtr_TisFetchAllResponse_t655_m3894(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/Callbacks_ToIntPtr_TisFetchAllResponse_t655_m3894_MethodInfo_var);
		AchievementManager_AchievementManager_FetchAll_m1639(NULL /*static, unused*/, L_2, 1, L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::InternalFetchAllCallback(System.IntPtr,System.IntPtr)
void STDCALL native_delegate_wrapper_AchievementManager_InternalFetchAllCallback_m2618(IntPtr_t ___response, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___response' to managed representation

	// Marshaling of parameter '___data' to managed representation

	AchievementManager_InternalFetchAllCallback_m2618(NULL, ___response, ___data, NULL);

	// Marshaling of parameter '___response' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern "C" void AchievementManager_InternalFetchAllCallback_m2618 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response;
		IntPtr_t L_1 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2638(NULL /*static, unused*/, (String_t*) &_stringLiteral554, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Fetch(System.String,System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>)
extern TypeInfo* FetchCallback_t399_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t939_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_1_t867_m3896_MethodInfo_var;
extern MethodInfo* AchievementManager_InternalFetchCallback_m2620_MethodInfo_var;
extern MethodInfo* FetchResponse_FromPointer_m2605_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3899_MethodInfo_var;
extern MethodInfo* Callbacks_ToIntPtr_TisFetchResponse_t653_m3897_MethodInfo_var;
extern "C" void AchievementManager_Fetch_m2619 (AchievementManager_t656 * __this, String_t* ___achId, Action_1_t867 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchCallback_t399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(874);
		Func_2_t939_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisAction_1_t867_m3896_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484075);
		AchievementManager_InternalFetchCallback_m2620_MethodInfo_var = il2cpp_codegen_method_info_from_index(428);
		FetchResponse_FromPointer_m2605_MethodInfo_var = il2cpp_codegen_method_info_from_index(429);
		Func_2__ctor_m3899_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484078);
		Callbacks_ToIntPtr_TisFetchResponse_t653_m3897_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484079);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___achId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		Action_1_t867 * L_1 = ___callback;
		Misc_CheckNotNull_TisAction_1_t867_m3896(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t867_m3896_MethodInfo_var);
		GameServices_t534 * L_2 = (__this->___mServices_0);
		NullCheck(L_2);
		HandleRef_t657  L_3 = GameServices_AsHandle_m2668(L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___achId;
		IntPtr_t L_5 = { AchievementManager_InternalFetchCallback_m2620_MethodInfo_var };
		FetchCallback_t399 * L_6 = (FetchCallback_t399 *)il2cpp_codegen_object_new (FetchCallback_t399_il2cpp_TypeInfo_var);
		FetchCallback__ctor_m1631(L_6, NULL, L_5, /*hidden argument*/NULL);
		Action_1_t867 * L_7 = ___callback;
		IntPtr_t L_8 = { FetchResponse_FromPointer_m2605_MethodInfo_var };
		Func_2_t939 * L_9 = (Func_2_t939 *)il2cpp_codegen_object_new (Func_2_t939_il2cpp_TypeInfo_var);
		Func_2__ctor_m3899(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m3899_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_10 = Callbacks_ToIntPtr_TisFetchResponse_t653_m3897(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/Callbacks_ToIntPtr_TisFetchResponse_t653_m3897_MethodInfo_var);
		AchievementManager_AchievementManager_Fetch_m1645(NULL /*static, unused*/, L_3, 1, L_4, L_6, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::InternalFetchCallback(System.IntPtr,System.IntPtr)
void STDCALL native_delegate_wrapper_AchievementManager_InternalFetchCallback_m2620(IntPtr_t ___response, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___response' to managed representation

	// Marshaling of parameter '___data' to managed representation

	AchievementManager_InternalFetchCallback_m2620(NULL, ___response, ___data, NULL);

	// Marshaling of parameter '___response' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern "C" void AchievementManager_InternalFetchCallback_m2620 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response;
		IntPtr_t L_1 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2638(NULL /*static, unused*/, (String_t*) &_stringLiteral555, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Increment(System.String,System.UInt32)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void AchievementManager_Increment_m2621 (AchievementManager_t656 * __this, String_t* ___achievementId, uint32_t ___numSteps, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___achievementId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		GameServices_t534 * L_1 = (__this->___mServices_0);
		NullCheck(L_1);
		HandleRef_t657  L_2 = GameServices_AsHandle_m2668(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___achievementId;
		uint32_t L_4 = ___numSteps;
		AchievementManager_AchievementManager_Increment_m1644(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Reveal(System.String)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void AchievementManager_Reveal_m2622 (AchievementManager_t656 * __this, String_t* ___achievementId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___achievementId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		GameServices_t534 * L_1 = (__this->___mServices_0);
		NullCheck(L_1);
		HandleRef_t657  L_2 = GameServices_AsHandle_m2668(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___achievementId;
		AchievementManager_AchievementManager_Reveal_m1640(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Unlock(System.String)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void AchievementManager_Unlock_m2623 (AchievementManager_t656 * __this, String_t* ___achievementId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___achievementId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		GameServices_t534 * L_1 = (__this->___mServices_0);
		NullCheck(L_1);
		HandleRef_t657  L_2 = GameServices_AsHandle_m2668(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___achievementId;
		AchievementManager_AchievementManager_Unlock_m1641(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRefMethodDeclarations.h"
// System.GC
#include "mscorlib_System_GCMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::.ctor(System.IntPtr)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern "C" void BaseReferenceHolder__ctor_m2624 (BaseReferenceHolder_t654 * __this, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___pointer;
		HandleRef_t657  L_1 = {0};
		HandleRef__ctor_m3900(&L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		HandleRef_t657  L_2 = PInvokeUtilities_CheckNonNull_m2856(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___mSelfPointer_0 = L_2;
		return;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.BaseReferenceHolder::IsDisposed()
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern "C" bool BaseReferenceHolder_IsDisposed_m2625 (BaseReferenceHolder_t654 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = (__this->___mSelfPointer_0);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.BaseReferenceHolder::SelfPtr()
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" HandleRef_t657  BaseReferenceHolder_SelfPtr_m2626 (BaseReferenceHolder_t654 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = BaseReferenceHolder_IsDisposed_m2625(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t905 * L_1 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_1, (String_t*) &_stringLiteral556, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		HandleRef_t657  L_2 = (__this->___mSelfPointer_0);
		return L_2;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::CallDispose(System.Runtime.InteropServices.HandleRef)
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Finalize()
extern "C" void BaseReferenceHolder_Finalize_m2627 (BaseReferenceHolder_t654 * __this, MethodInfo* method)
{
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		BaseReferenceHolder_Dispose_m2630(__this, 1, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1177(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose()
extern "C" void BaseReferenceHolder_Dispose_m2628 (BaseReferenceHolder_t654 * __this, MethodInfo* method)
{
	{
		BaseReferenceHolder_Dispose_m2630(__this, 0, /*hidden argument*/NULL);
		GC_SuppressFinalize_m3901(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr GooglePlayGames.Native.PInvoke.BaseReferenceHolder::AsPointer()
extern "C" IntPtr_t BaseReferenceHolder_AsPointer_m2629 (BaseReferenceHolder_t654 * __this, MethodInfo* method)
{
	HandleRef_t657  V_0 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		IntPtr_t L_1 = HandleRef_get_Handle_m3902((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose(System.Boolean)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void BaseReferenceHolder_Dispose_m2630 (BaseReferenceHolder_t654 * __this, bool ___fromFinalizer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = (__this->___mSelfPointer_0);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		HandleRef_t657  L_2 = (__this->___mSelfPointer_0);
		VirtActionInvoker1< HandleRef_t657  >::Invoke(5 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::CallDispose(System.Runtime.InteropServices.HandleRef) */, __this, L_2);
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		HandleRef_t657  L_4 = {0};
		HandleRef__ctor_m3900(&L_4, __this, L_3, /*hidden argument*/NULL);
		__this->___mSelfPointer_0 = L_4;
	}

IL_002d:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.Callbacks/Type
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks_TMethodDeclarations.h"



// GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks_S.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks_SMethodDeclarations.h"

// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::.ctor(System.Object,System.IntPtr)
extern "C" void ShowUICallbackInternal__ctor_m2631 (ShowUICallbackInternal_t659 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C" void ShowUICallbackInternal_Invoke_m2632 (ShowUICallbackInternal_t659 * __this, int32_t ___status, IntPtr_t ___data, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ShowUICallbackInternal_Invoke_m2632((ShowUICallbackInternal_t659 *)__this->___prev_9,___status, ___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___status, IntPtr_t ___data, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___status, ___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___status, IntPtr_t ___data, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___status, ___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ShowUICallbackInternal_t659(Il2CppObject* delegate, int32_t ___status, IntPtr_t ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___status' to native representation

	// Marshaling of parameter '___data' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___status, ___data);

	// Marshaling cleanup of parameter '___status' native representation

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * ShowUICallbackInternal_BeginInvoke_m2633 (ShowUICallbackInternal_t659 * __this, int32_t ___status, IntPtr_t ___data, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t412_il2cpp_TypeInfo_var, &___status);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___data);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::EndInvoke(System.IAsyncResult)
extern "C" void ShowUICallbackInternal_EndInvoke_m2634 (ShowUICallbackInternal_t659 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifndef _MSC_VER
#else
#endif

// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// System.Action`1<System.IntPtr>
#include "mscorlib_System_Action_1_gen_47.h"
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
#include "mscorlib_System_Action_1_gen_21MethodDeclarations.h"
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"
// System.Action`1<System.IntPtr>
#include "mscorlib_System_Action_1_gen_47MethodDeclarations.h"
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
struct Callbacks_t661;
struct Action_1_t660;
struct Callbacks_t661;
struct Object_t;
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Object>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Object>(System.IntPtr)
extern "C" Object_t * Callbacks_IntPtrToTempCallback_TisObject_t_m3904_gshared (Object_t * __this /* static, unused */, IntPtr_t p0, MethodInfo* method);
#define Callbacks_IntPtrToTempCallback_TisObject_t_m3904(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToTempCallback_TisObject_t_m3904_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>>(System.IntPtr)
#define Callbacks_IntPtrToTempCallback_TisAction_1_t660_m3903(__this /* static, unused */, p0, method) (( Action_1_t660 * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToTempCallback_TisObject_t_m3904_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_1_t940;
struct Callbacks_t661;
struct Object_t;
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Object>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Object>(System.IntPtr)
extern "C" Object_t * Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared (Object_t * __this /* static, unused */, IntPtr_t p0, MethodInfo* method);
#define Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Action`1<System.IntPtr>>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Action`1<System.IntPtr>>(System.IntPtr)
#define Callbacks_IntPtrToPermanentCallback_TisAction_1_t940_m3905(__this /* static, unused */, p0, method) (( Action_1_t940 * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_1_t940;
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Action`1<System.IntPtr>>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Action`1<System.IntPtr>>(System.IntPtr)
#define Callbacks_IntPtrToTempCallback_TisAction_1_t940_m3907(__this /* static, unused */, p0, method) (( Action_1_t940 * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToTempCallback_TisObject_t_m3904_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.PInvoke.Callbacks::.cctor()
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t660_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_U3CNoopUICallbackU3Em__72_m2640_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3758_MethodInfo_var;
extern "C" void Callbacks__cctor_m2635 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(689);
		Callbacks_U3CNoopUICallbackU3Em__72_m2640_MethodInfo_var = il2cpp_codegen_method_info_from_index(432);
		Action_1__ctor_m3758_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483864);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t660 * L_0 = ((Callbacks_t661_StaticFields*)Callbacks_t661_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1 = { Callbacks_U3CNoopUICallbackU3Em__72_m2640_MethodInfo_var };
		Action_1_t660 * L_2 = (Action_1_t660 *)il2cpp_codegen_object_new (Action_1_t660_il2cpp_TypeInfo_var);
		Action_1__ctor_m3758(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3758_MethodInfo_var);
		((Callbacks_t661_StaticFields*)Callbacks_t661_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
	}

IL_0018:
	{
		Action_1_t660 * L_3 = ((Callbacks_t661_StaticFields*)Callbacks_t661_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		((Callbacks_t661_StaticFields*)Callbacks_t661_il2cpp_TypeInfo_var->static_fields)->___NoopUICallback_0 = L_3;
		return;
	}
}
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr(System.Delegate)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Callbacks_ToIntPtr_m2636 (Object_t * __this /* static, unused */, Delegate_t211 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	GCHandle_t941  V_0 = {0};
	{
		Delegate_t211 * L_0 = ___callback;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		return L_1;
	}

IL_000c:
	{
		Delegate_t211 * L_2 = ___callback;
		GCHandle_t941  L_3 = GCHandle_Alloc_m3908(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GCHandle_t941  L_4 = V_0;
		IntPtr_t L_5 = GCHandle_ToIntPtr_m3909(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks::InternalShowUICallback(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
void STDCALL native_delegate_wrapper_Callbacks_InternalShowUICallback_m2637(int32_t ___status, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___status' to managed representation

	// Marshaling of parameter '___data' to managed representation

	Callbacks_InternalShowUICallback_m2637(NULL, ___status, ___data, NULL);

	// Marshaling of parameter '___status' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t135_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_IntPtrToTempCallback_TisAction_1_t660_m3903_MethodInfo_var;
extern "C" void Callbacks_InternalShowUICallback_m2637 (Object_t * __this /* static, unused */, int32_t ___status, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Exception_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		Callbacks_IntPtrToTempCallback_TisAction_1_t660_m3903_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484081);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t660 * V_0 = {0};
	Exception_t135 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___status;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(UIStatus_t412_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral559, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t660 * L_5 = Callbacks_IntPtrToTempCallback_TisAction_1_t660_m3903(NULL /*static, unused*/, L_4, /*hidden argument*/Callbacks_IntPtrToTempCallback_TisAction_1_t660_m3903_MethodInfo_var);
		V_0 = L_5;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		Action_1_t660 * L_6 = V_0;
		int32_t L_7 = ___status;
		NullCheck(L_6);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::Invoke(!0) */, L_6, L_7);
		goto IL_003e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t135 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t135_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0028;
		throw e;
	}

CATCH_0028:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t135 *)__exception_local);
		Exception_t135 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral560, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_003e;
	} // end catch (depth: 1)

IL_003e:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks::PerformInternalCallback(System.String,GooglePlayGames.Native.PInvoke.Callbacks/Type,System.IntPtr,System.IntPtr)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t135_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_IntPtrToPermanentCallback_TisAction_1_t940_m3905_MethodInfo_var;
extern MethodInfo* Callbacks_IntPtrToTempCallback_TisAction_1_t940_m3907_MethodInfo_var;
extern "C" void Callbacks_PerformInternalCallback_m2638 (Object_t * __this /* static, unused */, String_t* ___callbackName, int32_t ___callbackType, IntPtr_t ___response, IntPtr_t ___userData, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Exception_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Callbacks_IntPtrToPermanentCallback_TisAction_1_t940_m3905_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484082);
		Callbacks_IntPtrToTempCallback_TisAction_1_t940_m3907_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484083);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t940 * V_0 = {0};
	Exception_t135 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Action_1_t940 * G_B3_0 = {0};
	{
		String_t* L_0 = ___callbackName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral561, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___callbackType;
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		IntPtr_t L_3 = ___userData;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t940 * L_4 = Callbacks_IntPtrToPermanentCallback_TisAction_1_t940_m3905(NULL /*static, unused*/, L_3, /*hidden argument*/Callbacks_IntPtrToPermanentCallback_TisAction_1_t940_m3905_MethodInfo_var);
		G_B3_0 = L_4;
		goto IL_0027;
	}

IL_0021:
	{
		IntPtr_t L_5 = ___userData;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t940 * L_6 = Callbacks_IntPtrToTempCallback_TisAction_1_t940_m3907(NULL /*static, unused*/, L_5, /*hidden argument*/Callbacks_IntPtrToTempCallback_TisAction_1_t940_m3907_MethodInfo_var);
		G_B3_0 = L_6;
	}

IL_0027:
	{
		V_0 = G_B3_0;
		Action_1_t940 * L_7 = V_0;
		if (L_7)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		Action_1_t940 * L_8 = V_0;
		IntPtr_t L_9 = ___response;
		NullCheck(L_8);
		VirtActionInvoker1< IntPtr_t >::Invoke(10 /* System.Void System.Action`1<System.IntPtr>::Invoke(!0) */, L_8, L_9);
		goto IL_0069;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t135 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t135_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003b;
		throw e;
	}

CATCH_003b:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t135 *)__exception_local);
		ObjectU5BU5D_t208* L_10 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, (String_t*) &_stringLiteral562);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 0)) = (Object_t *)(String_t*) &_stringLiteral562;
		ObjectU5BU5D_t208* L_11 = L_10;
		String_t* L_12 = ___callbackName;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 1)) = (Object_t *)L_12;
		ObjectU5BU5D_t208* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, (String_t*) &_stringLiteral563);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 2)) = (Object_t *)(String_t*) &_stringLiteral563;
		ObjectU5BU5D_t208* L_14 = L_13;
		Exception_t135 * L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 3)) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m976(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		goto IL_0069;
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Byte[] GooglePlayGames.Native.PInvoke.Callbacks::IntPtrAndSizeToByteArray(System.IntPtr,System.UIntPtr)
extern TypeInfo* ByteU5BU5D_t350_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t188_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t350* Callbacks_IntPtrAndSizeToByteArray_m2639 (Object_t * __this /* static, unused */, IntPtr_t ___data, UIntPtr_t  ___dataLength, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		Marshal_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t350* V_0 = {0};
	{
		uint64_t L_0 = UIntPtr_ToUInt64_m3888((&___dataLength), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		return (ByteU5BU5D_t350*)NULL;
	}

IL_000e:
	{
		uint32_t L_1 = UIntPtr_ToUInt32_m3910((&___dataLength), /*hidden argument*/NULL);
		V_0 = ((ByteU5BU5D_t350*)SZArrayNew(ByteU5BU5D_t350_il2cpp_TypeInfo_var, (((uintptr_t)L_1))));
		IntPtr_t L_2 = ___data;
		ByteU5BU5D_t350* L_3 = V_0;
		uint32_t L_4 = UIntPtr_ToUInt32_m3910((&___dataLength), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t188_il2cpp_TypeInfo_var);
		Marshal_Copy_m3911(NULL /*static, unused*/, L_2, L_3, 0, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_5 = V_0;
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks::<NoopUICallback>m__72(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void Callbacks_U3CNoopUICallbackU3Em__72_m2640 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___status;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(UIStatus_t412_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral566, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManageMethodDeclarations.h"

// GooglePlayGames.Native.NativeEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEvent.h"
// GooglePlayGames.Native.Cwrapper.EventManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_1MethodDeclarations.h"
// GooglePlayGames.Native.NativeEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEventMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::.ctor(System.IntPtr)
extern "C" void FetchResponse__ctor_m2641 (FetchResponse_t662 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::ResponseStatus()
extern "C" int32_t FetchResponse_ResponseStatus_m2642 (FetchResponse_t662 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = EventManager_EventManager_FetchResponse_GetStatus_m1714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::RequestSucceeded()
extern "C" bool FetchResponse_RequestSucceeded_m2643 (FetchResponse_t662 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = FetchResponse_ResponseStatus_m2642(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// GooglePlayGames.Native.NativeEvent GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::Data()
extern TypeInfo* NativeEvent_t674_il2cpp_TypeInfo_var;
extern "C" NativeEvent_t674 * FetchResponse_Data_m2644 (FetchResponse_t662 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeEvent_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(877);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = FetchResponse_RequestSucceeded_m2643(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeEvent_t674 *)NULL;
	}

IL_000d:
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = EventManager_EventManager_FetchResponse_GetData_m1715(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeEvent_t674 * L_3 = (NativeEvent_t674 *)il2cpp_codegen_object_new (NativeEvent_t674_il2cpp_TypeInfo_var);
		NativeEvent__ctor_m2738(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchResponse_CallDispose_m2645 (FetchResponse_t662 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		EventManager_EventManager_FetchResponse_Dispose_m1713(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* FetchResponse_t662_il2cpp_TypeInfo_var;
extern "C" FetchResponse_t662 * FetchResponse_FromPointer_m2646 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		FetchResponse_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(703);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchResponse_t662 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		FetchResponse_t662 * L_5 = (FetchResponse_t662 *)il2cpp_codegen_object_new (FetchResponse_t662_il2cpp_TypeInfo_var);
		FetchResponse__ctor_m2641(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_0MethodDeclarations.h"

// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>
#include "mscorlib_System_Collections_Generic_List_1_gen_17.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtil_1.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.NativeEvent>
#include "System_Core_System_Func_2_gen_8.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtil_1MethodDeclarations.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.NativeEvent>
#include "System_Core_System_Func_2_gen_8MethodDeclarations.h"
struct PInvokeUtilities_t682;
struct IntPtrU5BU5D_t859;
struct OutMethod_1_t942;
// Declaration !!0[] GooglePlayGames.Native.PInvoke.PInvokeUtilities::OutParamsToArray<System.IntPtr>(GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<!!0>)
// !!0[] GooglePlayGames.Native.PInvoke.PInvokeUtilities::OutParamsToArray<System.IntPtr>(GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<!!0>)
extern "C" IntPtrU5BU5D_t859* PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912_gshared (Object_t * __this /* static, unused */, OutMethod_1_t942 * p0, MethodInfo* method);
#define PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912(__this /* static, unused */, p0, method) (( IntPtrU5BU5D_t859* (*) (Object_t * /* static, unused */, OutMethod_1_t942 *, MethodInfo*))PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t219;
struct IEnumerable_1_t943;
struct IEnumerable_1_t944;
struct Func_2_t663;
struct Enumerable_t219;
struct IEnumerable_1_t221;
struct IEnumerable_1_t944;
struct Func_2_t945;
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.IntPtr,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.IntPtr,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C" Object_t* Enumerable_Select_TisIntPtr_t_TisObject_t_m3914_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t945 * p1, MethodInfo* method);
#define Enumerable_Select_TisIntPtr_t_TisObject_t_m3914(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t945 *, MethodInfo*))Enumerable_Select_TisIntPtr_t_TisObject_t_m3914_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.IntPtr,GooglePlayGames.Native.NativeEvent>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.IntPtr,GooglePlayGames.Native.NativeEvent>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisIntPtr_t_TisNativeEvent_t674_m3913(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t663 *, MethodInfo*))Enumerable_Select_TisIntPtr_t_TisObject_t_m3914_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct List_1_t868;
struct IEnumerable_1_t943;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.Native.NativeEvent>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.Native.NativeEvent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisNativeEvent_t674_m3915(__this /* static, unused */, p0, method) (( List_1_t868 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C" void FetchAllResponse__ctor_m2647 (FetchAllResponse_t664 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::ResponseStatus()
extern "C" int32_t FetchAllResponse_ResponseStatus_m2648 (FetchAllResponse_t664 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = EventManager_EventManager_FetchAllResponse_GetStatus_m1711(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent> GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::Data()
extern TypeInfo* OutMethod_1_t942_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* FetchAllResponse_t664_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t663_il2cpp_TypeInfo_var;
extern MethodInfo* FetchAllResponse_U3CDataU3Em__79_m2653_MethodInfo_var;
extern MethodInfo* OutMethod_1__ctor_m3916_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912_MethodInfo_var;
extern MethodInfo* FetchAllResponse_U3CDataU3Em__7A_m2654_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3917_MethodInfo_var;
extern MethodInfo* Enumerable_Select_TisIntPtr_t_TisNativeEvent_t674_m3913_MethodInfo_var;
extern MethodInfo* Enumerable_ToList_TisNativeEvent_t674_m3915_MethodInfo_var;
extern "C" List_1_t868 * FetchAllResponse_Data_m2649 (FetchAllResponse_t664 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutMethod_1_t942_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(878);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		FetchAllResponse_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(699);
		Func_2_t663_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(879);
		FetchAllResponse_U3CDataU3Em__79_m2653_MethodInfo_var = il2cpp_codegen_method_info_from_index(436);
		OutMethod_1__ctor_m3916_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484085);
		PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484086);
		FetchAllResponse_U3CDataU3Em__7A_m2654_MethodInfo_var = il2cpp_codegen_method_info_from_index(439);
		Func_2__ctor_m3917_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484088);
		Enumerable_Select_TisIntPtr_t_TisNativeEvent_t674_m3913_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484089);
		Enumerable_ToList_TisNativeEvent_t674_m3915_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484090);
		s_Il2CppMethodIntialized = true;
	}
	IntPtrU5BU5D_t859* V_0 = {0};
	IntPtrU5BU5D_t859* G_B2_0 = {0};
	IntPtrU5BU5D_t859* G_B1_0 = {0};
	{
		IntPtr_t L_0 = { FetchAllResponse_U3CDataU3Em__79_m2653_MethodInfo_var };
		OutMethod_1_t942 * L_1 = (OutMethod_1_t942 *)il2cpp_codegen_object_new (OutMethod_1_t942_il2cpp_TypeInfo_var);
		OutMethod_1__ctor_m3916(L_1, __this, L_0, /*hidden argument*/OutMethod_1__ctor_m3916_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		IntPtrU5BU5D_t859* L_2 = PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912(NULL /*static, unused*/, L_1, /*hidden argument*/PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912_MethodInfo_var);
		V_0 = L_2;
		IntPtrU5BU5D_t859* L_3 = V_0;
		Func_2_t663 * L_4 = ((FetchAllResponse_t664_StaticFields*)FetchAllResponse_t664_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_1;
		G_B1_0 = L_3;
		if (L_4)
		{
			G_B2_0 = L_3;
			goto IL_002b;
		}
	}
	{
		IntPtr_t L_5 = { FetchAllResponse_U3CDataU3Em__7A_m2654_MethodInfo_var };
		Func_2_t663 * L_6 = (Func_2_t663 *)il2cpp_codegen_object_new (Func_2_t663_il2cpp_TypeInfo_var);
		Func_2__ctor_m3917(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m3917_MethodInfo_var);
		((FetchAllResponse_t664_StaticFields*)FetchAllResponse_t664_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_1 = L_6;
		G_B2_0 = G_B1_0;
	}

IL_002b:
	{
		Func_2_t663 * L_7 = ((FetchAllResponse_t664_StaticFields*)FetchAllResponse_t664_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_1;
		Object_t* L_8 = Enumerable_Select_TisIntPtr_t_TisNativeEvent_t674_m3913(NULL /*static, unused*/, (Object_t*)(Object_t*)G_B2_0, L_7, /*hidden argument*/Enumerable_Select_TisIntPtr_t_TisNativeEvent_t674_m3913_MethodInfo_var);
		List_1_t868 * L_9 = Enumerable_ToList_TisNativeEvent_t674_m3915(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_ToList_TisNativeEvent_t674_m3915_MethodInfo_var);
		return L_9;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::RequestSucceeded()
extern "C" bool FetchAllResponse_RequestSucceeded_m2650 (FetchAllResponse_t664 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = FetchAllResponse_ResponseStatus_m2648(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchAllResponse_CallDispose_m2651 (FetchAllResponse_t664 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		EventManager_EventManager_FetchAllResponse_Dispose_m1710(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* FetchAllResponse_t664_il2cpp_TypeInfo_var;
extern "C" FetchAllResponse_t664 * FetchAllResponse_FromPointer_m2652 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		FetchAllResponse_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(699);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchAllResponse_t664 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		FetchAllResponse_t664 * L_5 = (FetchAllResponse_t664 *)il2cpp_codegen_object_new (FetchAllResponse_t664_il2cpp_TypeInfo_var);
		FetchAllResponse__ctor_m2647(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<Data>m__79(System.IntPtr[],System.UIntPtr)
extern "C" UIntPtr_t  FetchAllResponse_U3CDataU3Em__79_m2653 (FetchAllResponse_t664 * __this, IntPtrU5BU5D_t859* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtrU5BU5D_t859* L_1 = ___out_arg;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = EventManager_EventManager_FetchAllResponse_GetData_m1712(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.NativeEvent GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<Data>m__7A(System.IntPtr)
extern TypeInfo* NativeEvent_t674_il2cpp_TypeInfo_var;
extern "C" NativeEvent_t674 * FetchAllResponse_U3CDataU3Em__7A_m2654 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeEvent_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(877);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		NativeEvent_t674 * L_1 = (NativeEvent_t674 *)il2cpp_codegen_object_new (NativeEvent_t674_il2cpp_TypeInfo_var);
		NativeEvent__ctor_m2738(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.PInvoke.EventManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.EventManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_1MethodDeclarations.h"

// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_22.h"
// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>
#include "System_Core_System_Func_2_gen_9.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_23.h"
// GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_0.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>
#include "System_Core_System_Func_2_gen_10.h"
// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManagMethodDeclarations.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>
#include "System_Core_System_Func_2_gen_9MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_0MethodDeclarations.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>
#include "System_Core_System_Func_2_gen_10MethodDeclarations.h"
struct Callbacks_t661;
struct Action_1_t869;
struct Func_2_t946;
// Declaration System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisFetchAllResponse_t664_m3918(__this /* static, unused */, p0, p1, method) (( IntPtr_t (*) (Object_t * /* static, unused */, Action_1_t869 *, Func_2_t946 *, MethodInfo*))Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared)(__this /* static, unused */, p0, p1, method)
struct Callbacks_t661;
struct Action_1_t870;
struct Func_2_t947;
// Declaration System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisFetchResponse_t662_m3919(__this /* static, unused */, p0, p1, method) (( IntPtr_t (*) (Object_t * /* static, unused */, Action_1_t870 *, Func_2_t947 *, MethodInfo*))Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void GooglePlayGames.Native.PInvoke.EventManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern MethodInfo* Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var;
extern "C" void EventManager__ctor_m2655 (EventManager_t548 * __this, GameServices_t534 * ___services, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484067);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		GameServices_t534 * L_0 = ___services;
		GameServices_t534 * L_1 = Misc_CheckNotNull_TisGameServices_t534_m3891(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var);
		__this->___mServices_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.EventManager::FetchAll(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>)
extern TypeInfo* FetchAllCallback_t419_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t946_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* EventManager_InternalFetchAllCallback_m2657_MethodInfo_var;
extern MethodInfo* FetchAllResponse_FromPointer_m2652_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3920_MethodInfo_var;
extern MethodInfo* Callbacks_ToIntPtr_TisFetchAllResponse_t664_m3918_MethodInfo_var;
extern "C" void EventManager_FetchAll_m2656 (EventManager_t548 * __this, int32_t ___source, Action_1_t869 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchAllCallback_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(880);
		Func_2_t946_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(881);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		EventManager_InternalFetchAllCallback_m2657_MethodInfo_var = il2cpp_codegen_method_info_from_index(443);
		FetchAllResponse_FromPointer_m2652_MethodInfo_var = il2cpp_codegen_method_info_from_index(444);
		Func_2__ctor_m3920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484093);
		Callbacks_ToIntPtr_TisFetchAllResponse_t664_m3918_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484094);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t534 * L_0 = (__this->___mServices_0);
		NullCheck(L_0);
		HandleRef_t657  L_1 = GameServices_AsHandle_m2668(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___source;
		IntPtr_t L_3 = { EventManager_InternalFetchAllCallback_m2657_MethodInfo_var };
		FetchAllCallback_t419 * L_4 = (FetchAllCallback_t419 *)il2cpp_codegen_object_new (FetchAllCallback_t419_il2cpp_TypeInfo_var);
		FetchAllCallback__ctor_m1699(L_4, NULL, L_3, /*hidden argument*/NULL);
		Action_1_t869 * L_5 = ___callback;
		IntPtr_t L_6 = { FetchAllResponse_FromPointer_m2652_MethodInfo_var };
		Func_2_t946 * L_7 = (Func_2_t946 *)il2cpp_codegen_object_new (Func_2_t946_il2cpp_TypeInfo_var);
		Func_2__ctor_m3920(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m3920_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_8 = Callbacks_ToIntPtr_TisFetchAllResponse_t664_m3918(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/Callbacks_ToIntPtr_TisFetchAllResponse_t664_m3918_MethodInfo_var);
		EventManager_EventManager_FetchAll_m1707(NULL /*static, unused*/, L_1, L_2, L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.EventManager::InternalFetchAllCallback(System.IntPtr,System.IntPtr)
void STDCALL native_delegate_wrapper_EventManager_InternalFetchAllCallback_m2657(IntPtr_t ___response, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___response' to managed representation

	// Marshaling of parameter '___data' to managed representation

	EventManager_InternalFetchAllCallback_m2657(NULL, ___response, ___data, NULL);

	// Marshaling of parameter '___response' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern "C" void EventManager_InternalFetchAllCallback_m2657 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response;
		IntPtr_t L_1 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2638(NULL /*static, unused*/, (String_t*) &_stringLiteral567, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.EventManager::Fetch(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>)
extern TypeInfo* FetchCallback_t420_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t947_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* EventManager_InternalFetchCallback_m2659_MethodInfo_var;
extern MethodInfo* FetchResponse_FromPointer_m2646_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3921_MethodInfo_var;
extern MethodInfo* Callbacks_ToIntPtr_TisFetchResponse_t662_m3919_MethodInfo_var;
extern "C" void EventManager_Fetch_m2658 (EventManager_t548 * __this, int32_t ___source, String_t* ___eventId, Action_1_t870 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchCallback_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(882);
		Func_2_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(883);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		EventManager_InternalFetchCallback_m2659_MethodInfo_var = il2cpp_codegen_method_info_from_index(447);
		FetchResponse_FromPointer_m2646_MethodInfo_var = il2cpp_codegen_method_info_from_index(448);
		Func_2__ctor_m3921_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484097);
		Callbacks_ToIntPtr_TisFetchResponse_t662_m3919_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484098);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t534 * L_0 = (__this->___mServices_0);
		NullCheck(L_0);
		HandleRef_t657  L_1 = GameServices_AsHandle_m2668(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___source;
		String_t* L_3 = ___eventId;
		IntPtr_t L_4 = { EventManager_InternalFetchCallback_m2659_MethodInfo_var };
		FetchCallback_t420 * L_5 = (FetchCallback_t420 *)il2cpp_codegen_object_new (FetchCallback_t420_il2cpp_TypeInfo_var);
		FetchCallback__ctor_m1703(L_5, NULL, L_4, /*hidden argument*/NULL);
		Action_1_t870 * L_6 = ___callback;
		IntPtr_t L_7 = { FetchResponse_FromPointer_m2646_MethodInfo_var };
		Func_2_t947 * L_8 = (Func_2_t947 *)il2cpp_codegen_object_new (Func_2_t947_il2cpp_TypeInfo_var);
		Func_2__ctor_m3921(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m3921_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_9 = Callbacks_ToIntPtr_TisFetchResponse_t662_m3919(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/Callbacks_ToIntPtr_TisFetchResponse_t662_m3919_MethodInfo_var);
		EventManager_EventManager_Fetch_m1708(NULL /*static, unused*/, L_1, L_2, L_3, L_5, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.EventManager::InternalFetchCallback(System.IntPtr,System.IntPtr)
void STDCALL native_delegate_wrapper_EventManager_InternalFetchCallback_m2659(IntPtr_t ___response, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___response' to managed representation

	// Marshaling of parameter '___data' to managed representation

	EventManager_InternalFetchCallback_m2659(NULL, ___response, ___data, NULL);

	// Marshaling of parameter '___response' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern "C" void EventManager_InternalFetchCallback_m2659 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response;
		IntPtr_t L_1 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2638(NULL /*static, unused*/, (String_t*) &_stringLiteral568, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.EventManager::Increment(System.String,System.UInt32)
extern "C" void EventManager_Increment_m2660 (EventManager_t548 * __this, String_t* ___eventId, uint32_t ___steps, MethodInfo* method)
{
	{
		GameServices_t534 * L_0 = (__this->___mServices_0);
		NullCheck(L_0);
		HandleRef_t657  L_1 = GameServices_AsHandle_m2668(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___eventId;
		uint32_t L_3 = ___steps;
		EventManager_EventManager_Increment_m1709(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.LeaderboardManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_LeaderboardManager.h"
// GooglePlayGames.Native.PlayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManager.h"
// GooglePlayGames.Native.Cwrapper.GameServices
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_GameServic_0MethodDeclarations.h"
// GooglePlayGames.Native.LeaderboardManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_LeaderboardManagerMethodDeclarations.h"
// GooglePlayGames.Native.PlayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManagerMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.GameServices::.ctor(System.IntPtr)
extern "C" void GameServices__ctor_m2661 (GameServices_t534 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.GameServices::IsAuthenticated()
extern "C" bool GameServices_IsAuthenticated_m2662 (GameServices_t534 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = GameServices_GameServices_IsAuthorized_m1721(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServices::SignOut()
extern "C" void GameServices_SignOut_m2663 (GameServices_t534 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		GameServices_GameServices_SignOut_m1723(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServices::StartAuthorizationUI()
extern "C" void GameServices_StartAuthorizationUI_m2664 (GameServices_t534 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		GameServices_GameServices_StartAuthorizationUI_m1724(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.AchievementManager GooglePlayGames.Native.PInvoke.GameServices::AchievementManager()
extern TypeInfo* AchievementManager_t656_il2cpp_TypeInfo_var;
extern "C" AchievementManager_t656 * GameServices_AchievementManager_m2665 (GameServices_t534 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementManager_t656_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		AchievementManager_t656 * L_0 = (AchievementManager_t656 *)il2cpp_codegen_object_new (AchievementManager_t656_il2cpp_TypeInfo_var);
		AchievementManager__ctor_m2615(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// GooglePlayGames.Native.LeaderboardManager GooglePlayGames.Native.PInvoke.GameServices::LeaderboardManager()
extern TypeInfo* LeaderboardManager_t670_il2cpp_TypeInfo_var;
extern "C" LeaderboardManager_t670 * GameServices_LeaderboardManager_m2666 (GameServices_t534 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LeaderboardManager_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(885);
		s_Il2CppMethodIntialized = true;
	}
	{
		LeaderboardManager_t670 * L_0 = (LeaderboardManager_t670 *)il2cpp_codegen_object_new (LeaderboardManager_t670_il2cpp_TypeInfo_var);
		LeaderboardManager__ctor_m2695(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// GooglePlayGames.Native.PlayerManager GooglePlayGames.Native.PInvoke.GameServices::PlayerManager()
extern TypeInfo* PlayerManager_t685_il2cpp_TypeInfo_var;
extern "C" PlayerManager_t685 * GameServices_PlayerManager_m2667 (GameServices_t534 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerManager_t685_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(886);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayerManager_t685 * L_0 = (PlayerManager_t685 *)il2cpp_codegen_object_new (PlayerManager_t685_il2cpp_TypeInfo_var);
		PlayerManager__ctor_m2875(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.GameServices::AsHandle()
extern "C" HandleRef_t657  GameServices_AsHandle_m2668 (GameServices_t534 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServices::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void GameServices_CallDispose_m2669 (GameServices_t534 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		GameServices_GameServices_Dispose_m1722(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_0MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0.h"


// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void AuthFinishedCallback__ctor_m2670 (AuthFinishedCallback_t665 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus)
extern "C" void AuthFinishedCallback_Invoke_m2671 (AuthFinishedCallback_t665 * __this, int32_t ___operation, int32_t ___status, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AuthFinishedCallback_Invoke_m2671((AuthFinishedCallback_t665 *)__this->___prev_9,___operation, ___status, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___operation, int32_t ___status, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___operation, ___status,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___operation, int32_t ___status, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___operation, ___status,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AuthFinishedCallback_t665(Il2CppObject* delegate, int32_t ___operation, int32_t ___status)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___operation' to native representation

	// Marshaling of parameter '___status' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___operation, ___status);

	// Marshaling cleanup of parameter '___operation' native representation

	// Marshaling cleanup of parameter '___status' native representation

}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.AsyncCallback,System.Object)
extern TypeInfo* AuthOperation_t504_il2cpp_TypeInfo_var;
extern TypeInfo* AuthStatus_t411_il2cpp_TypeInfo_var;
extern "C" Object_t * AuthFinishedCallback_BeginInvoke_m2672 (AuthFinishedCallback_t665 * __this, int32_t ___operation, int32_t ___status, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthOperation_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(647);
		AuthStatus_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AuthOperation_t504_il2cpp_TypeInfo_var, &___operation);
	__d_args[1] = Box(AuthStatus_t411_il2cpp_TypeInfo_var, &___status);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::EndInvoke(System.IAsyncResult)
extern "C" void AuthFinishedCallback_EndInvoke_m2673 (AuthFinishedCallback_t665 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void AuthStartedCallback__ctor_m2674 (AuthStartedCallback_t666 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation)
extern "C" void AuthStartedCallback_Invoke_m2675 (AuthStartedCallback_t666 * __this, int32_t ___operation, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AuthStartedCallback_Invoke_m2675((AuthStartedCallback_t666 *)__this->___prev_9,___operation, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___operation, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___operation,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___operation, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___operation,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AuthStartedCallback_t666(Il2CppObject* delegate, int32_t ___operation)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___operation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___operation);

	// Marshaling cleanup of parameter '___operation' native representation

}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.AsyncCallback,System.Object)
extern TypeInfo* AuthOperation_t504_il2cpp_TypeInfo_var;
extern "C" Object_t * AuthStartedCallback_BeginInvoke_m2676 (AuthStartedCallback_t666 * __this, int32_t ___operation, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthOperation_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(647);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AuthOperation_t504_il2cpp_TypeInfo_var, &___operation);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::EndInvoke(System.IAsyncResult)
extern "C" void AuthStartedCallback_EndInvoke_m2677 (AuthStartedCallback_t666 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.PInvoke.GameServicesBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.GameServicesBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_2MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_1.h"
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_0.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "System_Core_System_Action_3_gen_2.h"
// GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_3.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
#include "System_Core_System_Action_3_gen_3.h"
// GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_2.h"
// GooglePlayGames.Native.PInvoke.PlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PlatformCon.h"
// GooglePlayGames.Native.Cwrapper.InternalHooks
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_InternalHoMethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_1MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.Builder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_BuilderMethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_0MethodDeclarations.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "System_Core_System_Action_3_gen_2MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_3MethodDeclarations.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
#include "System_Core_System_Action_3_gen_3MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_2MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.PlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PlatformConMethodDeclarations.h"
struct Callbacks_t661;
struct AuthFinishedCallback_t665;
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback>(System.IntPtr)
#define Callbacks_IntPtrToPermanentCallback_TisAuthFinishedCallback_t665_m3922(__this /* static, unused */, p0, method) (( AuthFinishedCallback_t665 * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct AuthStartedCallback_t666;
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback>(System.IntPtr)
#define Callbacks_IntPtrToPermanentCallback_TisAuthStartedCallback_t666_m3923(__this /* static, unused */, p0, method) (( AuthStartedCallback_t666 * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_3_t871;
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>>(System.IntPtr)
#define Callbacks_IntPtrToPermanentCallback_TisAction_3_t871_m3924(__this /* static, unused */, p0, method) (( Action_3_t871 * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_3_t872;
// Declaration !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>>(System.IntPtr)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToPermanentCallback<System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>>(System.IntPtr)
#define Callbacks_IntPtrToPermanentCallback_TisAction_3_t872_m3925(__this /* static, unused */, p0, method) (( Action_3_t872 * (*) (Object_t * /* static, unused */, IntPtr_t, MethodInfo*))Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::.ctor(System.IntPtr)
extern "C" void GameServicesBuilder__ctor_m2678 (GameServicesBuilder_t667 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		InternalHooks_InternalHooks_ConfigureForUnityPlugin_m1725(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnAuthFinishedCallback(GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback)
extern TypeInfo* OnAuthActionFinishedCallback_t404_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* GameServicesBuilder_InternalAuthFinishedCallback_m2681_MethodInfo_var;
extern "C" void GameServicesBuilder_SetOnAuthFinishedCallback_m2679 (GameServicesBuilder_t667 * __this, AuthFinishedCallback_t665 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnAuthActionFinishedCallback_t404_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(887);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		GameServicesBuilder_InternalAuthFinishedCallback_m2681_MethodInfo_var = il2cpp_codegen_method_info_from_index(451);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = { GameServicesBuilder_InternalAuthFinishedCallback_m2681_MethodInfo_var };
		OnAuthActionFinishedCallback_t404 * L_2 = (OnAuthActionFinishedCallback_t404 *)il2cpp_codegen_object_new (OnAuthActionFinishedCallback_t404_il2cpp_TypeInfo_var);
		OnAuthActionFinishedCallback__ctor_m1661(L_2, NULL, L_1, /*hidden argument*/NULL);
		AuthFinishedCallback_t665 * L_3 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_4 = Callbacks_ToIntPtr_m2636(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Builder_GameServices_Builder_SetOnAuthActionFinished_m1684(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::EnableSnapshots()
extern "C" void GameServicesBuilder_EnableSnapshots_m2680 (GameServicesBuilder_t667 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		Builder_GameServices_Builder_EnableSnapshots_m1681(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalAuthFinishedCallback(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr)
void STDCALL native_delegate_wrapper_GameServicesBuilder_InternalAuthFinishedCallback_m2681(int32_t ___op, int32_t ___status, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___op' to managed representation

	// Marshaling of parameter '___status' to managed representation

	// Marshaling of parameter '___data' to managed representation

	GameServicesBuilder_InternalAuthFinishedCallback_m2681(NULL, ___op, ___status, ___data, NULL);

	// Marshaling of parameter '___op' to native representation

	// Marshaling of parameter '___status' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_IntPtrToPermanentCallback_TisAuthFinishedCallback_t665_m3922_MethodInfo_var;
extern "C" void GameServicesBuilder_InternalAuthFinishedCallback_m2681 (Object_t * __this /* static, unused */, int32_t ___op, int32_t ___status, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Exception_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Callbacks_IntPtrToPermanentCallback_TisAuthFinishedCallback_t665_m3922_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484100);
		s_Il2CppMethodIntialized = true;
	}
	AuthFinishedCallback_t665 * V_0 = {0};
	Exception_t135 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		AuthFinishedCallback_t665 * L_1 = Callbacks_IntPtrToPermanentCallback_TisAuthFinishedCallback_t665_m3922(NULL /*static, unused*/, L_0, /*hidden argument*/Callbacks_IntPtrToPermanentCallback_TisAuthFinishedCallback_t665_m3922_MethodInfo_var);
		V_0 = L_1;
		AuthFinishedCallback_t665 * L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		AuthFinishedCallback_t665 * L_3 = V_0;
		int32_t L_4 = ___op;
		int32_t L_5 = ___status;
		NullCheck(L_3);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus) */, L_3, L_4, L_5);
		goto IL_0031;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t135 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t135_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001b;
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t135 *)__exception_local);
		Exception_t135 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral569, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		goto IL_0031;
	} // end catch (depth: 1)

IL_0031:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnAuthStartedCallback(GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback)
extern TypeInfo* OnAuthActionStartedCallback_t403_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* GameServicesBuilder_InternalAuthStartedCallback_m2683_MethodInfo_var;
extern "C" void GameServicesBuilder_SetOnAuthStartedCallback_m2682 (GameServicesBuilder_t667 * __this, AuthStartedCallback_t666 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnAuthActionStartedCallback_t403_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(888);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		GameServicesBuilder_InternalAuthStartedCallback_m2683_MethodInfo_var = il2cpp_codegen_method_info_from_index(453);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = { GameServicesBuilder_InternalAuthStartedCallback_m2683_MethodInfo_var };
		OnAuthActionStartedCallback_t403 * L_2 = (OnAuthActionStartedCallback_t403 *)il2cpp_codegen_object_new (OnAuthActionStartedCallback_t403_il2cpp_TypeInfo_var);
		OnAuthActionStartedCallback__ctor_m1657(L_2, NULL, L_1, /*hidden argument*/NULL);
		AuthStartedCallback_t666 * L_3 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_4 = Callbacks_ToIntPtr_m2636(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Builder_GameServices_Builder_SetOnAuthActionStarted_m1677(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalAuthStartedCallback(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr)
void STDCALL native_delegate_wrapper_GameServicesBuilder_InternalAuthStartedCallback_m2683(int32_t ___op, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___op' to managed representation

	// Marshaling of parameter '___data' to managed representation

	GameServicesBuilder_InternalAuthStartedCallback_m2683(NULL, ___op, ___data, NULL);

	// Marshaling of parameter '___op' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_IntPtrToPermanentCallback_TisAuthStartedCallback_t666_m3923_MethodInfo_var;
extern "C" void GameServicesBuilder_InternalAuthStartedCallback_m2683 (Object_t * __this /* static, unused */, int32_t ___op, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Exception_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Callbacks_IntPtrToPermanentCallback_TisAuthStartedCallback_t666_m3923_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484102);
		s_Il2CppMethodIntialized = true;
	}
	AuthStartedCallback_t666 * V_0 = {0};
	Exception_t135 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		AuthStartedCallback_t666 * L_1 = Callbacks_IntPtrToPermanentCallback_TisAuthStartedCallback_t666_m3923(NULL /*static, unused*/, L_0, /*hidden argument*/Callbacks_IntPtrToPermanentCallback_TisAuthStartedCallback_t666_m3923_MethodInfo_var);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			AuthStartedCallback_t666 * L_2 = V_0;
			if (!L_2)
			{
				goto IL_0014;
			}
		}

IL_000d:
		{
			AuthStartedCallback_t666 * L_3 = V_0;
			int32_t L_4 = ___op;
			NullCheck(L_3);
			VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation) */, L_3, L_4);
		}

IL_0014:
		{
			goto IL_002f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t135 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t135_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0019;
		throw e;
	}

CATCH_0019:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t135 *)__exception_local);
		Exception_t135 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral570, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_002f;
	} // end catch (depth: 1)

IL_002f:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void GameServicesBuilder_CallDispose_m2684 (GameServicesBuilder_t667 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		Builder_GameServices_Builder_Dispose_m1689(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalOnTurnBasedMatchEventCallback(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
void STDCALL native_delegate_wrapper_GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685(int32_t ___eventType, char* ___matchId, IntPtr_t ___match, IntPtr_t ___userData)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___eventType' to managed representation

	// Marshaling of parameter '___matchId' to managed representation
	String_t* ____matchId_unmarshaled = { 0 };
	____matchId_unmarshaled = il2cpp_codegen_marshal_string_result(___matchId);

	// Marshaling of parameter '___match' to managed representation

	// Marshaling of parameter '___userData' to managed representation

	GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685(NULL, ___eventType, ____matchId_unmarshaled, ___match, ___userData, NULL);

	// Marshaling of parameter '___eventType' to native representation

	// Marshaling of parameter '___matchId' to native representation

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___userData' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_IntPtrToPermanentCallback_TisAction_3_t871_m3924_MethodInfo_var;
extern "C" void GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685 (Object_t * __this /* static, unused */, int32_t ___eventType, String_t* ___matchId, IntPtr_t ___match, IntPtr_t ___userData, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Exception_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Callbacks_IntPtrToPermanentCallback_TisAction_3_t871_m3924_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484103);
		s_Il2CppMethodIntialized = true;
	}
	Action_3_t871 * V_0 = {0};
	NativeTurnBasedMatch_t680 * V_1 = {0};
	Exception_t135 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = ___userData;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_3_t871 * L_1 = Callbacks_IntPtrToPermanentCallback_TisAction_3_t871_m3924(NULL /*static, unused*/, L_0, /*hidden argument*/Callbacks_IntPtrToPermanentCallback_TisAction_3_t871_m3924_MethodInfo_var);
		V_0 = L_1;
		IntPtr_t L_2 = ___match;
		NativeTurnBasedMatch_t680 * L_3 = NativeTurnBasedMatch_FromPointer_m2845(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Action_3_t871 * L_4 = V_0;
				if (!L_4)
				{
					goto IL_001d;
				}
			}

IL_0014:
			{
				Action_3_t871 * L_5 = V_0;
				int32_t L_6 = ___eventType;
				String_t* L_7 = ___matchId;
				NativeTurnBasedMatch_t680 * L_8 = V_1;
				NullCheck(L_5);
				VirtActionInvoker3< int32_t, String_t*, NativeTurnBasedMatch_t680 * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::Invoke(!0,!1,!2) */, L_5, L_6, L_7, L_8);
			}

IL_001d:
			{
				goto IL_0038;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t135 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t135_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0022;
			throw e;
		}

CATCH_0022:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t135 *)__exception_local);
			Exception_t135 * L_9 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral571, L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			goto IL_0038;
		} // end catch (depth: 2)

IL_0038:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		{
			NativeTurnBasedMatch_t680 * L_11 = V_1;
			if (!L_11)
			{
				goto IL_0049;
			}
		}

IL_0043:
		{
			NativeTurnBasedMatch_t680 * L_12 = V_1;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_12);
		}

IL_0049:
		{
			IL2CPP_END_FINALLY(61)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_004a:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnTurnBasedMatchEventCallback(System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>)
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* OnTurnBasedMatchEventCallback_t406_il2cpp_TypeInfo_var;
extern MethodInfo* GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685_MethodInfo_var;
extern "C" void GameServicesBuilder_SetOnTurnBasedMatchEventCallback_m2686 (GameServicesBuilder_t667 * __this, Action_3_t871 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		OnTurnBasedMatchEventCallback_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(890);
		GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685_MethodInfo_var = il2cpp_codegen_method_info_from_index(456);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		Action_3_t871 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Callbacks_ToIntPtr_m2636(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HandleRef_t657  L_2 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = { GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685_MethodInfo_var };
		OnTurnBasedMatchEventCallback_t406 * L_4 = (OnTurnBasedMatchEventCallback_t406 *)il2cpp_codegen_object_new (OnTurnBasedMatchEventCallback_t406_il2cpp_TypeInfo_var);
		OnTurnBasedMatchEventCallback__ctor_m1669(L_4, NULL, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = V_0;
		Builder_GameServices_Builder_SetOnTurnBasedMatchEvent_m1685(NULL /*static, unused*/, L_2, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalOnMultiplayerInvitationEventCallback(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
void STDCALL native_delegate_wrapper_GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687(int32_t ___eventType, char* ___matchId, IntPtr_t ___match, IntPtr_t ___userData)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___eventType' to managed representation

	// Marshaling of parameter '___matchId' to managed representation
	String_t* ____matchId_unmarshaled = { 0 };
	____matchId_unmarshaled = il2cpp_codegen_marshal_string_result(___matchId);

	// Marshaling of parameter '___match' to managed representation

	// Marshaling of parameter '___userData' to managed representation

	GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687(NULL, ___eventType, ____matchId_unmarshaled, ___match, ___userData, NULL);

	// Marshaling of parameter '___eventType' to native representation

	// Marshaling of parameter '___matchId' to native representation

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___userData' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* Callbacks_IntPtrToPermanentCallback_TisAction_3_t872_m3925_MethodInfo_var;
extern "C" void GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687 (Object_t * __this /* static, unused */, int32_t ___eventType, String_t* ___matchId, IntPtr_t ___match, IntPtr_t ___userData, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Exception_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Callbacks_IntPtrToPermanentCallback_TisAction_3_t872_m3925_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484105);
		s_Il2CppMethodIntialized = true;
	}
	Action_3_t872 * V_0 = {0};
	MultiplayerInvitation_t601 * V_1 = {0};
	Exception_t135 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = ___userData;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_3_t872 * L_1 = Callbacks_IntPtrToPermanentCallback_TisAction_3_t872_m3925(NULL /*static, unused*/, L_0, /*hidden argument*/Callbacks_IntPtrToPermanentCallback_TisAction_3_t872_m3925_MethodInfo_var);
		V_0 = L_1;
		IntPtr_t L_2 = ___match;
		MultiplayerInvitation_t601 * L_3 = MultiplayerInvitation_FromPointer_m2709(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Action_3_t872 * L_4 = V_0;
				if (!L_4)
				{
					goto IL_001d;
				}
			}

IL_0014:
			{
				Action_3_t872 * L_5 = V_0;
				int32_t L_6 = ___eventType;
				String_t* L_7 = ___matchId;
				MultiplayerInvitation_t601 * L_8 = V_1;
				NullCheck(L_5);
				VirtActionInvoker3< int32_t, String_t*, MultiplayerInvitation_t601 * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::Invoke(!0,!1,!2) */, L_5, L_6, L_7, L_8);
			}

IL_001d:
			{
				goto IL_0038;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t135 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t135_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0022;
			throw e;
		}

CATCH_0022:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t135 *)__exception_local);
			Exception_t135 * L_9 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral572, L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			goto IL_0038;
		} // end catch (depth: 2)

IL_0038:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		{
			MultiplayerInvitation_t601 * L_11 = V_1;
			if (!L_11)
			{
				goto IL_0049;
			}
		}

IL_0043:
		{
			MultiplayerInvitation_t601 * L_12 = V_1;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_12);
		}

IL_0049:
		{
			IL2CPP_END_FINALLY(61)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_004a:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnMultiplayerInvitationEventCallback(System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>)
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* OnMultiplayerInvitationEventCallback_t405_il2cpp_TypeInfo_var;
extern MethodInfo* GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687_MethodInfo_var;
extern "C" void GameServicesBuilder_SetOnMultiplayerInvitationEventCallback_m2688 (GameServicesBuilder_t667 * __this, Action_3_t872 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		OnMultiplayerInvitationEventCallback_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687_MethodInfo_var = il2cpp_codegen_method_info_from_index(458);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		Action_3_t872 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Callbacks_ToIntPtr_m2636(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HandleRef_t657  L_2 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = { GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687_MethodInfo_var };
		OnMultiplayerInvitationEventCallback_t405 * L_4 = (OnMultiplayerInvitationEventCallback_t405 *)il2cpp_codegen_object_new (OnMultiplayerInvitationEventCallback_t405_il2cpp_TypeInfo_var);
		OnMultiplayerInvitationEventCallback__ctor_m1665(L_4, NULL, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = V_0;
		Builder_GameServices_Builder_SetOnMultiplayerInvitationEvent_m1687(NULL /*static, unused*/, L_2, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.PInvoke.GameServicesBuilder::Build(GooglePlayGames.Native.PInvoke.PlatformConfiguration)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern TypeInfo* GameServices_t534_il2cpp_TypeInfo_var;
extern "C" GameServices_t534 * GameServicesBuilder_Build_m2689 (GameServicesBuilder_t667 * __this, PlatformConfiguration_t669 * ___configRef, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		GameServices_t534_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(870);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		PlatformConfiguration_t669 * L_1 = ___configRef;
		NullCheck(L_1);
		HandleRef_t657  L_2 = PlatformConfiguration_AsHandle_m2869(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = HandleRef_ToIntPtr_m3926(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = Builder_GameServices_Builder_Create_m1688(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		IntPtr_t L_5 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_6 = L_5;
		Object_t * L_7 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_6);
		bool L_8 = IntPtr_Equals_m3885((&V_0), L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0038;
		}
	}
	{
		InvalidOperationException_t905 * L_9 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_9, (String_t*) &_stringLiteral573, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0038:
	{
		IntPtr_t L_10 = V_0;
		GameServices_t534 * L_11 = (GameServices_t534 *)il2cpp_codegen_object_new (GameServices_t534_il2cpp_TypeInfo_var);
		GameServices__ctor_m2661(L_11, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// GooglePlayGames.Native.PInvoke.GameServicesBuilder GooglePlayGames.Native.PInvoke.GameServicesBuilder::Create()
extern TypeInfo* GameServicesBuilder_t667_il2cpp_TypeInfo_var;
extern "C" GameServicesBuilder_t667 * GameServicesBuilder_Create_m2690 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameServicesBuilder_t667_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(892);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = Builder_GameServices_Builder_Construct_m1680(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameServicesBuilder_t667 * L_1 = (GameServicesBuilder_t667 *)il2cpp_codegen_object_new (GameServicesBuilder_t667_il2cpp_TypeInfo_var);
		GameServicesBuilder__ctor_m2678(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_IosPlatform.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_IosPlatformMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_IosPlatforMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::.ctor(System.IntPtr)
extern "C" void IosPlatformConfiguration__ctor_m2691 (IosPlatformConfiguration_t668 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		PlatformConfiguration__ctor_m2868(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::SetClientId(System.String)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void IosPlatformConfiguration_SetClientId_m2692 (IosPlatformConfiguration_t668 * __this, String_t* ___clientId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___clientId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___clientId;
		IosPlatformConfiguration_IosPlatformConfiguration_SetClientID_m1729(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void IosPlatformConfiguration_CallDispose_m2693 (IosPlatformConfiguration_t668 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		IosPlatformConfiguration_IosPlatformConfiguration_Dispose_m1727(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::Create()
extern TypeInfo* IosPlatformConfiguration_t668_il2cpp_TypeInfo_var;
extern "C" IosPlatformConfiguration_t668 * IosPlatformConfiguration_Create_m2694 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IosPlatformConfiguration_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(893);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = IosPlatformConfiguration_IosPlatformConfiguration_Construct_m1726(NULL /*static, unused*/, /*hidden argument*/NULL);
		IosPlatformConfiguration_t668 * L_1 = (IosPlatformConfiguration_t668 *)il2cpp_codegen_object_new (IosPlatformConfiguration_t668_il2cpp_TypeInfo_var);
		IosPlatformConfiguration__ctor_m2691(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int64
#include "mscorlib_System_Int64.h"
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_5.h"
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_6.h"
// GooglePlayGames.Native.Cwrapper.LeaderboardManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_7MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_5MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_6MethodDeclarations.h"


// System.Void GooglePlayGames.Native.LeaderboardManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern MethodInfo* Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var;
extern "C" void LeaderboardManager__ctor_m2695 (LeaderboardManager_t670 * __this, GameServices_t534 * ___services, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484067);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		GameServices_t534 * L_0 = ___services;
		GameServices_t534 * L_1 = Misc_CheckNotNull_TisGameServices_t534_m3891(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var);
		__this->___mServices_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.LeaderboardManager::SubmitScore(System.String,System.Int64)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t233_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void LeaderboardManager_SubmitScore_m2696 (LeaderboardManager_t670 * __this, String_t* ___leaderboardId, int64_t ___score, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Int64_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		ObjectU5BU5D_t208* L_1 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, (String_t*) &_stringLiteral574);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)(String_t*) &_stringLiteral574;
		ObjectU5BU5D_t208* L_2 = L_1;
		int64_t L_3 = ___score;
		int64_t L_4 = L_3;
		Object_t * L_5 = Box(Int64_t233_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t208* L_6 = L_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, (String_t*) &_stringLiteral575);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)(String_t*) &_stringLiteral575;
		ObjectU5BU5D_t208* L_7 = L_6;
		String_t* L_8 = ___leaderboardId;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m976(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		GameServices_t534 * L_10 = (__this->___mServices_0);
		NullCheck(L_10);
		HandleRef_t657  L_11 = GameServices_AsHandle_m2668(L_10, /*hidden argument*/NULL);
		String_t* L_12 = ___leaderboardId;
		int64_t L_13 = ___score;
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		LeaderboardManager_LeaderboardManager_SubmitScore_m1772(NULL /*static, unused*/, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.LeaderboardManager::ShowAllUI(System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>)
extern TypeInfo* ShowAllUICallback_t432_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var;
extern MethodInfo* Callbacks_InternalShowUICallback_m2637_MethodInfo_var;
extern "C" void LeaderboardManager_ShowAllUI_m2697 (LeaderboardManager_t670 * __this, Action_1_t660 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShowAllUICallback_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(894);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484068);
		Callbacks_InternalShowUICallback_m2637_MethodInfo_var = il2cpp_codegen_method_info_from_index(421);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t660 * L_0 = ___callback;
		Misc_CheckNotNull_TisAction_1_t660_m3892(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var);
		GameServices_t534 * L_1 = (__this->___mServices_0);
		NullCheck(L_1);
		HandleRef_t657  L_2 = GameServices_AsHandle_m2668(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = { Callbacks_InternalShowUICallback_m2637_MethodInfo_var };
		ShowAllUICallback_t432 * L_4 = (ShowAllUICallback_t432 *)il2cpp_codegen_object_new (ShowAllUICallback_t432_il2cpp_TypeInfo_var);
		ShowAllUICallback__ctor_m1756(L_4, NULL, L_3, /*hidden argument*/NULL);
		Action_1_t660 * L_5 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_6 = Callbacks_ToIntPtr_m2636(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		LeaderboardManager_LeaderboardManager_ShowAllUI_m1767(NULL /*static, unused*/, L_2, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.LeaderboardManager::ShowUI(System.String,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>)
extern TypeInfo* ShowUICallback_t433_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var;
extern MethodInfo* Callbacks_InternalShowUICallback_m2637_MethodInfo_var;
extern "C" void LeaderboardManager_ShowUI_m2698 (LeaderboardManager_t670 * __this, String_t* ___leaderboardId, Action_1_t660 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShowUICallback_t433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(895);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484068);
		Callbacks_InternalShowUICallback_m2637_MethodInfo_var = il2cpp_codegen_method_info_from_index(421);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t660 * L_0 = ___callback;
		Misc_CheckNotNull_TisAction_1_t660_m3892(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t660_m3892_MethodInfo_var);
		GameServices_t534 * L_1 = (__this->___mServices_0);
		NullCheck(L_1);
		HandleRef_t657  L_2 = GameServices_AsHandle_m2668(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___leaderboardId;
		IntPtr_t L_4 = { Callbacks_InternalShowUICallback_m2637_MethodInfo_var };
		ShowUICallback_t433 * L_5 = (ShowUICallback_t433 *)il2cpp_codegen_object_new (ShowUICallback_t433_il2cpp_TypeInfo_var);
		ShowUICallback__ctor_m1760(L_5, NULL, L_4, /*hidden argument*/NULL);
		Action_1_t660 * L_6 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_7 = Callbacks_ToIntPtr_m2636(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		LeaderboardManager_LeaderboardManager_ShowUI_m1770(NULL /*static, unused*/, L_2, L_3, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtil.h"
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit.h"
// GooglePlayGames.BasicApi.Multiplayer.Invitation
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit_0.h"
// GooglePlayGames.Native.Cwrapper.MultiplayerInvitation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_MultiplayeMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtilMethodDeclarations.h"
// GooglePlayGames.BasicApi.Multiplayer.Invitation
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit_0MethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.MultiplayerInvitation::.ctor(System.IntPtr)
extern "C" void MultiplayerInvitation__ctor_m2699 (MultiplayerInvitation_t601 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Inviter()
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern "C" MultiplayerParticipant_t672 * MultiplayerInvitation_Inviter_m2700 (MultiplayerInvitation_t601 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		s_Il2CppMethodIntialized = true;
	}
	MultiplayerParticipant_t672 * V_0 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = MultiplayerInvitation_MultiplayerInvitation_InvitingParticipant_m1791(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant_t672 * L_2 = (MultiplayerParticipant_t672 *)il2cpp_codegen_object_new (MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant__ctor_m2711(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		MultiplayerParticipant_t672 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = MultiplayerParticipant_Valid_m2718(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		MultiplayerParticipant_t672 * L_5 = V_0;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_5);
		return (MultiplayerParticipant_t672 *)NULL;
	}

IL_0024:
	{
		MultiplayerParticipant_t672 * L_6 = V_0;
		return L_6;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Variant()
extern "C" uint32_t MultiplayerInvitation_Variant_m2701 (MultiplayerInvitation_t601 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = MultiplayerInvitation_MultiplayerInvitation_Variant_m1792(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Type()
extern "C" int32_t MultiplayerInvitation_Type_m2702 (MultiplayerInvitation_t601 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = MultiplayerInvitation_MultiplayerInvitation_Type_m1797(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* MultiplayerInvitation_U3CIdU3Em__7B_m2710_MethodInfo_var;
extern "C" String_t* MultiplayerInvitation_Id_m2703 (MultiplayerInvitation_t601 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		MultiplayerInvitation_U3CIdU3Em__7B_m2710_MethodInfo_var = il2cpp_codegen_method_info_from_index(459);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { MultiplayerInvitation_U3CIdU3Em__7B_m2710_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.MultiplayerInvitation::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void MultiplayerInvitation_CallDispose_m2704 (MultiplayerInvitation_t601 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		MultiplayerInvitation_MultiplayerInvitation_Dispose_m1799(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.MultiplayerInvitation::AutomatchingSlots()
extern "C" uint32_t MultiplayerInvitation_AutomatchingSlots_m2705 (MultiplayerInvitation_t601 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = MultiplayerInvitation_MultiplayerInvitation_AutomatchingSlotsAvailable_m1790(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.MultiplayerInvitation::ParticipantCount()
extern "C" uint32_t MultiplayerInvitation_ParticipantCount_m2706 (MultiplayerInvitation_t601 * __this, MethodInfo* method)
{
	UIntPtr_t  V_0 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = MultiplayerInvitation_MultiplayerInvitation_Participants_Length_m1794(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		uint32_t L_2 = UIntPtr_ToUInt32_m3910((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType GooglePlayGames.Native.PInvoke.MultiplayerInvitation::ToInvType(GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType)
extern TypeInfo* MultiplayerInvitationType_t519_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" int32_t MultiplayerInvitation_ToInvType_m2707 (Object_t * __this /* static, unused */, int32_t ___invitationType, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerInvitationType_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(897);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___invitationType;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		goto IL_0019;
	}

IL_0015:
	{
		return (int32_t)(0);
	}

IL_0017:
	{
		return (int32_t)(1);
	}

IL_0019:
	{
		int32_t L_3 = ___invitationType;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(MultiplayerInvitationType_t519_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral576, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return (int32_t)(2);
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Invitation GooglePlayGames.Native.PInvoke.MultiplayerInvitation::AsInvitation()
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* Invitation_t341_il2cpp_TypeInfo_var;
extern "C" Invitation_t341 * MultiplayerInvitation_AsInvitation_m2708 (MultiplayerInvitation_t601 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Invitation_t341_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(692);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	Participant_t340 * V_3 = {0};
	MultiplayerParticipant_t672 * V_4 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Participant_t340 * G_B4_0 = {0};
	{
		int32_t L_0 = MultiplayerInvitation_Type_m2702(__this, /*hidden argument*/NULL);
		int32_t L_1 = MultiplayerInvitation_ToInvType_m2707(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = MultiplayerInvitation_Id_m2703(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		uint32_t L_3 = MultiplayerInvitation_Variant_m2701(__this, /*hidden argument*/NULL);
		V_2 = L_3;
		MultiplayerParticipant_t672 * L_4 = MultiplayerInvitation_Inviter_m2700(__this, /*hidden argument*/NULL);
		V_4 = L_4;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			MultiplayerParticipant_t672 * L_5 = V_4;
			if (L_5)
			{
				goto IL_002f;
			}
		}

IL_0029:
		{
			G_B4_0 = ((Participant_t340 *)(NULL));
			goto IL_0036;
		}

IL_002f:
		{
			MultiplayerParticipant_t672 * L_6 = V_4;
			NullCheck(L_6);
			Participant_t340 * L_7 = MultiplayerParticipant_AsParticipant_m2720(L_6, /*hidden argument*/NULL);
			G_B4_0 = L_7;
		}

IL_0036:
		{
			V_3 = G_B4_0;
			IL2CPP_LEAVE(0x4B, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		{
			MultiplayerParticipant_t672 * L_8 = V_4;
			if (!L_8)
			{
				goto IL_004a;
			}
		}

IL_0043:
		{
			MultiplayerParticipant_t672 * L_9 = V_4;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_9);
		}

IL_004a:
		{
			IL2CPP_END_FINALLY(60)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_004b:
	{
		int32_t L_10 = V_0;
		String_t* L_11 = V_1;
		Participant_t340 * L_12 = V_3;
		int32_t L_13 = V_2;
		Invitation_t341 * L_14 = (Invitation_t341 *)il2cpp_codegen_object_new (Invitation_t341_il2cpp_TypeInfo_var);
		Invitation__ctor_m1371(L_14, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.MultiplayerInvitation::FromPointer(System.IntPtr)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* MultiplayerInvitation_t601_il2cpp_TypeInfo_var;
extern "C" MultiplayerInvitation_t601 * MultiplayerInvitation_FromPointer_m2709 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		MultiplayerInvitation_t601_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(654);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (MultiplayerInvitation_t601 *)NULL;
	}

IL_000d:
	{
		IntPtr_t L_2 = ___selfPointer;
		MultiplayerInvitation_t601 * L_3 = (MultiplayerInvitation_t601 *)il2cpp_codegen_object_new (MultiplayerInvitation_t601_il2cpp_TypeInfo_var);
		MultiplayerInvitation__ctor_m2699(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.MultiplayerInvitation::<Id>m__7B(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerInvitation_U3CIdU3Em__7B_m2710 (MultiplayerInvitation_t601 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = MultiplayerInvitation_MultiplayerInvitation_Id_m1798(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_14.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"
// GooglePlayGames.Native.NativePlayer
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativePlayer.h"
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_14MethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.MultiplayerParticipant
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Multiplaye_0MethodDeclarations.h"
// GooglePlayGames.Native.NativePlayer
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativePlayerMethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.Sentinels
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SentinelsMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.MultiplayerParticipant::.ctor(System.IntPtr)
extern "C" void MultiplayerParticipant__ctor_m2711 (MultiplayerParticipant_t672 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.MultiplayerParticipant::.cctor()
extern TypeInfo* Dictionary_2_t671_il2cpp_TypeInfo_var;
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2__ctor_m3927_MethodInfo_var;
extern "C" void MultiplayerParticipant__cctor_m2712 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(898);
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		Dictionary_2__ctor_m3927_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484108);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t671 * V_0 = {0};
	{
		Dictionary_2_t671 * L_0 = (Dictionary_2_t671 *)il2cpp_codegen_object_new (Dictionary_2_t671_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3927(L_0, /*hidden argument*/Dictionary_2__ctor_m3927_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t671 * L_1 = V_0;
		NullCheck(L_1);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(!0,!1) */, L_1, 1, 1);
		Dictionary_2_t671 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(!0,!1) */, L_2, 2, 2);
		Dictionary_2_t671 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(!0,!1) */, L_3, 3, 3);
		Dictionary_2_t671 * L_4 = V_0;
		NullCheck(L_4);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(!0,!1) */, L_4, 4, 4);
		Dictionary_2_t671 * L_5 = V_0;
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(!0,!1) */, L_5, 5, 0);
		Dictionary_2_t671 * L_6 = V_0;
		NullCheck(L_6);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(!0,!1) */, L_6, 6, 5);
		Dictionary_2_t671 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(!0,!1) */, L_7, 7, 6);
		Dictionary_2_t671 * L_8 = V_0;
		((MultiplayerParticipant_t672_StaticFields*)MultiplayerParticipant_t672_il2cpp_TypeInfo_var->static_fields)->___StatusConversion_1 = L_8;
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Status()
extern "C" int32_t MultiplayerParticipant_Status_m2713 (MultiplayerParticipant_t672 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = MultiplayerParticipant_MultiplayerParticipant_Status_m1800(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.MultiplayerParticipant::IsConnectedToRoom()
extern "C" bool MultiplayerParticipant_IsConnectedToRoom_m2714 (MultiplayerParticipant_t672 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = MultiplayerParticipant_MultiplayerParticipant_IsConnectedToRoom_m1802(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.PInvoke.MultiplayerParticipant::DisplayName()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* MultiplayerParticipant_U3CDisplayNameU3Em__7C_m2723_MethodInfo_var;
extern "C" String_t* MultiplayerParticipant_DisplayName_m2715 (MultiplayerParticipant_t672 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		MultiplayerParticipant_U3CDisplayNameU3Em__7C_m2723_MethodInfo_var = il2cpp_codegen_method_info_from_index(461);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { MultiplayerParticipant_U3CDisplayNameU3Em__7C_m2723_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.Native.NativePlayer GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Player()
extern TypeInfo* NativePlayer_t675_il2cpp_TypeInfo_var;
extern "C" NativePlayer_t675 * MultiplayerParticipant_Player_m2716 (MultiplayerParticipant_t672 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativePlayer_t675_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(899);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = MultiplayerParticipant_MultiplayerParticipant_HasPlayer_m1804(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (NativePlayer_t675 *)NULL;
	}

IL_0012:
	{
		HandleRef_t657  L_2 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = MultiplayerParticipant_MultiplayerParticipant_Player_m1807(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NativePlayer_t675 * L_4 = (NativePlayer_t675 *)il2cpp_codegen_object_new (NativePlayer_t675_il2cpp_TypeInfo_var);
		NativePlayer__ctor_m2751(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* MultiplayerParticipant_U3CIdU3Em__7D_m2724_MethodInfo_var;
extern "C" String_t* MultiplayerParticipant_Id_m2717 (MultiplayerParticipant_t672 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		MultiplayerParticipant_U3CIdU3Em__7D_m2724_MethodInfo_var = il2cpp_codegen_method_info_from_index(462);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { MultiplayerParticipant_U3CIdU3Em__7D_m2724_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Valid()
extern "C" bool MultiplayerParticipant_Valid_m2718 (MultiplayerParticipant_t672 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = MultiplayerParticipant_MultiplayerParticipant_Valid_m1809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.MultiplayerParticipant::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void MultiplayerParticipant_CallDispose_m2719 (MultiplayerParticipant_t672 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		MultiplayerParticipant_MultiplayerParticipant_Dispose_m1808(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.PInvoke.MultiplayerParticipant::AsParticipant()
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern TypeInfo* Participant_t340_il2cpp_TypeInfo_var;
extern "C" Participant_t340 * MultiplayerParticipant_AsParticipant_m2720 (MultiplayerParticipant_t672 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		Participant_t340_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(606);
		s_Il2CppMethodIntialized = true;
	}
	NativePlayer_t675 * V_0 = {0};
	int32_t G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B2_2 = {0};
	int32_t G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	String_t* G_B1_2 = {0};
	Player_t347 * G_B3_0 = {0};
	int32_t G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	String_t* G_B3_3 = {0};
	{
		NativePlayer_t675 * L_0 = MultiplayerParticipant_Player_m2716(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = MultiplayerParticipant_DisplayName_m2715(__this, /*hidden argument*/NULL);
		String_t* L_2 = MultiplayerParticipant_Id_m2717(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		Dictionary_2_t671 * L_3 = ((MultiplayerParticipant_t672_StaticFields*)MultiplayerParticipant_t672_il2cpp_TypeInfo_var->static_fields)->___StatusConversion_1;
		int32_t L_4 = MultiplayerParticipant_Status_m2713(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_5 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Item(!0) */, L_3, L_4);
		NativePlayer_t675 * L_6 = V_0;
		G_B1_0 = L_5;
		G_B1_1 = L_2;
		G_B1_2 = L_1;
		if (L_6)
		{
			G_B2_0 = L_5;
			G_B2_1 = L_2;
			G_B2_2 = L_1;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = ((Player_t347 *)(NULL));
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0035;
	}

IL_002f:
	{
		NativePlayer_t675 * L_7 = V_0;
		NullCheck(L_7);
		Player_t347 * L_8 = NativePlayer_AsPlayer_m2756(L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0035:
	{
		bool L_9 = MultiplayerParticipant_IsConnectedToRoom_m2714(__this, /*hidden argument*/NULL);
		Participant_t340 * L_10 = (Participant_t340 *)il2cpp_codegen_object_new (Participant_t340_il2cpp_TypeInfo_var);
		Participant__ctor_m1385(L_10, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.MultiplayerParticipant::FromPointer(System.IntPtr)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern "C" MultiplayerParticipant_t672 * MultiplayerParticipant_FromPointer_m2721 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___pointer;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (MultiplayerParticipant_t672 *)NULL;
	}

IL_000d:
	{
		IntPtr_t L_2 = ___pointer;
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant_t672 * L_3 = (MultiplayerParticipant_t672 *)il2cpp_codegen_object_new (MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant__ctor_m2711(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.MultiplayerParticipant::AutomatchingSentinel()
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern "C" MultiplayerParticipant_t672 * MultiplayerParticipant_AutomatchingSentinel_m2722 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = Sentinels_Sentinels_AutomatchingParticipant_m2069(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant_t672 * L_1 = (MultiplayerParticipant_t672 *)il2cpp_codegen_object_new (MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant__ctor_m2711(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.MultiplayerParticipant::<DisplayName>m__7C(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerParticipant_U3CDisplayNameU3Em__7C_m2723 (MultiplayerParticipant_t672 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = MultiplayerParticipant_MultiplayerParticipant_DisplayName_m1803(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.MultiplayerParticipant::<Id>m__7D(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerParticipant_U3CIdU3Em__7D_m2724 (MultiplayerParticipant_t672 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = MultiplayerParticipant_MultiplayerParticipant_Id_m1811(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.Types/AchievementState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi_0.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi.h"
// GooglePlayGames.BasicApi.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achievement.h"
// GooglePlayGames.Native.Cwrapper.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_AchievemenMethodDeclarations.h"
// GooglePlayGames.BasicApi.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_AchievementMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeAchievement::.ctor(System.IntPtr)
extern "C" void NativeAchievement__ctor_m2725 (NativeAchievement_t673 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 GooglePlayGames.Native.NativeAchievement::CurrentSteps()
extern "C" uint32_t NativeAchievement_CurrentSteps_m2726 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = Achievement_Achievement_CurrentSteps_m1619(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.NativeAchievement::Description()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeAchievement_U3CDescriptionU3Em__6E_m2735_MethodInfo_var;
extern "C" String_t* NativeAchievement_Description_m2727 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeAchievement_U3CDescriptionU3Em__6E_m2735_MethodInfo_var = il2cpp_codegen_method_info_from_index(463);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeAchievement_U3CDescriptionU3Em__6E_m2735_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeAchievement::Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeAchievement_U3CIdU3Em__6F_m2736_MethodInfo_var;
extern "C" String_t* NativeAchievement_Id_m2728 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeAchievement_U3CIdU3Em__6F_m2736_MethodInfo_var = il2cpp_codegen_method_info_from_index(464);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeAchievement_U3CIdU3Em__6F_m2736_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeAchievement::Name()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeAchievement_U3CNameU3Em__70_m2737_MethodInfo_var;
extern "C" String_t* NativeAchievement_Name_m2729 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeAchievement_U3CNameU3Em__70_m2737_MethodInfo_var = il2cpp_codegen_method_info_from_index(465);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeAchievement_U3CNameU3Em__70_m2737_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/AchievementState GooglePlayGames.Native.NativeAchievement::State()
extern "C" int32_t NativeAchievement_State_m2730 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = Achievement_Achievement_State_m1620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 GooglePlayGames.Native.NativeAchievement::TotalSteps()
extern "C" uint32_t NativeAchievement_TotalSteps_m2731 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = Achievement_Achievement_TotalSteps_m1613(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/AchievementType GooglePlayGames.Native.NativeAchievement::Type()
extern "C" int32_t NativeAchievement_Type_m2732 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = Achievement_Achievement_Type_m1624(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.NativeAchievement::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeAchievement_CallDispose_m2733 (NativeAchievement_t673 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		Achievement_Achievement_Dispose_m1615(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.Native.NativeAchievement::AsAchievement()
extern TypeInfo* Achievement_t333_il2cpp_TypeInfo_var;
extern "C" Achievement_t333 * NativeAchievement_AsAchievement_m2734 (NativeAchievement_t673 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t333_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(597);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t333 * V_0 = {0};
	{
		Achievement_t333 * L_0 = (Achievement_t333 *)il2cpp_codegen_object_new (Achievement_t333_il2cpp_TypeInfo_var);
		Achievement__ctor_m1327(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t333 * L_1 = V_0;
		String_t* L_2 = NativeAchievement_Id_m2728(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Achievement_set_Id_m1340(L_1, L_2, /*hidden argument*/NULL);
		Achievement_t333 * L_3 = V_0;
		String_t* L_4 = NativeAchievement_Name_m2729(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Achievement_set_Name_m1344(L_3, L_4, /*hidden argument*/NULL);
		Achievement_t333 * L_5 = V_0;
		String_t* L_6 = NativeAchievement_Description_m2727(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Achievement_set_Description_m1342(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = NativeAchievement_Type_m2732(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_0055;
		}
	}
	{
		Achievement_t333 * L_8 = V_0;
		NullCheck(L_8);
		Achievement_set_IsIncremental_m1330(L_8, 1, /*hidden argument*/NULL);
		Achievement_t333 * L_9 = V_0;
		uint32_t L_10 = NativeAchievement_CurrentSteps_m2726(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Achievement_set_CurrentSteps_m1332(L_9, L_10, /*hidden argument*/NULL);
		Achievement_t333 * L_11 = V_0;
		uint32_t L_12 = NativeAchievement_TotalSteps_m2731(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Achievement_set_TotalSteps_m1334(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0055:
	{
		Achievement_t333 * L_13 = V_0;
		int32_t L_14 = NativeAchievement_State_m2730(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Achievement_set_IsRevealed_m1338(L_13, ((((int32_t)L_14) == ((int32_t)2))? 1 : 0), /*hidden argument*/NULL);
		Achievement_t333 * L_15 = V_0;
		int32_t L_16 = NativeAchievement_State_m2730(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Achievement_set_IsUnlocked_m1336(L_15, ((((int32_t)L_16) == ((int32_t)3))? 1 : 0), /*hidden argument*/NULL);
		Achievement_t333 * L_17 = V_0;
		return L_17;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeAchievement::<Description>m__6E(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeAchievement_U3CDescriptionU3Em__6E_m2735 (NativeAchievement_t673 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Achievement_Achievement_Description_m1614(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeAchievement::<Id>m__6F(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeAchievement_U3CIdU3Em__6F_m2736 (NativeAchievement_t673 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Achievement_Achievement_Id_m1625(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeAchievement::<Name>m__70(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeAchievement_U3CNameU3Em__70_m2737 (NativeAchievement_t673 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Achievement_Achievement_Name_m1626(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.BasicApi.Events.EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_EventVisib.h"
// GooglePlayGames.Native.Cwrapper.Types/EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Even.h"
// GooglePlayGames.Native.Cwrapper.Event
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeEvent::.ctor(System.IntPtr)
extern "C" void NativeEvent__ctor_m2738 (NativeEvent_t674 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeEvent::get_Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeEvent_U3Cget_IdU3Em__7E_m2747_MethodInfo_var;
extern "C" String_t* NativeEvent_get_Id_m2739 (NativeEvent_t674 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeEvent_U3Cget_IdU3Em__7E_m2747_MethodInfo_var = il2cpp_codegen_method_info_from_index(466);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeEvent_U3Cget_IdU3Em__7E_m2747_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeEvent::get_Name()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeEvent_U3Cget_NameU3Em__7F_m2748_MethodInfo_var;
extern "C" String_t* NativeEvent_get_Name_m2740 (NativeEvent_t674 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeEvent_U3Cget_NameU3Em__7F_m2748_MethodInfo_var = il2cpp_codegen_method_info_from_index(467);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeEvent_U3Cget_NameU3Em__7F_m2748_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeEvent::get_Description()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeEvent_U3Cget_DescriptionU3Em__80_m2749_MethodInfo_var;
extern "C" String_t* NativeEvent_get_Description_m2741 (NativeEvent_t674 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeEvent_U3Cget_DescriptionU3Em__80_m2749_MethodInfo_var = il2cpp_codegen_method_info_from_index(468);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeEvent_U3Cget_DescriptionU3Em__80_m2749_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeEvent::get_ImageUrl()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeEvent_U3Cget_ImageUrlU3Em__81_m2750_MethodInfo_var;
extern "C" String_t* NativeEvent_get_ImageUrl_m2742 (NativeEvent_t674 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeEvent_U3Cget_ImageUrlU3Em__81_m2750_MethodInfo_var = il2cpp_codegen_method_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeEvent_U3Cget_ImageUrlU3Em__81_m2750_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.UInt64 GooglePlayGames.Native.NativeEvent::get_CurrentCount()
extern "C" uint64_t NativeEvent_get_CurrentCount_m2743 (NativeEvent_t674 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint64_t L_1 = Event_Event_Count_m1690(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.Native.NativeEvent::get_Visibility()
extern TypeInfo* EventVisibility_t508_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t NativeEvent_get_Visibility_m2744 (NativeEvent_t674 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventVisibility_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(900);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = Event_Event_Visibility_m1693(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		goto IL_0025;
	}

IL_0021:
	{
		return (int32_t)(1);
	}

IL_0023:
	{
		return (int32_t)(2);
	}

IL_0025:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(EventVisibility_t508_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral577, L_7, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_9 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_9);
	}
}
// System.Void GooglePlayGames.Native.NativeEvent::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeEvent_CallDispose_m2745 (NativeEvent_t674 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		Event_Event_Dispose_m1696(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeEvent::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t239_il2cpp_TypeInfo_var;
extern TypeInfo* EventVisibility_t338_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* NativeEvent_ToString_m2746 (NativeEvent_t674 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		UInt64_t239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(175);
		EventVisibility_t338_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(901);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = BaseReferenceHolder_IsDisposed_m2625(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return (String_t*) &_stringLiteral578;
	}

IL_0011:
	{
		ObjectU5BU5D_t208* L_1 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 6));
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String GooglePlayGames.Native.NativeEvent::get_Id() */, __this);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)L_2;
		ObjectU5BU5D_t208* L_3 = L_1;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeEvent::get_Name() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 1)) = (Object_t *)L_4;
		ObjectU5BU5D_t208* L_5 = L_3;
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String GooglePlayGames.Native.NativeEvent::get_Description() */, __this);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2)) = (Object_t *)L_6;
		ObjectU5BU5D_t208* L_7 = L_5;
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String GooglePlayGames.Native.NativeEvent::get_ImageUrl() */, __this);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_8;
		ObjectU5BU5D_t208* L_9 = L_7;
		uint64_t L_10 = (uint64_t)VirtFuncInvoker0< uint64_t >::Invoke(10 /* System.UInt64 GooglePlayGames.Native.NativeEvent::get_CurrentCount() */, __this);
		uint64_t L_11 = L_10;
		Object_t * L_12 = Box(UInt64_t239_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 4)) = (Object_t *)L_12;
		ObjectU5BU5D_t208* L_13 = L_9;
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.Native.NativeEvent::get_Visibility() */, __this);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(EventVisibility_t338_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5)) = (Object_t *)L_16;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m3689(NULL /*static, unused*/, (String_t*) &_stringLiteral579, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_Id>m__7E(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_IdU3Em__7E_m2747 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Event_Event_Id_m1694(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_Name>m__7F(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_NameU3Em__7F_m2748 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Event_Event_Name_m1698(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_Description>m__80(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_DescriptionU3Em__80_m2749 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Event_Event_Description_m1691(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_ImageUrl>m__81(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_ImageUrlU3Em__81_m2750 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Event_Event_ImageUrl_m1692(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Imag.h"
// GooglePlayGames.Native.Cwrapper.Player
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativePlayer::.ctor(System.IntPtr)
extern "C" void NativePlayer__ctor_m2751 (NativePlayer_t675 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativePlayer::Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativePlayer_U3CIdU3Em__82_m2757_MethodInfo_var;
extern "C" String_t* NativePlayer_Id_m2752 (NativePlayer_t675 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativePlayer_U3CIdU3Em__82_m2757_MethodInfo_var = il2cpp_codegen_method_info_from_index(470);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativePlayer_U3CIdU3Em__82_m2757_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativePlayer::Name()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativePlayer_U3CNameU3Em__83_m2758_MethodInfo_var;
extern "C" String_t* NativePlayer_Name_m2753 (NativePlayer_t675 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativePlayer_U3CNameU3Em__83_m2758_MethodInfo_var = il2cpp_codegen_method_info_from_index(471);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativePlayer_U3CNameU3Em__83_m2758_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativePlayer::AvatarURL()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativePlayer_U3CAvatarURLU3Em__84_m2759_MethodInfo_var;
extern "C" String_t* NativePlayer_AvatarURL_m2754 (NativePlayer_t675 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativePlayer_U3CAvatarURLU3Em__84_m2759_MethodInfo_var = il2cpp_codegen_method_info_from_index(472);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativePlayer_U3CAvatarURLU3Em__84_m2759_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GooglePlayGames.Native.NativePlayer::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativePlayer_CallDispose_m2755 (NativePlayer_t675 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		Player_Player_Dispose_m1820(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Player GooglePlayGames.Native.NativePlayer::AsPlayer()
extern TypeInfo* Player_t347_il2cpp_TypeInfo_var;
extern "C" Player_t347 * NativePlayer_AsPlayer_m2756 (NativePlayer_t675 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Player_t347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = NativePlayer_Name_m2753(__this, /*hidden argument*/NULL);
		String_t* L_1 = NativePlayer_Id_m2752(__this, /*hidden argument*/NULL);
		String_t* L_2 = NativePlayer_AvatarURL_m2754(__this, /*hidden argument*/NULL);
		Player_t347 * L_3 = (Player_t347 *)il2cpp_codegen_object_new (Player_t347_il2cpp_TypeInfo_var);
		Player__ctor_m1396(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativePlayer::<Id>m__82(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativePlayer_U3CIdU3Em__82_m2757 (NativePlayer_t675 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Player_Player_Id_m1828(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativePlayer::<Name>m__83(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativePlayer_U3CNameU3Em__83_m2758 (NativePlayer_t675 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Player_Player_Name_m1819(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativePlayer::<AvatarURL>m__84(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativePlayer_U3CAvatarURLU3Em__84_m2759 (NativePlayer_t675 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Player_Player_AvatarUrl_m1821(NULL /*static, unused*/, L_0, 1, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.NativeQuest
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuest.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeQuest
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestMethodDeclarations.h"

// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Nullable`1<System.DateTime>
#include "mscorlib_System_Nullable_1_gen_0.h"
// GooglePlayGames.Native.NativeQuestMilestone
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestMileston.h"
// GooglePlayGames.BasicApi.Quests.QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestState.h"
// GooglePlayGames.Native.Cwrapper.Types/QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques.h"
// GooglePlayGames.Native.Cwrapper.Quest
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestMethodDeclarations.h"
// System.Nullable`1<System.DateTime>
#include "mscorlib_System_Nullable_1_gen_0MethodDeclarations.h"
// GooglePlayGames.Native.NativeQuestMilestone
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestMilestonMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeQuest::.ctor(System.IntPtr)
extern "C" void NativeQuest__ctor_m2760 (NativeQuest_t677 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeQuest::get_Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuest_U3Cget_IdU3Em__85_m2775_MethodInfo_var;
extern "C" String_t* NativeQuest_get_Id_m2761 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuest_U3Cget_IdU3Em__85_m2775_MethodInfo_var = il2cpp_codegen_method_info_from_index(473);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuest_U3Cget_IdU3Em__85_m2775_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeQuest::get_Name()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuest_U3Cget_NameU3Em__86_m2776_MethodInfo_var;
extern "C" String_t* NativeQuest_get_Name_m2762 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuest_U3Cget_NameU3Em__86_m2776_MethodInfo_var = il2cpp_codegen_method_info_from_index(474);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuest_U3Cget_NameU3Em__86_m2776_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeQuest::get_Description()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuest_U3Cget_DescriptionU3Em__87_m2777_MethodInfo_var;
extern "C" String_t* NativeQuest_get_Description_m2763 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuest_U3Cget_DescriptionU3Em__87_m2777_MethodInfo_var = il2cpp_codegen_method_info_from_index(475);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuest_U3Cget_DescriptionU3Em__87_m2777_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeQuest::get_BannerUrl()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuest_U3Cget_BannerUrlU3Em__88_m2778_MethodInfo_var;
extern "C" String_t* NativeQuest_get_BannerUrl_m2764 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuest_U3Cget_BannerUrlU3Em__88_m2778_MethodInfo_var = il2cpp_codegen_method_info_from_index(476);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuest_U3Cget_BannerUrlU3Em__88_m2778_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeQuest::get_IconUrl()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuest_U3Cget_IconUrlU3Em__89_m2779_MethodInfo_var;
extern "C" String_t* NativeQuest_get_IconUrl_m2765 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuest_U3Cget_IconUrlU3Em__89_m2779_MethodInfo_var = il2cpp_codegen_method_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuest_U3Cget_IconUrlU3Em__89_m2779_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.DateTime GooglePlayGames.Native.NativeQuest::get_StartTime()
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern "C" DateTime_t48  NativeQuest_get_StartTime_m2766 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int64_t L_1 = Quest_Quest_StartTime_m1863(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		DateTime_t48  L_2 = PInvokeUtilities_FromMillisSinceUnixEpoch_m2859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.DateTime GooglePlayGames.Native.NativeQuest::get_ExpirationTime()
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern "C" DateTime_t48  NativeQuest_get_ExpirationTime_m2767 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int64_t L_1 = Quest_Quest_ExpirationTime_m1858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		DateTime_t48  L_2 = PInvokeUtilities_FromMillisSinceUnixEpoch_m2859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Nullable`1<System.DateTime> GooglePlayGames.Native.NativeQuest::get_AcceptedTime()
extern TypeInfo* Nullable_1_t873_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* Nullable_1__ctor_m3928_MethodInfo_var;
extern "C" Nullable_1_t873  NativeQuest_get_AcceptedTime_m2768 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Nullable_1_t873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(902);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		Nullable_1__ctor_m3928_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484126);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	Nullable_1_t873  V_1 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int64_t L_1 = Quest_Quest_AcceptedTime_m1866(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int64_t L_2 = V_0;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		Initobj (Nullable_1_t873_il2cpp_TypeInfo_var, (&V_1));
		Nullable_1_t873  L_3 = V_1;
		return L_3;
	}

IL_001c:
	{
		int64_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		DateTime_t48  L_5 = PInvokeUtilities_FromMillisSinceUnixEpoch_m2859(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Nullable_1_t873  L_6 = {0};
		Nullable_1__ctor_m3928(&L_6, L_5, /*hidden argument*/Nullable_1__ctor_m3928_MethodInfo_var);
		return L_6;
	}
}
// GooglePlayGames.BasicApi.Quests.IQuestMilestone GooglePlayGames.Native.NativeQuest::get_Milestone()
extern "C" Object_t * NativeQuest_get_Milestone_m2769 (NativeQuest_t677 * __this, MethodInfo* method)
{
	{
		NativeQuestMilestone_t676 * L_0 = (__this->___mCachedMilestone_1);
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = Quest_Quest_CurrentMilestone_m1865(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeQuestMilestone_t676 * L_3 = NativeQuestMilestone_FromPointer_m2791(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->___mCachedMilestone_1 = L_3;
	}

IL_0025:
	{
		NativeQuestMilestone_t676 * L_4 = (__this->___mCachedMilestone_1);
		il2cpp_codegen_memory_barrier();
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Quests.QuestState GooglePlayGames.Native.NativeQuest::get_State()
extern TypeInfo* QuestState_t516_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t NativeQuest_get_State_m2770 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuestState_t516_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(903);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = Quest_Quest_State_m1860(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
		{
			goto IL_0037;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 3)
		{
			goto IL_0039;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 4)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 5)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_003f;
	}

IL_0033:
	{
		return (int32_t)(1);
	}

IL_0035:
	{
		return (int32_t)(2);
	}

IL_0037:
	{
		return (int32_t)(3);
	}

IL_0039:
	{
		return (int32_t)(4);
	}

IL_003b:
	{
		return (int32_t)(5);
	}

IL_003d:
	{
		return (int32_t)(6);
	}

IL_003f:
	{
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(QuestState_t516_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral580, L_6, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_8 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_8, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_8);
	}
}
// System.Boolean GooglePlayGames.Native.NativeQuest::Valid()
extern "C" bool NativeQuest_Valid_m2771 (NativeQuest_t677 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = Quest_Quest_Valid_m1862(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.NativeQuest::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeQuest_CallDispose_m2772 (NativeQuest_t677 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		Quest_Quest_Dispose_m1864(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeQuest::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* QuestState_t367_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t48_il2cpp_TypeInfo_var;
extern TypeInfo* Nullable_1_t873_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* NativeQuest_ToString_m2773 (NativeQuest_t677 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		QuestState_t367_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(904);
		DateTime_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Nullable_1_t873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(902);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = BaseReferenceHolder_IsDisposed_m2625(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return (String_t*) &_stringLiteral581;
	}

IL_0011:
	{
		ObjectU5BU5D_t208* L_1 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, ((int32_t)9)));
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String GooglePlayGames.Native.NativeQuest::get_Id() */, __this);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)L_2;
		ObjectU5BU5D_t208* L_3 = L_1;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeQuest::get_Name() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 1)) = (Object_t *)L_4;
		ObjectU5BU5D_t208* L_5 = L_3;
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String GooglePlayGames.Native.NativeQuest::get_Description() */, __this);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2)) = (Object_t *)L_6;
		ObjectU5BU5D_t208* L_7 = L_5;
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String GooglePlayGames.Native.NativeQuest::get_BannerUrl() */, __this);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_8;
		ObjectU5BU5D_t208* L_9 = L_7;
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String GooglePlayGames.Native.NativeQuest::get_IconUrl() */, __this);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 4)) = (Object_t *)L_10;
		ObjectU5BU5D_t208* L_11 = L_9;
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(15 /* GooglePlayGames.BasicApi.Quests.QuestState GooglePlayGames.Native.NativeQuest::get_State() */, __this);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(QuestState_t367_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5)) = (Object_t *)L_14;
		ObjectU5BU5D_t208* L_15 = L_11;
		DateTime_t48  L_16 = (DateTime_t48 )VirtFuncInvoker0< DateTime_t48  >::Invoke(11 /* System.DateTime GooglePlayGames.Native.NativeQuest::get_StartTime() */, __this);
		DateTime_t48  L_17 = L_16;
		Object_t * L_18 = Box(DateTime_t48_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 6);
		ArrayElementTypeCheck (L_15, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 6)) = (Object_t *)L_18;
		ObjectU5BU5D_t208* L_19 = L_15;
		DateTime_t48  L_20 = (DateTime_t48 )VirtFuncInvoker0< DateTime_t48  >::Invoke(12 /* System.DateTime GooglePlayGames.Native.NativeQuest::get_ExpirationTime() */, __this);
		DateTime_t48  L_21 = L_20;
		Object_t * L_22 = Box(DateTime_t48_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7)) = (Object_t *)L_22;
		ObjectU5BU5D_t208* L_23 = L_19;
		Nullable_1_t873  L_24 = (Nullable_1_t873 )VirtFuncInvoker0< Nullable_1_t873  >::Invoke(13 /* System.Nullable`1<System.DateTime> GooglePlayGames.Native.NativeQuest::get_AcceptedTime() */, __this);
		Nullable_1_t873  L_25 = L_24;
		Object_t * L_26 = Box(Nullable_1_t873_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 8);
		ArrayElementTypeCheck (L_23, L_26);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 8)) = (Object_t *)L_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m3689(NULL /*static, unused*/, (String_t*) &_stringLiteral582, L_23, /*hidden argument*/NULL);
		return L_27;
	}
}
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.NativeQuest::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern "C" NativeQuest_t677 * NativeQuest_FromPointer_m2774 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (NativeQuest_t677 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		NativeQuest_t677 * L_5 = (NativeQuest_t677 *)il2cpp_codegen_object_new (NativeQuest_t677_il2cpp_TypeInfo_var);
		NativeQuest__ctor_m2760(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_Id>m__85(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_IdU3Em__85_m2775 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Quest_Quest_Id_m1867(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_Name>m__86(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_NameU3Em__86_m2776 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Quest_Quest_Name_m1868(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_Description>m__87(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_DescriptionU3Em__87_m2777 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Quest_Quest_Description_m1856(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_BannerUrl>m__88(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_BannerUrlU3Em__88_m2778 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Quest_Quest_BannerUrl_m1857(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_IconUrl>m__89(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_IconUrlU3Em__89_m2779 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = Quest_Quest_IconUrl_m1859(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtil_2.h"
// GooglePlayGames.BasicApi.Quests.MilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_MilestoneS.h"
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques_0.h"
// GooglePlayGames.Native.Cwrapper.QuestMilestone
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestMilesMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PInvokeUtil_2MethodDeclarations.h"
struct PInvokeUtilities_t682;
struct ByteU5BU5D_t350;
struct OutMethod_1_t948;
// Declaration !!0[] GooglePlayGames.Native.PInvoke.PInvokeUtilities::OutParamsToArray<System.Byte>(GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<!!0>)
// !!0[] GooglePlayGames.Native.PInvoke.PInvokeUtilities::OutParamsToArray<System.Byte>(GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<!!0>)
extern "C" ByteU5BU5D_t350* PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_gshared (Object_t * __this /* static, unused */, OutMethod_1_t948 * p0, MethodInfo* method);
#define PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929(__this /* static, unused */, p0, method) (( ByteU5BU5D_t350* (*) (Object_t * /* static, unused */, OutMethod_1_t948 *, MethodInfo*))PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeQuestMilestone::.ctor(System.IntPtr)
extern "C" void NativeQuestMilestone__ctor_m2780 (NativeQuestMilestone_t676 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeQuestMilestone::get_Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuestMilestone_U3Cget_IdU3Em__8A_m2792_MethodInfo_var;
extern "C" String_t* NativeQuestMilestone_get_Id_m2781 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuestMilestone_U3Cget_IdU3Em__8A_m2792_MethodInfo_var = il2cpp_codegen_method_info_from_index(479);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuestMilestone_U3Cget_IdU3Em__8A_m2792_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeQuestMilestone::get_EventId()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuestMilestone_U3Cget_EventIdU3Em__8B_m2793_MethodInfo_var;
extern "C" String_t* NativeQuestMilestone_get_EventId_m2782 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuestMilestone_U3Cget_EventIdU3Em__8B_m2793_MethodInfo_var = il2cpp_codegen_method_info_from_index(480);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuestMilestone_U3Cget_EventIdU3Em__8B_m2793_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeQuestMilestone::get_QuestId()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuestMilestone_U3Cget_QuestIdU3Em__8C_m2794_MethodInfo_var;
extern "C" String_t* NativeQuestMilestone_get_QuestId_m2783 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuestMilestone_U3Cget_QuestIdU3Em__8C_m2794_MethodInfo_var = il2cpp_codegen_method_info_from_index(481);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuestMilestone_U3Cget_QuestIdU3Em__8C_m2794_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.UInt64 GooglePlayGames.Native.NativeQuestMilestone::get_CurrentCount()
extern "C" uint64_t NativeQuestMilestone_get_CurrentCount_m2784 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint64_t L_1 = QuestMilestone_QuestMilestone_CurrentCount_m1914(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt64 GooglePlayGames.Native.NativeQuestMilestone::get_TargetCount()
extern "C" uint64_t NativeQuestMilestone_get_TargetCount_m2785 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint64_t L_1 = QuestMilestone_QuestMilestone_TargetCount_m1917(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte[] GooglePlayGames.Native.NativeQuestMilestone::get_CompletionRewardData()
extern TypeInfo* OutMethod_1_t948_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__8D_m2795_MethodInfo_var;
extern MethodInfo* OutMethod_1__ctor_m3930_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_MethodInfo_var;
extern "C" ByteU5BU5D_t350* NativeQuestMilestone_get_CompletionRewardData_m2786 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutMethod_1_t948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(905);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__8D_m2795_MethodInfo_var = il2cpp_codegen_method_info_from_index(482);
		OutMethod_1__ctor_m3930_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484131);
		PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__8D_m2795_MethodInfo_var };
		OutMethod_1_t948 * L_1 = (OutMethod_1_t948 *)il2cpp_codegen_object_new (OutMethod_1_t948_il2cpp_TypeInfo_var);
		OutMethod_1__ctor_m3930(L_1, __this, L_0, /*hidden argument*/OutMethod_1__ctor_m3930_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		ByteU5BU5D_t350* L_2 = PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929(NULL /*static, unused*/, L_1, /*hidden argument*/PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_MethodInfo_var);
		return L_2;
	}
}
// GooglePlayGames.BasicApi.Quests.MilestoneState GooglePlayGames.Native.NativeQuestMilestone::get_State()
extern TypeInfo* QuestMilestoneState_t517_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t NativeQuestMilestone_get_State_m2787 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuestMilestoneState_t517_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = QuestMilestone_QuestMilestone_State_m1920(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
		{
			goto IL_002d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 3)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0033;
	}

IL_002b:
	{
		return (int32_t)(4);
	}

IL_002d:
	{
		return (int32_t)(3);
	}

IL_002f:
	{
		return (int32_t)(2);
	}

IL_0031:
	{
		return (int32_t)(1);
	}

IL_0033:
	{
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(QuestMilestoneState_t517_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral580, L_6, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_8 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_8, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_8);
	}
}
// System.Boolean GooglePlayGames.Native.NativeQuestMilestone::Valid()
extern "C" bool NativeQuestMilestone_Valid_m2788 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = QuestMilestone_QuestMilestone_Valid_m1921(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestMilestone::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeQuestMilestone_CallDispose_m2789 (NativeQuestMilestone_t676 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		QuestMilestone_QuestMilestone_Dispose_m1916(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeQuestMilestone::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t239_il2cpp_TypeInfo_var;
extern TypeInfo* MilestoneState_t368_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* NativeQuestMilestone_ToString_m2790 (NativeQuestMilestone_t676 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		UInt64_t239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(175);
		MilestoneState_t368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(907);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t208* L_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String GooglePlayGames.Native.NativeQuestMilestone::get_Id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t208* L_2 = L_0;
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeQuestMilestone::get_EventId() */, __this);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t208* L_4 = L_2;
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String GooglePlayGames.Native.NativeQuestMilestone::get_QuestId() */, __this);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_5;
		ObjectU5BU5D_t208* L_6 = L_4;
		uint64_t L_7 = (uint64_t)VirtFuncInvoker0< uint64_t >::Invoke(9 /* System.UInt64 GooglePlayGames.Native.NativeQuestMilestone::get_CurrentCount() */, __this);
		uint64_t L_8 = L_7;
		Object_t * L_9 = Box(UInt64_t239_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3)) = (Object_t *)L_9;
		ObjectU5BU5D_t208* L_10 = L_6;
		uint64_t L_11 = (uint64_t)VirtFuncInvoker0< uint64_t >::Invoke(10 /* System.UInt64 GooglePlayGames.Native.NativeQuestMilestone::get_TargetCount() */, __this);
		uint64_t L_12 = L_11;
		Object_t * L_13 = Box(UInt64_t239_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4)) = (Object_t *)L_13;
		ObjectU5BU5D_t208* L_14 = L_10;
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* GooglePlayGames.BasicApi.Quests.MilestoneState GooglePlayGames.Native.NativeQuestMilestone::get_State() */, __this);
		int32_t L_16 = L_15;
		Object_t * L_17 = Box(MilestoneState_t368_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 5);
		ArrayElementTypeCheck (L_14, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 5)) = (Object_t *)L_17;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m3689(NULL /*static, unused*/, (String_t*) &_stringLiteral583, L_14, /*hidden argument*/NULL);
		return L_18;
	}
}
// GooglePlayGames.Native.NativeQuestMilestone GooglePlayGames.Native.NativeQuestMilestone::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* NativeQuestMilestone_t676_il2cpp_TypeInfo_var;
extern "C" NativeQuestMilestone_t676 * NativeQuestMilestone_FromPointer_m2791 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		NativeQuestMilestone_t676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(733);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___pointer;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m3931(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return (NativeQuestMilestone_t676 *)NULL;
	}

IL_0012:
	{
		IntPtr_t L_3 = ___pointer;
		NativeQuestMilestone_t676 * L_4 = (NativeQuestMilestone_t676 *)il2cpp_codegen_object_new (NativeQuestMilestone_t676_il2cpp_TypeInfo_var);
		NativeQuestMilestone__ctor_m2780(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_Id>m__8A(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_IdU3Em__8A_m2792 (NativeQuestMilestone_t676 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = QuestMilestone_QuestMilestone_Id_m1922(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_EventId>m__8B(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_EventIdU3Em__8B_m2793 (NativeQuestMilestone_t676 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = QuestMilestone_QuestMilestone_EventId_m1913(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_QuestId>m__8C(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_QuestIdU3Em__8C_m2794 (NativeQuestMilestone_t676 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = QuestMilestone_QuestMilestone_QuestId_m1918(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_CompletionRewardData>m__8D(System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__8D_m2795 (NativeQuestMilestone_t676 * __this, ByteU5BU5D_t350* ___out_bytes, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_1 = ___out_bytes;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = QuestMilestone_QuestMilestone_CompletionRewardData_m1919(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "System_Core_System_Func_2_gen_11.h"
// GooglePlayGames.Native.Cwrapper.RealTimeRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeRoMethodDeclarations.h"
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "System_Core_System_Func_2_gen_11MethodDeclarations.h"
struct PInvokeUtilities_t682;
struct IEnumerable_1_t874;
struct Func_2_t949;
struct PInvokeUtilities_t682;
struct IEnumerable_1_t221;
struct Func_2_t935;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<System.Object>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
// System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<System.Object>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
extern "C" Object_t* PInvokeUtilities_ToEnumerable_TisObject_t_m3933_gshared (Object_t * __this /* static, unused */, UIntPtr_t  p0, Func_2_t935 * p1, MethodInfo* method);
#define PInvokeUtilities_ToEnumerable_TisObject_t_m3933(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, UIntPtr_t , Func_2_t935 *, MethodInfo*))PInvokeUtilities_ToEnumerable_TisObject_t_m3933_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
// System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, UIntPtr_t , Func_2_t949 *, MethodInfo*))PInvokeUtilities_ToEnumerable_TisObject_t_m3933_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::.ctor(System.IntPtr)
extern "C" void NativeRealTimeRoom__ctor_m2796 (NativeRealTimeRoom_t574 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeRealTimeRoom_U3CIdU3Em__8E_m2803_MethodInfo_var;
extern "C" String_t* NativeRealTimeRoom_Id_m2797 (NativeRealTimeRoom_t574 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeRealTimeRoom_U3CIdU3Em__8E_m2803_MethodInfo_var = il2cpp_codegen_method_info_from_index(485);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeRealTimeRoom_U3CIdU3Em__8E_m2803_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant> GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::Participants()
extern TypeInfo* Func_2_t949_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeRealTimeRoom_U3CParticipantsU3Em__8F_m2804_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3934_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932_MethodInfo_var;
extern "C" Object_t* NativeRealTimeRoom_Participants_m2798 (NativeRealTimeRoom_t574 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Func_2_t949_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(908);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeRealTimeRoom_U3CParticipantsU3Em__8F_m2804_MethodInfo_var = il2cpp_codegen_method_info_from_index(486);
		Func_2__ctor_m3934_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484135);
		PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484136);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = RealTimeRoom_RealTimeRoom_Participants_Length_m2012(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = { NativeRealTimeRoom_U3CParticipantsU3Em__8F_m2804_MethodInfo_var };
		Func_2_t949 * L_3 = (Func_2_t949 *)il2cpp_codegen_object_new (Func_2_t949_il2cpp_TypeInfo_var);
		Func_2__ctor_m3934(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m3934_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		Object_t* L_4 = PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932_MethodInfo_var);
		return L_4;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::ParticipantCount()
extern "C" uint32_t NativeRealTimeRoom_ParticipantCount_m2799 (NativeRealTimeRoom_t574 * __this, MethodInfo* method)
{
	UIntPtr_t  V_0 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = RealTimeRoom_RealTimeRoom_Participants_Length_m2012(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		uint32_t L_2 = UIntPtr_ToUInt32_m3910((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::Status()
extern "C" int32_t NativeRealTimeRoom_Status_m2800 (NativeRealTimeRoom_t574 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = RealTimeRoom_RealTimeRoom_Status_m2008(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeRealTimeRoom_CallDispose_m2801 (NativeRealTimeRoom_t574 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		RealTimeRoom_RealTimeRoom_Dispose_m2019(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* NativeRealTimeRoom_t574_il2cpp_TypeInfo_var;
extern "C" NativeRealTimeRoom_t574 * NativeRealTimeRoom_FromPointer_m2802 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		NativeRealTimeRoom_t574_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(749);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___selfPointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (NativeRealTimeRoom_t574 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___selfPointer;
		NativeRealTimeRoom_t574 * L_5 = (NativeRealTimeRoom_t574 *)il2cpp_codegen_object_new (NativeRealTimeRoom_t574_il2cpp_TypeInfo_var);
		NativeRealTimeRoom__ctor_m2796(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::<Id>m__8E(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeRealTimeRoom_U3CIdU3Em__8E_m2803 (NativeRealTimeRoom_t574 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = RealTimeRoom_RealTimeRoom_Id_m2018(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::<Participants>m__8F(System.UIntPtr)
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern "C" MultiplayerParticipant_t672 * NativeRealTimeRoom_U3CParticipantsU3Em__8F_m2804 (NativeRealTimeRoom_t574 * __this, UIntPtr_t  ___index, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index;
		IntPtr_t L_2 = RealTimeRoom_RealTimeRoom_Participants_GetElement_m2013(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant_t672 * L_3 = (MultiplayerParticipant_t672 *)il2cpp_codegen_object_new (MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant__ctor_m2711(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.SnapshotMetadata
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMeMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSnapshotMetadata::.ctor(System.IntPtr)
extern "C" void NativeSnapshotMetadata__ctor_m2805 (NativeSnapshotMetadata_t611 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeSnapshotMetadata::get_IsOpen()
extern "C" bool NativeSnapshotMetadata_get_IsOpen_m2806 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = SnapshotMetadata_SnapshotMetadata_IsOpen_m2119(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_Filename()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeSnapshotMetadata_U3Cget_FilenameU3Em__90_m2814_MethodInfo_var;
extern "C" String_t* NativeSnapshotMetadata_get_Filename_m2807 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeSnapshotMetadata_U3Cget_FilenameU3Em__90_m2814_MethodInfo_var = il2cpp_codegen_method_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeSnapshotMetadata_U3Cget_FilenameU3Em__90_m2814_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_Description()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeSnapshotMetadata_U3Cget_DescriptionU3Em__91_m2815_MethodInfo_var;
extern "C" String_t* NativeSnapshotMetadata_get_Description_m2808 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeSnapshotMetadata_U3Cget_DescriptionU3Em__91_m2815_MethodInfo_var = il2cpp_codegen_method_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeSnapshotMetadata_U3Cget_DescriptionU3Em__91_m2815_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_CoverImageURL()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeSnapshotMetadata_U3Cget_CoverImageURLU3Em__92_m2816_MethodInfo_var;
extern "C" String_t* NativeSnapshotMetadata_get_CoverImageURL_m2809 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeSnapshotMetadata_U3Cget_CoverImageURLU3Em__92_m2816_MethodInfo_var = il2cpp_codegen_method_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeSnapshotMetadata_U3Cget_CoverImageURLU3Em__92_m2816_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.TimeSpan GooglePlayGames.Native.NativeSnapshotMetadata::get_TotalTimePlayed()
extern TypeInfo* TimeSpan_t190_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t190  NativeSnapshotMetadata_get_TotalTimePlayed_m2810 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t190_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int64_t L_1 = SnapshotMetadata_SnapshotMetadata_PlayedTime_m2122(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int64_t L_2 = V_0;
		if ((((int64_t)L_2) >= ((int64_t)(((int64_t)0)))))
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t190_il2cpp_TypeInfo_var);
		TimeSpan_t190  L_3 = TimeSpan_FromMilliseconds_m861(NULL /*static, unused*/, (0.0), /*hidden argument*/NULL);
		return L_3;
	}

IL_0023:
	{
		int64_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t190_il2cpp_TypeInfo_var);
		TimeSpan_t190  L_5 = TimeSpan_FromMilliseconds_m861(NULL /*static, unused*/, (((double)L_4)), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.DateTime GooglePlayGames.Native.NativeSnapshotMetadata::get_LastModifiedTimestamp()
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern "C" DateTime_t48  NativeSnapshotMetadata_get_LastModifiedTimestamp_m2811 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int64_t L_1 = SnapshotMetadata_SnapshotMetadata_LastModifiedTime_m2123(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		DateTime_t48  L_2 = PInvokeUtilities_FromMillisSinceUnixEpoch_m2859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t190_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t48_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* NativeSnapshotMetadata_ToString_m2812 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		TimeSpan_t190_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		DateTime_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = BaseReferenceHolder_IsDisposed_m2625(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return (String_t*) &_stringLiteral584;
	}

IL_0011:
	{
		ObjectU5BU5D_t208* L_1 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 6));
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean GooglePlayGames.Native.NativeSnapshotMetadata::get_IsOpen() */, __this);
		bool L_3 = L_2;
		Object_t * L_4 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)L_4;
		ObjectU5BU5D_t208* L_5 = L_1;
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_Filename() */, __this);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1)) = (Object_t *)L_6;
		ObjectU5BU5D_t208* L_7 = L_5;
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_Description() */, __this);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2)) = (Object_t *)L_8;
		ObjectU5BU5D_t208* L_9 = L_7;
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_CoverImageURL() */, __this);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 3)) = (Object_t *)L_10;
		ObjectU5BU5D_t208* L_11 = L_9;
		TimeSpan_t190  L_12 = (TimeSpan_t190 )VirtFuncInvoker0< TimeSpan_t190  >::Invoke(10 /* System.TimeSpan GooglePlayGames.Native.NativeSnapshotMetadata::get_TotalTimePlayed() */, __this);
		TimeSpan_t190  L_13 = L_12;
		Object_t * L_14 = Box(TimeSpan_t190_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 4);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 4)) = (Object_t *)L_14;
		ObjectU5BU5D_t208* L_15 = L_11;
		DateTime_t48  L_16 = (DateTime_t48 )VirtFuncInvoker0< DateTime_t48  >::Invoke(11 /* System.DateTime GooglePlayGames.Native.NativeSnapshotMetadata::get_LastModifiedTimestamp() */, __this);
		DateTime_t48  L_17 = L_16;
		Object_t * L_18 = Box(DateTime_t48_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 5);
		ArrayElementTypeCheck (L_15, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 5)) = (Object_t *)L_18;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m3689(NULL /*static, unused*/, (String_t*) &_stringLiteral585, L_15, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void GooglePlayGames.Native.NativeSnapshotMetadata::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeSnapshotMetadata_CallDispose_m2813 (NativeSnapshotMetadata_t611 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		SnapshotMetadata_SnapshotMetadata_Dispose_m2116(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeSnapshotMetadata::<get_Filename>m__90(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeSnapshotMetadata_U3Cget_FilenameU3Em__90_m2814 (NativeSnapshotMetadata_t611 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = SnapshotMetadata_SnapshotMetadata_FileName_m2120(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeSnapshotMetadata::<get_Description>m__91(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeSnapshotMetadata_U3Cget_DescriptionU3Em__91_m2815 (NativeSnapshotMetadata_t611 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = SnapshotMetadata_SnapshotMetadata_Description_m2118(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.NativeSnapshotMetadata::<get_CoverImageURL>m__92(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeSnapshotMetadata_U3Cget_CoverImageURLU3Em__92_m2816 (NativeSnapshotMetadata_t611 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___out_size;
		UIntPtr_t  L_3 = SnapshotMetadata_SnapshotMetadata_CoverImageURL_m2117(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMe_1MethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// GooglePlayGames.Native.NativeSnapshotMetadataChange
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSnapshotMetad_1MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::.ctor()
extern "C" void Builder__ctor_m2817 (Builder_t678 * __this, MethodInfo* method)
{
	{
		IntPtr_t L_0 = SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Construct_m2133(NULL /*static, unused*/, /*hidden argument*/NULL);
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void Builder_CallDispose_m2818 (Builder_t678 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Dispose_m2137(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::SetDescription(System.String)
extern "C" Builder_t678 * Builder_SetDescription_m2819 (Builder_t678 * __this, String_t* ___description, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___description;
		SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetDescription_m2132(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::SetPlayedTime(System.UInt64)
extern "C" Builder_t678 * Builder_SetPlayedTime_m2820 (Builder_t678 * __this, uint64_t ___playedTime, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint64_t L_1 = ___playedTime;
		SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetPlayedTime_m2134(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::SetCoverImageFromPngData(System.Byte[])
extern MethodInfo* Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var;
extern "C" Builder_t678 * Builder_SetCoverImageFromPngData_m2821 (Builder_t678 * __this, ByteU5BU5D_t350* ___pngData, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483826);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t350* L_0 = ___pngData;
		Misc_CheckNotNull_TisByteU5BU5D_t350_m3707(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var);
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_2 = ___pngData;
		ByteU5BU5D_t350* L_3 = ___pngData;
		NullCheck(L_3);
		int64_t L_4 = Array_get_LongLength_m3935(L_3, /*hidden argument*/NULL);
		UIntPtr_t  L_5 = {0};
		UIntPtr__ctor_m3936(&L_5, L_4, /*hidden argument*/NULL);
		SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetCoverImageFromPngData_m2135(NULL /*static, unused*/, L_1, L_2, L_5, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.NativeSnapshotMetadataChange GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::Build()
extern "C" NativeSnapshotMetadataChange_t679 * Builder_Build_m2822 (Builder_t678 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Create_m2136(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NativeSnapshotMetadataChange_t679 * L_2 = NativeSnapshotMetadataChange_FromPointer_m2825(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMe_0MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange::.ctor(System.IntPtr)
extern "C" void NativeSnapshotMetadataChange__ctor_m2823 (NativeSnapshotMetadataChange_t679 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeSnapshotMetadataChange_CallDispose_m2824 (NativeSnapshotMetadataChange_t679 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		SnapshotMetadataChange_SnapshotMetadataChange_Dispose_m2129(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeSnapshotMetadataChange GooglePlayGames.Native.NativeSnapshotMetadataChange::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSnapshotMetadataChange_t679_il2cpp_TypeInfo_var;
extern "C" NativeSnapshotMetadataChange_t679 * NativeSnapshotMetadataChange_FromPointer_m2825 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		NativeSnapshotMetadataChange_t679_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(909);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (NativeSnapshotMetadataChange_t679 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		NativeSnapshotMetadataChange_t679 * L_5 = (NativeSnapshotMetadataChange_t679 *)il2cpp_codegen_object_new (NativeSnapshotMetadataChange_t679_il2cpp_TypeInfo_var);
		NativeSnapshotMetadataChange__ctor_m2823(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.Types/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc_0.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_0.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB.h"
// GooglePlayGames.Native.Cwrapper.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedMMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::.ctor(System.IntPtr)
extern "C" void NativeTurnBasedMatch__ctor_m2826 (NativeTurnBasedMatch_t680 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::AvailableAutomatchSlots()
extern "C" uint32_t NativeTurnBasedMatch_AvailableAutomatchSlots_m2827 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMatch_TurnBasedMatch_AutomatchingSlotsAvailable_m2138(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::CreationTime()
extern "C" uint64_t NativeTurnBasedMatch_CreationTime_m2828 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint64_t L_1 = TurnBasedMatch_TurnBasedMatch_CreationTime_m2139(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant> GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Participants()
extern TypeInfo* Func_2_t949_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeTurnBasedMatch_U3CParticipantsU3Em__93_m2846_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3934_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932_MethodInfo_var;
extern "C" Object_t* NativeTurnBasedMatch_Participants_m2829 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Func_2_t949_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(908);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeTurnBasedMatch_U3CParticipantsU3Em__93_m2846_MethodInfo_var = il2cpp_codegen_method_info_from_index(492);
		Func_2__ctor_m3934_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484135);
		PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484136);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMatch_TurnBasedMatch_Participants_Length_m2140(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = { NativeTurnBasedMatch_U3CParticipantsU3Em__93_m2846_MethodInfo_var };
		Func_2_t949 * L_3 = (Func_2_t949 *)il2cpp_codegen_object_new (Func_2_t949_il2cpp_TypeInfo_var);
		Func_2__ctor_m3934(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m3934_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		Object_t* L_4 = PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisMultiplayerParticipant_t672_m3932_MethodInfo_var);
		return L_4;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Version()
extern "C" uint32_t NativeTurnBasedMatch_Version_m2830 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMatch_TurnBasedMatch_Version_m2142(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Variant()
extern "C" uint32_t NativeTurnBasedMatch_Variant_m2831 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMatch_TurnBasedMatch_Variant_m2147(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.PInvoke.ParticipantResults GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Results()
extern TypeInfo* ParticipantResults_t683_il2cpp_TypeInfo_var;
extern "C" ParticipantResults_t683 * NativeTurnBasedMatch_Results_m2832 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticipantResults_t683_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(910);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = TurnBasedMatch_TurnBasedMatch_ParticipantResults_m2143(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ParticipantResults_t683 * L_2 = (ParticipantResults_t683 *)il2cpp_codegen_object_new (ParticipantResults_t683_il2cpp_TypeInfo_var);
		ParticipantResults__ctor_m2862(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::ParticipantWithId(System.String)
extern TypeInfo* IEnumerable_1_t874_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t925_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" MultiplayerParticipant_t672 * NativeTurnBasedMatch_ParticipantWithId_m2833 (NativeTurnBasedMatch_t680 * __this, String_t* ___participantId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(762);
		IEnumerator_1_t925_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(763);
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	MultiplayerParticipant_t672 * V_0 = {0};
	Object_t* V_1 = {0};
	MultiplayerParticipant_t672 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t* L_0 = NativeTurnBasedMatch_Participants_m2829(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GetEnumerator() */, IEnumerable_1_t874_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0036;
		}

IL_0011:
		{
			Object_t* L_2 = V_1;
			NullCheck(L_2);
			MultiplayerParticipant_t672 * L_3 = (MultiplayerParticipant_t672 *)InterfaceFuncInvoker0< MultiplayerParticipant_t672 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Current() */, IEnumerator_1_t925_il2cpp_TypeInfo_var, L_2);
			V_0 = L_3;
			MultiplayerParticipant_t672 * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = MultiplayerParticipant_Id_m2717(L_4, /*hidden argument*/NULL);
			String_t* L_6 = ___participantId;
			NullCheck(L_5);
			bool L_7 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_5, L_6);
			if (!L_7)
			{
				goto IL_0030;
			}
		}

IL_0029:
		{
			MultiplayerParticipant_t672 * L_8 = V_0;
			V_2 = L_8;
			IL2CPP_LEAVE(0x53, FINALLY_0046);
		}

IL_0030:
		{
			MultiplayerParticipant_t672 * L_9 = V_0;
			NullCheck(L_9);
			VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_9);
		}

IL_0036:
		{
			Object_t* L_10 = V_1;
			NullCheck(L_10);
			bool L_11 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0011;
			}
		}

IL_0041:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0046);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		{
			Object_t* L_12 = V_1;
			if (L_12)
			{
				goto IL_004a;
			}
		}

IL_0049:
		{
			IL2CPP_END_FINALLY(70)
		}

IL_004a:
		{
			Object_t* L_13 = V_1;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_13);
			IL2CPP_END_FINALLY(70)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0051:
	{
		return (MultiplayerParticipant_t672 *)NULL;
	}

IL_0053:
	{
		MultiplayerParticipant_t672 * L_14 = V_2;
		return L_14;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::PendingParticipant()
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern "C" MultiplayerParticipant_t672 * NativeTurnBasedMatch_PendingParticipant_m2834 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		s_Il2CppMethodIntialized = true;
	}
	MultiplayerParticipant_t672 * V_0 = {0};
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = TurnBasedMatch_TurnBasedMatch_PendingParticipant_m2146(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant_t672 * L_2 = (MultiplayerParticipant_t672 *)il2cpp_codegen_object_new (MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant__ctor_m2711(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		MultiplayerParticipant_t672 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = MultiplayerParticipant_Valid_m2718(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		MultiplayerParticipant_t672 * L_5 = V_0;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_5);
		return (MultiplayerParticipant_t672 *)NULL;
	}

IL_0024:
	{
		MultiplayerParticipant_t672 * L_6 = V_0;
		return L_6;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::MatchStatus()
extern "C" int32_t NativeTurnBasedMatch_MatchStatus_m2835 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = TurnBasedMatch_TurnBasedMatch_Status_m2144(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Description()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeTurnBasedMatch_U3CDescriptionU3Em__94_m2847_MethodInfo_var;
extern "C" String_t* NativeTurnBasedMatch_Description_m2836 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeTurnBasedMatch_U3CDescriptionU3Em__94_m2847_MethodInfo_var = il2cpp_codegen_method_info_from_index(493);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeTurnBasedMatch_U3CDescriptionU3Em__94_m2847_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::HasRematchId()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool NativeTurnBasedMatch_HasRematchId_m2837 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = NativeTurnBasedMatch_RematchId_m2838(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m899(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_3, (String_t*) &_stringLiteral427);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 1;
	}

IL_0023:
	{
		return G_B3_0;
	}
}
// System.String GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::RematchId()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeTurnBasedMatch_U3CRematchIdU3Em__95_m2848_MethodInfo_var;
extern "C" String_t* NativeTurnBasedMatch_RematchId_m2838 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeTurnBasedMatch_U3CRematchIdU3Em__95_m2848_MethodInfo_var = il2cpp_codegen_method_info_from_index(494);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeTurnBasedMatch_U3CRematchIdU3Em__95_m2848_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Byte[] GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Data()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* OutMethod_1_t948_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeTurnBasedMatch_U3CDataU3Em__96_m2849_MethodInfo_var;
extern MethodInfo* OutMethod_1__ctor_m3930_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_MethodInfo_var;
extern "C" ByteU5BU5D_t350* NativeTurnBasedMatch_Data_m2839 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		OutMethod_1_t948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(905);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeTurnBasedMatch_U3CDataU3Em__96_m2849_MethodInfo_var = il2cpp_codegen_method_info_from_index(495);
		OutMethod_1__ctor_m3930_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484131);
		PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		bool L_1 = TurnBasedMatch_TurnBasedMatch_HasData_m2151(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral586, /*hidden argument*/NULL);
		return (ByteU5BU5D_t350*)NULL;
	}

IL_001c:
	{
		IntPtr_t L_2 = { NativeTurnBasedMatch_U3CDataU3Em__96_m2849_MethodInfo_var };
		OutMethod_1_t948 * L_3 = (OutMethod_1_t948 *)il2cpp_codegen_object_new (OutMethod_1_t948_il2cpp_TypeInfo_var);
		OutMethod_1__ctor_m3930(L_3, __this, L_2, /*hidden argument*/OutMethod_1__ctor_m3930_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		ByteU5BU5D_t350* L_4 = PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929(NULL /*static, unused*/, L_3, /*hidden argument*/PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_MethodInfo_var);
		return L_4;
	}
}
// System.String GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Id()
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* NativeTurnBasedMatch_U3CIdU3Em__97_m2850_MethodInfo_var;
extern "C" String_t* NativeTurnBasedMatch_Id_m2840 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeTurnBasedMatch_U3CIdU3Em__97_m2850_MethodInfo_var = il2cpp_codegen_method_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { NativeTurnBasedMatch_U3CIdU3Em__97_m2850_MethodInfo_var };
		OutStringMethod_t681 * L_1 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_2 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeTurnBasedMatch_CallDispose_m2841 (NativeTurnBasedMatch_t680 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		TurnBasedMatch_TurnBasedMatch_Dispose_m2161(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::AsTurnBasedMatch(System.String)
extern TypeInfo* List_1_t351_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t874_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t925_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern TypeInfo* TurnBasedMatch_t353_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3797_MethodInfo_var;
extern "C" TurnBasedMatch_t353 * NativeTurnBasedMatch_AsTurnBasedMatch_m2842 (NativeTurnBasedMatch_t680 * __this, String_t* ___selfPlayerId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t351_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(747);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerable_1_t874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(762);
		IEnumerator_1_t925_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(763);
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		TurnBasedMatch_t353_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		List_1__ctor_m3797_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483917);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t351 * V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	MultiplayerParticipant_t672 * V_3 = {0};
	MultiplayerParticipant_t672 * V_4 = {0};
	Object_t* V_5 = {0};
	MultiplayerParticipant_t672 * V_6 = {0};
	NativePlayer_t675 * V_7 = {0};
	bool V_8 = false;
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B30_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t351_il2cpp_TypeInfo_var);
		List_1_t351 * L_0 = (List_1_t351 *)il2cpp_codegen_object_new (List_1_t351_il2cpp_TypeInfo_var);
		List_1__ctor_m3797(L_0, /*hidden argument*/List_1__ctor_m3797_MethodInfo_var);
		V_0 = L_0;
		V_1 = (String_t*)NULL;
		V_2 = (String_t*)NULL;
		MultiplayerParticipant_t672 * L_1 = NativeTurnBasedMatch_PendingParticipant_m2834(__this, /*hidden argument*/NULL);
		V_3 = L_1;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			MultiplayerParticipant_t672 * L_2 = V_3;
			if (!L_2)
			{
				goto IL_001e;
			}
		}

IL_0017:
		{
			MultiplayerParticipant_t672 * L_3 = V_3;
			NullCheck(L_3);
			String_t* L_4 = MultiplayerParticipant_Id_m2717(L_3, /*hidden argument*/NULL);
			V_2 = L_4;
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x30, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		{
			MultiplayerParticipant_t672 * L_5 = V_3;
			if (!L_5)
			{
				goto IL_002f;
			}
		}

IL_0029:
		{
			MultiplayerParticipant_t672 * L_6 = V_3;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_6);
		}

IL_002f:
		{
			IL2CPP_END_FINALLY(35)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x30, IL_0030)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0030:
	{
		Object_t* L_7 = NativeTurnBasedMatch_Participants_m2829(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Object_t* L_8 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GetEnumerator() */, IEnumerable_1_t874_il2cpp_TypeInfo_var, L_7);
		V_5 = L_8;
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ae;
		}

IL_0042:
		{
			Object_t* L_9 = V_5;
			NullCheck(L_9);
			MultiplayerParticipant_t672 * L_10 = (MultiplayerParticipant_t672 *)InterfaceFuncInvoker0< MultiplayerParticipant_t672 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Current() */, IEnumerator_1_t925_il2cpp_TypeInfo_var, L_9);
			V_4 = L_10;
			MultiplayerParticipant_t672 * L_11 = V_4;
			V_6 = L_11;
		}

IL_004f:
		try
		{ // begin try (depth: 2)
			{
				MultiplayerParticipant_t672 * L_12 = V_4;
				NullCheck(L_12);
				NativePlayer_t675 * L_13 = MultiplayerParticipant_Player_m2716(L_12, /*hidden argument*/NULL);
				V_7 = L_13;
			}

IL_0058:
			try
			{ // begin try (depth: 3)
				{
					NativePlayer_t675 * L_14 = V_7;
					if (!L_14)
					{
						goto IL_0079;
					}
				}

IL_005f:
				{
					NativePlayer_t675 * L_15 = V_7;
					NullCheck(L_15);
					String_t* L_16 = NativePlayer_Id_m2752(L_15, /*hidden argument*/NULL);
					String_t* L_17 = ___selfPlayerId;
					NullCheck(L_16);
					bool L_18 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_16, L_17);
					if (!L_18)
					{
						goto IL_0079;
					}
				}

IL_0071:
				{
					MultiplayerParticipant_t672 * L_19 = V_4;
					NullCheck(L_19);
					String_t* L_20 = MultiplayerParticipant_Id_m2717(L_19, /*hidden argument*/NULL);
					V_1 = L_20;
				}

IL_0079:
				{
					IL2CPP_LEAVE(0x8D, FINALLY_007e);
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t135 *)e.ex;
				goto FINALLY_007e;
			}

FINALLY_007e:
			{ // begin finally (depth: 3)
				{
					NativePlayer_t675 * L_21 = V_7;
					if (!L_21)
					{
						goto IL_008c;
					}
				}

IL_0085:
				{
					NativePlayer_t675 * L_22 = V_7;
					NullCheck(L_22);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_22);
				}

IL_008c:
				{
					IL2CPP_END_FINALLY(126)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(126)
			{
				IL2CPP_JUMP_TBL(0x8D, IL_008d)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
			}

IL_008d:
			{
				List_1_t351 * L_23 = V_0;
				MultiplayerParticipant_t672 * L_24 = V_4;
				NullCheck(L_24);
				Participant_t340 * L_25 = MultiplayerParticipant_AsParticipant_m2720(L_24, /*hidden argument*/NULL);
				NullCheck(L_23);
				VirtActionInvoker1< Participant_t340 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Add(!0) */, L_23, L_25);
				IL2CPP_LEAVE(0xAE, FINALLY_009f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_009f;
		}

FINALLY_009f:
		{ // begin finally (depth: 2)
			{
				MultiplayerParticipant_t672 * L_26 = V_6;
				if (!L_26)
				{
					goto IL_00ad;
				}
			}

IL_00a6:
			{
				MultiplayerParticipant_t672 * L_27 = V_6;
				NullCheck(L_27);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_27);
			}

IL_00ad:
			{
				IL2CPP_END_FINALLY(159)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(159)
		{
			IL2CPP_JUMP_TBL(0xAE, IL_00ae)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_00ae:
		{
			Object_t* L_28 = V_5;
			NullCheck(L_28);
			bool L_29 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_28);
			if (L_29)
			{
				goto IL_0042;
			}
		}

IL_00ba:
		{
			IL2CPP_LEAVE(0xCC, FINALLY_00bf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_00bf;
	}

FINALLY_00bf:
	{ // begin finally (depth: 1)
		{
			Object_t* L_30 = V_5;
			if (L_30)
			{
				goto IL_00c4;
			}
		}

IL_00c3:
		{
			IL2CPP_END_FINALLY(191)
		}

IL_00c4:
		{
			Object_t* L_31 = V_5;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_31);
			IL2CPP_END_FINALLY(191)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(191)
	{
		IL2CPP_JUMP_TBL(0xCC, IL_00cc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_00cc:
	{
		int32_t L_32 = NativeTurnBasedMatch_MatchStatus_m2835(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)5))))
		{
			goto IL_00e3;
		}
	}
	{
		bool L_33 = NativeTurnBasedMatch_HasRematchId_m2837(__this, /*hidden argument*/NULL);
		G_B30_0 = ((((int32_t)L_33) == ((int32_t)0))? 1 : 0);
		goto IL_00e4;
	}

IL_00e3:
	{
		G_B30_0 = 0;
	}

IL_00e4:
	{
		V_8 = G_B30_0;
		String_t* L_34 = NativeTurnBasedMatch_Id_m2840(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_35 = NativeTurnBasedMatch_Data_m2839(__this, /*hidden argument*/NULL);
		bool L_36 = V_8;
		String_t* L_37 = V_1;
		List_1_t351 * L_38 = V_0;
		uint32_t L_39 = NativeTurnBasedMatch_AvailableAutomatchSlots_m2827(__this, /*hidden argument*/NULL);
		String_t* L_40 = V_2;
		int32_t L_41 = NativeTurnBasedMatch_MatchStatus_m2835(__this, /*hidden argument*/NULL);
		int32_t L_42 = NativeTurnBasedMatch_ToTurnStatus_m2843(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		String_t* L_43 = V_2;
		int32_t L_44 = NativeTurnBasedMatch_MatchStatus_m2835(__this, /*hidden argument*/NULL);
		int32_t L_45 = NativeTurnBasedMatch_ToMatchStatus_m2844(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		uint32_t L_46 = NativeTurnBasedMatch_Variant_m2831(__this, /*hidden argument*/NULL);
		uint32_t L_47 = NativeTurnBasedMatch_Version_m2830(__this, /*hidden argument*/NULL);
		TurnBasedMatch_t353 * L_48 = (TurnBasedMatch_t353 *)il2cpp_codegen_object_new (TurnBasedMatch_t353_il2cpp_TypeInfo_var);
		TurnBasedMatch__ctor_m1403(L_48, L_34, L_35, L_36, L_37, L_38, L_39, L_40, L_42, L_45, L_46, L_47, /*hidden argument*/NULL);
		return L_48;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::ToTurnStatus(GooglePlayGames.Native.Cwrapper.Types/MatchStatus)
extern "C" int32_t NativeTurnBasedMatch_ToTurnStatus_m2843 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0037;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_002d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_002b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_002f;
		}
	}
	{
		goto IL_0039;
	}

IL_002b:
	{
		return (int32_t)(0);
	}

IL_002d:
	{
		return (int32_t)(0);
	}

IL_002f:
	{
		return (int32_t)(0);
	}

IL_0031:
	{
		return (int32_t)(1);
	}

IL_0033:
	{
		return (int32_t)(2);
	}

IL_0035:
	{
		return (int32_t)(0);
	}

IL_0037:
	{
		return (int32_t)(3);
	}

IL_0039:
	{
		return (int32_t)(4);
	}
}
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::ToMatchStatus(System.String,GooglePlayGames.Native.Cwrapper.Types/MatchStatus)
extern "C" int32_t NativeTurnBasedMatch_ToMatchStatus_m2844 (Object_t * __this /* static, unused */, String_t* ___pendingParticipantId, int32_t ___status, MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B11_0 = 0;
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0037;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_002d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_002b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_002f;
		}
	}
	{
		goto IL_0045;
	}

IL_002b:
	{
		return (int32_t)(2);
	}

IL_002d:
	{
		return (int32_t)(3);
	}

IL_002f:
	{
		return (int32_t)(4);
	}

IL_0031:
	{
		return (int32_t)(0);
	}

IL_0033:
	{
		return (int32_t)(0);
	}

IL_0035:
	{
		return (int32_t)(3);
	}

IL_0037:
	{
		String_t* L_2 = ___pendingParticipantId;
		if (L_2)
		{
			goto IL_0043;
		}
	}
	{
		G_B11_0 = 1;
		goto IL_0044;
	}

IL_0043:
	{
		G_B11_0 = 0;
	}

IL_0044:
	{
		return (int32_t)(G_B11_0);
	}

IL_0045:
	{
		return (int32_t)(5);
	}
}
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::FromPointer(System.IntPtr)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* NativeTurnBasedMatch_t680_il2cpp_TypeInfo_var;
extern "C" NativeTurnBasedMatch_t680 * NativeTurnBasedMatch_FromPointer_m2845 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		NativeTurnBasedMatch_t680_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(653);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeTurnBasedMatch_t680 *)NULL;
	}

IL_000d:
	{
		IntPtr_t L_2 = ___selfPointer;
		NativeTurnBasedMatch_t680 * L_3 = (NativeTurnBasedMatch_t680 *)il2cpp_codegen_object_new (NativeTurnBasedMatch_t680_il2cpp_TypeInfo_var);
		NativeTurnBasedMatch__ctor_m2826(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Participants>m__93(System.UIntPtr)
extern TypeInfo* MultiplayerParticipant_t672_il2cpp_TypeInfo_var;
extern "C" MultiplayerParticipant_t672 * NativeTurnBasedMatch_U3CParticipantsU3Em__93_m2846 (NativeTurnBasedMatch_t680 * __this, UIntPtr_t  ___index, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerParticipant_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(750);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index;
		IntPtr_t L_2 = TurnBasedMatch_TurnBasedMatch_Participants_GetElement_m2141(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant_t672 * L_3 = (MultiplayerParticipant_t672 *)il2cpp_codegen_object_new (MultiplayerParticipant_t672_il2cpp_TypeInfo_var);
		MultiplayerParticipant__ctor_m2711(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Description>m__94(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CDescriptionU3Em__94_m2847 (NativeTurnBasedMatch_t680 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = TurnBasedMatch_TurnBasedMatch_Description_m2145(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<RematchId>m__95(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CRematchIdU3Em__95_m2848 (NativeTurnBasedMatch_t680 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = TurnBasedMatch_TurnBasedMatch_RematchId_m2155(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Data>m__96(System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CDataU3Em__96_m2849 (NativeTurnBasedMatch_t680 * __this, ByteU5BU5D_t350* ___bytes, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_1 = ___bytes;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = TurnBasedMatch_TurnBasedMatch_Data_m2149(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Id>m__97(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CIdU3Em__97_m2850 (NativeTurnBasedMatch_t680 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		StringBuilder_t36 * L_1 = ___out_string;
		UIntPtr_t  L_2 = ___size;
		UIntPtr_t  L_3 = TurnBasedMatch_TurnBasedMatch_Id_m2160(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::.ctor(System.Object,System.IntPtr)
extern "C" void OutStringMethod__ctor_m2851 (OutStringMethod_t681 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::Invoke(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  OutStringMethod_Invoke_m2852 (OutStringMethod_t681 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OutStringMethod_Invoke_m2852((OutStringMethod_t681 *)__this->___prev_9,___out_string, ___out_size, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (Object_t *, Object_t * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___out_string, ___out_size,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (Object_t * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___out_string, ___out_size,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef UIntPtr_t  (*FunctionPointerType) (Object_t * __this, UIntPtr_t  ___out_size, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___out_string, ___out_size,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" UIntPtr_t  pinvoke_delegate_wrapper_OutStringMethod_t681(Il2CppObject* delegate, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size)
{
	typedef UIntPtr_t  (STDCALL *native_function_ptr_type)(char*, UIntPtr_t );
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___out_string' to native representation
	char* ____out_string_marshaled = { 0 };
	____out_string_marshaled = il2cpp_codegen_marshal_string_builder(___out_string);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____out_string_marshaled, ___out_size);

	// Marshaling of parameter '___out_string' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_string, ____out_string_marshaled);

	// Marshaling cleanup of parameter '___out_string' native representation
	il2cpp_codegen_marshal_free(____out_string_marshaled);
	____out_string_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::BeginInvoke(System.Text.StringBuilder,System.UIntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* UIntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OutStringMethod_BeginInvoke_m2853 (OutStringMethod_t681 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___out_string;
	__d_args[1] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___out_size);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::EndInvoke(System.IAsyncResult)
extern "C" UIntPtr_t  OutStringMethod_EndInvoke_m2854 (OutStringMethod_t681 * __this, Object_t * ___result, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(UIntPtr_t *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif

// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities::.cctor()
extern TypeInfo* DateTime_t48_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern "C" void PInvokeUtilities__cctor_m2855 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t48  L_0 = {0};
		DateTime__ctor_m3937(&L_0, ((int32_t)1970), 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t48_il2cpp_TypeInfo_var);
		DateTime_t48  L_1 = DateTime_SpecifyKind_m3938(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		((PInvokeUtilities_t682_StaticFields*)PInvokeUtilities_t682_il2cpp_TypeInfo_var->static_fields)->___UnixEpoch_0 = L_1;
		return;
	}
}
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.PInvokeUtilities::CheckNonNull(System.Runtime.InteropServices.HandleRef)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" HandleRef_t657  PInvokeUtilities_CheckNonNull_m2856 (Object_t * __this /* static, unused */, HandleRef_t657  ___reference, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = ___reference;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t905 * L_2 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3939(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0011:
	{
		HandleRef_t657  L_3 = ___reference;
		return L_3;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities::IsNull(System.Runtime.InteropServices.HandleRef)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern "C" bool PInvokeUtilities_IsNull_m2857 (Object_t * __this /* static, unused */, HandleRef_t657  ___reference, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = ___reference;
		IntPtr_t L_1 = HandleRef_ToIntPtr_m3926(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_2 = PInvokeUtilities_IsNull_m2858(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities::IsNull(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool PInvokeUtilities_IsNull_m2858 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.DateTime GooglePlayGames.Native.PInvoke.PInvokeUtilities::FromMillisSinceUnixEpoch(System.Int64)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t190_il2cpp_TypeInfo_var;
extern "C" DateTime_t48  PInvokeUtilities_FromMillisSinceUnixEpoch_m2859 (Object_t * __this /* static, unused */, int64_t ___millisSinceEpoch, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		TimeSpan_t190_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t48  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		DateTime_t48  L_0 = ((PInvokeUtilities_t682_StaticFields*)PInvokeUtilities_t682_il2cpp_TypeInfo_var->static_fields)->___UnixEpoch_0;
		V_0 = L_0;
		int64_t L_1 = ___millisSinceEpoch;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t190_il2cpp_TypeInfo_var);
		TimeSpan_t190  L_2 = TimeSpan_FromMilliseconds_m861(NULL /*static, unused*/, (((double)L_1)), /*hidden argument*/NULL);
		DateTime_t48  L_3 = DateTime_Add_m3940((&V_0), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String GooglePlayGames.Native.PInvoke.PInvokeUtilities::OutParamsToString(GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod)
extern TypeInfo* UIntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t36_il2cpp_TypeInfo_var;
extern "C" String_t* PInvokeUtilities_OutParamsToString_m2860 (Object_t * __this /* static, unused */, OutStringMethod_t681 * ___outStringMethod, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		StringBuilder_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	UIntPtr_t  V_0 = {0};
	StringBuilder_t36 * V_1 = {0};
	{
		OutStringMethod_t681 * L_0 = ___outStringMethod;
		IL2CPP_RUNTIME_CLASS_INIT(UIntPtr_t_il2cpp_TypeInfo_var);
		UIntPtr_t  L_1 = ((UIntPtr_t_StaticFields*)UIntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_0;
		NullCheck(L_0);
		UIntPtr_t  L_2 = (UIntPtr_t )VirtFuncInvoker2< UIntPtr_t , StringBuilder_t36 *, UIntPtr_t  >::Invoke(10 /* System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::Invoke(System.Text.StringBuilder,System.UIntPtr) */, L_0, (StringBuilder_t36 *)NULL, L_1);
		V_0 = L_2;
		UIntPtr_t  L_3 = ((UIntPtr_t_StaticFields*)UIntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_0;
		UIntPtr_t  L_4 = L_3;
		Object_t * L_5 = Box(UIntPtr_t_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = UIntPtr_Equals_m3941((&V_0), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0025;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0025:
	{
		uint32_t L_7 = UIntPtr_ToUInt32_m3910((&V_0), /*hidden argument*/NULL);
		StringBuilder_t36 * L_8 = (StringBuilder_t36 *)il2cpp_codegen_object_new (StringBuilder_t36_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3942(L_8, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		OutStringMethod_t681 * L_9 = ___outStringMethod;
		StringBuilder_t36 * L_10 = V_1;
		UIntPtr_t  L_11 = V_0;
		NullCheck(L_9);
		VirtFuncInvoker2< UIntPtr_t , StringBuilder_t36 *, UIntPtr_t  >::Invoke(10 /* System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::Invoke(System.Text.StringBuilder,System.UIntPtr) */, L_9, L_10, L_11);
		StringBuilder_t36 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Text.StringBuilder::ToString() */, L_12);
		return L_13;
	}
}
// System.Int64 GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToMilliseconds(System.TimeSpan)
extern TypeInfo* Convert_t184_il2cpp_TypeInfo_var;
extern "C" int64_t PInvokeUtilities_ToMilliseconds_m2861 (Object_t * __this /* static, unused */, TimeSpan_t190  ___span, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		double L_0 = TimeSpan_get_TotalMilliseconds_m3711((&___span), /*hidden argument*/NULL);
		V_0 = L_0;
		double L_1 = V_0;
		if ((!(((double)L_1) > ((double)(9.2233720368547758E+18)))))
		{
			goto IL_0021;
		}
	}
	{
		return ((int64_t)std::numeric_limits<int64_t>::max());
	}

IL_0021:
	{
		double L_2 = V_0;
		if ((!(((double)L_2) < ((double)(-9.2233720368547758E+18)))))
		{
			goto IL_003a;
		}
	}
	{
		return ((int64_t)std::numeric_limits<int64_t>::min());
	}

IL_003a:
	{
		double L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t184_il2cpp_TypeInfo_var);
		int64_t L_4 = Convert_ToInt64_m3943(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.Native.Cwrapper.ParticipantResults
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_ParticipanMethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.ParticipantResults::.ctor(System.IntPtr)
extern "C" void ParticipantResults__ctor_m2862 (ParticipantResults_t683 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.ParticipantResults::HasResultsForParticipant(System.String)
extern "C" bool ParticipantResults_HasResultsForParticipant_m2863 (ParticipantResults_t683 * __this, String_t* ___participantId, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___participantId;
		bool L_2 = ParticipantResults_ParticipantResults_HasResultsForParticipant_m1816(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.ParticipantResults::PlacingForParticipant(System.String)
extern "C" uint32_t ParticipantResults_PlacingForParticipant_m2864 (ParticipantResults_t683 * __this, String_t* ___participantId, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___participantId;
		uint32_t L_2 = ParticipantResults_ParticipantResults_PlaceForParticipant_m1815(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.PInvoke.ParticipantResults::ResultsForParticipant(System.String)
extern "C" int32_t ParticipantResults_ResultsForParticipant_m2865 (ParticipantResults_t683 * __this, String_t* ___participantId, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___participantId;
		int32_t L_2 = ParticipantResults_ParticipantResults_MatchResultForParticipant_m1814(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.Native.PInvoke.ParticipantResults GooglePlayGames.Native.PInvoke.ParticipantResults::WithResult(System.String,System.UInt32,GooglePlayGames.Native.Cwrapper.Types/MatchResult)
extern TypeInfo* ParticipantResults_t683_il2cpp_TypeInfo_var;
extern "C" ParticipantResults_t683 * ParticipantResults_WithResult_m2866 (ParticipantResults_t683 * __this, String_t* ___participantId, uint32_t ___placing, int32_t ___result, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticipantResults_t683_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(910);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___participantId;
		uint32_t L_2 = ___placing;
		int32_t L_3 = ___result;
		IntPtr_t L_4 = ParticipantResults_ParticipantResults_WithResult_m1812(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		ParticipantResults_t683 * L_5 = (ParticipantResults_t683 *)il2cpp_codegen_object_new (ParticipantResults_t683_il2cpp_TypeInfo_var);
		ParticipantResults__ctor_m2862(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.ParticipantResults::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ParticipantResults_CallDispose_m2867 (ParticipantResults_t683 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		ParticipantResults_ParticipantResults_Dispose_m1817(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.Native.PInvoke.PlatformConfiguration::.ctor(System.IntPtr)
extern "C" void PlatformConfiguration__ctor_m2868 (PlatformConfiguration_t669 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.PlatformConfiguration::AsHandle()
extern "C" HandleRef_t657  PlatformConfiguration_AsHandle_m2869 (PlatformConfiguration_t669 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManager_Fetch.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManager_FetchMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.PlayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana_2MethodDeclarations.h"


// System.Void GooglePlayGames.Native.PlayerManager/FetchSelfResponse::.ctor(System.IntPtr)
extern "C" void FetchSelfResponse__ctor_m2870 (FetchSelfResponse_t684 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PlayerManager/FetchSelfResponse::Status()
extern "C" int32_t FetchSelfResponse_Status_m2871 (FetchSelfResponse_t684 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = PlayerManager_PlayerManager_FetchSelfResponse_GetStatus_m1847(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativePlayer GooglePlayGames.Native.PlayerManager/FetchSelfResponse::Self()
extern TypeInfo* NativePlayer_t675_il2cpp_TypeInfo_var;
extern "C" NativePlayer_t675 * FetchSelfResponse_Self_m2872 (FetchSelfResponse_t684 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativePlayer_t675_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(899);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = PlayerManager_PlayerManager_FetchSelfResponse_GetData_m1848(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NativePlayer_t675 * L_2 = (NativePlayer_t675 *)il2cpp_codegen_object_new (NativePlayer_t675_il2cpp_TypeInfo_var);
		NativePlayer__ctor_m2751(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GooglePlayGames.Native.PlayerManager/FetchSelfResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchSelfResponse_CallDispose_m2873 (FetchSelfResponse_t684 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		PlayerManager_PlayerManager_FetchSelfResponse_Dispose_m1846(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse GooglePlayGames.Native.PlayerManager/FetchSelfResponse::FromPointer(System.IntPtr)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* FetchSelfResponse_t684_il2cpp_TypeInfo_var;
extern "C" FetchSelfResponse_t684 * FetchSelfResponse_FromPointer_m2874 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		FetchSelfResponse_t684_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (FetchSelfResponse_t684 *)NULL;
	}

IL_000d:
	{
		IntPtr_t L_2 = ___selfPointer;
		FetchSelfResponse_t684 * L_3 = (FetchSelfResponse_t684 *)il2cpp_codegen_object_new (FetchSelfResponse_t684_il2cpp_TypeInfo_var);
		FetchSelfResponse__ctor_m2870(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Action`1<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>
#include "mscorlib_System_Action_1_gen_18.h"
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PlayerManager/FetchSelfResponse>
#include "System_Core_System_Func_2_gen_12.h"
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerManaMethodDeclarations.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PlayerManager/FetchSelfResponse>
#include "System_Core_System_Func_2_gen_12MethodDeclarations.h"
struct Callbacks_t661;
struct Action_1_t875;
struct Func_2_t950;
// Declaration System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisFetchSelfResponse_t684_m3944(__this /* static, unused */, p0, p1, method) (( IntPtr_t (*) (Object_t * /* static, unused */, Action_1_t875 *, Func_2_t950 *, MethodInfo*))Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void GooglePlayGames.Native.PlayerManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern MethodInfo* Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var;
extern "C" void PlayerManager__ctor_m2875 (PlayerManager_t685 * __this, GameServices_t534 * ___services, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484067);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		GameServices_t534 * L_0 = ___services;
		GameServices_t534 * L_1 = Misc_CheckNotNull_TisGameServices_t534_m3891(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisGameServices_t534_m3891_MethodInfo_var);
		__this->___mGameServices_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.PlayerManager::FetchSelf(System.Action`1<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>)
extern TypeInfo* FetchSelfCallback_t439_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t950_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* PlayerManager_InternalFetchSelfCallback_m2877_MethodInfo_var;
extern MethodInfo* FetchSelfResponse_FromPointer_m2874_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3945_MethodInfo_var;
extern MethodInfo* Callbacks_ToIntPtr_TisFetchSelfResponse_t684_m3944_MethodInfo_var;
extern "C" void PlayerManager_FetchSelf_m2876 (PlayerManager_t685 * __this, Action_1_t875 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchSelfCallback_t439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Func_2_t950_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(912);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		PlayerManager_InternalFetchSelfCallback_m2877_MethodInfo_var = il2cpp_codegen_method_info_from_index(497);
		FetchSelfResponse_FromPointer_m2874_MethodInfo_var = il2cpp_codegen_method_info_from_index(498);
		Func_2__ctor_m3945_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484147);
		Callbacks_ToIntPtr_TisFetchSelfResponse_t684_m3944_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484148);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t534 * L_0 = (__this->___mGameServices_0);
		NullCheck(L_0);
		HandleRef_t657  L_1 = GameServices_AsHandle_m2668(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = { PlayerManager_InternalFetchSelfCallback_m2877_MethodInfo_var };
		FetchSelfCallback_t439 * L_3 = (FetchSelfCallback_t439 *)il2cpp_codegen_object_new (FetchSelfCallback_t439_il2cpp_TypeInfo_var);
		FetchSelfCallback__ctor_m1829(L_3, NULL, L_2, /*hidden argument*/NULL);
		Action_1_t875 * L_4 = ___callback;
		IntPtr_t L_5 = { FetchSelfResponse_FromPointer_m2874_MethodInfo_var };
		Func_2_t950 * L_6 = (Func_2_t950 *)il2cpp_codegen_object_new (Func_2_t950_il2cpp_TypeInfo_var);
		Func_2__ctor_m3945(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m3945_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		IntPtr_t L_7 = Callbacks_ToIntPtr_TisFetchSelfResponse_t684_m3944(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/Callbacks_ToIntPtr_TisFetchSelfResponse_t684_m3944_MethodInfo_var);
		PlayerManager_PlayerManager_FetchSelf_m1845(NULL /*static, unused*/, L_1, 1, L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PlayerManager::InternalFetchSelfCallback(System.IntPtr,System.IntPtr)
void STDCALL native_delegate_wrapper_PlayerManager_InternalFetchSelfCallback_m2877(IntPtr_t ___response, IntPtr_t ___data)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___response' to managed representation

	// Marshaling of parameter '___data' to managed representation

	PlayerManager_InternalFetchSelfCallback_m2877(NULL, ___response, ___data, NULL);

	// Marshaling of parameter '___response' to native representation

	// Marshaling of parameter '___data' to native representation

}
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern "C" void PlayerManager_InternalFetchSelfCallback_m2877 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response;
		IntPtr_t L_1 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2638(NULL /*static, unused*/, (String_t*) &_stringLiteral587, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PlayerSelec.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PlayerSelecMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_7MethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62::.ctor()
extern "C" void U3CPlayerIdAtIndexU3Ec__AnonStorey62__ctor_m2878 (U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62::<>m__98(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  U3CPlayerIdAtIndexU3Ec__AnonStorey62_U3CU3Em__98_m2879 (U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method)
{
	{
		PlayerSelectUIResponse_t686 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(L_0, /*hidden argument*/NULL);
		UIntPtr_t  L_2 = (__this->___index_0);
		StringBuilder_t36 * L_3 = ___out_string;
		UIntPtr_t  L_4 = ___size;
		UIntPtr_t  L_5 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement_m2235(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Func`2<System.UIntPtr,System.String>
#include "System_Core_System_Func_2_gen_13.h"
// System.Func`2<System.UIntPtr,System.String>
#include "System_Core_System_Func_2_gen_13MethodDeclarations.h"
struct PInvokeUtilities_t682;
struct IEnumerator_1_t34;
struct Func_2_t951;
// Declaration System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<System.String>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
// System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<System.String>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerator_TisString_t_m3946(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, UIntPtr_t , Func_2_t951 *, MethodInfo*))PInvokeUtilities_ToEnumerator_TisObject_t_m3887_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::.ctor(System.IntPtr)
extern "C" void PlayerSelectUIResponse__ctor_m2880 (PlayerSelectUIResponse_t686 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * PlayerSelectUIResponse_System_Collections_IEnumerable_GetEnumerator_m2881 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(7 /* System.Collections.Generic.IEnumerator`1<System.String> GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::GetEnumerator() */, __this);
		return L_0;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::Status()
extern "C" int32_t PlayerSelectUIResponse_Status_m2882 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus_m2233(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::PlayerIdAtIndex(System.UIntPtr)
extern TypeInfo* U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687_il2cpp_TypeInfo_var;
extern TypeInfo* OutStringMethod_t681_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* U3CPlayerIdAtIndexU3Ec__AnonStorey62_U3CU3Em__98_m2879_MethodInfo_var;
extern "C" String_t* PlayerSelectUIResponse_PlayerIdAtIndex_m2883 (PlayerSelectUIResponse_t686 * __this, UIntPtr_t  ___index, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(913);
		OutStringMethod_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		U3CPlayerIdAtIndexU3Ec__AnonStorey62_U3CU3Em__98_m2879_MethodInfo_var = il2cpp_codegen_method_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * V_0 = {0};
	{
		U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * L_0 = (U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 *)il2cpp_codegen_object_new (U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687_il2cpp_TypeInfo_var);
		U3CPlayerIdAtIndexU3Ec__AnonStorey62__ctor_m2878(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * L_1 = V_0;
		UIntPtr_t  L_2 = ___index;
		NullCheck(L_1);
		L_1->___index_0 = L_2;
		U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * L_4 = V_0;
		IntPtr_t L_5 = { U3CPlayerIdAtIndexU3Ec__AnonStorey62_U3CU3Em__98_m2879_MethodInfo_var };
		OutStringMethod_t681 * L_6 = (OutStringMethod_t681 *)il2cpp_codegen_object_new (OutStringMethod_t681_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2851(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		String_t* L_7 = PInvokeUtilities_OutParamsToString_m2860(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::GetEnumerator()
extern TypeInfo* Func_2_t951_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* PlayerSelectUIResponse_PlayerIdAtIndex_m2883_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3947_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_ToEnumerator_TisString_t_m3946_MethodInfo_var;
extern "C" Object_t* PlayerSelectUIResponse_GetEnumerator_m2884 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Func_2_t951_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(914);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		PlayerSelectUIResponse_PlayerIdAtIndex_m2883_MethodInfo_var = il2cpp_codegen_method_info_from_index(502);
		Func_2__ctor_m3947_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484151);
		PInvokeUtilities_ToEnumerator_TisString_t_m3946_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484152);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length_m2234(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = { PlayerSelectUIResponse_PlayerIdAtIndex_m2883_MethodInfo_var };
		Func_2_t951 * L_3 = (Func_2_t951 *)il2cpp_codegen_object_new (Func_2_t951_il2cpp_TypeInfo_var);
		Func_2__ctor_m3947(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m3947_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		Object_t* L_4 = PInvokeUtilities_ToEnumerator_TisString_t_m3946(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerator_TisString_t_m3946_MethodInfo_var);
		return L_4;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::MinimumAutomatchingPlayers()
extern "C" uint32_t PlayerSelectUIResponse_MinimumAutomatchingPlayers_m2885 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers_m2236(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::MaximumAutomatchingPlayers()
extern "C" uint32_t PlayerSelectUIResponse_MaximumAutomatchingPlayers_m2886 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers_m2237(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void PlayerSelectUIResponse_CallDispose_m2887 (PlayerSelectUIResponse_t686 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose_m2232(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::FromPointer(System.IntPtr)
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern TypeInfo* PlayerSelectUIResponse_t686_il2cpp_TypeInfo_var;
extern "C" PlayerSelectUIResponse_t686 * PlayerSelectUIResponse_FromPointer_m2888 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		PlayerSelectUIResponse_t686_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(792);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___pointer;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m2858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (PlayerSelectUIResponse_t686 *)NULL;
	}

IL_000d:
	{
		IntPtr_t L_2 = ___pointer;
		PlayerSelectUIResponse_t686 * L_3 = (PlayerSelectUIResponse_t686 *)il2cpp_codegen_object_new (PlayerSelectUIResponse_t686_il2cpp_TypeInfo_var);
		PlayerSelectUIResponse__ctor_m2880(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManageMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.QuestManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_4MethodDeclarations.h"


// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::.ctor(System.IntPtr)
extern "C" void FetchResponse__ctor_m2889 (FetchResponse_t688 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::ResponseStatus()
extern "C" int32_t FetchResponse_ResponseStatus_m2890 (FetchResponse_t688 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = QuestManager_QuestManager_FetchResponse_GetStatus_m1896(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::Data()
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern "C" NativeQuest_t677 * FetchResponse_Data_m2891 (FetchResponse_t688 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = FetchResponse_RequestSucceeded_m2892(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeQuest_t677 *)NULL;
	}

IL_000d:
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = QuestManager_QuestManager_FetchResponse_GetData_m1897(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeQuest_t677 * L_3 = (NativeQuest_t677 *)il2cpp_codegen_object_new (NativeQuest_t677_il2cpp_TypeInfo_var);
		NativeQuest__ctor_m2760(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::RequestSucceeded()
extern "C" bool FetchResponse_RequestSucceeded_m2892 (FetchResponse_t688 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = FetchResponse_ResponseStatus_m2890(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchResponse_CallDispose_m2893 (FetchResponse_t688 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		QuestManager_QuestManager_FetchResponse_Dispose_m1895(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* FetchResponse_t688_il2cpp_TypeInfo_var;
extern "C" FetchResponse_t688 * FetchResponse_FromPointer_m2894 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		FetchResponse_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(708);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchResponse_t688 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		FetchResponse_t688 * L_5 = (FetchResponse_t688 *)il2cpp_codegen_object_new (FetchResponse_t688_il2cpp_TypeInfo_var);
		FetchResponse__ctor_m2889(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_0MethodDeclarations.h"

// System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeQuest>
#include "System_Core_System_Func_2_gen_14.h"
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeQuest>
#include "System_Core_System_Func_2_gen_14MethodDeclarations.h"
struct PInvokeUtilities_t682;
struct IEnumerable_1_t876;
struct Func_2_t952;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<GooglePlayGames.Native.NativeQuest>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
// System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<GooglePlayGames.Native.NativeQuest>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerable_TisNativeQuest_t677_m3948(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, UIntPtr_t , Func_2_t952 *, MethodInfo*))PInvokeUtilities_ToEnumerable_TisObject_t_m3933_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::.ctor(System.IntPtr)
extern "C" void FetchListResponse__ctor_m2895 (FetchListResponse_t689 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::ResponseStatus()
extern "C" int32_t FetchListResponse_ResponseStatus_m2896 (FetchListResponse_t689 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = QuestManager_QuestManager_FetchListResponse_GetStatus_m1899(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::RequestSucceeded()
extern "C" bool FetchListResponse_RequestSucceeded_m2897 (FetchListResponse_t689 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = FetchListResponse_ResponseStatus_m2896(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.NativeQuest> GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::Data()
extern TypeInfo* Func_2_t952_il2cpp_TypeInfo_var;
extern TypeInfo* PInvokeUtilities_t682_il2cpp_TypeInfo_var;
extern MethodInfo* FetchListResponse_U3CDataU3Em__99_m2901_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3949_MethodInfo_var;
extern MethodInfo* PInvokeUtilities_ToEnumerable_TisNativeQuest_t677_m3948_MethodInfo_var;
extern "C" Object_t* FetchListResponse_Data_m2898 (FetchListResponse_t689 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Func_2_t952_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(915);
		PInvokeUtilities_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		FetchListResponse_U3CDataU3Em__99_m2901_MethodInfo_var = il2cpp_codegen_method_info_from_index(505);
		Func_2__ctor_m3949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484154);
		PInvokeUtilities_ToEnumerable_TisNativeQuest_t677_m3948_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484155);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = QuestManager_QuestManager_FetchListResponse_GetData_Length_m1900(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = { FetchListResponse_U3CDataU3Em__99_m2901_MethodInfo_var };
		Func_2_t952 * L_3 = (Func_2_t952 *)il2cpp_codegen_object_new (Func_2_t952_il2cpp_TypeInfo_var);
		Func_2__ctor_m3949(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m3949_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t682_il2cpp_TypeInfo_var);
		Object_t* L_4 = PInvokeUtilities_ToEnumerable_TisNativeQuest_t677_m3948(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisNativeQuest_t677_m3948_MethodInfo_var);
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchListResponse_CallDispose_m2899 (FetchListResponse_t689 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		QuestManager_QuestManager_FetchListResponse_Dispose_m1898(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* FetchListResponse_t689_il2cpp_TypeInfo_var;
extern "C" FetchListResponse_t689 * FetchListResponse_FromPointer_m2900 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		FetchListResponse_t689_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(713);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchListResponse_t689 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		FetchListResponse_t689 * L_5 = (FetchListResponse_t689 *)il2cpp_codegen_object_new (FetchListResponse_t689_il2cpp_TypeInfo_var);
		FetchListResponse__ctor_m2895(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::<Data>m__99(System.UIntPtr)
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern "C" NativeQuest_t677 * FetchListResponse_U3CDataU3Em__99_m2901 (FetchListResponse_t689 * __this, UIntPtr_t  ___index, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index;
		IntPtr_t L_2 = QuestManager_QuestManager_FetchListResponse_GetData_GetElement_m1901(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		NativeQuest_t677 * L_3 = (NativeQuest_t677 *)il2cpp_codegen_object_new (NativeQuest_t677_il2cpp_TypeInfo_var);
		NativeQuest__ctor_m2760(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_1MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_5.h"


// System.Void GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::.ctor(System.IntPtr)
extern "C" void ClaimMilestoneResponse__ctor_m2902 (ClaimMilestoneResponse_t690 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::ResponseStatus()
extern "C" int32_t ClaimMilestoneResponse_ResponseStatus_m2903 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = QuestManager_QuestManager_ClaimMilestoneResponse_GetStatus_m1906(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::Quest()
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern "C" NativeQuest_t677 * ClaimMilestoneResponse_Quest_m2904 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuest_t677 * V_0 = {0};
	{
		bool L_0 = ClaimMilestoneResponse_RequestSucceeded_m2906(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeQuest_t677 *)NULL;
	}

IL_000d:
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = QuestManager_QuestManager_ClaimMilestoneResponse_GetQuest_m1908(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeQuest_t677 * L_3 = (NativeQuest_t677 *)il2cpp_codegen_object_new (NativeQuest_t677_il2cpp_TypeInfo_var);
		NativeQuest__ctor_m2760(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		NativeQuest_t677 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = NativeQuest_Valid_m2771(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		NativeQuest_t677 * L_6 = V_0;
		return L_6;
	}

IL_002b:
	{
		NativeQuest_t677 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_7);
		return (NativeQuest_t677 *)NULL;
	}
}
// GooglePlayGames.Native.NativeQuestMilestone GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::ClaimedMilestone()
extern TypeInfo* NativeQuestMilestone_t676_il2cpp_TypeInfo_var;
extern "C" NativeQuestMilestone_t676 * ClaimMilestoneResponse_ClaimedMilestone_m2905 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuestMilestone_t676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(733);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuestMilestone_t676 * V_0 = {0};
	{
		bool L_0 = ClaimMilestoneResponse_RequestSucceeded_m2906(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeQuestMilestone_t676 *)NULL;
	}

IL_000d:
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = QuestManager_QuestManager_ClaimMilestoneResponse_GetClaimedMilestone_m1907(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeQuestMilestone_t676 * L_3 = (NativeQuestMilestone_t676 *)il2cpp_codegen_object_new (NativeQuestMilestone_t676_il2cpp_TypeInfo_var);
		NativeQuestMilestone__ctor_m2780(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		NativeQuestMilestone_t676 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = NativeQuestMilestone_Valid_m2788(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		NativeQuestMilestone_t676 * L_6 = V_0;
		return L_6;
	}

IL_002b:
	{
		NativeQuestMilestone_t676 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_7);
		return (NativeQuestMilestone_t676 *)NULL;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::RequestSucceeded()
extern "C" bool ClaimMilestoneResponse_RequestSucceeded_m2906 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = ClaimMilestoneResponse_ResponseStatus_m2903(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ClaimMilestoneResponse_CallDispose_m2907 (ClaimMilestoneResponse_t690 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		QuestManager_QuestManager_ClaimMilestoneResponse_Dispose_m1905(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ClaimMilestoneResponse_t690_il2cpp_TypeInfo_var;
extern "C" ClaimMilestoneResponse_t690 * ClaimMilestoneResponse_FromPointer_m2908 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		ClaimMilestoneResponse_t690_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(731);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (ClaimMilestoneResponse_t690 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		ClaimMilestoneResponse_t690 * L_5 = (ClaimMilestoneResponse_t690 *)il2cpp_codegen_object_new (ClaimMilestoneResponse_t690_il2cpp_TypeInfo_var);
		ClaimMilestoneResponse__ctor_m2902(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_2MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_4.h"


// System.Void GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::.ctor(System.IntPtr)
extern "C" void AcceptResponse__ctor_m2909 (AcceptResponse_t691 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::ResponseStatus()
extern "C" int32_t AcceptResponse_ResponseStatus_m2910 (AcceptResponse_t691 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = QuestManager_QuestManager_AcceptResponse_GetStatus_m1903(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::AcceptedQuest()
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern "C" NativeQuest_t677 * AcceptResponse_AcceptedQuest_m2911 (AcceptResponse_t691 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AcceptResponse_RequestSucceeded_m2912(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeQuest_t677 *)NULL;
	}

IL_000d:
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = QuestManager_QuestManager_AcceptResponse_GetAcceptedQuest_m1904(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeQuest_t677 * L_3 = (NativeQuest_t677 *)il2cpp_codegen_object_new (NativeQuest_t677_il2cpp_TypeInfo_var);
		NativeQuest__ctor_m2760(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::RequestSucceeded()
extern "C" bool AcceptResponse_RequestSucceeded_m2912 (AcceptResponse_t691 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = AcceptResponse_ResponseStatus_m2910(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void AcceptResponse_CallDispose_m2913 (AcceptResponse_t691 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		QuestManager_QuestManager_AcceptResponse_Dispose_m1902(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* AcceptResponse_t691_il2cpp_TypeInfo_var;
extern "C" AcceptResponse_t691 * AcceptResponse_FromPointer_m2914 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		AcceptResponse_t691_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(725);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (AcceptResponse_t691 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		AcceptResponse_t691 * L_5 = (AcceptResponse_t691 *)il2cpp_codegen_object_new (AcceptResponse_t691_il2cpp_TypeInfo_var);
		AcceptResponse__ctor_m2909(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::.ctor(System.IntPtr)
extern "C" void QuestUIResponse__ctor_m2915 (QuestUIResponse_t692 * __this, IntPtr_t ___selfPointer, MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___selfPointer;
		BaseReferenceHolder__ctor_m2624(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::RequestStatus()
extern "C" int32_t QuestUIResponse_RequestStatus_m2916 (QuestUIResponse_t692 * __this, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		int32_t L_1 = QuestManager_QuestManager_QuestUIResponse_GetStatus_m1910(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::RequestSucceeded()
extern "C" bool QuestUIResponse_RequestSucceeded_m2917 (QuestUIResponse_t692 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = QuestUIResponse_RequestStatus_m2916(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::AcceptedQuest()
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern "C" NativeQuest_t677 * QuestUIResponse_AcceptedQuest_m2918 (QuestUIResponse_t692 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuest_t677 * V_0 = {0};
	{
		bool L_0 = QuestUIResponse_RequestSucceeded_m2917(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeQuest_t677 *)NULL;
	}

IL_000d:
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = QuestManager_QuestManager_QuestUIResponse_GetAcceptedQuest_m1911(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeQuest_t677 * L_3 = (NativeQuest_t677 *)il2cpp_codegen_object_new (NativeQuest_t677_il2cpp_TypeInfo_var);
		NativeQuest__ctor_m2760(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		NativeQuest_t677 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = NativeQuest_Valid_m2771(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		NativeQuest_t677 * L_6 = V_0;
		return L_6;
	}

IL_002b:
	{
		NativeQuest_t677 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_7);
		return (NativeQuest_t677 *)NULL;
	}
}
// GooglePlayGames.Native.NativeQuestMilestone GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::MilestoneToClaim()
extern TypeInfo* NativeQuestMilestone_t676_il2cpp_TypeInfo_var;
extern "C" NativeQuestMilestone_t676 * QuestUIResponse_MilestoneToClaim_m2919 (QuestUIResponse_t692 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuestMilestone_t676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(733);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuestMilestone_t676 * V_0 = {0};
	{
		bool L_0 = QuestUIResponse_RequestSucceeded_m2917(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeQuestMilestone_t676 *)NULL;
	}

IL_000d:
	{
		HandleRef_t657  L_1 = BaseReferenceHolder_SelfPtr_m2626(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = QuestManager_QuestManager_QuestUIResponse_GetMilestoneToClaim_m1912(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeQuestMilestone_t676 * L_3 = (NativeQuestMilestone_t676 *)il2cpp_codegen_object_new (NativeQuestMilestone_t676_il2cpp_TypeInfo_var);
		NativeQuestMilestone__ctor_m2780(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		NativeQuestMilestone_t676 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = NativeQuestMilestone_Valid_m2788(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		NativeQuestMilestone_t676 * L_6 = V_0;
		return L_6;
	}

IL_002b:
	{
		NativeQuestMilestone_t676 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_7);
		return (NativeQuestMilestone_t676 *)NULL;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void QuestUIResponse_CallDispose_m2920 (QuestUIResponse_t692 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method)
{
	{
		HandleRef_t657  L_0 = ___selfPointer;
		QuestManager_QuestManager_QuestUIResponse_Dispose_m1909(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::FromPointer(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* QuestUIResponse_t692_il2cpp_TypeInfo_var;
extern "C" QuestUIResponse_t692 * QuestUIResponse_FromPointer_m2921 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		QuestUIResponse_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(720);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3885((&___pointer), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (QuestUIResponse_t692 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer;
		QuestUIResponse_t692 * L_5 = (QuestUIResponse_t692 *)il2cpp_codegen_object_new (QuestUIResponse_t692_il2cpp_TypeInfo_var);
		QuestUIResponse__ctor_m2915(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
