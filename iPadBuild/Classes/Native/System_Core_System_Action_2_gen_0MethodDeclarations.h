﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<Soomla.Store.PurchasableVirtualItem,System.String>
struct Action_2_t95;
// System.Object
struct Object_t;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<Soomla.Store.PurchasableVirtualItem,System.String>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Object,System.Object>
#include "System_Core_System_Action_2_gen_17MethodDeclarations.h"
#define Action_2__ctor_m953(__this, ___object, ___method, method) (( void (*) (Action_2_t95 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m16118_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<Soomla.Store.PurchasableVirtualItem,System.String>::Invoke(T1,T2)
#define Action_2_Invoke_m17241(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t95 *, PurchasableVirtualItem_t124 *, String_t*, MethodInfo*))Action_2_Invoke_m16120_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<Soomla.Store.PurchasableVirtualItem,System.String>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m17242(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t95 *, PurchasableVirtualItem_t124 *, String_t*, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m16122_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<Soomla.Store.PurchasableVirtualItem,System.String>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m17243(__this, ___result, method) (( void (*) (Action_2_t95 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m16124_gshared)(__this, ___result, method)
