﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52
struct U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52::.ctor()
extern "C" void U3CRegisterMatchDelegateU3Ec__AnonStorey52__ctor_m2552 (U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52::<>m__5C(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C" void U3CRegisterMatchDelegateU3Ec__AnonStorey52_U3CU3Em__5C_m2553 (U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639 * __this, TurnBasedMatch_t353 * ___match, bool ___autoLaunch, MethodInfo* method) IL2CPP_METHOD_ATTR;
