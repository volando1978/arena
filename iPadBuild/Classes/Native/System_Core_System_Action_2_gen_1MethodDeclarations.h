﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG>
struct Action_2_t94;
// System.Object
struct Object_t;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Object,System.Object>
#include "System_Core_System_Action_2_gen_17MethodDeclarations.h"
#define Action_2__ctor_m955(__this, ___object, ___method, method) (( void (*) (Action_2_t94 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m16118_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG>::Invoke(T1,T2)
#define Action_2_Invoke_m17247(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t94 *, VirtualGood_t79 *, UpgradeVG_t133 *, MethodInfo*))Action_2_Invoke_m16120_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m17248(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t94 *, VirtualGood_t79 *, UpgradeVG_t133 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m16122_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m17249(__this, ___result, method) (( void (*) (Action_2_t94 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m16124_gshared)(__this, ___result, method)
