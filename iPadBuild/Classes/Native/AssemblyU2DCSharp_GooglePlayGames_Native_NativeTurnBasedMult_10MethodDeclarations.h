﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58
struct U3CLeaveU3Ec__AnonStorey58_t647;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58::.ctor()
extern "C" void U3CLeaveU3Ec__AnonStorey58__ctor_m2567 (U3CLeaveU3Ec__AnonStorey58_t647 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58::<>m__62(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CLeaveU3Ec__AnonStorey58_U3CU3Em__62_m2568 (U3CLeaveU3Ec__AnonStorey58_t647 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey58::<>m__6B(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C" void U3CLeaveU3Ec__AnonStorey58_U3CU3Em__6B_m2569 (U3CLeaveU3Ec__AnonStorey58_t647 * __this, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
