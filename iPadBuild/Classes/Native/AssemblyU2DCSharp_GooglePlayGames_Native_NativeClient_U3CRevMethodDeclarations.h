﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E
struct U3CRevealAchievementU3Ec__AnonStorey1E_t527;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;

// System.Void GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E::.ctor()
extern "C" void U3CRevealAchievementU3Ec__AnonStorey1E__ctor_m2243 (U3CRevealAchievementU3Ec__AnonStorey1E_t527 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E::<>m__16(GooglePlayGames.BasicApi.Achievement)
extern "C" void U3CRevealAchievementU3Ec__AnonStorey1E_U3CU3Em__16_m2244 (U3CRevealAchievementU3Ec__AnonStorey1E_t527 * __this, Achievement_t333 * ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
