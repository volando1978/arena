﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// System.Collections.ArrayList
struct ArrayList_t737;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// circCont
struct  circCont_t777  : public MonoBehaviour_t26
{
	// UnityEngine.Vector3 circCont::tra
	Vector3_t758  ___tra_2;
	// UnityEngine.Vector3 circCont::rot
	Vector3_t758  ___rot_3;
	// System.Single circCont::traV
	float ___traV_4;
	// System.Single circCont::rotV
	float ___rotV_5;
	// UnityEngine.GameObject circCont::circular
	GameObject_t144 * ___circular_6;
	// System.Collections.ArrayList circCont::abec
	ArrayList_t737 * ___abec_7;
	// System.Int32 circCont::dia
	int32_t ___dia_8;
	// System.Single circCont::len
	float ___len_9;
};
