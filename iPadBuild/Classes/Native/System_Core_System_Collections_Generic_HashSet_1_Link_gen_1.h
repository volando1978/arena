﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.HashSet`1/Link<System.Int32>
struct  Link_t3760 
{
	// System.Int32 System.Collections.Generic.HashSet`1/Link<System.Int32>::HashCode
	int32_t ___HashCode_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Link<System.Int32>::Next
	int32_t ___Next_1;
};
