﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t1961_il2cpp_TypeInfo;
extern TypeInfo Locale_t1962_il2cpp_TypeInfo;
extern TypeInfo MonoTODOAttribute_t1963_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t2165_il2cpp_TypeInfo;
extern TypeInfo Stack_1_t2164_il2cpp_TypeInfo;
extern TypeInfo HybridDictionary_t1965_il2cpp_TypeInfo;
extern TypeInfo DictionaryNode_t1966_il2cpp_TypeInfo;
extern TypeInfo DictionaryNodeEnumerator_t1967_il2cpp_TypeInfo;
extern TypeInfo DictionaryNodeCollectionEnumerator_t1969_il2cpp_TypeInfo;
extern TypeInfo DictionaryNodeCollection_t1970_il2cpp_TypeInfo;
extern TypeInfo ListDictionary_t1964_il2cpp_TypeInfo;
extern TypeInfo _Item_t1972_il2cpp_TypeInfo;
extern TypeInfo _KeysEnumerator_t1974_il2cpp_TypeInfo;
extern TypeInfo KeysCollection_t1975_il2cpp_TypeInfo;
extern TypeInfo NameObjectCollectionBase_t1973_il2cpp_TypeInfo;
extern TypeInfo NameValueCollection_t1978_il2cpp_TypeInfo;
extern TypeInfo EditorBrowsableAttribute_t1703_il2cpp_TypeInfo;
extern TypeInfo EditorBrowsableState_t1979_il2cpp_TypeInfo;
extern TypeInfo TypeConverter_t1980_il2cpp_TypeInfo;
extern TypeInfo TypeConverterAttribute_t1981_il2cpp_TypeInfo;
extern TypeInfo Stopwatch_t44_il2cpp_TypeInfo;
extern TypeInfo AuthenticationLevel_t1982_il2cpp_TypeInfo;
extern TypeInfo SslPolicyErrors_t1983_il2cpp_TypeInfo;
extern TypeInfo AddressFamily_t1984_il2cpp_TypeInfo;
extern TypeInfo DefaultCertificatePolicy_t1985_il2cpp_TypeInfo;
extern TypeInfo FileWebRequest_t1989_il2cpp_TypeInfo;
extern TypeInfo FileWebRequestCreator_t1991_il2cpp_TypeInfo;
extern TypeInfo FtpRequestCreator_t1992_il2cpp_TypeInfo;
extern TypeInfo FtpWebRequest_t1994_il2cpp_TypeInfo;
extern TypeInfo GlobalProxySelection_t1995_il2cpp_TypeInfo;
extern TypeInfo HttpRequestCreator_t1996_il2cpp_TypeInfo;
extern TypeInfo HttpVersion_t1998_il2cpp_TypeInfo;
extern TypeInfo HttpWebRequest_t2001_il2cpp_TypeInfo;
extern TypeInfo ICertificatePolicy_t2006_il2cpp_TypeInfo;
extern TypeInfo ICredentials_t2009_il2cpp_TypeInfo;
extern TypeInfo IPAddress_t2002_il2cpp_TypeInfo;
extern TypeInfo IPv6Address_t2003_il2cpp_TypeInfo;
extern TypeInfo IWebProxy_t1988_il2cpp_TypeInfo;
extern TypeInfo IWebRequestCreate_t2166_il2cpp_TypeInfo;
extern TypeInfo SecurityProtocolType_t2004_il2cpp_TypeInfo;
extern TypeInfo ServicePoint_t2000_il2cpp_TypeInfo;
extern TypeInfo SPKey_t2005_il2cpp_TypeInfo;
extern TypeInfo ServicePointManager_t2007_il2cpp_TypeInfo;
extern TypeInfo WebHeaderCollection_t1987_il2cpp_TypeInfo;
extern TypeInfo WebProxy_t2010_il2cpp_TypeInfo;
extern TypeInfo WebRequest_t1990_il2cpp_TypeInfo;
extern TypeInfo OpenFlags_t2012_il2cpp_TypeInfo;
extern TypeInfo PublicKey_t2016_il2cpp_TypeInfo;
extern TypeInfo StoreLocation_t2017_il2cpp_TypeInfo;
extern TypeInfo StoreName_t2018_il2cpp_TypeInfo;
extern TypeInfo X500DistinguishedName_t2019_il2cpp_TypeInfo;
extern TypeInfo X500DistinguishedNameFlags_t2020_il2cpp_TypeInfo;
extern TypeInfo X509BasicConstraintsExtension_t2021_il2cpp_TypeInfo;
extern TypeInfo X509Certificate2_t2025_il2cpp_TypeInfo;
extern TypeInfo X509Certificate2Collection_t2027_il2cpp_TypeInfo;
extern TypeInfo X509Certificate2Enumerator_t2028_il2cpp_TypeInfo;
extern TypeInfo X509CertificateEnumerator_t2029_il2cpp_TypeInfo;
extern TypeInfo X509CertificateCollection_t1999_il2cpp_TypeInfo;
extern TypeInfo X509Chain_t2037_il2cpp_TypeInfo;
extern TypeInfo X509ChainElement_t2035_il2cpp_TypeInfo;
extern TypeInfo X509ChainElementCollection_t2031_il2cpp_TypeInfo;
extern TypeInfo X509ChainElementEnumerator_t2038_il2cpp_TypeInfo;
extern TypeInfo X509ChainPolicy_t2032_il2cpp_TypeInfo;
extern TypeInfo X509ChainStatus_t2034_il2cpp_TypeInfo;
extern TypeInfo X509ChainStatusFlags_t2040_il2cpp_TypeInfo;
extern TypeInfo X509EnhancedKeyUsageExtension_t2041_il2cpp_TypeInfo;
extern TypeInfo X509Extension_t2022_il2cpp_TypeInfo;
extern TypeInfo X509ExtensionCollection_t2023_il2cpp_TypeInfo;
extern TypeInfo X509ExtensionEnumerator_t2042_il2cpp_TypeInfo;
extern TypeInfo X509FindType_t2043_il2cpp_TypeInfo;
extern TypeInfo X509KeyUsageExtension_t2044_il2cpp_TypeInfo;
extern TypeInfo X509KeyUsageFlags_t2045_il2cpp_TypeInfo;
extern TypeInfo X509NameType_t2046_il2cpp_TypeInfo;
extern TypeInfo X509RevocationFlag_t2047_il2cpp_TypeInfo;
extern TypeInfo X509RevocationMode_t2048_il2cpp_TypeInfo;
extern TypeInfo X509Store_t2036_il2cpp_TypeInfo;
extern TypeInfo X509SubjectKeyIdentifierExtension_t2050_il2cpp_TypeInfo;
extern TypeInfo X509SubjectKeyIdentifierHashAlgorithm_t2051_il2cpp_TypeInfo;
extern TypeInfo X509VerificationFlags_t2052_il2cpp_TypeInfo;
extern TypeInfo AsnDecodeStatus_t2053_il2cpp_TypeInfo;
extern TypeInfo AsnEncodedData_t2014_il2cpp_TypeInfo;
extern TypeInfo Oid_t2015_il2cpp_TypeInfo;
extern TypeInfo OidCollection_t2039_il2cpp_TypeInfo;
extern TypeInfo OidEnumerator_t2054_il2cpp_TypeInfo;
extern TypeInfo BaseMachine_t2055_il2cpp_TypeInfo;
extern TypeInfo Capture_t2056_il2cpp_TypeInfo;
extern TypeInfo CaptureCollection_t2058_il2cpp_TypeInfo;
extern TypeInfo Group_t2059_il2cpp_TypeInfo;
extern TypeInfo GroupCollection_t2061_il2cpp_TypeInfo;
extern TypeInfo Match_t2063_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t2065_il2cpp_TypeInfo;
extern TypeInfo MatchCollection_t2064_il2cpp_TypeInfo;
extern TypeInfo Regex_t200_il2cpp_TypeInfo;
extern TypeInfo RegexOptions_t2068_il2cpp_TypeInfo;
extern TypeInfo OpCode_t2069_il2cpp_TypeInfo;
extern TypeInfo OpFlags_t2070_il2cpp_TypeInfo;
extern TypeInfo Position_t2071_il2cpp_TypeInfo;
extern TypeInfo IMachine_t2062_il2cpp_TypeInfo;
extern TypeInfo IMachineFactory_t2067_il2cpp_TypeInfo;
extern TypeInfo Key_t2072_il2cpp_TypeInfo;
extern TypeInfo FactoryCache_t2066_il2cpp_TypeInfo;
extern TypeInfo Node_t2074_il2cpp_TypeInfo;
extern TypeInfo MRUList_t2073_il2cpp_TypeInfo;
extern TypeInfo Category_t2075_il2cpp_TypeInfo;
extern TypeInfo CategoryUtils_t2076_il2cpp_TypeInfo;
extern TypeInfo LinkRef_t2077_il2cpp_TypeInfo;
extern TypeInfo ICompiler_t2137_il2cpp_TypeInfo;
extern TypeInfo InterpreterFactory_t2078_il2cpp_TypeInfo;
extern TypeInfo Link_t2079_il2cpp_TypeInfo;
extern TypeInfo PatternLinkStack_t2080_il2cpp_TypeInfo;
extern TypeInfo PatternCompiler_t2082_il2cpp_TypeInfo;
extern TypeInfo LinkStack_t2081_il2cpp_TypeInfo;
extern TypeInfo Mark_t2083_il2cpp_TypeInfo;
extern TypeInfo IntStack_t2084_il2cpp_TypeInfo;
extern TypeInfo RepeatContext_t2085_il2cpp_TypeInfo;
extern TypeInfo Mode_t2086_il2cpp_TypeInfo;
extern TypeInfo Interpreter_t2089_il2cpp_TypeInfo;
extern TypeInfo Interval_t2090_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t2091_il2cpp_TypeInfo;
extern TypeInfo CostDelegate_t2092_il2cpp_TypeInfo;
extern TypeInfo IntervalCollection_t2093_il2cpp_TypeInfo;
extern TypeInfo Parser_t2094_il2cpp_TypeInfo;
extern TypeInfo QuickSearch_t2087_il2cpp_TypeInfo;
extern TypeInfo ExpressionCollection_t2095_il2cpp_TypeInfo;
extern TypeInfo Expression_t2096_il2cpp_TypeInfo;
extern TypeInfo CompositeExpression_t2097_il2cpp_TypeInfo;
extern TypeInfo Group_t2098_il2cpp_TypeInfo;
extern TypeInfo RegularExpression_t2099_il2cpp_TypeInfo;
extern TypeInfo CapturingGroup_t2100_il2cpp_TypeInfo;
extern TypeInfo BalancingGroup_t2101_il2cpp_TypeInfo;
extern TypeInfo NonBacktrackingGroup_t2102_il2cpp_TypeInfo;
extern TypeInfo Repetition_t2103_il2cpp_TypeInfo;
extern TypeInfo Assertion_t2104_il2cpp_TypeInfo;
extern TypeInfo CaptureAssertion_t2107_il2cpp_TypeInfo;
extern TypeInfo ExpressionAssertion_t2105_il2cpp_TypeInfo;
extern TypeInfo Alternation_t2108_il2cpp_TypeInfo;
extern TypeInfo Literal_t2106_il2cpp_TypeInfo;
extern TypeInfo PositionAssertion_t2109_il2cpp_TypeInfo;
extern TypeInfo Reference_t2110_il2cpp_TypeInfo;
extern TypeInfo BackslashNumber_t2111_il2cpp_TypeInfo;
extern TypeInfo CharacterClass_t2113_il2cpp_TypeInfo;
extern TypeInfo AnchorInfo_t2114_il2cpp_TypeInfo;
extern TypeInfo DefaultUriParser_t2115_il2cpp_TypeInfo;
extern TypeInfo GenericUriParser_t2117_il2cpp_TypeInfo;
extern TypeInfo UriScheme_t2118_il2cpp_TypeInfo;
extern TypeInfo Uri_t1986_il2cpp_TypeInfo;
extern TypeInfo UriFormatException_t2120_il2cpp_TypeInfo;
extern TypeInfo UriHostNameType_t2121_il2cpp_TypeInfo;
extern TypeInfo UriKind_t2122_il2cpp_TypeInfo;
extern TypeInfo UriParser_t2116_il2cpp_TypeInfo;
extern TypeInfo UriPartial_t2123_il2cpp_TypeInfo;
extern TypeInfo UriTypeConverter_t2124_il2cpp_TypeInfo;
extern TypeInfo RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU24128_t2125_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU2412_t2126_il2cpp_TypeInfo;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo;
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_System_Assembly_Types[157] = 
{
	&U3CModuleU3E_t1961_il2cpp_TypeInfo,
	&Locale_t1962_il2cpp_TypeInfo,
	&MonoTODOAttribute_t1963_il2cpp_TypeInfo,
	&Enumerator_t2165_il2cpp_TypeInfo,
	&Stack_1_t2164_il2cpp_TypeInfo,
	&HybridDictionary_t1965_il2cpp_TypeInfo,
	&DictionaryNode_t1966_il2cpp_TypeInfo,
	&DictionaryNodeEnumerator_t1967_il2cpp_TypeInfo,
	&DictionaryNodeCollectionEnumerator_t1969_il2cpp_TypeInfo,
	&DictionaryNodeCollection_t1970_il2cpp_TypeInfo,
	&ListDictionary_t1964_il2cpp_TypeInfo,
	&_Item_t1972_il2cpp_TypeInfo,
	&_KeysEnumerator_t1974_il2cpp_TypeInfo,
	&KeysCollection_t1975_il2cpp_TypeInfo,
	&NameObjectCollectionBase_t1973_il2cpp_TypeInfo,
	&NameValueCollection_t1978_il2cpp_TypeInfo,
	&EditorBrowsableAttribute_t1703_il2cpp_TypeInfo,
	&EditorBrowsableState_t1979_il2cpp_TypeInfo,
	&TypeConverter_t1980_il2cpp_TypeInfo,
	&TypeConverterAttribute_t1981_il2cpp_TypeInfo,
	&Stopwatch_t44_il2cpp_TypeInfo,
	&AuthenticationLevel_t1982_il2cpp_TypeInfo,
	&SslPolicyErrors_t1983_il2cpp_TypeInfo,
	&AddressFamily_t1984_il2cpp_TypeInfo,
	&DefaultCertificatePolicy_t1985_il2cpp_TypeInfo,
	&FileWebRequest_t1989_il2cpp_TypeInfo,
	&FileWebRequestCreator_t1991_il2cpp_TypeInfo,
	&FtpRequestCreator_t1992_il2cpp_TypeInfo,
	&FtpWebRequest_t1994_il2cpp_TypeInfo,
	&GlobalProxySelection_t1995_il2cpp_TypeInfo,
	&HttpRequestCreator_t1996_il2cpp_TypeInfo,
	&HttpVersion_t1998_il2cpp_TypeInfo,
	&HttpWebRequest_t2001_il2cpp_TypeInfo,
	&ICertificatePolicy_t2006_il2cpp_TypeInfo,
	&ICredentials_t2009_il2cpp_TypeInfo,
	&IPAddress_t2002_il2cpp_TypeInfo,
	&IPv6Address_t2003_il2cpp_TypeInfo,
	&IWebProxy_t1988_il2cpp_TypeInfo,
	&IWebRequestCreate_t2166_il2cpp_TypeInfo,
	&SecurityProtocolType_t2004_il2cpp_TypeInfo,
	&ServicePoint_t2000_il2cpp_TypeInfo,
	&SPKey_t2005_il2cpp_TypeInfo,
	&ServicePointManager_t2007_il2cpp_TypeInfo,
	&WebHeaderCollection_t1987_il2cpp_TypeInfo,
	&WebProxy_t2010_il2cpp_TypeInfo,
	&WebRequest_t1990_il2cpp_TypeInfo,
	&OpenFlags_t2012_il2cpp_TypeInfo,
	&PublicKey_t2016_il2cpp_TypeInfo,
	&StoreLocation_t2017_il2cpp_TypeInfo,
	&StoreName_t2018_il2cpp_TypeInfo,
	&X500DistinguishedName_t2019_il2cpp_TypeInfo,
	&X500DistinguishedNameFlags_t2020_il2cpp_TypeInfo,
	&X509BasicConstraintsExtension_t2021_il2cpp_TypeInfo,
	&X509Certificate2_t2025_il2cpp_TypeInfo,
	&X509Certificate2Collection_t2027_il2cpp_TypeInfo,
	&X509Certificate2Enumerator_t2028_il2cpp_TypeInfo,
	&X509CertificateEnumerator_t2029_il2cpp_TypeInfo,
	&X509CertificateCollection_t1999_il2cpp_TypeInfo,
	&X509Chain_t2037_il2cpp_TypeInfo,
	&X509ChainElement_t2035_il2cpp_TypeInfo,
	&X509ChainElementCollection_t2031_il2cpp_TypeInfo,
	&X509ChainElementEnumerator_t2038_il2cpp_TypeInfo,
	&X509ChainPolicy_t2032_il2cpp_TypeInfo,
	&X509ChainStatus_t2034_il2cpp_TypeInfo,
	&X509ChainStatusFlags_t2040_il2cpp_TypeInfo,
	&X509EnhancedKeyUsageExtension_t2041_il2cpp_TypeInfo,
	&X509Extension_t2022_il2cpp_TypeInfo,
	&X509ExtensionCollection_t2023_il2cpp_TypeInfo,
	&X509ExtensionEnumerator_t2042_il2cpp_TypeInfo,
	&X509FindType_t2043_il2cpp_TypeInfo,
	&X509KeyUsageExtension_t2044_il2cpp_TypeInfo,
	&X509KeyUsageFlags_t2045_il2cpp_TypeInfo,
	&X509NameType_t2046_il2cpp_TypeInfo,
	&X509RevocationFlag_t2047_il2cpp_TypeInfo,
	&X509RevocationMode_t2048_il2cpp_TypeInfo,
	&X509Store_t2036_il2cpp_TypeInfo,
	&X509SubjectKeyIdentifierExtension_t2050_il2cpp_TypeInfo,
	&X509SubjectKeyIdentifierHashAlgorithm_t2051_il2cpp_TypeInfo,
	&X509VerificationFlags_t2052_il2cpp_TypeInfo,
	&AsnDecodeStatus_t2053_il2cpp_TypeInfo,
	&AsnEncodedData_t2014_il2cpp_TypeInfo,
	&Oid_t2015_il2cpp_TypeInfo,
	&OidCollection_t2039_il2cpp_TypeInfo,
	&OidEnumerator_t2054_il2cpp_TypeInfo,
	&BaseMachine_t2055_il2cpp_TypeInfo,
	&Capture_t2056_il2cpp_TypeInfo,
	&CaptureCollection_t2058_il2cpp_TypeInfo,
	&Group_t2059_il2cpp_TypeInfo,
	&GroupCollection_t2061_il2cpp_TypeInfo,
	&Match_t2063_il2cpp_TypeInfo,
	&Enumerator_t2065_il2cpp_TypeInfo,
	&MatchCollection_t2064_il2cpp_TypeInfo,
	&Regex_t200_il2cpp_TypeInfo,
	&RegexOptions_t2068_il2cpp_TypeInfo,
	&OpCode_t2069_il2cpp_TypeInfo,
	&OpFlags_t2070_il2cpp_TypeInfo,
	&Position_t2071_il2cpp_TypeInfo,
	&IMachine_t2062_il2cpp_TypeInfo,
	&IMachineFactory_t2067_il2cpp_TypeInfo,
	&Key_t2072_il2cpp_TypeInfo,
	&FactoryCache_t2066_il2cpp_TypeInfo,
	&Node_t2074_il2cpp_TypeInfo,
	&MRUList_t2073_il2cpp_TypeInfo,
	&Category_t2075_il2cpp_TypeInfo,
	&CategoryUtils_t2076_il2cpp_TypeInfo,
	&LinkRef_t2077_il2cpp_TypeInfo,
	&ICompiler_t2137_il2cpp_TypeInfo,
	&InterpreterFactory_t2078_il2cpp_TypeInfo,
	&Link_t2079_il2cpp_TypeInfo,
	&PatternLinkStack_t2080_il2cpp_TypeInfo,
	&PatternCompiler_t2082_il2cpp_TypeInfo,
	&LinkStack_t2081_il2cpp_TypeInfo,
	&Mark_t2083_il2cpp_TypeInfo,
	&IntStack_t2084_il2cpp_TypeInfo,
	&RepeatContext_t2085_il2cpp_TypeInfo,
	&Mode_t2086_il2cpp_TypeInfo,
	&Interpreter_t2089_il2cpp_TypeInfo,
	&Interval_t2090_il2cpp_TypeInfo,
	&Enumerator_t2091_il2cpp_TypeInfo,
	&CostDelegate_t2092_il2cpp_TypeInfo,
	&IntervalCollection_t2093_il2cpp_TypeInfo,
	&Parser_t2094_il2cpp_TypeInfo,
	&QuickSearch_t2087_il2cpp_TypeInfo,
	&ExpressionCollection_t2095_il2cpp_TypeInfo,
	&Expression_t2096_il2cpp_TypeInfo,
	&CompositeExpression_t2097_il2cpp_TypeInfo,
	&Group_t2098_il2cpp_TypeInfo,
	&RegularExpression_t2099_il2cpp_TypeInfo,
	&CapturingGroup_t2100_il2cpp_TypeInfo,
	&BalancingGroup_t2101_il2cpp_TypeInfo,
	&NonBacktrackingGroup_t2102_il2cpp_TypeInfo,
	&Repetition_t2103_il2cpp_TypeInfo,
	&Assertion_t2104_il2cpp_TypeInfo,
	&CaptureAssertion_t2107_il2cpp_TypeInfo,
	&ExpressionAssertion_t2105_il2cpp_TypeInfo,
	&Alternation_t2108_il2cpp_TypeInfo,
	&Literal_t2106_il2cpp_TypeInfo,
	&PositionAssertion_t2109_il2cpp_TypeInfo,
	&Reference_t2110_il2cpp_TypeInfo,
	&BackslashNumber_t2111_il2cpp_TypeInfo,
	&CharacterClass_t2113_il2cpp_TypeInfo,
	&AnchorInfo_t2114_il2cpp_TypeInfo,
	&DefaultUriParser_t2115_il2cpp_TypeInfo,
	&GenericUriParser_t2117_il2cpp_TypeInfo,
	&UriScheme_t2118_il2cpp_TypeInfo,
	&Uri_t1986_il2cpp_TypeInfo,
	&UriFormatException_t2120_il2cpp_TypeInfo,
	&UriHostNameType_t2121_il2cpp_TypeInfo,
	&UriKind_t2122_il2cpp_TypeInfo,
	&UriParser_t2116_il2cpp_TypeInfo,
	&UriPartial_t2123_il2cpp_TypeInfo,
	&UriTypeConverter_t2124_il2cpp_TypeInfo,
	&RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo,
	&U24ArrayTypeU24128_t2125_il2cpp_TypeInfo,
	&U24ArrayTypeU2412_t2126_il2cpp_TypeInfo,
	&U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_System_dll_Image;
Il2CppAssembly g_System_Assembly = 
{
	{ "System", 0, 0, "\x0\x24\x0\x0\x4\x80\x0\x0\x94\x0\x0\x0\x6\x2\x0\x0\x0\x24\x0\x0\x52\x53\x41\x31\x0\x4\x0\x0\x1\x0\x1\x0\x8D\x56\xC7\x6F\x9E\x86\x49\x38\x30\x49\xF3\x83\xC4\x4B\xE0\xEC\x20\x41\x81\x82\x2A\x6C\x31\xCF\x5E\xB7\xEF\x48\x69\x44\xD0\x32\x18\x8E\xA1\xD3\x92\x7\x63\x71\x2C\xCB\x12\xD7\x5F\xB7\x7E\x98\x11\x14\x9E\x61\x48\xE5\xD3\x2F\xBA\xAB\x37\x61\x1C\x18\x78\xDD\xC1\x9E\x20\xEF\x13\x5D\xC\xB2\xCF\xF2\xBF\xEC\x3D\x11\x58\x10\xC3\xD9\x6\x96\x38\xFE\x4B\xE2\x15\xDB\xF7\x95\x86\x19\x20\xE5\xAB\x6F\x7D\xB2\xE2\xCE\xEF\x13\x6A\xC2\x3D\x5D\xD2\xBF\x3\x17\x0\xAE\xC2\x32\xF6\xC6\xB1\xC7\x85\xB4\x30\x5C\x12\x3B\x37\xAB", { 0x7C, 0xEC, 0x85, 0xD7, 0xBE, 0xA7, 0x79, 0x8E }, 32772, 0, 1, 2, 0, 5, 0 },
	&g_System_dll_Image,
	1,
};
extern const CustomAttributesCacheGenerator g_System_Assembly_AttributeGenerators[67];
Il2CppImage g_System_dll_Image = 
{
	 "System.dll" ,
	&g_System_Assembly,
	g_System_Assembly_Types,
	156,
	NULL,
	67,
	NULL,
	g_System_Assembly_AttributeGenerators,
};
