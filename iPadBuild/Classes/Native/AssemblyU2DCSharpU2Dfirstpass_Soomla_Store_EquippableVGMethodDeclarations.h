﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// Soomla.Store.EquippableVG/EquippingModel
struct EquippingModel_t128;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.EquippableVG::.ctor(Soomla.Store.EquippableVG/EquippingModel,System.String,System.String,System.String,Soomla.Store.PurchaseType)
extern "C" void EquippableVG__ctor_m623 (EquippableVG_t129 * __this, EquippingModel_t128 * ___equippingModel, String_t* ___name, String_t* ___description, String_t* ___itemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EquippableVG::.ctor(JSONObject)
extern "C" void EquippableVG__ctor_m624 (EquippableVG_t129 * __this, JSONObject_t30 * ___jsonItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EquippableVG::.cctor()
extern "C" void EquippableVG__cctor_m625 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.EquippableVG::toJSONObject()
extern "C" JSONObject_t30 * EquippableVG_toJSONObject_m626 (EquippableVG_t129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EquippableVG::Equip()
extern "C" void EquippableVG_Equip_m627 (EquippableVG_t129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EquippableVG::Equip(System.Boolean)
extern "C" void EquippableVG_Equip_m628 (EquippableVG_t129 * __this, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EquippableVG::Unequip()
extern "C" void EquippableVG_Unequip_m629 (EquippableVG_t129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EquippableVG::Unequip(System.Boolean)
extern "C" void EquippableVG_Unequip_m630 (EquippableVG_t129 * __this, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
