﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualCurrencyPack
struct VirtualCurrencyPack_t78;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.VirtualCurrencyPack::.ctor(System.String,System.String,System.String,System.Int32,System.String,Soomla.Store.PurchaseType)
extern "C" void VirtualCurrencyPack__ctor_m610 (VirtualCurrencyPack_t78 * __this, String_t* ___name, String_t* ___description, String_t* ___itemId, int32_t ___currencyAmount, String_t* ___currencyItemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualCurrencyPack::.ctor(JSONObject)
extern "C" void VirtualCurrencyPack__ctor_m611 (VirtualCurrencyPack_t78 * __this, JSONObject_t30 * ___jsonItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualCurrencyPack::.cctor()
extern "C" void VirtualCurrencyPack__cctor_m612 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.VirtualCurrencyPack::toJSONObject()
extern "C" JSONObject_t30 * VirtualCurrencyPack_toJSONObject_m613 (VirtualCurrencyPack_t78 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyPack::Give(System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyPack_Give_m614 (VirtualCurrencyPack_t78 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyPack::Take(System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyPack_Take_m615 (VirtualCurrencyPack_t78 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyPack::ResetBalance(System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyPack_ResetBalance_m616 (VirtualCurrencyPack_t78 * __this, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyPack::GetBalance()
extern "C" int32_t VirtualCurrencyPack_GetBalance_m617 (VirtualCurrencyPack_t78 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.VirtualCurrencyPack::canBuy()
extern "C" bool VirtualCurrencyPack_canBuy_m618 (VirtualCurrencyPack_t78 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
