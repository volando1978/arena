﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t1552;
struct CacheIndex_t1552_marshaled;

void CacheIndex_t1552_marshal(const CacheIndex_t1552& unmarshaled, CacheIndex_t1552_marshaled& marshaled);
void CacheIndex_t1552_marshal_back(const CacheIndex_t1552_marshaled& marshaled, CacheIndex_t1552& unmarshaled);
void CacheIndex_t1552_marshal_cleanup(CacheIndex_t1552_marshaled& marshaled);
