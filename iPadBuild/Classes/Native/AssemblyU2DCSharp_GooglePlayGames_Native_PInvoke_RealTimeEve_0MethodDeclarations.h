﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t594;
// System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>
struct Action_1_t693;
// System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Action_2_t881;
// System.String
struct String_t;
// System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>
struct Action_4_t882;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::.ctor(System.IntPtr)
extern "C" void RealTimeEventListenerHelper__ctor_m2936 (RealTimeEventListenerHelper_t594 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RealTimeEventListenerHelper_CallDispose_m2937 (RealTimeEventListenerHelper_t594 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::SetOnRoomStatusChangedCallback(System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>)
extern "C" RealTimeEventListenerHelper_t594 * RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback_m2938 (RealTimeEventListenerHelper_t594 * __this, Action_1_t693 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::InternalOnRoomStatusChangedCallback(System.IntPtr,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_InternalOnRoomStatusChangedCallback_m2939 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::SetOnRoomConnectedSetChangedCallback(System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>)
extern "C" RealTimeEventListenerHelper_t594 * RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback_m2940 (RealTimeEventListenerHelper_t594 * __this, Action_1_t693 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::InternalOnRoomConnectedSetChangedCallback(System.IntPtr,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_InternalOnRoomConnectedSetChangedCallback_m2941 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::SetOnP2PConnectedCallback(System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>)
extern "C" RealTimeEventListenerHelper_t594 * RealTimeEventListenerHelper_SetOnP2PConnectedCallback_m2942 (RealTimeEventListenerHelper_t594 * __this, Action_2_t881 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::InternalOnP2PConnectedCallback(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_InternalOnP2PConnectedCallback_m2943 (Object_t * __this /* static, unused */, IntPtr_t ___room, IntPtr_t ___participant, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::SetOnP2PDisconnectedCallback(System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>)
extern "C" RealTimeEventListenerHelper_t594 * RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback_m2944 (RealTimeEventListenerHelper_t594 * __this, Action_2_t881 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::InternalOnP2PDisconnectedCallback(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_InternalOnP2PDisconnectedCallback_m2945 (Object_t * __this /* static, unused */, IntPtr_t ___room, IntPtr_t ___participant, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::SetOnParticipantStatusChangedCallback(System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>)
extern "C" RealTimeEventListenerHelper_t594 * RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback_m2946 (RealTimeEventListenerHelper_t594 * __this, Action_2_t881 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::InternalOnParticipantStatusChangedCallback(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_InternalOnParticipantStatusChangedCallback_m2947 (Object_t * __this /* static, unused */, IntPtr_t ___room, IntPtr_t ___participant, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::PerformRoomAndParticipantCallback(System.String,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_PerformRoomAndParticipantCallback_m2948 (Object_t * __this /* static, unused */, String_t* ___callbackName, IntPtr_t ___room, IntPtr_t ___participant, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::SetOnDataReceivedCallback(System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>)
extern "C" RealTimeEventListenerHelper_t594 * RealTimeEventListenerHelper_SetOnDataReceivedCallback_m2949 (RealTimeEventListenerHelper_t594 * __this, Action_4_t882 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::InternalOnDataReceived(System.IntPtr,System.IntPtr,System.IntPtr,System.UIntPtr,System.Boolean,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_InternalOnDataReceived_m2950 (Object_t * __this /* static, unused */, IntPtr_t ___room, IntPtr_t ___participant, IntPtr_t ___data, UIntPtr_t  ___dataLength, bool ___isReliable, IntPtr_t ___userData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::ToCallbackPointer(System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>)
extern "C" IntPtr_t RealTimeEventListenerHelper_ToCallbackPointer_m2951 (Object_t * __this /* static, unused */, Action_1_t693 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper::Create()
extern "C" RealTimeEventListenerHelper_t594 * RealTimeEventListenerHelper_Create_m2952 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
