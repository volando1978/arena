﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.EventArgs
struct EventArgs_t2218;

// System.Void System.EventArgs::.ctor()
extern "C" void EventArgs__ctor_m14005 (EventArgs_t2218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventArgs::.cctor()
extern "C" void EventArgs__cctor_m14006 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
