﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.GameServices
struct GameServices_t423;
// GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback
struct FlushCallback_t422;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_Flush(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback,System.IntPtr)
extern "C" void GameServices_GameServices_Flush_m1720 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, FlushCallback_t422 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.GameServices::GameServices_IsAuthorized(System.Runtime.InteropServices.HandleRef)
extern "C" bool GameServices_GameServices_IsAuthorized_m1721 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void GameServices_GameServices_Dispose_m1722 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_SignOut(System.Runtime.InteropServices.HandleRef)
extern "C" void GameServices_GameServices_SignOut_m1723 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_StartAuthorizationUI(System.Runtime.InteropServices.HandleRef)
extern "C" void GameServices_GameServices_StartAuthorizationUI_m1724 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
