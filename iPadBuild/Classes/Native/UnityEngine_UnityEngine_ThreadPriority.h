﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.ThreadPriority
#include "UnityEngine_UnityEngine_ThreadPriority.h"
// UnityEngine.ThreadPriority
struct  ThreadPriority_t1495 
{
	// System.Int32 UnityEngine.ThreadPriority::value__
	int32_t ___value___1;
};
