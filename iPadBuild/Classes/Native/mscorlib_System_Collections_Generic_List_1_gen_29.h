﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEventSystemHandler[]
struct IEventSystemHandlerU5BU5D_t3870;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct  List_1_t1358  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>::_items
	IEventSystemHandlerU5BU5D_t3870* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>::_version
	int32_t ____version_3;
};
struct List_1_t1358_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>::EmptyArray
	IEventSystemHandlerU5BU5D_t3870* ___EmptyArray_4;
};
