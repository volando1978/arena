﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_t355;
struct ConnectionRequest_t355_marshaled;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Nearby.EndpointDetails
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_EndpointDe.h"

// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionRequest::.ctor(System.String,System.String,System.String,System.String,System.Byte[])
extern "C" void ConnectionRequest__ctor_m1424 (ConnectionRequest_t355 * __this, String_t* ___remoteEndpointId, String_t* ___remoteDeviceId, String_t* ___remoteEndpointName, String_t* ___serviceId, ByteU5BU5D_t350* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_RemoteEndpoint()
extern "C" EndpointDetails_t356  ConnectionRequest_get_RemoteEndpoint_m1425 (ConnectionRequest_t355 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_Payload()
extern "C" ByteU5BU5D_t350* ConnectionRequest_get_Payload_m1426 (ConnectionRequest_t355 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void ConnectionRequest_t355_marshal(const ConnectionRequest_t355& unmarshaled, ConnectionRequest_t355_marshaled& marshaled);
void ConnectionRequest_t355_marshal_back(const ConnectionRequest_t355_marshaled& marshaled, ConnectionRequest_t355& unmarshaled);
void ConnectionRequest_t355_marshal_cleanup(ConnectionRequest_t355_marshaled& marshaled);
