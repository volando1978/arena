﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback
struct FetchInvitationsCallback_t464;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchInvitationsCallback__ctor_m1979 (FetchInvitationsCallback_t464 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchInvitationsCallback_Invoke_m1980 (FetchInvitationsCallback_t464 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchInvitationsCallback_t464(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchInvitationsCallback_BeginInvoke_m1981 (FetchInvitationsCallback_t464 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchInvitationsCallback_EndInvoke_m1982 (FetchInvitationsCallback_t464 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
