﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$128
struct U24ArrayTypeU24128_t2125;
struct U24ArrayTypeU24128_t2125_marshaled;

void U24ArrayTypeU24128_t2125_marshal(const U24ArrayTypeU24128_t2125& unmarshaled, U24ArrayTypeU24128_t2125_marshaled& marshaled);
void U24ArrayTypeU24128_t2125_marshal_back(const U24ArrayTypeU24128_t2125_marshaled& marshaled, U24ArrayTypeU24128_t2125& unmarshaled);
void U24ArrayTypeU24128_t2125_marshal_cleanup(U24ArrayTypeU24128_t2125_marshaled& marshaled);
