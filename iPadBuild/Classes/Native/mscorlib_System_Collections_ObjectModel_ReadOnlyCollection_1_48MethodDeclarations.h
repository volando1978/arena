﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t4100;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t1401;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1669;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t4514;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m24798_gshared (ReadOnlyCollection_1_t4100 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m24798(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t4100 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m24798_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24799_gshared (ReadOnlyCollection_1_t4100 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24799(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t4100 *, UICharInfo_t1400 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24799_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24800_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24800(__this, method) (( void (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24800_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24801_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, UICharInfo_t1400  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24801(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t4100 *, int32_t, UICharInfo_t1400 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24801_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24802_gshared (ReadOnlyCollection_1_t4100 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24802(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t4100 *, UICharInfo_t1400 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24802_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24803_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24803(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t4100 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24803_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UICharInfo_t1400  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24804_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24804(__this, ___index, method) (( UICharInfo_t1400  (*) (ReadOnlyCollection_1_t4100 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24804_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24805_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, UICharInfo_t1400  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24805(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t4100 *, int32_t, UICharInfo_t1400 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24805_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24806_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24806(__this, method) (( bool (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24806_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24807_gshared (ReadOnlyCollection_1_t4100 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24807(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t4100 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24807_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24808_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24808(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24808_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m24809_gshared (ReadOnlyCollection_1_t4100 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m24809(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t4100 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m24809_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m24810_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m24810(__this, method) (( void (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m24810_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m24811_gshared (ReadOnlyCollection_1_t4100 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m24811(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t4100 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m24811_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24812_gshared (ReadOnlyCollection_1_t4100 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24812(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t4100 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24812_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m24813_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m24813(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t4100 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m24813_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m24814_gshared (ReadOnlyCollection_1_t4100 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m24814(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t4100 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m24814_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24815_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24815(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t4100 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24815_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24816_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24816(__this, method) (( bool (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24816_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24817_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24817(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24817_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24818_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24818(__this, method) (( bool (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24818_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24819_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24819(__this, method) (( bool (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24819_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m24820_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m24820(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t4100 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m24820_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m24821_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m24821(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t4100 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m24821_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m24822_gshared (ReadOnlyCollection_1_t4100 * __this, UICharInfo_t1400  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m24822(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t4100 *, UICharInfo_t1400 , MethodInfo*))ReadOnlyCollection_1_Contains_m24822_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m24823_gshared (ReadOnlyCollection_1_t4100 * __this, UICharInfoU5BU5D_t1669* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m24823(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t4100 *, UICharInfoU5BU5D_t1669*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m24823_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m24824_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m24824(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m24824_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m24825_gshared (ReadOnlyCollection_1_t4100 * __this, UICharInfo_t1400  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m24825(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t4100 *, UICharInfo_t1400 , MethodInfo*))ReadOnlyCollection_1_IndexOf_m24825_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m24826_gshared (ReadOnlyCollection_1_t4100 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m24826(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t4100 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m24826_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t1400  ReadOnlyCollection_1_get_Item_m24827_gshared (ReadOnlyCollection_1_t4100 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m24827(__this, ___index, method) (( UICharInfo_t1400  (*) (ReadOnlyCollection_1_t4100 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m24827_gshared)(__this, ___index, method)
