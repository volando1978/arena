﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t536;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// GooglePlayGames.Native.PInvoke.RealtimeManager
struct RealtimeManager_t564;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener
struct RealTimeMultiplayerListener_t573;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t594;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::.ctor(GooglePlayGames.Native.NativeClient,GooglePlayGames.Native.PInvoke.RealtimeManager)
extern "C" void NativeRealtimeMultiplayerClient__ctor_m2477 (NativeRealtimeMultiplayerClient_t536 * __this, NativeClient_t524 * ___nativeClient, RealtimeManager_t564 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetTerminatedSession()
extern "C" RoomSession_t566 * NativeRealtimeMultiplayerClient_GetTerminatedSession_m2478 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::CreateQuickGame(System.UInt32,System.UInt32,System.UInt32,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void NativeRealtimeMultiplayerClient_CreateQuickGame_m2479 (NativeRealtimeMultiplayerClient_t536 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::CreateQuickGame(System.UInt32,System.UInt32,System.UInt32,System.UInt64,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void NativeRealtimeMultiplayerClient_CreateQuickGame_m2480 (NativeRealtimeMultiplayerClient_t536 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, uint64_t ___exclusiveBitMask, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient::HelperForSession(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" RealTimeEventListenerHelper_t594 * NativeRealtimeMultiplayerClient_HelperForSession_m2481 (Object_t * __this /* static, unused */, RoomSession_t566 * ___session, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::CreateWithInvitationScreen(System.UInt32,System.UInt32,System.UInt32,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void NativeRealtimeMultiplayerClient_CreateWithInvitationScreen_m2482 (NativeRealtimeMultiplayerClient_t536 * __this, uint32_t ___minOpponents, uint32_t ___maxOppponents, uint32_t ___variant, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::ShowWaitingRoomUI()
extern "C" void NativeRealtimeMultiplayerClient_ShowWaitingRoomUI_m2483 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::AcceptFromInbox(GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void NativeRealtimeMultiplayerClient_AcceptFromInbox_m2484 (NativeRealtimeMultiplayerClient_t536 * __this, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::AcceptInvitation(System.String,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void NativeRealtimeMultiplayerClient_AcceptInvitation_m2485 (NativeRealtimeMultiplayerClient_t536 * __this, String_t* ___invitationId, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::LeaveRoom()
extern "C" void NativeRealtimeMultiplayerClient_LeaveRoom_m2486 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessageToAll(System.Boolean,System.Byte[])
extern "C" void NativeRealtimeMultiplayerClient_SendMessageToAll_m2487 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessageToAll(System.Boolean,System.Byte[],System.Int32,System.Int32)
extern "C" void NativeRealtimeMultiplayerClient_SendMessageToAll_m2488 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessage(System.Boolean,System.String,System.Byte[])
extern "C" void NativeRealtimeMultiplayerClient_SendMessage_m2489 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::SendMessage(System.Boolean,System.String,System.Byte[],System.Int32,System.Int32)
extern "C" void NativeRealtimeMultiplayerClient_SendMessage_m2490 (NativeRealtimeMultiplayerClient_t536 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetConnectedParticipants()
extern "C" List_1_t351 * NativeRealtimeMultiplayerClient_GetConnectedParticipants_m2491 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetSelf()
extern "C" Participant_t340 * NativeRealtimeMultiplayerClient_GetSelf_m2492 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient::GetParticipant(System.String)
extern "C" Participant_t340 * NativeRealtimeMultiplayerClient_GetParticipant_m2493 (NativeRealtimeMultiplayerClient_t536 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient::IsRoomConnected()
extern "C" bool NativeRealtimeMultiplayerClient_IsRoomConnected_m2494 (NativeRealtimeMultiplayerClient_t536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient::DeclineInvitation(System.String)
extern "C" void NativeRealtimeMultiplayerClient_DeclineInvitation_m2495 (NativeRealtimeMultiplayerClient_t536 * __this, String_t* ___invitationId, MethodInfo* method) IL2CPP_METHOD_ATTR;
