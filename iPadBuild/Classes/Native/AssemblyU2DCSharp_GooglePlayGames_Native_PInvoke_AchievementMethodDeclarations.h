﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
struct FetchResponse_t653;
// GooglePlayGames.Native.NativeAchievement
struct NativeAchievement_t673;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::.ctor(System.IntPtr)
extern "C" void FetchResponse__ctor_m2601 (FetchResponse_t653 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::Status()
extern "C" int32_t FetchResponse_Status_m2602 (FetchResponse_t653 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::Achievement()
extern "C" NativeAchievement_t673 * FetchResponse_Achievement_m2603 (FetchResponse_t653 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchResponse_CallDispose_m2604 (FetchResponse_t653 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::FromPointer(System.IntPtr)
extern "C" FetchResponse_t653 * FetchResponse_FromPointer_m2605 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
