﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.VirtualItemReward
struct VirtualItemReward_t141;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.VirtualItemReward::.ctor(System.String,System.String,System.String,System.Int32)
extern "C" void VirtualItemReward__ctor_m683 (VirtualItemReward_t141 * __this, String_t* ___rewardId, String_t* ___name, String_t* ___associatedItemId, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.VirtualItemReward::.ctor(JSONObject)
extern "C" void VirtualItemReward__ctor_m684 (VirtualItemReward_t141 * __this, JSONObject_t30 * ___jsonReward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.VirtualItemReward::.cctor()
extern "C" void VirtualItemReward__cctor_m685 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.VirtualItemReward::toJSONObject()
extern "C" JSONObject_t30 * VirtualItemReward_toJSONObject_m686 (VirtualItemReward_t141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.VirtualItemReward::giveInner()
extern "C" bool VirtualItemReward_giveInner_m687 (VirtualItemReward_t141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.VirtualItemReward::takeInner()
extern "C" bool VirtualItemReward_takeInner_m688 (VirtualItemReward_t141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
