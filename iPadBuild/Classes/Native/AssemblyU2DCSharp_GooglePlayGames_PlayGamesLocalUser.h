﻿#pragma once
#include <stdint.h>
// GooglePlayGames.PlayGamesPlatform
struct PlayGamesPlatform_t382;
// UnityEngine.WWW
struct WWW_t383;
// UnityEngine.Texture2D
struct Texture2D_t384;
// GooglePlayGames.PlayGamesUserProfile
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserProfile.h"
// GooglePlayGames.PlayGamesLocalUser
struct  PlayGamesLocalUser_t385  : public PlayGamesUserProfile_t386
{
	// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesLocalUser::mPlatform
	PlayGamesPlatform_t382 * ___mPlatform_0;
	// UnityEngine.WWW GooglePlayGames.PlayGamesLocalUser::mAvatarUrl
	WWW_t383 * ___mAvatarUrl_1;
	// UnityEngine.Texture2D GooglePlayGames.PlayGamesLocalUser::mImage
	Texture2D_t384 * ___mImage_2;
};
