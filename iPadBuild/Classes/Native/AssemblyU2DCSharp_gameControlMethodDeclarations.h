﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameControl
struct gameControl_t794;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.GameObject
struct GameObject_t144;

// System.Void gameControl::.ctor()
extern "C" void gameControl__ctor_m3449 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::.cctor()
extern "C" void gameControl__cctor_m3450 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// gameControl gameControl::get_Instance()
extern "C" gameControl_t794 * gameControl_get_Instance_m3451 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::Awake()
extern "C" void gameControl_Awake_m3452 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::callScoreTable()
extern "C" void gameControl_callScoreTable_m3453 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::Start()
extern "C" void gameControl_Start_m3454 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::Update()
extern "C" void gameControl_Update_m3455 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::FixedUpdate()
extern "C" void gameControl_FixedUpdate_m3456 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameControl::checkTutorial()
extern "C" bool gameControl_checkTutorial_m3457 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::toGame()
extern "C" void gameControl_toGame_m3458 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameControl::startGame()
extern "C" Object_t * gameControl_startGame_m3459 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::finishGame()
extern "C" void gameControl_finishGame_m3460 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameControl::wait()
extern "C" Object_t * gameControl_wait_m3461 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameControl::waitForTouchDown()
extern "C" Object_t * gameControl_waitForTouchDown_m3462 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameControl::waitTime(System.Single)
extern "C" Object_t * gameControl_waitTime_m3463 (gameControl_t794 * __this, float ___seconds, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameControl::runTutorial()
extern "C" Object_t * gameControl_runTutorial_m3464 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameControl::ChangeLevelWait(System.Single)
extern "C" Object_t * gameControl_ChangeLevelWait_m3465 (gameControl_t794 * __this, float ___seconds, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::levelUpgrade()
extern "C" void gameControl_levelUpgrade_m3466 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::setScores()
extern "C" void gameControl_setScores_m3467 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::setLastKills()
extern "C" void gameControl_setLastKills_m3468 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::clearEnemies()
extern "C" void gameControl_clearEnemies_m3469 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::clearPlayer()
extern "C" void gameControl_clearPlayer_m3470 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::clearPowerUps()
extern "C" void gameControl_clearPowerUps_m3471 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject gameControl::createNaveProxy()
extern "C" GameObject_t144 * gameControl_createNaveProxy_m3472 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::initMenu()
extern "C" void gameControl_initMenu_m3473 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::removeMenu()
extern "C" void gameControl_removeMenu_m3474 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::toWeaponRoom()
extern "C" void gameControl_toWeaponRoom_m3475 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::removeWeaponRoom()
extern "C" void gameControl_removeWeaponRoom_m3476 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::toGameOverRoom()
extern "C" void gameControl_toGameOverRoom_m3477 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::removeGameOverRoom()
extern "C" void gameControl_removeGameOverRoom_m3478 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::toStoreRoom()
extern "C" void gameControl_toStoreRoom_m3479 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::removeStoreRoom()
extern "C" void gameControl_removeStoreRoom_m3480 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::resetTutorial()
extern "C" void gameControl_resetTutorial_m3481 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::finishTutorial()
extern "C" void gameControl_finishTutorial_m3482 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::OnApplicationQuit()
extern "C" void gameControl_OnApplicationQuit_m3483 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::hideAgujeros()
extern "C" void gameControl_hideAgujeros_m3484 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::showAgujeros()
extern "C" void gameControl_showAgujeros_m3485 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::failingTutorial()
extern "C" void gameControl_failingTutorial_m3486 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl::checkRemoveTutorial()
extern "C" void gameControl_checkRemoveTutorial_m3487 (gameControl_t794 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
