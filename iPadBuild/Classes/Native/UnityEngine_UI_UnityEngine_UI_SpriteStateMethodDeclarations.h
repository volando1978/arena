﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.SpriteState
struct SpriteState_t1290;
// UnityEngine.Sprite
struct Sprite_t766;

// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern "C" Sprite_t766 * SpriteState_get_highlightedSprite_m5418 (SpriteState_t1290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_highlightedSprite_m5419 (SpriteState_t1290 * __this, Sprite_t766 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern "C" Sprite_t766 * SpriteState_get_pressedSprite_m5420 (SpriteState_t1290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_pressedSprite_m5421 (SpriteState_t1290 * __this, Sprite_t766 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern "C" Sprite_t766 * SpriteState_get_disabledSprite_m5422 (SpriteState_t1290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_disabledSprite_m5423 (SpriteState_t1290 * __this, Sprite_t766 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
