﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void InvitationReceivedDelegate__ctor_m3672 (InvitationReceivedDelegate_t363 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C" void InvitationReceivedDelegate_Invoke_m3673 (InvitationReceivedDelegate_t363 * __this, Invitation_t341 * ___invitation, bool ___shouldAutoAccept, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_InvitationReceivedDelegate_t363(Il2CppObject* delegate, Invitation_t341 * ___invitation, bool ___shouldAutoAccept);
// System.IAsyncResult GooglePlayGames.BasicApi.InvitationReceivedDelegate::BeginInvoke(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean,System.AsyncCallback,System.Object)
extern "C" Object_t * InvitationReceivedDelegate_BeginInvoke_m3674 (InvitationReceivedDelegate_t363 * __this, Invitation_t341 * ___invitation, bool ___shouldAutoAccept, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void InvitationReceivedDelegate_EndInvoke_m3675 (InvitationReceivedDelegate_t363 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
