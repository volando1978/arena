﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Events.IEvent[]
struct IEventU5BU5D_t3640;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>
struct  List_1_t915  : public Object_t
{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::_items
	IEventU5BU5D_t3640* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::_version
	int32_t ____version_3;
};
struct List_1_t915_StaticFields{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::EmptyArray
	IEventU5BU5D_t3640* ___EmptyArray_4;
};
