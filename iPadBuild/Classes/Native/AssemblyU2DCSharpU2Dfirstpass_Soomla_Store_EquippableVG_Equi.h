﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.EquippableVG/EquippingModel
struct EquippingModel_t128;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.EquippableVG/EquippingModel
struct  EquippingModel_t128  : public Object_t
{
	// System.String Soomla.Store.EquippableVG/EquippingModel::name
	String_t* ___name_0;
	// System.Int32 Soomla.Store.EquippableVG/EquippingModel::value
	int32_t ___value_1;
};
struct EquippingModel_t128_StaticFields{
	// Soomla.Store.EquippableVG/EquippingModel Soomla.Store.EquippableVG/EquippingModel::LOCAL
	EquippingModel_t128 * ___LOCAL_2;
	// Soomla.Store.EquippableVG/EquippingModel Soomla.Store.EquippableVG/EquippingModel::CATEGORY
	EquippingModel_t128 * ___CATEGORY_3;
	// Soomla.Store.EquippableVG/EquippingModel Soomla.Store.EquippableVG/EquippingModel::GLOBAL
	EquippingModel_t128 * ___GLOBAL_4;
};
