﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct List_1_t163;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct  Enumerator_t3470 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::l
	List_1_t163 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::current
	UnityKeyValuePair_2_t164 * ___current_3;
};
