﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct ObjectPool_1_t1333;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t1334;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t1366;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m6171(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t1333 *, UnityAction_1_t1334 *, UnityAction_1_t1334 *, MethodInfo*))ObjectPool_1__ctor_m21655_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m24080(__this, method) (( int32_t (*) (ObjectPool_1_t1333 *, MethodInfo*))ObjectPool_1_get_countAll_m21657_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m24081(__this, ___value, method) (( void (*) (ObjectPool_1_t1333 *, int32_t, MethodInfo*))ObjectPool_1_set_countAll_m21659_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m24082(__this, method) (( int32_t (*) (ObjectPool_1_t1333 *, MethodInfo*))ObjectPool_1_get_countActive_m21661_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m24083(__this, method) (( int32_t (*) (ObjectPool_1_t1333 *, MethodInfo*))ObjectPool_1_get_countInactive_m21663_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m6172(__this, method) (( List_1_t1366 * (*) (ObjectPool_1_t1333 *, MethodInfo*))ObjectPool_1_Get_m21665_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m6173(__this, ___element, method) (( void (*) (ObjectPool_1_t1333 *, List_1_t1366 *, MethodInfo*))ObjectPool_1_Release_m21667_gshared)(__this, ___element, method)
