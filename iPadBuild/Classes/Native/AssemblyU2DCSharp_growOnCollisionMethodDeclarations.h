﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// growOnCollision
struct growOnCollision_t808;

// System.Void growOnCollision::.ctor()
extern "C" void growOnCollision__ctor_m3547 (growOnCollision_t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void growOnCollision::Start()
extern "C" void growOnCollision_Start_m3548 (growOnCollision_t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void growOnCollision::Update()
extern "C" void growOnCollision_Update_m3549 (growOnCollision_t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
