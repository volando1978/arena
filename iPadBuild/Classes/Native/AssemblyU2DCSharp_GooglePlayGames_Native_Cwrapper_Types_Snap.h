﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Snap.h"
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
struct  SnapshotConflictPolicy_t521 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy::value__
	int32_t ___value___1;
};
