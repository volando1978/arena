﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Terrain
struct Terrain_t1600;

// System.Void UnityEngine.Terrain::.ctor()
extern "C" void Terrain__ctor_m7240 (Terrain_t1600 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
