﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Status/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Res.h"
// GooglePlayGames.Native.Cwrapper.Status/ResponseStatus
struct  ResponseStatus_t482 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Status/ResponseStatus::value__
	int32_t ___value___1;
};
