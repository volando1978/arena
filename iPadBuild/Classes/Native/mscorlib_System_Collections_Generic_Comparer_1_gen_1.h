﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.IntPtr>
struct Comparer_1_t3815;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.IntPtr>
struct  Comparer_1_t3815  : public Object_t
{
};
struct Comparer_1_t3815_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.IntPtr>::_default
	Comparer_1_t3815 * ____default_0;
};
