﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom
struct LeavingRoom_t589;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// System.Action
struct Action_t588;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession,GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,System.Action)
extern "C" void LeavingRoom__ctor_m2440 (LeavingRoom_t589 * __this, RoomSession_t566 * ___session, NativeRealTimeRoom_t574 * ___room, Action_t588 * ___leavingCompleteCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::IsActive()
extern "C" bool LeavingRoom_IsActive_m2441 (LeavingRoom_t589 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::OnStateEntered()
extern "C" void LeavingRoom_OnStateEntered_m2442 (LeavingRoom_t589 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::<OnStateEntered>m__46(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern "C" void LeavingRoom_U3COnStateEnteredU3Em__46_m2443 (LeavingRoom_t589 * __this, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
