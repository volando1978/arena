﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
struct QuestUIResponse_t692;
// GooglePlayGames.Native.NativeQuest
struct NativeQuest_t677;
// GooglePlayGames.Native.NativeQuestMilestone
struct NativeQuestMilestone_t676;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::.ctor(System.IntPtr)
extern "C" void QuestUIResponse__ctor_m2915 (QuestUIResponse_t692 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::RequestStatus()
extern "C" int32_t QuestUIResponse_RequestStatus_m2916 (QuestUIResponse_t692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::RequestSucceeded()
extern "C" bool QuestUIResponse_RequestSucceeded_m2917 (QuestUIResponse_t692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::AcceptedQuest()
extern "C" NativeQuest_t677 * QuestUIResponse_AcceptedQuest_m2918 (QuestUIResponse_t692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuestMilestone GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::MilestoneToClaim()
extern "C" NativeQuestMilestone_t676 * QuestUIResponse_MilestoneToClaim_m2919 (QuestUIResponse_t692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void QuestUIResponse_CallDispose_m2920 (QuestUIResponse_t692 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::FromPointer(System.IntPtr)
extern "C" QuestUIResponse_t692 * QuestUIResponse_FromPointer_m2921 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
