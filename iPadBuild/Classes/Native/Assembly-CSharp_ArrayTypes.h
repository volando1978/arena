﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// GooglePlayGames.BasicApi.Achievement[]
// GooglePlayGames.BasicApi.Achievement[]
struct  AchievementU5BU5D_t3635  : public Array_t
{
};
// GooglePlayGames.BasicApi.Events.IEvent[]
// GooglePlayGames.BasicApi.Events.IEvent[]
struct  IEventU5BU5D_t3640  : public Array_t
{
};
// GooglePlayGames.BasicApi.Multiplayer.Participant[]
// GooglePlayGames.BasicApi.Multiplayer.Participant[]
struct  ParticipantU5BU5D_t3647  : public Array_t
{
};
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult[]
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult[]
struct  ParticipantResultU5BU5D_t3675  : public Array_t
{
};
// GooglePlayGames.BasicApi.Quests.IQuest[]
// GooglePlayGames.BasicApi.Quests.IQuest[]
struct  IQuestU5BU5D_t3682  : public Array_t
{
};
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata[]
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata[]
struct  ISavedGameMetadataU5BU5D_t3689  : public Array_t
{
};
// GooglePlayGames.Native.NativeEvent[]
// GooglePlayGames.Native.NativeEvent[]
struct  NativeEventU5BU5D_t3728  : public Array_t
{
};
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant[]
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant[]
struct  MultiplayerParticipantU5BU5D_t3739  : public Array_t
{
};
struct MultiplayerParticipantU5BU5D_t3739_StaticFields{
};
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct  ParticipantStatusU5BU5D_t3757  : public Array_t
{
};
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus[]
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus[]
struct  ParticipantStatusU5BU5D_t3786  : public Array_t
{
};
// cabezaScr/Frame[]
// cabezaScr/Frame[]
struct  FrameU5BU5D_t3823  : public Array_t
{
};
