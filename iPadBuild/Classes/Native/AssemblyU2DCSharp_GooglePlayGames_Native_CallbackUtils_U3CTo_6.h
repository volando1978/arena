﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2<System.Int32,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey16_2_t3726;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Int32,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey17_2_t3727  : public Object_t
{
	// T1 GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Int32,System.Object>::val1
	int32_t ___val1_0;
	// T2 GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Int32,System.Object>::val2
	Object_t * ___val2_1;
	// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2<T1,T2> GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Int32,System.Object>::<>f__ref$22
	U3CToOnGameThreadU3Ec__AnonStorey16_2_t3726 * ___U3CU3Ef__refU2422_2;
};
