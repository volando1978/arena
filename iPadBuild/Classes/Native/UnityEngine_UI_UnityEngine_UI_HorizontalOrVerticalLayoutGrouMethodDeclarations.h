﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t1319;

// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m5587 (HorizontalOrVerticalLayoutGroup_t1319 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m5588 (HorizontalOrVerticalLayoutGroup_t1319 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m5589 (HorizontalOrVerticalLayoutGroup_t1319 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m5590 (HorizontalOrVerticalLayoutGroup_t1319 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591 (HorizontalOrVerticalLayoutGroup_t1319 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m5592 (HorizontalOrVerticalLayoutGroup_t1319 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593 (HorizontalOrVerticalLayoutGroup_t1319 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m5594 (HorizontalOrVerticalLayoutGroup_t1319 * __this, int32_t ___axis, bool ___isVertical, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m5595 (HorizontalOrVerticalLayoutGroup_t1319 * __this, int32_t ___axis, bool ___isVertical, MethodInfo* method) IL2CPP_METHOD_ATTR;
