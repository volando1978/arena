﻿#pragma once
#include <stdint.h>
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct Action_1_t660;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks
struct  Callbacks_t661  : public Object_t
{
};
struct Callbacks_t661_StaticFields{
	// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus> GooglePlayGames.Native.PInvoke.Callbacks::NoopUICallback
	Action_1_t660 * ___NoopUICallback_0;
	// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus> GooglePlayGames.Native.PInvoke.Callbacks::<>f__am$cache1
	Action_1_t660 * ___U3CU3Ef__amU24cache1_1;
};
