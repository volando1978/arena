﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState
struct RoomCreationPendingState_t582;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t695;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" void RoomCreationPendingState__ctor_m2407 (RoomCreationPendingState_t582 * __this, RoomSession_t566 * ___session, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern "C" void RoomCreationPendingState_HandleRoomResponse_m2408 (RoomCreationPendingState_t582 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::IsActive()
extern "C" bool RoomCreationPendingState_IsActive_m2409 (RoomCreationPendingState_t582 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState::LeaveRoom()
extern "C" void RoomCreationPendingState_LeaveRoom_m2410 (RoomCreationPendingState_t582 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
