﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>
struct Enumerator_t4038;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t720;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t1302;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m23940(__this, ___l, method) (( void (*) (Enumerator_t4038 *, List_1_t1302 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23941(__this, method) (( Object_t * (*) (Enumerator_t4038 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::Dispose()
#define Enumerator_Dispose_m23942(__this, method) (( void (*) (Enumerator_t4038 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::VerifyState()
#define Enumerator_VerifyState_m23943(__this, method) (( void (*) (Enumerator_t4038 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::MoveNext()
#define Enumerator_MoveNext_m23944(__this, method) (( bool (*) (Enumerator_t4038 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::get_Current()
#define Enumerator_get_Current_m23945(__this, method) (( Toggle_t720 * (*) (Enumerator_t4038 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
