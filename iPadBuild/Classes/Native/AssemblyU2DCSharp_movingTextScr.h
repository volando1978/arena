﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// movingTextScr
struct  movingTextScr_t818  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle movingTextScr::st
	GUIStyle_t724 * ___st_2;
	// System.Int32 movingTextScr::bulletType
	int32_t ___bulletType_3;
};
