﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
struct  ParticipantStatus_t346 
{
	// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus::value__
	int32_t ___value___1;
};
