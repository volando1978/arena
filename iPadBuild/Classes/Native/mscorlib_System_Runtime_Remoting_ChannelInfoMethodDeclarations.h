﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t2590;
// System.Object[]
struct ObjectU5BU5D_t208;

// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern "C" void ChannelInfo__ctor_m12512 (ChannelInfo_t2590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern "C" ObjectU5BU5D_t208* ChannelInfo_get_ChannelData_m12513 (ChannelInfo_t2590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
