﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange
struct SnapshotMetadataChange_t480;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  SnapshotMetadataChange_SnapshotMetadataChange_Description_m2124 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Image(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t SnapshotMetadataChange_SnapshotMetadataChange_Image_m2125 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTimeIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_PlayedTimeIsChanged_m2126 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_Valid_m2127 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t SnapshotMetadataChange_SnapshotMetadataChange_PlayedTime_m2128 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void SnapshotMetadataChange_SnapshotMetadataChange_Dispose_m2129 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_ImageIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_ImageIsChanged_m2130 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_DescriptionIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_DescriptionIsChanged_m2131 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
