﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.Base64Constants
struct Base64Constants_t2683;

// System.Void System.Security.Cryptography.Base64Constants::.cctor()
extern "C" void Base64Constants__cctor_m12906 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
