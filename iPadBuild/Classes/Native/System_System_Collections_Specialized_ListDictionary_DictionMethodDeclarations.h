﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t1966;
// System.Object
struct Object_t;

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNode::.ctor(System.Object,System.Object,System.Collections.Specialized.ListDictionary/DictionaryNode)
extern "C" void DictionaryNode__ctor_m7871 (DictionaryNode_t1966 * __this, Object_t * ___key, Object_t * ___value, DictionaryNode_t1966 * ___next, MethodInfo* method) IL2CPP_METHOD_ATTR;
