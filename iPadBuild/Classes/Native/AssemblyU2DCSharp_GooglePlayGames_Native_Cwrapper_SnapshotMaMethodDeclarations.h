﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback
struct FetchAllCallback_t473;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchAllCallback__ctor_m2070 (FetchAllCallback_t473 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchAllCallback_Invoke_m2071 (FetchAllCallback_t473 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchAllCallback_t473(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchAllCallback_BeginInvoke_m2072 (FetchAllCallback_t473 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchAllCallback_EndInvoke_m2073 (FetchAllCallback_t473 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
