﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver
struct NativeConflictResolver_t613;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45
struct U3CInternalManualOpenU3Ec__AnonStorey45_t622;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46
struct  U3CInternalManualOpenU3Ec__AnonStorey46_t623  : public Object_t
{
	// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::resolver
	NativeConflictResolver_t613 * ___resolver_0;
	// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::original
	NativeSnapshotMetadata_t611 * ___original_1;
	// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::unmerged
	NativeSnapshotMetadata_t611 * ___unmerged_2;
	// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45 GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45/<InternalManualOpen>c__AnonStorey46::<>f__ref$69
	U3CInternalManualOpenU3Ec__AnonStorey45_t622 * ___U3CU3Ef__refU2469_3;
};
