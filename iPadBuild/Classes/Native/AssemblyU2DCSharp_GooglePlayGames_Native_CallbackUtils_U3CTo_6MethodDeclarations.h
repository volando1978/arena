﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Int32,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey17_2_t3727;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Int32,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19764_gshared (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3727 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19764(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3727 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19764_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Int32,System.Object>::<>m__B()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19765_gshared (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3727 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19765(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3727 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19765_gshared)(__this, method)
