﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<cabezaScr/Frame>
struct List_1_t772;
// System.Object
struct Object_t;
// cabezaScr/Frame
struct Frame_t770;
// System.Collections.Generic.IEnumerable`1<cabezaScr/Frame>
struct IEnumerable_1_t4397;
// System.Collections.Generic.IEnumerator`1<cabezaScr/Frame>
struct IEnumerator_1_t4398;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<cabezaScr/Frame>
struct ICollection_1_t4399;
// System.Collections.ObjectModel.ReadOnlyCollection`1<cabezaScr/Frame>
struct ReadOnlyCollection_1_t3825;
// cabezaScr/Frame[]
struct FrameU5BU5D_t3823;
// System.Predicate`1<cabezaScr/Frame>
struct Predicate_1_t3826;
// System.Action`1<cabezaScr/Frame>
struct Action_1_t3827;
// System.Comparison`1<cabezaScr/Frame>
struct Comparison_1_t3829;
// System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27.h"

// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m4165(__this, method) (( void (*) (List_1_t772 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20931(__this, ___collection, method) (( void (*) (List_1_t772 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::.ctor(System.Int32)
#define List_1__ctor_m20932(__this, ___capacity, method) (( void (*) (List_1_t772 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::.cctor()
#define List_1__cctor_m20933(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20934(__this, method) (( Object_t* (*) (List_1_t772 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20935(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t772 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20936(__this, method) (( Object_t * (*) (List_1_t772 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20937(__this, ___item, method) (( int32_t (*) (List_1_t772 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20938(__this, ___item, method) (( bool (*) (List_1_t772 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20939(__this, ___item, method) (( int32_t (*) (List_1_t772 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20940(__this, ___index, ___item, method) (( void (*) (List_1_t772 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20941(__this, ___item, method) (( void (*) (List_1_t772 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20942(__this, method) (( bool (*) (List_1_t772 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20943(__this, method) (( bool (*) (List_1_t772 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20944(__this, method) (( Object_t * (*) (List_1_t772 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20945(__this, method) (( bool (*) (List_1_t772 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20946(__this, method) (( bool (*) (List_1_t772 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20947(__this, ___index, method) (( Object_t * (*) (List_1_t772 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20948(__this, ___index, ___value, method) (( void (*) (List_1_t772 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::Add(T)
#define List_1_Add_m20949(__this, ___item, method) (( void (*) (List_1_t772 *, Frame_t770 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20950(__this, ___newCount, method) (( void (*) (List_1_t772 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20951(__this, ___collection, method) (( void (*) (List_1_t772 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20952(__this, ___enumerable, method) (( void (*) (List_1_t772 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20953(__this, ___collection, method) (( void (*) (List_1_t772 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<cabezaScr/Frame>::AsReadOnly()
#define List_1_AsReadOnly_m20954(__this, method) (( ReadOnlyCollection_1_t3825 * (*) (List_1_t772 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::Clear()
#define List_1_Clear_m20955(__this, method) (( void (*) (List_1_t772 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<cabezaScr/Frame>::Contains(T)
#define List_1_Contains_m20956(__this, ___item, method) (( bool (*) (List_1_t772 *, Frame_t770 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20957(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t772 *, FrameU5BU5D_t3823*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<cabezaScr/Frame>::Find(System.Predicate`1<T>)
#define List_1_Find_m20958(__this, ___match, method) (( Frame_t770 * (*) (List_1_t772 *, Predicate_1_t3826 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20959(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3826 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m20960(__this, ___match, method) (( int32_t (*) (List_1_t772 *, Predicate_1_t3826 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20961(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t772 *, int32_t, int32_t, Predicate_1_t3826 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m20962(__this, ___action, method) (( void (*) (List_1_t772 *, Action_1_t3827 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<cabezaScr/Frame>::GetEnumerator()
#define List_1_GetEnumerator_m20963(__this, method) (( Enumerator_t3828  (*) (List_1_t772 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::IndexOf(T)
#define List_1_IndexOf_m20964(__this, ___item, method) (( int32_t (*) (List_1_t772 *, Frame_t770 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20965(__this, ___start, ___delta, method) (( void (*) (List_1_t772 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20966(__this, ___index, method) (( void (*) (List_1_t772 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::Insert(System.Int32,T)
#define List_1_Insert_m20967(__this, ___index, ___item, method) (( void (*) (List_1_t772 *, int32_t, Frame_t770 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20968(__this, ___collection, method) (( void (*) (List_1_t772 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<cabezaScr/Frame>::Remove(T)
#define List_1_Remove_m20969(__this, ___item, method) (( bool (*) (List_1_t772 *, Frame_t770 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20970(__this, ___match, method) (( int32_t (*) (List_1_t772 *, Predicate_1_t3826 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20971(__this, ___index, method) (( void (*) (List_1_t772 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::Reverse()
#define List_1_Reverse_m20972(__this, method) (( void (*) (List_1_t772 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::Sort()
#define List_1_Sort_m20973(__this, method) (( void (*) (List_1_t772 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20974(__this, ___comparison, method) (( void (*) (List_1_t772 *, Comparison_1_t3829 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<cabezaScr/Frame>::ToArray()
#define List_1_ToArray_m20975(__this, method) (( FrameU5BU5D_t3823* (*) (List_1_t772 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::TrimExcess()
#define List_1_TrimExcess_m20976(__this, method) (( void (*) (List_1_t772 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::get_Capacity()
#define List_1_get_Capacity_m20977(__this, method) (( int32_t (*) (List_1_t772 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20978(__this, ___value, method) (( void (*) (List_1_t772 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::get_Count()
#define List_1_get_Count_m20979(__this, method) (( int32_t (*) (List_1_t772 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<cabezaScr/Frame>::get_Item(System.Int32)
#define List_1_get_Item_m20980(__this, ___index, method) (( Frame_t770 * (*) (List_1_t772 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<cabezaScr/Frame>::set_Item(System.Int32,T)
#define List_1_set_Item_m20981(__this, ___index, ___value, method) (( void (*) (List_1_t772 *, int32_t, Frame_t770 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
