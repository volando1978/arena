﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.UnityAds
struct UnityAds_t154;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t43;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t153;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"
// UnityEngine.Advertisements.ShowResult
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho_0.h"

// System.Void UnityEngine.Advertisements.UnityAds::.ctor()
extern "C" void UnityAds__ctor_m739 (UnityAds_t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::.cctor()
extern "C" void UnityAds__cctor_m740 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Advertisements.UnityAds UnityEngine.Advertisements.UnityAds::get_SharedInstance()
extern "C" UnityAds_t154 * UnityAds_get_SharedInstance_m741 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::Init(System.String,System.Boolean)
extern "C" void UnityAds_Init_m742 (UnityAds_t154 * __this, String_t* ___gameId, bool ___testModeEnabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::Awake()
extern "C" void UnityAds_Awake_m743 (UnityAds_t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAds::isSupported()
extern "C" bool UnityAds_isSupported_m744 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAds::getSDKVersion()
extern "C" String_t* UnityAds_getSDKVersion_m745 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern "C" void UnityAds_setLogLevel_m746 (Object_t * __this /* static, unused */, int32_t ___logLevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAds::canShowZone(System.String)
extern "C" bool UnityAds_canShowZone_m747 (Object_t * __this /* static, unused */, String_t* ___zone, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAds::hasMultipleRewardItems()
extern "C" bool UnityAds_hasMultipleRewardItems_m748 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> UnityEngine.Advertisements.UnityAds::getRewardItemKeys()
extern "C" List_1_t43 * UnityAds_getRewardItemKeys_m749 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAds::getDefaultRewardItemKey()
extern "C" String_t* UnityAds_getDefaultRewardItemKey_m750 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAds::getCurrentRewardItemKey()
extern "C" String_t* UnityAds_getCurrentRewardItemKey_m751 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAds::setRewardItemKey(System.String)
extern "C" bool UnityAds_setRewardItemKey_m752 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::setDefaultRewardItemAsRewardItem()
extern "C" void UnityAds_setDefaultRewardItemAsRewardItem_m753 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAds::getRewardItemNameKey()
extern "C" String_t* UnityAds_getRewardItemNameKey_m754 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAds::getRewardItemPictureKey()
extern "C" String_t* UnityAds_getRewardItemPictureKey_m755 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Advertisements.UnityAds::getRewardItemDetailsWithKey(System.String)
extern "C" Dictionary_2_t165 * UnityAds_getRewardItemDetailsWithKey_m756 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern "C" void UnityAds_Show_m757 (UnityAds_t154 * __this, String_t* ___zoneId, ShowOptions_t153 * ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String)
extern "C" bool UnityAds_show_m758 (Object_t * __this /* static, unused */, String_t* ___zoneId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String,System.String)
extern "C" bool UnityAds_show_m759 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" bool UnityAds_show_m760 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, Dictionary_2_t165 * ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::deliverCallback(UnityEngine.Advertisements.ShowResult)
extern "C" void UnityAds_deliverCallback_m761 (Object_t * __this /* static, unused */, int32_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::hide()
extern "C" void UnityAds_hide_m762 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::fillRewardItemKeyData()
extern "C" void UnityAds_fillRewardItemKeyData_m763 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAds::parseOptionsDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" String_t* UnityAds_parseOptionsDictionary_m764 (Object_t * __this /* static, unused */, Dictionary_2_t165 * ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::onHide()
extern "C" void UnityAds_onHide_m765 (UnityAds_t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::onShow()
extern "C" void UnityAds_onShow_m766 (UnityAds_t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::onVideoStarted()
extern "C" void UnityAds_onVideoStarted_m767 (UnityAds_t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::onVideoCompleted(System.String)
extern "C" void UnityAds_onVideoCompleted_m768 (UnityAds_t154 * __this, String_t* ___parameters, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::onFetchCompleted()
extern "C" void UnityAds_onFetchCompleted_m769 (UnityAds_t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAds::onFetchFailed()
extern "C" void UnityAds_onFetchFailed_m770 (UnityAds_t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
