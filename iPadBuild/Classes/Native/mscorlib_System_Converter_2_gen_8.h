﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.String>
struct  Converter_2_t3460  : public MulticastDelegate_t22
{
};
