﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2D
struct U3CCreateQuickGameU3Ec__AnonStorey2D_t591;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2D::.ctor()
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2D__ctor_m2448 (U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
