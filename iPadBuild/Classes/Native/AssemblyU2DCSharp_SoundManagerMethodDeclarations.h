﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SoundManager
struct SoundManager_t756;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void SoundManager::.ctor()
extern "C" void SoundManager__ctor_m3254 (SoundManager_t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Awake()
extern "C" void SoundManager_Awake_m3255 (SoundManager_t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Update()
extern "C" void SoundManager_Update_m3256 (SoundManager_t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playDing()
extern "C" void SoundManager_playDing_m3257 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playnBulletClip()
extern "C" void SoundManager_playnBulletClip_m3258 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playwBulletClip()
extern "C" void SoundManager_playwBulletClip_m3259 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playlaserBulletClip()
extern "C" void SoundManager_playlaserBulletClip_m3260 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playtriBulletClip()
extern "C" void SoundManager_playtriBulletClip_m3261 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playcircularBulletClip()
extern "C" void SoundManager_playcircularBulletClip_m3262 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playmoireBulletClip()
extern "C" void SoundManager_playmoireBulletClip_m3263 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playdustClip()
extern "C" void SoundManager_playdustClip_m3264 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playBombaClip(UnityEngine.Vector2)
extern "C" void SoundManager_playBombaClip_m3265 (Object_t * __this /* static, unused */, Vector2_t739  ___pos, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playSmallBombaClip(UnityEngine.Vector2)
extern "C" void SoundManager_playSmallBombaClip_m3266 (Object_t * __this /* static, unused */, Vector2_t739  ___pos, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playBombaAmarillaClip(UnityEngine.Vector2)
extern "C" void SoundManager_playBombaAmarillaClip_m3267 (Object_t * __this /* static, unused */, Vector2_t739  ___pos, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playRisaClip()
extern "C" void SoundManager_playRisaClip_m3268 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playBolasClip()
extern "C" void SoundManager_playBolasClip_m3269 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playTresClip()
extern "C" void SoundManager_playTresClip_m3270 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playLaserClip()
extern "C" void SoundManager_playLaserClip_m3271 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playCircularClip()
extern "C" void SoundManager_playCircularClip_m3272 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playRepeteaClip()
extern "C" void SoundManager_playRepeteaClip_m3273 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playRayosClip()
extern "C" void SoundManager_playRayosClip_m3274 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playFantasmaClip()
extern "C" void SoundManager_playFantasmaClip_m3275 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playBombaSpeechClip()
extern "C" void SoundManager_playBombaSpeechClip_m3276 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playBoomClip()
extern "C" void SoundManager_playBoomClip_m3277 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playCaralludoClip()
extern "C" void SoundManager_playCaralludoClip_m3278 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playCrispadoClip()
extern "C" void SoundManager_playCrispadoClip_m3279 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playInterferenciaClip()
extern "C" void SoundManager_playInterferenciaClip_m3280 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playPillarPaqueteClip()
extern "C" void SoundManager_playPillarPaqueteClip_m3281 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playShortButton()
extern "C" void SoundManager_playShortButton_m3282 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playLongButton()
extern "C" void SoundManager_playLongButton_m3283 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playArenaWoman()
extern "C" void SoundManager_playArenaWoman_m3284 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playXplosionsWoman()
extern "C" void SoundManager_playXplosionsWoman_m3285 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playRaysWoman()
extern "C" void SoundManager_playRaysWoman_m3286 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playAwesomeWoman()
extern "C" void SoundManager_playAwesomeWoman_m3287 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
