﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>
struct List_1_t49;
// System.Object
struct Object_t;
// Soomla.Schedule/DateTimeRange
struct DateTimeRange_t47;
// System.Collections.Generic.IEnumerable`1<Soomla.Schedule/DateTimeRange>
struct IEnumerable_1_t4290;
// System.Collections.Generic.IEnumerator`1<Soomla.Schedule/DateTimeRange>
struct IEnumerator_1_t4291;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Schedule/DateTimeRange>
struct ICollection_1_t4292;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Schedule/DateTimeRange>
struct ReadOnlyCollection_1_t3511;
// Soomla.Schedule/DateTimeRange[]
struct DateTimeRangeU5BU5D_t3509;
// System.Predicate`1<Soomla.Schedule/DateTimeRange>
struct Predicate_1_t3512;
// System.Action`1<Soomla.Schedule/DateTimeRange>
struct Action_1_t3513;
// System.Comparison`1<Soomla.Schedule/DateTimeRange>
struct Comparison_1_t3514;
// System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m916(__this, method) (( void (*) (List_1_t49 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16631(__this, ___collection, method) (( void (*) (List_1_t49 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::.ctor(System.Int32)
#define List_1__ctor_m16632(__this, ___capacity, method) (( void (*) (List_1_t49 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::.cctor()
#define List_1__cctor_m16633(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16634(__this, method) (( Object_t* (*) (List_1_t49 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16635(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t49 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16636(__this, method) (( Object_t * (*) (List_1_t49 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16637(__this, ___item, method) (( int32_t (*) (List_1_t49 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16638(__this, ___item, method) (( bool (*) (List_1_t49 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16639(__this, ___item, method) (( int32_t (*) (List_1_t49 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16640(__this, ___index, ___item, method) (( void (*) (List_1_t49 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16641(__this, ___item, method) (( void (*) (List_1_t49 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16642(__this, method) (( bool (*) (List_1_t49 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16643(__this, method) (( bool (*) (List_1_t49 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16644(__this, method) (( Object_t * (*) (List_1_t49 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16645(__this, method) (( bool (*) (List_1_t49 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16646(__this, method) (( bool (*) (List_1_t49 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16647(__this, ___index, method) (( Object_t * (*) (List_1_t49 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16648(__this, ___index, ___value, method) (( void (*) (List_1_t49 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Add(T)
#define List_1_Add_m16649(__this, ___item, method) (( void (*) (List_1_t49 *, DateTimeRange_t47 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16650(__this, ___newCount, method) (( void (*) (List_1_t49 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16651(__this, ___collection, method) (( void (*) (List_1_t49 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16652(__this, ___enumerable, method) (( void (*) (List_1_t49 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16653(__this, ___collection, method) (( void (*) (List_1_t49 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::AsReadOnly()
#define List_1_AsReadOnly_m16654(__this, method) (( ReadOnlyCollection_1_t3511 * (*) (List_1_t49 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Clear()
#define List_1_Clear_m16655(__this, method) (( void (*) (List_1_t49 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Contains(T)
#define List_1_Contains_m16656(__this, ___item, method) (( bool (*) (List_1_t49 *, DateTimeRange_t47 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16657(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t49 *, DateTimeRangeU5BU5D_t3509*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Find(System.Predicate`1<T>)
#define List_1_Find_m16658(__this, ___match, method) (( DateTimeRange_t47 * (*) (List_1_t49 *, Predicate_1_t3512 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16659(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3512 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m16660(__this, ___match, method) (( int32_t (*) (List_1_t49 *, Predicate_1_t3512 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16661(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t49 *, int32_t, int32_t, Predicate_1_t3512 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m16662(__this, ___action, method) (( void (*) (List_1_t49 *, Action_1_t3513 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::GetEnumerator()
#define List_1_GetEnumerator_m920(__this, method) (( Enumerator_t205  (*) (List_1_t49 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::IndexOf(T)
#define List_1_IndexOf_m16663(__this, ___item, method) (( int32_t (*) (List_1_t49 *, DateTimeRange_t47 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16664(__this, ___start, ___delta, method) (( void (*) (List_1_t49 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16665(__this, ___index, method) (( void (*) (List_1_t49 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Insert(System.Int32,T)
#define List_1_Insert_m16666(__this, ___index, ___item, method) (( void (*) (List_1_t49 *, int32_t, DateTimeRange_t47 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16667(__this, ___collection, method) (( void (*) (List_1_t49 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Remove(T)
#define List_1_Remove_m16668(__this, ___item, method) (( bool (*) (List_1_t49 *, DateTimeRange_t47 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16669(__this, ___match, method) (( int32_t (*) (List_1_t49 *, Predicate_1_t3512 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16670(__this, ___index, method) (( void (*) (List_1_t49 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Reverse()
#define List_1_Reverse_m16671(__this, method) (( void (*) (List_1_t49 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Sort()
#define List_1_Sort_m16672(__this, method) (( void (*) (List_1_t49 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16673(__this, ___comparison, method) (( void (*) (List_1_t49 *, Comparison_1_t3514 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::ToArray()
#define List_1_ToArray_m16674(__this, method) (( DateTimeRangeU5BU5D_t3509* (*) (List_1_t49 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::TrimExcess()
#define List_1_TrimExcess_m16675(__this, method) (( void (*) (List_1_t49 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::get_Capacity()
#define List_1_get_Capacity_m16676(__this, method) (( int32_t (*) (List_1_t49 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16677(__this, ___value, method) (( void (*) (List_1_t49 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::get_Count()
#define List_1_get_Count_m16678(__this, method) (( int32_t (*) (List_1_t49 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::get_Item(System.Int32)
#define List_1_get_Item_m16679(__this, ___index, method) (( DateTimeRange_t47 * (*) (List_1_t49 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::set_Item(System.Int32,T)
#define List_1_set_Item_m16680(__this, ___index, ___value, method) (( void (*) (List_1_t49 *, int32_t, DateTimeRange_t47 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
