﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F
struct U3CUpdateAchievementU3Ec__AnonStorey1F_t528;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
struct FetchResponse_t653;

// System.Void GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F::.ctor()
extern "C" void U3CUpdateAchievementU3Ec__AnonStorey1F__ctor_m2245 (U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F::<>m__17(GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse)
extern "C" void U3CUpdateAchievementU3Ec__AnonStorey1F_U3CU3Em__17_m2246 (U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * __this, FetchResponse_t653 * ___rsp, MethodInfo* method) IL2CPP_METHOD_ATTR;
