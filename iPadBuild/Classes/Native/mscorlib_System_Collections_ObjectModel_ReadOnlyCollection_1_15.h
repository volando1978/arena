﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<GooglePlayGames.BasicApi.Achievement>
struct IList_1_t3636;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>
struct  ReadOnlyCollection_1_t3637  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::list
	Object_t* ___list_0;
};
