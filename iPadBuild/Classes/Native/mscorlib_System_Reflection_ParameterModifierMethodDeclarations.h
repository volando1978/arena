﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.ParameterModifier
struct ParameterModifier_t2550;
struct ParameterModifier_t2550_marshaled;

void ParameterModifier_t2550_marshal(const ParameterModifier_t2550& unmarshaled, ParameterModifier_t2550_marshaled& marshaled);
void ParameterModifier_t2550_marshal_back(const ParameterModifier_t2550_marshaled& marshaled, ParameterModifier_t2550& unmarshaled);
void ParameterModifier_t2550_marshal_cleanup(ParameterModifier_t2550_marshaled& marshaled);
