﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.NetworkReachability
#include "UnityEngine_UnityEngine_NetworkReachability.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// UnityEngine.NetworkReachability
#include "UnityEngine_UnityEngine_NetworkReachabilityMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LogCallback__ctor_m6964 (LogCallback_t1555 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C" void LogCallback_Invoke_m6965 (LogCallback_t1555 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LogCallback_Invoke_m6965((LogCallback_t1555 *)__this->___prev_9,___condition, ___stackTrace, ___type, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___stackTrace, int32_t ___type, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LogCallback_t1555(Il2CppObject* delegate, String_t* ___condition, String_t* ___stackTrace, int32_t ___type)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___condition' to native representation
	char* ____condition_marshaled = { 0 };
	____condition_marshaled = il2cpp_codegen_marshal_string(___condition);

	// Marshaling of parameter '___stackTrace' to native representation
	char* ____stackTrace_marshaled = { 0 };
	____stackTrace_marshaled = il2cpp_codegen_marshal_string(___stackTrace);

	// Marshaling of parameter '___type' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____condition_marshaled, ____stackTrace_marshaled, ___type);

	// Marshaling cleanup of parameter '___condition' native representation
	il2cpp_codegen_marshal_free(____condition_marshaled);
	____condition_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace' native representation
	il2cpp_codegen_marshal_free(____stackTrace_marshaled);
	____stackTrace_marshaled = NULL;

	// Marshaling cleanup of parameter '___type' native representation

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern TypeInfo* LogType_t1492_il2cpp_TypeInfo_var;
extern "C" Object_t * LogCallback_BeginInvoke_m6966 (LogCallback_t1555 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogType_t1492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2895);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition;
	__d_args[1] = ___stackTrace;
	__d_args[2] = Box(LogType_t1492_il2cpp_TypeInfo_var, &___type);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C" void LogCallback_EndInvoke_m6967 (LogCallback_t1555 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Application
#include "UnityEngine_UnityEngine_Application.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperation.h"


// System.Void UnityEngine.Application::Quit()
extern "C" void Application_Quit_m1017 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef void (*Application_Quit_m1017_ftn) ();
	static Application_Quit_m1017_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m1017_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C" void Application_LoadLevel_m4258 (Object_t * __this /* static, unused */, String_t* ___name, MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Application_LoadLevelAsync_m6968(NULL /*static, unused*/, L_0, (-1), 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" AsyncOperation_t1486 * Application_LoadLevelAsync_m6968 (Object_t * __this /* static, unused */, String_t* ___monoLevelName, int32_t ___index, bool ___additive, bool ___mustCompleteNextFrame, MethodInfo* method)
{
	typedef AsyncOperation_t1486 * (*Application_LoadLevelAsync_m6968_ftn) (String_t*, int32_t, bool, bool);
	static Application_LoadLevelAsync_m6968_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_LoadLevelAsync_m6968_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___monoLevelName, ___index, ___additive, ___mustCompleteNextFrame);
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" bool Application_get_isPlaying_m3733 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m3733_ftn) ();
	static Application_get_isPlaying_m3733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m3733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C" bool Application_get_isEditor_m1032 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef bool (*Application_get_isEditor_m1032_ftn) ();
	static Application_get_isEditor_m1032_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m1032_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" int32_t Application_get_platform_m1033 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m1033_ftn) ();
	static Application_get_platform_m1033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m1033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_unityVersion()
extern "C" String_t* Application_get_unityVersion_m1058 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef String_t* (*Application_get_unityVersion_m1058_ftn) ();
	static Application_get_unityVersion_m1058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_unityVersion_m1058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_unityVersion()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C" void Application_set_targetFrameRate_m4239 (Object_t * __this /* static, unused */, int32_t ___value, MethodInfo* method)
{
	typedef void (*Application_set_targetFrameRate_m4239_ftn) (int32_t);
	static Application_set_targetFrameRate_m4239_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_targetFrameRate_m4239_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_targetFrameRate(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern TypeInfo* Application_t1556_il2cpp_TypeInfo_var;
extern "C" void Application_CallLogCallback_m6969 (Object_t * __this /* static, unused */, String_t* ___logString, String_t* ___stackTrace, int32_t ___type, bool ___invokedOnMainThread, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Application_t1556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2896);
		s_Il2CppMethodIntialized = true;
	}
	LogCallback_t1555 * V_0 = {0};
	LogCallback_t1555 * V_1 = {0};
	{
		bool L_0 = ___invokedOnMainThread;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t1555 * L_1 = ((Application_t1556_StaticFields*)Application_t1556_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0;
		V_0 = L_1;
		LogCallback_t1555 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t1555 * L_3 = V_0;
		String_t* L_4 = ___logString;
		String_t* L_5 = ___stackTrace;
		int32_t L_6 = ___type;
		NullCheck(L_3);
		VirtActionInvoker3< String_t*, String_t*, int32_t >::Invoke(10 /* System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType) */, L_3, L_4, L_5, L_6);
	}

IL_001b:
	{
		LogCallback_t1555 * L_7 = ((Application_t1556_StaticFields*)Application_t1556_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandlerThreaded_1;
		V_1 = L_7;
		LogCallback_t1555 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		LogCallback_t1555 * L_9 = V_1;
		String_t* L_10 = ___logString;
		String_t* L_11 = ___stackTrace;
		int32_t L_12 = ___type;
		NullCheck(L_9);
		VirtActionInvoker3< String_t*, String_t*, int32_t >::Invoke(10 /* System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType) */, L_9, L_10, L_11, L_12);
	}

IL_0030:
	{
		return;
	}
}
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
extern "C" int32_t Application_get_internetReachability_m1063 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (*Application_get_internetReachability_m1063_ftn) ();
	static Application_get_internetReachability_m1063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_internetReachability_m1063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_internetReachability()");
	return _il2cpp_icall_func();
}
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"

// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"


// System.Void UnityEngine.Behaviour::.ctor()
extern "C" void Behaviour__ctor_m6970 (Behaviour_t1416 * __this, MethodInfo* method)
{
	{
		Component__ctor_m7055(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" bool Behaviour_get_enabled_m5770 (Behaviour_t1416 * __this, MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m5770_ftn) (Behaviour_t1416 *);
	static Behaviour_get_enabled_m5770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m5770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" void Behaviour_set_enabled_m4228 (Behaviour_t1416 * __this, bool ___value, MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m4228_ftn) (Behaviour_t1416 *, bool);
	static Behaviour_set_enabled_m4228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m4228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C" bool Behaviour_get_isActiveAndEnabled_m5771 (Behaviour_t1416 * __this, MethodInfo* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m5771_ftn) (Behaviour_t1416 *);
	static Behaviour_get_isActiveAndEnabled_m5771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m5771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallbackMethodDeclarations.h"

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"


// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C" void CameraCallback__ctor_m6971 (CameraCallback_t1557 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C" void CameraCallback_Invoke_m6972 (CameraCallback_t1557 * __this, Camera_t978 * ___cam, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CameraCallback_Invoke_m6972((CameraCallback_t1557 *)__this->___prev_9,___cam, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Camera_t978 * ___cam, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Camera_t978 * ___cam, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t1557(Il2CppObject* delegate, Camera_t978 * ___cam)
{
	// Marshaling of parameter '___cam' to native representation
	Camera_t978 * ____cam_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.Camera'."));
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C" Object_t * CameraCallback_BeginInvoke_m6973 (CameraCallback_t1557 * __this, Camera_t978 * ___cam, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C" void CameraCallback_EndInvoke_m6974 (CameraCallback_t1557 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"


// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C" float Camera_get_nearClipPlane_m5823 (Camera_t978 * __this, MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m5823_ftn) (Camera_t978 *);
	static Camera_get_nearClipPlane_m5823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m5823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C" float Camera_get_farClipPlane_m5822 (Camera_t978 * __this, MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m5822_ftn) (Camera_t978 *);
	static Camera_get_farClipPlane_m5822_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m5822_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C" float Camera_get_orthographicSize_m4024 (Camera_t978 * __this, MethodInfo* method)
{
	typedef float (*Camera_get_orthographicSize_m4024_ftn) (Camera_t978 *);
	static Camera_get_orthographicSize_m4024_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_orthographicSize_m4024_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_orthographicSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C" void Camera_set_orthographicSize_m4173 (Camera_t978 * __this, float ___value, MethodInfo* method)
{
	typedef void (*Camera_set_orthographicSize_m4173_ftn) (Camera_t978 *, float);
	static Camera_set_orthographicSize_m4173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographicSize_m4173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C" void Camera_set_orthographic_m4022 (Camera_t978 * __this, bool ___value, MethodInfo* method)
{
	typedef void (*Camera_set_orthographic_m4022_ftn) (Camera_t978 *, bool);
	static Camera_set_orthographic_m4022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographic_m4022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C" float Camera_get_depth_m5738 (Camera_t978 * __this, MethodInfo* method)
{
	typedef float (*Camera_get_depth_m5738_ftn) (Camera_t978 *);
	static Camera_get_depth_m5738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m5738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_aspect()
extern "C" float Camera_get_aspect_m4023 (Camera_t978 * __this, MethodInfo* method)
{
	typedef float (*Camera_get_aspect_m4023_ftn) (Camera_t978 *);
	static Camera_get_aspect_m4023_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_aspect_m4023_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_aspect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C" int32_t Camera_get_cullingMask_m5833 (Camera_t978 * __this, MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m5833_ftn) (Camera_t978 *);
	static Camera_get_cullingMask_m5833_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m5833_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C" int32_t Camera_get_eventMask_m6975 (Camera_t978 * __this, MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m6975_ftn) (Camera_t978 *);
	static Camera_get_eventMask_m6975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m6975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::INTERNAL_get_backgroundColor(UnityEngine.Color&)
extern "C" void Camera_INTERNAL_get_backgroundColor_m6976 (Camera_t978 * __this, Color_t747 * ___value, MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_backgroundColor_m6976_ftn) (Camera_t978 *, Color_t747 *);
	static Camera_INTERNAL_get_backgroundColor_m6976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_backgroundColor_m6976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_backgroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
extern "C" void Camera_INTERNAL_set_backgroundColor_m6977 (Camera_t978 * __this, Color_t747 * ___value, MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_backgroundColor_m6977_ftn) (Camera_t978 *, Color_t747 *);
	static Camera_INTERNAL_set_backgroundColor_m6977_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_backgroundColor_m6977_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
extern "C" Color_t747  Camera_get_backgroundColor_m4229 (Camera_t978 * __this, MethodInfo* method)
{
	Color_t747  V_0 = {0};
	{
		Camera_INTERNAL_get_backgroundColor_m6976(__this, (&V_0), /*hidden argument*/NULL);
		Color_t747  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern "C" void Camera_set_backgroundColor_m4232 (Camera_t978 * __this, Color_t747  ___value, MethodInfo* method)
{
	{
		Camera_INTERNAL_set_backgroundColor_m6977(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_set_rect_m6978 (Camera_t978 * __this, Rect_t738 * ___value, MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_rect_m6978_ftn) (Camera_t978 *, Rect_t738 *);
	static Camera_INTERNAL_set_rect_m6978_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_rect_m6978_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern "C" void Camera_set_rect_m4174 (Camera_t978 * __this, Rect_t738  ___value, MethodInfo* method)
{
	{
		Camera_INTERNAL_set_rect_m6978(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_get_pixelRect_m6979 (Camera_t978 * __this, Rect_t738 * ___value, MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m6979_ftn) (Camera_t978 *, Rect_t738 *);
	static Camera_INTERNAL_get_pixelRect_m6979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m6979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C" Rect_t738  Camera_get_pixelRect_m6980 (Camera_t978 * __this, MethodInfo* method)
{
	Rect_t738  V_0 = {0};
	{
		Camera_INTERNAL_get_pixelRect_m6979(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t738  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C" RenderTexture_t1507 * Camera_get_targetTexture_m6981 (Camera_t978 * __this, MethodInfo* method)
{
	typedef RenderTexture_t1507 * (*Camera_get_targetTexture_m6981_ftn) (Camera_t978 *);
	static Camera_get_targetTexture_m6981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m6981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C" int32_t Camera_get_clearFlags_m6982 (Camera_t978 * __this, MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m6982_ftn) (Camera_t978 *);
	static Camera_get_clearFlags_m6982_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m6982_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C" Vector3_t758  Camera_WorldToScreenPoint_m4040 (Camera_t978 * __this, Vector3_t758  ___position, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Camera_INTERNAL_CALL_WorldToScreenPoint_m6983(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t758  Camera_INTERNAL_CALL_WorldToScreenPoint_m6983 (Object_t * __this /* static, unused */, Camera_t978 * ___self, Vector3_t758 * ___position, MethodInfo* method)
{
	typedef Vector3_t758  (*Camera_INTERNAL_CALL_WorldToScreenPoint_m6983_ftn) (Camera_t978 *, Vector3_t758 *);
	static Camera_INTERNAL_CALL_WorldToScreenPoint_m6983_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_WorldToScreenPoint_m6983_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C" Vector3_t758  Camera_ViewportToWorldPoint_m4247 (Camera_t978 * __this, Vector3_t758  ___position, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Camera_INTERNAL_CALL_ViewportToWorldPoint_m6984(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t758  Camera_INTERNAL_CALL_ViewportToWorldPoint_m6984 (Object_t * __this /* static, unused */, Camera_t978 * ___self, Vector3_t758 * ___position, MethodInfo* method)
{
	typedef Vector3_t758  (*Camera_INTERNAL_CALL_ViewportToWorldPoint_m6984_ftn) (Camera_t978 *, Vector3_t758 *);
	static Camera_INTERNAL_CALL_ViewportToWorldPoint_m6984_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ViewportToWorldPoint_m6984_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C" Vector3_t758  Camera_ScreenToWorldPoint_m4196 (Camera_t978 * __this, Vector3_t758  ___position, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Camera_INTERNAL_CALL_ScreenToWorldPoint_m6985(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t758  Camera_INTERNAL_CALL_ScreenToWorldPoint_m6985 (Object_t * __this /* static, unused */, Camera_t978 * ___self, Vector3_t758 * ___position, MethodInfo* method)
{
	typedef Vector3_t758  (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m6985_ftn) (Camera_t978 *, Vector3_t758 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m6985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m6985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C" Vector3_t758  Camera_ScreenToViewportPoint_m5898 (Camera_t978 * __this, Vector3_t758  ___position, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Camera_INTERNAL_CALL_ScreenToViewportPoint_m6986(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t758  Camera_INTERNAL_CALL_ScreenToViewportPoint_m6986 (Object_t * __this /* static, unused */, Camera_t978 * ___self, Vector3_t758 * ___position, MethodInfo* method)
{
	typedef Vector3_t758  (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m6986_ftn) (Camera_t978 *, Vector3_t758 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m6986_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m6986_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C" Ray_t1375  Camera_ScreenPointToRay_m5821 (Camera_t978 * __this, Vector3_t758  ___position, MethodInfo* method)
{
	{
		Ray_t1375  L_0 = Camera_INTERNAL_CALL_ScreenPointToRay_m6987(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Ray_t1375  Camera_INTERNAL_CALL_ScreenPointToRay_m6987 (Object_t * __this /* static, unused */, Camera_t978 * ___self, Vector3_t758 * ___position, MethodInfo* method)
{
	typedef Ray_t1375  (*Camera_INTERNAL_CALL_ScreenPointToRay_m6987_ftn) (Camera_t978 *, Vector3_t758 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m6987_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m6987_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" Camera_t978 * Camera_get_main_m4021 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef Camera_t978 * (*Camera_get_main_m4021_ftn) ();
	static Camera_get_main_m4021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m4021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C" int32_t Camera_get_allCamerasCount_m6988 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m6988_ftn) ();
	static Camera_get_allCamerasCount_m6988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m6988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C" int32_t Camera_GetAllCameras_m6989 (Object_t * __this /* static, unused */, CameraU5BU5D_t1631* ___cameras, MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m6989_ftn) (CameraU5BU5D_t1631*);
	static Camera_GetAllCameras_m6989_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m6989_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern TypeInfo* Camera_t978_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreCull_m6990 (Object_t * __this /* static, unused */, Camera_t978 * ___cam, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t978_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t1557 * L_0 = ((Camera_t978_StaticFields*)Camera_t978_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t1557 * L_1 = ((Camera_t978_StaticFields*)Camera_t978_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		Camera_t978 * L_2 = ___cam;
		NullCheck(L_1);
		VirtActionInvoker1< Camera_t978 * >::Invoke(10 /* System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera) */, L_1, L_2);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern TypeInfo* Camera_t978_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreRender_m6991 (Object_t * __this /* static, unused */, Camera_t978 * ___cam, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t978_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t1557 * L_0 = ((Camera_t978_StaticFields*)Camera_t978_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t1557 * L_1 = ((Camera_t978_StaticFields*)Camera_t978_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		Camera_t978 * L_2 = ___cam;
		NullCheck(L_1);
		VirtActionInvoker1< Camera_t978 * >::Invoke(10 /* System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera) */, L_1, L_2);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern TypeInfo* Camera_t978_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPostRender_m6992 (Object_t * __this /* static, unused */, Camera_t978 * ___cam, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t978_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t1557 * L_0 = ((Camera_t978_StaticFields*)Camera_t978_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t1557 * L_1 = ((Camera_t978_StaticFields*)Camera_t978_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		Camera_t978 * L_2 = ___cam;
		NullCheck(L_1);
		VirtActionInvoker1< Camera_t978 * >::Invoke(10 /* System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera) */, L_1, L_2);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t144 * Camera_RaycastTry_m6993 (Camera_t978 * __this, Ray_t1375  ___ray, float ___distance, int32_t ___layerMask, MethodInfo* method)
{
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		GameObject_t144 * L_2 = Camera_INTERNAL_CALL_RaycastTry_m6994(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C" GameObject_t144 * Camera_INTERNAL_CALL_RaycastTry_m6994 (Object_t * __this /* static, unused */, Camera_t978 * ___self, Ray_t1375 * ___ray, float ___distance, int32_t ___layerMask, MethodInfo* method)
{
	typedef GameObject_t144 * (*Camera_INTERNAL_CALL_RaycastTry_m6994_ftn) (Camera_t978 *, Ray_t1375 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m6994_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m6994_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t144 * Camera_RaycastTry2D_m6995 (Camera_t978 * __this, Ray_t1375  ___ray, float ___distance, int32_t ___layerMask, MethodInfo* method)
{
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		GameObject_t144 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m6996(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C" GameObject_t144 * Camera_INTERNAL_CALL_RaycastTry2D_m6996 (Object_t * __this /* static, unused */, Camera_t978 * ___self, Ray_t1375 * ___ray, float ___distance, int32_t ___layerMask, MethodInfo* method)
{
	typedef GameObject_t144 * (*Camera_INTERNAL_CALL_RaycastTry2D_m6996_ftn) (Camera_t978 *, Ray_t1375 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m6996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m6996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask);
}
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_Debug.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C" void Debug_DrawLine_m6997 (Object_t * __this /* static, unused */, Vector3_t758  ___start, Vector3_t758  ___end, Color_t747  ___color, float ___duration, bool ___depthTest, MethodInfo* method)
{
	{
		float L_0 = ___duration;
		bool L_1 = ___depthTest;
		Debug_INTERNAL_CALL_DrawLine_m6998(NULL /*static, unused*/, (&___start), (&___end), (&___color), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern "C" void Debug_INTERNAL_CALL_DrawLine_m6998 (Object_t * __this /* static, unused */, Vector3_t758 * ___start, Vector3_t758 * ___end, Color_t747 * ___color, float ___duration, bool ___depthTest, MethodInfo* method)
{
	typedef void (*Debug_INTERNAL_CALL_DrawLine_m6998_ftn) (Vector3_t758 *, Vector3_t758 *, Color_t747 *, float, bool);
	static Debug_INTERNAL_CALL_DrawLine_m6998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_INTERNAL_CALL_DrawLine_m6998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)");
	_il2cpp_icall_func(___start, ___end, ___color, ___duration, ___depthTest);
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern "C" void Debug_DrawRay_m4135 (Object_t * __this /* static, unused */, Vector3_t758  ___start, Vector3_t758  ___dir, Color_t747  ___color, MethodInfo* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		V_0 = 1;
		V_1 = (0.0f);
		Vector3_t758  L_0 = ___start;
		Vector3_t758  L_1 = ___dir;
		Color_t747  L_2 = ___color;
		float L_3 = V_1;
		bool L_4 = V_0;
		Debug_DrawRay_m6999(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C" void Debug_DrawRay_m6999 (Object_t * __this /* static, unused */, Vector3_t758  ___start, Vector3_t758  ___dir, Color_t747  ___color, float ___duration, bool ___depthTest, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = ___start;
		Vector3_t758  L_1 = ___start;
		Vector3_t758  L_2 = ___dir;
		Vector3_t758  L_3 = Vector3_op_Addition_m4278(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Color_t747  L_4 = ___color;
		float L_5 = ___duration;
		bool L_6 = ___depthTest;
		Debug_DrawLine_m6997(NULL /*static, unused*/, L_0, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)
extern "C" void Debug_Internal_Log_m7000 (Object_t * __this /* static, unused */, int32_t ___level, String_t* ___msg, Object_t187 * ___obj, MethodInfo* method)
{
	typedef void (*Debug_Internal_Log_m7000_ftn) (int32_t, String_t*, Object_t187 *);
	static Debug_Internal_Log_m7000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_Log_m7000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level, ___msg, ___obj);
}
// System.Void UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_Internal_LogException_m7001 (Object_t * __this /* static, unused */, Exception_t135 * ___exception, Object_t187 * ___obj, MethodInfo* method)
{
	typedef void (*Debug_Internal_LogException_m7001_ftn) (Exception_t135 *, Object_t187 *);
	static Debug_Internal_LogException_m7001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_LogException_m7001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception, ___obj);
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" void Debug_Log_m869 (Object_t * __this /* static, unused */, Object_t * ___message, MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 0;
		if (!L_0)
		{
			G_B2_0 = 0;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = (String_t*) &_stringLiteral960;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m7000(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" void Debug_LogError_m910 (Object_t * __this /* static, unused */, Object_t * ___message, MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 2;
		if (!L_0)
		{
			G_B2_0 = 2;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = (String_t*) &_stringLiteral960;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m7000(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C" void Debug_LogError_m5949 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t187 * ___context, MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t187 * L_2 = ___context;
		Debug_Internal_Log_m7000(NULL /*static, unused*/, 2, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C" void Debug_LogException_m7002 (Object_t * __this /* static, unused */, Exception_t135 * ___exception, MethodInfo* method)
{
	{
		Exception_t135 * L_0 = ___exception;
		Debug_Internal_LogException_m7001(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_LogException_m5855 (Object_t * __this /* static, unused */, Exception_t135 * ___exception, Object_t187 * ___context, MethodInfo* method)
{
	{
		Exception_t135 * L_0 = ___exception;
		Object_t187 * L_1 = ___context;
		Debug_Internal_LogException_m7001(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" void Debug_LogWarning_m904 (Object_t * __this /* static, unused */, Object_t * ___message, MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Debug_Internal_Log_m7000(NULL /*static, unused*/, 1, L_1, (Object_t187 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C" void Debug_LogWarning_m6105 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t187 * ___context, MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t187 * L_2 = ___context;
		Debug_Internal_Log_m7000(NULL /*static, unused*/, 1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern "C" bool Debug_get_isDebugBuild_m931 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef bool (*Debug_get_isDebugBuild_m931_ftn) ();
	static Debug_get_isDebugBuild_m931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_get_isDebugBuild_m931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::get_isDebugBuild()");
	return _il2cpp_icall_func();
}
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegateMethodDeclarations.h"



// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void DisplaysUpdatedDelegate__ctor_m7003 (DisplaysUpdatedDelegate_t1559 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C" void DisplaysUpdatedDelegate_Invoke_m7004 (DisplaysUpdatedDelegate_t1559 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m7004((DisplaysUpdatedDelegate_t1559 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1559(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DisplaysUpdatedDelegate_BeginInvoke_m7005 (DisplaysUpdatedDelegate_t1559 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m7006 (DisplaysUpdatedDelegate_t1559 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Display
#include "UnityEngine_UnityEngine_Display.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Display
#include "UnityEngine_UnityEngine_DisplayMethodDeclarations.h"

// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
#include "mscorlib_ArrayTypes.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"


// System.Void UnityEngine.Display::.ctor()
extern "C" void Display__ctor_m7007 (Display_t1561 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = {0};
		IntPtr__ctor_m7510(&L_0, 0, /*hidden argument*/NULL);
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C" void Display__ctor_m7008 (Display_t1561 * __this, IntPtr_t ___nativeDisplay, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay;
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern TypeInfo* DisplayU5BU5D_t1560_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" void Display__cctor_m7009 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t1560_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2897);
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisplayU5BU5D_t1560* L_0 = ((DisplayU5BU5D_t1560*)SZArrayNew(DisplayU5BU5D_t1560_il2cpp_TypeInfo_var, 1));
		Display_t1561 * L_1 = (Display_t1561 *)il2cpp_codegen_object_new (Display_t1561_il2cpp_TypeInfo_var);
		Display__ctor_m7007(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Display_t1561 **)(Display_t1561 **)SZArrayLdElema(L_0, 0)) = (Display_t1561 *)L_1;
		((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___displays_1 = L_0;
		DisplayU5BU5D_t1560* L_2 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t1561 **)(Display_t1561 **)SZArrayLdElema(L_2, L_3));
		((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = (DisplaysUpdatedDelegate_t1559 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Display::add_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t1559_il2cpp_TypeInfo_var;
extern "C" void Display_add_onDisplaysUpdated_m7010 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t1559 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		DisplaysUpdatedDelegate_t1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2899);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1559 * L_0 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t1559 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Combine_m952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t1559 *)Castclass(L_2, DisplaysUpdatedDelegate_t1559_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Display::remove_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t1559_il2cpp_TypeInfo_var;
extern "C" void Display_remove_onDisplaysUpdated_m7011 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t1559 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		DisplaysUpdatedDelegate_t1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2899);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1559 * L_0 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t1559 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Remove_m5890(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t1559 *)Castclass(L_2, DisplaysUpdatedDelegate_t1559_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingWidth_m7012 (Display_t1561 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m7028(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingHeight_m7013 (Display_t1561 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m7028(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemWidth_m7014 (Display_t1561 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m7027(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemHeight_m7015 (Display_t1561 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m7027(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_colorBuffer()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t1619  Display_get_colorBuffer_m7016 (Display_t1561 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t1619  V_0 = {0};
	RenderBuffer_t1619  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m7029(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t1619  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_depthBuffer()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t1619  Display_get_depthBuffer_m7017 (Display_t1561 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t1619  V_0 = {0};
	RenderBuffer_t1619  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m7029(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t1619  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Display::Activate()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m7018 (Display_t1561 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m7031(NULL /*static, unused*/, L_0, 0, 0, ((int32_t)60), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::Activate(System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m7019 (Display_t1561 * __this, int32_t ___width, int32_t ___height, int32_t ___refreshRate, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___refreshRate;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m7031(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetParams(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" void Display_SetParams_m7020 (Display_t1561 * __this, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___x;
		int32_t L_4 = ___y;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_SetParamsImpl_m7032(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetRenderingResolution(System.Int32,System.Int32)
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" void Display_SetRenderingResolution_m7021 (Display_t1561 * __this, int32_t ___w, int32_t ___h, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___w;
		int32_t L_2 = ___h;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_SetRenderingResolutionImpl_m7030(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Display::MultiDisplayLicense()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" bool Display_MultiDisplayLicense_m7022 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		bool L_0 = Display_MultiDisplayLicenseImpl_m7033(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" Vector3_t758  Display_RelativeMouseAt_m7023 (Object_t * __this /* static, unused */, Vector3_t758  ___inputMouseCoordinates, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t758  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = ((&___inputMouseCoordinates)->___x_1);
		V_3 = (((int32_t)L_0));
		float L_1 = ((&___inputMouseCoordinates)->___y_2);
		V_4 = (((int32_t)L_1));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m7034(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->___z_3 = (((float)L_4));
		int32_t L_5 = V_1;
		(&V_0)->___x_1 = (((float)L_5));
		int32_t L_6 = V_2;
		(&V_0)->___y_2 = (((float)L_6));
		Vector3_t758  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Display UnityEngine.Display::get_main()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" Display_t1561 * Display_get_main_m7024 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		Display_t1561 * L_0 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2;
		return L_0;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern TypeInfo* DisplayU5BU5D_t1560_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" void Display_RecreateDisplayList_m7025 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t859* ___nativeDisplay, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t1560_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2897);
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t859* L_0 = ___nativeDisplay;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___displays_1 = ((DisplayU5BU5D_t1560*)SZArrayNew(DisplayU5BU5D_t1560_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_0)->max_length)))));
		V_0 = 0;
		goto IL_0027;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1560* L_1 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t859* L_3 = ___nativeDisplay;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Display_t1561 * L_6 = (Display_t1561 *)il2cpp_codegen_object_new (Display_t1561_il2cpp_TypeInfo_var);
		Display__ctor_m7008(L_6, (*(IntPtr_t*)(IntPtr_t*)SZArrayLdElema(L_3, L_5)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_6);
		*((Display_t1561 **)(Display_t1561 **)SZArrayLdElema(L_1, L_2)) = (Display_t1561 *)L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_8 = V_0;
		IntPtrU5BU5D_t859* L_9 = ___nativeDisplay;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1560* L_10 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t1561 **)(Display_t1561 **)SZArrayLdElema(L_10, L_11));
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern TypeInfo* Display_t1561_il2cpp_TypeInfo_var;
extern "C" void Display_FireDisplaysUpdated_m7026 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2898);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1559 * L_0 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1561_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1559 * L_1 = ((Display_t1561_StaticFields*)Display_t1561_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(10 /* System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke() */, L_1);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetSystemExtImpl_m7027 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, MethodInfo* method)
{
	typedef void (*Display_GetSystemExtImpl_m7027_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m7027_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m7027_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetRenderingExtImpl_m7028 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, MethodInfo* method)
{
	typedef void (*Display_GetRenderingExtImpl_m7028_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m7028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m7028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
extern "C" void Display_GetRenderingBuffersImpl_m7029 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, RenderBuffer_t1619 * ___color, RenderBuffer_t1619 * ___depth, MethodInfo* method)
{
	typedef void (*Display_GetRenderingBuffersImpl_m7029_ftn) (IntPtr_t, RenderBuffer_t1619 *, RenderBuffer_t1619 *);
	static Display_GetRenderingBuffersImpl_m7029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingBuffersImpl_m7029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(___nativeDisplay, ___color, ___depth);
}
// System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
extern "C" void Display_SetRenderingResolutionImpl_m7030 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___w, int32_t ___h, MethodInfo* method)
{
	typedef void (*Display_SetRenderingResolutionImpl_m7030_ftn) (IntPtr_t, int32_t, int32_t);
	static Display_SetRenderingResolutionImpl_m7030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetRenderingResolutionImpl_m7030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern "C" void Display_ActivateDisplayImpl_m7031 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___refreshRate, MethodInfo* method)
{
	typedef void (*Display_ActivateDisplayImpl_m7031_ftn) (IntPtr_t, int32_t, int32_t, int32_t);
	static Display_ActivateDisplayImpl_m7031_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_ActivateDisplayImpl_m7031_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___refreshRate);
}
// System.Void UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Display_SetParamsImpl_m7032 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, MethodInfo* method)
{
	typedef void (*Display_SetParamsImpl_m7032_ftn) (IntPtr_t, int32_t, int32_t, int32_t, int32_t);
	static Display_SetParamsImpl_m7032_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetParamsImpl_m7032_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___x, ___y);
}
// System.Boolean UnityEngine.Display::MultiDisplayLicenseImpl()
extern "C" bool Display_MultiDisplayLicenseImpl_m7033 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef bool (*Display_MultiDisplayLicenseImpl_m7033_ftn) ();
	static Display_MultiDisplayLicenseImpl_m7033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_MultiDisplayLicenseImpl_m7033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::MultiDisplayLicenseImpl()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C" int32_t Display_RelativeMouseAtImpl_m7034 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, int32_t* ___rx, int32_t* ___ry, MethodInfo* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m7034_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m7034_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m7034_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	return _il2cpp_icall_func(___x, ___y, ___rx, ___ry);
}
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"

// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"


// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" void MonoBehaviour__ctor_m850 (MonoBehaviour_t26 * __this, MethodInfo* method)
{
	{
		Behaviour__ctor_m6970(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" Coroutine_t1268 * MonoBehaviour_StartCoroutine_m968 (MonoBehaviour_t26 * __this, Object_t * ___routine, MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		Coroutine_t1268 * L_1 = MonoBehaviour_StartCoroutine_Auto_m7035(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C" Coroutine_t1268 * MonoBehaviour_StartCoroutine_Auto_m7035 (MonoBehaviour_t26 * __this, Object_t * ___routine, MethodInfo* method)
{
	typedef Coroutine_t1268 * (*MonoBehaviour_StartCoroutine_Auto_m7035_ftn) (MonoBehaviour_t26 *, Object_t *);
	static MonoBehaviour_StartCoroutine_Auto_m7035_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m7035_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C" Coroutine_t1268 * MonoBehaviour_StartCoroutine_m4178 (MonoBehaviour_t26 * __this, String_t* ___methodName, Object_t * ___value, MethodInfo* method)
{
	typedef Coroutine_t1268 * (*MonoBehaviour_StartCoroutine_m4178_ftn) (MonoBehaviour_t26 *, String_t*, Object_t *);
	static MonoBehaviour_StartCoroutine_m4178_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m4178_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName, ___value);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C" Coroutine_t1268 * MonoBehaviour_StartCoroutine_m4065 (MonoBehaviour_t26 * __this, String_t* ___methodName, MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_0;
		Coroutine_t1268 * L_2 = MonoBehaviour_StartCoroutine_m4178(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutine_m7036 (MonoBehaviour_t26 * __this, Object_t * ___routine, MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m7037(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_m6046 (MonoBehaviour_t26 * __this, Coroutine_t1268 * ___routine, MethodInfo* method)
{
	{
		Coroutine_t1268 * L_0 = ___routine;
		MonoBehaviour_StopCoroutine_Auto_m7038(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m7037 (MonoBehaviour_t26 * __this, Object_t * ___routine, MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m7037_ftn) (MonoBehaviour_t26 *, Object_t *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m7037_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m7037_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_Auto_m7038 (MonoBehaviour_t26 * __this, Coroutine_t1268 * ___routine, MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m7038_ftn) (MonoBehaviour_t26 *, Coroutine_t1268 *);
	static MonoBehaviour_StopCoroutine_Auto_m7038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m7038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C" void MonoBehaviour_print_m994 (Object_t * __this /* static, unused */, Object_t * ___message, MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		Debug_Log_m869(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhaseMethodDeclarations.h"



// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionModeMethodDeclarations.h"



// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"

// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"


// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m5784 (Touch_t901 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FingerId_0);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t739  Touch_get_position_m4100 (Touch_t901 * __this, MethodInfo* method)
{
	{
		Vector2_t739  L_0 = (__this->___m_Position_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C" Vector2_t739  Touch_get_deltaPosition_m4099 (Touch_t901 * __this, MethodInfo* method)
{
	{
		Vector2_t739  L_0 = (__this->___m_PositionDelta_3);
		return L_0;
	}
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m4098 (Touch_t901 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Phase_6);
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Touch
void Touch_t901_marshal(const Touch_t901& unmarshaled, Touch_t901_marshaled& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.___m_FingerId_0;
	marshaled.___m_Position_1 = unmarshaled.___m_Position_1;
	marshaled.___m_RawPosition_2 = unmarshaled.___m_RawPosition_2;
	marshaled.___m_PositionDelta_3 = unmarshaled.___m_PositionDelta_3;
	marshaled.___m_TimeDelta_4 = unmarshaled.___m_TimeDelta_4;
	marshaled.___m_TapCount_5 = unmarshaled.___m_TapCount_5;
	marshaled.___m_Phase_6 = unmarshaled.___m_Phase_6;
}
void Touch_t901_marshal_back(const Touch_t901_marshaled& marshaled, Touch_t901& unmarshaled)
{
	unmarshaled.___m_FingerId_0 = marshaled.___m_FingerId_0;
	unmarshaled.___m_Position_1 = marshaled.___m_Position_1;
	unmarshaled.___m_RawPosition_2 = marshaled.___m_RawPosition_2;
	unmarshaled.___m_PositionDelta_3 = marshaled.___m_PositionDelta_3;
	unmarshaled.___m_TimeDelta_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.___m_TapCount_5 = marshaled.___m_TapCount_5;
	unmarshaled.___m_Phase_6 = marshaled.___m_Phase_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
void Touch_t901_marshal_cleanup(Touch_t901_marshaled& marshaled)
{
}
// UnityEngine.Input
#include "UnityEngine_UnityEngine_Input.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"



// System.Void UnityEngine.Input::.cctor()
extern "C" void Input__cctor_m7039 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Input::GetKeyString(System.String)
extern "C" bool Input_GetKeyString_m7040 (Object_t * __this /* static, unused */, String_t* ___name, MethodInfo* method)
{
	typedef bool (*Input_GetKeyString_m7040_ftn) (String_t*);
	static Input_GetKeyString_m7040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyString_m7040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyString(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Boolean UnityEngine.Input::GetKeyDownString(System.String)
extern "C" bool Input_GetKeyDownString_m7041 (Object_t * __this /* static, unused */, String_t* ___name, MethodInfo* method)
{
	typedef bool (*Input_GetKeyDownString_m7041_ftn) (String_t*);
	static Input_GetKeyDownString_m7041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyDownString_m7041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyDownString(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C" float Input_GetAxisRaw_m5814 (Object_t * __this /* static, unused */, String_t* ___axisName, MethodInfo* method)
{
	typedef float (*Input_GetAxisRaw_m5814_ftn) (String_t*);
	static Input_GetAxisRaw_m5814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m5814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	return _il2cpp_icall_func(___axisName);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C" bool Input_GetButtonDown_m4095 (Object_t * __this /* static, unused */, String_t* ___buttonName, MethodInfo* method)
{
	typedef bool (*Input_GetButtonDown_m4095_ftn) (String_t*);
	static Input_GetButtonDown_m4095_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m4095_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	return _il2cpp_icall_func(___buttonName);
}
// System.Boolean UnityEngine.Input::GetKey(System.String)
extern TypeInfo* Input_t987_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKey_m4093 (Object_t * __this /* static, unused */, String_t* ___name, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t987_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyString_m7040(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern TypeInfo* Input_t987_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKeyDown_m4094 (Object_t * __this /* static, unused */, String_t* ___name, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t987_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDownString_m7041(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C" bool Input_GetMouseButton_m5819 (Object_t * __this /* static, unused */, int32_t ___button, MethodInfo* method)
{
	typedef bool (*Input_GetMouseButton_m5819_ftn) (int32_t);
	static Input_GetMouseButton_m5819_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m5819_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" bool Input_GetMouseButtonDown_m5785 (Object_t * __this /* static, unused */, int32_t ___button, MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonDown_m5785_ftn) (int32_t);
	static Input_GetMouseButtonDown_m5785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m5785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C" bool Input_GetMouseButtonUp_m5786 (Object_t * __this /* static, unused */, int32_t ___button, MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonUp_m5786_ftn) (int32_t);
	static Input_GetMouseButtonUp_m5786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m5786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C" Vector3_t758  Input_get_mousePosition_m5787 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef Vector3_t758  (*Input_get_mousePosition_m5787_ftn) ();
	static Input_get_mousePosition_m5787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mousePosition_m5787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mousePosition()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern "C" Vector2_t739  Input_get_mouseScrollDelta_m5788 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef Vector2_t739  (*Input_get_mouseScrollDelta_m5788_ftn) ();
	static Input_get_mouseScrollDelta_m5788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mouseScrollDelta_m5788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mouseScrollDelta()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern TypeInfo* Input_t987_il2cpp_TypeInfo_var;
extern "C" bool Input_get_mousePresent_m5813 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t987_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		bool L_0 = Input_get_touchSupported_m5818(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Touch[] UnityEngine.Input::get_touches()
extern TypeInfo* Input_t987_il2cpp_TypeInfo_var;
extern TypeInfo* TouchU5BU5D_t998_il2cpp_TypeInfo_var;
extern "C" TouchU5BU5D_t998* Input_get_touches_m4279 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t987_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		TouchU5BU5D_t998_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2900);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	TouchU5BU5D_t998* V_1 = {0};
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m4096(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = ((TouchU5BU5D_t998*)SZArrayNew(TouchU5BU5D_t998_il2cpp_TypeInfo_var, L_1));
		V_2 = 0;
		goto IL_002a;
	}

IL_0014:
	{
		TouchU5BU5D_t998* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		Touch_t901  L_5 = Input_GetTouch_m4097(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		*((Touch_t901 *)(Touch_t901 *)SZArrayLdElema(L_2, L_3)) = L_5;
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_7 = V_2;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0014;
		}
	}
	{
		TouchU5BU5D_t998* L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" Touch_t901  Input_GetTouch_m4097 (Object_t * __this /* static, unused */, int32_t ___index, MethodInfo* method)
{
	typedef Touch_t901  (*Input_GetTouch_m4097_ftn) (int32_t);
	static Input_GetTouch_m4097_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetTouch_m4097_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetTouch(System.Int32)");
	return _il2cpp_icall_func(___index);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" int32_t Input_get_touchCount_m4096 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (*Input_get_touchCount_m4096_ftn) ();
	static Input_get_touchCount_m4096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m4096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C" bool Input_get_touchSupported_m5818 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C" void Input_set_imeCompositionMode_m6041 (Object_t * __this /* static, unused */, int32_t ___value, MethodInfo* method)
{
	typedef void (*Input_set_imeCompositionMode_m6041_ftn) (int32_t);
	static Input_set_imeCompositionMode_m6041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m6041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C" String_t* Input_get_compositionString_m5977 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef String_t* (*Input_get_compositionString_m5977_ftn) ();
	static Input_get_compositionString_m5977_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m5977_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_set_compositionCursorPos_m7042 (Object_t * __this /* static, unused */, Vector2_t739 * ___value, MethodInfo* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m7042_ftn) (Vector2_t739 *);
	static Input_INTERNAL_set_compositionCursorPos_m7042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m7042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern TypeInfo* Input_t987_il2cpp_TypeInfo_var;
extern "C" void Input_set_compositionCursorPos_m6030 (Object_t * __this /* static, unused */, Vector2_t739  ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t987_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m7042(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlagsMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"


// System.Void UnityEngine.Object::.ctor()
extern "C" void Object__ctor_m7043 (Object_t187 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C" Object_t187 * Object_Internal_CloneSingle_m7044 (Object_t * __this /* static, unused */, Object_t187 * ___data, MethodInfo* method)
{
	typedef Object_t187 * (*Object_Internal_CloneSingle_m7044_ftn) (Object_t187 *);
	static Object_Internal_CloneSingle_m7044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m7044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" Object_t187 * Object_Internal_InstantiateSingle_m7045 (Object_t * __this /* static, unused */, Object_t187 * ___data, Vector3_t758  ___pos, Quaternion_t771  ___rot, MethodInfo* method)
{
	{
		Object_t187 * L_0 = ___data;
		Object_t187 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m7046(NULL /*static, unused*/, L_0, (&___pos), (&___rot), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t187 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m7046 (Object_t * __this /* static, unused */, Object_t187 * ___data, Vector3_t758 * ___pos, Quaternion_t771 * ___rot, MethodInfo* method)
{
	typedef Object_t187 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m7046_ftn) (Object_t187 *, Vector3_t758 *, Quaternion_t771 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m7046_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m7046_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data, ___pos, ___rot);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" void Object_Destroy_m7047 (Object_t * __this /* static, unused */, Object_t187 * ___obj, float ___t, MethodInfo* method)
{
	typedef void (*Object_Destroy_m7047_ftn) (Object_t187 *, float);
	static Object_Destroy_m7047_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m7047_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj, ___t);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" void Object_Destroy_m855 (Object_t * __this /* static, unused */, Object_t187 * ___obj, MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t187 * L_0 = ___obj;
		float L_1 = V_0;
		Object_Destroy_m7047(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C" void Object_DestroyImmediate_m7048 (Object_t * __this /* static, unused */, Object_t187 * ___obj, bool ___allowDestroyingAssets, MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m7048_ftn) (Object_t187 *, bool);
	static Object_DestroyImmediate_m7048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m7048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj, ___allowDestroyingAssets);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" void Object_DestroyImmediate_m6042 (Object_t * __this /* static, unused */, Object_t187 * ___obj, MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Object_t187 * L_0 = ___obj;
		bool L_1 = V_0;
		Object_DestroyImmediate_m7048(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C" ObjectU5BU5D_t1662* Object_FindObjectsOfType_m7049 (Object_t * __this /* static, unused */, Type_t * ___type, MethodInfo* method)
{
	typedef ObjectU5BU5D_t1662* (*Object_FindObjectsOfType_m7049_ftn) (Type_t *);
	static Object_FindObjectsOfType_m7049_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m7049_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// System.String UnityEngine.Object::get_name()
extern "C" String_t* Object_get_name_m1064 (Object_t187 * __this, MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m1064_ftn) (Object_t187 *);
	static Object_get_name_m1064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m1064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" void Object_set_name_m1062 (Object_t187 * __this, String_t* ___value, MethodInfo* method)
{
	typedef void (*Object_set_name_m1062_ftn) (Object_t187 *, String_t*);
	static Object_set_name_m1062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m1062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" void Object_DontDestroyOnLoad_m854 (Object_t * __this /* static, unused */, Object_t187 * ___target, MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m854_ftn) (Object_t187 *);
	static Object_DontDestroyOnLoad_m854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" void Object_set_hideFlags_m1037 (Object_t187 * __this, int32_t ___value, MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m1037_ftn) (Object_t187 *, int32_t);
	static Object_set_hideFlags_m1037_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m1037_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Object::ToString()
extern "C" String_t* Object_ToString_m1201 (Object_t187 * __this, MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m1201_ftn) (Object_t187 *);
	static Object_ToString_m1201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m1201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern TypeInfo* Object_t187_il2cpp_TypeInfo_var;
extern "C" bool Object_Equals_m1199 (Object_t187 * __this, Object_t * ___o, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2218);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = Object_CompareBaseObjects_m7050(NULL /*static, unused*/, __this, ((Object_t187 *)IsInst(L_0, Object_t187_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C" int32_t Object_GetHashCode_m1200 (Object_t187 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m7052(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_CompareBaseObjects_m7050 (Object_t * __this /* static, unused */, Object_t187 * ___lhs, Object_t187 * ___rhs, MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t187 * L_0 = ___lhs;
		V_0 = ((((Object_t*)(Object_t187 *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		Object_t187 * L_1 = ___rhs;
		V_1 = ((((Object_t*)(Object_t187 *)L_1) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t187 * L_5 = ___lhs;
		bool L_6 = Object_IsNativeObjectAlive_m7051(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t187 * L_8 = ___rhs;
		bool L_9 = Object_IsNativeObjectAlive_m7051(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t187 * L_10 = ___lhs;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___m_InstanceID_0);
		Object_t187 * L_12 = ___rhs;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___m_InstanceID_0);
		return ((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool Object_IsNativeObjectAlive_m7051 (Object_t * __this /* static, unused */, Object_t187 * ___o, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t187 * L_0 = ___o;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m7053(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_3 = IntPtr_op_Inequality_m7511(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C" int32_t Object_GetInstanceID_m7052 (Object_t187 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_InstanceID_0);
		return L_0;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C" IntPtr_t Object_GetCachedPtr_m7053 (Object_t187 * __this, MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_CachedPtr_1);
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" Object_t187 * Object_Instantiate_m4028 (Object_t * __this /* static, unused */, Object_t187 * ___original, Vector3_t758  ___position, Quaternion_t771  ___rotation, MethodInfo* method)
{
	{
		Object_t187 * L_0 = ___original;
		Object_CheckNullArgument_m7054(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral961, /*hidden argument*/NULL);
		Object_t187 * L_1 = ___original;
		Vector3_t758  L_2 = ___position;
		Quaternion_t771  L_3 = ___rotation;
		Object_t187 * L_4 = Object_Internal_InstantiateSingle_m7045(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern TypeInfo* ArgumentException_t1409_il2cpp_TypeInfo_var;
extern "C" void Object_CheckNullArgument_m7054 (Object_t * __this /* static, unused */, Object_t * ___arg, String_t* ___message, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___arg;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message;
		ArgumentException_t1409 * L_2 = (ArgumentException_t1409 *)il2cpp_codegen_object_new (ArgumentException_t1409_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m6124(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C" Object_t187 * Object_FindObjectOfType_m1060 (Object_t * __this /* static, unused */, Type_t * ___type, MethodInfo* method)
{
	ObjectU5BU5D_t1662* V_0 = {0};
	{
		Type_t * L_0 = ___type;
		ObjectU5BU5D_t1662* L_1 = Object_FindObjectsOfType_m7049(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t1662* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t1662* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		return (*(Object_t187 **)(Object_t187 **)SZArrayLdElema(L_3, L_4));
	}

IL_0014:
	{
		return (Object_t187 *)NULL;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" bool Object_op_Implicit_m1059 (Object_t * __this /* static, unused */, Object_t187 * ___exists, MethodInfo* method)
{
	{
		Object_t187 * L_0 = ___exists;
		bool L_1 = Object_CompareBaseObjects_m7050(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Equality_m848 (Object_t * __this /* static, unused */, Object_t187 * ___x, Object_t187 * ___y, MethodInfo* method)
{
	{
		Object_t187 * L_0 = ___x;
		Object_t187 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m7050(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Inequality_m3719 (Object_t * __this /* static, unused */, Object_t187 * ___x, Object_t187 * ___y, MethodInfo* method)
{
	{
		Object_t187 * L_0 = ___x;
		Object_t187 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m7050(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
void Object_t187_marshal(const Object_t187& unmarshaled, Object_t187_marshaled& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.___m_InstanceID_0;
	marshaled.___m_CachedPtr_1 = unmarshaled.___m_CachedPtr_1;
}
void Object_t187_marshal_back(const Object_t187_marshaled& marshaled, Object_t187& unmarshaled)
{
	unmarshaled.___m_InstanceID_0 = marshaled.___m_InstanceID_0;
	unmarshaled.___m_CachedPtr_1 = marshaled.___m_CachedPtr_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
void Object_t187_marshal_cleanup(Object_t187_marshaled& marshaled)
{
}
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_32.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"


// System.Void UnityEngine.Component::.ctor()
extern "C" void Component__ctor_m7055 (Component_t230 * __this, MethodInfo* method)
{
	{
		Object__ctor_m7043(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t809 * Component_get_transform_m4030 (Component_t230 * __this, MethodInfo* method)
{
	typedef Transform_t809 * (*Component_get_transform_m4030_ftn) (Component_t230 *);
	static Component_get_transform_m4030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m4030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t144 * Component_get_gameObject_m853 (Component_t230 * __this, MethodInfo* method)
{
	typedef GameObject_t144 * (*Component_get_gameObject_m853_ftn) (Component_t230 *);
	static Component_get_gameObject_m853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C" Component_t230 * Component_GetComponent_m6155 (Component_t230 * __this, Type_t * ___type, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		Component_t230 * L_2 = GameObject_GetComponent_m7059(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void Component_GetComponentFastPath_m7056 (Component_t230 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m7056_ftn) (Component_t230 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m7056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m7056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C" Component_t230 * Component_GetComponentInChildren_m7057 (Component_t230 * __this, Type_t * ___t, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t230 * L_2 = GameObject_GetComponentInChildren_m7061(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C" void Component_GetComponentsForListInternal_m7058 (Component_t230 * __this, Type_t * ___searchType, Object_t * ___resultList, MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m7058_ftn) (Component_t230 *, Type_t *, Object_t *);
	static Component_GetComponentsForListInternal_m7058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m7058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType, ___resultList);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C" void Component_GetComponents_m5872 (Component_t230 * __this, Type_t * ___type, List_1_t1366 * ___results, MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		List_1_t1366 * L_1 = ___results;
		Component_GetComponentsForListInternal_m7058(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Component::get_tag()
extern "C" String_t* Component_get_tag_m4162 (Component_t230 * __this, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m4048(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"


// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" void GameObject__ctor_m1036 (GameObject_t144 * __this, String_t* ___name, MethodInfo* method)
{
	{
		Object__ctor_m7043(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m7066(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C" void GameObject__ctor_m1061 (GameObject_t144 * __this, MethodInfo* method)
{
	{
		Object__ctor_m7043(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m7066(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" Component_t230 * GameObject_GetComponent_m7059 (GameObject_t144 * __this, Type_t * ___type, MethodInfo* method)
{
	typedef Component_t230 * (*GameObject_GetComponent_m7059_ftn) (GameObject_t144 *, Type_t *);
	static GameObject_GetComponent_m7059_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m7059_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void GameObject_GetComponentFastPath_m7060 (GameObject_t144 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, MethodInfo* method)
{
	typedef void (*GameObject_GetComponentFastPath_m7060_ftn) (GameObject_t144 *, Type_t *, IntPtr_t);
	static GameObject_GetComponentFastPath_m7060_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m7060_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t809_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" Component_t230 * GameObject_GetComponentInChildren_m7061 (GameObject_t144 * __this, Type_t * ___type, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Transform_t809_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	Component_t230 * V_0 = {0};
	Transform_t809 * V_1 = {0};
	Transform_t809 * V_2 = {0};
	Object_t * V_3 = {0};
	Component_t230 * V_4 = {0};
	Component_t230 * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameObject_get_activeInHierarchy_m4271(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t230 * L_2 = GameObject_GetComponent_m7059(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t230 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_3, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t230 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t809 * L_6 = GameObject_get_transform_m4029(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		Transform_t809 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_7, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0095;
		}
	}
	{
		Transform_t809 * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_9);
		V_3 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_0040:
		{
			Object_t * L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_11);
			V_2 = ((Transform_t809 *)Castclass(L_12, Transform_t809_il2cpp_TypeInfo_var));
			Transform_t809 * L_13 = V_2;
			NullCheck(L_13);
			GameObject_t144 * L_14 = Component_get_gameObject_m853(L_13, /*hidden argument*/NULL);
			Type_t * L_15 = ___type;
			NullCheck(L_14);
			Component_t230 * L_16 = GameObject_GetComponentInChildren_m7061(L_14, L_15, /*hidden argument*/NULL);
			V_4 = L_16;
			Component_t230 * L_17 = V_4;
			bool L_18 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_17, (Object_t187 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0070;
			}
		}

IL_0067:
		{
			Component_t230 * L_19 = V_4;
			V_5 = L_19;
			IL2CPP_LEAVE(0x97, FINALLY_0080);
		}

IL_0070:
		{
			Object_t * L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0040;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Object_t * L_22 = V_3;
			V_6 = ((Object_t *)IsInst(L_22, IDisposable_t191_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_6;
			if (L_23)
			{
				goto IL_008d;
			}
		}

IL_008c:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_008d:
		{
			Object_t * L_24 = V_6;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0095:
	{
		return (Component_t230 *)NULL;
	}

IL_0097:
	{
		Component_t230 * L_25 = V_5;
		return L_25;
	}
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C" Array_t * GameObject_GetComponentsInternal_m7062 (GameObject_t144 * __this, Type_t * ___type, bool ___useSearchTypeAsArrayReturnType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, MethodInfo* method)
{
	typedef Array_t * (*GameObject_GetComponentsInternal_m7062_ftn) (GameObject_t144 *, Type_t *, bool, bool, bool, bool, Object_t *);
	static GameObject_GetComponentsInternal_m7062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m7062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, ___type, ___useSearchTypeAsArrayReturnType, ___recursive, ___includeInactive, ___reverse, ___resultList);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t809 * GameObject_get_transform_m4029 (GameObject_t144 * __this, MethodInfo* method)
{
	typedef Transform_t809 * (*GameObject_get_transform_m4029_ftn) (GameObject_t144 *);
	static GameObject_get_transform_m4029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m4029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" int32_t GameObject_get_layer_m6016 (GameObject_t144 * __this, MethodInfo* method)
{
	typedef int32_t (*GameObject_get_layer_m6016_ftn) (GameObject_t144 *);
	static GameObject_get_layer_m6016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m6016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C" void GameObject_set_layer_m6017 (GameObject_t144 * __this, int32_t ___value, MethodInfo* method)
{
	typedef void (*GameObject_set_layer_m6017_ftn) (GameObject_t144 *, int32_t);
	static GameObject_set_layer_m6017_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m6017_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m4272 (GameObject_t144 * __this, bool ___value, MethodInfo* method)
{
	typedef void (*GameObject_SetActive_m4272_ftn) (GameObject_t144 *, bool);
	static GameObject_SetActive_m4272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m4272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m4271 (GameObject_t144 * __this, MethodInfo* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m4271_ftn) (GameObject_t144 *);
	static GameObject_get_activeInHierarchy_m4271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m4271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.GameObject::get_tag()
extern "C" String_t* GameObject_get_tag_m4048 (GameObject_t144 * __this, MethodInfo* method)
{
	typedef String_t* (*GameObject_get_tag_m4048_ftn) (GameObject_t144 *);
	static GameObject_get_tag_m4048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_tag_m4048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_tag()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C" GameObject_t144 * GameObject_FindGameObjectWithTag_m4082 (Object_t * __this /* static, unused */, String_t* ___tag, MethodInfo* method)
{
	typedef GameObject_t144 * (*GameObject_FindGameObjectWithTag_m4082_ftn) (String_t*);
	static GameObject_FindGameObjectWithTag_m4082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_FindGameObjectWithTag_m4082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::FindGameObjectWithTag(System.String)");
	return _il2cpp_icall_func(___tag);
}
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" GameObjectU5BU5D_t977* GameObject_FindGameObjectsWithTag_m4018 (Object_t * __this /* static, unused */, String_t* ___tag, MethodInfo* method)
{
	typedef GameObjectU5BU5D_t977* (*GameObject_FindGameObjectsWithTag_m4018_ftn) (String_t*);
	static GameObject_FindGameObjectsWithTag_m4018_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_FindGameObjectsWithTag_m4018_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::FindGameObjectsWithTag(System.String)");
	return _il2cpp_icall_func(___tag);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m7063 (GameObject_t144 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m7063_ftn) (GameObject_t144 *, String_t*, Object_t *, int32_t);
	static GameObject_SendMessage_m7063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m7063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t230 * GameObject_Internal_AddComponentWithType_m7064 (GameObject_t144 * __this, Type_t * ___componentType, MethodInfo* method)
{
	typedef Component_t230 * (*GameObject_Internal_AddComponentWithType_m7064_ftn) (GameObject_t144 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m7064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m7064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, ___componentType);
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t230 * GameObject_AddComponent_m7065 (GameObject_t144 * __this, Type_t * ___componentType, MethodInfo* method)
{
	{
		Type_t * L_0 = ___componentType;
		Component_t230 * L_1 = GameObject_Internal_AddComponentWithType_m7064(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C" void GameObject_Internal_CreateGameObject_m7066 (Object_t * __this /* static, unused */, GameObject_t144 * ___mono, String_t* ___name, MethodInfo* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m7066_ftn) (GameObject_t144 *, String_t*);
	static GameObject_Internal_CreateGameObject_m7066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m7066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono, ___name);
}
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"



// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C" void Enumerator__ctor_m7067 (Enumerator_t1564 * __this, Transform_t809 * ___outer, MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		Transform_t809 * L_0 = ___outer;
		__this->___outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m7068 (Enumerator_t1564 * __this, MethodInfo* method)
{
	{
		Transform_t809 * L_0 = (__this->___outer_0);
		int32_t L_1 = (__this->___currentIndex_1);
		NullCheck(L_0);
		Transform_t809 * L_2 = Transform_GetChild_m4050(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m7069 (Enumerator_t1564 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t809 * L_0 = (__this->___outer_0);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m4051(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___currentIndex_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->___currentIndex_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return ((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"


// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_position_m7070 (Transform_t809 * __this, Vector3_t758 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m7070_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_get_position_m7070_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m7070_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_position_m7071 (Transform_t809 * __this, Vector3_t758 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m7071_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_set_position_m7071_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m7071_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" Vector3_t758  Transform_get_position_m4034 (Transform_t809 * __this, MethodInfo* method)
{
	Vector3_t758  V_0 = {0};
	{
		Transform_INTERNAL_get_position_m7070(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t758  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" void Transform_set_position_m4039 (Transform_t809 * __this, Vector3_t758  ___value, MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m7071(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localPosition_m7072 (Transform_t809 * __this, Vector3_t758 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m7072_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_get_localPosition_m7072_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m7072_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localPosition_m7073 (Transform_t809 * __this, Vector3_t758 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m7073_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_set_localPosition_m7073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m7073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" Vector3_t758  Transform_get_localPosition_m4138 (Transform_t809 * __this, MethodInfo* method)
{
	Vector3_t758  V_0 = {0};
	{
		Transform_INTERNAL_get_localPosition_m7072(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t758  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" void Transform_set_localPosition_m4193 (Transform_t809 * __this, Vector3_t758  ___value, MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m7073(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C" Vector3_t758  Transform_get_right_m4133 (Transform_t809 * __this, MethodInfo* method)
{
	{
		Quaternion_t771  L_0 = Transform_get_rotation_m4131(__this, /*hidden argument*/NULL);
		Vector3_t758  L_1 = Vector3_get_right_m6095(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t758  L_2 = Quaternion_op_Multiply_m5902(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C" Vector3_t758  Transform_get_forward_m5904 (Transform_t809 * __this, MethodInfo* method)
{
	{
		Quaternion_t771  L_0 = Transform_get_rotation_m4131(__this, /*hidden argument*/NULL);
		Vector3_t758  L_1 = Vector3_get_forward_m4179(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t758  L_2 = Quaternion_op_Multiply_m5902(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_rotation_m7074 (Transform_t809 * __this, Quaternion_t771 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m7074_ftn) (Transform_t809 *, Quaternion_t771 *);
	static Transform_INTERNAL_get_rotation_m7074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m7074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_rotation_m7075 (Transform_t809 * __this, Quaternion_t771 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m7075_ftn) (Transform_t809 *, Quaternion_t771 *);
	static Transform_INTERNAL_set_rotation_m7075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m7075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" Quaternion_t771  Transform_get_rotation_m4131 (Transform_t809 * __this, MethodInfo* method)
{
	Quaternion_t771  V_0 = {0};
	{
		Transform_INTERNAL_get_rotation_m7074(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t771  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" void Transform_set_rotation_m4171 (Transform_t809 * __this, Quaternion_t771  ___value, MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m7075(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_localRotation_m7076 (Transform_t809 * __this, Quaternion_t771 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m7076_ftn) (Transform_t809 *, Quaternion_t771 *);
	static Transform_INTERNAL_get_localRotation_m7076_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m7076_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_localRotation_m7077 (Transform_t809 * __this, Quaternion_t771 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m7077_ftn) (Transform_t809 *, Quaternion_t771 *);
	static Transform_INTERNAL_set_localRotation_m7077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m7077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" Quaternion_t771  Transform_get_localRotation_m6019 (Transform_t809 * __this, MethodInfo* method)
{
	Quaternion_t771  V_0 = {0};
	{
		Transform_INTERNAL_get_localRotation_m7076(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t771  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" void Transform_set_localRotation_m4270 (Transform_t809 * __this, Quaternion_t771  ___value, MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m7077(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localScale_m7078 (Transform_t809 * __this, Vector3_t758 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m7078_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_get_localScale_m7078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m7078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localScale_m7079 (Transform_t809 * __this, Vector3_t758 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m7079_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_set_localScale_m7079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m7079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" Vector3_t758  Transform_get_localScale_m4158 (Transform_t809 * __this, MethodInfo* method)
{
	Vector3_t758  V_0 = {0};
	{
		Transform_INTERNAL_get_localScale_m7078(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t758  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" void Transform_set_localScale_m4159 (Transform_t809 * __this, Vector3_t758  ___value, MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m7079(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" Transform_t809 * Transform_get_parent_m4132 (Transform_t809 * __this, MethodInfo* method)
{
	{
		Transform_t809 * L_0 = Transform_get_parentInternal_m7080(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern TypeInfo* RectTransform_t1227_il2cpp_TypeInfo_var;
extern "C" void Transform_set_parent_m4031 (Transform_t809 * __this, Transform_t809 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2237);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t1227 *)IsInst(__this, RectTransform_t1227_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogWarning_m6105(NULL /*static, unused*/, (String_t*) &_stringLiteral962, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t809 * L_0 = ___value;
		Transform_set_parentInternal_m7081(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C" Transform_t809 * Transform_get_parentInternal_m7080 (Transform_t809 * __this, MethodInfo* method)
{
	typedef Transform_t809 * (*Transform_get_parentInternal_m7080_ftn) (Transform_t809 *);
	static Transform_get_parentInternal_m7080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m7080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C" void Transform_set_parentInternal_m7081 (Transform_t809 * __this, Transform_t809 * ___value, MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m7081_ftn) (Transform_t809 *, Transform_t809 *);
	static Transform_set_parentInternal_m7081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m7081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" void Transform_SetParent_m6014 (Transform_t809 * __this, Transform_t809 * ___parent, MethodInfo* method)
{
	{
		Transform_t809 * L_0 = ___parent;
		Transform_SetParent_m7082(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C" void Transform_SetParent_m7082 (Transform_t809 * __this, Transform_t809 * ___parent, bool ___worldPositionStays, MethodInfo* method)
{
	typedef void (*Transform_SetParent_m7082_ftn) (Transform_t809 *, Transform_t809 *, bool);
	static Transform_SetParent_m7082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m7082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent, ___worldPositionStays);
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m7083 (Transform_t809 * __this, Matrix4x4_t997 * ___value, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m7083_ftn) (Transform_t809 *, Matrix4x4_t997 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m7083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m7083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C" Matrix4x4_t997  Transform_get_worldToLocalMatrix_m6071 (Transform_t809 * __this, MethodInfo* method)
{
	Matrix4x4_t997  V_0 = {0};
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m7083(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t997  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C" void Transform_Translate_m4201 (Transform_t809 * __this, Vector3_t758  ___translation, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		Vector3_t758  L_0 = ___translation;
		int32_t L_1 = V_0;
		Transform_Translate_m7084(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Translate_m7084 (Transform_t809 * __this, Vector3_t758  ___translation, int32_t ___relativeTo, MethodInfo* method)
{
	{
		int32_t L_0 = ___relativeTo;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t758  L_1 = Transform_get_position_m4034(__this, /*hidden argument*/NULL);
		Vector3_t758  L_2 = ___translation;
		Vector3_t758  L_3 = Vector3_op_Addition_m4278(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m4039(__this, L_3, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_001d:
	{
		Vector3_t758  L_4 = Transform_get_position_m4034(__this, /*hidden argument*/NULL);
		Vector3_t758  L_5 = ___translation;
		Vector3_t758  L_6 = Transform_TransformDirection_m7089(__this, L_5, /*hidden argument*/NULL);
		Vector3_t758  L_7 = Vector3_op_Addition_m4278(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m4039(__this, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern "C" void Transform_Translate_m4047 (Transform_t809 * __this, float ___x, float ___y, float ___z, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		int32_t L_3 = V_0;
		Transform_Translate_m7085(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C" void Transform_Translate_m7085 (Transform_t809 * __this, float ___x, float ___y, float ___z, int32_t ___relativeTo, MethodInfo* method)
{
	{
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		Vector3_t758  L_3 = {0};
		Vector3__ctor_m4026(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo;
		Transform_Translate_m7084(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C" void Transform_Rotate_m4259 (Transform_t809 * __this, Vector3_t758  ___eulerAngles, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		Vector3_t758  L_0 = ___eulerAngles;
		int32_t L_1 = V_0;
		Transform_Rotate_m7086(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Rotate_m7086 (Transform_t809 * __this, Vector3_t758  ___eulerAngles, int32_t ___relativeTo, MethodInfo* method)
{
	Quaternion_t771  V_0 = {0};
	{
		float L_0 = ((&___eulerAngles)->___x_1);
		float L_1 = ((&___eulerAngles)->___y_2);
		float L_2 = ((&___eulerAngles)->___z_3);
		Quaternion_t771  L_3 = Quaternion_Euler_m4190(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0039;
		}
	}
	{
		Quaternion_t771  L_5 = Transform_get_localRotation_m6019(__this, /*hidden argument*/NULL);
		Quaternion_t771  L_6 = V_0;
		Quaternion_t771  L_7 = Quaternion_op_Multiply_m4191(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m4270(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0039:
	{
		Quaternion_t771  L_8 = Transform_get_rotation_m4131(__this, /*hidden argument*/NULL);
		Quaternion_t771  L_9 = Transform_get_rotation_m4131(__this, /*hidden argument*/NULL);
		Quaternion_t771  L_10 = Quaternion_Inverse_m6090(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t771  L_11 = V_0;
		Quaternion_t771  L_12 = Quaternion_op_Multiply_m4191(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t771  L_13 = Transform_get_rotation_m4131(__this, /*hidden argument*/NULL);
		Quaternion_t771  L_14 = Quaternion_op_Multiply_m4191(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t771  L_15 = Quaternion_op_Multiply_m4191(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m4171(__this, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::RotateAroundInternal(UnityEngine.Vector3,System.Single)
extern "C" void Transform_RotateAroundInternal_m7087 (Transform_t809 * __this, Vector3_t758  ___axis, float ___angle, MethodInfo* method)
{
	{
		float L_0 = ___angle;
		Transform_INTERNAL_CALL_RotateAroundInternal_m7088(NULL /*static, unused*/, __this, (&___axis), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)
extern "C" void Transform_INTERNAL_CALL_RotateAroundInternal_m7088 (Object_t * __this /* static, unused */, Transform_t809 * ___self, Vector3_t758 * ___axis, float ___angle, MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_RotateAroundInternal_m7088_ftn) (Transform_t809 *, Vector3_t758 *, float);
	static Transform_INTERNAL_CALL_RotateAroundInternal_m7088_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_RotateAroundInternal_m7088_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___self, ___axis, ___angle);
}
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" void Transform_RotateAround_m4180 (Transform_t809 * __this, Vector3_t758  ___point, Vector3_t758  ___axis, float ___angle, MethodInfo* method)
{
	Vector3_t758  V_0 = {0};
	Quaternion_t771  V_1 = {0};
	Vector3_t758  V_2 = {0};
	{
		Vector3_t758  L_0 = Transform_get_position_m4034(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___angle;
		Vector3_t758  L_2 = ___axis;
		Quaternion_t771  L_3 = Quaternion_AngleAxis_m6736(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t758  L_4 = V_0;
		Vector3_t758  L_5 = ___point;
		Vector3_t758  L_6 = Vector3_op_Subtraction_m4167(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Quaternion_t771  L_7 = V_1;
		Vector3_t758  L_8 = V_2;
		Vector3_t758  L_9 = Quaternion_op_Multiply_m5902(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector3_t758  L_10 = ___point;
		Vector3_t758  L_11 = V_2;
		Vector3_t758  L_12 = Vector3_op_Addition_m4278(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Vector3_t758  L_13 = V_0;
		Transform_set_position_m4039(__this, L_13, /*hidden argument*/NULL);
		Vector3_t758  L_14 = ___axis;
		float L_15 = ___angle;
		Transform_RotateAroundInternal_m7087(__this, L_14, ((float)((float)L_15*(float)(0.0174532924f))), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C" Vector3_t758  Transform_TransformDirection_m7089 (Transform_t809 * __this, Vector3_t758  ___direction, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Transform_INTERNAL_CALL_TransformDirection_m7090(NULL /*static, unused*/, __this, (&___direction), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t758  Transform_INTERNAL_CALL_TransformDirection_m7090 (Object_t * __this /* static, unused */, Transform_t809 * ___self, Vector3_t758 * ___direction, MethodInfo* method)
{
	typedef Vector3_t758  (*Transform_INTERNAL_CALL_TransformDirection_m7090_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_CALL_TransformDirection_m7090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m7090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___direction);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t758  Transform_TransformPoint_m6091 (Transform_t809 * __this, Vector3_t758  ___position, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Transform_INTERNAL_CALL_TransformPoint_m7091(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t758  Transform_INTERNAL_CALL_TransformPoint_m7091 (Object_t * __this /* static, unused */, Transform_t809 * ___self, Vector3_t758 * ___position, MethodInfo* method)
{
	typedef Vector3_t758  (*Transform_INTERNAL_CALL_TransformPoint_m7091_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_CALL_TransformPoint_m7091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m7091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t758  Transform_InverseTransformPoint_m5988 (Transform_t809 * __this, Vector3_t758  ___position, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Transform_INTERNAL_CALL_InverseTransformPoint_m7092(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t758  Transform_INTERNAL_CALL_InverseTransformPoint_m7092 (Object_t * __this /* static, unused */, Transform_t809 * ___self, Vector3_t758 * ___position, MethodInfo* method)
{
	typedef Vector3_t758  (*Transform_INTERNAL_CALL_InverseTransformPoint_m7092_ftn) (Transform_t809 *, Vector3_t758 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m7092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m7092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" int32_t Transform_get_childCount_m4051 (Transform_t809 * __this, MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m4051_ftn) (Transform_t809 *);
	static Transform_get_childCount_m4051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m4051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C" void Transform_SetAsFirstSibling_m6015 (Transform_t809 * __this, MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m6015_ftn) (Transform_t809 *);
	static Transform_SetAsFirstSibling_m6015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m6015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern TypeInfo* Enumerator_t1564_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_GetEnumerator_m7093 (Transform_t809 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1564_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2901);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1564 * L_0 = (Enumerator_t1564 *)il2cpp_codegen_object_new (Enumerator_t1564_il2cpp_TypeInfo_var);
		Enumerator__ctor_m7067(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" Transform_t809 * Transform_GetChild_m4050 (Transform_t809 * __this, int32_t ___index, MethodInfo* method)
{
	typedef Transform_t809 * (*Transform_GetChild_m4050_ftn) (Transform_t809 *, int32_t);
	static Transform_GetChild_m4050_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m4050_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// UnityEngine.Time
#include "UnityEngine_UnityEngine_Time.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"



// System.Single UnityEngine.Time::get_deltaTime()
extern "C" float Time_get_deltaTime_m4036 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m4036_ftn) ();
	static Time_get_deltaTime_m4036_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m4036_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C" float Time_get_unscaledTime_m5816 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m5816_ftn) ();
	static Time_get_unscaledTime_m5816_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m5816_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C" float Time_get_unscaledDeltaTime_m5846 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m5846_ftn) ();
	static Time_get_unscaledDeltaTime_m5846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m5846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C" float Time_get_timeScale_m4203 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef float (*Time_get_timeScale_m4203_ftn) ();
	static Time_get_timeScale_m4203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m4203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C" void Time_set_timeScale_m4091 (Object_t * __this /* static, unused */, float ___value, MethodInfo* method)
{
	typedef void (*Time_set_timeScale_m4091_ftn) (float);
	static Time_set_timeScale_m4091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_timeScale_m4091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_timeScale(System.Single)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.Random
#include "UnityEngine_UnityEngine_Random.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"



// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" float Random_Range_m4128 (Object_t * __this /* static, unused */, float ___min, float ___max, MethodInfo* method)
{
	typedef float (*Random_Range_m4128_ftn) (float, float);
	static Random_Range_m4128_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m4128_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" int32_t Random_Range_m4046 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, MethodInfo* method)
{
	{
		int32_t L_0 = ___min;
		int32_t L_1 = ___max;
		int32_t L_2 = Random_RandomRangeInt_m7094(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C" int32_t Random_RandomRangeInt_m7094 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m7094_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m7094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m7094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min, ___max);
}
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"



// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m7095 (YieldInstruction_t1493 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
void YieldInstruction_t1493_marshal(const YieldInstruction_t1493& unmarshaled, YieldInstruction_t1493_marshaled& marshaled)
{
}
void YieldInstruction_t1493_marshal_back(const YieldInstruction_t1493_marshaled& marshaled, YieldInstruction_t1493& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
void YieldInstruction_t1493_marshal_cleanup(YieldInstruction_t1493_marshaled& marshaled)
{
}
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"

// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"


// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C" void PlayerPrefsException__ctor_m7096 (PlayerPrefsException_t1567 * __this, String_t* ___error, MethodInfo* method)
{
	{
		String_t* L_0 = ___error;
		Exception__ctor_m1031(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"



// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C" bool PlayerPrefs_TrySetInt_m7097 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m7097_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m7097_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m7097_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern TypeInfo* PlayerPrefsException_t1567_il2cpp_TypeInfo_var;
extern "C" void PlayerPrefs_SetInt_m4250 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t1567_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2902);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		int32_t L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetInt_m7097(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t1567 * L_3 = (PlayerPrefsException_t1567 *)il2cpp_codegen_object_new (PlayerPrefsException_t1567_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m7096(L_3, (String_t*) &_stringLiteral963, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" int32_t PlayerPrefs_GetInt_m7098 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___defaultValue, MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m7098_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m7098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m7098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C" int32_t PlayerPrefs_GetInt_m4252 (Object_t * __this /* static, unused */, String_t* ___key, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___key;
		int32_t L_1 = V_0;
		int32_t L_2 = PlayerPrefs_GetInt_m7098(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C" bool PlayerPrefs_HasKey_m4251 (Object_t * __this /* static, unused */, String_t* ___key, MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m4251_ftn) (String_t*);
	static PlayerPrefs_HasKey_m4251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m4251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key);
}
// System.Void UnityEngine.PlayerPrefs::DeleteAll()
extern "C" void PlayerPrefs_DeleteAll_m4253 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteAll_m4253_ftn) ();
	static PlayerPrefs_DeleteAll_m4253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteAll_m4253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteAll()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C" void PlayerPrefs_Save_m4254 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef void (*PlayerPrefs_Save_m4254_ftn) ();
	static PlayerPrefs_Save_m4254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_Save_m4254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::Save()");
	_il2cpp_icall_func();
}
// UnityEngine.iOS.ADBannerView/Layout
#include "UnityEngine_UnityEngine_iOS_ADBannerView_Layout.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.ADBannerView/Layout
#include "UnityEngine_UnityEngine_iOS_ADBannerView_LayoutMethodDeclarations.h"



// UnityEngine.iOS.ADBannerView/Type
#include "UnityEngine_UnityEngine_iOS_ADBannerView_Type.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.ADBannerView/Type
#include "UnityEngine_UnityEngine_iOS_ADBannerView_TypeMethodDeclarations.h"



// UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWasClickedDel.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWasClickedDelMethodDeclarations.h"



// System.Void UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void BannerWasClickedDelegate__ctor_m7099 (BannerWasClickedDelegate_t1571 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::Invoke()
extern "C" void BannerWasClickedDelegate_Invoke_m7100 (BannerWasClickedDelegate_t1571 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		BannerWasClickedDelegate_Invoke_m7100((BannerWasClickedDelegate_t1571 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_BannerWasClickedDelegate_t1571(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * BannerWasClickedDelegate_BeginInvoke_m7101 (BannerWasClickedDelegate_t1571 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void BannerWasClickedDelegate_EndInvoke_m7102 (BannerWasClickedDelegate_t1571 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWasLoadedDele.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWasLoadedDeleMethodDeclarations.h"



// System.Void UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void BannerWasLoadedDelegate__ctor_m4009 (BannerWasLoadedDelegate_t974 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::Invoke()
extern "C" void BannerWasLoadedDelegate_Invoke_m7103 (BannerWasLoadedDelegate_t974 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		BannerWasLoadedDelegate_Invoke_m7103((BannerWasLoadedDelegate_t974 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_BannerWasLoadedDelegate_t974(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * BannerWasLoadedDelegate_BeginInvoke_m7104 (BannerWasLoadedDelegate_t974 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void BannerWasLoadedDelegate_EndInvoke_m7105 (BannerWasLoadedDelegate_t974 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.iOS.ADBannerView
#include "UnityEngine_UnityEngine_iOS_ADBannerView.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.ADBannerView
#include "UnityEngine_UnityEngine_iOS_ADBannerViewMethodDeclarations.h"



// System.Void UnityEngine.iOS.ADBannerView::.ctor(UnityEngine.iOS.ADBannerView/Type,UnityEngine.iOS.ADBannerView/Layout)
extern TypeInfo* ADBannerView_t717_il2cpp_TypeInfo_var;
extern "C" void ADBannerView__ctor_m4008 (ADBannerView_t717 * __this, int32_t ___type, int32_t ___layout, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADBannerView_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(973);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		bool L_0 = ((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->____AlwaysFalseDummy_2;
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		ADBannerView_FireBannerWasClicked_m7111(NULL /*static, unused*/, /*hidden argument*/NULL);
		ADBannerView_FireBannerWasLoaded_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001a:
	{
		int32_t L_1 = ___type;
		int32_t L_2 = ___layout;
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = ADBannerView_Native_CreateBanner_m7108(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->____bannerView_1 = L_3;
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::.cctor()
extern "C" void ADBannerView__cctor_m7106 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::add_onBannerWasLoaded(UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate)
extern TypeInfo* ADBannerView_t717_il2cpp_TypeInfo_var;
extern TypeInfo* BannerWasLoadedDelegate_t974_il2cpp_TypeInfo_var;
extern "C" void ADBannerView_add_onBannerWasLoaded_m4010 (Object_t * __this /* static, unused */, BannerWasLoadedDelegate_t974 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADBannerView_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(973);
		BannerWasLoadedDelegate_t974_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(975);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		BannerWasLoadedDelegate_t974 * L_0 = ((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasLoaded_4;
		BannerWasLoadedDelegate_t974 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Combine_m952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasLoaded_4 = ((BannerWasLoadedDelegate_t974 *)Castclass(L_2, BannerWasLoadedDelegate_t974_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::remove_onBannerWasLoaded(UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate)
extern TypeInfo* ADBannerView_t717_il2cpp_TypeInfo_var;
extern TypeInfo* BannerWasLoadedDelegate_t974_il2cpp_TypeInfo_var;
extern "C" void ADBannerView_remove_onBannerWasLoaded_m7107 (Object_t * __this /* static, unused */, BannerWasLoadedDelegate_t974 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADBannerView_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(973);
		BannerWasLoadedDelegate_t974_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(975);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		BannerWasLoadedDelegate_t974 * L_0 = ((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasLoaded_4;
		BannerWasLoadedDelegate_t974 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Remove_m5890(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasLoaded_4 = ((BannerWasLoadedDelegate_t974 *)Castclass(L_2, BannerWasLoadedDelegate_t974_il2cpp_TypeInfo_var));
		return;
	}
}
// System.IntPtr UnityEngine.iOS.ADBannerView::Native_CreateBanner(System.Int32,System.Int32)
extern "C" IntPtr_t ADBannerView_Native_CreateBanner_m7108 (Object_t * __this /* static, unused */, int32_t ___type, int32_t ___layout, MethodInfo* method)
{
	typedef IntPtr_t (*ADBannerView_Native_CreateBanner_m7108_ftn) (int32_t, int32_t);
	static ADBannerView_Native_CreateBanner_m7108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADBannerView_Native_CreateBanner_m7108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADBannerView::Native_CreateBanner(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___type, ___layout);
}
// System.Void UnityEngine.iOS.ADBannerView::Native_DestroyBanner(System.IntPtr)
extern "C" void ADBannerView_Native_DestroyBanner_m7109 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method)
{
	typedef void (*ADBannerView_Native_DestroyBanner_m7109_ftn) (IntPtr_t);
	static ADBannerView_Native_DestroyBanner_m7109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADBannerView_Native_DestroyBanner_m7109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADBannerView::Native_DestroyBanner(System.IntPtr)");
	_il2cpp_icall_func(___view);
}
// System.Void UnityEngine.iOS.ADBannerView::Finalize()
extern TypeInfo* ADBannerView_t717_il2cpp_TypeInfo_var;
extern "C" void ADBannerView_Finalize_m7110 (ADBannerView_t717 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADBannerView_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(973);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0 = (__this->____bannerView_1);
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		ADBannerView_Native_DestroyBanner_m7109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		Object_Finalize_m1177(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::FireBannerWasClicked()
extern TypeInfo* ADBannerView_t717_il2cpp_TypeInfo_var;
extern "C" void ADBannerView_FireBannerWasClicked_m7111 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADBannerView_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(973);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		BannerWasClickedDelegate_t1571 * L_0 = ((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasClicked_3;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		BannerWasClickedDelegate_t1571 * L_1 = ((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasClicked_3;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(10 /* System.Void UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::Invoke() */, L_1);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::FireBannerWasLoaded()
extern TypeInfo* ADBannerView_t717_il2cpp_TypeInfo_var;
extern "C" void ADBannerView_FireBannerWasLoaded_m7112 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADBannerView_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(973);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		BannerWasLoadedDelegate_t974 * L_0 = ((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasLoaded_4;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t717_il2cpp_TypeInfo_var);
		BannerWasLoadedDelegate_t974 * L_1 = ((ADBannerView_t717_StaticFields*)ADBannerView_t717_il2cpp_TypeInfo_var->static_fields)->___onBannerWasLoaded_4;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(10 /* System.Void UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::Invoke() */, L_1);
	}

IL_0014:
	{
		return;
	}
}
// UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_InterstitialWas.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_InterstitialWasMethodDeclarations.h"



// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void InterstitialWasLoadedDelegate__ctor_m7113 (InterstitialWasLoadedDelegate_t1572 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::Invoke()
extern "C" void InterstitialWasLoadedDelegate_Invoke_m7114 (InterstitialWasLoadedDelegate_t1572 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		InterstitialWasLoadedDelegate_Invoke_m7114((InterstitialWasLoadedDelegate_t1572 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_InterstitialWasLoadedDelegate_t1572(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * InterstitialWasLoadedDelegate_BeginInvoke_m7115 (InterstitialWasLoadedDelegate_t1572 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void InterstitialWasLoadedDelegate_EndInvoke_m7116 (InterstitialWasLoadedDelegate_t1572 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.iOS.ADInterstitialAd
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.ADInterstitialAd
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAdMethodDeclarations.h"



// System.Void UnityEngine.iOS.ADInterstitialAd::.ctor()
extern "C" void ADInterstitialAd__ctor_m3688 (ADInterstitialAd_t331 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		ADInterstitialAd_CtorImpl_m7123(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::.cctor()
extern "C" void ADInterstitialAd__cctor_m7117 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// System.IntPtr UnityEngine.iOS.ADInterstitialAd::Native_CreateInterstitial(System.Boolean)
extern "C" IntPtr_t ADInterstitialAd_Native_CreateInterstitial_m7118 (Object_t * __this /* static, unused */, bool ___autoReload, MethodInfo* method)
{
	typedef IntPtr_t (*ADInterstitialAd_Native_CreateInterstitial_m7118_ftn) (bool);
	static ADInterstitialAd_Native_CreateInterstitial_m7118_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADInterstitialAd_Native_CreateInterstitial_m7118_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADInterstitialAd::Native_CreateInterstitial(System.Boolean)");
	return _il2cpp_icall_func(___autoReload);
}
// System.Void UnityEngine.iOS.ADInterstitialAd::Native_ShowInterstitial(System.IntPtr)
extern "C" void ADInterstitialAd_Native_ShowInterstitial_m7119 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method)
{
	typedef void (*ADInterstitialAd_Native_ShowInterstitial_m7119_ftn) (IntPtr_t);
	static ADInterstitialAd_Native_ShowInterstitial_m7119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADInterstitialAd_Native_ShowInterstitial_m7119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADInterstitialAd::Native_ShowInterstitial(System.IntPtr)");
	_il2cpp_icall_func(___view);
}
// System.Void UnityEngine.iOS.ADInterstitialAd::Native_ReloadInterstitial(System.IntPtr)
extern "C" void ADInterstitialAd_Native_ReloadInterstitial_m7120 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method)
{
	typedef void (*ADInterstitialAd_Native_ReloadInterstitial_m7120_ftn) (IntPtr_t);
	static ADInterstitialAd_Native_ReloadInterstitial_m7120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADInterstitialAd_Native_ReloadInterstitial_m7120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADInterstitialAd::Native_ReloadInterstitial(System.IntPtr)");
	_il2cpp_icall_func(___view);
}
// System.Boolean UnityEngine.iOS.ADInterstitialAd::Native_InterstitialAdLoaded(System.IntPtr)
extern "C" bool ADInterstitialAd_Native_InterstitialAdLoaded_m7121 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method)
{
	typedef bool (*ADInterstitialAd_Native_InterstitialAdLoaded_m7121_ftn) (IntPtr_t);
	static ADInterstitialAd_Native_InterstitialAdLoaded_m7121_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADInterstitialAd_Native_InterstitialAdLoaded_m7121_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADInterstitialAd::Native_InterstitialAdLoaded(System.IntPtr)");
	return _il2cpp_icall_func(___view);
}
// System.Void UnityEngine.iOS.ADInterstitialAd::Native_DestroyInterstitial(System.IntPtr)
extern "C" void ADInterstitialAd_Native_DestroyInterstitial_m7122 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method)
{
	typedef void (*ADInterstitialAd_Native_DestroyInterstitial_m7122_ftn) (IntPtr_t);
	static ADInterstitialAd_Native_DestroyInterstitial_m7122_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADInterstitialAd_Native_DestroyInterstitial_m7122_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADInterstitialAd::Native_DestroyInterstitial(System.IntPtr)");
	_il2cpp_icall_func(___view);
}
// System.Void UnityEngine.iOS.ADInterstitialAd::CtorImpl(System.Boolean)
extern TypeInfo* ADInterstitialAd_t331_il2cpp_TypeInfo_var;
extern "C" void ADInterstitialAd_CtorImpl_m7123 (ADInterstitialAd_t331 * __this, bool ___autoReload, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADInterstitialAd_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		bool L_0 = ((ADInterstitialAd_t331_StaticFields*)ADInterstitialAd_t331_il2cpp_TypeInfo_var->static_fields)->____AlwaysFalseDummy_1;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		ADInterstitialAd_FireInterstitialWasLoaded_m7125(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		bool L_1 = ___autoReload;
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = ADInterstitialAd_Native_CreateInterstitial_m7118(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___interstitialView_0 = L_2;
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::Finalize()
extern TypeInfo* ADInterstitialAd_t331_il2cpp_TypeInfo_var;
extern "C" void ADInterstitialAd_Finalize_m7124 (ADInterstitialAd_t331 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADInterstitialAd_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0 = (__this->___interstitialView_0);
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		ADInterstitialAd_Native_DestroyInterstitial_m7122(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		Object_Finalize_m1177(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::Show()
extern TypeInfo* ADInterstitialAd_t331_il2cpp_TypeInfo_var;
extern "C" void ADInterstitialAd_Show_m4234 (ADInterstitialAd_t331 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADInterstitialAd_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ADInterstitialAd_get_loaded_m4233(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_1 = (__this->___interstitialView_0);
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		ADInterstitialAd_Native_ShowInterstitial_m7119(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001b:
	{
		Debug_Log_m869(NULL /*static, unused*/, (String_t*) &_stringLiteral964, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::ReloadAd()
extern TypeInfo* ADInterstitialAd_t331_il2cpp_TypeInfo_var;
extern "C" void ADInterstitialAd_ReloadAd_m4207 (ADInterstitialAd_t331 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADInterstitialAd_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___interstitialView_0);
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		ADInterstitialAd_Native_ReloadInterstitial_m7120(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.iOS.ADInterstitialAd::get_loaded()
extern TypeInfo* ADInterstitialAd_t331_il2cpp_TypeInfo_var;
extern "C" bool ADInterstitialAd_get_loaded_m4233 (ADInterstitialAd_t331 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADInterstitialAd_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___interstitialView_0);
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		bool L_1 = ADInterstitialAd_Native_InterstitialAdLoaded_m7121(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::FireInterstitialWasLoaded()
extern TypeInfo* ADInterstitialAd_t331_il2cpp_TypeInfo_var;
extern "C" void ADInterstitialAd_FireInterstitialWasLoaded_m7125 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADInterstitialAd_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		InterstitialWasLoadedDelegate_t1572 * L_0 = ((ADInterstitialAd_t331_StaticFields*)ADInterstitialAd_t331_il2cpp_TypeInfo_var->static_fields)->___onInterstitialWasLoaded_2;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		InterstitialWasLoadedDelegate_t1572 * L_1 = ((ADInterstitialAd_t331_StaticFields*)ADInterstitialAd_t331_il2cpp_TypeInfo_var->static_fields)->___onInterstitialWasLoaded_2;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(10 /* System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::Invoke() */, L_1);
	}

IL_0014:
	{
		return;
	}
}
// UnityEngine.iOS.DeviceGeneration
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.DeviceGeneration
#include "UnityEngine_UnityEngine_iOS_DeviceGenerationMethodDeclarations.h"



// UnityEngine.iOS.Device
#include "UnityEngine_UnityEngine_iOS_Device.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.iOS.Device
#include "UnityEngine_UnityEngine_iOS_DeviceMethodDeclarations.h"



// UnityEngine.iOS.DeviceGeneration UnityEngine.iOS.Device::get_generation()
extern "C" int32_t Device_get_generation_m4205 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (*Device_get_generation_m4205_ftn) ();
	static Device_get_generation_m4205_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Device_get_generation_m4205_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.Device::get_generation()");
	return _il2cpp_icall_func();
}
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_Particle.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"



// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t758  Particle_get_position_m7126 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = (__this->___m_Position_0);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C" void Particle_set_position_m7127 (Particle_t1574 * __this, Vector3_t758  ___value, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = ___value;
		__this->___m_Position_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t758  Particle_get_velocity_m7128 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = (__this->___m_Velocity_1);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m7129 (Particle_t1574 * __this, Vector3_t758  ___value, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = ___value;
		__this->___m_Velocity_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m7130 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Energy_5);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C" void Particle_set_energy_m7131 (Particle_t1574 * __this, float ___value, MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Energy_5 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m7132 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_StartEnergy_6);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m7133 (Particle_t1574 * __this, float ___value, MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_StartEnergy_6 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m7134 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Size_2);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m7135 (Particle_t1574 * __this, float ___value, MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Size_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m7136 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Rotation_3);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m7137 (Particle_t1574 * __this, float ___value, MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Rotation_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m7138 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AngularVelocity_4);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m7139 (Particle_t1574 * __this, float ___value, MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_AngularVelocity_4 = L_0;
		return;
	}
}
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t747  Particle_get_color_m7140 (Particle_t1574 * __this, MethodInfo* method)
{
	{
		Color_t747  L_0 = (__this->___m_Color_7);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C" void Particle_set_color_m7141 (Particle_t1574 * __this, Color_t747  ___value, MethodInfo* method)
{
	{
		Color_t747  L_0 = ___value;
		__this->___m_Color_7 = L_0;
		return;
	}
}
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceModeMethodDeclarations.h"



// UnityEngine.Physics
#include "UnityEngine_UnityEngine_Physics.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"

// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"


// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Internal_Raycast_m7142 (Object_t * __this /* static, unused */, Vector3_t758  ___origin, Vector3_t758  ___direction, RaycastHit_t990 * ___hitInfo, float ___maxDistance, int32_t ___layermask, MethodInfo* method)
{
	{
		RaycastHit_t990 * L_0 = ___hitInfo;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layermask;
		bool L_3 = Physics_INTERNAL_CALL_Internal_Raycast_m7143(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_INTERNAL_CALL_Internal_Raycast_m7143 (Object_t * __this /* static, unused */, Vector3_t758 * ___origin, Vector3_t758 * ___direction, RaycastHit_t990 * ___hitInfo, float ___maxDistance, int32_t ___layermask, MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m7143_ftn) (Vector3_t758 *, Vector3_t758 *, RaycastHit_t990 *, float, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m7143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m7143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___hitInfo, ___maxDistance, ___layermask);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C" bool Physics_Raycast_m4136 (Object_t * __this /* static, unused */, Vector3_t758  ___origin, Vector3_t758  ___direction, RaycastHit_t990 * ___hitInfo, MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = ((int32_t)-5);
		V_1 = (std::numeric_limits<float>::infinity());
		Vector3_t758  L_0 = ___origin;
		Vector3_t758  L_1 = ___direction;
		RaycastHit_t990 * L_2 = ___hitInfo;
		float L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m7144(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m7144 (Object_t * __this /* static, unused */, Vector3_t758  ___origin, Vector3_t758  ___direction, RaycastHit_t990 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = ___origin;
		Vector3_t758  L_1 = ___direction;
		RaycastHit_t990 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Internal_Raycast_m7142(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m5899 (Object_t * __this /* static, unused */, Ray_t1375  ___ray, RaycastHit_t990 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Ray_get_origin_m5824((&___ray), /*hidden argument*/NULL);
		Vector3_t758  L_1 = Ray_get_direction_m5825((&___ray), /*hidden argument*/NULL);
		RaycastHit_t990 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Raycast_m7144(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t1380* Physics_RaycastAll_m5835 (Object_t * __this /* static, unused */, Ray_t1375  ___ray, float ___maxDistance, int32_t ___layerMask, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = Ray_get_origin_m5824((&___ray), /*hidden argument*/NULL);
		Vector3_t758  L_1 = Ray_get_direction_m5825((&___ray), /*hidden argument*/NULL);
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layerMask;
		RaycastHitU5BU5D_t1380* L_4 = Physics_RaycastAll_m7145(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t1380* Physics_RaycastAll_m7145 (Object_t * __this /* static, unused */, Vector3_t758  ___origin, Vector3_t758  ___direction, float ___maxDistance, int32_t ___layermask, MethodInfo* method)
{
	{
		float L_0 = ___maxDistance;
		int32_t L_1 = ___layermask;
		RaycastHitU5BU5D_t1380* L_2 = Physics_INTERNAL_CALL_RaycastAll_m7146(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t1380* Physics_INTERNAL_CALL_RaycastAll_m7146 (Object_t * __this /* static, unused */, Vector3_t758 * ___origin, Vector3_t758 * ___direction, float ___maxDistance, int32_t ___layermask, MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t1380* (*Physics_INTERNAL_CALL_RaycastAll_m7146_ftn) (Vector3_t758 *, Vector3_t758 *, float, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m7146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m7146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___maxDistance, ___layermask);
}
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"



// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C" void Rigidbody_AddForce_m4188 (Rigidbody_t994 * __this, Vector3_t758  ___force, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m7147(NULL /*static, unused*/, __this, (&___force), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C" void Rigidbody_INTERNAL_CALL_AddForce_m7147 (Object_t * __this /* static, unused */, Rigidbody_t994 * ___self, Vector3_t758 * ___force, int32_t ___mode, MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m7147_ftn) (Rigidbody_t994 *, Vector3_t758 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m7147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m7147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self, ___force, ___mode);
}
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"



// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C" Rigidbody_t994 * Collider_get_attachedRigidbody_m7148 (Collider_t900 * __this, MethodInfo* method)
{
	typedef Rigidbody_t994 * (*Collider_get_attachedRigidbody_m7148_ftn) (Collider_t900 *);
	static Collider_get_attachedRigidbody_m7148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_attachedRigidbody_m7148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"



// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t758  RaycastHit_get_point_m5837 (RaycastHit_t990 * __this, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = (__this->___m_Point_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t758  RaycastHit_get_normal_m5838 (RaycastHit_t990 * __this, MethodInfo* method)
{
	{
		Vector3_t758  L_0 = (__this->___m_Normal_1);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m5840 (RaycastHit_t990 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_3);
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t900 * RaycastHit_get_collider_m5839 (RaycastHit_t990 * __this, MethodInfo* method)
{
	{
		Collider_t900 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C" Rigidbody_t994 * RaycastHit_get_rigidbody_m7149 (RaycastHit_t990 * __this, MethodInfo* method)
{
	Rigidbody_t994 * G_B3_0 = {0};
	{
		Collider_t900 * L_0 = RaycastHit_get_collider_m5839(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider_t900 * L_2 = RaycastHit_get_collider_m5839(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t994 * L_3 = Collider_get_attachedRigidbody_m7148(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody_t994 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C" Transform_t809 * RaycastHit_get_transform_m4137 (RaycastHit_t990 * __this, MethodInfo* method)
{
	Rigidbody_t994 * V_0 = {0};
	{
		Rigidbody_t994 * L_0 = RaycastHit_get_rigidbody_m7149(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t994 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_1, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody_t994 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t809 * L_4 = Component_get_transform_m4030(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider_t900 * L_5 = RaycastHit_get_collider_m5839(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_5, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider_t900 * L_7 = RaycastHit_get_collider_m5839(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t809 * L_8 = Component_get_transform_m4030(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t809 *)NULL;
	}
}
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2D.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_44.h"
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_44MethodDeclarations.h"


// System.Void UnityEngine.Physics2D::.cctor()
extern TypeInfo* List_1_t1577_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t1377_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m7512_MethodInfo_var;
extern "C" void Physics2D__cctor_m7150 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1577_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2904);
		Physics2D_t1377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2206);
		List_1__ctor_m7512_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484567);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t1577_il2cpp_TypeInfo_var);
		List_1_t1577 * L_0 = (List_1_t1577 *)il2cpp_codegen_object_new (List_1_t1577_il2cpp_TypeInfo_var);
		List_1__ctor_m7512(L_0, /*hidden argument*/List_1__ctor_m7512_MethodInfo_var);
		((Physics2D_t1377_StaticFields*)Physics2D_t1377_il2cpp_TypeInfo_var->static_fields)->___m_LastDisabledRigidbody2D_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t1377_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Raycast_m7151 (Object_t * __this /* static, unused */, Vector2_t739  ___origin, Vector2_t739  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t1378 * ___raycastHit, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t1377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2206);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = ___minDepth;
		float L_3 = ___maxDepth;
		RaycastHit2D_t1378 * L_4 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1377_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m7152(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m7152 (Object_t * __this /* static, unused */, Vector2_t739 * ___origin, Vector2_t739 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t1378 * ___raycastHit, MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m7152_ftn) (Vector2_t739 *, Vector2_t739 *, float, int32_t, float, float, RaycastHit2D_t1378 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m7152_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m7152_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t1377_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t1378  Physics2D_Raycast_m5900 (Object_t * __this /* static, unused */, Vector2_t739  ___origin, Vector2_t739  ___direction, float ___distance, int32_t ___layerMask, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t1377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2206);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t739  L_0 = ___origin;
		Vector2_t739  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1377_il2cpp_TypeInfo_var);
		RaycastHit2D_t1378  L_6 = Physics2D_Raycast_m7153(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t1377_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t1378  Physics2D_Raycast_m7153 (Object_t * __this /* static, unused */, Vector2_t739  ___origin, Vector2_t739  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t1377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2206);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1378  V_0 = {0};
	{
		Vector2_t739  L_0 = ___origin;
		Vector2_t739  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = ___minDepth;
		float L_5 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1377_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m7151(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t1378  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t1377_il2cpp_TypeInfo_var;
extern "C" RaycastHit2DU5BU5D_t1376* Physics2D_RaycastAll_m5826 (Object_t * __this /* static, unused */, Vector2_t739  ___origin, Vector2_t739  ___direction, float ___distance, int32_t ___layerMask, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t1377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2206);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1377_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t1376* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m7154(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C" RaycastHit2DU5BU5D_t1376* Physics2D_INTERNAL_CALL_RaycastAll_m7154 (Object_t * __this /* static, unused */, Vector2_t739 * ___origin, Vector2_t739 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t1376* (*Physics2D_INTERNAL_CALL_RaycastAll_m7154_ftn) (Vector2_t739 *, Vector2_t739 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m7154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m7154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"

// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2D.h"
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"


// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t739  RaycastHit2D_get_point_m5827 (RaycastHit2D_t1378 * __this, MethodInfo* method)
{
	{
		Vector2_t739  L_0 = (__this->___m_Point_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t739  RaycastHit2D_get_normal_m5828 (RaycastHit2D_t1378 * __this, MethodInfo* method)
{
	{
		Vector2_t739  L_0 = (__this->___m_Normal_2);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m5901 (RaycastHit2D_t1378 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Fraction_4);
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t1379 * RaycastHit2D_get_collider_m5829 (RaycastHit2D_t1378 * __this, MethodInfo* method)
{
	{
		Collider2D_t1379 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" Rigidbody2D_t1578 * RaycastHit2D_get_rigidbody_m7155 (RaycastHit2D_t1378 * __this, MethodInfo* method)
{
	Rigidbody2D_t1578 * G_B3_0 = {0};
	{
		Collider2D_t1379 * L_0 = RaycastHit2D_get_collider_m5829(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t1379 * L_2 = RaycastHit2D_get_collider_m5829(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t1578 * L_3 = Collider2D_get_attachedRigidbody_m7156(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t1578 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" Transform_t809 * RaycastHit2D_get_transform_m5830 (RaycastHit2D_t1378 * __this, MethodInfo* method)
{
	Rigidbody2D_t1578 * V_0 = {0};
	{
		Rigidbody2D_t1578 * L_0 = RaycastHit2D_get_rigidbody_m7155(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t1578 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_1, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t1578 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t809 * L_4 = Component_get_transform_m4030(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t1379 * L_5 = RaycastHit2D_get_collider_m5829(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_5, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t1379 * L_7 = RaycastHit2D_get_collider_m5829(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t809 * L_8 = Component_get_transform_m4030(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t809 *)NULL;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t1578 * Collider2D_get_attachedRigidbody_m7156 (Collider2D_t1379 * __this, MethodInfo* method)
{
	typedef Rigidbody2D_t1578 * (*Collider2D_get_attachedRigidbody_m7156_ftn) (Collider2D_t1379 *);
	static Collider2D_get_attachedRigidbody_m7156_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m7156_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"



// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void AudioConfigurationChangeHandler__ctor_m7157 (AudioConfigurationChangeHandler_t1579 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C" void AudioConfigurationChangeHandler_Invoke_m7158 (AudioConfigurationChangeHandler_t1579 * __this, bool ___deviceWasChanged, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m7158((AudioConfigurationChangeHandler_t1579 *)__this->___prev_9,___deviceWasChanged, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ___deviceWasChanged, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___deviceWasChanged, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t1579(Il2CppObject* delegate, bool ___deviceWasChanged)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___deviceWasChanged' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___deviceWasChanged);

	// Marshaling cleanup of parameter '___deviceWasChanged' native representation

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioConfigurationChangeHandler_BeginInvoke_m7159 (AudioConfigurationChangeHandler_t1579 * __this, bool ___deviceWasChanged, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t203_il2cpp_TypeInfo_var, &___deviceWasChanged);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m7160 (AudioConfigurationChangeHandler_t1579 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettings.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"



// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern TypeInfo* AudioSettings_t1580_il2cpp_TypeInfo_var;
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m7161 (AudioSettings_t1580 * __this, bool ___deviceWasChanged, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioSettings_t1580_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2905);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t1579 * L_0 = ((AudioSettings_t1580_StaticFields*)AudioSettings_t1580_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t1579 * L_1 = ((AudioSettings_t1580_StaticFields*)AudioSettings_t1580_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		bool L_2 = ___deviceWasChanged;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean) */, L_1, L_2);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.AudioType
#include "UnityEngine_UnityEngine_AudioType.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioType
#include "UnityEngine_UnityEngine_AudioTypeMethodDeclarations.h"



// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"



// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMReaderCallback__ctor_m7162 (PCMReaderCallback_t1583 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C" void PCMReaderCallback_Invoke_m7163 (PCMReaderCallback_t1583 * __this, SingleU5BU5D_t1582* ___data, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMReaderCallback_Invoke_m7163((PCMReaderCallback_t1583 *)__this->___prev_9,___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SingleU5BU5D_t1582* ___data, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SingleU5BU5D_t1582* ___data, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t1583(Il2CppObject* delegate, SingleU5BU5D_t1582* ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___data' to native representation
	float* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data);

	// Native function invocation
	_il2cpp_pinvoke_func(____data_marshaled);

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C" Object_t * PCMReaderCallback_BeginInvoke_m7164 (PCMReaderCallback_t1583 * __this, SingleU5BU5D_t1582* ___data, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMReaderCallback_EndInvoke_m7165 (PCMReaderCallback_t1583 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"



// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMSetPositionCallback__ctor_m7166 (PCMSetPositionCallback_t1584 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C" void PCMSetPositionCallback_Invoke_m7167 (PCMSetPositionCallback_t1584 * __this, int32_t ___position, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMSetPositionCallback_Invoke_m7167((PCMSetPositionCallback_t1584 *)__this->___prev_9,___position, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___position, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___position, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t1584(Il2CppObject* delegate, int32_t ___position)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___position' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___position);

	// Marshaling cleanup of parameter '___position' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t189_il2cpp_TypeInfo_var;
extern "C" Object_t * PCMSetPositionCallback_BeginInvoke_m7168 (PCMSetPositionCallback_t1584 * __this, int32_t ___position, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t189_il2cpp_TypeInfo_var, &___position);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMSetPositionCallback_EndInvoke_m7169 (PCMSetPositionCallback_t1584 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"



// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m7170 (AudioClip_t753 * __this, SingleU5BU5D_t1582* ___data, MethodInfo* method)
{
	{
		PCMReaderCallback_t1583 * L_0 = (__this->___m_PCMReaderCallback_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t1583 * L_1 = (__this->___m_PCMReaderCallback_2);
		SingleU5BU5D_t1582* L_2 = ___data;
		NullCheck(L_1);
		VirtActionInvoker1< SingleU5BU5D_t1582* >::Invoke(10 /* System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[]) */, L_1, L_2);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m7171 (AudioClip_t753 * __this, int32_t ___position, MethodInfo* method)
{
	{
		PCMSetPositionCallback_t1584 * L_0 = (__this->___m_PCMSetPositionCallback_3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t1584 * L_1 = (__this->___m_PCMSetPositionCallback_3);
		int32_t L_2 = ___position;
		NullCheck(L_1);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32) */, L_1, L_2);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"

// System.UInt64
#include "mscorlib_System_UInt64.h"


// System.Single UnityEngine.AudioSource::get_volume()
extern "C" float AudioSource_get_volume_m4226 (AudioSource_t755 * __this, MethodInfo* method)
{
	typedef float (*AudioSource_get_volume_m4226_ftn) (AudioSource_t755 *);
	static AudioSource_get_volume_m4226_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m4226_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C" void AudioSource_set_volume_m4227 (AudioSource_t755 * __this, float ___value, MethodInfo* method)
{
	typedef void (*AudioSource_set_volume_m4227_ftn) (AudioSource_t755 *, float);
	static AudioSource_set_volume_m4227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m4227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C" float AudioSource_get_pitch_m4221 (AudioSource_t755 * __this, MethodInfo* method)
{
	typedef float (*AudioSource_get_pitch_m4221_ftn) (AudioSource_t755 *);
	static AudioSource_get_pitch_m4221_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_pitch_m4221_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_pitch()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C" void AudioSource_set_pitch_m4145 (AudioSource_t755 * __this, float ___value, MethodInfo* method)
{
	typedef void (*AudioSource_set_pitch_m4145_ftn) (AudioSource_t755 *, float);
	static AudioSource_set_pitch_m4145_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m4145_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C" AudioClip_t753 * AudioSource_get_clip_m4144 (AudioSource_t755 * __this, MethodInfo* method)
{
	typedef AudioClip_t753 * (*AudioSource_get_clip_m4144_ftn) (AudioSource_t755 *);
	static AudioSource_get_clip_m4144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m4144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C" void AudioSource_set_clip_m4282 (AudioSource_t755 * __this, AudioClip_t753 * ___value, MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m4282_ftn) (AudioSource_t755 *, AudioClip_t753 *);
	static AudioSource_set_clip_m4282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m4282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C" void AudioSource_Play_m7172 (AudioSource_t755 * __this, uint64_t ___delay, MethodInfo* method)
{
	typedef void (*AudioSource_Play_m7172_ftn) (AudioSource_t755 *, uint64_t);
	static AudioSource_Play_m7172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m7172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C" void AudioSource_Play_m4283 (AudioSource_t755 * __this, MethodInfo* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)0));
		uint64_t L_0 = V_0;
		AudioSource_Play_m7172(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C" bool AudioSource_get_isPlaying_m4147 (AudioSource_t755 * __this, MethodInfo* method)
{
	typedef bool (*AudioSource_get_isPlaying_m4147_ftn) (AudioSource_t755 *);
	static AudioSource_get_isPlaying_m4147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_isPlaying_m4147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C" void AudioSource_PlayOneShot_m7173 (AudioSource_t755 * __this, AudioClip_t753 * ___clip, float ___volumeScale, MethodInfo* method)
{
	typedef void (*AudioSource_PlayOneShot_m7173_ftn) (AudioSource_t755 *, AudioClip_t753 *, float);
	static AudioSource_PlayOneShot_m7173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShot_m7173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip, ___volumeScale);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C" void AudioSource_PlayOneShot_m4146 (AudioSource_t755 * __this, AudioClip_t753 * ___clip, MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t753 * L_0 = ___clip;
		float L_1 = V_0;
		AudioSource_PlayOneShot_m7173(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern "C" void AudioSource_set_mute_m4255 (AudioSource_t755 * __this, bool ___value, MethodInfo* method)
{
	typedef void (*AudioSource_set_mute_m4255_ftn) (AudioSource_t755 *, bool);
	static AudioSource_set_mute_m4255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_mute_m4255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_mute(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AudioSource::get_minDistance()
extern "C" float AudioSource_get_minDistance_m4222 (AudioSource_t755 * __this, MethodInfo* method)
{
	typedef float (*AudioSource_get_minDistance_m4222_ftn) (AudioSource_t755 *);
	static AudioSource_get_minDistance_m4222_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_minDistance_m4222_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_minDistance()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_minDistance(System.Single)
extern "C" void AudioSource_set_minDistance_m4223 (AudioSource_t755 * __this, float ___value, MethodInfo* method)
{
	typedef void (*AudioSource_set_minDistance_m4223_ftn) (AudioSource_t755 *, float);
	static AudioSource_set_minDistance_m4223_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_minDistance_m4223_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_minDistance(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AudioSource::get_maxDistance()
extern "C" float AudioSource_get_maxDistance_m4224 (AudioSource_t755 * __this, MethodInfo* method)
{
	typedef float (*AudioSource_get_maxDistance_m4224_ftn) (AudioSource_t755 *);
	static AudioSource_get_maxDistance_m4224_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_maxDistance_m4224_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_maxDistance()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_maxDistance(System.Single)
extern "C" void AudioSource_set_maxDistance_m4225 (AudioSource_t755 * __this, float ___value, MethodInfo* method)
{
	typedef void (*AudioSource_set_maxDistance_m4225_ftn) (AudioSource_t755 *, float);
	static AudioSource_set_maxDistance_m4225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_maxDistance_m4225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_maxDistance(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"



// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m7174 (WebCamDevice_t1585 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m7175 (WebCamDevice_t1585 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Flags_1);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
void WebCamDevice_t1585_marshal(const WebCamDevice_t1585& unmarshaled, WebCamDevice_t1585_marshaled& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Name_0);
	marshaled.___m_Flags_1 = unmarshaled.___m_Flags_1;
}
void WebCamDevice_t1585_marshal_back(const WebCamDevice_t1585_marshaled& marshaled, WebCamDevice_t1585& unmarshaled)
{
	unmarshaled.___m_Name_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0);
	unmarshaled.___m_Flags_1 = marshaled.___m_Flags_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
void WebCamDevice_t1585_marshal_cleanup(WebCamDevice_t1585_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"



// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEvent.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"

// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationState.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"


// System.Void UnityEngine.AnimationEvent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AnimationEvent__ctor_m7176 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		__this->___m_Time_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_FunctionName_1 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_StringParameter_2 = L_1;
		__this->___m_ObjectReferenceParameter_3 = (Object_t187 *)NULL;
		__this->___m_FloatParameter_4 = (0.0f);
		__this->___m_IntParameter_5 = 0;
		__this->___m_MessageOptions_6 = 0;
		__this->___m_Source_7 = 0;
		__this->___m_StateSender_8 = (AnimationState_t1587 *)NULL;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C" String_t* AnimationEvent_get_data_m7177 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C" void AnimationEvent_set_data_m7178 (AnimationEvent_t1588 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C" String_t* AnimationEvent_get_stringParameter_m7179 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C" void AnimationEvent_set_stringParameter_m7180 (AnimationEvent_t1588 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C" float AnimationEvent_get_floatParameter_m7181 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatParameter_4);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C" void AnimationEvent_set_floatParameter_m7182 (AnimationEvent_t1588 * __this, float ___value, MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FloatParameter_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C" int32_t AnimationEvent_get_intParameter_m7183 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntParameter_5);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C" void AnimationEvent_set_intParameter_m7184 (AnimationEvent_t1588 * __this, int32_t ___value, MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_IntParameter_5 = L_0;
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C" Object_t187 * AnimationEvent_get_objectReferenceParameter_m7185 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		Object_t187 * L_0 = (__this->___m_ObjectReferenceParameter_3);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C" void AnimationEvent_set_objectReferenceParameter_m7186 (AnimationEvent_t1588 * __this, Object_t187 * ___value, MethodInfo* method)
{
	{
		Object_t187 * L_0 = ___value;
		__this->___m_ObjectReferenceParameter_3 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C" String_t* AnimationEvent_get_functionName_m7187 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_FunctionName_1);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C" void AnimationEvent_set_functionName_m7188 (AnimationEvent_t1588 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_FunctionName_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C" float AnimationEvent_get_time_m7189 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Time_0);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C" void AnimationEvent_set_time_m7190 (AnimationEvent_t1588 * __this, float ___value, MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Time_0 = L_0;
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C" int32_t AnimationEvent_get_messageOptions_m7191 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_MessageOptions_6);
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C" void AnimationEvent_set_messageOptions_m7192 (AnimationEvent_t1588 * __this, int32_t ___value, MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_MessageOptions_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C" bool AnimationEvent_get_isFiredByLegacy_m7193 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C" bool AnimationEvent_get_isFiredByAnimator_m7194 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern "C" AnimationState_t1587 * AnimationEvent_get_animationState_m7195 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m7193(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral965, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t1587 * L_1 = (__this->___m_StateSender_8);
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern "C" AnimatorStateInfo_t1589  AnimationEvent_get_animatorStateInfo_m7196 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m7194(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral966, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t1589  L_1 = (__this->___m_AnimatorStateInfo_9);
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern "C" AnimatorClipInfo_t1590  AnimationEvent_get_animatorClipInfo_m7197 (AnimationEvent_t1588 * __this, MethodInfo* method)
{
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m7194(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral967, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t1590  L_1 = (__this->___m_AnimatorClipInfo_10);
		return L_1;
	}
}
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"



// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"



// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m7198 (AnimationCurve_t1592 * __this, KeyframeU5BU5D_t1668* ___keys, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t1668* L_0 = ___keys;
		AnimationCurve_Init_m7202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m7199 (AnimationCurve_t1592 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m7202(__this, (KeyframeU5BU5D_t1668*)(KeyframeU5BU5D_t1668*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m7200 (AnimationCurve_t1592 * __this, MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m7200_ftn) (AnimationCurve_t1592 *);
	static AnimationCurve_Cleanup_m7200_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m7200_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m7201 (AnimationCurve_t1592 * __this, MethodInfo* method)
{
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m7200(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1177(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m7202 (AnimationCurve_t1592 * __this, KeyframeU5BU5D_t1668* ___keys, MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m7202_ftn) (AnimationCurve_t1592 *, KeyframeU5BU5D_t1668*);
	static AnimationCurve_Init_m7202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m7202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
void AnimationCurve_t1592_marshal(const AnimationCurve_t1592& unmarshaled, AnimationCurve_t1592_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.___m_Ptr_0;
}
void AnimationCurve_t1592_marshal_back(const AnimationCurve_t1592_marshaled& marshaled, AnimationCurve_t1592& unmarshaled)
{
	unmarshaled.___m_Ptr_0 = marshaled.___m_Ptr_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
void AnimationCurve_t1592_marshal_cleanup(AnimationCurve_t1592_marshaled& marshaled)
{
}
// UnityEngine.PlayMode
#include "UnityEngine_UnityEngine_PlayMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.PlayMode
#include "UnityEngine_UnityEngine_PlayModeMethodDeclarations.h"



// UnityEngine.Animation/Enumerator
#include "UnityEngine_UnityEngine_Animation_Enumerator.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Animation/Enumerator
#include "UnityEngine_UnityEngine_Animation_EnumeratorMethodDeclarations.h"

// UnityEngine.Animation
#include "UnityEngine_UnityEngine_Animation.h"
// UnityEngine.Animation
#include "UnityEngine_UnityEngine_AnimationMethodDeclarations.h"


// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C" void Enumerator__ctor_m7203 (Enumerator_t1594 * __this, Animation_t982 * ___outer, MethodInfo* method)
{
	{
		__this->___m_CurrentIndex_1 = (-1);
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		Animation_t982 * L_0 = ___outer;
		__this->___m_Outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Animation/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m7204 (Enumerator_t1594 * __this, MethodInfo* method)
{
	{
		Animation_t982 * L_0 = (__this->___m_Outer_0);
		int32_t L_1 = (__this->___m_CurrentIndex_1);
		NullCheck(L_0);
		AnimationState_t1587 * L_2 = Animation_GetStateAtIndex_m7211(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m7205 (Enumerator_t1594 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Animation_t982 * L_0 = (__this->___m_Outer_0);
		NullCheck(L_0);
		int32_t L_1 = Animation_GetStateCount_m7212(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___m_CurrentIndex_1);
		__this->___m_CurrentIndex_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = (__this->___m_CurrentIndex_1);
		int32_t L_4 = V_0;
		return ((((int32_t)L_3) < ((int32_t)L_4))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Animation::Rewind()
extern "C" void Animation_Rewind_m4269 (Animation_t982 * __this, MethodInfo* method)
{
	{
		Animation_INTERNAL_CALL_Rewind_m7206(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::INTERNAL_CALL_Rewind(UnityEngine.Animation)
extern "C" void Animation_INTERNAL_CALL_Rewind_m7206 (Object_t * __this /* static, unused */, Animation_t982 * ___self, MethodInfo* method)
{
	typedef void (*Animation_INTERNAL_CALL_Rewind_m7206_ftn) (Animation_t982 *);
	static Animation_INTERNAL_CALL_Rewind_m7206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_INTERNAL_CALL_Rewind_m7206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::INTERNAL_CALL_Rewind(UnityEngine.Animation)");
	_il2cpp_icall_func(___self);
}
// System.Boolean UnityEngine.Animation::Play()
extern "C" bool Animation_Play_m4268 (Animation_t982 * __this, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		bool L_1 = Animation_Play_m7207(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Animation::Play(UnityEngine.PlayMode)
extern "C" bool Animation_Play_m7207 (Animation_t982 * __this, int32_t ___mode, MethodInfo* method)
{
	{
		int32_t L_0 = ___mode;
		bool L_1 = Animation_PlayDefaultAnimation_m7209(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C" bool Animation_Play_m7208 (Animation_t982 * __this, String_t* ___animation, int32_t ___mode, MethodInfo* method)
{
	typedef bool (*Animation_Play_m7208_ftn) (Animation_t982 *, String_t*, int32_t);
	static Animation_Play_m7208_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Play_m7208_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)");
	return _il2cpp_icall_func(__this, ___animation, ___mode);
}
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C" bool Animation_Play_m4049 (Animation_t982 * __this, String_t* ___animation, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		String_t* L_0 = ___animation;
		int32_t L_1 = V_0;
		bool L_2 = Animation_Play_m7208(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)
extern "C" bool Animation_PlayDefaultAnimation_m7209 (Animation_t982 * __this, int32_t ___mode, MethodInfo* method)
{
	typedef bool (*Animation_PlayDefaultAnimation_m7209_ftn) (Animation_t982 *, int32_t);
	static Animation_PlayDefaultAnimation_m7209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_PlayDefaultAnimation_m7209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)");
	return _il2cpp_icall_func(__this, ___mode);
}
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
extern TypeInfo* Enumerator_t1594_il2cpp_TypeInfo_var;
extern "C" Object_t * Animation_GetEnumerator_m7210 (Animation_t982 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1594_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2906);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1594 * L_0 = (Enumerator_t1594 *)il2cpp_codegen_object_new (Enumerator_t1594_il2cpp_TypeInfo_var);
		Enumerator__ctor_m7203(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C" AnimationState_t1587 * Animation_GetStateAtIndex_m7211 (Animation_t982 * __this, int32_t ___index, MethodInfo* method)
{
	typedef AnimationState_t1587 * (*Animation_GetStateAtIndex_m7211_ftn) (Animation_t982 *, int32_t);
	static Animation_GetStateAtIndex_m7211_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateAtIndex_m7211_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateAtIndex(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C" int32_t Animation_GetStateCount_m7212 (Animation_t982 * __this, MethodInfo* method)
{
	typedef int32_t (*Animation_GetStateCount_m7212_ftn) (Animation_t982 *);
	static Animation_GetStateCount_m7212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateCount_m7212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateCount()");
	return _il2cpp_icall_func(__this);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"


// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" bool AnimatorStateInfo_IsName_m7213 (AnimatorStateInfo_t1589 * __this, String_t* ___name, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m7233(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = (__this->___m_FullPath_2);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (__this->___m_Name_0);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (__this->___m_Path_1);
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return G_B4_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" int32_t AnimatorStateInfo_get_fullPathHash_m7214 (AnimatorStateInfo_t1589 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C" int32_t AnimatorStateInfo_get_nameHash_m7215 (AnimatorStateInfo_t1589 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Path_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" int32_t AnimatorStateInfo_get_shortNameHash_m7216 (AnimatorStateInfo_t1589 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" float AnimatorStateInfo_get_normalizedTime_m7217 (AnimatorStateInfo_t1589 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" float AnimatorStateInfo_get_length_m7218 (AnimatorStateInfo_t1589 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Length_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" int32_t AnimatorStateInfo_get_tagHash_m7219 (AnimatorStateInfo_t1589 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Tag_5);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" bool AnimatorStateInfo_IsTag_m7220 (AnimatorStateInfo_t1589 * __this, String_t* ___tag, MethodInfo* method)
{
	{
		String_t* L_0 = ___tag;
		int32_t L_1 = Animator_StringToHash_m7233(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Tag_5);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" bool AnimatorStateInfo_get_loop_m7221 (AnimatorStateInfo_t1589 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Loop_6);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"



// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" bool AnimatorTransitionInfo_IsName_m7222 (AnimatorTransitionInfo_t1596 * __this, String_t* ___name, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m7233(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Name_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name;
		int32_t L_4 = Animator_StringToHash_m7233(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = (__this->___m_FullPath_0);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" bool AnimatorTransitionInfo_IsUserName_m7223 (AnimatorTransitionInfo_t1596 * __this, String_t* ___name, MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m7233(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_UserName_1);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C" int32_t AnimatorTransitionInfo_get_fullPathHash_m7224 (AnimatorTransitionInfo_t1596 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_0);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" int32_t AnimatorTransitionInfo_get_nameHash_m7225 (AnimatorTransitionInfo_t1596 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" int32_t AnimatorTransitionInfo_get_userNameHash_m7226 (AnimatorTransitionInfo_t1596 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UserName_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" float AnimatorTransitionInfo_get_normalizedTime_m7227 (AnimatorTransitionInfo_t1596 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C" bool AnimatorTransitionInfo_get_anyState_m7228 (AnimatorTransitionInfo_t1596 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AnyState_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C" bool AnimatorTransitionInfo_get_entry_m7229 (AnimatorTransitionInfo_t1596 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C" bool AnimatorTransitionInfo_get_exit_m7230 (AnimatorTransitionInfo_t1596 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
void AnimatorTransitionInfo_t1596_marshal(const AnimatorTransitionInfo_t1596& unmarshaled, AnimatorTransitionInfo_t1596_marshaled& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.___m_FullPath_0;
	marshaled.___m_UserName_1 = unmarshaled.___m_UserName_1;
	marshaled.___m_Name_2 = unmarshaled.___m_Name_2;
	marshaled.___m_NormalizedTime_3 = unmarshaled.___m_NormalizedTime_3;
	marshaled.___m_AnyState_4 = unmarshaled.___m_AnyState_4;
	marshaled.___m_TransitionType_5 = unmarshaled.___m_TransitionType_5;
}
void AnimatorTransitionInfo_t1596_marshal_back(const AnimatorTransitionInfo_t1596_marshaled& marshaled, AnimatorTransitionInfo_t1596& unmarshaled)
{
	unmarshaled.___m_FullPath_0 = marshaled.___m_FullPath_0;
	unmarshaled.___m_UserName_1 = marshaled.___m_UserName_1;
	unmarshaled.___m_Name_2 = marshaled.___m_Name_2;
	unmarshaled.___m_NormalizedTime_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.___m_AnyState_4 = marshaled.___m_AnyState_4;
	unmarshaled.___m_TransitionType_5 = marshaled.___m_TransitionType_5;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
void AnimatorTransitionInfo_t1596_marshal_cleanup(AnimatorTransitionInfo_t1596_marshaled& marshaled)
{
}
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"


// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" void Animator_SetTrigger_m6100 (Animator_t749 * __this, String_t* ___name, MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_SetTriggerString_m7234(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" void Animator_ResetTrigger_m6099 (Animator_t749 * __this, String_t* ___name, MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_ResetTriggerString_m7235(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C" void Animator_set_speed_m4129 (Animator_t749 * __this, float ___value, MethodInfo* method)
{
	typedef void (*Animator_set_speed_m4129_ftn) (Animator_t749 *, float);
	static Animator_set_speed_m4129_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_speed_m4129_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Animator::Play(System.String)
extern "C" void Animator_Play_m4214 (Animator_t749 * __this, String_t* ___stateName, MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		V_0 = (-std::numeric_limits<float>::infinity());
		V_1 = (-1);
		String_t* L_0 = ___stateName;
		int32_t L_1 = V_1;
		float L_2 = V_0;
		Animator_Play_m7231(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C" void Animator_Play_m7231 (Animator_t749 * __this, String_t* ___stateName, int32_t ___layer, float ___normalizedTime, MethodInfo* method)
{
	{
		String_t* L_0 = ___stateName;
		int32_t L_1 = Animator_StringToHash_m7233(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___layer;
		float L_3 = ___normalizedTime;
		Animator_Play_m7232(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C" void Animator_Play_m7232 (Animator_t749 * __this, int32_t ___stateNameHash, int32_t ___layer, float ___normalizedTime, MethodInfo* method)
{
	typedef void (*Animator_Play_m7232_ftn) (Animator_t749 *, int32_t, int32_t, float);
	static Animator_Play_m7232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_Play_m7232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash, ___layer, ___normalizedTime);
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C" RuntimeAnimatorController_t1406 * Animator_get_runtimeAnimatorController_m6098 (Animator_t749 * __this, MethodInfo* method)
{
	typedef RuntimeAnimatorController_t1406 * (*Animator_get_runtimeAnimatorController_m6098_ftn) (Animator_t749 *);
	static Animator_get_runtimeAnimatorController_m6098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m6098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m7233 (Object_t * __this /* static, unused */, String_t* ___name, MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m7233_ftn) (String_t*);
	static Animator_StringToHash_m7233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m7233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m7234 (Animator_t749 * __this, String_t* ___name, MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m7234_ftn) (Animator_t749 *, String_t*);
	static Animator_SetTriggerString_m7234_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m7234_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" void Animator_ResetTriggerString_m7235 (Animator_t749 * __this, String_t* ___name, MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m7235_ftn) (Animator_t749 *, String_t*);
	static Animator_ResetTriggerString_m7235_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m7235_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBone.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"



// Conversion methods for marshalling of: UnityEngine.SkeletonBone
void SkeletonBone_t1597_marshal(const SkeletonBone_t1597& unmarshaled, SkeletonBone_t1597_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___position_1 = unmarshaled.___position_1;
	marshaled.___rotation_2 = unmarshaled.___rotation_2;
	marshaled.___scale_3 = unmarshaled.___scale_3;
	marshaled.___transformModified_4 = unmarshaled.___transformModified_4;
}
void SkeletonBone_t1597_marshal_back(const SkeletonBone_t1597_marshaled& marshaled, SkeletonBone_t1597& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___position_1 = marshaled.___position_1;
	unmarshaled.___rotation_2 = marshaled.___rotation_2;
	unmarshaled.___scale_3 = marshaled.___scale_3;
	unmarshaled.___transformModified_4 = marshaled.___transformModified_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
void SkeletonBone_t1597_marshal_cleanup(SkeletonBone_t1597_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimit.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"



// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBone.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"



// System.String UnityEngine.HumanBone::get_boneName()
extern "C" String_t* HumanBone_get_boneName_m7236 (HumanBone_t1599 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_BoneName_0);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
extern "C" void HumanBone_set_boneName_m7237 (HumanBone_t1599 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_BoneName_0 = L_0;
		return;
	}
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C" String_t* HumanBone_get_humanName_m7238 (HumanBone_t1599 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_HumanName_1);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C" void HumanBone_set_humanName_m7239 (HumanBone_t1599 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_HumanName_1 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
void HumanBone_t1599_marshal(const HumanBone_t1599& unmarshaled, HumanBone_t1599_marshaled& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_BoneName_0);
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.___m_HumanName_1);
	marshaled.___limit_2 = unmarshaled.___limit_2;
}
void HumanBone_t1599_marshal_back(const HumanBone_t1599_marshaled& marshaled, HumanBone_t1599& unmarshaled)
{
	unmarshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0);
	unmarshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1);
	unmarshaled.___limit_2 = marshaled.___limit_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
void HumanBone_t1599_marshal_cleanup(HumanBone_t1599_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorControllerMethodDeclarations.h"



// UnityEngine.Terrain
#include "UnityEngine_UnityEngine_Terrain.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Terrain
#include "UnityEngine_UnityEngine_TerrainMethodDeclarations.h"



// System.Void UnityEngine.Terrain::.ctor()
extern "C" void Terrain__ctor_m7240 (Terrain_t1600 * __this, MethodInfo* method)
{
	{
		Behaviour__ctor_m6970(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"



// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"



// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"



// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"

// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"


// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m7241 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		float L_0 = (__this->___width_3);
		return (((int32_t)L_0));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m7242 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_width_m4079(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m7243 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m4080(L_0, /*hidden argument*/NULL);
		return (((int32_t)((-L_1))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m7244 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m7245 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t738 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m4078(L_1, /*hidden argument*/NULL);
		Rect_t738 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_height_m4080(L_3, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((float)((float)L_2+(float)L_4))))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m7246 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t738 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m4078(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)L_2))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m7247 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m7248 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		Rect_t738 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_width_m4079(L_2, /*hidden argument*/NULL);
		return (((int32_t)((float)((float)L_1+(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomLeftUnFlipped_m7249 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		Rect_t738 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m4078(L_2, /*hidden argument*/NULL);
		Vector2_t739  L_4 = {0};
		Vector2__ctor_m4033(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomRightUnFlipped_m7250 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		Rect_t738 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m4079(L_2, /*hidden argument*/NULL);
		Rect_t738 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m4078(L_4, /*hidden argument*/NULL);
		Vector2_t739  L_6 = {0};
		Vector2__ctor_m4033(&L_6, ((float)((float)L_1+(float)L_3)), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvTopRightUnFlipped_m7251 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		Rect_t738 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m4079(L_2, /*hidden argument*/NULL);
		Rect_t738 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m4078(L_4, /*hidden argument*/NULL);
		Rect_t738 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_height_m4080(L_6, /*hidden argument*/NULL);
		Vector2_t739  L_8 = {0};
		Vector2__ctor_m4033(&L_8, ((float)((float)L_1+(float)L_3)), ((float)((float)L_5+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvTopLeftUnFlipped_m7252 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	{
		Rect_t738 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		Rect_t738 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m4078(L_2, /*hidden argument*/NULL);
		Rect_t738 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_height_m4080(L_4, /*hidden argument*/NULL);
		Vector2_t739  L_6 = {0};
		Vector2__ctor_m4033(&L_6, L_1, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomLeft_m7253 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	Vector2_t739  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t739  L_1 = CharacterInfo_get_uvBottomLeftUnFlipped_m7249(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t739  L_2 = CharacterInfo_get_uvBottomLeftUnFlipped_m7249(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomRight_m7254 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	Vector2_t739  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t739  L_1 = CharacterInfo_get_uvTopLeftUnFlipped_m7252(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t739  L_2 = CharacterInfo_get_uvBottomRightUnFlipped_m7250(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t739  CharacterInfo_get_uvTopRight_m7255 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	Vector2_t739  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t739  L_1 = CharacterInfo_get_uvTopRightUnFlipped_m7251(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t739  L_2 = CharacterInfo_get_uvTopRightUnFlipped_m7251(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t739  CharacterInfo_get_uvTopLeft_m7256 (CharacterInfo_t1601 * __this, MethodInfo* method)
{
	Vector2_t739  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t739  L_1 = CharacterInfo_get_uvBottomRightUnFlipped_m7250(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t739  L_2 = CharacterInfo_get_uvTopLeftUnFlipped_m7252(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
void CharacterInfo_t1601_marshal(const CharacterInfo_t1601& unmarshaled, CharacterInfo_t1601_marshaled& marshaled)
{
	marshaled.___index_0 = unmarshaled.___index_0;
	marshaled.___uv_1 = unmarshaled.___uv_1;
	marshaled.___vert_2 = unmarshaled.___vert_2;
	marshaled.___width_3 = unmarshaled.___width_3;
	marshaled.___size_4 = unmarshaled.___size_4;
	marshaled.___style_5 = unmarshaled.___style_5;
	marshaled.___flipped_6 = unmarshaled.___flipped_6;
	marshaled.___ascent_7 = unmarshaled.___ascent_7;
}
void CharacterInfo_t1601_marshal_back(const CharacterInfo_t1601_marshaled& marshaled, CharacterInfo_t1601& unmarshaled)
{
	unmarshaled.___index_0 = marshaled.___index_0;
	unmarshaled.___uv_1 = marshaled.___uv_1;
	unmarshaled.___vert_2 = marshaled.___vert_2;
	unmarshaled.___width_3 = marshaled.___width_3;
	unmarshaled.___size_4 = marshaled.___size_4;
	unmarshaled.___style_5 = marshaled.___style_5;
	unmarshaled.___flipped_6 = marshaled.___flipped_6;
	unmarshaled.___ascent_7 = marshaled.___ascent_7;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
void CharacterInfo_t1601_marshal_cleanup(CharacterInfo_t1601_marshaled& marshaled)
{
}
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"



// System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FontTextureRebuildCallback__ctor_m7257 (FontTextureRebuildCallback_t1602 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern "C" void FontTextureRebuildCallback_Invoke_m7258 (FontTextureRebuildCallback_t1602 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FontTextureRebuildCallback_Invoke_m7258((FontTextureRebuildCallback_t1602 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t1602(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * FontTextureRebuildCallback_BeginInvoke_m7259 (FontTextureRebuildCallback_t1602 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern "C" void FontTextureRebuildCallback_EndInvoke_m7260 (FontTextureRebuildCallback_t1602 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_48.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_48MethodDeclarations.h"


// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t1222_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1383_il2cpp_TypeInfo_var;
extern "C" void Font_add_textureRebuilt_m5860 (Object_t * __this /* static, unused */, Action_1_t1383 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t1222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2222);
		Action_1_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1383 * L_0 = ((Font_t1222_StaticFields*)Font_t1222_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t1383 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Combine_m952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t1222_StaticFields*)Font_t1222_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t1383 *)Castclass(L_2, Action_1_t1383_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t1222_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1383_il2cpp_TypeInfo_var;
extern "C" void Font_remove_textureRebuilt_m7261 (Object_t * __this /* static, unused */, Action_1_t1383 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t1222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2222);
		Action_1_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1383 * L_0 = ((Font_t1222_StaticFields*)Font_t1222_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t1383 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Remove_m5890(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t1222_StaticFields*)Font_t1222_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t1383 *)Castclass(L_2, Action_1_t1383_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C" Material_t989 * Font_get_material_m6110 (Font_t1222 * __this, MethodInfo* method)
{
	typedef Material_t989 * (*Font_get_material_m6110_ftn) (Font_t1222 *);
	static Font_get_material_m6110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_material_m6110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern "C" bool Font_HasCharacter_m6000 (Font_t1222 * __this, uint16_t ___c, MethodInfo* method)
{
	typedef bool (*Font_HasCharacter_m6000_ftn) (Font_t1222 *, uint16_t);
	static Font_HasCharacter_m6000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_HasCharacter_m6000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, ___c);
}
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern TypeInfo* Font_t1222_il2cpp_TypeInfo_var;
extern "C" void Font_InvokeTextureRebuilt_Internal_m7262 (Object_t * __this /* static, unused */, Font_t1222 * ___font, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t1222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2222);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1383 * V_0 = {0};
	{
		Action_1_t1383 * L_0 = ((Font_t1222_StaticFields*)Font_t1222_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		V_0 = L_0;
		Action_1_t1383 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1383 * L_2 = V_0;
		Font_t1222 * L_3 = ___font;
		NullCheck(L_2);
		VirtActionInvoker1< Font_t1222 * >::Invoke(10 /* System.Void System.Action`1<UnityEngine.Font>::Invoke(!0) */, L_2, L_3);
	}

IL_0013:
	{
		Font_t1222 * L_4 = ___font;
		NullCheck(L_4);
		FontTextureRebuildCallback_t1602 * L_5 = (L_4->___m_FontTextureRebuildCallback_3);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Font_t1222 * L_6 = ___font;
		NullCheck(L_6);
		FontTextureRebuildCallback_t1602 * L_7 = (L_6->___m_FontTextureRebuildCallback_3);
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(10 /* System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke() */, L_7);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C" bool Font_get_dynamic_m6113 (Font_t1222 * __this, MethodInfo* method)
{
	typedef bool (*Font_get_dynamic_m6113_ftn) (Font_t1222 *);
	static Font_get_dynamic_m6113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_dynamic_m6113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C" int32_t Font_get_fontSize_m6115 (Font_t1222 * __this, MethodInfo* method)
{
	typedef int32_t (*Font_get_fontSize_m6115_ftn) (Font_t1222 *);
	static Font_get_fontSize_m6115_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontSize_m6115_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"



// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"



// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_33.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_45.h"
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_46.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_33MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"


// System.Void UnityEngine.TextGenerator::.ctor()
extern "C" void TextGenerator__ctor_m5973 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	{
		TextGenerator__ctor_m6108(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern TypeInfo* List_1_t1267_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1603_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1604_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m7513_MethodInfo_var;
extern MethodInfo* List_1__ctor_m7514_MethodInfo_var;
extern MethodInfo* List_1__ctor_m7515_MethodInfo_var;
extern "C" void TextGenerator__ctor_m6108 (TextGenerator_t1266 * __this, int32_t ___initialCapacity, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2230);
		List_1_t1603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2907);
		List_1_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2908);
		List_1__ctor_m7513_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484568);
		List_1__ctor_m7514_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484569);
		List_1__ctor_m7515_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484570);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t1267_il2cpp_TypeInfo_var);
		List_1_t1267 * L_1 = (List_1_t1267 *)il2cpp_codegen_object_new (List_1_t1267_il2cpp_TypeInfo_var);
		List_1__ctor_m7513(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m7513_MethodInfo_var);
		__this->___m_Verts_5 = L_1;
		int32_t L_2 = ___initialCapacity;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t1603_il2cpp_TypeInfo_var);
		List_1_t1603 * L_3 = (List_1_t1603 *)il2cpp_codegen_object_new (List_1_t1603_il2cpp_TypeInfo_var);
		List_1__ctor_m7514(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m7514_MethodInfo_var);
		__this->___m_Characters_6 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t1604_il2cpp_TypeInfo_var);
		List_1_t1604 * L_4 = (List_1_t1604 *)il2cpp_codegen_object_new (List_1_t1604_il2cpp_TypeInfo_var);
		List_1__ctor_m7515(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m7515_MethodInfo_var);
		__this->___m_Lines_7 = L_4;
		TextGenerator_Init_m7264(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C" void TextGenerator_System_IDisposable_Dispose_m7263 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m7265(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C" void TextGenerator_Init_m7264 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m7264_ftn) (TextGenerator_t1266 *);
	static TextGenerator_Init_m7264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m7264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C" void TextGenerator_Dispose_cpp_m7265 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m7265_ftn) (TextGenerator_t1266 *);
	static TextGenerator_Dispose_cpp_m7265_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m7265_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_m7266 (TextGenerator_t1266 * __this, String_t* ___str, Font_t1222 * ___font, Color_t747  ___color, int32_t ___fontSize, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, Vector2_t739  ___extents, Vector2_t739  ___pivot, bool ___generateOutOfBounds, MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t1222 * L_1 = ___font;
		Color_t747  L_2 = ___color;
		int32_t L_3 = ___fontSize;
		float L_4 = ___lineSpacing;
		int32_t L_5 = ___style;
		bool L_6 = ___richText;
		bool L_7 = ___resizeTextForBestFit;
		int32_t L_8 = ___resizeTextMinSize;
		int32_t L_9 = ___resizeTextMaxSize;
		int32_t L_10 = ___verticalOverFlow;
		int32_t L_11 = ___horizontalOverflow;
		bool L_12 = ___updateBounds;
		int32_t L_13 = ___anchor;
		float L_14 = ((&___extents)->___x_1);
		float L_15 = ((&___extents)->___y_2);
		float L_16 = ((&___pivot)->___x_1);
		float L_17 = ((&___pivot)->___y_2);
		bool L_18 = ___generateOutOfBounds;
		bool L_19 = TextGenerator_Populate_Internal_cpp_m7267(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_cpp_m7267 (TextGenerator_t1266 * __this, String_t* ___str, Font_t1222 * ___font, Color_t747  ___color, int32_t ___fontSize, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t1222 * L_1 = ___font;
		int32_t L_2 = ___fontSize;
		float L_3 = ___lineSpacing;
		int32_t L_4 = ___style;
		bool L_5 = ___richText;
		bool L_6 = ___resizeTextForBestFit;
		int32_t L_7 = ___resizeTextMinSize;
		int32_t L_8 = ___resizeTextMaxSize;
		int32_t L_9 = ___verticalOverFlow;
		int32_t L_10 = ___horizontalOverflow;
		bool L_11 = ___updateBounds;
		int32_t L_12 = ___anchor;
		float L_13 = ___extentsX;
		float L_14 = ___extentsY;
		float L_15 = ___pivotX;
		float L_16 = ___pivotY;
		bool L_17 = ___generateOutOfBounds;
		bool L_18 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7268(NULL /*static, unused*/, __this, L_0, L_1, (&___color), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7268 (Object_t * __this /* static, unused */, TextGenerator_t1266 * ___self, String_t* ___str, Font_t1222 * ___font, Color_t747 * ___color, int32_t ___fontSize, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7268_ftn) (TextGenerator_t1266 *, String_t*, Font_t1222 *, Color_t747 *, int32_t, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(___self, ___str, ___font, ___color, ___fontSize, ___lineSpacing, ___style, ___richText, ___resizeTextForBestFit, ___resizeTextMinSize, ___resizeTextMaxSize, ___verticalOverFlow, ___horizontalOverflow, ___updateBounds, ___anchor, ___extentsX, ___extentsY, ___pivotX, ___pivotY, ___generateOutOfBounds);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C" Rect_t738  TextGenerator_get_rectExtents_m6013 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef Rect_t738  (*TextGenerator_get_rectExtents_m6013_ftn) (TextGenerator_t1266 *);
	static TextGenerator_get_rectExtents_m6013_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_rectExtents_m6013_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_rectExtents()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C" int32_t TextGenerator_get_vertexCount_m7269 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m7269_ftn) (TextGenerator_t1266 *);
	static TextGenerator_get_vertexCount_m7269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m7269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C" void TextGenerator_GetVerticesInternal_m7270 (TextGenerator_t1266 * __this, Object_t * ___vertices, MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m7270_ftn) (TextGenerator_t1266 *, Object_t *);
	static TextGenerator_GetVerticesInternal_m7270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m7270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
extern "C" UIVertexU5BU5D_t1264* TextGenerator_GetVerticesArray_m7271 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef UIVertexU5BU5D_t1264* (*TextGenerator_GetVerticesArray_m7271_ftn) (TextGenerator_t1266 *);
	static TextGenerator_GetVerticesArray_m7271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesArray_m7271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C" int32_t TextGenerator_get_characterCount_m7272 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m7272_ftn) (TextGenerator_t1266 *);
	static TextGenerator_get_characterCount_m7272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m7272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t980_il2cpp_TypeInfo_var;
extern "C" int32_t TextGenerator_get_characterCountVisible_m5994 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Mathf_t980_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(981);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m899(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = (__this->___m_LastString_1);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m902(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m7269(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t980_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m6004(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m6006(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C" void TextGenerator_GetCharactersInternal_m7273 (TextGenerator_t1266 * __this, Object_t * ___characters, MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m7273_ftn) (TextGenerator_t1266 *, Object_t *);
	static TextGenerator_GetCharactersInternal_m7273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m7273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters);
}
// UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
extern "C" UICharInfoU5BU5D_t1669* TextGenerator_GetCharactersArray_m7274 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef UICharInfoU5BU5D_t1669* (*TextGenerator_GetCharactersArray_m7274_ftn) (TextGenerator_t1266 *);
	static TextGenerator_GetCharactersArray_m7274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersArray_m7274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C" int32_t TextGenerator_get_lineCount_m5993 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m5993_ftn) (TextGenerator_t1266 *);
	static TextGenerator_get_lineCount_m5993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m5993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C" void TextGenerator_GetLinesInternal_m7275 (TextGenerator_t1266 * __this, Object_t * ___lines, MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m7275_ftn) (TextGenerator_t1266 *, Object_t *);
	static TextGenerator_GetLinesInternal_m7275_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m7275_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines);
}
// UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
extern "C" UILineInfoU5BU5D_t1670* TextGenerator_GetLinesArray_m7276 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef UILineInfoU5BU5D_t1670* (*TextGenerator_GetLinesArray_m7276_ftn) (TextGenerator_t1266 *);
	static TextGenerator_GetLinesArray_m7276_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesArray_m7276_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
extern "C" int32_t TextGenerator_get_fontSizeUsedForBestFit_m6029 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_fontSizeUsedForBestFit_m6029_ftn) (TextGenerator_t1266 *);
	static TextGenerator_get_fontSizeUsedForBestFit_m6029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_fontSizeUsedForBestFit_m6029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern "C" void TextGenerator_Finalize_m7277 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1177(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern "C" TextGenerationSettings_t1364  TextGenerator_ValidatedSettings_m7278 (TextGenerator_t1266 * __this, TextGenerationSettings_t1364  ___settings, MethodInfo* method)
{
	{
		Font_t1222 * L_0 = ((&___settings)->___font_0);
		bool L_1 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t1222 * L_2 = ((&___settings)->___font_0);
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m6113(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t1364  L_4 = ___settings;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = ((&___settings)->___fontSize_2);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = ((&___settings)->___fontStyle_5);
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		Debug_LogWarning_m904(NULL /*static, unused*/, (String_t*) &_stringLiteral968, /*hidden argument*/NULL);
		(&___settings)->___fontSize_2 = 0;
		(&___settings)->___fontStyle_5 = 0;
	}

IL_0057:
	{
		bool L_7 = ((&___settings)->___resizeTextForBestFit_7);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		Debug_LogWarning_m904(NULL /*static, unused*/, (String_t*) &_stringLiteral969, /*hidden argument*/NULL);
		(&___settings)->___resizeTextForBestFit_7 = 0;
	}

IL_0075:
	{
		TextGenerationSettings_t1364  L_8 = ___settings;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C" void TextGenerator_Invalidate_m6112 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	{
		__this->___m_HasGenerated_3 = 0;
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C" void TextGenerator_GetCharacters_m7279 (TextGenerator_t1266 * __this, List_1_t1603 * ___characters, MethodInfo* method)
{
	{
		List_1_t1603 * L_0 = ___characters;
		TextGenerator_GetCharactersInternal_m7273(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C" void TextGenerator_GetLines_m7280 (TextGenerator_t1266 * __this, List_1_t1604 * ___lines, MethodInfo* method)
{
	{
		List_1_t1604 * L_0 = ___lines;
		TextGenerator_GetLinesInternal_m7275(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void TextGenerator_GetVertices_m7281 (TextGenerator_t1266 * __this, List_1_t1267 * ___vertices, MethodInfo* method)
{
	{
		List_1_t1267 * L_0 = ___vertices;
		TextGenerator_GetVerticesInternal_m7270(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredWidth_m6118 (TextGenerator_t1266 * __this, String_t* ___str, TextGenerationSettings_t1364  ___settings, MethodInfo* method)
{
	Rect_t738  V_0 = {0};
	{
		(&___settings)->___horizontalOverflow_12 = 1;
		(&___settings)->___verticalOverflow_11 = 1;
		(&___settings)->___updateBounds_10 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t1364  L_1 = ___settings;
		TextGenerator_Populate_m6012(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t738  L_2 = TextGenerator_get_rectExtents_m6013(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m4079((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredHeight_m6119 (TextGenerator_t1266 * __this, String_t* ___str, TextGenerationSettings_t1364  ___settings, MethodInfo* method)
{
	Rect_t738  V_0 = {0};
	{
		(&___settings)->___verticalOverflow_11 = 1;
		(&___settings)->___updateBounds_10 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t1364  L_1 = ___settings;
		TextGenerator_Populate_m6012(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t738  L_2 = TextGenerator_get_rectExtents_m6013(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m4080((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextGenerator_Populate_m6012 (TextGenerator_t1266 * __this, String_t* ___str, TextGenerationSettings_t1364  ___settings, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasGenerated_3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str;
		String_t* L_2 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m835(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t1364  L_4 = (__this->___m_LastSettings_2);
		bool L_5 = TextGenerationSettings_Equals_m7418((&___settings), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = (__this->___m_LastValid_4);
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str;
		TextGenerationSettings_t1364  L_8 = ___settings;
		bool L_9 = TextGenerator_PopulateAlways_m7282(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerator_PopulateAlways_m7282 (TextGenerator_t1266 * __this, String_t* ___str, TextGenerationSettings_t1364  ___settings, MethodInfo* method)
{
	TextGenerationSettings_t1364  V_0 = {0};
	{
		String_t* L_0 = ___str;
		__this->___m_LastString_1 = L_0;
		__this->___m_HasGenerated_3 = 1;
		__this->___m_CachedVerts_8 = 0;
		__this->___m_CachedCharacters_9 = 0;
		__this->___m_CachedLines_10 = 0;
		TextGenerationSettings_t1364  L_1 = ___settings;
		__this->___m_LastSettings_2 = L_1;
		TextGenerationSettings_t1364  L_2 = ___settings;
		TextGenerationSettings_t1364  L_3 = TextGenerator_ValidatedSettings_m7278(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str;
		Font_t1222 * L_5 = ((&V_0)->___font_0);
		Color_t747  L_6 = ((&V_0)->___color_1);
		int32_t L_7 = ((&V_0)->___fontSize_2);
		float L_8 = ((&V_0)->___lineSpacing_3);
		int32_t L_9 = ((&V_0)->___fontStyle_5);
		bool L_10 = ((&V_0)->___richText_4);
		bool L_11 = ((&V_0)->___resizeTextForBestFit_7);
		int32_t L_12 = ((&V_0)->___resizeTextMinSize_8);
		int32_t L_13 = ((&V_0)->___resizeTextMaxSize_9);
		int32_t L_14 = ((&V_0)->___verticalOverflow_11);
		int32_t L_15 = ((&V_0)->___horizontalOverflow_12);
		bool L_16 = ((&V_0)->___updateBounds_10);
		int32_t L_17 = ((&V_0)->___textAnchor_6);
		Vector2_t739  L_18 = ((&V_0)->___generationExtents_13);
		Vector2_t739  L_19 = ((&V_0)->___pivot_14);
		bool L_20 = ((&V_0)->___generateOutOfBounds_15);
		bool L_21 = TextGenerator_Populate_Internal_m7266(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, /*hidden argument*/NULL);
		__this->___m_LastValid_4 = L_21;
		bool L_22 = (__this->___m_LastValid_4);
		return L_22;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C" Object_t* TextGenerator_get_verts_m6117 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedVerts_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1267 * L_1 = (__this->___m_Verts_5);
		TextGenerator_GetVertices_m7281(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedVerts_8 = 1;
	}

IL_001e:
	{
		List_1_t1267 * L_2 = (__this->___m_Verts_5);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C" Object_t* TextGenerator_get_characters_m5995 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedCharacters_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1603 * L_1 = (__this->___m_Characters_6);
		TextGenerator_GetCharacters_m7279(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedCharacters_9 = 1;
	}

IL_001e:
	{
		List_1_t1603 * L_2 = (__this->___m_Characters_6);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C" Object_t* TextGenerator_get_lines_m5992 (TextGenerator_t1266 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedLines_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1604 * L_1 = (__this->___m_Lines_7);
		TextGenerator_GetLines_m7280(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedLines_10 = 1;
	}

IL_001e:
	{
		List_1_t1604 * L_2 = (__this->___m_Lines_7);
		return L_2;
	}
}
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"



// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"



// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C" void WillRenderCanvases__ctor_m5849 (WillRenderCanvases_t1381 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C" void WillRenderCanvases_Invoke_m7283 (WillRenderCanvases_t1381 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WillRenderCanvases_Invoke_m7283((WillRenderCanvases_t1381 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t1381(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * WillRenderCanvases_BeginInvoke_m7284 (WillRenderCanvases_t1381 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C" void WillRenderCanvases_EndInvoke_m7285 (WillRenderCanvases_t1381 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"



// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t1229_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t1381_il2cpp_TypeInfo_var;
extern "C" void Canvas_add_willRenderCanvases_m5850 (Object_t * __this /* static, unused */, WillRenderCanvases_t1381 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t1229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2238);
		WillRenderCanvases_t1381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2215);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t1381 * L_0 = ((Canvas_t1229_StaticFields*)Canvas_t1229_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t1381 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Combine_m952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t1229_StaticFields*)Canvas_t1229_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t1381 *)Castclass(L_2, WillRenderCanvases_t1381_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t1229_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t1381_il2cpp_TypeInfo_var;
extern "C" void Canvas_remove_willRenderCanvases_m7286 (Object_t * __this /* static, unused */, WillRenderCanvases_t1381 * ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t1229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2238);
		WillRenderCanvases_t1381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2215);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t1381 * L_0 = ((Canvas_t1229_StaticFields*)Canvas_t1229_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t1381 * L_1 = ___value;
		Delegate_t211 * L_2 = Delegate_Remove_m5890(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t1229_StaticFields*)Canvas_t1229_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t1381 *)Castclass(L_2, WillRenderCanvases_t1381_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C" int32_t Canvas_get_renderMode_m5895 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m5895_ftn) (Canvas_t1229 *);
	static Canvas_get_renderMode_m5895_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m5895_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C" bool Canvas_get_isRootCanvas_m6130 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m6130_ftn) (Canvas_t1229 *);
	static Canvas_get_isRootCanvas_m6130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m6130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C" Camera_t978 * Canvas_get_worldCamera_m5906 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef Camera_t978 * (*Canvas_get_worldCamera_m5906_ftn) (Canvas_t1229 *);
	static Canvas_get_worldCamera_m5906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m5906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C" float Canvas_get_scaleFactor_m6114 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m6114_ftn) (Canvas_t1229 *);
	static Canvas_get_scaleFactor_m6114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m6114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C" void Canvas_set_scaleFactor_m6134 (Canvas_t1229 * __this, float ___value, MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m6134_ftn) (Canvas_t1229 *, float);
	static Canvas_set_scaleFactor_m6134_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m6134_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C" float Canvas_get_referencePixelsPerUnit_m5925 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m5925_ftn) (Canvas_t1229 *);
	static Canvas_get_referencePixelsPerUnit_m5925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m5925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C" void Canvas_set_referencePixelsPerUnit_m6135 (Canvas_t1229 * __this, float ___value, MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m6135_ftn) (Canvas_t1229 *, float);
	static Canvas_set_referencePixelsPerUnit_m6135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m6135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C" bool Canvas_get_pixelPerfect_m5882 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m5882_ftn) (Canvas_t1229 *);
	static Canvas_get_pixelPerfect_m5882_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m5882_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C" int32_t Canvas_get_renderOrder_m5897 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m5897_ftn) (Canvas_t1229 *);
	static Canvas_get_renderOrder_m5897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m5897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C" int32_t Canvas_get_sortingOrder_m5896 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m5896_ftn) (Canvas_t1229 *);
	static Canvas_get_sortingOrder_m5896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m5896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_sortingLayerID()
extern "C" int32_t Canvas_get_sortingLayerID_m5905 (Canvas_t1229 * __this, MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingLayerID_m5905_ftn) (Canvas_t1229 *);
	static Canvas_get_sortingLayerID_m5905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingLayerID_m5905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C" Material_t989 * Canvas_GetDefaultCanvasMaterial_m5870 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef Material_t989 * (*Canvas_GetDefaultCanvasMaterial_m5870_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m5870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m5870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasTextMaterial()
extern "C" Material_t989 * Canvas_GetDefaultCanvasTextMaterial_m6109 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef Material_t989 * (*Canvas_GetDefaultCanvasTextMaterial_m6109_ftn) ();
	static Canvas_GetDefaultCanvasTextMaterial_m6109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasTextMaterial_m6109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasTextMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern TypeInfo* Canvas_t1229_il2cpp_TypeInfo_var;
extern "C" void Canvas_SendWillRenderCanvases_m7287 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t1229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2238);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t1381 * L_0 = ((Canvas_t1229_StaticFields*)Canvas_t1229_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t1381 * L_1 = ((Canvas_t1229_StaticFields*)Canvas_t1229_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(10 /* System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke() */, L_1);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C" void Canvas_ForceUpdateCanvases_m6059 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m7287(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"



// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C" bool CanvasGroup_get_interactable_m6087 (CanvasGroup_t1387 * __this, MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m6087_ftn) (CanvasGroup_t1387 *);
	static CanvasGroup_get_interactable_m6087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m6087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C" bool CanvasGroup_get_blocksRaycasts_m7288 (CanvasGroup_t1387 * __this, MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m7288_ftn) (CanvasGroup_t1387 *);
	static CanvasGroup_get_blocksRaycasts_m7288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m7288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C" bool CanvasGroup_get_ignoreParentGroups_m5881 (CanvasGroup_t1387 * __this, MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m5881_ftn) (CanvasGroup_t1387 *);
	static CanvasGroup_get_ignoreParentGroups_m5881_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m5881_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C" bool CanvasGroup_IsRaycastLocationValid_m7289 (CanvasGroup_t1387 * __this, Vector2_t739  ___sp, Camera_t978 * ___eventCamera, MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m7288(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"

// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"


// System.Void UnityEngine.UIVertex::.cctor()
extern TypeInfo* UIVertex_t1265_il2cpp_TypeInfo_var;
extern "C" void UIVertex__cctor_m7290 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t1265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2231);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t1265  V_0 = {0};
	{
		Color32_t992  L_0 = {0};
		Color32__ctor_m4249(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t1265_StaticFields*)UIVertex_t1265_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6 = L_0;
		Vector4_t1362  L_1 = {0};
		Vector4__ctor_m5880(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t1265_StaticFields*)UIVertex_t1265_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_1;
		Initobj (UIVertex_t1265_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t758  L_2 = Vector3_get_zero_m4166(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_2;
		Vector3_t758  L_3 = Vector3_get_back_m6729(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___normal_1 = L_3;
		Vector4_t1362  L_4 = ((UIVertex_t1265_StaticFields*)UIVertex_t1265_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		(&V_0)->___tangent_5 = L_4;
		Color32_t992  L_5 = ((UIVertex_t1265_StaticFields*)UIVertex_t1265_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6;
		(&V_0)->___color_2 = L_5;
		Vector2_t739  L_6 = Vector2_get_zero_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv0_3 = L_6;
		Vector2_t739  L_7 = Vector2_get_zero_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_7;
		UIVertex_t1265  L_8 = V_0;
		((UIVertex_t1265_StaticFields*)UIVertex_t1265_il2cpp_TypeInfo_var->static_fields)->___simpleVert_8 = L_8;
		return;
	}
}
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"

// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// System.UInt16
#include "mscorlib_System_UInt16.h"
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"


// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C" void CanvasRenderer_SetColor_m5887 (CanvasRenderer_t1228 * __this, Color_t747  ___color, MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m7291(NULL /*static, unused*/, __this, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m7291 (Object_t * __this /* static, unused */, CanvasRenderer_t1228 * ___self, Color_t747 * ___color, MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m7291_ftn) (CanvasRenderer_t1228 *, Color_t747 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m7291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m7291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___color);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C" Color_t747  CanvasRenderer_GetColor_m5885 (CanvasRenderer_t1228 * __this, MethodInfo* method)
{
	typedef Color_t747  (*CanvasRenderer_GetColor_m5885_ftn) (CanvasRenderer_t1228 *);
	static CanvasRenderer_GetColor_m5885_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_GetColor_m5885_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetColor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_isMask(System.Boolean)
extern "C" void CanvasRenderer_set_isMask_m6165 (CanvasRenderer_t1228 * __this, bool ___value, MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_isMask_m6165_ftn) (CanvasRenderer_t1228 *, bool);
	static CanvasRenderer_set_isMask_m6165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_isMask_m6165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_isMask(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C" void CanvasRenderer_SetMaterial_m5879 (CanvasRenderer_t1228 * __this, Material_t989 * ___material, Texture_t736 * ___texture, MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m5879_ftn) (CanvasRenderer_t1228 *, Material_t989 *, Texture_t736 *);
	static CanvasRenderer_SetMaterial_m5879_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m5879_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___material, ___texture);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t194_il2cpp_TypeInfo_var;
extern "C" void CanvasRenderer_SetVertices_m5877 (CanvasRenderer_t1228 * __this, List_1_t1267 * ___vertices, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		UInt16_t194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1267 * L_0 = ___vertices;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0039;
		}
	}
	{
		ObjectU5BU5D_t208* L_2 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 1));
		uint16_t L_3 = ((int32_t)65535);
		Object_t * L_4 = Box(UInt16_t194_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0)) = (Object_t *)L_4;
		String_t* L_5 = UnityString_Format_m6960(NULL /*static, unused*/, (String_t*) &_stringLiteral970, L_2, /*hidden argument*/NULL);
		Debug_LogWarning_m6105(NULL /*static, unused*/, L_5, __this, /*hidden argument*/NULL);
		List_1_t1267 * L_6 = ___vertices;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear() */, L_6);
	}

IL_0039:
	{
		List_1_t1267 * L_7 = ___vertices;
		CanvasRenderer_SetVerticesInternal_m7292(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)
extern "C" void CanvasRenderer_SetVerticesInternal_m7292 (CanvasRenderer_t1228 * __this, Object_t * ___vertices, MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternal_m7292_ftn) (CanvasRenderer_t1228 *, Object_t *);
	static CanvasRenderer_SetVerticesInternal_m7292_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternal_m7292_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(UnityEngine.UIVertex[],System.Int32)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t194_il2cpp_TypeInfo_var;
extern "C" void CanvasRenderer_SetVertices_m5979 (CanvasRenderer_t1228 * __this, UIVertexU5BU5D_t1264* ___vertices, int32_t ___size, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		UInt16_t194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t208* L_1 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 1));
		uint16_t L_2 = ((int32_t)65535);
		Object_t * L_3 = Box(UInt16_t194_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)L_3;
		String_t* L_4 = UnityString_Format_m6960(NULL /*static, unused*/, (String_t*) &_stringLiteral970, L_1, /*hidden argument*/NULL);
		Debug_LogWarning_m6105(NULL /*static, unused*/, L_4, __this, /*hidden argument*/NULL);
		___size = 0;
	}

IL_0031:
	{
		UIVertexU5BU5D_t1264* L_5 = ___vertices;
		int32_t L_6 = ___size;
		CanvasRenderer_SetVerticesInternalArray_m7293(__this, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)
extern "C" void CanvasRenderer_SetVerticesInternalArray_m7293 (CanvasRenderer_t1228 * __this, UIVertexU5BU5D_t1264* ___vertices, int32_t ___size, MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternalArray_m7293_ftn) (CanvasRenderer_t1228 *, UIVertexU5BU5D_t1264*, int32_t);
	static CanvasRenderer_SetVerticesInternalArray_m7293_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternalArray_m7293_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)");
	_il2cpp_icall_func(__this, ___vertices, ___size);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C" void CanvasRenderer_Clear_m5874 (CanvasRenderer_t1228 * __this, MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m5874_ftn) (CanvasRenderer_t1228 *);
	static CanvasRenderer_Clear_m5874_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m5874_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C" int32_t CanvasRenderer_get_absoluteDepth_m5871 (CanvasRenderer_t1228 * __this, MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m5871_ftn) (CanvasRenderer_t1228 *);
	static CanvasRenderer_get_absoluteDepth_m5871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m5871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"

// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"


// System.Void UnityEngine.RectTransformUtility::.cctor()
extern TypeInfo* Vector3U5BU5D_t1284_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility__cctor_m7294 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2294);
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t1389_StaticFields*)RectTransformUtility_t1389_il2cpp_TypeInfo_var->static_fields)->___s_Corners_0 = ((Vector3U5BU5D_t1284*)SZArrayNew(Vector3U5BU5D_t1284_il2cpp_TypeInfo_var, 4));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_RectangleContainsScreenPoint_m5907 (Object_t * __this /* static, unused */, RectTransform_t1227 * ___rect, Vector2_t739  ___screenPoint, Camera_t978 * ___cam, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t1227 * L_0 = ___rect;
		Camera_t978 * L_1 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7295(NULL /*static, unused*/, L_0, (&___screenPoint), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C" bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7295 (Object_t * __this /* static, unused */, RectTransform_t1227 * ___rect, Vector2_t739 * ___screenPoint, Camera_t978 * ___cam, MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7295_ftn) (RectTransform_t1227 *, Vector2_t739 *, Camera_t978 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect, ___screenPoint, ___cam);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" Vector2_t739  RectTransformUtility_PixelAdjustPoint_m5883 (Object_t * __this /* static, unused */, Vector2_t739  ___point, Transform_t809 * ___elementTransform, Canvas_t1229 * ___canvas, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t739  V_0 = {0};
	{
		Vector2_t739  L_0 = ___point;
		Transform_t809 * L_1 = ___elementTransform;
		Canvas_t1229 * L_2 = ___canvas;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m7296(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t739  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_PixelAdjustPoint_m7296 (Object_t * __this /* static, unused */, Vector2_t739  ___point, Transform_t809 * ___elementTransform, Canvas_t1229 * ___canvas, Vector2_t739 * ___output, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t809 * L_0 = ___elementTransform;
		Canvas_t1229 * L_1 = ___canvas;
		Vector2_t739 * L_2 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7297(NULL /*static, unused*/, (&___point), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7297 (Object_t * __this /* static, unused */, Vector2_t739 * ___point, Transform_t809 * ___elementTransform, Canvas_t1229 * ___canvas, Vector2_t739 * ___output, MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7297_ftn) (Vector2_t739 *, Transform_t809 *, Canvas_t1229 *, Vector2_t739 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point, ___elementTransform, ___canvas, ___output);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t738  RectTransformUtility_PixelAdjustRect_m5884 (Object_t * __this /* static, unused */, RectTransform_t1227 * ___rectTransform, Canvas_t1229 * ___canvas, MethodInfo* method)
{
	typedef Rect_t738  (*RectTransformUtility_PixelAdjustRect_m5884_ftn) (RectTransform_t1227 *, Canvas_t1229 *);
	static RectTransformUtility_PixelAdjustRect_m5884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_PixelAdjustRect_m5884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)");
	return _il2cpp_icall_func(___rectTransform, ___canvas);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m7298 (Object_t * __this /* static, unused */, RectTransform_t1227 * ___rect, Vector2_t739  ___screenPoint, Camera_t978 * ___cam, Vector3_t758 * ___worldPoint, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t1375  V_0 = {0};
	Plane_t1397  V_1 = {0};
	float V_2 = 0.0f;
	{
		Vector3_t758 * L_0 = ___worldPoint;
		Vector2_t739  L_1 = Vector2_get_zero_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t758  L_2 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		*L_0 = L_2;
		Camera_t978 * L_3 = ___cam;
		Vector2_t739  L_4 = ___screenPoint;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		Ray_t1375  L_5 = RectTransformUtility_ScreenPointToRay_m7299(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t1227 * L_6 = ___rect;
		NullCheck(L_6);
		Quaternion_t771  L_7 = Transform_get_rotation_m4131(L_6, /*hidden argument*/NULL);
		Vector3_t758  L_8 = Vector3_get_back_m6729(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t758  L_9 = Quaternion_op_Multiply_m5902(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t1227 * L_10 = ___rect;
		NullCheck(L_10);
		Vector3_t758  L_11 = Transform_get_position_m4034(L_10, /*hidden argument*/NULL);
		Plane__ctor_m5989((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t1375  L_12 = V_0;
		bool L_13 = Plane_Raycast_m5990((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		Vector3_t758 * L_14 = ___worldPoint;
		float L_15 = V_2;
		Vector3_t758  L_16 = Ray_GetPoint_m5991((&V_0), L_15, /*hidden argument*/NULL);
		*L_14 = L_16;
		return 1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m5944 (Object_t * __this /* static, unused */, RectTransform_t1227 * ___rect, Vector2_t739  ___screenPoint, Camera_t978 * ___cam, Vector2_t739 * ___localPoint, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t758  V_0 = {0};
	{
		Vector2_t739 * L_0 = ___localPoint;
		Vector2_t739  L_1 = Vector2_get_zero_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_0 = L_1;
		RectTransform_t1227 * L_2 = ___rect;
		Vector2_t739  L_3 = ___screenPoint;
		Camera_t978 * L_4 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m7298(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t739 * L_6 = ___localPoint;
		RectTransform_t1227 * L_7 = ___rect;
		Vector3_t758  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t758  L_9 = Transform_InverseTransformPoint_m5988(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t739  L_10 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		*L_6 = L_10;
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C" Ray_t1375  RectTransformUtility_ScreenPointToRay_m7299 (Object_t * __this /* static, unused */, Camera_t978 * ___cam, Vector2_t739  ___screenPos, MethodInfo* method)
{
	Vector3_t758  V_0 = {0};
	{
		Camera_t978 * L_0 = ___cam;
		bool L_1 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t978 * L_2 = ___cam;
		Vector2_t739  L_3 = ___screenPos;
		Vector3_t758  L_4 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t1375  L_5 = Camera_ScreenPointToRay_m5821(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t739  L_6 = ___screenPos;
		Vector3_t758  L_7 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t758 * L_8 = (&V_0);
		float L_9 = (L_8->___z_3);
		L_8->___z_3 = ((float)((float)L_9-(float)(100.0f)));
		Vector3_t758  L_10 = V_0;
		Vector3_t758  L_11 = Vector3_get_forward_m4179(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t1375  L_12 = {0};
		Ray__ctor_m6821(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t1227_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m6054 (Object_t * __this /* static, unused */, RectTransform_t1227 * ___rect, int32_t ___axis, bool ___keepPositioning, bool ___recursive, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2237);
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t1227 * V_1 = {0};
	Vector2_t739  V_2 = {0};
	Vector2_t739  V_3 = {0};
	Vector2_t739  V_4 = {0};
	Vector2_t739  V_5 = {0};
	float V_6 = 0.0f;
	{
		RectTransform_t1227 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m848(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t1227 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t809 * L_5 = Transform_GetChild_m4050(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t1227 *)IsInst(L_5, RectTransform_t1227_il2cpp_TypeInfo_var));
		RectTransform_t1227 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_6, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t1227 * L_8 = V_1;
		int32_t L_9 = ___axis;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m6054(NULL /*static, unused*/, L_8, L_9, 0, 1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t1227 * L_12 = ___rect;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m4051(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t1227 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t739  L_15 = RectTransform_get_pivot_m5930(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis;
		int32_t L_17 = ___axis;
		float L_18 = Vector2_get_Item_m5941((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m5950((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t1227 * L_19 = ___rect;
		Vector2_t739  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m6027(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t1227 * L_22 = ___rect;
		NullCheck(L_22);
		Vector2_t739  L_23 = RectTransform_get_anchoredPosition_m6023(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis;
		int32_t L_25 = ___axis;
		float L_26 = Vector2_get_Item_m5941((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m5950((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t1227 * L_27 = ___rect;
		Vector2_t739  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m6026(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t1227 * L_29 = ___rect;
		NullCheck(L_29);
		Vector2_t739  L_30 = RectTransform_get_anchorMin_m5931(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t1227 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t739  L_32 = RectTransform_get_anchorMax_m6022(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis;
		float L_34 = Vector2_get_Item_m5941((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis;
		int32_t L_36 = ___axis;
		float L_37 = Vector2_get_Item_m5941((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m5950((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis;
		float L_39 = V_6;
		Vector2_set_Item_m5950((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t1227 * L_40 = ___rect;
		Vector2_t739  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m6025(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t1227 * L_42 = ___rect;
		Vector2_t739  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m5932(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t1227_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t1389_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutAxes_m6053 (Object_t * __this /* static, unused */, RectTransform_t1227 * ___rect, bool ___keepPositioning, bool ___recursive, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2237);
		RectTransformUtility_t1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t1227 * V_1 = {0};
	{
		RectTransform_t1227 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m848(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t1227 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t809 * L_5 = Transform_GetChild_m4050(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t1227 *)IsInst(L_5, RectTransform_t1227_il2cpp_TypeInfo_var));
		RectTransform_t1227 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_6, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t1227 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m6053(NULL /*static, unused*/, L_8, 0, 1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t1227 * L_11 = ___rect;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m4051(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t1227 * L_13 = ___rect;
		RectTransform_t1227 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t739  L_15 = RectTransform_get_pivot_m5930(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		Vector2_t739  L_16 = RectTransformUtility_GetTransposed_m7300(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m6027(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t1227 * L_17 = ___rect;
		RectTransform_t1227 * L_18 = ___rect;
		NullCheck(L_18);
		Vector2_t739  L_19 = RectTransform_get_sizeDelta_m6024(L_18, /*hidden argument*/NULL);
		Vector2_t739  L_20 = RectTransformUtility_GetTransposed_m7300(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m5933(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t1227 * L_22 = ___rect;
		RectTransform_t1227 * L_23 = ___rect;
		NullCheck(L_23);
		Vector2_t739  L_24 = RectTransform_get_anchoredPosition_m6023(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1389_il2cpp_TypeInfo_var);
		Vector2_t739  L_25 = RectTransformUtility_GetTransposed_m7300(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m6026(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t1227 * L_26 = ___rect;
		RectTransform_t1227 * L_27 = ___rect;
		NullCheck(L_27);
		Vector2_t739  L_28 = RectTransform_get_anchorMin_m5931(L_27, /*hidden argument*/NULL);
		Vector2_t739  L_29 = RectTransformUtility_GetTransposed_m7300(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m6025(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t1227 * L_30 = ___rect;
		RectTransform_t1227 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t739  L_32 = RectTransform_get_anchorMax_m6022(L_31, /*hidden argument*/NULL);
		Vector2_t739  L_33 = RectTransformUtility_GetTransposed_m7300(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m5932(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C" Vector2_t739  RectTransformUtility_GetTransposed_m7300 (Object_t * __this /* static, unused */, Vector2_t739  ___input, MethodInfo* method)
{
	{
		float L_0 = ((&___input)->___y_2);
		float L_1 = ((&___input)->___x_1);
		Vector2_t739  L_2 = {0};
		Vector2__ctor_m4033(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// AOT.MonoPInvokeCallbackAttribute
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute.h"
#ifndef _MSC_VER
#else
#endif
// AOT.MonoPInvokeCallbackAttribute
#include "UnityEngine_AOT_MonoPInvokeCallbackAttributeMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"


// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C" void MonoPInvokeCallbackAttribute__ctor_m4449 (MonoPInvokeCallbackAttribute_t1010 * __this, Type_t * ___type, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"



// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C" void WrapperlessIcall__ctor_m7301 (WrapperlessIcall_t1606 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"



// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m7302 (IL2CPPStructAlignmentAttribute_t1607 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		__this->___Align_0 = 1;
		return;
	}
}
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"

// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// System.Collections.Generic.Stack`1<System.Type>
#include "System_System_Collections_Generic_Stack_1_gen.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.Collections.Generic.List`1<System.Type>
#include "mscorlib_System_Collections_Generic_List_1_gen_47.h"
// System.Collections.Generic.Stack`1<System.Type>
#include "System_System_Collections_Generic_Stack_1_genMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Type>
#include "mscorlib_System_Collections_Generic_List_1_gen_47MethodDeclarations.h"


// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern TypeInfo* DisallowMultipleComponentU5BU5D_t1608_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeHelperEngine_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditModeU5BU5D_t1609_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t1610_il2cpp_TypeInfo_var;
extern "C" void AttributeHelperEngine__cctor_m7303 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponentU5BU5D_t1608_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2909);
		AttributeHelperEngine_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2910);
		ExecuteInEditModeU5BU5D_t1609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2911);
		RequireComponentU5BU5D_t1610_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2912);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AttributeHelperEngine_t1611_StaticFields*)AttributeHelperEngine_t1611_il2cpp_TypeInfo_var->static_fields)->____disallowMultipleComponentArray_0 = ((DisallowMultipleComponentU5BU5D_t1608*)SZArrayNew(DisallowMultipleComponentU5BU5D_t1608_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t1611_StaticFields*)AttributeHelperEngine_t1611_il2cpp_TypeInfo_var->static_fields)->____executeInEditModeArray_1 = ((ExecuteInEditModeU5BU5D_t1609*)SZArrayNew(ExecuteInEditModeU5BU5D_t1609_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t1611_StaticFields*)AttributeHelperEngine_t1611_il2cpp_TypeInfo_var->static_fields)->____requireComponentArray_2 = ((RequireComponentU5BU5D_t1610*)SZArrayNew(RequireComponentU5BU5D_t1610_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t26_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t1437_0_0_0_var;
extern TypeInfo* Stack_1_t1686_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern MethodInfo* Stack_1__ctor_m7516_MethodInfo_var;
extern MethodInfo* Stack_1_Push_m7517_MethodInfo_var;
extern MethodInfo* Stack_1_Pop_m7518_MethodInfo_var;
extern "C" Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m7304 (Object_t * __this /* static, unused */, Type_t * ___type, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t26_0_0_0_var = il2cpp_codegen_type_from_index(156);
		DisallowMultipleComponent_t1437_0_0_0_var = il2cpp_codegen_type_from_index(2356);
		Stack_1_t1686_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		Stack_1__ctor_m7516_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484571);
		Stack_1_Push_m7517_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484572);
		Stack_1_Pop_m7518_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484573);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t1686 * V_0 = {0};
	Type_t * V_1 = {0};
	ObjectU5BU5D_t208* V_2 = {0};
	{
		Stack_1_t1686 * L_0 = (Stack_1_t1686 *)il2cpp_codegen_object_new (Stack_1_t1686_il2cpp_TypeInfo_var);
		Stack_1__ctor_m7516(L_0, /*hidden argument*/Stack_1__ctor_m7516_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t1686 * L_1 = V_0;
		Type_t * L_2 = ___type;
		NullCheck(L_1);
		Stack_1_Push_m7517(L_1, L_2, /*hidden argument*/Stack_1_Push_m7517_MethodInfo_var);
		Type_t * L_3 = ___type;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t26_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_6) == ((Object_t*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005a;
	}

IL_0037:
	{
		Stack_1_t1686 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m7518(L_8, /*hidden argument*/Stack_1_Pop_m7518_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t1437_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t208* L_12 = (ObjectU5BU5D_t208*)VirtFuncInvoker2< ObjectU5BU5D_t208*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, 0);
		V_2 = L_12;
		ObjectU5BU5D_t208* L_13 = V_2;
		NullCheck(L_13);
		if (!(((int32_t)(((Array_t *)L_13)->max_length))))
		{
			goto IL_005a;
		}
	}
	{
		Type_t * L_14 = V_1;
		return L_14;
	}

IL_005a:
	{
		Stack_1_t1686 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count() */, L_15);
		if ((((int32_t)L_16) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t1432_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t26_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1671_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1687_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m7519_MethodInfo_var;
extern MethodInfo* List_1_ToArray_m7520_MethodInfo_var;
extern "C" TypeU5BU5D_t1671* AttributeHelperEngine_GetRequiredComponents_m7305 (Object_t * __this /* static, unused */, Type_t * ___klass, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RequireComponent_t1432_0_0_0_var = il2cpp_codegen_type_from_index(2354);
		MonoBehaviour_t26_0_0_0_var = il2cpp_codegen_type_from_index(156);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		TypeU5BU5D_t1671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2914);
		List_1_t1687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2915);
		List_1__ctor_m7519_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484574);
		List_1_ToArray_m7520_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484575);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1687 * V_0 = {0};
	ObjectU5BU5D_t208* V_1 = {0};
	int32_t V_2 = 0;
	RequireComponent_t1432 * V_3 = {0};
	TypeU5BU5D_t1671* V_4 = {0};
	{
		V_0 = (List_1_t1687 *)NULL;
		goto IL_00d9;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t1432_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t208* L_2 = (ObjectU5BU5D_t208*)VirtFuncInvoker2< ObjectU5BU5D_t208*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_1 = L_2;
		V_2 = 0;
		goto IL_00c8;
	}

IL_0020:
	{
		ObjectU5BU5D_t208* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_3 = ((RequireComponent_t1432 *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5)), RequireComponent_t1432_il2cpp_TypeInfo_var));
		List_1_t1687 * L_6 = V_0;
		if (L_6)
		{
			goto IL_0073;
		}
	}
	{
		ObjectU5BU5D_t208* L_7 = V_1;
		NullCheck(L_7);
		if ((!(((uint32_t)(((int32_t)(((Array_t *)L_7)->max_length)))) == ((uint32_t)1))))
		{
			goto IL_0073;
		}
	}
	{
		Type_t * L_8 = ___klass;
		NullCheck(L_8);
		Type_t * L_9 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t26_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_9) == ((Object_t*)(Type_t *)L_10))))
		{
			goto IL_0073;
		}
	}
	{
		TypeU5BU5D_t1671* L_11 = ((TypeU5BU5D_t1671*)SZArrayNew(TypeU5BU5D_t1671_il2cpp_TypeInfo_var, 3));
		RequireComponent_t1432 * L_12 = V_3;
		NullCheck(L_12);
		Type_t * L_13 = (L_12->___m_Type0_0);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_13);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_11, 0)) = (Type_t *)L_13;
		TypeU5BU5D_t1671* L_14 = L_11;
		RequireComponent_t1432 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = (L_15->___m_Type1_1);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		ArrayElementTypeCheck (L_14, L_16);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_14, 1)) = (Type_t *)L_16;
		TypeU5BU5D_t1671* L_17 = L_14;
		RequireComponent_t1432 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = (L_18->___m_Type2_2);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		ArrayElementTypeCheck (L_17, L_19);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_17, 2)) = (Type_t *)L_19;
		V_4 = L_17;
		TypeU5BU5D_t1671* L_20 = V_4;
		return L_20;
	}

IL_0073:
	{
		List_1_t1687 * L_21 = V_0;
		if (L_21)
		{
			goto IL_007f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t1687_il2cpp_TypeInfo_var);
		List_1_t1687 * L_22 = (List_1_t1687 *)il2cpp_codegen_object_new (List_1_t1687_il2cpp_TypeInfo_var);
		List_1__ctor_m7519(L_22, /*hidden argument*/List_1__ctor_m7519_MethodInfo_var);
		V_0 = L_22;
	}

IL_007f:
	{
		RequireComponent_t1432 * L_23 = V_3;
		NullCheck(L_23);
		Type_t * L_24 = (L_23->___m_Type0_0);
		if (!L_24)
		{
			goto IL_0096;
		}
	}
	{
		List_1_t1687 * L_25 = V_0;
		RequireComponent_t1432 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = (L_26->___m_Type0_0);
		NullCheck(L_25);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_25, L_27);
	}

IL_0096:
	{
		RequireComponent_t1432 * L_28 = V_3;
		NullCheck(L_28);
		Type_t * L_29 = (L_28->___m_Type1_1);
		if (!L_29)
		{
			goto IL_00ad;
		}
	}
	{
		List_1_t1687 * L_30 = V_0;
		RequireComponent_t1432 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = (L_31->___m_Type1_1);
		NullCheck(L_30);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_30, L_32);
	}

IL_00ad:
	{
		RequireComponent_t1432 * L_33 = V_3;
		NullCheck(L_33);
		Type_t * L_34 = (L_33->___m_Type2_2);
		if (!L_34)
		{
			goto IL_00c4;
		}
	}
	{
		List_1_t1687 * L_35 = V_0;
		RequireComponent_t1432 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = (L_36->___m_Type2_2);
		NullCheck(L_35);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_35, L_37);
	}

IL_00c4:
	{
		int32_t L_38 = V_2;
		V_2 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_39 = V_2;
		ObjectU5BU5D_t208* L_40 = V_1;
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)(((Array_t *)L_40)->max_length))))))
		{
			goto IL_0020;
		}
	}
	{
		Type_t * L_41 = ___klass;
		NullCheck(L_41);
		Type_t * L_42 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_41);
		___klass = L_42;
	}

IL_00d9:
	{
		Type_t * L_43 = ___klass;
		if (!L_43)
		{
			goto IL_00ef;
		}
	}
	{
		Type_t * L_44 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t26_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_44) == ((Object_t*)(Type_t *)L_45))))
		{
			goto IL_0007;
		}
	}

IL_00ef:
	{
		List_1_t1687 * L_46 = V_0;
		if (L_46)
		{
			goto IL_00f7;
		}
	}
	{
		return (TypeU5BU5D_t1671*)NULL;
	}

IL_00f7:
	{
		List_1_t1687 * L_47 = V_0;
		NullCheck(L_47);
		TypeU5BU5D_t1671* L_48 = List_1_ToArray_m7520(L_47, /*hidden argument*/List_1_ToArray_m7520_MethodInfo_var);
		return L_48;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t1438_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t26_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool AttributeHelperEngine_CheckIsEditorScript_m7306 (Object_t * __this /* static, unused */, Type_t * ___klass, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t1438_0_0_0_var = il2cpp_codegen_type_from_index(2357);
		MonoBehaviour_t26_0_0_0_var = il2cpp_codegen_type_from_index(156);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t208* V_0 = {0};
	{
		goto IL_0029;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t1438_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t208* L_2 = (ObjectU5BU5D_t208*)VirtFuncInvoker2< ObjectU5BU5D_t208*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_0 = L_2;
		ObjectU5BU5D_t208* L_3 = V_0;
		NullCheck(L_3);
		if (!(((int32_t)(((Array_t *)L_3)->max_length))))
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		Type_t * L_4 = ___klass;
		NullCheck(L_4);
		Type_t * L_5 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_4);
		___klass = L_5;
	}

IL_0029:
	{
		Type_t * L_6 = ___klass;
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_7 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t26_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0005;
		}
	}

IL_003f:
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"



// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C" void DisallowMultipleComponent__ctor_m6238 (DisallowMultipleComponent_t1437 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"



// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C" void RequireComponent__ctor_m6214 (RequireComponent_t1432 * __this, Type_t * ___requiredComponent, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent;
		__this->___m_Type0_0 = L_0;
		return;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"



// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C" void AddComponentMenu__ctor_m6201 (AddComponentMenu_t1429 * __this, String_t* ___menuName, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		__this->___m_Ordering_1 = 0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C" void AddComponentMenu__ctor_m6233 (AddComponentMenu_t1429 * __this, String_t* ___menuName, int32_t ___order, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		int32_t L_1 = ___order;
		__this->___m_Ordering_1 = L_1;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"



// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C" void ExecuteInEditMode__ctor_m6239 (ExecuteInEditMode_t1438 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"

// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.Binder
#include "mscorlib_System_Reflection_Binder.h"
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
// System.Globalization.CultureInfo
#include "mscorlib_System_Globalization_CultureInfo.h"


// System.Void UnityEngine.SetupCoroutine::.ctor()
extern "C" void SetupCoroutine__ctor_m7307 (SetupCoroutine_t1612 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeMember_m7308 (Object_t * __this /* static, unused */, Object_t * ___behaviour, String_t* ___name, Object_t * ___variable, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t208* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t208*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t208* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)L_2;
	}

IL_0013:
	{
		Object_t * L_3 = ___behaviour;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m932(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name;
		Object_t * L_6 = ___behaviour;
		ObjectU5BU5D_t208* L_7 = V_0;
		NullCheck(L_4);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1688 *, Object_t *, ObjectU5BU5D_t208*, ParameterModifierU5BU5D_t1689*, CultureInfo_t232 *, StringU5BU5D_t169* >::Invoke(70 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1688 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t1689*)(ParameterModifierU5BU5D_t1689*)NULL, (CultureInfo_t232 *)NULL, (StringU5BU5D_t169*)(StringU5BU5D_t169*)NULL);
		return L_8;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeStatic_m7309 (Object_t * __this /* static, unused */, Type_t * ___klass, String_t* ___name, Object_t * ___variable, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t208* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t208*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t208* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)L_2;
	}

IL_0013:
	{
		Type_t * L_3 = ___klass;
		String_t* L_4 = ___name;
		ObjectU5BU5D_t208* L_5 = V_0;
		NullCheck(L_3);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1688 *, Object_t *, ObjectU5BU5D_t208*, ParameterModifierU5BU5D_t1689*, CultureInfo_t232 *, StringU5BU5D_t169* >::Invoke(70 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_3, L_4, ((int32_t)312), (Binder_t1688 *)NULL, NULL, L_5, (ParameterModifierU5BU5D_t1689*)(ParameterModifierU5BU5D_t1689*)NULL, (CultureInfo_t232 *)NULL, (StringU5BU5D_t169*)(StringU5BU5D_t169*)NULL);
		return L_6;
	}
}
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"



// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m7310 (WritableAttribute_t1613 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"



// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C" void AssemblyIsEditorAssembly__ctor_m7311 (AssemblyIsEditorAssembly_t1614 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"

// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"


// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern TypeInfo* UserProfile_t1624_il2cpp_TypeInfo_var;
extern "C" UserProfile_t1624 * GcUserProfileData_ToUserProfile_m7312 (GcUserProfileData_t1615 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfile_t1624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2852);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	{
		String_t* L_0 = (__this->___userName_0);
		String_t* L_1 = (__this->___userID_1);
		int32_t L_2 = (__this->___isFriend_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t384 * L_3 = (__this->___image_3);
		UserProfile_t1624 * L_4 = (UserProfile_t1624 *)il2cpp_codegen_object_new (UserProfile_t1624_il2cpp_TypeInfo_var);
		UserProfile__ctor_m7324(L_4, G_B3_2, G_B3_1, G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern "C" void GcUserProfileData_AddToArray_m7313 (GcUserProfileData_t1615 * __this, UserProfileU5BU5D_t1497** ___array, int32_t ___number, MethodInfo* method)
{
	{
		UserProfileU5BU5D_t1497** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t1497**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t1497**)L_0)))->max_length)))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t1497** L_3 = ___array;
		int32_t L_4 = ___number;
		UserProfile_t1624 * L_5 = GcUserProfileData_ToUserProfile_m7312(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t1497**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t1497**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t1497**)L_3)), L_5);
		*((UserProfile_t1624 **)(UserProfile_t1624 **)SZArrayLdElema((*((UserProfileU5BU5D_t1497**)L_3)), L_4)) = (UserProfile_t1624 *)L_5;
		goto IL_002a;
	}

IL_0020:
	{
		Debug_Log_m869(NULL /*static, unused*/, (String_t*) &_stringLiteral971, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"

// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"


// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern TypeInfo* AchievementDescription_t1626_il2cpp_TypeInfo_var;
extern "C" AchievementDescription_t1626 * GcAchievementDescriptionData_ToAchievementDescription_m7314 (GcAchievementDescriptionData_t1616 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescription_t1626_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2850);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	Texture2D_t384 * G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	String_t* G_B2_4 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	Texture2D_t384 * G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B1_4 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	Texture2D_t384 * G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	String_t* G_B3_5 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		String_t* L_1 = (__this->___m_Title_1);
		Texture2D_t384 * L_2 = (__this->___m_Image_2);
		String_t* L_3 = (__this->___m_AchievedDescription_3);
		String_t* L_4 = (__this->___m_UnachievedDescription_4);
		int32_t L_5 = (__this->___m_Hidden_5);
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = (__this->___m_Points_6);
		AchievementDescription_t1626 * L_7 = (AchievementDescription_t1626 *)il2cpp_codegen_object_new (AchievementDescription_t1626_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m7344(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"

// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"


// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern TypeInfo* Achievement_t1625_il2cpp_TypeInfo_var;
extern "C" Achievement_t1625 * GcAchievementData_ToAchievement_m7315 (GcAchievementData_t1617 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t1625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2855);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t48  V_0 = {0};
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = {0};
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = {0};
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		double L_1 = (__this->___m_PercentCompleted_1);
		int32_t L_2 = (__this->___m_Completed_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = (__this->___m_Hidden_3);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m3716((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_LastReportedDate_4);
		DateTime_t48  L_5 = DateTime_AddSeconds_m7521((&V_0), (((double)L_4)), /*hidden argument*/NULL);
		Achievement_t1625 * L_6 = (Achievement_t1625 *)il2cpp_codegen_object_new (Achievement_t1625_il2cpp_TypeInfo_var);
		Achievement__ctor_m7333(L_6, G_B6_3, G_B6_2, G_B6_1, G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
void GcAchievementData_t1617_marshal(const GcAchievementData_t1617& unmarshaled, GcAchievementData_t1617_marshaled& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Identifier_0);
	marshaled.___m_PercentCompleted_1 = unmarshaled.___m_PercentCompleted_1;
	marshaled.___m_Completed_2 = unmarshaled.___m_Completed_2;
	marshaled.___m_Hidden_3 = unmarshaled.___m_Hidden_3;
	marshaled.___m_LastReportedDate_4 = unmarshaled.___m_LastReportedDate_4;
}
void GcAchievementData_t1617_marshal_back(const GcAchievementData_t1617_marshaled& marshaled, GcAchievementData_t1617& unmarshaled)
{
	unmarshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0);
	unmarshaled.___m_PercentCompleted_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.___m_Completed_2 = marshaled.___m_Completed_2;
	unmarshaled.___m_Hidden_3 = marshaled.___m_Hidden_3;
	unmarshaled.___m_LastReportedDate_4 = marshaled.___m_LastReportedDate_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
void GcAchievementData_t1617_marshal_cleanup(GcAchievementData_t1617_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"

// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"


// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern TypeInfo* Score_t1627_il2cpp_TypeInfo_var;
extern "C" Score_t1627 * GcScoreData_ToScore_m7316 (GcScoreData_t1618 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Score_t1627_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2857);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t48  V_0 = {0};
	{
		String_t* L_0 = (__this->___m_Category_0);
		int32_t L_1 = (__this->___m_ValueHigh_2);
		int32_t L_2 = (__this->___m_ValueLow_1);
		String_t* L_3 = (__this->___m_PlayerID_5);
		DateTime__ctor_m3716((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_Date_3);
		DateTime_t48  L_5 = DateTime_AddSeconds_m7521((&V_0), (((double)L_4)), /*hidden argument*/NULL);
		String_t* L_6 = (__this->___m_FormattedValue_4);
		int32_t L_7 = (__this->___m_Rank_6);
		Score_t1627 * L_8 = (Score_t1627 *)il2cpp_codegen_object_new (Score_t1627_il2cpp_TypeInfo_var);
		Score__ctor_m7355(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)L_1))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)L_2)))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
void GcScoreData_t1618_marshal(const GcScoreData_t1618& unmarshaled, GcScoreData_t1618_marshaled& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Category_0);
	marshaled.___m_ValueLow_1 = unmarshaled.___m_ValueLow_1;
	marshaled.___m_ValueHigh_2 = unmarshaled.___m_ValueHigh_2;
	marshaled.___m_Date_3 = unmarshaled.___m_Date_3;
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.___m_FormattedValue_4);
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.___m_PlayerID_5);
	marshaled.___m_Rank_6 = unmarshaled.___m_Rank_6;
}
void GcScoreData_t1618_marshal_back(const GcScoreData_t1618_marshaled& marshaled, GcScoreData_t1618& unmarshaled)
{
	unmarshaled.___m_Category_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0);
	unmarshaled.___m_ValueLow_1 = marshaled.___m_ValueLow_1;
	unmarshaled.___m_ValueHigh_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.___m_Date_3 = marshaled.___m_Date_3;
	unmarshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4);
	unmarshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5);
	unmarshaled.___m_Rank_6 = marshaled.___m_Rank_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
void GcScoreData_t1618_marshal_cleanup(GcScoreData_t1618_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
