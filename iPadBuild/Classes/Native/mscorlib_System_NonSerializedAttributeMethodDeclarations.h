﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.NonSerializedAttribute
struct NonSerializedAttribute_t2827;

// System.Void System.NonSerializedAttribute::.ctor()
extern "C" void NonSerializedAttribute__ctor_m14145 (NonSerializedAttribute_t2827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
