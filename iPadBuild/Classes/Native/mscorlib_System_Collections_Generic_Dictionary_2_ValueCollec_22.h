﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3655;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>
struct  ValueCollection_t3663  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::dictionary
	Dictionary_2_t3655 * ___dictionary_0;
};
