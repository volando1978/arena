﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct KeyValuePair_2_t3741;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m19968(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3741 *, String_t*, MultiplayerParticipant_t672 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Key()
#define KeyValuePair_2_get_Key_m19969(__this, method) (( String_t* (*) (KeyValuePair_2_t3741 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m19970(__this, ___value, method) (( void (*) (KeyValuePair_2_t3741 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Value()
#define KeyValuePair_2_get_Value_m19971(__this, method) (( MultiplayerParticipant_t672 * (*) (KeyValuePair_2_t3741 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m19972(__this, ___value, method) (( void (*) (KeyValuePair_2_t3741 *, MultiplayerParticipant_t672 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::ToString()
#define KeyValuePair_2_ToString_m19973(__this, method) (( String_t* (*) (KeyValuePair_2_t3741 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
