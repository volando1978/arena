﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey19_3_t3737;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey19_3__ctor_m19887_gshared (U3CToOnGameThreadU3Ec__AnonStorey19_3_t3737 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey19_3__ctor_m19887(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey19_3_t3737 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey19_3__ctor_m19887_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>::<>m__C()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey19_3_U3CU3Em__C_m19888_gshared (U3CToOnGameThreadU3Ec__AnonStorey19_3_t3737 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey19_3_U3CU3Em__C_m19888(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey19_3_t3737 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey19_3_U3CU3Em__C_m19888_gshared)(__this, method)
