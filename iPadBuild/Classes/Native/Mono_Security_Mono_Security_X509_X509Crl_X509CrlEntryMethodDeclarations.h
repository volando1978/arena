﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t2135;
// System.Byte[]
struct ByteU5BU5D_t350;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t2156;
// Mono.Security.ASN1
struct ASN1_t2131;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl/X509CrlEntry::.ctor(Mono.Security.ASN1)
extern "C" void X509CrlEntry__ctor_m9243 (X509CrlEntry_t2135 * __this, ASN1_t2131 * ___entry, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::get_SerialNumber()
extern "C" ByteU5BU5D_t350* X509CrlEntry_get_SerialNumber_m9244 (X509CrlEntry_t2135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl/X509CrlEntry::get_RevocationDate()
extern "C" DateTime_t48  X509CrlEntry_get_RevocationDate_m8840 (X509CrlEntry_t2135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl/X509CrlEntry::get_Extensions()
extern "C" X509ExtensionCollection_t2156 * X509CrlEntry_get_Extensions_m8846 (X509CrlEntry_t2135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
