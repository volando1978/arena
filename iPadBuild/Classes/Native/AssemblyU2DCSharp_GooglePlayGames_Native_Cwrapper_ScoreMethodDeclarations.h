﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Score
struct Score_t469;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.UInt64 GooglePlayGames.Native.Cwrapper.Score::Score_Value(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t Score_Score_Value_m2037 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.Score::Score_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool Score_Score_Valid_m2038 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.Score::Score_Rank(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t Score_Score_Rank_m2039 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Score::Score_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void Score_Score_Dispose_m2040 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Score::Score_Metadata(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Score_Score_Metadata_m2041 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
