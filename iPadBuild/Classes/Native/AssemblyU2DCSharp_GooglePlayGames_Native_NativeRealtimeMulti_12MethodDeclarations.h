﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState
struct ConnectingState_t584;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse
struct WaitingRoomUIResponse_t697;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::.ctor(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" void ConnectingState__ctor_m2411 (ConnectingState_t584 * __this, NativeRealTimeRoom_t574 * ___room, RoomSession_t566 * ___session, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::.cctor()
extern "C" void ConnectingState__cctor_m2412 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::OnStateEntered()
extern "C" void ConnectingState_OnStateEntered_m2413 (ConnectingState_t584 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::HandleConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void ConnectingState_HandleConnectedSetChanged_m2414 (ConnectingState_t584 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::HandleParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void ConnectingState_HandleParticipantStatusChanged_m2415 (ConnectingState_t584 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::LeaveRoom()
extern "C" void ConnectingState_LeaveRoom_m2416 (ConnectingState_t584 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::ShowWaitingRoomUI(System.UInt32)
extern "C" void ConnectingState_ShowWaitingRoomUI_m2417 (ConnectingState_t584 * __this, uint32_t ___minimumParticipantsBeforeStarting, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::<LeaveRoom>m__3B()
extern "C" void ConnectingState_U3CLeaveRoomU3Em__3B_m2418 (ConnectingState_t584 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::<ShowWaitingRoomUI>m__3C(GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse)
extern "C" void ConnectingState_U3CShowWaitingRoomUIU3Em__3C_m2419 (ConnectingState_t584 * __this, WaitingRoomUIResponse_t697 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
