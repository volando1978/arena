﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t208;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t1208  : public UnityEventBase_t1655
{
	// System.Object[] UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::m_InvokeArray
	ObjectU5BU5D_t208* ___m_InvokeArray_4;
};
