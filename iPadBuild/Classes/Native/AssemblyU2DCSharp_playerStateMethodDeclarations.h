﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// playerState
struct playerState_t831;

// System.Void playerState::.ctor()
extern "C" void playerState__ctor_m3638 (playerState_t831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playerState::Start()
extern "C" void playerState_Start_m3639 (playerState_t831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playerState::Update()
extern "C" void playerState_Update_m3640 (playerState_t831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
