﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct KeyCollection_t3678;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t344;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.String[]
struct StringU5BU5D_t169;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_48.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_7MethodDeclarations.h"
#define KeyCollection__ctor_m19156(__this, ___dictionary, method) (( void (*) (KeyCollection_t3678 *, Dictionary_2_t344 *, MethodInfo*))KeyCollection__ctor_m17105_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m19157(__this, ___item, method) (( void (*) (KeyCollection_t3678 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17106_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m19158(__this, method) (( void (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17107_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m19159(__this, ___item, method) (( bool (*) (KeyCollection_t3678 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17108_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m19160(__this, ___item, method) (( bool (*) (KeyCollection_t3678 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17109_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m19161(__this, method) (( Object_t* (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17110_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m19162(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3678 *, Array_t *, int32_t, MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m17111_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m19163(__this, method) (( Object_t * (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17112_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m19164(__this, method) (( bool (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17113_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m19165(__this, method) (( bool (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17114_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m19166(__this, method) (( Object_t * (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m17115_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m19167(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3678 *, StringU5BU5D_t169*, int32_t, MethodInfo*))KeyCollection_CopyTo_m17116_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::GetEnumerator()
#define KeyCollection_GetEnumerator_m19168(__this, method) (( Enumerator_t4364  (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_GetEnumerator_m17117_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Count()
#define KeyCollection_get_Count_m19169(__this, method) (( int32_t (*) (KeyCollection_t3678 *, MethodInfo*))KeyCollection_get_Count_m17118_gshared)(__this, method)
