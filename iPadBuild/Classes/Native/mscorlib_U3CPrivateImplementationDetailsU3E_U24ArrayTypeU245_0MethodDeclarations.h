﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$52
struct U24ArrayTypeU2452_t2871;
struct U24ArrayTypeU2452_t2871_marshaled;

void U24ArrayTypeU2452_t2871_marshal(const U24ArrayTypeU2452_t2871& unmarshaled, U24ArrayTypeU2452_t2871_marshaled& marshaled);
void U24ArrayTypeU2452_t2871_marshal_back(const U24ArrayTypeU2452_t2871_marshaled& marshaled, U24ArrayTypeU2452_t2871& unmarshaled);
void U24ArrayTypeU2452_t2871_marshal_cleanup(U24ArrayTypeU2452_t2871_marshaled& marshaled);
