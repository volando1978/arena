﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>
struct List_1_t868;
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.NativeEvent>
struct IEnumerable_1_t943;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.NativeEvent>
struct IEnumerator_1_t4378;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<GooglePlayGames.Native.NativeEvent>
struct ICollection_1_t4379;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.Native.NativeEvent>
struct ReadOnlyCollection_1_t3730;
// GooglePlayGames.Native.NativeEvent[]
struct NativeEventU5BU5D_t3728;
// System.Predicate`1<GooglePlayGames.Native.NativeEvent>
struct Predicate_1_t3731;
// System.Action`1<GooglePlayGames.Native.NativeEvent>
struct Action_1_t3732;
// System.Comparison`1<GooglePlayGames.Native.NativeEvent>
struct Comparison_1_t3734;
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_24.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m19772(__this, method) (( void (*) (List_1_t868 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19773(__this, ___collection, method) (( void (*) (List_1_t868 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::.ctor(System.Int32)
#define List_1__ctor_m19774(__this, ___capacity, method) (( void (*) (List_1_t868 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::.cctor()
#define List_1__cctor_m19775(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19776(__this, method) (( Object_t* (*) (List_1_t868 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19777(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t868 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19778(__this, method) (( Object_t * (*) (List_1_t868 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19779(__this, ___item, method) (( int32_t (*) (List_1_t868 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19780(__this, ___item, method) (( bool (*) (List_1_t868 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19781(__this, ___item, method) (( int32_t (*) (List_1_t868 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19782(__this, ___index, ___item, method) (( void (*) (List_1_t868 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19783(__this, ___item, method) (( void (*) (List_1_t868 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19784(__this, method) (( bool (*) (List_1_t868 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19785(__this, method) (( bool (*) (List_1_t868 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19786(__this, method) (( Object_t * (*) (List_1_t868 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19787(__this, method) (( bool (*) (List_1_t868 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19788(__this, method) (( bool (*) (List_1_t868 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19789(__this, ___index, method) (( Object_t * (*) (List_1_t868 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19790(__this, ___index, ___value, method) (( void (*) (List_1_t868 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Add(T)
#define List_1_Add_m19791(__this, ___item, method) (( void (*) (List_1_t868 *, NativeEvent_t674 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19792(__this, ___newCount, method) (( void (*) (List_1_t868 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19793(__this, ___collection, method) (( void (*) (List_1_t868 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19794(__this, ___enumerable, method) (( void (*) (List_1_t868 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19795(__this, ___collection, method) (( void (*) (List_1_t868 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::AsReadOnly()
#define List_1_AsReadOnly_m19796(__this, method) (( ReadOnlyCollection_1_t3730 * (*) (List_1_t868 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Clear()
#define List_1_Clear_m19797(__this, method) (( void (*) (List_1_t868 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Contains(T)
#define List_1_Contains_m19798(__this, ___item, method) (( bool (*) (List_1_t868 *, NativeEvent_t674 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19799(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t868 *, NativeEventU5BU5D_t3728*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Find(System.Predicate`1<T>)
#define List_1_Find_m19800(__this, ___match, method) (( NativeEvent_t674 * (*) (List_1_t868 *, Predicate_1_t3731 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19801(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3731 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m19802(__this, ___match, method) (( int32_t (*) (List_1_t868 *, Predicate_1_t3731 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19803(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t868 *, int32_t, int32_t, Predicate_1_t3731 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m19804(__this, ___action, method) (( void (*) (List_1_t868 *, Action_1_t3732 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::GetEnumerator()
#define List_1_GetEnumerator_m19805(__this, method) (( Enumerator_t3733  (*) (List_1_t868 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::IndexOf(T)
#define List_1_IndexOf_m19806(__this, ___item, method) (( int32_t (*) (List_1_t868 *, NativeEvent_t674 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19807(__this, ___start, ___delta, method) (( void (*) (List_1_t868 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19808(__this, ___index, method) (( void (*) (List_1_t868 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Insert(System.Int32,T)
#define List_1_Insert_m19809(__this, ___index, ___item, method) (( void (*) (List_1_t868 *, int32_t, NativeEvent_t674 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19810(__this, ___collection, method) (( void (*) (List_1_t868 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Remove(T)
#define List_1_Remove_m19811(__this, ___item, method) (( bool (*) (List_1_t868 *, NativeEvent_t674 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19812(__this, ___match, method) (( int32_t (*) (List_1_t868 *, Predicate_1_t3731 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19813(__this, ___index, method) (( void (*) (List_1_t868 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Reverse()
#define List_1_Reverse_m19814(__this, method) (( void (*) (List_1_t868 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Sort()
#define List_1_Sort_m19815(__this, method) (( void (*) (List_1_t868 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19816(__this, ___comparison, method) (( void (*) (List_1_t868 *, Comparison_1_t3734 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::ToArray()
#define List_1_ToArray_m19817(__this, method) (( NativeEventU5BU5D_t3728* (*) (List_1_t868 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::TrimExcess()
#define List_1_TrimExcess_m19818(__this, method) (( void (*) (List_1_t868 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::get_Capacity()
#define List_1_get_Capacity_m19819(__this, method) (( int32_t (*) (List_1_t868 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19820(__this, ___value, method) (( void (*) (List_1_t868 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::get_Count()
#define List_1_get_Count_m19821(__this, method) (( int32_t (*) (List_1_t868 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::get_Item(System.Int32)
#define List_1_get_Item_m19822(__this, ___index, method) (( NativeEvent_t674 * (*) (List_1_t868 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::set_Item(System.Int32,T)
#define List_1_set_Item_m19823(__this, ___index, ___value, method) (( void (*) (List_1_t868 *, int32_t, NativeEvent_t674 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
