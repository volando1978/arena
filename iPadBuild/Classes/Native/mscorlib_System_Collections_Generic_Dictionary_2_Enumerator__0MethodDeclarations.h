﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>
struct Enumerator_t199;
// System.Object
struct Object_t;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t167;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m16610(__this, ___dictionary, method) (( void (*) (Enumerator_t199 *, Dictionary_2_t167 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16611(__this, method) (( Object_t * (*) (Enumerator_t199 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16612(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t199 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16613(__this, method) (( Object_t * (*) (Enumerator_t199 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16614(__this, method) (( Object_t * (*) (Enumerator_t199 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::MoveNext()
#define Enumerator_MoveNext_m893(__this, method) (( bool (*) (Enumerator_t199 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::get_Current()
#define Enumerator_get_Current_m890(__this, method) (( KeyValuePair_2_t198  (*) (Enumerator_t199 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16615(__this, method) (( String_t* (*) (Enumerator_t199 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16616(__this, method) (( JSONObject_t30 * (*) (Enumerator_t199 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::VerifyState()
#define Enumerator_VerifyState_m16617(__this, method) (( void (*) (Enumerator_t199 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16618(__this, method) (( void (*) (Enumerator_t199 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>::Dispose()
#define Enumerator_Dispose_m16619(__this, method) (( void (*) (Enumerator_t199 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
