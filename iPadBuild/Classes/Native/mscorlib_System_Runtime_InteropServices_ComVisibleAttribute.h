﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
struct  ComVisibleAttribute_t1426  : public Attribute_t1546
{
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::Visible
	bool ___Visible_0;
};
