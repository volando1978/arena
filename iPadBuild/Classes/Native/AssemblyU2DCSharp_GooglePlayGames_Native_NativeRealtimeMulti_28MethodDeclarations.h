﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33
struct U3CAcceptFromInboxU3Ec__AnonStorey33_t602;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse
struct RoomInboxUIResponse_t696;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey33__ctor_m2467 (U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33::<>m__29(GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse)
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey33_U3CU3Em__29_m2468 (U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * __this, RoomInboxUIResponse_t696 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
