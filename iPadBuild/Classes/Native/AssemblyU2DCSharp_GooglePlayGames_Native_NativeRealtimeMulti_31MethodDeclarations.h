﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39
struct U3CAcceptInvitationU3Ec__AnonStorey39_t608;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey39__ctor_m2471 (U3CAcceptInvitationU3Ec__AnonStorey39_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39::<>m__2F()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey39_U3CU3Em__2F_m2472 (U3CAcceptInvitationU3Ec__AnonStorey39_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
