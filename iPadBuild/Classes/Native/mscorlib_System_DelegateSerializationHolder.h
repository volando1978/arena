﻿#pragma once
#include <stdint.h>
// System.Delegate
struct Delegate_t211;
// System.Object
#include "mscorlib_System_Object.h"
// System.DelegateSerializationHolder
struct  DelegateSerializationHolder_t2797  : public Object_t
{
	// System.Delegate System.DelegateSerializationHolder::_delegate
	Delegate_t211 * ____delegate_0;
};
