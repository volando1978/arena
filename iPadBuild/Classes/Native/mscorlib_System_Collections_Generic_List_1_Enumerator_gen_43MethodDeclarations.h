﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>
struct Enumerator_t4025;
// System.Object
struct Object_t;
// UnityEngine.CanvasGroup
struct CanvasGroup_t1387;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1289;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m23744(__this, ___l, method) (( void (*) (Enumerator_t4025 *, List_1_t1289 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23745(__this, method) (( Object_t * (*) (Enumerator_t4025 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::Dispose()
#define Enumerator_Dispose_m23746(__this, method) (( void (*) (Enumerator_t4025 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::VerifyState()
#define Enumerator_VerifyState_m23747(__this, method) (( void (*) (Enumerator_t4025 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::MoveNext()
#define Enumerator_MoveNext_m23748(__this, method) (( bool (*) (Enumerator_t4025 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::get_Current()
#define Enumerator_get_Current_m23749(__this, method) (( CanvasGroup_t1387 * (*) (Enumerator_t4025 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
