﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B
struct U3CRoomSetupProgressU3Ec__AnonStorey3B_t567;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B::.ctor()
extern "C" void U3CRoomSetupProgressU3Ec__AnonStorey3B__ctor_m2352 (U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B::<>m__30()
extern "C" void U3CRoomSetupProgressU3Ec__AnonStorey3B_U3CU3Em__30_m2353 (U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
