﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t350;
// Mono.Security.X509.X509Extension
#include "Mono_Security_Mono_Security_X509_X509Extension.h"
// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
struct  AuthorityKeyIdentifierExtension_t2157  : public X509Extension_t2134
{
	// System.Byte[] Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::aki
	ByteU5BU5D_t350* ___aki_3;
};
