﻿#pragma once
#include <stdint.h>
// UnityEngine.Event
struct Event_t1269;
struct Event_t1269_marshaled;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct  KeyValuePair_2_t4136 
{
	// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::key
	Event_t1269 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::value
	int32_t ___value_1;
};
