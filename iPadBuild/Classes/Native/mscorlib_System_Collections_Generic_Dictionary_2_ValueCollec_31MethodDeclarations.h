﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
struct Enumerator_t3929;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3920;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22367_gshared (Enumerator_t3929 * __this, Dictionary_2_t3920 * ___host, MethodInfo* method);
#define Enumerator__ctor_m22367(__this, ___host, method) (( void (*) (Enumerator_t3929 *, Dictionary_2_t3920 *, MethodInfo*))Enumerator__ctor_m22367_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22368_gshared (Enumerator_t3929 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22368(__this, method) (( Object_t * (*) (Enumerator_t3929 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22368_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m22369_gshared (Enumerator_t3929 * __this, MethodInfo* method);
#define Enumerator_Dispose_m22369(__this, method) (( void (*) (Enumerator_t3929 *, MethodInfo*))Enumerator_Dispose_m22369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22370_gshared (Enumerator_t3929 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m22370(__this, method) (( bool (*) (Enumerator_t3929 *, MethodInfo*))Enumerator_MoveNext_m22370_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m22371_gshared (Enumerator_t3929 * __this, MethodInfo* method);
#define Enumerator_get_Current_m22371(__this, method) (( Object_t * (*) (Enumerator_t3929 *, MethodInfo*))Enumerator_get_Current_m22371_gshared)(__this, method)
