﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct EqualityComparer_1_t2924;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct  EqualityComparer_1_t2924  : public Object_t
{
};
struct EqualityComparer_1_t2924_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::_default
	EqualityComparer_1_t2924 * ____default_0;
};
