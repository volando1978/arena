﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t4108;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Comparison`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m24886_gshared (Comparison_1_t4108 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Comparison_1__ctor_m24886(__this, ___object, ___method, method) (( void (*) (Comparison_1_t4108 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m24886_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m24887_gshared (Comparison_1_t4108 * __this, UICharInfo_t1400  ___x, UICharInfo_t1400  ___y, MethodInfo* method);
#define Comparison_1_Invoke_m24887(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t4108 *, UICharInfo_t1400 , UICharInfo_t1400 , MethodInfo*))Comparison_1_Invoke_m24887_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UICharInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m24888_gshared (Comparison_1_t4108 * __this, UICharInfo_t1400  ___x, UICharInfo_t1400  ___y, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Comparison_1_BeginInvoke_m24888(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t4108 *, UICharInfo_t1400 , UICharInfo_t1400 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m24888_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m24889_gshared (Comparison_1_t4108 * __this, Object_t * ___result, MethodInfo* method);
#define Comparison_1_EndInvoke_m24889(__this, ___result, method) (( int32_t (*) (Comparison_1_t4108 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m24889_gshared)(__this, ___result, method)
