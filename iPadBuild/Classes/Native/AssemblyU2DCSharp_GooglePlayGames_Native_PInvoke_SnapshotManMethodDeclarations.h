﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse
struct OpenResponse_t701;
// System.String
struct String_t;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_6.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::.ctor(System.IntPtr)
extern "C" void OpenResponse__ctor_m3010 (OpenResponse_t701 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::RequestSucceeded()
extern "C" bool OpenResponse_RequestSucceeded_m3011 (OpenResponse_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ResponseStatus()
extern "C" int32_t OpenResponse_ResponseStatus_m3012 (OpenResponse_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ConflictId()
extern "C" String_t* OpenResponse_ConflictId_m3013 (OpenResponse_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::Data()
extern "C" NativeSnapshotMetadata_t611 * OpenResponse_Data_m3014 (OpenResponse_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ConflictOriginal()
extern "C" NativeSnapshotMetadata_t611 * OpenResponse_ConflictOriginal_m3015 (OpenResponse_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ConflictUnmerged()
extern "C" NativeSnapshotMetadata_t611 * OpenResponse_ConflictUnmerged_m3016 (OpenResponse_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void OpenResponse_CallDispose_m3017 (OpenResponse_t701 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::FromPointer(System.IntPtr)
extern "C" OpenResponse_t701 * OpenResponse_FromPointer_m3018 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::<ConflictId>m__9D(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  OpenResponse_U3CConflictIdU3Em__9D_m3019 (OpenResponse_t701 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
