﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t563;
// GooglePlayGames.Native.PInvoke.RealtimeManager
struct RealtimeManager_t564;
// System.String modreq(System.Runtime.CompilerServices.IsVolatile)
struct String_t;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State modreq(System.Runtime.CompilerServices.IsVolatile)
struct State_t565;
// System.Object
#include "mscorlib_System_Object.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct  RoomSession_t566  : public Object_t
{
	// System.Object GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::mLifecycleLock
	Object_t * ___mLifecycleLock_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::mListener
	OnGameThreadForwardingListener_t563 * ___mListener_1;
	// GooglePlayGames.Native.PInvoke.RealtimeManager GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::mManager
	RealtimeManager_t564 * ___mManager_2;
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::mCurrentPlayerId
	String_t* ___mCurrentPlayerId_3;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::mState
	State_t565 * ___mState_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::mStillPreRoomCreation
	bool ___mStillPreRoomCreation_5;
	// System.UInt32 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::mMinPlayersToStart
	uint32_t ___mMinPlayersToStart_6;
};
