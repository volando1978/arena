﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t1211;

// System.Void UnityEngine.UI.Button/ButtonClickedEvent::.ctor()
extern "C" void ButtonClickedEvent__ctor_m4847 (ButtonClickedEvent_t1211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
