﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InputHelper
struct InputHelper_t740;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"

// System.Void InputHelper::.ctor()
extern "C" void InputHelper__ctor_m3198 (InputHelper_t740 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputHelper::.cctor()
extern "C" void InputHelper__cctor_m3199 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::left()
extern "C" bool InputHelper_left_m3200 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::right()
extern "C" bool InputHelper_right_m3201 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::up()
extern "C" bool InputHelper_up_m3202 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::down()
extern "C" bool InputHelper_down_m3203 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::leftDown()
extern "C" bool InputHelper_leftDown_m3204 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::rightDown()
extern "C" bool InputHelper_rightDown_m3205 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::upDown()
extern "C" bool InputHelper_upDown_m3206 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::downDown()
extern "C" bool InputHelper_downDown_m3207 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::space()
extern "C" bool InputHelper_space_m3208 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::mouseClick()
extern "C" bool InputHelper_mouseClick_m3209 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 InputHelper::touch()
extern "C" Vector2_t739  InputHelper_touch_m3210 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputHelper::IsBegun(UnityEngine.Touch,UnityEngine.Touch)
extern "C" bool InputHelper_IsBegun_m3211 (Object_t * __this /* static, unused */, Touch_t901  ___touch1, Touch_t901  ___touch2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 InputHelper::GetMidpoint(UnityEngine.Touch,UnityEngine.Touch)
extern "C" Vector2_t739  InputHelper_GetMidpoint_m3212 (Object_t * __this /* static, unused */, Touch_t901  ___touch1, Touch_t901  ___touch2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InputHelper::GetInputRange()
extern "C" float InputHelper_GetInputRange_m3213 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputHelper::OnGUI()
extern "C" void InputHelper_OnGUI_m3214 (InputHelper_t740 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
