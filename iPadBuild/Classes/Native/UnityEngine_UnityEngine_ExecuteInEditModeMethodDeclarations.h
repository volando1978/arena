﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t1438;

// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C" void ExecuteInEditMode__ctor_m6239 (ExecuteInEditMode_t1438 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
