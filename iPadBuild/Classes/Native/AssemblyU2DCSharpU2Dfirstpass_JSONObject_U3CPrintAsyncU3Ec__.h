﻿#pragma once
#include <stdint.h>
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.IEnumerable
struct IEnumerable_t38;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.Object
#include "mscorlib_System_Object.h"
// JSONObject/<PrintAsync>c__Iterator1
struct  U3CPrintAsyncU3Ec__Iterator1_t39  : public Object_t
{
	// System.Text.StringBuilder JSONObject/<PrintAsync>c__Iterator1::<builder>__0
	StringBuilder_t36 * ___U3CbuilderU3E__0_0;
	// System.Boolean JSONObject/<PrintAsync>c__Iterator1::pretty
	bool ___pretty_1;
	// System.Collections.IEnumerator JSONObject/<PrintAsync>c__Iterator1::<$s_7>__1
	Object_t * ___U3CU24s_7U3E__1_2;
	// System.Collections.IEnumerable JSONObject/<PrintAsync>c__Iterator1::<e>__2
	Object_t * ___U3CeU3E__2_3;
	// System.Int32 JSONObject/<PrintAsync>c__Iterator1::$PC
	int32_t ___U24PC_4;
	// System.String JSONObject/<PrintAsync>c__Iterator1::$current
	String_t* ___U24current_5;
	// System.Boolean JSONObject/<PrintAsync>c__Iterator1::<$>pretty
	bool ___U3CU24U3Epretty_6;
	// JSONObject JSONObject/<PrintAsync>c__Iterator1::<>f__this
	JSONObject_t30 * ___U3CU3Ef__this_7;
};
