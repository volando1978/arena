﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51
struct U3CFindInvitationWithIdU3Ec__AnonStorey51_t638;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
struct TurnBasedMatchesResponse_t708;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51::.ctor()
extern "C" void U3CFindInvitationWithIdU3Ec__AnonStorey51__ctor_m2550 (U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51::<>m__5B(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse)
extern "C" void U3CFindInvitationWithIdU3Ec__AnonStorey51_U3CU3Em__5B_m2551 (U3CFindInvitationWithIdU3Ec__AnonStorey51_t638 * __this, TurnBasedMatchesResponse_t708 * ___allMatches, MethodInfo* method) IL2CPP_METHOD_ATTR;
