﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1<System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey14_1_t3706;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1/<ToOnGameThread>c__AnonStorey15`1<System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey15_1_t3707  : public Object_t
{
	// T GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1/<ToOnGameThread>c__AnonStorey15`1<System.Object>::val
	Object_t * ___val_0;
	// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1<T> GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1/<ToOnGameThread>c__AnonStorey15`1<System.Object>::<>f__ref$20
	U3CToOnGameThreadU3Ec__AnonStorey14_1_t3706 * ___U3CU3Ef__refU2420_1;
};
