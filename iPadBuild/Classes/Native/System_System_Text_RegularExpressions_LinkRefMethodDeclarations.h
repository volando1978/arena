﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkRef
struct LinkRef_t2077;

// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern "C" void LinkRef__ctor_m8386 (LinkRef_t2077 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
