﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<JSONObject>
struct List_1_t42;
// System.Object
struct Object_t;
// JSONObject
struct JSONObject_t30;
// System.Collections.Generic.IEnumerable`1<JSONObject>
struct IEnumerable_1_t4283;
// System.Collections.Generic.IEnumerator`1<JSONObject>
struct IEnumerator_1_t4284;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<JSONObject>
struct ICollection_1_t4285;
// System.Collections.ObjectModel.ReadOnlyCollection`1<JSONObject>
struct ReadOnlyCollection_1_t3495;
// JSONObject[]
struct JSONObjectU5BU5D_t168;
// System.Predicate`1<JSONObject>
struct Predicate_1_t3496;
// System.Action`1<JSONObject>
struct Action_1_t3497;
// System.Comparison`1<JSONObject>
struct Comparison_1_t3498;
// System.Collections.Generic.List`1/Enumerator<JSONObject>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.List`1<JSONObject>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m882(__this, method) (( void (*) (List_1_t42 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m894(__this, ___collection, method) (( void (*) (List_1_t42 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::.ctor(System.Int32)
#define List_1__ctor_m16344(__this, ___capacity, method) (( void (*) (List_1_t42 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::.cctor()
#define List_1__cctor_m16345(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<JSONObject>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16346(__this, method) (( Object_t* (*) (List_1_t42 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16347(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t42 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<JSONObject>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16348(__this, method) (( Object_t * (*) (List_1_t42 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16349(__this, ___item, method) (( int32_t (*) (List_1_t42 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16350(__this, ___item, method) (( bool (*) (List_1_t42 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16351(__this, ___item, method) (( int32_t (*) (List_1_t42 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16352(__this, ___index, ___item, method) (( void (*) (List_1_t42 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16353(__this, ___item, method) (( void (*) (List_1_t42 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<JSONObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16354(__this, method) (( bool (*) (List_1_t42 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSONObject>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16355(__this, method) (( bool (*) (List_1_t42 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<JSONObject>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16356(__this, method) (( Object_t * (*) (List_1_t42 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16357(__this, method) (( bool (*) (List_1_t42 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16358(__this, method) (( bool (*) (List_1_t42 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16359(__this, ___index, method) (( Object_t * (*) (List_1_t42 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16360(__this, ___index, ___value, method) (( void (*) (List_1_t42 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::Add(T)
#define List_1_Add_m16361(__this, ___item, method) (( void (*) (List_1_t42 *, JSONObject_t30 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16362(__this, ___newCount, method) (( void (*) (List_1_t42 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16363(__this, ___collection, method) (( void (*) (List_1_t42 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16364(__this, ___enumerable, method) (( void (*) (List_1_t42 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m897(__this, ___collection, method) (( void (*) (List_1_t42 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<JSONObject>::AsReadOnly()
#define List_1_AsReadOnly_m16365(__this, method) (( ReadOnlyCollection_1_t3495 * (*) (List_1_t42 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::Clear()
#define List_1_Clear_m16366(__this, method) (( void (*) (List_1_t42 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSONObject>::Contains(T)
#define List_1_Contains_m16367(__this, ___item, method) (( bool (*) (List_1_t42 *, JSONObject_t30 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16368(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t42 *, JSONObjectU5BU5D_t168*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<JSONObject>::Find(System.Predicate`1<T>)
#define List_1_Find_m16369(__this, ___match, method) (( JSONObject_t30 * (*) (List_1_t42 *, Predicate_1_t3496 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16370(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3496 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m16371(__this, ___match, method) (( int32_t (*) (List_1_t42 *, Predicate_1_t3496 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16372(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t42 *, int32_t, int32_t, Predicate_1_t3496 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m16373(__this, ___action, method) (( void (*) (List_1_t42 *, Action_1_t3497 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<JSONObject>::GetEnumerator()
#define List_1_GetEnumerator_m917(__this, method) (( Enumerator_t204  (*) (List_1_t42 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::IndexOf(T)
#define List_1_IndexOf_m16374(__this, ___item, method) (( int32_t (*) (List_1_t42 *, JSONObject_t30 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16375(__this, ___start, ___delta, method) (( void (*) (List_1_t42 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16376(__this, ___index, method) (( void (*) (List_1_t42 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::Insert(System.Int32,T)
#define List_1_Insert_m16377(__this, ___index, ___item, method) (( void (*) (List_1_t42 *, int32_t, JSONObject_t30 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16378(__this, ___collection, method) (( void (*) (List_1_t42 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<JSONObject>::Remove(T)
#define List_1_Remove_m16379(__this, ___item, method) (( bool (*) (List_1_t42 *, JSONObject_t30 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16380(__this, ___match, method) (( int32_t (*) (List_1_t42 *, Predicate_1_t3496 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16381(__this, ___index, method) (( void (*) (List_1_t42 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::Reverse()
#define List_1_Reverse_m16382(__this, method) (( void (*) (List_1_t42 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::Sort()
#define List_1_Sort_m16383(__this, method) (( void (*) (List_1_t42 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16384(__this, ___comparison, method) (( void (*) (List_1_t42 *, Comparison_1_t3498 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<JSONObject>::ToArray()
#define List_1_ToArray_m16385(__this, method) (( JSONObjectU5BU5D_t168* (*) (List_1_t42 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::TrimExcess()
#define List_1_TrimExcess_m16386(__this, method) (( void (*) (List_1_t42 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Capacity()
#define List_1_get_Capacity_m16387(__this, method) (( int32_t (*) (List_1_t42 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16388(__this, ___value, method) (( void (*) (List_1_t42 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count()
#define List_1_get_Count_m16389(__this, method) (( int32_t (*) (List_1_t42 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32)
#define List_1_get_Item_m16390(__this, ___index, method) (( JSONObject_t30 * (*) (List_1_t42 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<JSONObject>::set_Item(System.Int32,T)
#define List_1_set_Item_m16391(__this, ___index, ___value, method) (( void (*) (List_1_t42 *, int32_t, JSONObject_t30 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
