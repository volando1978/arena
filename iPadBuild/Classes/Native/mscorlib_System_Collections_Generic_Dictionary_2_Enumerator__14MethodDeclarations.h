﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Enumerator_t3680;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t344;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m19184(__this, ___dictionary, method) (( void (*) (Enumerator_t3680 *, Dictionary_2_t344 *, MethodInfo*))Enumerator__ctor_m17124_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19185(__this, method) (( Object_t * (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17125_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19186(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19187(__this, method) (( Object_t * (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19188(__this, method) (( Object_t * (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
#define Enumerator_MoveNext_m19189(__this, method) (( bool (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_MoveNext_m17129_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
#define Enumerator_get_Current_m19190(__this, method) (( KeyValuePair_2_t3677  (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_get_Current_m17130_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m19191(__this, method) (( String_t* (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_get_CurrentKey_m17131_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m19192(__this, method) (( int32_t (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_get_CurrentValue_m17132_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::VerifyState()
#define Enumerator_VerifyState_m19193(__this, method) (( void (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_VerifyState_m17133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m19194(__this, method) (( void (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_VerifyCurrent_m17134_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Dispose()
#define Enumerator_Dispose_m19195(__this, method) (( void (*) (Enumerator_t3680 *, MethodInfo*))Enumerator_Dispose_m17135_gshared)(__this, method)
