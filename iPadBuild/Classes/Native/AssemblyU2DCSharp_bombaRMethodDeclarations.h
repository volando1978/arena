﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// bombaR
struct bombaR_t764;

// System.Void bombaR::.ctor()
extern "C" void bombaR__ctor_m3311 (bombaR_t764 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bombaR::Start()
extern "C" void bombaR_Start_m3312 (bombaR_t764 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bombaR::Update()
extern "C" void bombaR_Update_m3313 (bombaR_t764 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
