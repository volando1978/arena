﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3974;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m23047_gshared (DefaultComparer_t3974 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m23047(__this, method) (( void (*) (DefaultComparer_t3974 *, MethodInfo*))DefaultComparer__ctor_m23047_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m23048_gshared (DefaultComparer_t3974 * __this, UIVertex_t1265  ___x, UIVertex_t1265  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m23048(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3974 *, UIVertex_t1265 , UIVertex_t1265 , MethodInfo*))DefaultComparer_Compare_m23048_gshared)(__this, ___x, ___y, method)
