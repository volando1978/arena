﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t627;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48
struct  U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata> GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48::callback
	Action_2_t627 * ___callback_0;
};
