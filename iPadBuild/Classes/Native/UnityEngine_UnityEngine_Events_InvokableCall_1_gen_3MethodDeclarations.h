﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t4012;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t4011;
// System.Object[]
struct ObjectU5BU5D_t208;

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m23552_gshared (InvokableCall_1_t4012 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, MethodInfo* method);
#define InvokableCall_1__ctor_m23552(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t4012 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_1__ctor_m23552_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m23553_gshared (InvokableCall_1_t4012 * __this, UnityAction_1_t4011 * ___callback, MethodInfo* method);
#define InvokableCall_1__ctor_m23553(__this, ___callback, method) (( void (*) (InvokableCall_1_t4012 *, UnityAction_1_t4011 *, MethodInfo*))InvokableCall_1__ctor_m23553_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m23554_gshared (InvokableCall_1_t4012 * __this, ObjectU5BU5D_t208* ___args, MethodInfo* method);
#define InvokableCall_1_Invoke_m23554(__this, ___args, method) (( void (*) (InvokableCall_1_t4012 *, ObjectU5BU5D_t208*, MethodInfo*))InvokableCall_1_Invoke_m23554_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m23555_gshared (InvokableCall_1_t4012 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, MethodInfo* method);
#define InvokableCall_1_Find_m23555(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t4012 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_1_Find_m23555_gshared)(__this, ___targetObj, ___method, method)
