﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`1<System.String>
struct UnityDictionary_1_t13;

// System.Void UnityEngine.UnityDictionary`1<System.String>::.ctor()
// UnityEngine.UnityDictionary`1<System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_UnityDictionary_1__0MethodDeclarations.h"
#define UnityDictionary_1__ctor_m839(__this, method) (( void (*) (UnityDictionary_1_t13 *, MethodInfo*))UnityDictionary_1__ctor_m15867_gshared)(__this, method)
