﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.PurchasableVirtualItem::.ctor(System.String,System.String,System.String,Soomla.Store.PurchaseType)
extern "C" void PurchasableVirtualItem__ctor_m585 (PurchasableVirtualItem_t124 * __this, String_t* ___name, String_t* ___description, String_t* ___itemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.PurchasableVirtualItem::.ctor(JSONObject)
extern "C" void PurchasableVirtualItem__ctor_m586 (PurchasableVirtualItem_t124 * __this, JSONObject_t30 * ___jsonItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.PurchasableVirtualItem::CanAfford()
extern "C" bool PurchasableVirtualItem_CanAfford_m587 (PurchasableVirtualItem_t124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.PurchasableVirtualItem::Buy(System.String)
extern "C" void PurchasableVirtualItem_Buy_m588 (PurchasableVirtualItem_t124 * __this, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.PurchasableVirtualItem::canBuy()
// JSONObject Soomla.Store.PurchasableVirtualItem::toJSONObject()
extern "C" JSONObject_t30 * PurchasableVirtualItem_toJSONObject_m589 (PurchasableVirtualItem_t124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
