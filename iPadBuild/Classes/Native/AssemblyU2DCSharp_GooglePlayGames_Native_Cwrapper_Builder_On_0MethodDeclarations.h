﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
struct OnAuthActionStartedCallback_t403;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"

// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnAuthActionStartedCallback__ctor_m1657 (OnAuthActionStartedCallback_t403 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr)
extern "C" void OnAuthActionStartedCallback_Invoke_m1658 (OnAuthActionStartedCallback_t403 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnAuthActionStartedCallback_t403(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnAuthActionStartedCallback_BeginInvoke_m1659 (OnAuthActionStartedCallback_t403 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnAuthActionStartedCallback_EndInvoke_m1660 (OnAuthActionStartedCallback_t403 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
