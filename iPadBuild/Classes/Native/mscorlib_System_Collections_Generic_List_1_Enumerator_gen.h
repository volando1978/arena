﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<JSONObject>
struct List_1_t42;
// JSONObject
struct JSONObject_t30;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<JSONObject>
struct  Enumerator_t204 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<JSONObject>::l
	List_1_t42 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<JSONObject>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<JSONObject>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<JSONObject>::current
	JSONObject_t30 * ___current_3;
};
