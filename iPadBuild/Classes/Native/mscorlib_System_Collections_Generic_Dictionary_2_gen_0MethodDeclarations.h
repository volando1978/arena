﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t167;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// JSONObject
struct JSONObject_t30;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,JSONObject>
struct KeyCollection_t3504;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,JSONObject>
struct ValueCollection_t3505;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>[]
struct KeyValuePair_2U5BU5D_t4286;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>>
struct IEnumerator_1_t4287;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,JSONObject>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_23MethodDeclarations.h"
#define Dictionary_2__ctor_m16529(__this, method) (( void (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2__ctor_m16211_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16530(__this, ___comparer, method) (( void (*) (Dictionary_2_t167 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m16213_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::.ctor(System.Int32)
#define Dictionary_2__ctor_m16531(__this, ___capacity, method) (( void (*) (Dictionary_2_t167 *, int32_t, MethodInfo*))Dictionary_2__ctor_m16215_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m16532(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t167 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m16217_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m16533(__this, method) (( Object_t * (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16534(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t167 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16535(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t167 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m16536(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t167 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m16537(__this, ___key, method) (( void (*) (Dictionary_2_t167 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16538(__this, method) (( bool (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16539(__this, method) (( Object_t * (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16540(__this, method) (( bool (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16541(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t167 *, KeyValuePair_2_t198 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16542(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t167 *, KeyValuePair_2_t198 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16543(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t167 *, KeyValuePair_2U5BU5D_t4286*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16544(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t167 *, KeyValuePair_2_t198 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16545(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t167 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16546(__this, method) (( Object_t * (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16547(__this, method) (( Object_t* (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,JSONObject>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16548(__this, method) (( Object_t * (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,JSONObject>::get_Count()
#define Dictionary_2_get_Count_m16549(__this, method) (( int32_t (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_get_Count_m16251_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,JSONObject>::get_Item(TKey)
#define Dictionary_2_get_Item_m16550(__this, ___key, method) (( JSONObject_t30 * (*) (Dictionary_2_t167 *, String_t*, MethodInfo*))Dictionary_2_get_Item_m16253_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m16551(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t167 *, String_t*, JSONObject_t30 *, MethodInfo*))Dictionary_2_set_Item_m16255_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m16552(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t167 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m16257_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m16553(__this, ___size, method) (( void (*) (Dictionary_2_t167 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m16259_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m16554(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t167 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m16261_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,JSONObject>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m16555(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t198  (*) (Object_t * /* static, unused */, String_t*, JSONObject_t30 *, MethodInfo*))Dictionary_2_make_pair_m16263_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,JSONObject>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m16556(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, JSONObject_t30 *, MethodInfo*))Dictionary_2_pick_key_m16265_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,JSONObject>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m16557(__this /* static, unused */, ___key, ___value, method) (( JSONObject_t30 * (*) (Object_t * /* static, unused */, String_t*, JSONObject_t30 *, MethodInfo*))Dictionary_2_pick_value_m16267_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m16558(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t167 *, KeyValuePair_2U5BU5D_t4286*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m16269_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::Resize()
#define Dictionary_2_Resize_m16559(__this, method) (( void (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_Resize_m16271_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::Add(TKey,TValue)
#define Dictionary_2_Add_m16560(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t167 *, String_t*, JSONObject_t30 *, MethodInfo*))Dictionary_2_Add_m16273_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::Clear()
#define Dictionary_2_Clear_m16561(__this, method) (( void (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_Clear_m16275_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m16562(__this, ___key, method) (( bool (*) (Dictionary_2_t167 *, String_t*, MethodInfo*))Dictionary_2_ContainsKey_m16277_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m16563(__this, ___value, method) (( bool (*) (Dictionary_2_t167 *, JSONObject_t30 *, MethodInfo*))Dictionary_2_ContainsValue_m16279_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m16564(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t167 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m16281_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,JSONObject>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m16565(__this, ___sender, method) (( void (*) (Dictionary_2_t167 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m16283_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::Remove(TKey)
#define Dictionary_2_Remove_m16566(__this, ___key, method) (( bool (*) (Dictionary_2_t167 *, String_t*, MethodInfo*))Dictionary_2_Remove_m16285_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m16567(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t167 *, String_t*, JSONObject_t30 **, MethodInfo*))Dictionary_2_TryGetValue_m16287_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,JSONObject>::get_Keys()
#define Dictionary_2_get_Keys_m16568(__this, method) (( KeyCollection_t3504 * (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_get_Keys_m16289_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,JSONObject>::get_Values()
#define Dictionary_2_get_Values_m16569(__this, method) (( ValueCollection_t3505 * (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_get_Values_m16291_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,JSONObject>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m16570(__this, ___key, method) (( String_t* (*) (Dictionary_2_t167 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m16293_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,JSONObject>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m16571(__this, ___value, method) (( JSONObject_t30 * (*) (Dictionary_2_t167 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m16295_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,JSONObject>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m16572(__this, ___pair, method) (( bool (*) (Dictionary_2_t167 *, KeyValuePair_2_t198 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16297_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,JSONObject>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m889(__this, method) (( Enumerator_t199  (*) (Dictionary_2_t167 *, MethodInfo*))Dictionary_2_GetEnumerator_m16298_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,JSONObject>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m16573(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, String_t*, JSONObject_t30 *, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16300_gshared)(__this /* static, unused */, ___key, ___value, method)
