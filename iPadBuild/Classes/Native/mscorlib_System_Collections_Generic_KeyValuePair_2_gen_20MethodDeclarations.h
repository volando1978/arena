﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct KeyValuePair_2_t3921;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m22308_gshared (KeyValuePair_2_t3921 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2__ctor_m22308(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3921 *, int32_t, Object_t *, MethodInfo*))KeyValuePair_2__ctor_m22308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m22309_gshared (KeyValuePair_2_t3921 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Key_m22309(__this, method) (( int32_t (*) (KeyValuePair_2_t3921 *, MethodInfo*))KeyValuePair_2_get_Key_m22309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m22310_gshared (KeyValuePair_2_t3921 * __this, int32_t ___value, MethodInfo* method);
#define KeyValuePair_2_set_Key_m22310(__this, ___value, method) (( void (*) (KeyValuePair_2_t3921 *, int32_t, MethodInfo*))KeyValuePair_2_set_Key_m22310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m22311_gshared (KeyValuePair_2_t3921 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Value_m22311(__this, method) (( Object_t * (*) (KeyValuePair_2_t3921 *, MethodInfo*))KeyValuePair_2_get_Value_m22311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m22312_gshared (KeyValuePair_2_t3921 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Value_m22312(__this, ___value, method) (( void (*) (KeyValuePair_2_t3921 *, Object_t *, MethodInfo*))KeyValuePair_2_set_Value_m22312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m22313_gshared (KeyValuePair_2_t3921 * __this, MethodInfo* method);
#define KeyValuePair_2_ToString_m22313(__this, method) (( String_t* (*) (KeyValuePair_2_t3921 *, MethodInfo*))KeyValuePair_2_ToString_m22313_gshared)(__this, method)
