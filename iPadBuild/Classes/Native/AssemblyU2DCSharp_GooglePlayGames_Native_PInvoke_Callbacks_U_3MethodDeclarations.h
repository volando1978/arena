﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Byte>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3773;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Byte>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20358_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3773 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20358(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3773 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20358_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Byte>::<>m__75(T)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20359_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3773 * __this, uint8_t ___result, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20359(__this, ___result, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3773 *, uint8_t, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20359_gshared)(__this, ___result, method)
