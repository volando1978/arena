﻿#pragma once
#include <stdint.h>
// System.Func`2<System.IntPtr,GooglePlayGames.Native.NativeEvent>
struct Func_2_t663;
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_BaseReferen.h"
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
struct  FetchAllResponse_t664  : public BaseReferenceHolder_t654
{
};
struct FetchAllResponse_t664_StaticFields{
	// System.Func`2<System.IntPtr,GooglePlayGames.Native.NativeEvent> GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<>f__am$cache0
	Func_2_t663 * ___U3CU3Ef__amU24cache0_1;
};
