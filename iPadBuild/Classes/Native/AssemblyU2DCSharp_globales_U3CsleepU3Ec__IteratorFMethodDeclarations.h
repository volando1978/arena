﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// globales/<sleep>c__IteratorF
struct U3CsleepU3Ec__IteratorF_t805;
// System.Object
struct Object_t;

// System.Void globales/<sleep>c__IteratorF::.ctor()
extern "C" void U3CsleepU3Ec__IteratorF__ctor_m3511 (U3CsleepU3Ec__IteratorF_t805 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object globales/<sleep>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512 (U3CsleepU3Ec__IteratorF_t805 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object globales/<sleep>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513 (U3CsleepU3Ec__IteratorF_t805 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean globales/<sleep>c__IteratorF::MoveNext()
extern "C" bool U3CsleepU3Ec__IteratorF_MoveNext_m3514 (U3CsleepU3Ec__IteratorF_t805 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales/<sleep>c__IteratorF::Dispose()
extern "C" void U3CsleepU3Ec__IteratorF_Dispose_m3515 (U3CsleepU3Ec__IteratorF_t805 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales/<sleep>c__IteratorF::Reset()
extern "C" void U3CsleepU3Ec__IteratorF_Reset_m3516 (U3CsleepU3Ec__IteratorF_t805 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
