﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.CoreSettings
struct CoreSettings_t5;
// System.String
struct String_t;

// System.Void Soomla.CoreSettings::.ctor()
extern "C" void CoreSettings__ctor_m2 (CoreSettings_t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.CoreSettings::.cctor()
extern "C" void CoreSettings__cctor_m3 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.CoreSettings::get_SoomlaSecret()
extern "C" String_t* CoreSettings_get_SoomlaSecret_m4 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.CoreSettings::set_SoomlaSecret(System.String)
extern "C" void CoreSettings_set_SoomlaSecret_m5 (Object_t * __this /* static, unused */, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.CoreSettings::get_DebugMessages()
extern "C" bool CoreSettings_get_DebugMessages_m6 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.CoreSettings::set_DebugMessages(System.Boolean)
extern "C" void CoreSettings_set_DebugMessages_m7 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.CoreSettings::get_DebugUnityMessages()
extern "C" bool CoreSettings_get_DebugUnityMessages_m8 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.CoreSettings::set_DebugUnityMessages(System.Boolean)
extern "C" void CoreSettings_set_DebugUnityMessages_m9 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
