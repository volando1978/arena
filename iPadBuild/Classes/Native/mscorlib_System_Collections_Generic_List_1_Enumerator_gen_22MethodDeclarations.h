﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Enumerator_t3694;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct List_1_t931;

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m19413(__this, ___l, method) (( void (*) (Enumerator_t3694 *, List_1_t931 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19414(__this, method) (( Object_t * (*) (Enumerator_t3694 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Dispose()
#define Enumerator_Dispose_m19415(__this, method) (( void (*) (Enumerator_t3694 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::VerifyState()
#define Enumerator_VerifyState_m19416(__this, method) (( void (*) (Enumerator_t3694 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::MoveNext()
#define Enumerator_MoveNext_m19417(__this, method) (( bool (*) (Enumerator_t3694 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::get_Current()
#define Enumerator_get_Current_m19418(__this, method) (( Object_t * (*) (Enumerator_t3694 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
