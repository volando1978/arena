﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// cameraScript/<shakeSmall>c__Iterator6
struct U3CshakeSmallU3Ec__Iterator6_t776;
// System.Object
struct Object_t;

// System.Void cameraScript/<shakeSmall>c__Iterator6::.ctor()
extern "C" void U3CshakeSmallU3Ec__Iterator6__ctor_m3340 (U3CshakeSmallU3Ec__Iterator6_t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraScript/<shakeSmall>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341 (U3CshakeSmallU3Ec__Iterator6_t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraScript/<shakeSmall>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342 (U3CshakeSmallU3Ec__Iterator6_t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean cameraScript/<shakeSmall>c__Iterator6::MoveNext()
extern "C" bool U3CshakeSmallU3Ec__Iterator6_MoveNext_m3343 (U3CshakeSmallU3Ec__Iterator6_t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraScript/<shakeSmall>c__Iterator6::Dispose()
extern "C" void U3CshakeSmallU3Ec__Iterator6_Dispose_m3344 (U3CshakeSmallU3Ec__Iterator6_t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraScript/<shakeSmall>c__Iterator6::Reset()
extern "C" void U3CshakeSmallU3Ec__Iterator6_Reset_m3345 (U3CshakeSmallU3Ec__Iterator6_t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
