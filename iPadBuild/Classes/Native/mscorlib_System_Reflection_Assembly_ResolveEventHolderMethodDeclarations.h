﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_t2523;

// System.Void System.Reflection.Assembly/ResolveEventHolder::.ctor()
extern "C" void ResolveEventHolder__ctor_m12185 (ResolveEventHolder_t2523 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
