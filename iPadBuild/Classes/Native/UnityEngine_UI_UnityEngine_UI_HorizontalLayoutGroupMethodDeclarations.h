﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t1318;

// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern "C" void HorizontalLayoutGroup__ctor_m5582 (HorizontalLayoutGroup_t1318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m5583 (HorizontalLayoutGroup_t1318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m5584 (HorizontalLayoutGroup_t1318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m5585 (HorizontalLayoutGroup_t1318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m5586 (HorizontalLayoutGroup_t1318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
