﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// movementProxy
struct movementProxy_t815;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void movementProxy::.ctor()
extern "C" void movementProxy__ctor_m3569 (movementProxy_t815 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movementProxy::rotateShipDirection()
extern "C" void movementProxy_rotateShipDirection_m3570 (movementProxy_t815 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movementProxy::rotateInactive()
extern "C" void movementProxy_rotateInactive_m3571 (movementProxy_t815 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single movementProxy::calculateAngle(UnityEngine.Vector2)
extern "C" float movementProxy_calculateAngle_m3572 (movementProxy_t815 * __this, Vector2_t739  ___target, MethodInfo* method) IL2CPP_METHOD_ATTR;
