﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// circularScr
struct  circularScr_t778  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject circularScr::enemyController
	GameObject_t144 * ___enemyController_2;
	// UnityEngine.GameObject circularScr::paquete
	GameObject_t144 * ___paquete_3;
	// UnityEngine.GameObject circularScr::coin
	GameObject_t144 * ___coin_4;
	// UnityEngine.GameObject circularScr::movingText
	GameObject_t144 * ___movingText_5;
	// UnityEngine.GameObject circularScr::movingTextKills
	GameObject_t144 * ___movingTextKills_6;
	// UnityEngine.GameObject circularScr::explosion
	GameObject_t144 * ___explosion_7;
	// UnityEngine.GameObject circularScr::circularControler
	GameObject_t144 * ___circularControler_8;
	// UnityEngine.GameObject circularScr::center
	GameObject_t144 * ___center_9;
	// System.Single circularScr::radius
	float ___radius_10;
	// System.Boolean circularScr::release
	bool ___release_11;
	// UnityEngine.GameObject circularScr::explosionDelayed
	GameObject_t144 * ___explosionDelayed_12;
	// UnityEngine.GameObject circularScr::dust
	GameObject_t144 * ___dust_13;
	// UnityEngine.GameObject circularScr::randomExplosion
	GameObject_t144 * ___randomExplosion_14;
};
