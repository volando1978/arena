﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// StoreMenu
struct StoreMenu_t757;
// UnityEngine.Advertisements.ShowResult
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho_0.h"

// System.Void StoreMenu::.ctor()
extern "C" void StoreMenu__ctor_m3288 (StoreMenu_t757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreMenu::Awake()
extern "C" void StoreMenu_Awake_m3289 (StoreMenu_t757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreMenu::Start()
extern "C" void StoreMenu_Start_m3290 (StoreMenu_t757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreMenu::setSizes()
extern "C" void StoreMenu_setSizes_m3291 (StoreMenu_t757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreMenu::fillProducts()
extern "C" void StoreMenu_fillProducts_m3292 (StoreMenu_t757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreMenu::OnGUI()
extern "C" void StoreMenu_OnGUI_m3293 (StoreMenu_t757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreMenu::<OnGUI>m__A5(UnityEngine.Advertisements.ShowResult)
extern "C" void StoreMenu_U3COnGUIU3Em__A5_m3294 (Object_t * __this /* static, unused */, int32_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
