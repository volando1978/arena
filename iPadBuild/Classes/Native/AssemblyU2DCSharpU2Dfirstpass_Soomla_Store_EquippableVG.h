﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.EquippableVG/EquippingModel
struct EquippingModel_t128;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t75;
// Soomla.Store.LifetimeVG
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_LifetimeVG.h"
// Soomla.Store.EquippableVG
struct  EquippableVG_t129  : public LifetimeVG_t130
{
	// Soomla.Store.EquippableVG/EquippingModel Soomla.Store.EquippableVG::Equipping
	EquippingModel_t128 * ___Equipping_9;
};
struct EquippableVG_t129_StaticFields{
	// System.String Soomla.Store.EquippableVG::TAG
	String_t* ___TAG_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Soomla.Store.EquippableVG::<>f__switch$map1
	Dictionary_2_t75 * ___U3CU3Ef__switchU24map1_10;
};
