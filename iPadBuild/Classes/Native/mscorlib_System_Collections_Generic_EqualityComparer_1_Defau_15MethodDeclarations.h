﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t4253;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C" void DefaultComparer__ctor_m26167_gshared (DefaultComparer_t4253 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m26167(__this, method) (( void (*) (DefaultComparer_t4253 *, MethodInfo*))DefaultComparer__ctor_m26167_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26168_gshared (DefaultComparer_t4253 * __this, TimeSpan_t190  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m26168(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4253 *, TimeSpan_t190 , MethodInfo*))DefaultComparer_GetHashCode_m26168_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26169_gshared (DefaultComparer_t4253 * __this, TimeSpan_t190  ___x, TimeSpan_t190  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m26169(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4253 *, TimeSpan_t190 , TimeSpan_t190 , MethodInfo*))DefaultComparer_Equals_m26169_gshared)(__this, ___x, ___y, method)
