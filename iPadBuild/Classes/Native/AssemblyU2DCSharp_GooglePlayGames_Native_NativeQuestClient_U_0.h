﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>
struct Action_2_t552;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27
struct  U3CFetchMatchingStateU3Ec__AnonStorey27_t553  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>> GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27::callback
	Action_2_t552 * ___callback_0;
};
