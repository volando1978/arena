﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42
struct U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t617;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42::.ctor()
extern "C" void U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42__ctor_m2505 (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42::<>m__48(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern "C" void U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_U3CU3Em__48_m2506 (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method) IL2CPP_METHOD_ATTR;
