﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
struct InternalEnumerator_1_t4008;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23525_gshared (InternalEnumerator_1_t4008 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m23525(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4008 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m23525_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23526_gshared (InternalEnumerator_1_t4008 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23526(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4008 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23526_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23527_gshared (InternalEnumerator_1_t4008 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23527(__this, method) (( void (*) (InternalEnumerator_1_t4008 *, MethodInfo*))InternalEnumerator_1_Dispose_m23527_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23528_gshared (InternalEnumerator_1_t4008 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23528(__this, method) (( bool (*) (InternalEnumerator_1_t4008 *, MethodInfo*))InternalEnumerator_1_MoveNext_m23528_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t1400  InternalEnumerator_1_get_Current_m23529_gshared (InternalEnumerator_1_t4008 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23529(__this, method) (( UICharInfo_t1400  (*) (InternalEnumerator_1_t4008 *, MethodInfo*))InternalEnumerator_1_get_Current_m23529_gshared)(__this, method)
