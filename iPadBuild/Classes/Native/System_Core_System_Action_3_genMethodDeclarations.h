﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`3<Soomla.Store.VirtualCurrency,System.Int32,System.Int32>
struct Action_3_t91;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`3<Soomla.Store.VirtualCurrency,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
// System.Action`3<System.Object,System.Int32,System.Int32>
#include "System_Core_System_Action_3_gen_6MethodDeclarations.h"
#define Action_3__ctor_m957(__this, ___object, ___method, method) (( void (*) (Action_3_t91 *, Object_t *, IntPtr_t, MethodInfo*))Action_3__ctor_m17250_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`3<Soomla.Store.VirtualCurrency,System.Int32,System.Int32>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m17251(__this, ___arg1, ___arg2, ___arg3, method) (( void (*) (Action_3_t91 *, VirtualCurrency_t77 *, int32_t, int32_t, MethodInfo*))Action_3_Invoke_m17252_gshared)(__this, ___arg1, ___arg2, ___arg3, method)
// System.IAsyncResult System.Action`3<Soomla.Store.VirtualCurrency,System.Int32,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m17253(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method) (( Object_t * (*) (Action_3_t91 *, VirtualCurrency_t77 *, int32_t, int32_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_3_BeginInvoke_m17254_gshared)(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method)
// System.Void System.Action`3<Soomla.Store.VirtualCurrency,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m17255(__this, ___result, method) (( void (*) (Action_3_t91 *, Object_t *, MethodInfo*))Action_3_EndInvoke_m17256_gshared)(__this, ___result, method)
