﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t1330;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t1331;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.CanvasListPool
struct  CanvasListPool_t1332  : public Object_t
{
};
struct CanvasListPool_t1332_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>> UnityEngine.UI.CanvasListPool::s_CanvasListPool
	ObjectPool_1_t1330 * ___s_CanvasListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>> UnityEngine.UI.CanvasListPool::<>f__am$cache1
	UnityAction_1_t1331 * ___U3CU3Ef__amU24cache1_1;
};
