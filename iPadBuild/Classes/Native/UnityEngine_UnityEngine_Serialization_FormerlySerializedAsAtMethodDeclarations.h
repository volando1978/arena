﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t1430;
// System.String
struct String_t;

// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C" void FormerlySerializedAsAttribute__ctor_m6202 (FormerlySerializedAsAttribute_t1430 * __this, String_t* ___oldName, MethodInfo* method) IL2CPP_METHOD_ATTR;
