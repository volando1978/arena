﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>
struct ValueCollection_t3673;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t343;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t4359;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.UInt32[]
struct UInt32U5BU5D_t2194;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_50.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_22MethodDeclarations.h"
#define ValueCollection__ctor_m19075(__this, ___dictionary, method) (( void (*) (ValueCollection_t3673 *, Dictionary_2_t343 *, MethodInfo*))ValueCollection__ctor_m19003_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19076(__this, ___item, method) (( void (*) (ValueCollection_t3673 *, uint32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19004_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19077(__this, method) (( void (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19005_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19078(__this, ___item, method) (( bool (*) (ValueCollection_t3673 *, uint32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19006_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19079(__this, ___item, method) (( bool (*) (ValueCollection_t3673 *, uint32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19007_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19080(__this, method) (( Object_t* (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19008_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m19081(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3673 *, Array_t *, int32_t, MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m19009_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19082(__this, method) (( Object_t * (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19083(__this, method) (( bool (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19011_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19084(__this, method) (( bool (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19012_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m19085(__this, method) (( Object_t * (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m19013_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m19086(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3673 *, UInt32U5BU5D_t2194*, int32_t, MethodInfo*))ValueCollection_CopyTo_m19014_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::GetEnumerator()
#define ValueCollection_GetEnumerator_m19087(__this, method) (( Enumerator_t4361  (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_GetEnumerator_m19015_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::get_Count()
#define ValueCollection_get_Count_m19088(__this, method) (( int32_t (*) (ValueCollection_t3673 *, MethodInfo*))ValueCollection_get_Count_m19016_gshared)(__this, method)
