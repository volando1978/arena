﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t3860;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Comparison`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m21432_gshared (Comparison_1_t3860 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Comparison_1__ctor_m21432(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3860 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m21432_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m21433_gshared (Comparison_1_t3860 * __this, Vector3_t758  ___x, Vector3_t758  ___y, MethodInfo* method);
#define Comparison_1_Invoke_m21433(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3860 *, Vector3_t758 , Vector3_t758 , MethodInfo*))Comparison_1_Invoke_m21433_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Vector3>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m21434_gshared (Comparison_1_t3860 * __this, Vector3_t758  ___x, Vector3_t758  ___y, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Comparison_1_BeginInvoke_m21434(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3860 *, Vector3_t758 , Vector3_t758 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m21434_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m21435_gshared (Comparison_1_t3860 * __this, Object_t * ___result, MethodInfo* method);
#define Comparison_1_EndInvoke_m21435(__this, ___result, method) (( int32_t (*) (Comparison_1_t3860 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m21435_gshared)(__this, ___result, method)
