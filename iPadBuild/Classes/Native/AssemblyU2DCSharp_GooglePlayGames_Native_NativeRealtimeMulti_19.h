﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
struct RealtimeRoomConfig_t592;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t536;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B
struct  U3CCreateQuickGameU3Ec__AnonStorey2B_t593  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B::config
	RealtimeRoomConfig_t592 * ___config_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B::<>f__this
	NativeRealtimeMultiplayerClient_t536 * ___U3CU3Ef__this_1;
};
