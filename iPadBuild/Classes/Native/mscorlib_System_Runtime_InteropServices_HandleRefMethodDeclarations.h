﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.HandleRef
struct HandleRef_t657;
// System.Object
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C" void HandleRef__ctor_m3900 (HandleRef_t657 * __this, Object_t * ___wrapper, IntPtr_t ___handle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C" IntPtr_t HandleRef_get_Handle_m3902 (HandleRef_t657 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::ToIntPtr(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t HandleRef_ToIntPtr_m3926 (Object_t * __this /* static, unused */, HandleRef_t657  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
