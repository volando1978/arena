﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.SingleUsePackVG
struct SingleUsePackVG_t131;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.SingleUsePackVG::.ctor(System.String,System.Int32,System.String,System.String,System.String,Soomla.Store.PurchaseType)
extern "C" void SingleUsePackVG__ctor_m638 (SingleUsePackVG_t131 * __this, String_t* ___goodItemId, int32_t ___amount, String_t* ___name, String_t* ___description, String_t* ___itemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SingleUsePackVG::.ctor(JSONObject)
extern "C" void SingleUsePackVG__ctor_m639 (SingleUsePackVG_t131 * __this, JSONObject_t30 * ___jsonItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SingleUsePackVG::.cctor()
extern "C" void SingleUsePackVG__cctor_m640 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.SingleUsePackVG::toJSONObject()
extern "C" JSONObject_t30 * SingleUsePackVG_toJSONObject_m641 (SingleUsePackVG_t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.SingleUsePackVG::Give(System.Int32,System.Boolean)
extern "C" int32_t SingleUsePackVG_Give_m642 (SingleUsePackVG_t131 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.SingleUsePackVG::Take(System.Int32,System.Boolean)
extern "C" int32_t SingleUsePackVG_Take_m643 (SingleUsePackVG_t131 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.SingleUsePackVG::ResetBalance(System.Int32,System.Boolean)
extern "C" int32_t SingleUsePackVG_ResetBalance_m644 (SingleUsePackVG_t131 * __this, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.SingleUsePackVG::GetBalance()
extern "C" int32_t SingleUsePackVG_GetBalance_m645 (SingleUsePackVG_t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.SingleUsePackVG::canBuy()
extern "C" bool SingleUsePackVG_canBuy_m646 (SingleUsePackVG_t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
