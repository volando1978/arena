﻿#pragma once
#include <stdint.h>
// UnityEngine.CanvasGroup
struct CanvasGroup_t1387;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.CanvasGroup>
struct  Comparison_1_t4026  : public MulticastDelegate_t22
{
};
