﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCache.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache
extern TypeInfo FactoryCache_t2066_il2cpp_TypeInfo;
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCacheMethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo FactoryCache_t2066_FactoryCache__ctor_m8375_ParameterInfos[] = 
{
	{"capacity", 0, 134218167, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::.ctor(System.Int32)
MethodInfo FactoryCache__ctor_m8375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FactoryCache__ctor_m8375/* method */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, FactoryCache_t2066_FactoryCache__ctor_m8375_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType RegexOptions_t2068_0_0_0;
extern Il2CppType RegexOptions_t2068_0_0_0;
extern Il2CppType IMachineFactory_t2067_0_0_0;
extern Il2CppType IMachineFactory_t2067_0_0_0;
static ParameterInfo FactoryCache_t2066_FactoryCache_Add_m8376_ParameterInfos[] = 
{
	{"pattern", 0, 134218168, 0, &String_t_0_0_0},
	{"options", 1, 134218169, 0, &RegexOptions_t2068_0_0_0},
	{"factory", 2, 134218170, 0, &IMachineFactory_t2067_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Add(System.String,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.IMachineFactory)
MethodInfo FactoryCache_Add_m8376_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&FactoryCache_Add_m8376/* method */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t/* invoker_method */
	, FactoryCache_t2066_FactoryCache_Add_m8376_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Cleanup()
MethodInfo FactoryCache_Cleanup_m8377_MethodInfo = 
{
	"Cleanup"/* name */
	, (methodPointerType)&FactoryCache_Cleanup_m8377/* method */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo FactoryCache_t2066_FactoryCache_Lookup_m8378_ParameterInfos[] = 
{
	{"pattern", 0, 134218171, 0, &String_t_0_0_0},
	{"options", 1, 134218172, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType IMachineFactory_t2067_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.FactoryCache::Lookup(System.String,System.Text.RegularExpressions.RegexOptions)
MethodInfo FactoryCache_Lookup_m8378_MethodInfo = 
{
	"Lookup"/* name */
	, (methodPointerType)&FactoryCache_Lookup_m8378/* method */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t2067_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t189/* invoker_method */
	, FactoryCache_t2066_FactoryCache_Lookup_m8378_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FactoryCache_t2066_MethodInfos[] =
{
	&FactoryCache__ctor_m8375_MethodInfo,
	&FactoryCache_Add_m8376_MethodInfo,
	&FactoryCache_Cleanup_m8377_MethodInfo,
	&FactoryCache_Lookup_m8378_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo FactoryCache_t2066____capacity_0_FieldInfo = 
{
	"capacity"/* name */
	, &Int32_t189_0_0_1/* type */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* parent */
	, offsetof(FactoryCache_t2066, ___capacity_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_1;
FieldInfo FactoryCache_t2066____factories_1_FieldInfo = 
{
	"factories"/* name */
	, &Hashtable_t1667_0_0_1/* type */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* parent */
	, offsetof(FactoryCache_t2066, ___factories_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MRUList_t2073_0_0_1;
FieldInfo FactoryCache_t2066____mru_list_2_FieldInfo = 
{
	"mru_list"/* name */
	, &MRUList_t2073_0_0_1/* type */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* parent */
	, offsetof(FactoryCache_t2066, ___mru_list_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* FactoryCache_t2066_FieldInfos[] =
{
	&FactoryCache_t2066____capacity_0_FieldInfo,
	&FactoryCache_t2066____factories_1_FieldInfo,
	&FactoryCache_t2066____mru_list_2_FieldInfo,
	NULL
};
extern Il2CppType Key_t2072_0_0_0;
static const Il2CppType* FactoryCache_t2066_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Key_t2072_0_0_0,
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo Object_ToString_m1179_MethodInfo;
static Il2CppMethodReference FactoryCache_t2066_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool FactoryCache_t2066_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType FactoryCache_t2066_0_0_0;
extern Il2CppType FactoryCache_t2066_1_0_0;
extern Il2CppType Object_t_0_0_0;
struct FactoryCache_t2066;
const Il2CppTypeDefinitionMetadata FactoryCache_t2066_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FactoryCache_t2066_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FactoryCache_t2066_VTable/* vtableMethods */
	, FactoryCache_t2066_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo FactoryCache_t2066_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FactoryCache"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, FactoryCache_t2066_MethodInfos/* methods */
	, NULL/* properties */
	, FactoryCache_t2066_FieldInfos/* fields */
	, NULL/* events */
	, &FactoryCache_t2066_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FactoryCache_t2066_0_0_0/* byval_arg */
	, &FactoryCache_t2066_1_0_0/* this_arg */
	, &FactoryCache_t2066_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FactoryCache_t2066)/* instance_size */
	, sizeof (FactoryCache_t2066)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_Node.h"
// Metadata Definition System.Text.RegularExpressions.MRUList/Node
extern TypeInfo Node_t2074_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_NodeMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Node_t2074_Node__ctor_m8379_ParameterInfos[] = 
{
	{"value", 0, 134218177, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList/Node::.ctor(System.Object)
MethodInfo Node__ctor_m8379_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Node__ctor_m8379/* method */
	, &Node_t2074_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Node_t2074_Node__ctor_m8379_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Node_t2074_MethodInfos[] =
{
	&Node__ctor_m8379_MethodInfo,
	NULL
};
extern Il2CppType Object_t_0_0_6;
FieldInfo Node_t2074____value_0_FieldInfo = 
{
	"value"/* name */
	, &Object_t_0_0_6/* type */
	, &Node_t2074_il2cpp_TypeInfo/* parent */
	, offsetof(Node_t2074, ___value_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Node_t2074_0_0_6;
FieldInfo Node_t2074____previous_1_FieldInfo = 
{
	"previous"/* name */
	, &Node_t2074_0_0_6/* type */
	, &Node_t2074_il2cpp_TypeInfo/* parent */
	, offsetof(Node_t2074, ___previous_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Node_t2074_0_0_6;
FieldInfo Node_t2074____next_2_FieldInfo = 
{
	"next"/* name */
	, &Node_t2074_0_0_6/* type */
	, &Node_t2074_il2cpp_TypeInfo/* parent */
	, offsetof(Node_t2074, ___next_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Node_t2074_FieldInfos[] =
{
	&Node_t2074____value_0_FieldInfo,
	&Node_t2074____previous_1_FieldInfo,
	&Node_t2074____next_2_FieldInfo,
	NULL
};
static Il2CppMethodReference Node_t2074_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Node_t2074_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Node_t2074_0_0_0;
extern Il2CppType Node_t2074_1_0_0;
extern TypeInfo MRUList_t2073_il2cpp_TypeInfo;
extern Il2CppType MRUList_t2073_0_0_0;
struct Node_t2074;
const Il2CppTypeDefinitionMetadata Node_t2074_DefinitionMetadata = 
{
	&MRUList_t2073_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Node_t2074_VTable/* vtableMethods */
	, Node_t2074_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Node_t2074_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Node"/* name */
	, ""/* namespaze */
	, Node_t2074_MethodInfos/* methods */
	, NULL/* properties */
	, Node_t2074_FieldInfos/* fields */
	, NULL/* events */
	, &Node_t2074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Node_t2074_0_0_0/* byval_arg */
	, &Node_t2074_1_0_0/* this_arg */
	, &Node_t2074_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Node_t2074)/* instance_size */
	, sizeof (Node_t2074)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUList.h"
// Metadata Definition System.Text.RegularExpressions.MRUList
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUListMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::.ctor()
MethodInfo MRUList__ctor_m8380_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MRUList__ctor_m8380/* method */
	, &MRUList_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MRUList_t2073_MRUList_Use_m8381_ParameterInfos[] = 
{
	{"o", 0, 134218176, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::Use(System.Object)
MethodInfo MRUList_Use_m8381_MethodInfo = 
{
	"Use"/* name */
	, (methodPointerType)&MRUList_Use_m8381/* method */
	, &MRUList_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MRUList_t2073_MRUList_Use_m8381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.MRUList::Evict()
MethodInfo MRUList_Evict_m8382_MethodInfo = 
{
	"Evict"/* name */
	, (methodPointerType)&MRUList_Evict_m8382/* method */
	, &MRUList_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MRUList_t2073_MethodInfos[] =
{
	&MRUList__ctor_m8380_MethodInfo,
	&MRUList_Use_m8381_MethodInfo,
	&MRUList_Evict_m8382_MethodInfo,
	NULL
};
extern Il2CppType Node_t2074_0_0_1;
FieldInfo MRUList_t2073____head_0_FieldInfo = 
{
	"head"/* name */
	, &Node_t2074_0_0_1/* type */
	, &MRUList_t2073_il2cpp_TypeInfo/* parent */
	, offsetof(MRUList_t2073, ___head_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Node_t2074_0_0_1;
FieldInfo MRUList_t2073____tail_1_FieldInfo = 
{
	"tail"/* name */
	, &Node_t2074_0_0_1/* type */
	, &MRUList_t2073_il2cpp_TypeInfo/* parent */
	, offsetof(MRUList_t2073, ___tail_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* MRUList_t2073_FieldInfos[] =
{
	&MRUList_t2073____head_0_FieldInfo,
	&MRUList_t2073____tail_1_FieldInfo,
	NULL
};
static const Il2CppType* MRUList_t2073_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Node_t2074_0_0_0,
};
static Il2CppMethodReference MRUList_t2073_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool MRUList_t2073_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType MRUList_t2073_1_0_0;
struct MRUList_t2073;
const Il2CppTypeDefinitionMetadata MRUList_t2073_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MRUList_t2073_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MRUList_t2073_VTable/* vtableMethods */
	, MRUList_t2073_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MRUList_t2073_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MRUList"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, MRUList_t2073_MethodInfos/* methods */
	, NULL/* properties */
	, MRUList_t2073_FieldInfos/* fields */
	, NULL/* events */
	, &MRUList_t2073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MRUList_t2073_0_0_0/* byval_arg */
	, &MRUList_t2073_1_0_0/* this_arg */
	, &MRUList_t2073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MRUList_t2073)/* instance_size */
	, sizeof (MRUList_t2073)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
// Metadata Definition System.Text.RegularExpressions.Category
extern TypeInfo Category_t2075_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_CategoryMethodDeclarations.h"
static MethodInfo* Category_t2075_MethodInfos[] =
{
	NULL
};
extern Il2CppType UInt16_t194_0_0_1542;
FieldInfo Category_t2075____value___1_FieldInfo = 
{
	"value__"/* name */
	, &UInt16_t194_0_0_1542/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, offsetof(Category_t2075, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____None_2_FieldInfo = 
{
	"None"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____Any_3_FieldInfo = 
{
	"Any"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____AnySingleline_4_FieldInfo = 
{
	"AnySingleline"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____Word_5_FieldInfo = 
{
	"Word"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____Digit_6_FieldInfo = 
{
	"Digit"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____WhiteSpace_7_FieldInfo = 
{
	"WhiteSpace"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____EcmaAny_8_FieldInfo = 
{
	"EcmaAny"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____EcmaAnySingleline_9_FieldInfo = 
{
	"EcmaAnySingleline"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____EcmaWord_10_FieldInfo = 
{
	"EcmaWord"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____EcmaDigit_11_FieldInfo = 
{
	"EcmaDigit"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____EcmaWhiteSpace_12_FieldInfo = 
{
	"EcmaWhiteSpace"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeL_13_FieldInfo = 
{
	"UnicodeL"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeM_14_FieldInfo = 
{
	"UnicodeM"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeN_15_FieldInfo = 
{
	"UnicodeN"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeZ_16_FieldInfo = 
{
	"UnicodeZ"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeP_17_FieldInfo = 
{
	"UnicodeP"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeS_18_FieldInfo = 
{
	"UnicodeS"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeC_19_FieldInfo = 
{
	"UnicodeC"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLu_20_FieldInfo = 
{
	"UnicodeLu"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLl_21_FieldInfo = 
{
	"UnicodeLl"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLt_22_FieldInfo = 
{
	"UnicodeLt"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLm_23_FieldInfo = 
{
	"UnicodeLm"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLo_24_FieldInfo = 
{
	"UnicodeLo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMn_25_FieldInfo = 
{
	"UnicodeMn"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMe_26_FieldInfo = 
{
	"UnicodeMe"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMc_27_FieldInfo = 
{
	"UnicodeMc"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeNd_28_FieldInfo = 
{
	"UnicodeNd"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeNl_29_FieldInfo = 
{
	"UnicodeNl"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeNo_30_FieldInfo = 
{
	"UnicodeNo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeZs_31_FieldInfo = 
{
	"UnicodeZs"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeZl_32_FieldInfo = 
{
	"UnicodeZl"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeZp_33_FieldInfo = 
{
	"UnicodeZp"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePd_34_FieldInfo = 
{
	"UnicodePd"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePs_35_FieldInfo = 
{
	"UnicodePs"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePi_36_FieldInfo = 
{
	"UnicodePi"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePe_37_FieldInfo = 
{
	"UnicodePe"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePf_38_FieldInfo = 
{
	"UnicodePf"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePc_39_FieldInfo = 
{
	"UnicodePc"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePo_40_FieldInfo = 
{
	"UnicodePo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSm_41_FieldInfo = 
{
	"UnicodeSm"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSc_42_FieldInfo = 
{
	"UnicodeSc"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSk_43_FieldInfo = 
{
	"UnicodeSk"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSo_44_FieldInfo = 
{
	"UnicodeSo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCc_45_FieldInfo = 
{
	"UnicodeCc"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCf_46_FieldInfo = 
{
	"UnicodeCf"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCo_47_FieldInfo = 
{
	"UnicodeCo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCs_48_FieldInfo = 
{
	"UnicodeCs"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCn_49_FieldInfo = 
{
	"UnicodeCn"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeBasicLatin_50_FieldInfo = 
{
	"UnicodeBasicLatin"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLatin1Supplement_51_FieldInfo = 
{
	"UnicodeLatin1Supplement"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLatinExtendedA_52_FieldInfo = 
{
	"UnicodeLatinExtendedA"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLatinExtendedB_53_FieldInfo = 
{
	"UnicodeLatinExtendedB"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeIPAExtensions_54_FieldInfo = 
{
	"UnicodeIPAExtensions"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSpacingModifierLetters_55_FieldInfo = 
{
	"UnicodeSpacingModifierLetters"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCombiningDiacriticalMarks_56_FieldInfo = 
{
	"UnicodeCombiningDiacriticalMarks"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGreek_57_FieldInfo = 
{
	"UnicodeGreek"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCyrillic_58_FieldInfo = 
{
	"UnicodeCyrillic"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeArmenian_59_FieldInfo = 
{
	"UnicodeArmenian"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHebrew_60_FieldInfo = 
{
	"UnicodeHebrew"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeArabic_61_FieldInfo = 
{
	"UnicodeArabic"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSyriac_62_FieldInfo = 
{
	"UnicodeSyriac"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeThaana_63_FieldInfo = 
{
	"UnicodeThaana"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeDevanagari_64_FieldInfo = 
{
	"UnicodeDevanagari"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeBengali_65_FieldInfo = 
{
	"UnicodeBengali"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGurmukhi_66_FieldInfo = 
{
	"UnicodeGurmukhi"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGujarati_67_FieldInfo = 
{
	"UnicodeGujarati"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeOriya_68_FieldInfo = 
{
	"UnicodeOriya"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeTamil_69_FieldInfo = 
{
	"UnicodeTamil"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeTelugu_70_FieldInfo = 
{
	"UnicodeTelugu"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeKannada_71_FieldInfo = 
{
	"UnicodeKannada"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMalayalam_72_FieldInfo = 
{
	"UnicodeMalayalam"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSinhala_73_FieldInfo = 
{
	"UnicodeSinhala"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeThai_74_FieldInfo = 
{
	"UnicodeThai"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLao_75_FieldInfo = 
{
	"UnicodeLao"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeTibetan_76_FieldInfo = 
{
	"UnicodeTibetan"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMyanmar_77_FieldInfo = 
{
	"UnicodeMyanmar"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGeorgian_78_FieldInfo = 
{
	"UnicodeGeorgian"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHangulJamo_79_FieldInfo = 
{
	"UnicodeHangulJamo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeEthiopic_80_FieldInfo = 
{
	"UnicodeEthiopic"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCherokee_81_FieldInfo = 
{
	"UnicodeCherokee"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeUnifiedCanadianAboriginalSyllabics_82_FieldInfo = 
{
	"UnicodeUnifiedCanadianAboriginalSyllabics"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeOgham_83_FieldInfo = 
{
	"UnicodeOgham"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeRunic_84_FieldInfo = 
{
	"UnicodeRunic"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeKhmer_85_FieldInfo = 
{
	"UnicodeKhmer"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMongolian_86_FieldInfo = 
{
	"UnicodeMongolian"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLatinExtendedAdditional_87_FieldInfo = 
{
	"UnicodeLatinExtendedAdditional"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGreekExtended_88_FieldInfo = 
{
	"UnicodeGreekExtended"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGeneralPunctuation_89_FieldInfo = 
{
	"UnicodeGeneralPunctuation"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSuperscriptsandSubscripts_90_FieldInfo = 
{
	"UnicodeSuperscriptsandSubscripts"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCurrencySymbols_91_FieldInfo = 
{
	"UnicodeCurrencySymbols"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCombiningMarksforSymbols_92_FieldInfo = 
{
	"UnicodeCombiningMarksforSymbols"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLetterlikeSymbols_93_FieldInfo = 
{
	"UnicodeLetterlikeSymbols"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeNumberForms_94_FieldInfo = 
{
	"UnicodeNumberForms"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeArrows_95_FieldInfo = 
{
	"UnicodeArrows"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMathematicalOperators_96_FieldInfo = 
{
	"UnicodeMathematicalOperators"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMiscellaneousTechnical_97_FieldInfo = 
{
	"UnicodeMiscellaneousTechnical"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeControlPictures_98_FieldInfo = 
{
	"UnicodeControlPictures"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeOpticalCharacterRecognition_99_FieldInfo = 
{
	"UnicodeOpticalCharacterRecognition"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeEnclosedAlphanumerics_100_FieldInfo = 
{
	"UnicodeEnclosedAlphanumerics"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeBoxDrawing_101_FieldInfo = 
{
	"UnicodeBoxDrawing"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeBlockElements_102_FieldInfo = 
{
	"UnicodeBlockElements"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGeometricShapes_103_FieldInfo = 
{
	"UnicodeGeometricShapes"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMiscellaneousSymbols_104_FieldInfo = 
{
	"UnicodeMiscellaneousSymbols"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeDingbats_105_FieldInfo = 
{
	"UnicodeDingbats"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeBraillePatterns_106_FieldInfo = 
{
	"UnicodeBraillePatterns"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKRadicalsSupplement_107_FieldInfo = 
{
	"UnicodeCJKRadicalsSupplement"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeKangxiRadicals_108_FieldInfo = 
{
	"UnicodeKangxiRadicals"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeIdeographicDescriptionCharacters_109_FieldInfo = 
{
	"UnicodeIdeographicDescriptionCharacters"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKSymbolsandPunctuation_110_FieldInfo = 
{
	"UnicodeCJKSymbolsandPunctuation"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHiragana_111_FieldInfo = 
{
	"UnicodeHiragana"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeKatakana_112_FieldInfo = 
{
	"UnicodeKatakana"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeBopomofo_113_FieldInfo = 
{
	"UnicodeBopomofo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHangulCompatibilityJamo_114_FieldInfo = 
{
	"UnicodeHangulCompatibilityJamo"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeKanbun_115_FieldInfo = 
{
	"UnicodeKanbun"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeBopomofoExtended_116_FieldInfo = 
{
	"UnicodeBopomofoExtended"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeEnclosedCJKLettersandMonths_117_FieldInfo = 
{
	"UnicodeEnclosedCJKLettersandMonths"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKCompatibility_118_FieldInfo = 
{
	"UnicodeCJKCompatibility"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKUnifiedIdeographsExtensionA_119_FieldInfo = 
{
	"UnicodeCJKUnifiedIdeographsExtensionA"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKUnifiedIdeographs_120_FieldInfo = 
{
	"UnicodeCJKUnifiedIdeographs"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeYiSyllables_121_FieldInfo = 
{
	"UnicodeYiSyllables"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeYiRadicals_122_FieldInfo = 
{
	"UnicodeYiRadicals"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHangulSyllables_123_FieldInfo = 
{
	"UnicodeHangulSyllables"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHighSurrogates_124_FieldInfo = 
{
	"UnicodeHighSurrogates"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHighPrivateUseSurrogates_125_FieldInfo = 
{
	"UnicodeHighPrivateUseSurrogates"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeLowSurrogates_126_FieldInfo = 
{
	"UnicodeLowSurrogates"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodePrivateUse_127_FieldInfo = 
{
	"UnicodePrivateUse"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKCompatibilityIdeographs_128_FieldInfo = 
{
	"UnicodeCJKCompatibilityIdeographs"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeAlphabeticPresentationForms_129_FieldInfo = 
{
	"UnicodeAlphabeticPresentationForms"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeArabicPresentationFormsA_130_FieldInfo = 
{
	"UnicodeArabicPresentationFormsA"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCombiningHalfMarks_131_FieldInfo = 
{
	"UnicodeCombiningHalfMarks"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKCompatibilityForms_132_FieldInfo = 
{
	"UnicodeCJKCompatibilityForms"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSmallFormVariants_133_FieldInfo = 
{
	"UnicodeSmallFormVariants"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeArabicPresentationFormsB_134_FieldInfo = 
{
	"UnicodeArabicPresentationFormsB"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeSpecials_135_FieldInfo = 
{
	"UnicodeSpecials"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeHalfwidthandFullwidthForms_136_FieldInfo = 
{
	"UnicodeHalfwidthandFullwidthForms"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeOldItalic_137_FieldInfo = 
{
	"UnicodeOldItalic"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeGothic_138_FieldInfo = 
{
	"UnicodeGothic"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeDeseret_139_FieldInfo = 
{
	"UnicodeDeseret"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeByzantineMusicalSymbols_140_FieldInfo = 
{
	"UnicodeByzantineMusicalSymbols"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMusicalSymbols_141_FieldInfo = 
{
	"UnicodeMusicalSymbols"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeMathematicalAlphanumericSymbols_142_FieldInfo = 
{
	"UnicodeMathematicalAlphanumericSymbols"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKUnifiedIdeographsExtensionB_143_FieldInfo = 
{
	"UnicodeCJKUnifiedIdeographsExtensionB"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeCJKCompatibilityIdeographsSupplement_144_FieldInfo = 
{
	"UnicodeCJKCompatibilityIdeographsSupplement"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____UnicodeTags_145_FieldInfo = 
{
	"UnicodeTags"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Category_t2075_0_0_32854;
FieldInfo Category_t2075____LastValue_146_FieldInfo = 
{
	"LastValue"/* name */
	, &Category_t2075_0_0_32854/* type */
	, &Category_t2075_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Category_t2075_FieldInfos[] =
{
	&Category_t2075____value___1_FieldInfo,
	&Category_t2075____None_2_FieldInfo,
	&Category_t2075____Any_3_FieldInfo,
	&Category_t2075____AnySingleline_4_FieldInfo,
	&Category_t2075____Word_5_FieldInfo,
	&Category_t2075____Digit_6_FieldInfo,
	&Category_t2075____WhiteSpace_7_FieldInfo,
	&Category_t2075____EcmaAny_8_FieldInfo,
	&Category_t2075____EcmaAnySingleline_9_FieldInfo,
	&Category_t2075____EcmaWord_10_FieldInfo,
	&Category_t2075____EcmaDigit_11_FieldInfo,
	&Category_t2075____EcmaWhiteSpace_12_FieldInfo,
	&Category_t2075____UnicodeL_13_FieldInfo,
	&Category_t2075____UnicodeM_14_FieldInfo,
	&Category_t2075____UnicodeN_15_FieldInfo,
	&Category_t2075____UnicodeZ_16_FieldInfo,
	&Category_t2075____UnicodeP_17_FieldInfo,
	&Category_t2075____UnicodeS_18_FieldInfo,
	&Category_t2075____UnicodeC_19_FieldInfo,
	&Category_t2075____UnicodeLu_20_FieldInfo,
	&Category_t2075____UnicodeLl_21_FieldInfo,
	&Category_t2075____UnicodeLt_22_FieldInfo,
	&Category_t2075____UnicodeLm_23_FieldInfo,
	&Category_t2075____UnicodeLo_24_FieldInfo,
	&Category_t2075____UnicodeMn_25_FieldInfo,
	&Category_t2075____UnicodeMe_26_FieldInfo,
	&Category_t2075____UnicodeMc_27_FieldInfo,
	&Category_t2075____UnicodeNd_28_FieldInfo,
	&Category_t2075____UnicodeNl_29_FieldInfo,
	&Category_t2075____UnicodeNo_30_FieldInfo,
	&Category_t2075____UnicodeZs_31_FieldInfo,
	&Category_t2075____UnicodeZl_32_FieldInfo,
	&Category_t2075____UnicodeZp_33_FieldInfo,
	&Category_t2075____UnicodePd_34_FieldInfo,
	&Category_t2075____UnicodePs_35_FieldInfo,
	&Category_t2075____UnicodePi_36_FieldInfo,
	&Category_t2075____UnicodePe_37_FieldInfo,
	&Category_t2075____UnicodePf_38_FieldInfo,
	&Category_t2075____UnicodePc_39_FieldInfo,
	&Category_t2075____UnicodePo_40_FieldInfo,
	&Category_t2075____UnicodeSm_41_FieldInfo,
	&Category_t2075____UnicodeSc_42_FieldInfo,
	&Category_t2075____UnicodeSk_43_FieldInfo,
	&Category_t2075____UnicodeSo_44_FieldInfo,
	&Category_t2075____UnicodeCc_45_FieldInfo,
	&Category_t2075____UnicodeCf_46_FieldInfo,
	&Category_t2075____UnicodeCo_47_FieldInfo,
	&Category_t2075____UnicodeCs_48_FieldInfo,
	&Category_t2075____UnicodeCn_49_FieldInfo,
	&Category_t2075____UnicodeBasicLatin_50_FieldInfo,
	&Category_t2075____UnicodeLatin1Supplement_51_FieldInfo,
	&Category_t2075____UnicodeLatinExtendedA_52_FieldInfo,
	&Category_t2075____UnicodeLatinExtendedB_53_FieldInfo,
	&Category_t2075____UnicodeIPAExtensions_54_FieldInfo,
	&Category_t2075____UnicodeSpacingModifierLetters_55_FieldInfo,
	&Category_t2075____UnicodeCombiningDiacriticalMarks_56_FieldInfo,
	&Category_t2075____UnicodeGreek_57_FieldInfo,
	&Category_t2075____UnicodeCyrillic_58_FieldInfo,
	&Category_t2075____UnicodeArmenian_59_FieldInfo,
	&Category_t2075____UnicodeHebrew_60_FieldInfo,
	&Category_t2075____UnicodeArabic_61_FieldInfo,
	&Category_t2075____UnicodeSyriac_62_FieldInfo,
	&Category_t2075____UnicodeThaana_63_FieldInfo,
	&Category_t2075____UnicodeDevanagari_64_FieldInfo,
	&Category_t2075____UnicodeBengali_65_FieldInfo,
	&Category_t2075____UnicodeGurmukhi_66_FieldInfo,
	&Category_t2075____UnicodeGujarati_67_FieldInfo,
	&Category_t2075____UnicodeOriya_68_FieldInfo,
	&Category_t2075____UnicodeTamil_69_FieldInfo,
	&Category_t2075____UnicodeTelugu_70_FieldInfo,
	&Category_t2075____UnicodeKannada_71_FieldInfo,
	&Category_t2075____UnicodeMalayalam_72_FieldInfo,
	&Category_t2075____UnicodeSinhala_73_FieldInfo,
	&Category_t2075____UnicodeThai_74_FieldInfo,
	&Category_t2075____UnicodeLao_75_FieldInfo,
	&Category_t2075____UnicodeTibetan_76_FieldInfo,
	&Category_t2075____UnicodeMyanmar_77_FieldInfo,
	&Category_t2075____UnicodeGeorgian_78_FieldInfo,
	&Category_t2075____UnicodeHangulJamo_79_FieldInfo,
	&Category_t2075____UnicodeEthiopic_80_FieldInfo,
	&Category_t2075____UnicodeCherokee_81_FieldInfo,
	&Category_t2075____UnicodeUnifiedCanadianAboriginalSyllabics_82_FieldInfo,
	&Category_t2075____UnicodeOgham_83_FieldInfo,
	&Category_t2075____UnicodeRunic_84_FieldInfo,
	&Category_t2075____UnicodeKhmer_85_FieldInfo,
	&Category_t2075____UnicodeMongolian_86_FieldInfo,
	&Category_t2075____UnicodeLatinExtendedAdditional_87_FieldInfo,
	&Category_t2075____UnicodeGreekExtended_88_FieldInfo,
	&Category_t2075____UnicodeGeneralPunctuation_89_FieldInfo,
	&Category_t2075____UnicodeSuperscriptsandSubscripts_90_FieldInfo,
	&Category_t2075____UnicodeCurrencySymbols_91_FieldInfo,
	&Category_t2075____UnicodeCombiningMarksforSymbols_92_FieldInfo,
	&Category_t2075____UnicodeLetterlikeSymbols_93_FieldInfo,
	&Category_t2075____UnicodeNumberForms_94_FieldInfo,
	&Category_t2075____UnicodeArrows_95_FieldInfo,
	&Category_t2075____UnicodeMathematicalOperators_96_FieldInfo,
	&Category_t2075____UnicodeMiscellaneousTechnical_97_FieldInfo,
	&Category_t2075____UnicodeControlPictures_98_FieldInfo,
	&Category_t2075____UnicodeOpticalCharacterRecognition_99_FieldInfo,
	&Category_t2075____UnicodeEnclosedAlphanumerics_100_FieldInfo,
	&Category_t2075____UnicodeBoxDrawing_101_FieldInfo,
	&Category_t2075____UnicodeBlockElements_102_FieldInfo,
	&Category_t2075____UnicodeGeometricShapes_103_FieldInfo,
	&Category_t2075____UnicodeMiscellaneousSymbols_104_FieldInfo,
	&Category_t2075____UnicodeDingbats_105_FieldInfo,
	&Category_t2075____UnicodeBraillePatterns_106_FieldInfo,
	&Category_t2075____UnicodeCJKRadicalsSupplement_107_FieldInfo,
	&Category_t2075____UnicodeKangxiRadicals_108_FieldInfo,
	&Category_t2075____UnicodeIdeographicDescriptionCharacters_109_FieldInfo,
	&Category_t2075____UnicodeCJKSymbolsandPunctuation_110_FieldInfo,
	&Category_t2075____UnicodeHiragana_111_FieldInfo,
	&Category_t2075____UnicodeKatakana_112_FieldInfo,
	&Category_t2075____UnicodeBopomofo_113_FieldInfo,
	&Category_t2075____UnicodeHangulCompatibilityJamo_114_FieldInfo,
	&Category_t2075____UnicodeKanbun_115_FieldInfo,
	&Category_t2075____UnicodeBopomofoExtended_116_FieldInfo,
	&Category_t2075____UnicodeEnclosedCJKLettersandMonths_117_FieldInfo,
	&Category_t2075____UnicodeCJKCompatibility_118_FieldInfo,
	&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionA_119_FieldInfo,
	&Category_t2075____UnicodeCJKUnifiedIdeographs_120_FieldInfo,
	&Category_t2075____UnicodeYiSyllables_121_FieldInfo,
	&Category_t2075____UnicodeYiRadicals_122_FieldInfo,
	&Category_t2075____UnicodeHangulSyllables_123_FieldInfo,
	&Category_t2075____UnicodeHighSurrogates_124_FieldInfo,
	&Category_t2075____UnicodeHighPrivateUseSurrogates_125_FieldInfo,
	&Category_t2075____UnicodeLowSurrogates_126_FieldInfo,
	&Category_t2075____UnicodePrivateUse_127_FieldInfo,
	&Category_t2075____UnicodeCJKCompatibilityIdeographs_128_FieldInfo,
	&Category_t2075____UnicodeAlphabeticPresentationForms_129_FieldInfo,
	&Category_t2075____UnicodeArabicPresentationFormsA_130_FieldInfo,
	&Category_t2075____UnicodeCombiningHalfMarks_131_FieldInfo,
	&Category_t2075____UnicodeCJKCompatibilityForms_132_FieldInfo,
	&Category_t2075____UnicodeSmallFormVariants_133_FieldInfo,
	&Category_t2075____UnicodeArabicPresentationFormsB_134_FieldInfo,
	&Category_t2075____UnicodeSpecials_135_FieldInfo,
	&Category_t2075____UnicodeHalfwidthandFullwidthForms_136_FieldInfo,
	&Category_t2075____UnicodeOldItalic_137_FieldInfo,
	&Category_t2075____UnicodeGothic_138_FieldInfo,
	&Category_t2075____UnicodeDeseret_139_FieldInfo,
	&Category_t2075____UnicodeByzantineMusicalSymbols_140_FieldInfo,
	&Category_t2075____UnicodeMusicalSymbols_141_FieldInfo,
	&Category_t2075____UnicodeMathematicalAlphanumericSymbols_142_FieldInfo,
	&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionB_143_FieldInfo,
	&Category_t2075____UnicodeCJKCompatibilityIdeographsSupplement_144_FieldInfo,
	&Category_t2075____UnicodeTags_145_FieldInfo,
	&Category_t2075____LastValue_146_FieldInfo,
	NULL
};
static const uint16_t Category_t2075____None_2_DefaultValueData = 0;
extern Il2CppType UInt16_t194_0_0_0;
static Il2CppFieldDefaultValueEntry Category_t2075____None_2_DefaultValue = 
{
	&Category_t2075____None_2_FieldInfo/* field */
	, { (char*)&Category_t2075____None_2_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____Any_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Category_t2075____Any_3_DefaultValue = 
{
	&Category_t2075____Any_3_FieldInfo/* field */
	, { (char*)&Category_t2075____Any_3_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____AnySingleline_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry Category_t2075____AnySingleline_4_DefaultValue = 
{
	&Category_t2075____AnySingleline_4_FieldInfo/* field */
	, { (char*)&Category_t2075____AnySingleline_4_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____Word_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry Category_t2075____Word_5_DefaultValue = 
{
	&Category_t2075____Word_5_FieldInfo/* field */
	, { (char*)&Category_t2075____Word_5_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____Digit_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry Category_t2075____Digit_6_DefaultValue = 
{
	&Category_t2075____Digit_6_FieldInfo/* field */
	, { (char*)&Category_t2075____Digit_6_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____WhiteSpace_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry Category_t2075____WhiteSpace_7_DefaultValue = 
{
	&Category_t2075____WhiteSpace_7_FieldInfo/* field */
	, { (char*)&Category_t2075____WhiteSpace_7_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____EcmaAny_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry Category_t2075____EcmaAny_8_DefaultValue = 
{
	&Category_t2075____EcmaAny_8_FieldInfo/* field */
	, { (char*)&Category_t2075____EcmaAny_8_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____EcmaAnySingleline_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry Category_t2075____EcmaAnySingleline_9_DefaultValue = 
{
	&Category_t2075____EcmaAnySingleline_9_FieldInfo/* field */
	, { (char*)&Category_t2075____EcmaAnySingleline_9_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____EcmaWord_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry Category_t2075____EcmaWord_10_DefaultValue = 
{
	&Category_t2075____EcmaWord_10_FieldInfo/* field */
	, { (char*)&Category_t2075____EcmaWord_10_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____EcmaDigit_11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry Category_t2075____EcmaDigit_11_DefaultValue = 
{
	&Category_t2075____EcmaDigit_11_FieldInfo/* field */
	, { (char*)&Category_t2075____EcmaDigit_11_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____EcmaWhiteSpace_12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry Category_t2075____EcmaWhiteSpace_12_DefaultValue = 
{
	&Category_t2075____EcmaWhiteSpace_12_FieldInfo/* field */
	, { (char*)&Category_t2075____EcmaWhiteSpace_12_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeL_13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeL_13_DefaultValue = 
{
	&Category_t2075____UnicodeL_13_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeL_13_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeM_14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeM_14_DefaultValue = 
{
	&Category_t2075____UnicodeM_14_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeM_14_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeN_15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeN_15_DefaultValue = 
{
	&Category_t2075____UnicodeN_15_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeN_15_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeZ_16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeZ_16_DefaultValue = 
{
	&Category_t2075____UnicodeZ_16_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeZ_16_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeP_17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeP_17_DefaultValue = 
{
	&Category_t2075____UnicodeP_17_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeP_17_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeS_18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeS_18_DefaultValue = 
{
	&Category_t2075____UnicodeS_18_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeS_18_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeC_19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeC_19_DefaultValue = 
{
	&Category_t2075____UnicodeC_19_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeC_19_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLu_20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLu_20_DefaultValue = 
{
	&Category_t2075____UnicodeLu_20_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLu_20_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLl_21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLl_21_DefaultValue = 
{
	&Category_t2075____UnicodeLl_21_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLl_21_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLt_22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLt_22_DefaultValue = 
{
	&Category_t2075____UnicodeLt_22_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLt_22_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLm_23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLm_23_DefaultValue = 
{
	&Category_t2075____UnicodeLm_23_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLm_23_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLo_24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLo_24_DefaultValue = 
{
	&Category_t2075____UnicodeLo_24_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLo_24_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMn_25_DefaultValueData = 23;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMn_25_DefaultValue = 
{
	&Category_t2075____UnicodeMn_25_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMn_25_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMe_26_DefaultValueData = 24;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMe_26_DefaultValue = 
{
	&Category_t2075____UnicodeMe_26_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMe_26_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMc_27_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMc_27_DefaultValue = 
{
	&Category_t2075____UnicodeMc_27_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMc_27_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeNd_28_DefaultValueData = 26;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeNd_28_DefaultValue = 
{
	&Category_t2075____UnicodeNd_28_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeNd_28_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeNl_29_DefaultValueData = 27;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeNl_29_DefaultValue = 
{
	&Category_t2075____UnicodeNl_29_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeNl_29_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeNo_30_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeNo_30_DefaultValue = 
{
	&Category_t2075____UnicodeNo_30_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeNo_30_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeZs_31_DefaultValueData = 29;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeZs_31_DefaultValue = 
{
	&Category_t2075____UnicodeZs_31_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeZs_31_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeZl_32_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeZl_32_DefaultValue = 
{
	&Category_t2075____UnicodeZl_32_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeZl_32_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeZp_33_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeZp_33_DefaultValue = 
{
	&Category_t2075____UnicodeZp_33_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeZp_33_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePd_34_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePd_34_DefaultValue = 
{
	&Category_t2075____UnicodePd_34_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePd_34_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePs_35_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePs_35_DefaultValue = 
{
	&Category_t2075____UnicodePs_35_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePs_35_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePi_36_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePi_36_DefaultValue = 
{
	&Category_t2075____UnicodePi_36_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePi_36_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePe_37_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePe_37_DefaultValue = 
{
	&Category_t2075____UnicodePe_37_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePe_37_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePf_38_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePf_38_DefaultValue = 
{
	&Category_t2075____UnicodePf_38_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePf_38_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePc_39_DefaultValueData = 37;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePc_39_DefaultValue = 
{
	&Category_t2075____UnicodePc_39_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePc_39_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePo_40_DefaultValueData = 38;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePo_40_DefaultValue = 
{
	&Category_t2075____UnicodePo_40_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePo_40_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSm_41_DefaultValueData = 39;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSm_41_DefaultValue = 
{
	&Category_t2075____UnicodeSm_41_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSm_41_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSc_42_DefaultValueData = 40;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSc_42_DefaultValue = 
{
	&Category_t2075____UnicodeSc_42_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSc_42_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSk_43_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSk_43_DefaultValue = 
{
	&Category_t2075____UnicodeSk_43_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSk_43_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSo_44_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSo_44_DefaultValue = 
{
	&Category_t2075____UnicodeSo_44_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSo_44_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCc_45_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCc_45_DefaultValue = 
{
	&Category_t2075____UnicodeCc_45_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCc_45_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCf_46_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCf_46_DefaultValue = 
{
	&Category_t2075____UnicodeCf_46_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCf_46_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCo_47_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCo_47_DefaultValue = 
{
	&Category_t2075____UnicodeCo_47_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCo_47_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCs_48_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCs_48_DefaultValue = 
{
	&Category_t2075____UnicodeCs_48_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCs_48_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCn_49_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCn_49_DefaultValue = 
{
	&Category_t2075____UnicodeCn_49_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCn_49_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeBasicLatin_50_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeBasicLatin_50_DefaultValue = 
{
	&Category_t2075____UnicodeBasicLatin_50_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeBasicLatin_50_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLatin1Supplement_51_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLatin1Supplement_51_DefaultValue = 
{
	&Category_t2075____UnicodeLatin1Supplement_51_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLatin1Supplement_51_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLatinExtendedA_52_DefaultValueData = 50;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLatinExtendedA_52_DefaultValue = 
{
	&Category_t2075____UnicodeLatinExtendedA_52_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLatinExtendedA_52_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLatinExtendedB_53_DefaultValueData = 51;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLatinExtendedB_53_DefaultValue = 
{
	&Category_t2075____UnicodeLatinExtendedB_53_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLatinExtendedB_53_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeIPAExtensions_54_DefaultValueData = 52;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeIPAExtensions_54_DefaultValue = 
{
	&Category_t2075____UnicodeIPAExtensions_54_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeIPAExtensions_54_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSpacingModifierLetters_55_DefaultValueData = 53;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSpacingModifierLetters_55_DefaultValue = 
{
	&Category_t2075____UnicodeSpacingModifierLetters_55_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSpacingModifierLetters_55_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCombiningDiacriticalMarks_56_DefaultValueData = 54;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCombiningDiacriticalMarks_56_DefaultValue = 
{
	&Category_t2075____UnicodeCombiningDiacriticalMarks_56_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCombiningDiacriticalMarks_56_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGreek_57_DefaultValueData = 55;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGreek_57_DefaultValue = 
{
	&Category_t2075____UnicodeGreek_57_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGreek_57_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCyrillic_58_DefaultValueData = 56;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCyrillic_58_DefaultValue = 
{
	&Category_t2075____UnicodeCyrillic_58_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCyrillic_58_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeArmenian_59_DefaultValueData = 57;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeArmenian_59_DefaultValue = 
{
	&Category_t2075____UnicodeArmenian_59_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeArmenian_59_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHebrew_60_DefaultValueData = 58;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHebrew_60_DefaultValue = 
{
	&Category_t2075____UnicodeHebrew_60_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHebrew_60_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeArabic_61_DefaultValueData = 59;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeArabic_61_DefaultValue = 
{
	&Category_t2075____UnicodeArabic_61_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeArabic_61_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSyriac_62_DefaultValueData = 60;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSyriac_62_DefaultValue = 
{
	&Category_t2075____UnicodeSyriac_62_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSyriac_62_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeThaana_63_DefaultValueData = 61;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeThaana_63_DefaultValue = 
{
	&Category_t2075____UnicodeThaana_63_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeThaana_63_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeDevanagari_64_DefaultValueData = 62;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeDevanagari_64_DefaultValue = 
{
	&Category_t2075____UnicodeDevanagari_64_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeDevanagari_64_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeBengali_65_DefaultValueData = 63;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeBengali_65_DefaultValue = 
{
	&Category_t2075____UnicodeBengali_65_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeBengali_65_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGurmukhi_66_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGurmukhi_66_DefaultValue = 
{
	&Category_t2075____UnicodeGurmukhi_66_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGurmukhi_66_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGujarati_67_DefaultValueData = 65;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGujarati_67_DefaultValue = 
{
	&Category_t2075____UnicodeGujarati_67_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGujarati_67_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeOriya_68_DefaultValueData = 66;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeOriya_68_DefaultValue = 
{
	&Category_t2075____UnicodeOriya_68_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeOriya_68_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeTamil_69_DefaultValueData = 67;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeTamil_69_DefaultValue = 
{
	&Category_t2075____UnicodeTamil_69_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeTamil_69_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeTelugu_70_DefaultValueData = 68;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeTelugu_70_DefaultValue = 
{
	&Category_t2075____UnicodeTelugu_70_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeTelugu_70_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeKannada_71_DefaultValueData = 69;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeKannada_71_DefaultValue = 
{
	&Category_t2075____UnicodeKannada_71_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeKannada_71_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMalayalam_72_DefaultValueData = 70;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMalayalam_72_DefaultValue = 
{
	&Category_t2075____UnicodeMalayalam_72_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMalayalam_72_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSinhala_73_DefaultValueData = 71;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSinhala_73_DefaultValue = 
{
	&Category_t2075____UnicodeSinhala_73_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSinhala_73_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeThai_74_DefaultValueData = 72;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeThai_74_DefaultValue = 
{
	&Category_t2075____UnicodeThai_74_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeThai_74_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLao_75_DefaultValueData = 73;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLao_75_DefaultValue = 
{
	&Category_t2075____UnicodeLao_75_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLao_75_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeTibetan_76_DefaultValueData = 74;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeTibetan_76_DefaultValue = 
{
	&Category_t2075____UnicodeTibetan_76_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeTibetan_76_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMyanmar_77_DefaultValueData = 75;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMyanmar_77_DefaultValue = 
{
	&Category_t2075____UnicodeMyanmar_77_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMyanmar_77_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGeorgian_78_DefaultValueData = 76;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGeorgian_78_DefaultValue = 
{
	&Category_t2075____UnicodeGeorgian_78_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGeorgian_78_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHangulJamo_79_DefaultValueData = 77;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHangulJamo_79_DefaultValue = 
{
	&Category_t2075____UnicodeHangulJamo_79_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHangulJamo_79_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeEthiopic_80_DefaultValueData = 78;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeEthiopic_80_DefaultValue = 
{
	&Category_t2075____UnicodeEthiopic_80_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeEthiopic_80_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCherokee_81_DefaultValueData = 79;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCherokee_81_DefaultValue = 
{
	&Category_t2075____UnicodeCherokee_81_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCherokee_81_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeUnifiedCanadianAboriginalSyllabics_82_DefaultValueData = 80;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeUnifiedCanadianAboriginalSyllabics_82_DefaultValue = 
{
	&Category_t2075____UnicodeUnifiedCanadianAboriginalSyllabics_82_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeUnifiedCanadianAboriginalSyllabics_82_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeOgham_83_DefaultValueData = 81;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeOgham_83_DefaultValue = 
{
	&Category_t2075____UnicodeOgham_83_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeOgham_83_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeRunic_84_DefaultValueData = 82;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeRunic_84_DefaultValue = 
{
	&Category_t2075____UnicodeRunic_84_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeRunic_84_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeKhmer_85_DefaultValueData = 83;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeKhmer_85_DefaultValue = 
{
	&Category_t2075____UnicodeKhmer_85_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeKhmer_85_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMongolian_86_DefaultValueData = 84;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMongolian_86_DefaultValue = 
{
	&Category_t2075____UnicodeMongolian_86_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMongolian_86_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLatinExtendedAdditional_87_DefaultValueData = 85;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLatinExtendedAdditional_87_DefaultValue = 
{
	&Category_t2075____UnicodeLatinExtendedAdditional_87_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLatinExtendedAdditional_87_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGreekExtended_88_DefaultValueData = 86;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGreekExtended_88_DefaultValue = 
{
	&Category_t2075____UnicodeGreekExtended_88_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGreekExtended_88_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGeneralPunctuation_89_DefaultValueData = 87;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGeneralPunctuation_89_DefaultValue = 
{
	&Category_t2075____UnicodeGeneralPunctuation_89_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGeneralPunctuation_89_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSuperscriptsandSubscripts_90_DefaultValueData = 88;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSuperscriptsandSubscripts_90_DefaultValue = 
{
	&Category_t2075____UnicodeSuperscriptsandSubscripts_90_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSuperscriptsandSubscripts_90_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCurrencySymbols_91_DefaultValueData = 89;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCurrencySymbols_91_DefaultValue = 
{
	&Category_t2075____UnicodeCurrencySymbols_91_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCurrencySymbols_91_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCombiningMarksforSymbols_92_DefaultValueData = 90;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCombiningMarksforSymbols_92_DefaultValue = 
{
	&Category_t2075____UnicodeCombiningMarksforSymbols_92_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCombiningMarksforSymbols_92_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLetterlikeSymbols_93_DefaultValueData = 91;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLetterlikeSymbols_93_DefaultValue = 
{
	&Category_t2075____UnicodeLetterlikeSymbols_93_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLetterlikeSymbols_93_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeNumberForms_94_DefaultValueData = 92;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeNumberForms_94_DefaultValue = 
{
	&Category_t2075____UnicodeNumberForms_94_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeNumberForms_94_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeArrows_95_DefaultValueData = 93;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeArrows_95_DefaultValue = 
{
	&Category_t2075____UnicodeArrows_95_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeArrows_95_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMathematicalOperators_96_DefaultValueData = 94;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMathematicalOperators_96_DefaultValue = 
{
	&Category_t2075____UnicodeMathematicalOperators_96_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMathematicalOperators_96_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMiscellaneousTechnical_97_DefaultValueData = 95;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMiscellaneousTechnical_97_DefaultValue = 
{
	&Category_t2075____UnicodeMiscellaneousTechnical_97_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMiscellaneousTechnical_97_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeControlPictures_98_DefaultValueData = 96;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeControlPictures_98_DefaultValue = 
{
	&Category_t2075____UnicodeControlPictures_98_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeControlPictures_98_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeOpticalCharacterRecognition_99_DefaultValueData = 97;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeOpticalCharacterRecognition_99_DefaultValue = 
{
	&Category_t2075____UnicodeOpticalCharacterRecognition_99_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeOpticalCharacterRecognition_99_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeEnclosedAlphanumerics_100_DefaultValueData = 98;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeEnclosedAlphanumerics_100_DefaultValue = 
{
	&Category_t2075____UnicodeEnclosedAlphanumerics_100_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeEnclosedAlphanumerics_100_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeBoxDrawing_101_DefaultValueData = 99;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeBoxDrawing_101_DefaultValue = 
{
	&Category_t2075____UnicodeBoxDrawing_101_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeBoxDrawing_101_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeBlockElements_102_DefaultValueData = 100;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeBlockElements_102_DefaultValue = 
{
	&Category_t2075____UnicodeBlockElements_102_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeBlockElements_102_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGeometricShapes_103_DefaultValueData = 101;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGeometricShapes_103_DefaultValue = 
{
	&Category_t2075____UnicodeGeometricShapes_103_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGeometricShapes_103_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMiscellaneousSymbols_104_DefaultValueData = 102;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMiscellaneousSymbols_104_DefaultValue = 
{
	&Category_t2075____UnicodeMiscellaneousSymbols_104_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMiscellaneousSymbols_104_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeDingbats_105_DefaultValueData = 103;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeDingbats_105_DefaultValue = 
{
	&Category_t2075____UnicodeDingbats_105_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeDingbats_105_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeBraillePatterns_106_DefaultValueData = 104;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeBraillePatterns_106_DefaultValue = 
{
	&Category_t2075____UnicodeBraillePatterns_106_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeBraillePatterns_106_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKRadicalsSupplement_107_DefaultValueData = 105;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKRadicalsSupplement_107_DefaultValue = 
{
	&Category_t2075____UnicodeCJKRadicalsSupplement_107_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKRadicalsSupplement_107_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeKangxiRadicals_108_DefaultValueData = 106;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeKangxiRadicals_108_DefaultValue = 
{
	&Category_t2075____UnicodeKangxiRadicals_108_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeKangxiRadicals_108_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeIdeographicDescriptionCharacters_109_DefaultValueData = 107;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeIdeographicDescriptionCharacters_109_DefaultValue = 
{
	&Category_t2075____UnicodeIdeographicDescriptionCharacters_109_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeIdeographicDescriptionCharacters_109_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKSymbolsandPunctuation_110_DefaultValueData = 108;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKSymbolsandPunctuation_110_DefaultValue = 
{
	&Category_t2075____UnicodeCJKSymbolsandPunctuation_110_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKSymbolsandPunctuation_110_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHiragana_111_DefaultValueData = 109;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHiragana_111_DefaultValue = 
{
	&Category_t2075____UnicodeHiragana_111_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHiragana_111_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeKatakana_112_DefaultValueData = 110;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeKatakana_112_DefaultValue = 
{
	&Category_t2075____UnicodeKatakana_112_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeKatakana_112_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeBopomofo_113_DefaultValueData = 111;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeBopomofo_113_DefaultValue = 
{
	&Category_t2075____UnicodeBopomofo_113_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeBopomofo_113_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHangulCompatibilityJamo_114_DefaultValueData = 112;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHangulCompatibilityJamo_114_DefaultValue = 
{
	&Category_t2075____UnicodeHangulCompatibilityJamo_114_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHangulCompatibilityJamo_114_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeKanbun_115_DefaultValueData = 113;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeKanbun_115_DefaultValue = 
{
	&Category_t2075____UnicodeKanbun_115_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeKanbun_115_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeBopomofoExtended_116_DefaultValueData = 114;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeBopomofoExtended_116_DefaultValue = 
{
	&Category_t2075____UnicodeBopomofoExtended_116_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeBopomofoExtended_116_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeEnclosedCJKLettersandMonths_117_DefaultValueData = 115;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeEnclosedCJKLettersandMonths_117_DefaultValue = 
{
	&Category_t2075____UnicodeEnclosedCJKLettersandMonths_117_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeEnclosedCJKLettersandMonths_117_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKCompatibility_118_DefaultValueData = 116;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKCompatibility_118_DefaultValue = 
{
	&Category_t2075____UnicodeCJKCompatibility_118_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKCompatibility_118_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKUnifiedIdeographsExtensionA_119_DefaultValueData = 117;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKUnifiedIdeographsExtensionA_119_DefaultValue = 
{
	&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionA_119_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionA_119_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKUnifiedIdeographs_120_DefaultValueData = 118;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKUnifiedIdeographs_120_DefaultValue = 
{
	&Category_t2075____UnicodeCJKUnifiedIdeographs_120_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKUnifiedIdeographs_120_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeYiSyllables_121_DefaultValueData = 119;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeYiSyllables_121_DefaultValue = 
{
	&Category_t2075____UnicodeYiSyllables_121_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeYiSyllables_121_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeYiRadicals_122_DefaultValueData = 120;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeYiRadicals_122_DefaultValue = 
{
	&Category_t2075____UnicodeYiRadicals_122_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeYiRadicals_122_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHangulSyllables_123_DefaultValueData = 121;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHangulSyllables_123_DefaultValue = 
{
	&Category_t2075____UnicodeHangulSyllables_123_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHangulSyllables_123_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHighSurrogates_124_DefaultValueData = 122;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHighSurrogates_124_DefaultValue = 
{
	&Category_t2075____UnicodeHighSurrogates_124_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHighSurrogates_124_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHighPrivateUseSurrogates_125_DefaultValueData = 123;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHighPrivateUseSurrogates_125_DefaultValue = 
{
	&Category_t2075____UnicodeHighPrivateUseSurrogates_125_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHighPrivateUseSurrogates_125_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeLowSurrogates_126_DefaultValueData = 124;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeLowSurrogates_126_DefaultValue = 
{
	&Category_t2075____UnicodeLowSurrogates_126_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeLowSurrogates_126_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodePrivateUse_127_DefaultValueData = 125;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodePrivateUse_127_DefaultValue = 
{
	&Category_t2075____UnicodePrivateUse_127_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodePrivateUse_127_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKCompatibilityIdeographs_128_DefaultValueData = 126;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKCompatibilityIdeographs_128_DefaultValue = 
{
	&Category_t2075____UnicodeCJKCompatibilityIdeographs_128_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKCompatibilityIdeographs_128_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeAlphabeticPresentationForms_129_DefaultValueData = 127;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeAlphabeticPresentationForms_129_DefaultValue = 
{
	&Category_t2075____UnicodeAlphabeticPresentationForms_129_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeAlphabeticPresentationForms_129_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeArabicPresentationFormsA_130_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeArabicPresentationFormsA_130_DefaultValue = 
{
	&Category_t2075____UnicodeArabicPresentationFormsA_130_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeArabicPresentationFormsA_130_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCombiningHalfMarks_131_DefaultValueData = 129;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCombiningHalfMarks_131_DefaultValue = 
{
	&Category_t2075____UnicodeCombiningHalfMarks_131_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCombiningHalfMarks_131_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKCompatibilityForms_132_DefaultValueData = 130;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKCompatibilityForms_132_DefaultValue = 
{
	&Category_t2075____UnicodeCJKCompatibilityForms_132_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKCompatibilityForms_132_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSmallFormVariants_133_DefaultValueData = 131;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSmallFormVariants_133_DefaultValue = 
{
	&Category_t2075____UnicodeSmallFormVariants_133_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSmallFormVariants_133_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeArabicPresentationFormsB_134_DefaultValueData = 132;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeArabicPresentationFormsB_134_DefaultValue = 
{
	&Category_t2075____UnicodeArabicPresentationFormsB_134_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeArabicPresentationFormsB_134_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeSpecials_135_DefaultValueData = 133;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeSpecials_135_DefaultValue = 
{
	&Category_t2075____UnicodeSpecials_135_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeSpecials_135_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeHalfwidthandFullwidthForms_136_DefaultValueData = 134;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeHalfwidthandFullwidthForms_136_DefaultValue = 
{
	&Category_t2075____UnicodeHalfwidthandFullwidthForms_136_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeHalfwidthandFullwidthForms_136_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeOldItalic_137_DefaultValueData = 135;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeOldItalic_137_DefaultValue = 
{
	&Category_t2075____UnicodeOldItalic_137_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeOldItalic_137_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeGothic_138_DefaultValueData = 136;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeGothic_138_DefaultValue = 
{
	&Category_t2075____UnicodeGothic_138_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeGothic_138_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeDeseret_139_DefaultValueData = 137;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeDeseret_139_DefaultValue = 
{
	&Category_t2075____UnicodeDeseret_139_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeDeseret_139_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeByzantineMusicalSymbols_140_DefaultValueData = 138;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeByzantineMusicalSymbols_140_DefaultValue = 
{
	&Category_t2075____UnicodeByzantineMusicalSymbols_140_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeByzantineMusicalSymbols_140_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMusicalSymbols_141_DefaultValueData = 139;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMusicalSymbols_141_DefaultValue = 
{
	&Category_t2075____UnicodeMusicalSymbols_141_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMusicalSymbols_141_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeMathematicalAlphanumericSymbols_142_DefaultValueData = 140;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeMathematicalAlphanumericSymbols_142_DefaultValue = 
{
	&Category_t2075____UnicodeMathematicalAlphanumericSymbols_142_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeMathematicalAlphanumericSymbols_142_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKUnifiedIdeographsExtensionB_143_DefaultValueData = 141;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKUnifiedIdeographsExtensionB_143_DefaultValue = 
{
	&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionB_143_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionB_143_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeCJKCompatibilityIdeographsSupplement_144_DefaultValueData = 142;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeCJKCompatibilityIdeographsSupplement_144_DefaultValue = 
{
	&Category_t2075____UnicodeCJKCompatibilityIdeographsSupplement_144_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeCJKCompatibilityIdeographsSupplement_144_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____UnicodeTags_145_DefaultValueData = 143;
static Il2CppFieldDefaultValueEntry Category_t2075____UnicodeTags_145_DefaultValue = 
{
	&Category_t2075____UnicodeTags_145_FieldInfo/* field */
	, { (char*)&Category_t2075____UnicodeTags_145_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static const uint16_t Category_t2075____LastValue_146_DefaultValueData = 144;
static Il2CppFieldDefaultValueEntry Category_t2075____LastValue_146_DefaultValue = 
{
	&Category_t2075____LastValue_146_FieldInfo/* field */
	, { (char*)&Category_t2075____LastValue_146_DefaultValueData, &UInt16_t194_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Category_t2075_FieldDefaultValues[] = 
{
	&Category_t2075____None_2_DefaultValue,
	&Category_t2075____Any_3_DefaultValue,
	&Category_t2075____AnySingleline_4_DefaultValue,
	&Category_t2075____Word_5_DefaultValue,
	&Category_t2075____Digit_6_DefaultValue,
	&Category_t2075____WhiteSpace_7_DefaultValue,
	&Category_t2075____EcmaAny_8_DefaultValue,
	&Category_t2075____EcmaAnySingleline_9_DefaultValue,
	&Category_t2075____EcmaWord_10_DefaultValue,
	&Category_t2075____EcmaDigit_11_DefaultValue,
	&Category_t2075____EcmaWhiteSpace_12_DefaultValue,
	&Category_t2075____UnicodeL_13_DefaultValue,
	&Category_t2075____UnicodeM_14_DefaultValue,
	&Category_t2075____UnicodeN_15_DefaultValue,
	&Category_t2075____UnicodeZ_16_DefaultValue,
	&Category_t2075____UnicodeP_17_DefaultValue,
	&Category_t2075____UnicodeS_18_DefaultValue,
	&Category_t2075____UnicodeC_19_DefaultValue,
	&Category_t2075____UnicodeLu_20_DefaultValue,
	&Category_t2075____UnicodeLl_21_DefaultValue,
	&Category_t2075____UnicodeLt_22_DefaultValue,
	&Category_t2075____UnicodeLm_23_DefaultValue,
	&Category_t2075____UnicodeLo_24_DefaultValue,
	&Category_t2075____UnicodeMn_25_DefaultValue,
	&Category_t2075____UnicodeMe_26_DefaultValue,
	&Category_t2075____UnicodeMc_27_DefaultValue,
	&Category_t2075____UnicodeNd_28_DefaultValue,
	&Category_t2075____UnicodeNl_29_DefaultValue,
	&Category_t2075____UnicodeNo_30_DefaultValue,
	&Category_t2075____UnicodeZs_31_DefaultValue,
	&Category_t2075____UnicodeZl_32_DefaultValue,
	&Category_t2075____UnicodeZp_33_DefaultValue,
	&Category_t2075____UnicodePd_34_DefaultValue,
	&Category_t2075____UnicodePs_35_DefaultValue,
	&Category_t2075____UnicodePi_36_DefaultValue,
	&Category_t2075____UnicodePe_37_DefaultValue,
	&Category_t2075____UnicodePf_38_DefaultValue,
	&Category_t2075____UnicodePc_39_DefaultValue,
	&Category_t2075____UnicodePo_40_DefaultValue,
	&Category_t2075____UnicodeSm_41_DefaultValue,
	&Category_t2075____UnicodeSc_42_DefaultValue,
	&Category_t2075____UnicodeSk_43_DefaultValue,
	&Category_t2075____UnicodeSo_44_DefaultValue,
	&Category_t2075____UnicodeCc_45_DefaultValue,
	&Category_t2075____UnicodeCf_46_DefaultValue,
	&Category_t2075____UnicodeCo_47_DefaultValue,
	&Category_t2075____UnicodeCs_48_DefaultValue,
	&Category_t2075____UnicodeCn_49_DefaultValue,
	&Category_t2075____UnicodeBasicLatin_50_DefaultValue,
	&Category_t2075____UnicodeLatin1Supplement_51_DefaultValue,
	&Category_t2075____UnicodeLatinExtendedA_52_DefaultValue,
	&Category_t2075____UnicodeLatinExtendedB_53_DefaultValue,
	&Category_t2075____UnicodeIPAExtensions_54_DefaultValue,
	&Category_t2075____UnicodeSpacingModifierLetters_55_DefaultValue,
	&Category_t2075____UnicodeCombiningDiacriticalMarks_56_DefaultValue,
	&Category_t2075____UnicodeGreek_57_DefaultValue,
	&Category_t2075____UnicodeCyrillic_58_DefaultValue,
	&Category_t2075____UnicodeArmenian_59_DefaultValue,
	&Category_t2075____UnicodeHebrew_60_DefaultValue,
	&Category_t2075____UnicodeArabic_61_DefaultValue,
	&Category_t2075____UnicodeSyriac_62_DefaultValue,
	&Category_t2075____UnicodeThaana_63_DefaultValue,
	&Category_t2075____UnicodeDevanagari_64_DefaultValue,
	&Category_t2075____UnicodeBengali_65_DefaultValue,
	&Category_t2075____UnicodeGurmukhi_66_DefaultValue,
	&Category_t2075____UnicodeGujarati_67_DefaultValue,
	&Category_t2075____UnicodeOriya_68_DefaultValue,
	&Category_t2075____UnicodeTamil_69_DefaultValue,
	&Category_t2075____UnicodeTelugu_70_DefaultValue,
	&Category_t2075____UnicodeKannada_71_DefaultValue,
	&Category_t2075____UnicodeMalayalam_72_DefaultValue,
	&Category_t2075____UnicodeSinhala_73_DefaultValue,
	&Category_t2075____UnicodeThai_74_DefaultValue,
	&Category_t2075____UnicodeLao_75_DefaultValue,
	&Category_t2075____UnicodeTibetan_76_DefaultValue,
	&Category_t2075____UnicodeMyanmar_77_DefaultValue,
	&Category_t2075____UnicodeGeorgian_78_DefaultValue,
	&Category_t2075____UnicodeHangulJamo_79_DefaultValue,
	&Category_t2075____UnicodeEthiopic_80_DefaultValue,
	&Category_t2075____UnicodeCherokee_81_DefaultValue,
	&Category_t2075____UnicodeUnifiedCanadianAboriginalSyllabics_82_DefaultValue,
	&Category_t2075____UnicodeOgham_83_DefaultValue,
	&Category_t2075____UnicodeRunic_84_DefaultValue,
	&Category_t2075____UnicodeKhmer_85_DefaultValue,
	&Category_t2075____UnicodeMongolian_86_DefaultValue,
	&Category_t2075____UnicodeLatinExtendedAdditional_87_DefaultValue,
	&Category_t2075____UnicodeGreekExtended_88_DefaultValue,
	&Category_t2075____UnicodeGeneralPunctuation_89_DefaultValue,
	&Category_t2075____UnicodeSuperscriptsandSubscripts_90_DefaultValue,
	&Category_t2075____UnicodeCurrencySymbols_91_DefaultValue,
	&Category_t2075____UnicodeCombiningMarksforSymbols_92_DefaultValue,
	&Category_t2075____UnicodeLetterlikeSymbols_93_DefaultValue,
	&Category_t2075____UnicodeNumberForms_94_DefaultValue,
	&Category_t2075____UnicodeArrows_95_DefaultValue,
	&Category_t2075____UnicodeMathematicalOperators_96_DefaultValue,
	&Category_t2075____UnicodeMiscellaneousTechnical_97_DefaultValue,
	&Category_t2075____UnicodeControlPictures_98_DefaultValue,
	&Category_t2075____UnicodeOpticalCharacterRecognition_99_DefaultValue,
	&Category_t2075____UnicodeEnclosedAlphanumerics_100_DefaultValue,
	&Category_t2075____UnicodeBoxDrawing_101_DefaultValue,
	&Category_t2075____UnicodeBlockElements_102_DefaultValue,
	&Category_t2075____UnicodeGeometricShapes_103_DefaultValue,
	&Category_t2075____UnicodeMiscellaneousSymbols_104_DefaultValue,
	&Category_t2075____UnicodeDingbats_105_DefaultValue,
	&Category_t2075____UnicodeBraillePatterns_106_DefaultValue,
	&Category_t2075____UnicodeCJKRadicalsSupplement_107_DefaultValue,
	&Category_t2075____UnicodeKangxiRadicals_108_DefaultValue,
	&Category_t2075____UnicodeIdeographicDescriptionCharacters_109_DefaultValue,
	&Category_t2075____UnicodeCJKSymbolsandPunctuation_110_DefaultValue,
	&Category_t2075____UnicodeHiragana_111_DefaultValue,
	&Category_t2075____UnicodeKatakana_112_DefaultValue,
	&Category_t2075____UnicodeBopomofo_113_DefaultValue,
	&Category_t2075____UnicodeHangulCompatibilityJamo_114_DefaultValue,
	&Category_t2075____UnicodeKanbun_115_DefaultValue,
	&Category_t2075____UnicodeBopomofoExtended_116_DefaultValue,
	&Category_t2075____UnicodeEnclosedCJKLettersandMonths_117_DefaultValue,
	&Category_t2075____UnicodeCJKCompatibility_118_DefaultValue,
	&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionA_119_DefaultValue,
	&Category_t2075____UnicodeCJKUnifiedIdeographs_120_DefaultValue,
	&Category_t2075____UnicodeYiSyllables_121_DefaultValue,
	&Category_t2075____UnicodeYiRadicals_122_DefaultValue,
	&Category_t2075____UnicodeHangulSyllables_123_DefaultValue,
	&Category_t2075____UnicodeHighSurrogates_124_DefaultValue,
	&Category_t2075____UnicodeHighPrivateUseSurrogates_125_DefaultValue,
	&Category_t2075____UnicodeLowSurrogates_126_DefaultValue,
	&Category_t2075____UnicodePrivateUse_127_DefaultValue,
	&Category_t2075____UnicodeCJKCompatibilityIdeographs_128_DefaultValue,
	&Category_t2075____UnicodeAlphabeticPresentationForms_129_DefaultValue,
	&Category_t2075____UnicodeArabicPresentationFormsA_130_DefaultValue,
	&Category_t2075____UnicodeCombiningHalfMarks_131_DefaultValue,
	&Category_t2075____UnicodeCJKCompatibilityForms_132_DefaultValue,
	&Category_t2075____UnicodeSmallFormVariants_133_DefaultValue,
	&Category_t2075____UnicodeArabicPresentationFormsB_134_DefaultValue,
	&Category_t2075____UnicodeSpecials_135_DefaultValue,
	&Category_t2075____UnicodeHalfwidthandFullwidthForms_136_DefaultValue,
	&Category_t2075____UnicodeOldItalic_137_DefaultValue,
	&Category_t2075____UnicodeGothic_138_DefaultValue,
	&Category_t2075____UnicodeDeseret_139_DefaultValue,
	&Category_t2075____UnicodeByzantineMusicalSymbols_140_DefaultValue,
	&Category_t2075____UnicodeMusicalSymbols_141_DefaultValue,
	&Category_t2075____UnicodeMathematicalAlphanumericSymbols_142_DefaultValue,
	&Category_t2075____UnicodeCJKUnifiedIdeographsExtensionB_143_DefaultValue,
	&Category_t2075____UnicodeCJKCompatibilityIdeographsSupplement_144_DefaultValue,
	&Category_t2075____UnicodeTags_145_DefaultValue,
	&Category_t2075____LastValue_146_DefaultValue,
	NULL
};
extern MethodInfo Enum_Equals_m1279_MethodInfo;
extern MethodInfo Enum_GetHashCode_m1280_MethodInfo;
extern MethodInfo Enum_ToString_m1281_MethodInfo;
extern MethodInfo Enum_ToString_m1282_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m1283_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m1284_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m1285_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m1286_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m1287_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m1288_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m1289_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m1290_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m1291_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m1292_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m1293_MethodInfo;
extern MethodInfo Enum_ToString_m1294_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m1295_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m1296_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m1297_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m1298_MethodInfo;
extern MethodInfo Enum_CompareTo_m1299_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m1300_MethodInfo;
static Il2CppMethodReference Category_t2075_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Category_t2075_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormattable_t312_0_0_0;
extern Il2CppType IConvertible_t313_0_0_0;
extern Il2CppType IComparable_t314_0_0_0;
static Il2CppInterfaceOffsetPair Category_t2075_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Category_t2075_1_0_0;
extern Il2CppType Enum_t218_0_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t194_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Category_t2075_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Category_t2075_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Category_t2075_VTable/* vtableMethods */
	, Category_t2075_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Category_t2075_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Category"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Category_t2075_MethodInfos/* methods */
	, NULL/* properties */
	, Category_t2075_FieldInfos/* fields */
	, NULL/* events */
	, &UInt16_t194_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Category_t2075_0_0_0/* byval_arg */
	, &Category_t2075_1_0_0/* this_arg */
	, &Category_t2075_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Category_t2075_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Category_t2075)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Category_t2075)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 146/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtils.h"
// Metadata Definition System.Text.RegularExpressions.CategoryUtils
extern TypeInfo CategoryUtils_t2076_il2cpp_TypeInfo;
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtilsMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo CategoryUtils_t2076_CategoryUtils_CategoryFromName_m8383_ParameterInfos[] = 
{
	{"name", 0, 134218178, 0, &String_t_0_0_0},
};
extern Il2CppType Category_t2075_0_0_0;
extern void* RuntimeInvoker_Category_t2075_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.CategoryUtils::CategoryFromName(System.String)
MethodInfo CategoryUtils_CategoryFromName_m8383_MethodInfo = 
{
	"CategoryFromName"/* name */
	, (methodPointerType)&CategoryUtils_CategoryFromName_m8383/* method */
	, &CategoryUtils_t2076_il2cpp_TypeInfo/* declaring_type */
	, &Category_t2075_0_0_0/* return_type */
	, RuntimeInvoker_Category_t2075_Object_t/* invoker_method */
	, CategoryUtils_t2076_CategoryUtils_CategoryFromName_m8383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo CategoryUtils_t2076_CategoryUtils_IsCategory_m8384_ParameterInfos[] = 
{
	{"cat", 0, 134218179, 0, &Category_t2075_0_0_0},
	{"c", 1, 134218180, 0, &Char_t193_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_UInt16_t194_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Text.RegularExpressions.Category,System.Char)
MethodInfo CategoryUtils_IsCategory_m8384_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m8384/* method */
	, &CategoryUtils_t2076_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_UInt16_t194_Int16_t238/* invoker_method */
	, CategoryUtils_t2076_CategoryUtils_IsCategory_m8384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnicodeCategory_t2179_0_0_0;
extern Il2CppType UnicodeCategory_t2179_0_0_0;
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo CategoryUtils_t2076_CategoryUtils_IsCategory_m8385_ParameterInfos[] = 
{
	{"uc", 0, 134218181, 0, &UnicodeCategory_t2179_0_0_0},
	{"c", 1, 134218182, 0, &Char_t193_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Globalization.UnicodeCategory,System.Char)
MethodInfo CategoryUtils_IsCategory_m8385_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m8385/* method */
	, &CategoryUtils_t2076_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189_Int16_t238/* invoker_method */
	, CategoryUtils_t2076_CategoryUtils_IsCategory_m8385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CategoryUtils_t2076_MethodInfos[] =
{
	&CategoryUtils_CategoryFromName_m8383_MethodInfo,
	&CategoryUtils_IsCategory_m8384_MethodInfo,
	&CategoryUtils_IsCategory_m8385_MethodInfo,
	NULL
};
static Il2CppMethodReference CategoryUtils_t2076_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool CategoryUtils_t2076_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType CategoryUtils_t2076_0_0_0;
extern Il2CppType CategoryUtils_t2076_1_0_0;
struct CategoryUtils_t2076;
const Il2CppTypeDefinitionMetadata CategoryUtils_t2076_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CategoryUtils_t2076_VTable/* vtableMethods */
	, CategoryUtils_t2076_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CategoryUtils_t2076_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CategoryUtils"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, CategoryUtils_t2076_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CategoryUtils_t2076_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CategoryUtils_t2076_0_0_0/* byval_arg */
	, &CategoryUtils_t2076_1_0_0/* this_arg */
	, &CategoryUtils_t2076_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CategoryUtils_t2076)/* instance_size */
	, sizeof (CategoryUtils_t2076)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// Metadata Definition System.Text.RegularExpressions.LinkRef
extern TypeInfo LinkRef_t2077_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRefMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
MethodInfo LinkRef__ctor_m8386_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkRef__ctor_m8386/* method */
	, &LinkRef_t2077_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LinkRef_t2077_MethodInfos[] =
{
	&LinkRef__ctor_m8386_MethodInfo,
	NULL
};
static Il2CppMethodReference LinkRef_t2077_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool LinkRef_t2077_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType LinkRef_t2077_0_0_0;
extern Il2CppType LinkRef_t2077_1_0_0;
struct LinkRef_t2077;
const Il2CppTypeDefinitionMetadata LinkRef_t2077_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LinkRef_t2077_VTable/* vtableMethods */
	, LinkRef_t2077_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LinkRef_t2077_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkRef"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkRef_t2077_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &LinkRef_t2077_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkRef_t2077_0_0_0/* byval_arg */
	, &LinkRef_t2077_1_0_0/* this_arg */
	, &LinkRef_t2077_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkRef_t2077)/* instance_size */
	, sizeof (LinkRef_t2077)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.ICompiler
extern TypeInfo ICompiler_t2137_il2cpp_TypeInfo;
extern Il2CppType IMachineFactory_t2067_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.ICompiler::GetMachineFactory()
MethodInfo ICompiler_GetMachineFactory_m8911_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t2067_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFalse()
MethodInfo ICompiler_EmitFalse_m8912_MethodInfo = 
{
	"EmitFalse"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTrue()
MethodInfo ICompiler_EmitTrue_m8913_MethodInfo = 
{
	"EmitTrue"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitCharacter_m8914_ParameterInfos[] = 
{
	{"c", 0, 134218183, 0, &Char_t193_0_0_0},
	{"negate", 1, 134218184, 0, &Boolean_t203_0_0_0},
	{"ignore", 2, 134218185, 0, &Boolean_t203_0_0_0},
	{"reverse", 3, 134218186, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
MethodInfo ICompiler_EmitCharacter_m8914_MethodInfo = 
{
	"EmitCharacter"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitCharacter_m8914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitCategory_m8915_ParameterInfos[] = 
{
	{"cat", 0, 134218187, 0, &Category_t2075_0_0_0},
	{"negate", 1, 134218188, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218189, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
MethodInfo ICompiler_EmitCategory_m8915_MethodInfo = 
{
	"EmitCategory"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitCategory_m8915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitNotCategory_m8916_ParameterInfos[] = 
{
	{"cat", 0, 134218190, 0, &Category_t2075_0_0_0},
	{"negate", 1, 134218191, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218192, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
MethodInfo ICompiler_EmitNotCategory_m8916_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitNotCategory_m8916_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitRange_m8917_ParameterInfos[] = 
{
	{"lo", 0, 134218193, 0, &Char_t193_0_0_0},
	{"hi", 1, 134218194, 0, &Char_t193_0_0_0},
	{"negate", 2, 134218195, 0, &Boolean_t203_0_0_0},
	{"ignore", 3, 134218196, 0, &Boolean_t203_0_0_0},
	{"reverse", 4, 134218197, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238_Int16_t238_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
MethodInfo ICompiler_EmitRange_m8917_MethodInfo = 
{
	"EmitRange"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238_Int16_t238_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitRange_m8917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType BitArray_t2112_0_0_0;
extern Il2CppType BitArray_t2112_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitSet_m8918_ParameterInfos[] = 
{
	{"lo", 0, 134218198, 0, &Char_t193_0_0_0},
	{"set", 1, 134218199, 0, &BitArray_t2112_0_0_0},
	{"negate", 2, 134218200, 0, &Boolean_t203_0_0_0},
	{"ignore", 3, 134218201, 0, &Boolean_t203_0_0_0},
	{"reverse", 4, 134218202, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238_Object_t_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
MethodInfo ICompiler_EmitSet_m8918_MethodInfo = 
{
	"EmitSet"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238_Object_t_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitSet_m8918_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitString_m8919_ParameterInfos[] = 
{
	{"str", 0, 134218203, 0, &String_t_0_0_0},
	{"ignore", 1, 134218204, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218205, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitString(System.String,System.Boolean,System.Boolean)
MethodInfo ICompiler_EmitString_m8919_MethodInfo = 
{
	"EmitString"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitString_m8919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Position_t2071_0_0_0;
extern Il2CppType Position_t2071_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitPosition_m8920_ParameterInfos[] = 
{
	{"pos", 0, 134218206, 0, &Position_t2071_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitPosition(System.Text.RegularExpressions.Position)
MethodInfo ICompiler_EmitPosition_m8920_MethodInfo = 
{
	"EmitPosition"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitPosition_m8920_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitOpen_m8921_ParameterInfos[] = 
{
	{"gid", 0, 134218207, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitOpen(System.Int32)
MethodInfo ICompiler_EmitOpen_m8921_MethodInfo = 
{
	"EmitOpen"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitOpen_m8921_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitClose_m8922_ParameterInfos[] = 
{
	{"gid", 0, 134218208, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitClose(System.Int32)
MethodInfo ICompiler_EmitClose_m8922_MethodInfo = 
{
	"EmitClose"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitClose_m8922_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitBalanceStart_m8923_ParameterInfos[] = 
{
	{"gid", 0, 134218209, 0, &Int32_t189_0_0_0},
	{"balance", 1, 134218210, 0, &Int32_t189_0_0_0},
	{"capture", 2, 134218211, 0, &Boolean_t203_0_0_0},
	{"tail", 3, 134218212, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitBalanceStart_m8923_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitBalanceStart_m8923_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalance()
MethodInfo ICompiler_EmitBalance_m8924_MethodInfo = 
{
	"EmitBalance"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitReference_m8925_ParameterInfos[] = 
{
	{"gid", 0, 134218213, 0, &Int32_t189_0_0_0},
	{"ignore", 1, 134218214, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218215, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
MethodInfo ICompiler_EmitReference_m8925_MethodInfo = 
{
	"EmitReference"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_SByte_t236_SByte_t236/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitReference_m8925_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitIfDefined_m8926_ParameterInfos[] = 
{
	{"gid", 0, 134218216, 0, &Int32_t189_0_0_0},
	{"tail", 1, 134218217, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitIfDefined_m8926_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitIfDefined_m8926_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitSub_m8927_ParameterInfos[] = 
{
	{"tail", 0, 134218218, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitSub_m8927_MethodInfo = 
{
	"EmitSub"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitSub_m8927_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitTest_m8928_ParameterInfos[] = 
{
	{"yes", 0, 134218219, 0, &LinkRef_t2077_0_0_0},
	{"tail", 1, 134218220, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitTest_m8928_MethodInfo = 
{
	"EmitTest"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitTest_m8928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitBranch_m8929_ParameterInfos[] = 
{
	{"next", 0, 134218221, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitBranch_m8929_MethodInfo = 
{
	"EmitBranch"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitBranch_m8929_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitJump_m8930_ParameterInfos[] = 
{
	{"target", 0, 134218222, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitJump_m8930_MethodInfo = 
{
	"EmitJump"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitJump_m8930_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitRepeat_m8931_ParameterInfos[] = 
{
	{"min", 0, 134218223, 0, &Int32_t189_0_0_0},
	{"max", 1, 134218224, 0, &Int32_t189_0_0_0},
	{"lazy", 2, 134218225, 0, &Boolean_t203_0_0_0},
	{"until", 3, 134218226, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitRepeat_m8931_MethodInfo = 
{
	"EmitRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitRepeat_m8931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitUntil_m8932_ParameterInfos[] = 
{
	{"repeat", 0, 134218227, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitUntil_m8932_MethodInfo = 
{
	"EmitUntil"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitUntil_m8932_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitIn_m8933_ParameterInfos[] = 
{
	{"tail", 0, 134218228, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitIn_m8933_MethodInfo = 
{
	"EmitIn"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitIn_m8933_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitInfo_m8934_ParameterInfos[] = 
{
	{"count", 0, 134218229, 0, &Int32_t189_0_0_0},
	{"min", 1, 134218230, 0, &Int32_t189_0_0_0},
	{"max", 2, 134218231, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
MethodInfo ICompiler_EmitInfo_m8934_MethodInfo = 
{
	"EmitInfo"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitInfo_m8934_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitFastRepeat_m8935_ParameterInfos[] = 
{
	{"min", 0, 134218232, 0, &Int32_t189_0_0_0},
	{"max", 1, 134218233, 0, &Int32_t189_0_0_0},
	{"lazy", 2, 134218234, 0, &Boolean_t203_0_0_0},
	{"tail", 3, 134218235, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitFastRepeat_m8935_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitFastRepeat_m8935_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_EmitAnchor_m8936_ParameterInfos[] = 
{
	{"reverse", 0, 134218236, 0, &Boolean_t203_0_0_0},
	{"offset", 1, 134218237, 0, &Int32_t189_0_0_0},
	{"tail", 2, 134218238, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_EmitAnchor_m8936_MethodInfo = 
{
	"EmitAnchor"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236_Int32_t189_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_EmitAnchor_m8936_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranchEnd()
MethodInfo ICompiler_EmitBranchEnd_m8937_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAlternationEnd()
MethodInfo ICompiler_EmitAlternationEnd_m8938_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink()
MethodInfo ICompiler_NewLink_m8939_MethodInfo = 
{
	"NewLink"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t2077_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo ICompiler_t2137_ICompiler_ResolveLink_m8940_ParameterInfos[] = 
{
	{"link", 0, 134218239, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
MethodInfo ICompiler_ResolveLink_m8940_MethodInfo = 
{
	"ResolveLink"/* name */
	, NULL/* method */
	, &ICompiler_t2137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ICompiler_t2137_ICompiler_ResolveLink_m8940_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ICompiler_t2137_MethodInfos[] =
{
	&ICompiler_GetMachineFactory_m8911_MethodInfo,
	&ICompiler_EmitFalse_m8912_MethodInfo,
	&ICompiler_EmitTrue_m8913_MethodInfo,
	&ICompiler_EmitCharacter_m8914_MethodInfo,
	&ICompiler_EmitCategory_m8915_MethodInfo,
	&ICompiler_EmitNotCategory_m8916_MethodInfo,
	&ICompiler_EmitRange_m8917_MethodInfo,
	&ICompiler_EmitSet_m8918_MethodInfo,
	&ICompiler_EmitString_m8919_MethodInfo,
	&ICompiler_EmitPosition_m8920_MethodInfo,
	&ICompiler_EmitOpen_m8921_MethodInfo,
	&ICompiler_EmitClose_m8922_MethodInfo,
	&ICompiler_EmitBalanceStart_m8923_MethodInfo,
	&ICompiler_EmitBalance_m8924_MethodInfo,
	&ICompiler_EmitReference_m8925_MethodInfo,
	&ICompiler_EmitIfDefined_m8926_MethodInfo,
	&ICompiler_EmitSub_m8927_MethodInfo,
	&ICompiler_EmitTest_m8928_MethodInfo,
	&ICompiler_EmitBranch_m8929_MethodInfo,
	&ICompiler_EmitJump_m8930_MethodInfo,
	&ICompiler_EmitRepeat_m8931_MethodInfo,
	&ICompiler_EmitUntil_m8932_MethodInfo,
	&ICompiler_EmitIn_m8933_MethodInfo,
	&ICompiler_EmitInfo_m8934_MethodInfo,
	&ICompiler_EmitFastRepeat_m8935_MethodInfo,
	&ICompiler_EmitAnchor_m8936_MethodInfo,
	&ICompiler_EmitBranchEnd_m8937_MethodInfo,
	&ICompiler_EmitAlternationEnd_m8938_MethodInfo,
	&ICompiler_NewLink_m8939_MethodInfo,
	&ICompiler_ResolveLink_m8940_MethodInfo,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType ICompiler_t2137_1_0_0;
struct ICompiler_t2137;
const Il2CppTypeDefinitionMetadata ICompiler_t2137_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ICompiler_t2137_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, ICompiler_t2137_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ICompiler_t2137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICompiler_t2137_0_0_0/* byval_arg */
	, &ICompiler_t2137_1_0_0/* this_arg */
	, &ICompiler_t2137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactory.h"
// Metadata Definition System.Text.RegularExpressions.InterpreterFactory
extern TypeInfo InterpreterFactory_t2078_il2cpp_TypeInfo;
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactoryMethodDeclarations.h"
extern Il2CppType UInt16U5BU5D_t195_0_0_0;
extern Il2CppType UInt16U5BU5D_t195_0_0_0;
static ParameterInfo InterpreterFactory_t2078_InterpreterFactory__ctor_m8387_ParameterInfos[] = 
{
	{"pattern", 0, 134218240, 0, &UInt16U5BU5D_t195_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::.ctor(System.UInt16[])
MethodInfo InterpreterFactory__ctor_m8387_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InterpreterFactory__ctor_m8387/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InterpreterFactory_t2078_InterpreterFactory__ctor_m8387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMachine_t2062_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.InterpreterFactory::NewInstance()
MethodInfo InterpreterFactory_NewInstance_m8388_MethodInfo = 
{
	"NewInstance"/* name */
	, (methodPointerType)&InterpreterFactory_NewInstance_m8388/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &IMachine_t2062_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_GroupCount()
MethodInfo InterpreterFactory_get_GroupCount_m8389_MethodInfo = 
{
	"get_GroupCount"/* name */
	, (methodPointerType)&InterpreterFactory_get_GroupCount_m8389/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_Gap()
MethodInfo InterpreterFactory_get_Gap_m8390_MethodInfo = 
{
	"get_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_get_Gap_m8390/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo InterpreterFactory_t2078_InterpreterFactory_set_Gap_m8391_ParameterInfos[] = 
{
	{"value", 0, 134218241, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Gap(System.Int32)
MethodInfo InterpreterFactory_set_Gap_m8391_MethodInfo = 
{
	"set_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_set_Gap_m8391/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, InterpreterFactory_t2078_InterpreterFactory_set_Gap_m8391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Text.RegularExpressions.InterpreterFactory::get_Mapping()
MethodInfo InterpreterFactory_get_Mapping_m8392_MethodInfo = 
{
	"get_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_Mapping_m8392/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern Il2CppType IDictionary_t182_0_0_0;
static ParameterInfo InterpreterFactory_t2078_InterpreterFactory_set_Mapping_m8393_ParameterInfos[] = 
{
	{"value", 0, 134218242, 0, &IDictionary_t182_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Mapping(System.Collections.IDictionary)
MethodInfo InterpreterFactory_set_Mapping_m8393_MethodInfo = 
{
	"set_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_Mapping_m8393/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InterpreterFactory_t2078_InterpreterFactory_set_Mapping_m8393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StringU5BU5D_t169_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String[] System.Text.RegularExpressions.InterpreterFactory::get_NamesMapping()
MethodInfo InterpreterFactory_get_NamesMapping_m8394_MethodInfo = 
{
	"get_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_NamesMapping_m8394/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StringU5BU5D_t169_0_0_0;
extern Il2CppType StringU5BU5D_t169_0_0_0;
static ParameterInfo InterpreterFactory_t2078_InterpreterFactory_set_NamesMapping_m8395_ParameterInfos[] = 
{
	{"value", 0, 134218243, 0, &StringU5BU5D_t169_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_NamesMapping(System.String[])
MethodInfo InterpreterFactory_set_NamesMapping_m8395_MethodInfo = 
{
	"set_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_NamesMapping_m8395/* method */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InterpreterFactory_t2078_InterpreterFactory_set_NamesMapping_m8395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InterpreterFactory_t2078_MethodInfos[] =
{
	&InterpreterFactory__ctor_m8387_MethodInfo,
	&InterpreterFactory_NewInstance_m8388_MethodInfo,
	&InterpreterFactory_get_GroupCount_m8389_MethodInfo,
	&InterpreterFactory_get_Gap_m8390_MethodInfo,
	&InterpreterFactory_set_Gap_m8391_MethodInfo,
	&InterpreterFactory_get_Mapping_m8392_MethodInfo,
	&InterpreterFactory_set_Mapping_m8393_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m8394_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m8395_MethodInfo,
	NULL
};
extern Il2CppType IDictionary_t182_0_0_1;
FieldInfo InterpreterFactory_t2078____mapping_0_FieldInfo = 
{
	"mapping"/* name */
	, &IDictionary_t182_0_0_1/* type */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, offsetof(InterpreterFactory_t2078, ___mapping_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UInt16U5BU5D_t195_0_0_1;
FieldInfo InterpreterFactory_t2078____pattern_1_FieldInfo = 
{
	"pattern"/* name */
	, &UInt16U5BU5D_t195_0_0_1/* type */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, offsetof(InterpreterFactory_t2078, ___pattern_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_1;
FieldInfo InterpreterFactory_t2078____namesMapping_2_FieldInfo = 
{
	"namesMapping"/* name */
	, &StringU5BU5D_t169_0_0_1/* type */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, offsetof(InterpreterFactory_t2078, ___namesMapping_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo InterpreterFactory_t2078____gap_3_FieldInfo = 
{
	"gap"/* name */
	, &Int32_t189_0_0_1/* type */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, offsetof(InterpreterFactory_t2078, ___gap_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* InterpreterFactory_t2078_FieldInfos[] =
{
	&InterpreterFactory_t2078____mapping_0_FieldInfo,
	&InterpreterFactory_t2078____pattern_1_FieldInfo,
	&InterpreterFactory_t2078____namesMapping_2_FieldInfo,
	&InterpreterFactory_t2078____gap_3_FieldInfo,
	NULL
};
extern MethodInfo InterpreterFactory_get_GroupCount_m8389_MethodInfo;
static PropertyInfo InterpreterFactory_t2078____GroupCount_PropertyInfo = 
{
	&InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, &InterpreterFactory_get_GroupCount_m8389_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo InterpreterFactory_get_Gap_m8390_MethodInfo;
extern MethodInfo InterpreterFactory_set_Gap_m8391_MethodInfo;
static PropertyInfo InterpreterFactory_t2078____Gap_PropertyInfo = 
{
	&InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, "Gap"/* name */
	, &InterpreterFactory_get_Gap_m8390_MethodInfo/* get */
	, &InterpreterFactory_set_Gap_m8391_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo InterpreterFactory_get_Mapping_m8392_MethodInfo;
extern MethodInfo InterpreterFactory_set_Mapping_m8393_MethodInfo;
static PropertyInfo InterpreterFactory_t2078____Mapping_PropertyInfo = 
{
	&InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, "Mapping"/* name */
	, &InterpreterFactory_get_Mapping_m8392_MethodInfo/* get */
	, &InterpreterFactory_set_Mapping_m8393_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo InterpreterFactory_get_NamesMapping_m8394_MethodInfo;
extern MethodInfo InterpreterFactory_set_NamesMapping_m8395_MethodInfo;
static PropertyInfo InterpreterFactory_t2078____NamesMapping_PropertyInfo = 
{
	&InterpreterFactory_t2078_il2cpp_TypeInfo/* parent */
	, "NamesMapping"/* name */
	, &InterpreterFactory_get_NamesMapping_m8394_MethodInfo/* get */
	, &InterpreterFactory_set_NamesMapping_m8395_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* InterpreterFactory_t2078_PropertyInfos[] =
{
	&InterpreterFactory_t2078____GroupCount_PropertyInfo,
	&InterpreterFactory_t2078____Gap_PropertyInfo,
	&InterpreterFactory_t2078____Mapping_PropertyInfo,
	&InterpreterFactory_t2078____NamesMapping_PropertyInfo,
	NULL
};
extern MethodInfo InterpreterFactory_NewInstance_m8388_MethodInfo;
static Il2CppMethodReference InterpreterFactory_t2078_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&InterpreterFactory_NewInstance_m8388_MethodInfo,
	&InterpreterFactory_get_Mapping_m8392_MethodInfo,
	&InterpreterFactory_set_Mapping_m8393_MethodInfo,
	&InterpreterFactory_get_GroupCount_m8389_MethodInfo,
	&InterpreterFactory_get_Gap_m8390_MethodInfo,
	&InterpreterFactory_set_Gap_m8391_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m8394_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m8395_MethodInfo,
};
static bool InterpreterFactory_t2078_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* InterpreterFactory_t2078_InterfacesTypeInfos[] = 
{
	&IMachineFactory_t2067_0_0_0,
};
static Il2CppInterfaceOffsetPair InterpreterFactory_t2078_InterfacesOffsets[] = 
{
	{ &IMachineFactory_t2067_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType InterpreterFactory_t2078_0_0_0;
extern Il2CppType InterpreterFactory_t2078_1_0_0;
struct InterpreterFactory_t2078;
const Il2CppTypeDefinitionMetadata InterpreterFactory_t2078_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, InterpreterFactory_t2078_InterfacesTypeInfos/* implementedInterfaces */
	, InterpreterFactory_t2078_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InterpreterFactory_t2078_VTable/* vtableMethods */
	, InterpreterFactory_t2078_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo InterpreterFactory_t2078_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterpreterFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, InterpreterFactory_t2078_MethodInfos/* methods */
	, InterpreterFactory_t2078_PropertyInfos/* properties */
	, InterpreterFactory_t2078_FieldInfos/* fields */
	, NULL/* events */
	, &InterpreterFactory_t2078_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InterpreterFactory_t2078_0_0_0/* byval_arg */
	, &InterpreterFactory_t2078_1_0_0/* this_arg */
	, &InterpreterFactory_t2078_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterpreterFactory_t2078)/* instance_size */
	, sizeof (InterpreterFactory_t2078)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
extern TypeInfo Link_t2079_il2cpp_TypeInfo;
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_PatterMethodDeclarations.h"
static MethodInfo* Link_t2079_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Link_t2079____base_addr_0_FieldInfo = 
{
	"base_addr"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Link_t2079_il2cpp_TypeInfo/* parent */
	, offsetof(Link_t2079, ___base_addr_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Link_t2079____offset_addr_1_FieldInfo = 
{
	"offset_addr"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Link_t2079_il2cpp_TypeInfo/* parent */
	, offsetof(Link_t2079, ___offset_addr_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Link_t2079_FieldInfos[] =
{
	&Link_t2079____base_addr_0_FieldInfo,
	&Link_t2079____offset_addr_1_FieldInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1319_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1320_MethodInfo;
extern MethodInfo ValueType_ToString_m1321_MethodInfo;
static Il2CppMethodReference Link_t2079_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool Link_t2079_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Link_t2079_0_0_0;
extern Il2CppType Link_t2079_1_0_0;
extern Il2CppType ValueType_t329_0_0_0;
extern TypeInfo PatternLinkStack_t2080_il2cpp_TypeInfo;
extern Il2CppType PatternLinkStack_t2080_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t2079_DefinitionMetadata = 
{
	&PatternLinkStack_t2080_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, Link_t2079_VTable/* vtableMethods */
	, Link_t2079_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Link_t2079_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t2079_MethodInfos/* methods */
	, NULL/* properties */
	, Link_t2079_FieldInfos/* fields */
	, NULL/* events */
	, &Link_t2079_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t2079_0_0_0/* byval_arg */
	, &Link_t2079_1_0_0/* this_arg */
	, &Link_t2079_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Link_t2079)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Link_t2079)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Link_t2079 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::.ctor()
MethodInfo PatternLinkStack__ctor_m8396_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternLinkStack__ctor_m8396/* method */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PatternLinkStack_t2080_PatternLinkStack_set_BaseAddress_m8397_ParameterInfos[] = 
{
	{"value", 0, 134218314, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_BaseAddress(System.Int32)
MethodInfo PatternLinkStack_set_BaseAddress_m8397_MethodInfo = 
{
	"set_BaseAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_BaseAddress_m8397/* method */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, PatternLinkStack_t2080_PatternLinkStack_set_BaseAddress_m8397_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::get_OffsetAddress()
MethodInfo PatternLinkStack_get_OffsetAddress_m8398_MethodInfo = 
{
	"get_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_get_OffsetAddress_m8398/* method */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PatternLinkStack_t2080_PatternLinkStack_set_OffsetAddress_m8399_ParameterInfos[] = 
{
	{"value", 0, 134218315, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_OffsetAddress(System.Int32)
MethodInfo PatternLinkStack_set_OffsetAddress_m8399_MethodInfo = 
{
	"set_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_OffsetAddress_m8399/* method */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, PatternLinkStack_t2080_PatternLinkStack_set_OffsetAddress_m8399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PatternLinkStack_t2080_PatternLinkStack_GetOffset_m8400_ParameterInfos[] = 
{
	{"target_addr", 0, 134218316, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetOffset(System.Int32)
MethodInfo PatternLinkStack_GetOffset_m8400_MethodInfo = 
{
	"GetOffset"/* name */
	, (methodPointerType)&PatternLinkStack_GetOffset_m8400/* method */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int32_t189/* invoker_method */
	, PatternLinkStack_t2080_PatternLinkStack_GetOffset_m8400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetCurrent()
MethodInfo PatternLinkStack_GetCurrent_m8401_MethodInfo = 
{
	"GetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_GetCurrent_m8401/* method */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo PatternLinkStack_t2080_PatternLinkStack_SetCurrent_m8402_ParameterInfos[] = 
{
	{"l", 0, 134218317, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::SetCurrent(System.Object)
MethodInfo PatternLinkStack_SetCurrent_m8402_MethodInfo = 
{
	"SetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_SetCurrent_m8402/* method */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternLinkStack_t2080_PatternLinkStack_SetCurrent_m8402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PatternLinkStack_t2080_MethodInfos[] =
{
	&PatternLinkStack__ctor_m8396_MethodInfo,
	&PatternLinkStack_set_BaseAddress_m8397_MethodInfo,
	&PatternLinkStack_get_OffsetAddress_m8398_MethodInfo,
	&PatternLinkStack_set_OffsetAddress_m8399_MethodInfo,
	&PatternLinkStack_GetOffset_m8400_MethodInfo,
	&PatternLinkStack_GetCurrent_m8401_MethodInfo,
	&PatternLinkStack_SetCurrent_m8402_MethodInfo,
	NULL
};
extern Il2CppType Link_t2079_0_0_1;
FieldInfo PatternLinkStack_t2080____link_1_FieldInfo = 
{
	"link"/* name */
	, &Link_t2079_0_0_1/* type */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* parent */
	, offsetof(PatternLinkStack_t2080, ___link_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* PatternLinkStack_t2080_FieldInfos[] =
{
	&PatternLinkStack_t2080____link_1_FieldInfo,
	NULL
};
extern MethodInfo PatternLinkStack_set_BaseAddress_m8397_MethodInfo;
static PropertyInfo PatternLinkStack_t2080____BaseAddress_PropertyInfo = 
{
	&PatternLinkStack_t2080_il2cpp_TypeInfo/* parent */
	, "BaseAddress"/* name */
	, NULL/* get */
	, &PatternLinkStack_set_BaseAddress_m8397_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo PatternLinkStack_get_OffsetAddress_m8398_MethodInfo;
extern MethodInfo PatternLinkStack_set_OffsetAddress_m8399_MethodInfo;
static PropertyInfo PatternLinkStack_t2080____OffsetAddress_PropertyInfo = 
{
	&PatternLinkStack_t2080_il2cpp_TypeInfo/* parent */
	, "OffsetAddress"/* name */
	, &PatternLinkStack_get_OffsetAddress_m8398_MethodInfo/* get */
	, &PatternLinkStack_set_OffsetAddress_m8399_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* PatternLinkStack_t2080_PropertyInfos[] =
{
	&PatternLinkStack_t2080____BaseAddress_PropertyInfo,
	&PatternLinkStack_t2080____OffsetAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternLinkStack_t2080_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Link_t2079_0_0_0,
};
extern MethodInfo PatternLinkStack_GetCurrent_m8401_MethodInfo;
extern MethodInfo PatternLinkStack_SetCurrent_m8402_MethodInfo;
static Il2CppMethodReference PatternLinkStack_t2080_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&PatternLinkStack_GetCurrent_m8401_MethodInfo,
	&PatternLinkStack_SetCurrent_m8402_MethodInfo,
};
static bool PatternLinkStack_t2080_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType PatternLinkStack_t2080_1_0_0;
extern Il2CppType LinkStack_t2081_0_0_0;
extern TypeInfo PatternCompiler_t2082_il2cpp_TypeInfo;
extern Il2CppType PatternCompiler_t2082_0_0_0;
struct PatternLinkStack_t2080;
const Il2CppTypeDefinitionMetadata PatternLinkStack_t2080_DefinitionMetadata = 
{
	&PatternCompiler_t2082_0_0_0/* declaringType */
	, PatternLinkStack_t2080_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkStack_t2081_0_0_0/* parent */
	, PatternLinkStack_t2080_VTable/* vtableMethods */
	, PatternLinkStack_t2080_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PatternLinkStack_t2080_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternLinkStack"/* name */
	, ""/* namespaze */
	, PatternLinkStack_t2080_MethodInfos/* methods */
	, PatternLinkStack_t2080_PropertyInfos/* properties */
	, PatternLinkStack_t2080_FieldInfos/* fields */
	, NULL/* events */
	, &PatternLinkStack_t2080_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternLinkStack_t2080_0_0_0/* byval_arg */
	, &PatternLinkStack_t2080_1_0_0/* this_arg */
	, &PatternLinkStack_t2080_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternLinkStack_t2080)/* instance_size */
	, sizeof (PatternLinkStack_t2080)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompiler.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompilerMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::.ctor()
MethodInfo PatternCompiler__ctor_m8403_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternCompiler__ctor_m8403/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType OpCode_t2069_0_0_0;
extern Il2CppType OpCode_t2069_0_0_0;
extern Il2CppType OpFlags_t2070_0_0_0;
extern Il2CppType OpFlags_t2070_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EncodeOp_m8404_ParameterInfos[] = 
{
	{"op", 0, 134218244, 0, &OpCode_t2069_0_0_0},
	{"flags", 1, 134218245, 0, &OpFlags_t2070_0_0_0},
};
extern Il2CppType UInt16_t194_0_0_0;
extern void* RuntimeInvoker_UInt16_t194_UInt16_t194_UInt16_t194 (MethodInfo* method, void* obj, void** args);
// System.UInt16 System.Text.RegularExpressions.PatternCompiler::EncodeOp(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
MethodInfo PatternCompiler_EncodeOp_m8404_MethodInfo = 
{
	"EncodeOp"/* name */
	, (methodPointerType)&PatternCompiler_EncodeOp_m8404/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &UInt16_t194_0_0_0/* return_type */
	, RuntimeInvoker_UInt16_t194_UInt16_t194_UInt16_t194/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EncodeOp_m8404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMachineFactory_t2067_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.PatternCompiler::GetMachineFactory()
MethodInfo PatternCompiler_GetMachineFactory_m8405_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, (methodPointerType)&PatternCompiler_GetMachineFactory_m8405/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t2067_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFalse()
MethodInfo PatternCompiler_EmitFalse_m8406_MethodInfo = 
{
	"EmitFalse"/* name */
	, (methodPointerType)&PatternCompiler_EmitFalse_m8406/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTrue()
MethodInfo PatternCompiler_EmitTrue_m8407_MethodInfo = 
{
	"EmitTrue"/* name */
	, (methodPointerType)&PatternCompiler_EmitTrue_m8407/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitCount_m8408_ParameterInfos[] = 
{
	{"count", 0, 134218246, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCount(System.Int32)
MethodInfo PatternCompiler_EmitCount_m8408_MethodInfo = 
{
	"EmitCount"/* name */
	, (methodPointerType)&PatternCompiler_EmitCount_m8408/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitCount_m8408_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitCharacter_m8409_ParameterInfos[] = 
{
	{"c", 0, 134218247, 0, &Char_t193_0_0_0},
	{"negate", 1, 134218248, 0, &Boolean_t203_0_0_0},
	{"ignore", 2, 134218249, 0, &Boolean_t203_0_0_0},
	{"reverse", 3, 134218250, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_EmitCharacter_m8409_MethodInfo = 
{
	"EmitCharacter"/* name */
	, (methodPointerType)&PatternCompiler_EmitCharacter_m8409/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitCharacter_m8409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitCategory_m8410_ParameterInfos[] = 
{
	{"cat", 0, 134218251, 0, &Category_t2075_0_0_0},
	{"negate", 1, 134218252, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218253, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_EmitCategory_m8410_MethodInfo = 
{
	"EmitCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitCategory_m8410/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitCategory_m8410_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitNotCategory_m8411_ParameterInfos[] = 
{
	{"cat", 0, 134218254, 0, &Category_t2075_0_0_0},
	{"negate", 1, 134218255, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218256, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_EmitNotCategory_m8411_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitNotCategory_m8411/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitNotCategory_m8411_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitRange_m8412_ParameterInfos[] = 
{
	{"lo", 0, 134218257, 0, &Char_t193_0_0_0},
	{"hi", 1, 134218258, 0, &Char_t193_0_0_0},
	{"negate", 2, 134218259, 0, &Boolean_t203_0_0_0},
	{"ignore", 3, 134218260, 0, &Boolean_t203_0_0_0},
	{"reverse", 4, 134218261, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238_Int16_t238_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_EmitRange_m8412_MethodInfo = 
{
	"EmitRange"/* name */
	, (methodPointerType)&PatternCompiler_EmitRange_m8412/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238_Int16_t238_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitRange_m8412_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType BitArray_t2112_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitSet_m8413_ParameterInfos[] = 
{
	{"lo", 0, 134218262, 0, &Char_t193_0_0_0},
	{"set", 1, 134218263, 0, &BitArray_t2112_0_0_0},
	{"negate", 2, 134218264, 0, &Boolean_t203_0_0_0},
	{"ignore", 3, 134218265, 0, &Boolean_t203_0_0_0},
	{"reverse", 4, 134218266, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238_Object_t_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_EmitSet_m8413_MethodInfo = 
{
	"EmitSet"/* name */
	, (methodPointerType)&PatternCompiler_EmitSet_m8413/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238_Object_t_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitSet_m8413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitString_m8414_ParameterInfos[] = 
{
	{"str", 0, 134218267, 0, &String_t_0_0_0},
	{"ignore", 1, 134218268, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218269, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitString(System.String,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_EmitString_m8414_MethodInfo = 
{
	"EmitString"/* name */
	, (methodPointerType)&PatternCompiler_EmitString_m8414/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitString_m8414_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Position_t2071_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitPosition_m8415_ParameterInfos[] = 
{
	{"pos", 0, 134218270, 0, &Position_t2071_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitPosition(System.Text.RegularExpressions.Position)
MethodInfo PatternCompiler_EmitPosition_m8415_MethodInfo = 
{
	"EmitPosition"/* name */
	, (methodPointerType)&PatternCompiler_EmitPosition_m8415/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitPosition_m8415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitOpen_m8416_ParameterInfos[] = 
{
	{"gid", 0, 134218271, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitOpen(System.Int32)
MethodInfo PatternCompiler_EmitOpen_m8416_MethodInfo = 
{
	"EmitOpen"/* name */
	, (methodPointerType)&PatternCompiler_EmitOpen_m8416/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitOpen_m8416_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitClose_m8417_ParameterInfos[] = 
{
	{"gid", 0, 134218272, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitClose(System.Int32)
MethodInfo PatternCompiler_EmitClose_m8417_MethodInfo = 
{
	"EmitClose"/* name */
	, (methodPointerType)&PatternCompiler_EmitClose_m8417/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitClose_m8417_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitBalanceStart_m8418_ParameterInfos[] = 
{
	{"gid", 0, 134218273, 0, &Int32_t189_0_0_0},
	{"balance", 1, 134218274, 0, &Int32_t189_0_0_0},
	{"capture", 2, 134218275, 0, &Boolean_t203_0_0_0},
	{"tail", 3, 134218276, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitBalanceStart_m8418_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalanceStart_m8418/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitBalanceStart_m8418_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalance()
MethodInfo PatternCompiler_EmitBalance_m8419_MethodInfo = 
{
	"EmitBalance"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalance_m8419/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitReference_m8420_ParameterInfos[] = 
{
	{"gid", 0, 134218277, 0, &Int32_t189_0_0_0},
	{"ignore", 1, 134218278, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218279, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_EmitReference_m8420_MethodInfo = 
{
	"EmitReference"/* name */
	, (methodPointerType)&PatternCompiler_EmitReference_m8420/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitReference_m8420_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitIfDefined_m8421_ParameterInfos[] = 
{
	{"gid", 0, 134218280, 0, &Int32_t189_0_0_0},
	{"tail", 1, 134218281, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitIfDefined_m8421_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, (methodPointerType)&PatternCompiler_EmitIfDefined_m8421/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitIfDefined_m8421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitSub_m8422_ParameterInfos[] = 
{
	{"tail", 0, 134218282, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitSub_m8422_MethodInfo = 
{
	"EmitSub"/* name */
	, (methodPointerType)&PatternCompiler_EmitSub_m8422/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitSub_m8422_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitTest_m8423_ParameterInfos[] = 
{
	{"yes", 0, 134218283, 0, &LinkRef_t2077_0_0_0},
	{"tail", 1, 134218284, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitTest_m8423_MethodInfo = 
{
	"EmitTest"/* name */
	, (methodPointerType)&PatternCompiler_EmitTest_m8423/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitTest_m8423_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitBranch_m8424_ParameterInfos[] = 
{
	{"next", 0, 134218285, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitBranch_m8424_MethodInfo = 
{
	"EmitBranch"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranch_m8424/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitBranch_m8424_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitJump_m8425_ParameterInfos[] = 
{
	{"target", 0, 134218286, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitJump_m8425_MethodInfo = 
{
	"EmitJump"/* name */
	, (methodPointerType)&PatternCompiler_EmitJump_m8425/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitJump_m8425_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitRepeat_m8426_ParameterInfos[] = 
{
	{"min", 0, 134218287, 0, &Int32_t189_0_0_0},
	{"max", 1, 134218288, 0, &Int32_t189_0_0_0},
	{"lazy", 2, 134218289, 0, &Boolean_t203_0_0_0},
	{"until", 3, 134218290, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitRepeat_m8426_MethodInfo = 
{
	"EmitRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitRepeat_m8426/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitRepeat_m8426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitUntil_m8427_ParameterInfos[] = 
{
	{"repeat", 0, 134218291, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitUntil_m8427_MethodInfo = 
{
	"EmitUntil"/* name */
	, (methodPointerType)&PatternCompiler_EmitUntil_m8427/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitUntil_m8427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitFastRepeat_m8428_ParameterInfos[] = 
{
	{"min", 0, 134218292, 0, &Int32_t189_0_0_0},
	{"max", 1, 134218293, 0, &Int32_t189_0_0_0},
	{"lazy", 2, 134218294, 0, &Boolean_t203_0_0_0},
	{"tail", 3, 134218295, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitFastRepeat_m8428_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitFastRepeat_m8428/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitFastRepeat_m8428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitIn_m8429_ParameterInfos[] = 
{
	{"tail", 0, 134218296, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitIn_m8429_MethodInfo = 
{
	"EmitIn"/* name */
	, (methodPointerType)&PatternCompiler_EmitIn_m8429/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitIn_m8429_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitAnchor_m8430_ParameterInfos[] = 
{
	{"reverse", 0, 134218297, 0, &Boolean_t203_0_0_0},
	{"offset", 1, 134218298, 0, &Int32_t189_0_0_0},
	{"tail", 2, 134218299, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitAnchor_m8430_MethodInfo = 
{
	"EmitAnchor"/* name */
	, (methodPointerType)&PatternCompiler_EmitAnchor_m8430/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236_Int32_t189_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitAnchor_m8430_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitInfo_m8431_ParameterInfos[] = 
{
	{"count", 0, 134218300, 0, &Int32_t189_0_0_0},
	{"min", 1, 134218301, 0, &Int32_t189_0_0_0},
	{"max", 2, 134218302, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
MethodInfo PatternCompiler_EmitInfo_m8431_MethodInfo = 
{
	"EmitInfo"/* name */
	, (methodPointerType)&PatternCompiler_EmitInfo_m8431/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitInfo_m8431_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.PatternCompiler::NewLink()
MethodInfo PatternCompiler_NewLink_m8432_MethodInfo = 
{
	"NewLink"/* name */
	, (methodPointerType)&PatternCompiler_NewLink_m8432/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t2077_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_ResolveLink_m8433_ParameterInfos[] = 
{
	{"lref", 0, 134218303, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_ResolveLink_m8433_MethodInfo = 
{
	"ResolveLink"/* name */
	, (methodPointerType)&PatternCompiler_ResolveLink_m8433/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_ResolveLink_m8433_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranchEnd()
MethodInfo PatternCompiler_EmitBranchEnd_m8434_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranchEnd_m8434/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAlternationEnd()
MethodInfo PatternCompiler_EmitAlternationEnd_m8435_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitAlternationEnd_m8435/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_MakeFlags_m8436_ParameterInfos[] = 
{
	{"negate", 0, 134218304, 0, &Boolean_t203_0_0_0},
	{"ignore", 1, 134218305, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218306, 0, &Boolean_t203_0_0_0},
	{"lazy", 3, 134218307, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType OpFlags_t2070_0_0_0;
extern void* RuntimeInvoker_OpFlags_t2070_SByte_t236_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.OpFlags System.Text.RegularExpressions.PatternCompiler::MakeFlags(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
MethodInfo PatternCompiler_MakeFlags_m8436_MethodInfo = 
{
	"MakeFlags"/* name */
	, (methodPointerType)&PatternCompiler_MakeFlags_m8436/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &OpFlags_t2070_0_0_0/* return_type */
	, RuntimeInvoker_OpFlags_t2070_SByte_t236_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_MakeFlags_m8436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType OpCode_t2069_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_Emit_m8437_ParameterInfos[] = 
{
	{"op", 0, 134218308, 0, &OpCode_t2069_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode)
MethodInfo PatternCompiler_Emit_m8437_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m8437/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_Emit_m8437_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType OpCode_t2069_0_0_0;
extern Il2CppType OpFlags_t2070_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_Emit_m8438_ParameterInfos[] = 
{
	{"op", 0, 134218309, 0, &OpCode_t2069_0_0_0},
	{"flags", 1, 134218310, 0, &OpFlags_t2070_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194_UInt16_t194 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
MethodInfo PatternCompiler_Emit_m8438_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m8438/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194_UInt16_t194/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_Emit_m8438_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UInt16_t194_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_Emit_m8439_ParameterInfos[] = 
{
	{"word", 0, 134218311, 0, &UInt16_t194_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.UInt16)
MethodInfo PatternCompiler_Emit_m8439_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m8439/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_Emit_m8439_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler::get_CurrentAddress()
MethodInfo PatternCompiler_get_CurrentAddress_m8440_MethodInfo = 
{
	"get_CurrentAddress"/* name */
	, (methodPointerType)&PatternCompiler_get_CurrentAddress_m8440/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_BeginLink_m8441_ParameterInfos[] = 
{
	{"lref", 0, 134218312, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::BeginLink(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_BeginLink_m8441_MethodInfo = 
{
	"BeginLink"/* name */
	, (methodPointerType)&PatternCompiler_BeginLink_m8441/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_BeginLink_m8441_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LinkRef_t2077_0_0_0;
static ParameterInfo PatternCompiler_t2082_PatternCompiler_EmitLink_m8442_ParameterInfos[] = 
{
	{"lref", 0, 134218313, 0, &LinkRef_t2077_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitLink(System.Text.RegularExpressions.LinkRef)
MethodInfo PatternCompiler_EmitLink_m8442_MethodInfo = 
{
	"EmitLink"/* name */
	, (methodPointerType)&PatternCompiler_EmitLink_m8442/* method */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PatternCompiler_t2082_PatternCompiler_EmitLink_m8442_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PatternCompiler_t2082_MethodInfos[] =
{
	&PatternCompiler__ctor_m8403_MethodInfo,
	&PatternCompiler_EncodeOp_m8404_MethodInfo,
	&PatternCompiler_GetMachineFactory_m8405_MethodInfo,
	&PatternCompiler_EmitFalse_m8406_MethodInfo,
	&PatternCompiler_EmitTrue_m8407_MethodInfo,
	&PatternCompiler_EmitCount_m8408_MethodInfo,
	&PatternCompiler_EmitCharacter_m8409_MethodInfo,
	&PatternCompiler_EmitCategory_m8410_MethodInfo,
	&PatternCompiler_EmitNotCategory_m8411_MethodInfo,
	&PatternCompiler_EmitRange_m8412_MethodInfo,
	&PatternCompiler_EmitSet_m8413_MethodInfo,
	&PatternCompiler_EmitString_m8414_MethodInfo,
	&PatternCompiler_EmitPosition_m8415_MethodInfo,
	&PatternCompiler_EmitOpen_m8416_MethodInfo,
	&PatternCompiler_EmitClose_m8417_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m8418_MethodInfo,
	&PatternCompiler_EmitBalance_m8419_MethodInfo,
	&PatternCompiler_EmitReference_m8420_MethodInfo,
	&PatternCompiler_EmitIfDefined_m8421_MethodInfo,
	&PatternCompiler_EmitSub_m8422_MethodInfo,
	&PatternCompiler_EmitTest_m8423_MethodInfo,
	&PatternCompiler_EmitBranch_m8424_MethodInfo,
	&PatternCompiler_EmitJump_m8425_MethodInfo,
	&PatternCompiler_EmitRepeat_m8426_MethodInfo,
	&PatternCompiler_EmitUntil_m8427_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m8428_MethodInfo,
	&PatternCompiler_EmitIn_m8429_MethodInfo,
	&PatternCompiler_EmitAnchor_m8430_MethodInfo,
	&PatternCompiler_EmitInfo_m8431_MethodInfo,
	&PatternCompiler_NewLink_m8432_MethodInfo,
	&PatternCompiler_ResolveLink_m8433_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m8434_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m8435_MethodInfo,
	&PatternCompiler_MakeFlags_m8436_MethodInfo,
	&PatternCompiler_Emit_m8437_MethodInfo,
	&PatternCompiler_Emit_m8438_MethodInfo,
	&PatternCompiler_Emit_m8439_MethodInfo,
	&PatternCompiler_get_CurrentAddress_m8440_MethodInfo,
	&PatternCompiler_BeginLink_m8441_MethodInfo,
	&PatternCompiler_EmitLink_m8442_MethodInfo,
	NULL
};
extern Il2CppType ArrayList_t737_0_0_1;
FieldInfo PatternCompiler_t2082____pgm_0_FieldInfo = 
{
	"pgm"/* name */
	, &ArrayList_t737_0_0_1/* type */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* parent */
	, offsetof(PatternCompiler_t2082, ___pgm_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* PatternCompiler_t2082_FieldInfos[] =
{
	&PatternCompiler_t2082____pgm_0_FieldInfo,
	NULL
};
extern MethodInfo PatternCompiler_get_CurrentAddress_m8440_MethodInfo;
static PropertyInfo PatternCompiler_t2082____CurrentAddress_PropertyInfo = 
{
	&PatternCompiler_t2082_il2cpp_TypeInfo/* parent */
	, "CurrentAddress"/* name */
	, &PatternCompiler_get_CurrentAddress_m8440_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* PatternCompiler_t2082_PropertyInfos[] =
{
	&PatternCompiler_t2082____CurrentAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternCompiler_t2082_il2cpp_TypeInfo__nestedTypes[1] =
{
	&PatternLinkStack_t2080_0_0_0,
};
extern MethodInfo PatternCompiler_GetMachineFactory_m8405_MethodInfo;
extern MethodInfo PatternCompiler_EmitFalse_m8406_MethodInfo;
extern MethodInfo PatternCompiler_EmitTrue_m8407_MethodInfo;
extern MethodInfo PatternCompiler_EmitCharacter_m8409_MethodInfo;
extern MethodInfo PatternCompiler_EmitCategory_m8410_MethodInfo;
extern MethodInfo PatternCompiler_EmitNotCategory_m8411_MethodInfo;
extern MethodInfo PatternCompiler_EmitRange_m8412_MethodInfo;
extern MethodInfo PatternCompiler_EmitSet_m8413_MethodInfo;
extern MethodInfo PatternCompiler_EmitString_m8414_MethodInfo;
extern MethodInfo PatternCompiler_EmitPosition_m8415_MethodInfo;
extern MethodInfo PatternCompiler_EmitOpen_m8416_MethodInfo;
extern MethodInfo PatternCompiler_EmitClose_m8417_MethodInfo;
extern MethodInfo PatternCompiler_EmitBalanceStart_m8418_MethodInfo;
extern MethodInfo PatternCompiler_EmitBalance_m8419_MethodInfo;
extern MethodInfo PatternCompiler_EmitReference_m8420_MethodInfo;
extern MethodInfo PatternCompiler_EmitIfDefined_m8421_MethodInfo;
extern MethodInfo PatternCompiler_EmitSub_m8422_MethodInfo;
extern MethodInfo PatternCompiler_EmitTest_m8423_MethodInfo;
extern MethodInfo PatternCompiler_EmitBranch_m8424_MethodInfo;
extern MethodInfo PatternCompiler_EmitJump_m8425_MethodInfo;
extern MethodInfo PatternCompiler_EmitRepeat_m8426_MethodInfo;
extern MethodInfo PatternCompiler_EmitUntil_m8427_MethodInfo;
extern MethodInfo PatternCompiler_EmitIn_m8429_MethodInfo;
extern MethodInfo PatternCompiler_EmitInfo_m8431_MethodInfo;
extern MethodInfo PatternCompiler_EmitFastRepeat_m8428_MethodInfo;
extern MethodInfo PatternCompiler_EmitAnchor_m8430_MethodInfo;
extern MethodInfo PatternCompiler_EmitBranchEnd_m8434_MethodInfo;
extern MethodInfo PatternCompiler_EmitAlternationEnd_m8435_MethodInfo;
extern MethodInfo PatternCompiler_NewLink_m8432_MethodInfo;
extern MethodInfo PatternCompiler_ResolveLink_m8433_MethodInfo;
static Il2CppMethodReference PatternCompiler_t2082_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&PatternCompiler_GetMachineFactory_m8405_MethodInfo,
	&PatternCompiler_EmitFalse_m8406_MethodInfo,
	&PatternCompiler_EmitTrue_m8407_MethodInfo,
	&PatternCompiler_EmitCharacter_m8409_MethodInfo,
	&PatternCompiler_EmitCategory_m8410_MethodInfo,
	&PatternCompiler_EmitNotCategory_m8411_MethodInfo,
	&PatternCompiler_EmitRange_m8412_MethodInfo,
	&PatternCompiler_EmitSet_m8413_MethodInfo,
	&PatternCompiler_EmitString_m8414_MethodInfo,
	&PatternCompiler_EmitPosition_m8415_MethodInfo,
	&PatternCompiler_EmitOpen_m8416_MethodInfo,
	&PatternCompiler_EmitClose_m8417_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m8418_MethodInfo,
	&PatternCompiler_EmitBalance_m8419_MethodInfo,
	&PatternCompiler_EmitReference_m8420_MethodInfo,
	&PatternCompiler_EmitIfDefined_m8421_MethodInfo,
	&PatternCompiler_EmitSub_m8422_MethodInfo,
	&PatternCompiler_EmitTest_m8423_MethodInfo,
	&PatternCompiler_EmitBranch_m8424_MethodInfo,
	&PatternCompiler_EmitJump_m8425_MethodInfo,
	&PatternCompiler_EmitRepeat_m8426_MethodInfo,
	&PatternCompiler_EmitUntil_m8427_MethodInfo,
	&PatternCompiler_EmitIn_m8429_MethodInfo,
	&PatternCompiler_EmitInfo_m8431_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m8428_MethodInfo,
	&PatternCompiler_EmitAnchor_m8430_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m8434_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m8435_MethodInfo,
	&PatternCompiler_NewLink_m8432_MethodInfo,
	&PatternCompiler_ResolveLink_m8433_MethodInfo,
};
static bool PatternCompiler_t2082_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* PatternCompiler_t2082_InterfacesTypeInfos[] = 
{
	&ICompiler_t2137_0_0_0,
};
static Il2CppInterfaceOffsetPair PatternCompiler_t2082_InterfacesOffsets[] = 
{
	{ &ICompiler_t2137_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType PatternCompiler_t2082_1_0_0;
struct PatternCompiler_t2082;
const Il2CppTypeDefinitionMetadata PatternCompiler_t2082_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PatternCompiler_t2082_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PatternCompiler_t2082_InterfacesTypeInfos/* implementedInterfaces */
	, PatternCompiler_t2082_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PatternCompiler_t2082_VTable/* vtableMethods */
	, PatternCompiler_t2082_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PatternCompiler_t2082_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternCompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, PatternCompiler_t2082_MethodInfos/* methods */
	, PatternCompiler_t2082_PropertyInfos/* properties */
	, PatternCompiler_t2082_FieldInfos/* fields */
	, NULL/* events */
	, &PatternCompiler_t2082_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternCompiler_t2082_0_0_0/* byval_arg */
	, &PatternCompiler_t2082_1_0_0/* this_arg */
	, &PatternCompiler_t2082_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternCompiler_t2082)/* instance_size */
	, sizeof (PatternCompiler_t2082)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 40/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 34/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// Metadata Definition System.Text.RegularExpressions.LinkStack
extern TypeInfo LinkStack_t2081_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStackMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::.ctor()
MethodInfo LinkStack__ctor_m8443_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkStack__ctor_m8443/* method */
	, &LinkStack_t2081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::Push()
MethodInfo LinkStack_Push_m8444_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&LinkStack_Push_m8444/* method */
	, &LinkStack_t2081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.LinkStack::Pop()
MethodInfo LinkStack_Pop_m8445_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&LinkStack_Pop_m8445/* method */
	, &LinkStack_t2081_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.LinkStack::GetCurrent()
MethodInfo LinkStack_GetCurrent_m8941_MethodInfo = 
{
	"GetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t2081_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkStack_t2081_LinkStack_SetCurrent_m8942_ParameterInfos[] = 
{
	{"l", 0, 134218318, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::SetCurrent(System.Object)
MethodInfo LinkStack_SetCurrent_m8942_MethodInfo = 
{
	"SetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t2081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LinkStack_t2081_LinkStack_SetCurrent_m8942_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LinkStack_t2081_MethodInfos[] =
{
	&LinkStack__ctor_m8443_MethodInfo,
	&LinkStack_Push_m8444_MethodInfo,
	&LinkStack_Pop_m8445_MethodInfo,
	&LinkStack_GetCurrent_m8941_MethodInfo,
	&LinkStack_SetCurrent_m8942_MethodInfo,
	NULL
};
extern Il2CppType Stack_t1661_0_0_1;
FieldInfo LinkStack_t2081____stack_0_FieldInfo = 
{
	"stack"/* name */
	, &Stack_t1661_0_0_1/* type */
	, &LinkStack_t2081_il2cpp_TypeInfo/* parent */
	, offsetof(LinkStack_t2081, ___stack_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* LinkStack_t2081_FieldInfos[] =
{
	&LinkStack_t2081____stack_0_FieldInfo,
	NULL
};
static Il2CppMethodReference LinkStack_t2081_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	NULL,
	NULL,
};
static bool LinkStack_t2081_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType LinkStack_t2081_1_0_0;
struct LinkStack_t2081;
const Il2CppTypeDefinitionMetadata LinkStack_t2081_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkRef_t2077_0_0_0/* parent */
	, LinkStack_t2081_VTable/* vtableMethods */
	, LinkStack_t2081_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LinkStack_t2081_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkStack"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkStack_t2081_MethodInfos/* methods */
	, NULL/* properties */
	, LinkStack_t2081_FieldInfos/* fields */
	, NULL/* events */
	, &LinkStack_t2081_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkStack_t2081_0_0_0/* byval_arg */
	, &LinkStack_t2081_1_0_0/* this_arg */
	, &LinkStack_t2081_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkStack_t2081)/* instance_size */
	, sizeof (LinkStack_t2081)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
// Metadata Definition System.Text.RegularExpressions.Mark
extern TypeInfo Mark_t2083_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_MarkMethodDeclarations.h"
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Mark::get_IsDefined()
MethodInfo Mark_get_IsDefined_m8446_MethodInfo = 
{
	"get_IsDefined"/* name */
	, (methodPointerType)&Mark_get_IsDefined_m8446/* method */
	, &Mark_t2083_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Index()
MethodInfo Mark_get_Index_m8447_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&Mark_get_Index_m8447/* method */
	, &Mark_t2083_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Length()
MethodInfo Mark_get_Length_m8448_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&Mark_get_Length_m8448/* method */
	, &Mark_t2083_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Mark_t2083_MethodInfos[] =
{
	&Mark_get_IsDefined_m8446_MethodInfo,
	&Mark_get_Index_m8447_MethodInfo,
	&Mark_get_Length_m8448_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Mark_t2083____Start_0_FieldInfo = 
{
	"Start"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Mark_t2083_il2cpp_TypeInfo/* parent */
	, offsetof(Mark_t2083, ___Start_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Mark_t2083____End_1_FieldInfo = 
{
	"End"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Mark_t2083_il2cpp_TypeInfo/* parent */
	, offsetof(Mark_t2083, ___End_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Mark_t2083____Previous_2_FieldInfo = 
{
	"Previous"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Mark_t2083_il2cpp_TypeInfo/* parent */
	, offsetof(Mark_t2083, ___Previous_2) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Mark_t2083_FieldInfos[] =
{
	&Mark_t2083____Start_0_FieldInfo,
	&Mark_t2083____End_1_FieldInfo,
	&Mark_t2083____Previous_2_FieldInfo,
	NULL
};
extern MethodInfo Mark_get_IsDefined_m8446_MethodInfo;
static PropertyInfo Mark_t2083____IsDefined_PropertyInfo = 
{
	&Mark_t2083_il2cpp_TypeInfo/* parent */
	, "IsDefined"/* name */
	, &Mark_get_IsDefined_m8446_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Mark_get_Index_m8447_MethodInfo;
static PropertyInfo Mark_t2083____Index_PropertyInfo = 
{
	&Mark_t2083_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &Mark_get_Index_m8447_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Mark_get_Length_m8448_MethodInfo;
static PropertyInfo Mark_t2083____Length_PropertyInfo = 
{
	&Mark_t2083_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &Mark_get_Length_m8448_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Mark_t2083_PropertyInfos[] =
{
	&Mark_t2083____IsDefined_PropertyInfo,
	&Mark_t2083____Index_PropertyInfo,
	&Mark_t2083____Length_PropertyInfo,
	NULL
};
static Il2CppMethodReference Mark_t2083_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool Mark_t2083_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Mark_t2083_0_0_0;
extern Il2CppType Mark_t2083_1_0_0;
const Il2CppTypeDefinitionMetadata Mark_t2083_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, Mark_t2083_VTable/* vtableMethods */
	, Mark_t2083_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Mark_t2083_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mark"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Mark_t2083_MethodInfos/* methods */
	, Mark_t2083_PropertyInfos/* properties */
	, Mark_t2083_FieldInfos/* fields */
	, NULL/* events */
	, &Mark_t2083_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mark_t2083_0_0_0/* byval_arg */
	, &Mark_t2083_1_0_0/* this_arg */
	, &Mark_t2083_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mark_t2083)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mark_t2083)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Mark_t2083 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStack.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/IntStack
extern TypeInfo IntStack_t2084_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStackMethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::Pop()
MethodInfo IntStack_Pop_m8449_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&IntStack_Pop_m8449/* method */
	, &IntStack_t2084_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo IntStack_t2084_IntStack_Push_m8450_ParameterInfos[] = 
{
	{"value", 0, 134218356, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::Push(System.Int32)
MethodInfo IntStack_Push_m8450_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&IntStack_Push_m8450/* method */
	, &IntStack_t2084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, IntStack_t2084_IntStack_Push_m8450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::get_Count()
MethodInfo IntStack_get_Count_m8451_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntStack_get_Count_m8451/* method */
	, &IntStack_t2084_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo IntStack_t2084_IntStack_set_Count_m8452_ParameterInfos[] = 
{
	{"value", 0, 134218357, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::set_Count(System.Int32)
MethodInfo IntStack_set_Count_m8452_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&IntStack_set_Count_m8452/* method */
	, &IntStack_t2084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, IntStack_t2084_IntStack_set_Count_m8452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IntStack_t2084_MethodInfos[] =
{
	&IntStack_Pop_m8449_MethodInfo,
	&IntStack_Push_m8450_MethodInfo,
	&IntStack_get_Count_m8451_MethodInfo,
	&IntStack_set_Count_m8452_MethodInfo,
	NULL
};
extern Il2CppType Int32U5BU5D_t107_0_0_1;
FieldInfo IntStack_t2084____values_0_FieldInfo = 
{
	"values"/* name */
	, &Int32U5BU5D_t107_0_0_1/* type */
	, &IntStack_t2084_il2cpp_TypeInfo/* parent */
	, offsetof(IntStack_t2084, ___values_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo IntStack_t2084____count_1_FieldInfo = 
{
	"count"/* name */
	, &Int32_t189_0_0_1/* type */
	, &IntStack_t2084_il2cpp_TypeInfo/* parent */
	, offsetof(IntStack_t2084, ___count_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* IntStack_t2084_FieldInfos[] =
{
	&IntStack_t2084____values_0_FieldInfo,
	&IntStack_t2084____count_1_FieldInfo,
	NULL
};
extern MethodInfo IntStack_get_Count_m8451_MethodInfo;
extern MethodInfo IntStack_set_Count_m8452_MethodInfo;
static PropertyInfo IntStack_t2084____Count_PropertyInfo = 
{
	&IntStack_t2084_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntStack_get_Count_m8451_MethodInfo/* get */
	, &IntStack_set_Count_m8452_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IntStack_t2084_PropertyInfos[] =
{
	&IntStack_t2084____Count_PropertyInfo,
	NULL
};
static Il2CppMethodReference IntStack_t2084_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool IntStack_t2084_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType IntStack_t2084_0_0_0;
extern Il2CppType IntStack_t2084_1_0_0;
extern TypeInfo Interpreter_t2089_il2cpp_TypeInfo;
extern Il2CppType Interpreter_t2089_0_0_0;
const Il2CppTypeDefinitionMetadata IntStack_t2084_DefinitionMetadata = 
{
	&Interpreter_t2089_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, IntStack_t2084_VTable/* vtableMethods */
	, IntStack_t2084_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IntStack_t2084_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntStack"/* name */
	, ""/* namespaze */
	, IntStack_t2084_MethodInfos/* methods */
	, IntStack_t2084_PropertyInfos/* properties */
	, IntStack_t2084_FieldInfos/* fields */
	, NULL/* events */
	, &IntStack_t2084_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntStack_t2084_0_0_0/* byval_arg */
	, &IntStack_t2084_1_0_0/* this_arg */
	, &IntStack_t2084_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)IntStack_t2084_marshal/* marshal_to_native_func */
	, (methodPointerType)IntStack_t2084_marshal_back/* marshal_from_native_func */
	, (methodPointerType)IntStack_t2084_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (IntStack_t2084)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IntStack_t2084)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(IntStack_t2084_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatCont.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/RepeatContext
extern TypeInfo RepeatContext_t2085_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatContMethodDeclarations.h"
extern Il2CppType RepeatContext_t2085_0_0_0;
extern Il2CppType RepeatContext_t2085_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo RepeatContext_t2085_RepeatContext__ctor_m8453_ParameterInfos[] = 
{
	{"previous", 0, 134218358, 0, &RepeatContext_t2085_0_0_0},
	{"min", 1, 134218359, 0, &Int32_t189_0_0_0},
	{"max", 2, 134218360, 0, &Int32_t189_0_0_0},
	{"lazy", 3, 134218361, 0, &Boolean_t203_0_0_0},
	{"expr_pc", 4, 134218362, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::.ctor(System.Text.RegularExpressions.Interpreter/RepeatContext,System.Int32,System.Int32,System.Boolean,System.Int32)
MethodInfo RepeatContext__ctor_m8453_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RepeatContext__ctor_m8453/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_SByte_t236_Int32_t189/* invoker_method */
	, RepeatContext_t2085_RepeatContext__ctor_m8453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Count()
MethodInfo RepeatContext_get_Count_m8454_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&RepeatContext_get_Count_m8454/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo RepeatContext_t2085_RepeatContext_set_Count_m8455_ParameterInfos[] = 
{
	{"value", 0, 134218363, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Count(System.Int32)
MethodInfo RepeatContext_set_Count_m8455_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&RepeatContext_set_Count_m8455/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, RepeatContext_t2085_RepeatContext_set_Count_m8455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Start()
MethodInfo RepeatContext_get_Start_m8456_MethodInfo = 
{
	"get_Start"/* name */
	, (methodPointerType)&RepeatContext_get_Start_m8456/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo RepeatContext_t2085_RepeatContext_set_Start_m8457_ParameterInfos[] = 
{
	{"value", 0, 134218364, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Start(System.Int32)
MethodInfo RepeatContext_set_Start_m8457_MethodInfo = 
{
	"set_Start"/* name */
	, (methodPointerType)&RepeatContext_set_Start_m8457/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, RepeatContext_t2085_RepeatContext_set_Start_m8457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMinimum()
MethodInfo RepeatContext_get_IsMinimum_m8458_MethodInfo = 
{
	"get_IsMinimum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMinimum_m8458/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMaximum()
MethodInfo RepeatContext_get_IsMaximum_m8459_MethodInfo = 
{
	"get_IsMaximum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMaximum_m8459/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsLazy()
MethodInfo RepeatContext_get_IsLazy_m8460_MethodInfo = 
{
	"get_IsLazy"/* name */
	, (methodPointerType)&RepeatContext_get_IsLazy_m8460/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Expression()
MethodInfo RepeatContext_get_Expression_m8461_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&RepeatContext_get_Expression_m8461/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RepeatContext_t2085_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter/RepeatContext::get_Previous()
MethodInfo RepeatContext_get_Previous_m8462_MethodInfo = 
{
	"get_Previous"/* name */
	, (methodPointerType)&RepeatContext_get_Previous_m8462/* method */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* declaring_type */
	, &RepeatContext_t2085_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RepeatContext_t2085_MethodInfos[] =
{
	&RepeatContext__ctor_m8453_MethodInfo,
	&RepeatContext_get_Count_m8454_MethodInfo,
	&RepeatContext_set_Count_m8455_MethodInfo,
	&RepeatContext_get_Start_m8456_MethodInfo,
	&RepeatContext_set_Start_m8457_MethodInfo,
	&RepeatContext_get_IsMinimum_m8458_MethodInfo,
	&RepeatContext_get_IsMaximum_m8459_MethodInfo,
	&RepeatContext_get_IsLazy_m8460_MethodInfo,
	&RepeatContext_get_Expression_m8461_MethodInfo,
	&RepeatContext_get_Previous_m8462_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo RepeatContext_t2085____start_0_FieldInfo = 
{
	"start"/* name */
	, &Int32_t189_0_0_1/* type */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, offsetof(RepeatContext_t2085, ___start_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo RepeatContext_t2085____min_1_FieldInfo = 
{
	"min"/* name */
	, &Int32_t189_0_0_1/* type */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, offsetof(RepeatContext_t2085, ___min_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo RepeatContext_t2085____max_2_FieldInfo = 
{
	"max"/* name */
	, &Int32_t189_0_0_1/* type */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, offsetof(RepeatContext_t2085, ___max_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo RepeatContext_t2085____lazy_3_FieldInfo = 
{
	"lazy"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, offsetof(RepeatContext_t2085, ___lazy_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo RepeatContext_t2085____expr_pc_4_FieldInfo = 
{
	"expr_pc"/* name */
	, &Int32_t189_0_0_1/* type */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, offsetof(RepeatContext_t2085, ___expr_pc_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RepeatContext_t2085_0_0_1;
FieldInfo RepeatContext_t2085____previous_5_FieldInfo = 
{
	"previous"/* name */
	, &RepeatContext_t2085_0_0_1/* type */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, offsetof(RepeatContext_t2085, ___previous_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo RepeatContext_t2085____count_6_FieldInfo = 
{
	"count"/* name */
	, &Int32_t189_0_0_1/* type */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, offsetof(RepeatContext_t2085, ___count_6)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RepeatContext_t2085_FieldInfos[] =
{
	&RepeatContext_t2085____start_0_FieldInfo,
	&RepeatContext_t2085____min_1_FieldInfo,
	&RepeatContext_t2085____max_2_FieldInfo,
	&RepeatContext_t2085____lazy_3_FieldInfo,
	&RepeatContext_t2085____expr_pc_4_FieldInfo,
	&RepeatContext_t2085____previous_5_FieldInfo,
	&RepeatContext_t2085____count_6_FieldInfo,
	NULL
};
extern MethodInfo RepeatContext_get_Count_m8454_MethodInfo;
extern MethodInfo RepeatContext_set_Count_m8455_MethodInfo;
static PropertyInfo RepeatContext_t2085____Count_PropertyInfo = 
{
	&RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &RepeatContext_get_Count_m8454_MethodInfo/* get */
	, &RepeatContext_set_Count_m8455_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo RepeatContext_get_Start_m8456_MethodInfo;
extern MethodInfo RepeatContext_set_Start_m8457_MethodInfo;
static PropertyInfo RepeatContext_t2085____Start_PropertyInfo = 
{
	&RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, "Start"/* name */
	, &RepeatContext_get_Start_m8456_MethodInfo/* get */
	, &RepeatContext_set_Start_m8457_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo RepeatContext_get_IsMinimum_m8458_MethodInfo;
static PropertyInfo RepeatContext_t2085____IsMinimum_PropertyInfo = 
{
	&RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, "IsMinimum"/* name */
	, &RepeatContext_get_IsMinimum_m8458_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo RepeatContext_get_IsMaximum_m8459_MethodInfo;
static PropertyInfo RepeatContext_t2085____IsMaximum_PropertyInfo = 
{
	&RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, "IsMaximum"/* name */
	, &RepeatContext_get_IsMaximum_m8459_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo RepeatContext_get_IsLazy_m8460_MethodInfo;
static PropertyInfo RepeatContext_t2085____IsLazy_PropertyInfo = 
{
	&RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, "IsLazy"/* name */
	, &RepeatContext_get_IsLazy_m8460_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo RepeatContext_get_Expression_m8461_MethodInfo;
static PropertyInfo RepeatContext_t2085____Expression_PropertyInfo = 
{
	&RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &RepeatContext_get_Expression_m8461_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo RepeatContext_get_Previous_m8462_MethodInfo;
static PropertyInfo RepeatContext_t2085____Previous_PropertyInfo = 
{
	&RepeatContext_t2085_il2cpp_TypeInfo/* parent */
	, "Previous"/* name */
	, &RepeatContext_get_Previous_m8462_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* RepeatContext_t2085_PropertyInfos[] =
{
	&RepeatContext_t2085____Count_PropertyInfo,
	&RepeatContext_t2085____Start_PropertyInfo,
	&RepeatContext_t2085____IsMinimum_PropertyInfo,
	&RepeatContext_t2085____IsMaximum_PropertyInfo,
	&RepeatContext_t2085____IsLazy_PropertyInfo,
	&RepeatContext_t2085____Expression_PropertyInfo,
	&RepeatContext_t2085____Previous_PropertyInfo,
	NULL
};
static Il2CppMethodReference RepeatContext_t2085_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool RepeatContext_t2085_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType RepeatContext_t2085_1_0_0;
struct RepeatContext_t2085;
const Il2CppTypeDefinitionMetadata RepeatContext_t2085_DefinitionMetadata = 
{
	&Interpreter_t2089_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RepeatContext_t2085_VTable/* vtableMethods */
	, RepeatContext_t2085_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RepeatContext_t2085_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RepeatContext"/* name */
	, ""/* namespaze */
	, RepeatContext_t2085_MethodInfos/* methods */
	, RepeatContext_t2085_PropertyInfos/* properties */
	, RepeatContext_t2085_FieldInfos/* fields */
	, NULL/* events */
	, &RepeatContext_t2085_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RepeatContext_t2085_0_0_0/* byval_arg */
	, &RepeatContext_t2085_1_0_0/* this_arg */
	, &RepeatContext_t2085_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RepeatContext_t2085)/* instance_size */
	, sizeof (RepeatContext_t2085)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_Mode.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/Mode
extern TypeInfo Mode_t2086_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_ModeMethodDeclarations.h"
static MethodInfo* Mode_t2086_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Mode_t2086____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Mode_t2086_il2cpp_TypeInfo/* parent */
	, offsetof(Mode_t2086, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Mode_t2086_0_0_32854;
FieldInfo Mode_t2086____Search_2_FieldInfo = 
{
	"Search"/* name */
	, &Mode_t2086_0_0_32854/* type */
	, &Mode_t2086_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Mode_t2086_0_0_32854;
FieldInfo Mode_t2086____Match_3_FieldInfo = 
{
	"Match"/* name */
	, &Mode_t2086_0_0_32854/* type */
	, &Mode_t2086_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Mode_t2086_0_0_32854;
FieldInfo Mode_t2086____Count_4_FieldInfo = 
{
	"Count"/* name */
	, &Mode_t2086_0_0_32854/* type */
	, &Mode_t2086_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Mode_t2086_FieldInfos[] =
{
	&Mode_t2086____value___1_FieldInfo,
	&Mode_t2086____Search_2_FieldInfo,
	&Mode_t2086____Match_3_FieldInfo,
	&Mode_t2086____Count_4_FieldInfo,
	NULL
};
static const int32_t Mode_t2086____Search_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Mode_t2086____Search_2_DefaultValue = 
{
	&Mode_t2086____Search_2_FieldInfo/* field */
	, { (char*)&Mode_t2086____Search_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Mode_t2086____Match_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Mode_t2086____Match_3_DefaultValue = 
{
	&Mode_t2086____Match_3_FieldInfo/* field */
	, { (char*)&Mode_t2086____Match_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Mode_t2086____Count_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry Mode_t2086____Count_4_DefaultValue = 
{
	&Mode_t2086____Count_4_FieldInfo/* field */
	, { (char*)&Mode_t2086____Count_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Mode_t2086_FieldDefaultValues[] = 
{
	&Mode_t2086____Search_2_DefaultValue,
	&Mode_t2086____Match_3_DefaultValue,
	&Mode_t2086____Count_4_DefaultValue,
	NULL
};
static Il2CppMethodReference Mode_t2086_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Mode_t2086_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Mode_t2086_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Mode_t2086_0_0_0;
extern Il2CppType Mode_t2086_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t189_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Mode_t2086_DefinitionMetadata = 
{
	&Interpreter_t2089_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t2086_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Mode_t2086_VTable/* vtableMethods */
	, Mode_t2086_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Mode_t2086_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, Mode_t2086_MethodInfos/* methods */
	, NULL/* properties */
	, Mode_t2086_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mode_t2086_0_0_0/* byval_arg */
	, &Mode_t2086_1_0_0/* this_arg */
	, &Mode_t2086_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Mode_t2086_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t2086)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t2086)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_Interpreter.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_InterpreterMethodDeclarations.h"
extern Il2CppType UInt16U5BU5D_t195_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter__ctor_m8463_ParameterInfos[] = 
{
	{"program", 0, 134218319, 0, &UInt16U5BU5D_t195_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::.ctor(System.UInt16[])
MethodInfo Interpreter__ctor_m8463_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interpreter__ctor_m8463/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Interpreter_t2089_Interpreter__ctor_m8463_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_ReadProgramCount_m8464_ParameterInfos[] = 
{
	{"ptr", 0, 134218320, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::ReadProgramCount(System.Int32)
MethodInfo Interpreter_ReadProgramCount_m8464_MethodInfo = 
{
	"ReadProgramCount"/* name */
	, (methodPointerType)&Interpreter_ReadProgramCount_m8464/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_ReadProgramCount_m8464_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Regex_t200_0_0_0;
extern Il2CppType Regex_t200_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_Scan_m8465_ParameterInfos[] = 
{
	{"regex", 0, 134218321, 0, &Regex_t200_0_0_0},
	{"text", 1, 134218322, 0, &String_t_0_0_0},
	{"start", 2, 134218323, 0, &Int32_t189_0_0_0},
	{"end", 3, 134218324, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Match_t2063_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::Scan(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32)
MethodInfo Interpreter_Scan_m8465_MethodInfo = 
{
	"Scan"/* name */
	, (methodPointerType)&Interpreter_Scan_m8465/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Match_t2063_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_Scan_m8465_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Reset()
MethodInfo Interpreter_Reset_m8466_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Interpreter_Reset_m8466/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Mode_t2086_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_Eval_m8467_ParameterInfos[] = 
{
	{"mode", 0, 134218325, 0, &Mode_t2086_0_0_0},
	{"ref_ptr", 1, 134218326, 0, &Int32_t189_1_0_0},
	{"pc", 2, 134218327, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Eval(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32)
MethodInfo Interpreter_Eval_m8467_MethodInfo = 
{
	"Eval"/* name */
	, (methodPointerType)&Interpreter_Eval_m8467/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_Eval_m8467_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Mode_t2086_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_EvalChar_m8468_ParameterInfos[] = 
{
	{"mode", 0, 134218328, 0, &Mode_t2086_0_0_0},
	{"ptr", 1, 134218329, 0, &Int32_t189_1_0_0},
	{"pc", 2, 134218330, 0, &Int32_t189_1_0_0},
	{"multi", 3, 134218331, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317_Int32U26_t317_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::EvalChar(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32&,System.Boolean)
MethodInfo Interpreter_EvalChar_m8468_MethodInfo = 
{
	"EvalChar"/* name */
	, (methodPointerType)&Interpreter_EvalChar_m8468/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317_Int32U26_t317_SByte_t236/* invoker_method */
	, Interpreter_t2089_Interpreter_EvalChar_m8468_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_TryMatch_m8469_ParameterInfos[] = 
{
	{"ref_ptr", 0, 134218332, 0, &Int32_t189_1_0_0},
	{"pc", 1, 134218333, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::TryMatch(System.Int32&,System.Int32)
MethodInfo Interpreter_TryMatch_m8469_MethodInfo = 
{
	"TryMatch"/* name */
	, (methodPointerType)&Interpreter_TryMatch_m8469/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_TryMatch_m8469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Position_t2071_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_IsPosition_m8470_ParameterInfos[] = 
{
	{"pos", 0, 134218334, 0, &Position_t2071_0_0_0},
	{"ptr", 1, 134218335, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_UInt16_t194_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsPosition(System.Text.RegularExpressions.Position,System.Int32)
MethodInfo Interpreter_IsPosition_m8470_MethodInfo = 
{
	"IsPosition"/* name */
	, (methodPointerType)&Interpreter_IsPosition_m8470/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_UInt16_t194_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_IsPosition_m8470_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_IsWordChar_m8471_ParameterInfos[] = 
{
	{"c", 0, 134218336, 0, &Char_t193_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsWordChar(System.Char)
MethodInfo Interpreter_IsWordChar_m8471_MethodInfo = 
{
	"IsWordChar"/* name */
	, (methodPointerType)&Interpreter_IsWordChar_m8471/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int16_t238/* invoker_method */
	, Interpreter_t2089_Interpreter_IsWordChar_m8471_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_GetString_m8472_ParameterInfos[] = 
{
	{"pc", 0, 134218337, 0, &Int32_t189_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Interpreter::GetString(System.Int32)
MethodInfo Interpreter_GetString_m8472_MethodInfo = 
{
	"GetString"/* name */
	, (methodPointerType)&Interpreter_GetString_m8472/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_GetString_m8472_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_Open_m8473_ParameterInfos[] = 
{
	{"gid", 0, 134218338, 0, &Int32_t189_0_0_0},
	{"ptr", 1, 134218339, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Open(System.Int32,System.Int32)
MethodInfo Interpreter_Open_m8473_MethodInfo = 
{
	"Open"/* name */
	, (methodPointerType)&Interpreter_Open_m8473/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_Open_m8473_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_Close_m8474_ParameterInfos[] = 
{
	{"gid", 0, 134218340, 0, &Int32_t189_0_0_0},
	{"ptr", 1, 134218341, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Close(System.Int32,System.Int32)
MethodInfo Interpreter_Close_m8474_MethodInfo = 
{
	"Close"/* name */
	, (methodPointerType)&Interpreter_Close_m8474/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_Close_m8474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_Balance_m8475_ParameterInfos[] = 
{
	{"gid", 0, 134218342, 0, &Int32_t189_0_0_0},
	{"balance_gid", 1, 134218343, 0, &Int32_t189_0_0_0},
	{"capture", 2, 134218344, 0, &Boolean_t203_0_0_0},
	{"ptr", 3, 134218345, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Balance(System.Int32,System.Int32,System.Boolean,System.Int32)
MethodInfo Interpreter_Balance_m8475_MethodInfo = 
{
	"Balance"/* name */
	, (methodPointerType)&Interpreter_Balance_m8475/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_SByte_t236_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_Balance_m8475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::Checkpoint()
MethodInfo Interpreter_Checkpoint_m8476_MethodInfo = 
{
	"Checkpoint"/* name */
	, (methodPointerType)&Interpreter_Checkpoint_m8476/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_Backtrack_m8477_ParameterInfos[] = 
{
	{"cp", 0, 134218346, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Backtrack(System.Int32)
MethodInfo Interpreter_Backtrack_m8477_MethodInfo = 
{
	"Backtrack"/* name */
	, (methodPointerType)&Interpreter_Backtrack_m8477/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_Backtrack_m8477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::ResetGroups()
MethodInfo Interpreter_ResetGroups_m8478_MethodInfo = 
{
	"ResetGroups"/* name */
	, (methodPointerType)&Interpreter_ResetGroups_m8478/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_GetLastDefined_m8479_ParameterInfos[] = 
{
	{"gid", 0, 134218347, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::GetLastDefined(System.Int32)
MethodInfo Interpreter_GetLastDefined_m8479_MethodInfo = 
{
	"GetLastDefined"/* name */
	, (methodPointerType)&Interpreter_GetLastDefined_m8479/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_GetLastDefined_m8479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_CreateMark_m8480_ParameterInfos[] = 
{
	{"previous", 0, 134218348, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::CreateMark(System.Int32)
MethodInfo Interpreter_CreateMark_m8480_MethodInfo = 
{
	"CreateMark"/* name */
	, (methodPointerType)&Interpreter_CreateMark_m8480/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_CreateMark_m8480_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Interpreter_t2089_Interpreter_GetGroupInfo_m8481_ParameterInfos[] = 
{
	{"gid", 0, 134218349, 0, &Int32_t189_0_0_0},
	{"first_mark_index", 1, 134218350, 0, &Int32_t189_1_0_2},
	{"n_caps", 2, 134218351, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::GetGroupInfo(System.Int32,System.Int32&,System.Int32&)
MethodInfo Interpreter_GetGroupInfo_m8481_MethodInfo = 
{
	"GetGroupInfo"/* name */
	, (methodPointerType)&Interpreter_GetGroupInfo_m8481/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Interpreter_t2089_Interpreter_GetGroupInfo_m8481_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Group_t2059_0_0_0;
extern Il2CppType Group_t2059_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_PopulateGroup_m8482_ParameterInfos[] = 
{
	{"g", 0, 134218352, 0, &Group_t2059_0_0_0},
	{"first_mark_index", 1, 134218353, 0, &Int32_t189_0_0_0},
	{"n_caps", 2, 134218354, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::PopulateGroup(System.Text.RegularExpressions.Group,System.Int32,System.Int32)
MethodInfo Interpreter_PopulateGroup_m8482_MethodInfo = 
{
	"PopulateGroup"/* name */
	, (methodPointerType)&Interpreter_PopulateGroup_m8482/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189/* invoker_method */
	, Interpreter_t2089_Interpreter_PopulateGroup_m8482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Regex_t200_0_0_0;
static ParameterInfo Interpreter_t2089_Interpreter_GenerateMatch_m8483_ParameterInfos[] = 
{
	{"regex", 0, 134218355, 0, &Regex_t200_0_0_0},
};
extern Il2CppType Match_t2063_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::GenerateMatch(System.Text.RegularExpressions.Regex)
MethodInfo Interpreter_GenerateMatch_m8483_MethodInfo = 
{
	"GenerateMatch"/* name */
	, (methodPointerType)&Interpreter_GenerateMatch_m8483/* method */
	, &Interpreter_t2089_il2cpp_TypeInfo/* declaring_type */
	, &Match_t2063_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Interpreter_t2089_Interpreter_GenerateMatch_m8483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Interpreter_t2089_MethodInfos[] =
{
	&Interpreter__ctor_m8463_MethodInfo,
	&Interpreter_ReadProgramCount_m8464_MethodInfo,
	&Interpreter_Scan_m8465_MethodInfo,
	&Interpreter_Reset_m8466_MethodInfo,
	&Interpreter_Eval_m8467_MethodInfo,
	&Interpreter_EvalChar_m8468_MethodInfo,
	&Interpreter_TryMatch_m8469_MethodInfo,
	&Interpreter_IsPosition_m8470_MethodInfo,
	&Interpreter_IsWordChar_m8471_MethodInfo,
	&Interpreter_GetString_m8472_MethodInfo,
	&Interpreter_Open_m8473_MethodInfo,
	&Interpreter_Close_m8474_MethodInfo,
	&Interpreter_Balance_m8475_MethodInfo,
	&Interpreter_Checkpoint_m8476_MethodInfo,
	&Interpreter_Backtrack_m8477_MethodInfo,
	&Interpreter_ResetGroups_m8478_MethodInfo,
	&Interpreter_GetLastDefined_m8479_MethodInfo,
	&Interpreter_CreateMark_m8480_MethodInfo,
	&Interpreter_GetGroupInfo_m8481_MethodInfo,
	&Interpreter_PopulateGroup_m8482_MethodInfo,
	&Interpreter_GenerateMatch_m8483_MethodInfo,
	NULL
};
extern Il2CppType UInt16U5BU5D_t195_0_0_1;
FieldInfo Interpreter_t2089____program_1_FieldInfo = 
{
	"program"/* name */
	, &UInt16U5BU5D_t195_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___program_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Interpreter_t2089____program_start_2_FieldInfo = 
{
	"program_start"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___program_start_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Interpreter_t2089____text_3_FieldInfo = 
{
	"text"/* name */
	, &String_t_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___text_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Interpreter_t2089____text_end_4_FieldInfo = 
{
	"text_end"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___text_end_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Interpreter_t2089____group_count_5_FieldInfo = 
{
	"group_count"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___group_count_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Interpreter_t2089____match_min_6_FieldInfo = 
{
	"match_min"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___match_min_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType QuickSearch_t2087_0_0_1;
FieldInfo Interpreter_t2089____qs_7_FieldInfo = 
{
	"qs"/* name */
	, &QuickSearch_t2087_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___qs_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Interpreter_t2089____scan_ptr_8_FieldInfo = 
{
	"scan_ptr"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___scan_ptr_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RepeatContext_t2085_0_0_1;
FieldInfo Interpreter_t2089____repeat_9_FieldInfo = 
{
	"repeat"/* name */
	, &RepeatContext_t2085_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___repeat_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RepeatContext_t2085_0_0_1;
FieldInfo Interpreter_t2089____fast_10_FieldInfo = 
{
	"fast"/* name */
	, &RepeatContext_t2085_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___fast_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IntStack_t2084_0_0_1;
FieldInfo Interpreter_t2089____stack_11_FieldInfo = 
{
	"stack"/* name */
	, &IntStack_t2084_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___stack_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RepeatContext_t2085_0_0_1;
FieldInfo Interpreter_t2089____deep_12_FieldInfo = 
{
	"deep"/* name */
	, &RepeatContext_t2085_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___deep_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MarkU5BU5D_t2088_0_0_1;
FieldInfo Interpreter_t2089____marks_13_FieldInfo = 
{
	"marks"/* name */
	, &MarkU5BU5D_t2088_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___marks_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Interpreter_t2089____mark_start_14_FieldInfo = 
{
	"mark_start"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___mark_start_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Interpreter_t2089____mark_end_15_FieldInfo = 
{
	"mark_end"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___mark_end_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32U5BU5D_t107_0_0_1;
FieldInfo Interpreter_t2089____groups_16_FieldInfo = 
{
	"groups"/* name */
	, &Int32U5BU5D_t107_0_0_1/* type */
	, &Interpreter_t2089_il2cpp_TypeInfo/* parent */
	, offsetof(Interpreter_t2089, ___groups_16)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Interpreter_t2089_FieldInfos[] =
{
	&Interpreter_t2089____program_1_FieldInfo,
	&Interpreter_t2089____program_start_2_FieldInfo,
	&Interpreter_t2089____text_3_FieldInfo,
	&Interpreter_t2089____text_end_4_FieldInfo,
	&Interpreter_t2089____group_count_5_FieldInfo,
	&Interpreter_t2089____match_min_6_FieldInfo,
	&Interpreter_t2089____qs_7_FieldInfo,
	&Interpreter_t2089____scan_ptr_8_FieldInfo,
	&Interpreter_t2089____repeat_9_FieldInfo,
	&Interpreter_t2089____fast_10_FieldInfo,
	&Interpreter_t2089____stack_11_FieldInfo,
	&Interpreter_t2089____deep_12_FieldInfo,
	&Interpreter_t2089____marks_13_FieldInfo,
	&Interpreter_t2089____mark_start_14_FieldInfo,
	&Interpreter_t2089____mark_end_15_FieldInfo,
	&Interpreter_t2089____groups_16_FieldInfo,
	NULL
};
static const Il2CppType* Interpreter_t2089_il2cpp_TypeInfo__nestedTypes[3] =
{
	&IntStack_t2084_0_0_0,
	&RepeatContext_t2085_0_0_0,
	&Mode_t2086_0_0_0,
};
extern MethodInfo Interpreter_Scan_m8465_MethodInfo;
static Il2CppMethodReference Interpreter_t2089_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Interpreter_Scan_m8465_MethodInfo,
	&Interpreter_Scan_m8465_MethodInfo,
};
static bool Interpreter_t2089_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IMachine_t2062_0_0_0;
static Il2CppInterfaceOffsetPair Interpreter_t2089_InterfacesOffsets[] = 
{
	{ &IMachine_t2062_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Interpreter_t2089_1_0_0;
extern Il2CppType BaseMachine_t2055_0_0_0;
struct Interpreter_t2089;
const Il2CppTypeDefinitionMetadata Interpreter_t2089_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Interpreter_t2089_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Interpreter_t2089_InterfacesOffsets/* interfaceOffsets */
	, &BaseMachine_t2055_0_0_0/* parent */
	, Interpreter_t2089_VTable/* vtableMethods */
	, Interpreter_t2089_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Interpreter_t2089_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interpreter"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interpreter_t2089_MethodInfos/* methods */
	, NULL/* properties */
	, Interpreter_t2089_FieldInfos/* fields */
	, NULL/* events */
	, &Interpreter_t2089_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interpreter_t2089_0_0_0/* byval_arg */
	, &Interpreter_t2089_1_0_0/* this_arg */
	, &Interpreter_t2089_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Interpreter_t2089)/* instance_size */
	, sizeof (Interpreter_t2089)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// Metadata Definition System.Text.RegularExpressions.Interval
extern TypeInfo Interval_t2090_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_IntervalMethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interval_t2090_Interval__ctor_m8484_ParameterInfos[] = 
{
	{"low", 0, 134218365, 0, &Int32_t189_0_0_0},
	{"high", 1, 134218366, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::.ctor(System.Int32,System.Int32)
MethodInfo Interval__ctor_m8484_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interval__ctor_m8484/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189/* invoker_method */
	, Interval_t2090_Interval__ctor_m8484_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
extern void* RuntimeInvoker_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Interval::get_Empty()
MethodInfo Interval_get_Empty_m8485_MethodInfo = 
{
	"get_Empty"/* name */
	, (methodPointerType)&Interval_get_Empty_m8485/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t2090_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t2090/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsDiscontiguous()
MethodInfo Interval_get_IsDiscontiguous_m8486_MethodInfo = 
{
	"get_IsDiscontiguous"/* name */
	, (methodPointerType)&Interval_get_IsDiscontiguous_m8486/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsSingleton()
MethodInfo Interval_get_IsSingleton_m8487_MethodInfo = 
{
	"get_IsSingleton"/* name */
	, (methodPointerType)&Interval_get_IsSingleton_m8487/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsEmpty()
MethodInfo Interval_get_IsEmpty_m8488_MethodInfo = 
{
	"get_IsEmpty"/* name */
	, (methodPointerType)&Interval_get_IsEmpty_m8488/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::get_Size()
MethodInfo Interval_get_Size_m8489_MethodInfo = 
{
	"get_Size"/* name */
	, (methodPointerType)&Interval_get_Size_m8489/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo Interval_t2090_Interval_IsDisjoint_m8490_ParameterInfos[] = 
{
	{"i", 0, 134218367, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsDisjoint(System.Text.RegularExpressions.Interval)
MethodInfo Interval_IsDisjoint_m8490_MethodInfo = 
{
	"IsDisjoint"/* name */
	, (methodPointerType)&Interval_IsDisjoint_m8490/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Interval_t2090/* invoker_method */
	, Interval_t2090_Interval_IsDisjoint_m8490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo Interval_t2090_Interval_IsAdjacent_m8491_ParameterInfos[] = 
{
	{"i", 0, 134218368, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsAdjacent(System.Text.RegularExpressions.Interval)
MethodInfo Interval_IsAdjacent_m8491_MethodInfo = 
{
	"IsAdjacent"/* name */
	, (methodPointerType)&Interval_IsAdjacent_m8491/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Interval_t2090/* invoker_method */
	, Interval_t2090_Interval_IsAdjacent_m8491_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo Interval_t2090_Interval_Contains_m8492_ParameterInfos[] = 
{
	{"i", 0, 134218369, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Text.RegularExpressions.Interval)
MethodInfo Interval_Contains_m8492_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m8492/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Interval_t2090/* invoker_method */
	, Interval_t2090_Interval_Contains_m8492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Interval_t2090_Interval_Contains_m8493_ParameterInfos[] = 
{
	{"i", 0, 134218370, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Int32)
MethodInfo Interval_Contains_m8493_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m8493/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189/* invoker_method */
	, Interval_t2090_Interval_Contains_m8493_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo Interval_t2090_Interval_Intersects_m8494_ParameterInfos[] = 
{
	{"i", 0, 134218371, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Intersects(System.Text.RegularExpressions.Interval)
MethodInfo Interval_Intersects_m8494_MethodInfo = 
{
	"Intersects"/* name */
	, (methodPointerType)&Interval_Intersects_m8494/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Interval_t2090/* invoker_method */
	, Interval_t2090_Interval_Intersects_m8494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo Interval_t2090_Interval_Merge_m8495_ParameterInfos[] = 
{
	{"i", 0, 134218372, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::Merge(System.Text.RegularExpressions.Interval)
MethodInfo Interval_Merge_m8495_MethodInfo = 
{
	"Merge"/* name */
	, (methodPointerType)&Interval_Merge_m8495/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Interval_t2090/* invoker_method */
	, Interval_t2090_Interval_Merge_m8495_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Interval_t2090_Interval_CompareTo_m8496_ParameterInfos[] = 
{
	{"o", 0, 134218373, 0, &Object_t_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::CompareTo(System.Object)
MethodInfo Interval_CompareTo_m8496_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Interval_CompareTo_m8496/* method */
	, &Interval_t2090_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t/* invoker_method */
	, Interval_t2090_Interval_CompareTo_m8496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Interval_t2090_MethodInfos[] =
{
	&Interval__ctor_m8484_MethodInfo,
	&Interval_get_Empty_m8485_MethodInfo,
	&Interval_get_IsDiscontiguous_m8486_MethodInfo,
	&Interval_get_IsSingleton_m8487_MethodInfo,
	&Interval_get_IsEmpty_m8488_MethodInfo,
	&Interval_get_Size_m8489_MethodInfo,
	&Interval_IsDisjoint_m8490_MethodInfo,
	&Interval_IsAdjacent_m8491_MethodInfo,
	&Interval_Contains_m8492_MethodInfo,
	&Interval_Contains_m8493_MethodInfo,
	&Interval_Intersects_m8494_MethodInfo,
	&Interval_Merge_m8495_MethodInfo,
	&Interval_CompareTo_m8496_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Interval_t2090____low_0_FieldInfo = 
{
	"low"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Interval_t2090_il2cpp_TypeInfo/* parent */
	, offsetof(Interval_t2090, ___low_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Interval_t2090____high_1_FieldInfo = 
{
	"high"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Interval_t2090_il2cpp_TypeInfo/* parent */
	, offsetof(Interval_t2090, ___high_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo Interval_t2090____contiguous_2_FieldInfo = 
{
	"contiguous"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &Interval_t2090_il2cpp_TypeInfo/* parent */
	, offsetof(Interval_t2090, ___contiguous_2) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Interval_t2090_FieldInfos[] =
{
	&Interval_t2090____low_0_FieldInfo,
	&Interval_t2090____high_1_FieldInfo,
	&Interval_t2090____contiguous_2_FieldInfo,
	NULL
};
extern MethodInfo Interval_get_Empty_m8485_MethodInfo;
static PropertyInfo Interval_t2090____Empty_PropertyInfo = 
{
	&Interval_t2090_il2cpp_TypeInfo/* parent */
	, "Empty"/* name */
	, &Interval_get_Empty_m8485_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Interval_get_IsDiscontiguous_m8486_MethodInfo;
static PropertyInfo Interval_t2090____IsDiscontiguous_PropertyInfo = 
{
	&Interval_t2090_il2cpp_TypeInfo/* parent */
	, "IsDiscontiguous"/* name */
	, &Interval_get_IsDiscontiguous_m8486_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Interval_get_IsSingleton_m8487_MethodInfo;
static PropertyInfo Interval_t2090____IsSingleton_PropertyInfo = 
{
	&Interval_t2090_il2cpp_TypeInfo/* parent */
	, "IsSingleton"/* name */
	, &Interval_get_IsSingleton_m8487_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Interval_get_IsEmpty_m8488_MethodInfo;
static PropertyInfo Interval_t2090____IsEmpty_PropertyInfo = 
{
	&Interval_t2090_il2cpp_TypeInfo/* parent */
	, "IsEmpty"/* name */
	, &Interval_get_IsEmpty_m8488_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Interval_get_Size_m8489_MethodInfo;
static PropertyInfo Interval_t2090____Size_PropertyInfo = 
{
	&Interval_t2090_il2cpp_TypeInfo/* parent */
	, "Size"/* name */
	, &Interval_get_Size_m8489_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Interval_t2090_PropertyInfos[] =
{
	&Interval_t2090____Empty_PropertyInfo,
	&Interval_t2090____IsDiscontiguous_PropertyInfo,
	&Interval_t2090____IsSingleton_PropertyInfo,
	&Interval_t2090____IsEmpty_PropertyInfo,
	&Interval_t2090____Size_PropertyInfo,
	NULL
};
extern MethodInfo Interval_CompareTo_m8496_MethodInfo;
static Il2CppMethodReference Interval_t2090_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
	&Interval_CompareTo_m8496_MethodInfo,
};
static bool Interval_t2090_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Interval_t2090_InterfacesTypeInfos[] = 
{
	&IComparable_t314_0_0_0,
};
static Il2CppInterfaceOffsetPair Interval_t2090_InterfacesOffsets[] = 
{
	{ &IComparable_t314_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Interval_t2090_1_0_0;
const Il2CppTypeDefinitionMetadata Interval_t2090_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Interval_t2090_InterfacesTypeInfos/* implementedInterfaces */
	, Interval_t2090_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, Interval_t2090_VTable/* vtableMethods */
	, Interval_t2090_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Interval_t2090_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interval"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interval_t2090_MethodInfos/* methods */
	, Interval_t2090_PropertyInfos/* properties */
	, Interval_t2090_FieldInfos/* fields */
	, NULL/* events */
	, &Interval_t2090_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interval_t2090_0_0_0/* byval_arg */
	, &Interval_t2090_1_0_0/* this_arg */
	, &Interval_t2090_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Interval_t2090_marshal/* marshal_to_native_func */
	, (methodPointerType)Interval_t2090_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Interval_t2090_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Interval_t2090)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Interval_t2090)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Interval_t2090_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_Enu.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/Enumerator
extern TypeInfo Enumerator_t2091_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_EnuMethodDeclarations.h"
extern Il2CppType IList_t183_0_0_0;
extern Il2CppType IList_t183_0_0_0;
static ParameterInfo Enumerator_t2091_Enumerator__ctor_m8497_ParameterInfos[] = 
{
	{"list", 0, 134218383, 0, &IList_t183_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::.ctor(System.Collections.IList)
MethodInfo Enumerator__ctor_m8497_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m8497/* method */
	, &Enumerator_t2091_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Enumerator_t2091_Enumerator__ctor_m8497_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection/Enumerator::get_Current()
MethodInfo Enumerator_get_Current_m8498_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m8498/* method */
	, &Enumerator_t2091_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection/Enumerator::MoveNext()
MethodInfo Enumerator_MoveNext_m8499_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m8499/* method */
	, &Enumerator_t2091_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::Reset()
MethodInfo Enumerator_Reset_m8500_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Enumerator_Reset_m8500/* method */
	, &Enumerator_t2091_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Enumerator_t2091_MethodInfos[] =
{
	&Enumerator__ctor_m8497_MethodInfo,
	&Enumerator_get_Current_m8498_MethodInfo,
	&Enumerator_MoveNext_m8499_MethodInfo,
	&Enumerator_Reset_m8500_MethodInfo,
	NULL
};
extern Il2CppType IList_t183_0_0_1;
FieldInfo Enumerator_t2091____list_0_FieldInfo = 
{
	"list"/* name */
	, &IList_t183_0_0_1/* type */
	, &Enumerator_t2091_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t2091, ___list_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Enumerator_t2091____ptr_1_FieldInfo = 
{
	"ptr"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Enumerator_t2091_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t2091, ___ptr_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t2091_FieldInfos[] =
{
	&Enumerator_t2091____list_0_FieldInfo,
	&Enumerator_t2091____ptr_1_FieldInfo,
	NULL
};
extern MethodInfo Enumerator_get_Current_m8498_MethodInfo;
static PropertyInfo Enumerator_t2091____Current_PropertyInfo = 
{
	&Enumerator_t2091_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m8498_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t2091_PropertyInfos[] =
{
	&Enumerator_t2091____Current_PropertyInfo,
	NULL
};
extern MethodInfo Enumerator_MoveNext_m8499_MethodInfo;
extern MethodInfo Enumerator_Reset_m8500_MethodInfo;
static Il2CppMethodReference Enumerator_t2091_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Enumerator_get_Current_m8498_MethodInfo,
	&Enumerator_MoveNext_m8499_MethodInfo,
	&Enumerator_Reset_m8500_MethodInfo,
};
static bool Enumerator_t2091_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerator_t37_0_0_0;
static const Il2CppType* Enumerator_t2091_InterfacesTypeInfos[] = 
{
	&IEnumerator_t37_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t2091_InterfacesOffsets[] = 
{
	{ &IEnumerator_t37_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Enumerator_t2091_0_0_0;
extern Il2CppType Enumerator_t2091_1_0_0;
extern TypeInfo IntervalCollection_t2093_il2cpp_TypeInfo;
extern Il2CppType IntervalCollection_t2093_0_0_0;
struct Enumerator_t2091;
const Il2CppTypeDefinitionMetadata Enumerator_t2091_DefinitionMetadata = 
{
	&IntervalCollection_t2093_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t2091_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t2091_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t2091_VTable/* vtableMethods */
	, Enumerator_t2091_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Enumerator_t2091_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t2091_MethodInfos/* methods */
	, Enumerator_t2091_PropertyInfos/* properties */
	, Enumerator_t2091_FieldInfos/* fields */
	, NULL/* events */
	, &Enumerator_t2091_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t2091_0_0_0/* byval_arg */
	, &Enumerator_t2091_1_0_0/* this_arg */
	, &Enumerator_t2091_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t2091)/* instance_size */
	, sizeof (Enumerator_t2091)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_Cos.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/CostDelegate
extern TypeInfo CostDelegate_t2092_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_CosMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo CostDelegate_t2092_CostDelegate__ctor_m8501_ParameterInfos[] = 
{
	{"object", 0, 134218384, 0, &Object_t_0_0_0},
	{"method", 1, 134218385, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/CostDelegate::.ctor(System.Object,System.IntPtr)
MethodInfo CostDelegate__ctor_m8501_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CostDelegate__ctor_m8501/* method */
	, &CostDelegate_t2092_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, CostDelegate_t2092_CostDelegate__ctor_m8501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo CostDelegate_t2092_CostDelegate_Invoke_m8502_ParameterInfos[] = 
{
	{"i", 0, 134218386, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Double_t234_0_0_0;
extern void* RuntimeInvoker_Double_t234_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::Invoke(System.Text.RegularExpressions.Interval)
MethodInfo CostDelegate_Invoke_m8502_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CostDelegate_Invoke_m8502/* method */
	, &CostDelegate_t2092_il2cpp_TypeInfo/* declaring_type */
	, &Double_t234_0_0_0/* return_type */
	, RuntimeInvoker_Double_t234_Interval_t2090/* invoker_method */
	, CostDelegate_t2092_CostDelegate_Invoke_m8502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CostDelegate_t2092_CostDelegate_BeginInvoke_m8503_ParameterInfos[] = 
{
	{"i", 0, 134218387, 0, &Interval_t2090_0_0_0},
	{"callback", 1, 134218388, 0, &AsyncCallback_t20_0_0_0},
	{"object", 2, 134218389, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Interval_t2090_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Text.RegularExpressions.IntervalCollection/CostDelegate::BeginInvoke(System.Text.RegularExpressions.Interval,System.AsyncCallback,System.Object)
MethodInfo CostDelegate_BeginInvoke_m8503_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CostDelegate_BeginInvoke_m8503/* method */
	, &CostDelegate_t2092_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Interval_t2090_Object_t_Object_t/* invoker_method */
	, CostDelegate_t2092_CostDelegate_BeginInvoke_m8503_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo CostDelegate_t2092_CostDelegate_EndInvoke_m8504_ParameterInfos[] = 
{
	{"result", 0, 134218390, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Double_t234_0_0_0;
extern void* RuntimeInvoker_Double_t234_Object_t (MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::EndInvoke(System.IAsyncResult)
MethodInfo CostDelegate_EndInvoke_m8504_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CostDelegate_EndInvoke_m8504/* method */
	, &CostDelegate_t2092_il2cpp_TypeInfo/* declaring_type */
	, &Double_t234_0_0_0/* return_type */
	, RuntimeInvoker_Double_t234_Object_t/* invoker_method */
	, CostDelegate_t2092_CostDelegate_EndInvoke_m8504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CostDelegate_t2092_MethodInfos[] =
{
	&CostDelegate__ctor_m8501_MethodInfo,
	&CostDelegate_Invoke_m8502_MethodInfo,
	&CostDelegate_BeginInvoke_m8503_MethodInfo,
	&CostDelegate_EndInvoke_m8504_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m1272_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m1273_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m1274_MethodInfo;
extern MethodInfo Delegate_Clone_m1275_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m1276_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m1277_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m1278_MethodInfo;
extern MethodInfo CostDelegate_Invoke_m8502_MethodInfo;
extern MethodInfo CostDelegate_BeginInvoke_m8503_MethodInfo;
extern MethodInfo CostDelegate_EndInvoke_m8504_MethodInfo;
static Il2CppMethodReference CostDelegate_t2092_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&CostDelegate_Invoke_m8502_MethodInfo,
	&CostDelegate_BeginInvoke_m8503_MethodInfo,
	&CostDelegate_EndInvoke_m8504_MethodInfo,
};
static bool CostDelegate_t2092_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ICloneable_t309_0_0_0;
extern Il2CppType ISerializable_t310_0_0_0;
static Il2CppInterfaceOffsetPair CostDelegate_t2092_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType CostDelegate_t2092_0_0_0;
extern Il2CppType CostDelegate_t2092_1_0_0;
extern Il2CppType MulticastDelegate_t22_0_0_0;
struct CostDelegate_t2092;
const Il2CppTypeDefinitionMetadata CostDelegate_t2092_DefinitionMetadata = 
{
	&IntervalCollection_t2093_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CostDelegate_t2092_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, CostDelegate_t2092_VTable/* vtableMethods */
	, CostDelegate_t2092_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CostDelegate_t2092_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CostDelegate"/* name */
	, ""/* namespaze */
	, CostDelegate_t2092_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CostDelegate_t2092_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CostDelegate_t2092_0_0_0/* byval_arg */
	, &CostDelegate_t2092_1_0_0/* this_arg */
	, &CostDelegate_t2092_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CostDelegate_t2092/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CostDelegate_t2092)/* instance_size */
	, sizeof (CostDelegate_t2092)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollection.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollectionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::.ctor()
MethodInfo IntervalCollection__ctor_m8505_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IntervalCollection__ctor_m8505/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo IntervalCollection_t2093_IntervalCollection_get_Item_m8506_ParameterInfos[] = 
{
	{"i", 0, 134218374, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Interval_t2090_0_0_0;
extern void* RuntimeInvoker_Interval_t2090_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.IntervalCollection::get_Item(System.Int32)
MethodInfo IntervalCollection_get_Item_m8506_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&IntervalCollection_get_Item_m8506/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t2090_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t2090_Int32_t189/* invoker_method */
	, IntervalCollection_t2093_IntervalCollection_get_Item_m8506_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo IntervalCollection_t2093_IntervalCollection_Add_m8507_ParameterInfos[] = 
{
	{"i", 0, 134218375, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Add(System.Text.RegularExpressions.Interval)
MethodInfo IntervalCollection_Add_m8507_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&IntervalCollection_Add_m8507/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Interval_t2090/* invoker_method */
	, IntervalCollection_t2093_IntervalCollection_Add_m8507_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Normalize()
MethodInfo IntervalCollection_Normalize_m8508_MethodInfo = 
{
	"Normalize"/* name */
	, (methodPointerType)&IntervalCollection_Normalize_m8508/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CostDelegate_t2092_0_0_0;
static ParameterInfo IntervalCollection_t2093_IntervalCollection_GetMetaCollection_m8509_ParameterInfos[] = 
{
	{"cost_del", 0, 134218376, 0, &CostDelegate_t2092_0_0_0},
};
extern Il2CppType IntervalCollection_t2093_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IntervalCollection System.Text.RegularExpressions.IntervalCollection::GetMetaCollection(System.Text.RegularExpressions.IntervalCollection/CostDelegate)
MethodInfo IntervalCollection_GetMetaCollection_m8509_MethodInfo = 
{
	"GetMetaCollection"/* name */
	, (methodPointerType)&IntervalCollection_GetMetaCollection_m8509/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &IntervalCollection_t2093_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t2093_IntervalCollection_GetMetaCollection_m8509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType IntervalCollection_t2093_0_0_0;
extern Il2CppType CostDelegate_t2092_0_0_0;
static ParameterInfo IntervalCollection_t2093_IntervalCollection_Optimize_m8510_ParameterInfos[] = 
{
	{"begin", 0, 134218377, 0, &Int32_t189_0_0_0},
	{"end", 1, 134218378, 0, &Int32_t189_0_0_0},
	{"meta", 2, 134218379, 0, &IntervalCollection_t2093_0_0_0},
	{"cost_del", 3, 134218380, 0, &CostDelegate_t2092_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Optimize(System.Int32,System.Int32,System.Text.RegularExpressions.IntervalCollection,System.Text.RegularExpressions.IntervalCollection/CostDelegate)
MethodInfo IntervalCollection_Optimize_m8510_MethodInfo = 
{
	"Optimize"/* name */
	, (methodPointerType)&IntervalCollection_Optimize_m8510/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t2093_IntervalCollection_Optimize_m8510_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IntervalCollection::get_Count()
MethodInfo IntervalCollection_get_Count_m8511_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntervalCollection_get_Count_m8511/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection::get_IsSynchronized()
MethodInfo IntervalCollection_get_IsSynchronized_m8512_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&IntervalCollection_get_IsSynchronized_m8512/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection::get_SyncRoot()
MethodInfo IntervalCollection_get_SyncRoot_m8513_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&IntervalCollection_get_SyncRoot_m8513/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo IntervalCollection_t2093_IntervalCollection_CopyTo_m8514_ParameterInfos[] = 
{
	{"array", 0, 134218381, 0, &Array_t_0_0_0},
	{"index", 1, 134218382, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::CopyTo(System.Array,System.Int32)
MethodInfo IntervalCollection_CopyTo_m8514_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&IntervalCollection_CopyTo_m8514/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189/* invoker_method */
	, IntervalCollection_t2093_IntervalCollection_CopyTo_m8514_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Text.RegularExpressions.IntervalCollection::GetEnumerator()
MethodInfo IntervalCollection_GetEnumerator_m8515_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&IntervalCollection_GetEnumerator_m8515/* method */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IntervalCollection_t2093_MethodInfos[] =
{
	&IntervalCollection__ctor_m8505_MethodInfo,
	&IntervalCollection_get_Item_m8506_MethodInfo,
	&IntervalCollection_Add_m8507_MethodInfo,
	&IntervalCollection_Normalize_m8508_MethodInfo,
	&IntervalCollection_GetMetaCollection_m8509_MethodInfo,
	&IntervalCollection_Optimize_m8510_MethodInfo,
	&IntervalCollection_get_Count_m8511_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m8512_MethodInfo,
	&IntervalCollection_get_SyncRoot_m8513_MethodInfo,
	&IntervalCollection_CopyTo_m8514_MethodInfo,
	&IntervalCollection_GetEnumerator_m8515_MethodInfo,
	NULL
};
extern Il2CppType ArrayList_t737_0_0_1;
FieldInfo IntervalCollection_t2093____intervals_0_FieldInfo = 
{
	"intervals"/* name */
	, &ArrayList_t737_0_0_1/* type */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* parent */
	, offsetof(IntervalCollection_t2093, ___intervals_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* IntervalCollection_t2093_FieldInfos[] =
{
	&IntervalCollection_t2093____intervals_0_FieldInfo,
	NULL
};
extern MethodInfo IntervalCollection_get_Item_m8506_MethodInfo;
static PropertyInfo IntervalCollection_t2093____Item_PropertyInfo = 
{
	&IntervalCollection_t2093_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IntervalCollection_get_Item_m8506_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IntervalCollection_get_Count_m8511_MethodInfo;
static PropertyInfo IntervalCollection_t2093____Count_PropertyInfo = 
{
	&IntervalCollection_t2093_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntervalCollection_get_Count_m8511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IntervalCollection_get_IsSynchronized_m8512_MethodInfo;
static PropertyInfo IntervalCollection_t2093____IsSynchronized_PropertyInfo = 
{
	&IntervalCollection_t2093_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &IntervalCollection_get_IsSynchronized_m8512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IntervalCollection_get_SyncRoot_m8513_MethodInfo;
static PropertyInfo IntervalCollection_t2093____SyncRoot_PropertyInfo = 
{
	&IntervalCollection_t2093_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &IntervalCollection_get_SyncRoot_m8513_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IntervalCollection_t2093_PropertyInfos[] =
{
	&IntervalCollection_t2093____Item_PropertyInfo,
	&IntervalCollection_t2093____Count_PropertyInfo,
	&IntervalCollection_t2093____IsSynchronized_PropertyInfo,
	&IntervalCollection_t2093____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* IntervalCollection_t2093_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Enumerator_t2091_0_0_0,
	&CostDelegate_t2092_0_0_0,
};
extern MethodInfo IntervalCollection_CopyTo_m8514_MethodInfo;
extern MethodInfo IntervalCollection_GetEnumerator_m8515_MethodInfo;
static Il2CppMethodReference IntervalCollection_t2093_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&IntervalCollection_get_Count_m8511_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m8512_MethodInfo,
	&IntervalCollection_get_SyncRoot_m8513_MethodInfo,
	&IntervalCollection_CopyTo_m8514_MethodInfo,
	&IntervalCollection_GetEnumerator_m8515_MethodInfo,
};
static bool IntervalCollection_t2093_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ICollection_t1789_0_0_0;
extern Il2CppType IEnumerable_t38_0_0_0;
static const Il2CppType* IntervalCollection_t2093_InterfacesTypeInfos[] = 
{
	&ICollection_t1789_0_0_0,
	&IEnumerable_t38_0_0_0,
};
static Il2CppInterfaceOffsetPair IntervalCollection_t2093_InterfacesOffsets[] = 
{
	{ &ICollection_t1789_0_0_0, 4},
	{ &IEnumerable_t38_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType IntervalCollection_t2093_1_0_0;
struct IntervalCollection_t2093;
const Il2CppTypeDefinitionMetadata IntervalCollection_t2093_DefinitionMetadata = 
{
	NULL/* declaringType */
	, IntervalCollection_t2093_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, IntervalCollection_t2093_InterfacesTypeInfos/* implementedInterfaces */
	, IntervalCollection_t2093_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IntervalCollection_t2093_VTable/* vtableMethods */
	, IntervalCollection_t2093_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IntervalCollection_t2093_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntervalCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, IntervalCollection_t2093_MethodInfos/* methods */
	, IntervalCollection_t2093_PropertyInfos/* properties */
	, IntervalCollection_t2093_FieldInfos/* fields */
	, NULL/* events */
	, &IntervalCollection_t2093_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 56/* custom_attributes_cache */
	, &IntervalCollection_t2093_0_0_0/* byval_arg */
	, &IntervalCollection_t2093_1_0_0/* this_arg */
	, &IntervalCollection_t2093_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntervalCollection_t2093)/* instance_size */
	, sizeof (IntervalCollection_t2093)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_Parser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Parser
extern TypeInfo Parser_t2094_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_ParserMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::.ctor()
MethodInfo Parser__ctor_m8516_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Parser__ctor_m8516/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
static ParameterInfo Parser_t2094_Parser_ParseDecimal_m8517_ParameterInfos[] = 
{
	{"str", 0, 134218391, 0, &String_t_0_0_0},
	{"ptr", 1, 134218392, 0, &Int32_t189_1_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDecimal(System.String,System.Int32&)
MethodInfo Parser_ParseDecimal_m8517_MethodInfo = 
{
	"ParseDecimal"/* name */
	, (methodPointerType)&Parser_ParseDecimal_m8517/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317/* invoker_method */
	, Parser_t2094_Parser_ParseDecimal_m8517_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
static ParameterInfo Parser_t2094_Parser_ParseOctal_m8518_ParameterInfos[] = 
{
	{"str", 0, 134218393, 0, &String_t_0_0_0},
	{"ptr", 1, 134218394, 0, &Int32_t189_1_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseOctal(System.String,System.Int32&)
MethodInfo Parser_ParseOctal_m8518_MethodInfo = 
{
	"ParseOctal"/* name */
	, (methodPointerType)&Parser_ParseOctal_m8518/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317/* invoker_method */
	, Parser_t2094_Parser_ParseOctal_m8518_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseHex_m8519_ParameterInfos[] = 
{
	{"str", 0, 134218395, 0, &String_t_0_0_0},
	{"ptr", 1, 134218396, 0, &Int32_t189_1_0_0},
	{"digits", 2, 134218397, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseHex(System.String,System.Int32&,System.Int32)
MethodInfo Parser_ParseHex_m8519_MethodInfo = 
{
	"ParseHex"/* name */
	, (methodPointerType)&Parser_ParseHex_m8519/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseHex_m8519_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseNumber_m8520_ParameterInfos[] = 
{
	{"str", 0, 134218398, 0, &String_t_0_0_0},
	{"ptr", 1, 134218399, 0, &Int32_t189_1_0_0},
	{"b", 2, 134218400, 0, &Int32_t189_0_0_0},
	{"min", 3, 134218401, 0, &Int32_t189_0_0_0},
	{"max", 4, 134218402, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.String,System.Int32&,System.Int32,System.Int32,System.Int32)
MethodInfo Parser_ParseNumber_m8520_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m8520/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317_Int32_t189_Int32_t189_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseNumber_m8520_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
static ParameterInfo Parser_t2094_Parser_ParseName_m8521_ParameterInfos[] = 
{
	{"str", 0, 134218403, 0, &String_t_0_0_0},
	{"ptr", 1, 134218404, 0, &Int32_t189_1_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName(System.String,System.Int32&)
MethodInfo Parser_ParseName_m8521_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m8521/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t317/* invoker_method */
	, Parser_t2094_Parser_ParseName_m8521_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Parser_t2094_Parser_Unescape_m8522_ParameterInfos[] = 
{
	{"str", 0, 134218405, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::Unescape(System.String)
MethodInfo Parser_Unescape_m8522_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Parser_Unescape_m8522/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Parser_t2094_Parser_Unescape_m8522_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseRegularExpression_m8523_ParameterInfos[] = 
{
	{"pattern", 0, 134218406, 0, &String_t_0_0_0},
	{"options", 1, 134218407, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType RegularExpression_t2099_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.RegularExpression System.Text.RegularExpressions.Syntax.Parser::ParseRegularExpression(System.String,System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_ParseRegularExpression_m8523_MethodInfo = 
{
	"ParseRegularExpression"/* name */
	, (methodPointerType)&Parser_ParseRegularExpression_m8523/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &RegularExpression_t2099_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseRegularExpression_m8523_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Hashtable_t1667_0_0_0;
extern Il2CppType Hashtable_t1667_0_0_0;
static ParameterInfo Parser_t2094_Parser_GetMapping_m8524_ParameterInfos[] = 
{
	{"mapping", 0, 134218408, 0, &Hashtable_t1667_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::GetMapping(System.Collections.Hashtable)
MethodInfo Parser_GetMapping_m8524_MethodInfo = 
{
	"GetMapping"/* name */
	, (methodPointerType)&Parser_GetMapping_m8524/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t/* invoker_method */
	, Parser_t2094_Parser_GetMapping_m8524_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Group_t2098_0_0_0;
extern Il2CppType Group_t2098_0_0_0;
extern Il2CppType RegexOptions_t2068_0_0_0;
extern Il2CppType Assertion_t2104_0_0_0;
extern Il2CppType Assertion_t2104_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseGroup_m8525_ParameterInfos[] = 
{
	{"group", 0, 134218409, 0, &Group_t2098_0_0_0},
	{"options", 1, 134218410, 0, &RegexOptions_t2068_0_0_0},
	{"assertion", 2, 134218411, 0, &Assertion_t2104_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseGroup(System.Text.RegularExpressions.Syntax.Group,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.Syntax.Assertion)
MethodInfo Parser_ParseGroup_m8525_MethodInfo = 
{
	"ParseGroup"/* name */
	, (methodPointerType)&Parser_ParseGroup_m8525/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t/* invoker_method */
	, Parser_t2094_Parser_ParseGroup_m8525_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_1_0_0;
extern Il2CppType RegexOptions_t2068_1_0_0;
static ParameterInfo Parser_t2094_Parser_ParseGroupingConstruct_m8526_ParameterInfos[] = 
{
	{"options", 0, 134218412, 0, &RegexOptions_t2068_1_0_0},
};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t_RegexOptionsU26_t2180 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseGroupingConstruct(System.Text.RegularExpressions.RegexOptions&)
MethodInfo Parser_ParseGroupingConstruct_m8526_MethodInfo = 
{
	"ParseGroupingConstruct"/* name */
	, (methodPointerType)&Parser_ParseGroupingConstruct_m8526/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RegexOptionsU26_t2180/* invoker_method */
	, Parser_t2094_Parser_ParseGroupingConstruct_m8526_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ExpressionAssertion_t2105_0_0_0;
extern Il2CppType ExpressionAssertion_t2105_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseAssertionType_m8527_ParameterInfos[] = 
{
	{"assertion", 0, 134218413, 0, &ExpressionAssertion_t2105_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseAssertionType(System.Text.RegularExpressions.Syntax.ExpressionAssertion)
MethodInfo Parser_ParseAssertionType_m8527_MethodInfo = 
{
	"ParseAssertionType"/* name */
	, (methodPointerType)&Parser_ParseAssertionType_m8527/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Parser_t2094_Parser_ParseAssertionType_m8527_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_1_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseOptions_m8528_ParameterInfos[] = 
{
	{"options", 0, 134218414, 0, &RegexOptions_t2068_1_0_0},
	{"negate", 1, 134218415, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_RegexOptionsU26_t2180_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseOptions(System.Text.RegularExpressions.RegexOptions&,System.Boolean)
MethodInfo Parser_ParseOptions_m8528_MethodInfo = 
{
	"ParseOptions"/* name */
	, (methodPointerType)&Parser_ParseOptions_m8528/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_RegexOptionsU26_t2180_SByte_t236/* invoker_method */
	, Parser_t2094_Parser_ParseOptions_m8528_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseCharacterClass_m8529_ParameterInfos[] = 
{
	{"options", 0, 134218416, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseCharacterClass(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_ParseCharacterClass_m8529_MethodInfo = 
{
	"ParseCharacterClass"/* name */
	, (methodPointerType)&Parser_ParseCharacterClass_m8529/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseCharacterClass_m8529_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseRepetitionBounds_m8530_ParameterInfos[] = 
{
	{"min", 0, 134218417, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218418, 0, &Int32_t189_1_0_2},
	{"options", 2, 134218419, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseRepetitionBounds(System.Int32&,System.Int32&,System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_ParseRepetitionBounds_m8530_MethodInfo = 
{
	"ParseRepetitionBounds"/* name */
	, (methodPointerType)&Parser_ParseRepetitionBounds_m8530/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32U26_t317_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseRepetitionBounds_m8530_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern void* RuntimeInvoker_Category_t2075 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.Syntax.Parser::ParseUnicodeCategory()
MethodInfo Parser_ParseUnicodeCategory_m8531_MethodInfo = 
{
	"ParseUnicodeCategory"/* name */
	, (methodPointerType)&Parser_ParseUnicodeCategory_m8531/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Category_t2075_0_0_0/* return_type */
	, RuntimeInvoker_Category_t2075/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseSpecial_m8532_ParameterInfos[] = 
{
	{"options", 0, 134218420, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseSpecial(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_ParseSpecial_m8532_MethodInfo = 
{
	"ParseSpecial"/* name */
	, (methodPointerType)&Parser_ParseSpecial_m8532/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseSpecial_m8532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseEscape()
MethodInfo Parser_ParseEscape_m8533_MethodInfo = 
{
	"ParseEscape"/* name */
	, (methodPointerType)&Parser_ParseEscape_m8533/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName()
MethodInfo Parser_ParseName_m8534_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m8534/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo Parser_t2094_Parser_IsNameChar_m8535_ParameterInfos[] = 
{
	{"c", 0, 134218421, 0, &Char_t193_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsNameChar(System.Char)
MethodInfo Parser_IsNameChar_m8535_MethodInfo = 
{
	"IsNameChar"/* name */
	, (methodPointerType)&Parser_IsNameChar_m8535/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int16_t238/* invoker_method */
	, Parser_t2094_Parser_IsNameChar_m8535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseNumber_m8536_ParameterInfos[] = 
{
	{"b", 0, 134218422, 0, &Int32_t189_0_0_0},
	{"min", 1, 134218423, 0, &Int32_t189_0_0_0},
	{"max", 2, 134218424, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.Int32,System.Int32,System.Int32)
MethodInfo Parser_ParseNumber_m8536_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m8536/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseNumber_m8536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseDigit_m8537_ParameterInfos[] = 
{
	{"c", 0, 134218425, 0, &Char_t193_0_0_0},
	{"b", 1, 134218426, 0, &Int32_t189_0_0_0},
	{"n", 2, 134218427, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int16_t238_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDigit(System.Char,System.Int32,System.Int32)
MethodInfo Parser_ParseDigit_m8537_MethodInfo = 
{
	"ParseDigit"/* name */
	, (methodPointerType)&Parser_ParseDigit_m8537/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int16_t238_Int32_t189_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_ParseDigit_m8537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Parser_t2094_Parser_ConsumeWhitespace_m8538_ParameterInfos[] = 
{
	{"ignore", 0, 134218428, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ConsumeWhitespace(System.Boolean)
MethodInfo Parser_ConsumeWhitespace_m8538_MethodInfo = 
{
	"ConsumeWhitespace"/* name */
	, (methodPointerType)&Parser_ConsumeWhitespace_m8538/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Parser_t2094_Parser_ConsumeWhitespace_m8538_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Parser_t2094_Parser_ParseString_m8539_ParameterInfos[] = 
{
	{"pattern", 0, 134218429, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseString(System.String)
MethodInfo Parser_ParseString_m8539_MethodInfo = 
{
	"ParseString"/* name */
	, (methodPointerType)&Parser_ParseString_m8539/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Parser_t2094_Parser_ParseString_m8539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ResolveReferences()
MethodInfo Parser_ResolveReferences_m8540_MethodInfo = 
{
	"ResolveReferences"/* name */
	, (methodPointerType)&Parser_ResolveReferences_m8540/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ArrayList_t737_0_0_0;
extern Il2CppType ArrayList_t737_0_0_0;
static ParameterInfo Parser_t2094_Parser_HandleExplicitNumericGroups_m8541_ParameterInfos[] = 
{
	{"explicit_numeric_groups", 0, 134218430, 0, &ArrayList_t737_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::HandleExplicitNumericGroups(System.Collections.ArrayList)
MethodInfo Parser_HandleExplicitNumericGroups_m8541_MethodInfo = 
{
	"HandleExplicitNumericGroups"/* name */
	, (methodPointerType)&Parser_HandleExplicitNumericGroups_m8541/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Parser_t2094_Parser_HandleExplicitNumericGroups_m8541_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_IsIgnoreCase_m8542_ParameterInfos[] = 
{
	{"options", 0, 134218431, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnoreCase(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_IsIgnoreCase_m8542_MethodInfo = 
{
	"IsIgnoreCase"/* name */
	, (methodPointerType)&Parser_IsIgnoreCase_m8542/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_IsIgnoreCase_m8542_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_IsMultiline_m8543_ParameterInfos[] = 
{
	{"options", 0, 134218432, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsMultiline(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_IsMultiline_m8543_MethodInfo = 
{
	"IsMultiline"/* name */
	, (methodPointerType)&Parser_IsMultiline_m8543/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_IsMultiline_m8543_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_IsExplicitCapture_m8544_ParameterInfos[] = 
{
	{"options", 0, 134218433, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsExplicitCapture(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_IsExplicitCapture_m8544_MethodInfo = 
{
	"IsExplicitCapture"/* name */
	, (methodPointerType)&Parser_IsExplicitCapture_m8544/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_IsExplicitCapture_m8544_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_IsSingleline_m8545_ParameterInfos[] = 
{
	{"options", 0, 134218434, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsSingleline(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_IsSingleline_m8545_MethodInfo = 
{
	"IsSingleline"/* name */
	, (methodPointerType)&Parser_IsSingleline_m8545/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_IsSingleline_m8545_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_IsIgnorePatternWhitespace_m8546_ParameterInfos[] = 
{
	{"options", 0, 134218435, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnorePatternWhitespace(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_IsIgnorePatternWhitespace_m8546_MethodInfo = 
{
	"IsIgnorePatternWhitespace"/* name */
	, (methodPointerType)&Parser_IsIgnorePatternWhitespace_m8546/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_IsIgnorePatternWhitespace_m8546_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RegexOptions_t2068_0_0_0;
static ParameterInfo Parser_t2094_Parser_IsECMAScript_m8547_ParameterInfos[] = 
{
	{"options", 0, 134218436, 0, &RegexOptions_t2068_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsECMAScript(System.Text.RegularExpressions.RegexOptions)
MethodInfo Parser_IsECMAScript_m8547_MethodInfo = 
{
	"IsECMAScript"/* name */
	, (methodPointerType)&Parser_IsECMAScript_m8547/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int32_t189/* invoker_method */
	, Parser_t2094_Parser_IsECMAScript_m8547_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Parser_t2094_Parser_NewParseException_m8548_ParameterInfos[] = 
{
	{"msg", 0, 134218437, 0, &String_t_0_0_0},
};
extern Il2CppType ArgumentException_t1409_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.ArgumentException System.Text.RegularExpressions.Syntax.Parser::NewParseException(System.String)
MethodInfo Parser_NewParseException_m8548_MethodInfo = 
{
	"NewParseException"/* name */
	, (methodPointerType)&Parser_NewParseException_m8548/* method */
	, &Parser_t2094_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentException_t1409_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Parser_t2094_Parser_NewParseException_m8548_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Parser_t2094_MethodInfos[] =
{
	&Parser__ctor_m8516_MethodInfo,
	&Parser_ParseDecimal_m8517_MethodInfo,
	&Parser_ParseOctal_m8518_MethodInfo,
	&Parser_ParseHex_m8519_MethodInfo,
	&Parser_ParseNumber_m8520_MethodInfo,
	&Parser_ParseName_m8521_MethodInfo,
	&Parser_Unescape_m8522_MethodInfo,
	&Parser_ParseRegularExpression_m8523_MethodInfo,
	&Parser_GetMapping_m8524_MethodInfo,
	&Parser_ParseGroup_m8525_MethodInfo,
	&Parser_ParseGroupingConstruct_m8526_MethodInfo,
	&Parser_ParseAssertionType_m8527_MethodInfo,
	&Parser_ParseOptions_m8528_MethodInfo,
	&Parser_ParseCharacterClass_m8529_MethodInfo,
	&Parser_ParseRepetitionBounds_m8530_MethodInfo,
	&Parser_ParseUnicodeCategory_m8531_MethodInfo,
	&Parser_ParseSpecial_m8532_MethodInfo,
	&Parser_ParseEscape_m8533_MethodInfo,
	&Parser_ParseName_m8534_MethodInfo,
	&Parser_IsNameChar_m8535_MethodInfo,
	&Parser_ParseNumber_m8536_MethodInfo,
	&Parser_ParseDigit_m8537_MethodInfo,
	&Parser_ConsumeWhitespace_m8538_MethodInfo,
	&Parser_ParseString_m8539_MethodInfo,
	&Parser_ResolveReferences_m8540_MethodInfo,
	&Parser_HandleExplicitNumericGroups_m8541_MethodInfo,
	&Parser_IsIgnoreCase_m8542_MethodInfo,
	&Parser_IsMultiline_m8543_MethodInfo,
	&Parser_IsExplicitCapture_m8544_MethodInfo,
	&Parser_IsSingleline_m8545_MethodInfo,
	&Parser_IsIgnorePatternWhitespace_m8546_MethodInfo,
	&Parser_IsECMAScript_m8547_MethodInfo,
	&Parser_NewParseException_m8548_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo Parser_t2094____pattern_0_FieldInfo = 
{
	"pattern"/* name */
	, &String_t_0_0_1/* type */
	, &Parser_t2094_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t2094, ___pattern_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Parser_t2094____ptr_1_FieldInfo = 
{
	"ptr"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Parser_t2094_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t2094, ___ptr_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_1;
FieldInfo Parser_t2094____caps_2_FieldInfo = 
{
	"caps"/* name */
	, &ArrayList_t737_0_0_1/* type */
	, &Parser_t2094_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t2094, ___caps_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_1;
FieldInfo Parser_t2094____refs_3_FieldInfo = 
{
	"refs"/* name */
	, &Hashtable_t1667_0_0_1/* type */
	, &Parser_t2094_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t2094, ___refs_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Parser_t2094____num_groups_4_FieldInfo = 
{
	"num_groups"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Parser_t2094_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t2094, ___num_groups_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Parser_t2094____gap_5_FieldInfo = 
{
	"gap"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Parser_t2094_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t2094, ___gap_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Parser_t2094_FieldInfos[] =
{
	&Parser_t2094____pattern_0_FieldInfo,
	&Parser_t2094____ptr_1_FieldInfo,
	&Parser_t2094____caps_2_FieldInfo,
	&Parser_t2094____refs_3_FieldInfo,
	&Parser_t2094____num_groups_4_FieldInfo,
	&Parser_t2094____gap_5_FieldInfo,
	NULL
};
static Il2CppMethodReference Parser_t2094_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Parser_t2094_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Parser_t2094_0_0_0;
extern Il2CppType Parser_t2094_1_0_0;
struct Parser_t2094;
const Il2CppTypeDefinitionMetadata Parser_t2094_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Parser_t2094_VTable/* vtableMethods */
	, Parser_t2094_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Parser_t2094_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Parser"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Parser_t2094_MethodInfos/* methods */
	, NULL/* properties */
	, Parser_t2094_FieldInfos/* fields */
	, NULL/* events */
	, &Parser_t2094_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Parser_t2094_0_0_0/* byval_arg */
	, &Parser_t2094_1_0_0/* this_arg */
	, &Parser_t2094_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Parser_t2094)/* instance_size */
	, sizeof (Parser_t2094)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 33/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearch.h"
// Metadata Definition System.Text.RegularExpressions.QuickSearch
extern TypeInfo QuickSearch_t2087_il2cpp_TypeInfo;
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearchMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo QuickSearch_t2087_QuickSearch__ctor_m8549_ParameterInfos[] = 
{
	{"str", 0, 134218438, 0, &String_t_0_0_0},
	{"ignore", 1, 134218439, 0, &Boolean_t203_0_0_0},
	{"reverse", 2, 134218440, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.ctor(System.String,System.Boolean,System.Boolean)
MethodInfo QuickSearch__ctor_m8549_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QuickSearch__ctor_m8549/* method */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236/* invoker_method */
	, QuickSearch_t2087_QuickSearch__ctor_m8549_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.cctor()
MethodInfo QuickSearch__cctor_m8550_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&QuickSearch__cctor_m8550/* method */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::get_Length()
MethodInfo QuickSearch_get_Length_m8551_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&QuickSearch_get_Length_m8551/* method */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo QuickSearch_t2087_QuickSearch_Search_m8552_ParameterInfos[] = 
{
	{"text", 0, 134218441, 0, &String_t_0_0_0},
	{"start", 1, 134218442, 0, &Int32_t189_0_0_0},
	{"end", 2, 134218443, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::Search(System.String,System.Int32,System.Int32)
MethodInfo QuickSearch_Search_m8552_MethodInfo = 
{
	"Search"/* name */
	, (methodPointerType)&QuickSearch_Search_m8552/* method */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189/* invoker_method */
	, QuickSearch_t2087_QuickSearch_Search_m8552_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::SetupShiftTable()
MethodInfo QuickSearch_SetupShiftTable_m8553_MethodInfo = 
{
	"SetupShiftTable"/* name */
	, (methodPointerType)&QuickSearch_SetupShiftTable_m8553/* method */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo QuickSearch_t2087_QuickSearch_GetShiftDistance_m8554_ParameterInfos[] = 
{
	{"c", 0, 134218444, 0, &Char_t193_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::GetShiftDistance(System.Char)
MethodInfo QuickSearch_GetShiftDistance_m8554_MethodInfo = 
{
	"GetShiftDistance"/* name */
	, (methodPointerType)&QuickSearch_GetShiftDistance_m8554/* method */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int16_t238/* invoker_method */
	, QuickSearch_t2087_QuickSearch_GetShiftDistance_m8554_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo QuickSearch_t2087_QuickSearch_GetChar_m8555_ParameterInfos[] = 
{
	{"c", 0, 134218445, 0, &Char_t193_0_0_0},
};
extern Il2CppType Char_t193_0_0_0;
extern void* RuntimeInvoker_Char_t193_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Char System.Text.RegularExpressions.QuickSearch::GetChar(System.Char)
MethodInfo QuickSearch_GetChar_m8555_MethodInfo = 
{
	"GetChar"/* name */
	, (methodPointerType)&QuickSearch_GetChar_m8555/* method */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* declaring_type */
	, &Char_t193_0_0_0/* return_type */
	, RuntimeInvoker_Char_t193_Int16_t238/* invoker_method */
	, QuickSearch_t2087_QuickSearch_GetChar_m8555_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* QuickSearch_t2087_MethodInfos[] =
{
	&QuickSearch__ctor_m8549_MethodInfo,
	&QuickSearch__cctor_m8550_MethodInfo,
	&QuickSearch_get_Length_m8551_MethodInfo,
	&QuickSearch_Search_m8552_MethodInfo,
	&QuickSearch_SetupShiftTable_m8553_MethodInfo,
	&QuickSearch_GetShiftDistance_m8554_MethodInfo,
	&QuickSearch_GetChar_m8555_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo QuickSearch_t2087____str_0_FieldInfo = 
{
	"str"/* name */
	, &String_t_0_0_1/* type */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, offsetof(QuickSearch_t2087, ___str_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo QuickSearch_t2087____len_1_FieldInfo = 
{
	"len"/* name */
	, &Int32_t189_0_0_1/* type */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, offsetof(QuickSearch_t2087, ___len_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo QuickSearch_t2087____ignore_2_FieldInfo = 
{
	"ignore"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, offsetof(QuickSearch_t2087, ___ignore_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo QuickSearch_t2087____reverse_3_FieldInfo = 
{
	"reverse"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, offsetof(QuickSearch_t2087, ___reverse_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ByteU5BU5D_t350_0_0_1;
FieldInfo QuickSearch_t2087____shift_4_FieldInfo = 
{
	"shift"/* name */
	, &ByteU5BU5D_t350_0_0_1/* type */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, offsetof(QuickSearch_t2087, ___shift_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_1;
FieldInfo QuickSearch_t2087____shiftExtended_5_FieldInfo = 
{
	"shiftExtended"/* name */
	, &Hashtable_t1667_0_0_1/* type */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, offsetof(QuickSearch_t2087, ___shiftExtended_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_49;
FieldInfo QuickSearch_t2087____THRESHOLD_6_FieldInfo = 
{
	"THRESHOLD"/* name */
	, &Int32_t189_0_0_49/* type */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, offsetof(QuickSearch_t2087_StaticFields, ___THRESHOLD_6)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* QuickSearch_t2087_FieldInfos[] =
{
	&QuickSearch_t2087____str_0_FieldInfo,
	&QuickSearch_t2087____len_1_FieldInfo,
	&QuickSearch_t2087____ignore_2_FieldInfo,
	&QuickSearch_t2087____reverse_3_FieldInfo,
	&QuickSearch_t2087____shift_4_FieldInfo,
	&QuickSearch_t2087____shiftExtended_5_FieldInfo,
	&QuickSearch_t2087____THRESHOLD_6_FieldInfo,
	NULL
};
extern MethodInfo QuickSearch_get_Length_m8551_MethodInfo;
static PropertyInfo QuickSearch_t2087____Length_PropertyInfo = 
{
	&QuickSearch_t2087_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &QuickSearch_get_Length_m8551_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* QuickSearch_t2087_PropertyInfos[] =
{
	&QuickSearch_t2087____Length_PropertyInfo,
	NULL
};
static Il2CppMethodReference QuickSearch_t2087_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool QuickSearch_t2087_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType QuickSearch_t2087_0_0_0;
extern Il2CppType QuickSearch_t2087_1_0_0;
struct QuickSearch_t2087;
const Il2CppTypeDefinitionMetadata QuickSearch_t2087_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, QuickSearch_t2087_VTable/* vtableMethods */
	, QuickSearch_t2087_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo QuickSearch_t2087_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuickSearch"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, QuickSearch_t2087_MethodInfos/* methods */
	, QuickSearch_t2087_PropertyInfos/* properties */
	, QuickSearch_t2087_FieldInfos/* fields */
	, NULL/* events */
	, &QuickSearch_t2087_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuickSearch_t2087_0_0_0/* byval_arg */
	, &QuickSearch_t2087_1_0_0/* this_arg */
	, &QuickSearch_t2087_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QuickSearch_t2087)/* instance_size */
	, sizeof (QuickSearch_t2087)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QuickSearch_t2087_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColle.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionCollection
extern TypeInfo ExpressionCollection_t2095_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColleMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::.ctor()
MethodInfo ExpressionCollection__ctor_m8556_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionCollection__ctor_m8556/* method */
	, &ExpressionCollection_t2095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo ExpressionCollection_t2095_ExpressionCollection_Add_m8557_ParameterInfos[] = 
{
	{"e", 0, 134218446, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::Add(System.Text.RegularExpressions.Syntax.Expression)
MethodInfo ExpressionCollection_Add_m8557_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&ExpressionCollection_Add_m8557/* method */
	, &ExpressionCollection_t2095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ExpressionCollection_t2095_ExpressionCollection_Add_m8557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ExpressionCollection_t2095_ExpressionCollection_get_Item_m8558_ParameterInfos[] = 
{
	{"i", 0, 134218447, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionCollection::get_Item(System.Int32)
MethodInfo ExpressionCollection_get_Item_m8558_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&ExpressionCollection_get_Item_m8558/* method */
	, &ExpressionCollection_t2095_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189/* invoker_method */
	, ExpressionCollection_t2095_ExpressionCollection_get_Item_m8558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo ExpressionCollection_t2095_ExpressionCollection_set_Item_m8559_ParameterInfos[] = 
{
	{"i", 0, 134218448, 0, &Int32_t189_0_0_0},
	{"value", 1, 134218449, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::set_Item(System.Int32,System.Text.RegularExpressions.Syntax.Expression)
MethodInfo ExpressionCollection_set_Item_m8559_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&ExpressionCollection_set_Item_m8559/* method */
	, &ExpressionCollection_t2095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Object_t/* invoker_method */
	, ExpressionCollection_t2095_ExpressionCollection_set_Item_m8559_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ExpressionCollection_t2095_ExpressionCollection_OnValidate_m8560_ParameterInfos[] = 
{
	{"o", 0, 134218450, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::OnValidate(System.Object)
MethodInfo ExpressionCollection_OnValidate_m8560_MethodInfo = 
{
	"OnValidate"/* name */
	, (methodPointerType)&ExpressionCollection_OnValidate_m8560/* method */
	, &ExpressionCollection_t2095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ExpressionCollection_t2095_ExpressionCollection_OnValidate_m8560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ExpressionCollection_t2095_MethodInfos[] =
{
	&ExpressionCollection__ctor_m8556_MethodInfo,
	&ExpressionCollection_Add_m8557_MethodInfo,
	&ExpressionCollection_get_Item_m8558_MethodInfo,
	&ExpressionCollection_set_Item_m8559_MethodInfo,
	&ExpressionCollection_OnValidate_m8560_MethodInfo,
	NULL
};
extern MethodInfo ExpressionCollection_get_Item_m8558_MethodInfo;
extern MethodInfo ExpressionCollection_set_Item_m8559_MethodInfo;
static PropertyInfo ExpressionCollection_t2095____Item_PropertyInfo = 
{
	&ExpressionCollection_t2095_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ExpressionCollection_get_Item_m8558_MethodInfo/* get */
	, &ExpressionCollection_set_Item_m8559_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ExpressionCollection_t2095_PropertyInfos[] =
{
	&ExpressionCollection_t2095____Item_PropertyInfo,
	NULL
};
extern MethodInfo CollectionBase_GetEnumerator_m8963_MethodInfo;
extern MethodInfo CollectionBase_get_Count_m8964_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_ICollection_get_IsSynchronized_m8965_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_ICollection_get_SyncRoot_m8966_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_ICollection_CopyTo_m8967_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_get_IsFixedSize_m8968_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_get_IsReadOnly_m8969_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_get_Item_m8970_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_set_Item_m8971_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_Add_m8972_MethodInfo;
extern MethodInfo CollectionBase_Clear_m8973_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_Contains_m8974_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_IndexOf_m8975_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_Insert_m8976_MethodInfo;
extern MethodInfo CollectionBase_System_Collections_IList_Remove_m8977_MethodInfo;
extern MethodInfo CollectionBase_RemoveAt_m8978_MethodInfo;
extern MethodInfo CollectionBase_OnClear_m8979_MethodInfo;
extern MethodInfo CollectionBase_OnClearComplete_m8980_MethodInfo;
extern MethodInfo CollectionBase_OnInsert_m8981_MethodInfo;
extern MethodInfo CollectionBase_OnInsertComplete_m8982_MethodInfo;
extern MethodInfo CollectionBase_OnRemove_m8983_MethodInfo;
extern MethodInfo CollectionBase_OnRemoveComplete_m8984_MethodInfo;
extern MethodInfo CollectionBase_OnSet_m8985_MethodInfo;
extern MethodInfo CollectionBase_OnSetComplete_m8986_MethodInfo;
extern MethodInfo ExpressionCollection_OnValidate_m8560_MethodInfo;
static Il2CppMethodReference ExpressionCollection_t2095_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&CollectionBase_GetEnumerator_m8963_MethodInfo,
	&CollectionBase_get_Count_m8964_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_IsSynchronized_m8965_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_SyncRoot_m8966_MethodInfo,
	&CollectionBase_System_Collections_ICollection_CopyTo_m8967_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsFixedSize_m8968_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsReadOnly_m8969_MethodInfo,
	&CollectionBase_System_Collections_IList_get_Item_m8970_MethodInfo,
	&CollectionBase_System_Collections_IList_set_Item_m8971_MethodInfo,
	&CollectionBase_System_Collections_IList_Add_m8972_MethodInfo,
	&CollectionBase_Clear_m8973_MethodInfo,
	&CollectionBase_System_Collections_IList_Contains_m8974_MethodInfo,
	&CollectionBase_System_Collections_IList_IndexOf_m8975_MethodInfo,
	&CollectionBase_System_Collections_IList_Insert_m8976_MethodInfo,
	&CollectionBase_System_Collections_IList_Remove_m8977_MethodInfo,
	&CollectionBase_RemoveAt_m8978_MethodInfo,
	&CollectionBase_OnClear_m8979_MethodInfo,
	&CollectionBase_OnClearComplete_m8980_MethodInfo,
	&CollectionBase_OnInsert_m8981_MethodInfo,
	&CollectionBase_OnInsertComplete_m8982_MethodInfo,
	&CollectionBase_OnRemove_m8983_MethodInfo,
	&CollectionBase_OnRemoveComplete_m8984_MethodInfo,
	&CollectionBase_OnSet_m8985_MethodInfo,
	&CollectionBase_OnSetComplete_m8986_MethodInfo,
	&ExpressionCollection_OnValidate_m8560_MethodInfo,
};
static bool ExpressionCollection_t2095_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExpressionCollection_t2095_InterfacesOffsets[] = 
{
	{ &IEnumerable_t38_0_0_0, 4},
	{ &ICollection_t1789_0_0_0, 5},
	{ &IList_t183_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType ExpressionCollection_t2095_0_0_0;
extern Il2CppType ExpressionCollection_t2095_1_0_0;
extern Il2CppType CollectionBase_t2030_0_0_0;
struct ExpressionCollection_t2095;
const Il2CppTypeDefinitionMetadata ExpressionCollection_t2095_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExpressionCollection_t2095_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t2030_0_0_0/* parent */
	, ExpressionCollection_t2095_VTable/* vtableMethods */
	, ExpressionCollection_t2095_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ExpressionCollection_t2095_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionCollection"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionCollection_t2095_MethodInfos/* methods */
	, ExpressionCollection_t2095_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ExpressionCollection_t2095_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 57/* custom_attributes_cache */
	, &ExpressionCollection_t2095_0_0_0/* byval_arg */
	, &ExpressionCollection_t2095_1_0_0/* this_arg */
	, &ExpressionCollection_t2095_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionCollection_t2095)/* instance_size */
	, sizeof (ExpressionCollection_t2095)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Expression
extern TypeInfo Expression_t2096_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_ExpressionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::.ctor()
MethodInfo Expression__ctor_m8561_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Expression__ctor_m8561/* method */
	, &Expression_t2096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Expression_t2096_Expression_Compile_m8943_ParameterInfos[] = 
{
	{"cmp", 0, 134218451, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218452, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo Expression_Compile_m8943_MethodInfo = 
{
	"Compile"/* name */
	, NULL/* method */
	, &Expression_t2096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Expression_t2096_Expression_Compile_m8943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Expression_t2096_Expression_GetWidth_m8944_ParameterInfos[] = 
{
	{"min", 0, 134218453, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218454, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::GetWidth(System.Int32&,System.Int32&)
MethodInfo Expression_GetWidth_m8944_MethodInfo = 
{
	"GetWidth"/* name */
	, NULL/* method */
	, &Expression_t2096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Expression_t2096_Expression_GetWidth_m8944_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Expression::GetFixedWidth()
MethodInfo Expression_GetFixedWidth_m8562_MethodInfo = 
{
	"GetFixedWidth"/* name */
	, (methodPointerType)&Expression_GetFixedWidth_m8562/* method */
	, &Expression_t2096_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Expression_t2096_Expression_GetAnchorInfo_m8563_ParameterInfos[] = 
{
	{"reverse", 0, 134218455, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType AnchorInfo_t2114_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Expression::GetAnchorInfo(System.Boolean)
MethodInfo Expression_GetAnchorInfo_m8563_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Expression_GetAnchorInfo_m8563/* method */
	, &Expression_t2096_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t2114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t236/* invoker_method */
	, Expression_t2096_Expression_GetAnchorInfo_m8563_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Expression::IsComplex()
MethodInfo Expression_IsComplex_m8945_MethodInfo = 
{
	"IsComplex"/* name */
	, NULL/* method */
	, &Expression_t2096_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Expression_t2096_MethodInfos[] =
{
	&Expression__ctor_m8561_MethodInfo,
	&Expression_Compile_m8943_MethodInfo,
	&Expression_GetWidth_m8944_MethodInfo,
	&Expression_GetFixedWidth_m8562_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&Expression_IsComplex_m8945_MethodInfo,
	NULL
};
extern MethodInfo Expression_GetAnchorInfo_m8563_MethodInfo;
static Il2CppMethodReference Expression_t2096_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	NULL,
};
static bool Expression_t2096_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Expression_t2096_1_0_0;
struct Expression_t2096;
const Il2CppTypeDefinitionMetadata Expression_t2096_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Expression_t2096_VTable/* vtableMethods */
	, Expression_t2096_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Expression_t2096_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Expression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Expression_t2096_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Expression_t2096_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Expression_t2096_0_0_0/* byval_arg */
	, &Expression_t2096_1_0_0/* this_arg */
	, &Expression_t2096_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Expression_t2096)/* instance_size */
	, sizeof (Expression_t2096)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpres.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CompositeExpression
extern TypeInfo CompositeExpression_t2097_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpresMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::.ctor()
MethodInfo CompositeExpression__ctor_m8564_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CompositeExpression__ctor_m8564/* method */
	, &CompositeExpression_t2097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ExpressionCollection_t2095_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::get_Expressions()
MethodInfo CompositeExpression_get_Expressions_m8565_MethodInfo = 
{
	"get_Expressions"/* name */
	, (methodPointerType)&CompositeExpression_get_Expressions_m8565/* method */
	, &CompositeExpression_t2097_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t2095_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo CompositeExpression_t2097_CompositeExpression_GetWidth_m8566_ParameterInfos[] = 
{
	{"min", 0, 134218456, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218457, 0, &Int32_t189_1_0_2},
	{"count", 2, 134218458, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::GetWidth(System.Int32&,System.Int32&,System.Int32)
MethodInfo CompositeExpression_GetWidth_m8566_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CompositeExpression_GetWidth_m8566/* method */
	, &CompositeExpression_t2097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317_Int32_t189/* invoker_method */
	, CompositeExpression_t2097_CompositeExpression_GetWidth_m8566_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CompositeExpression::IsComplex()
MethodInfo CompositeExpression_IsComplex_m8567_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CompositeExpression_IsComplex_m8567/* method */
	, &CompositeExpression_t2097_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CompositeExpression_t2097_MethodInfos[] =
{
	&CompositeExpression__ctor_m8564_MethodInfo,
	&CompositeExpression_get_Expressions_m8565_MethodInfo,
	&CompositeExpression_GetWidth_m8566_MethodInfo,
	&CompositeExpression_IsComplex_m8567_MethodInfo,
	NULL
};
extern Il2CppType ExpressionCollection_t2095_0_0_1;
FieldInfo CompositeExpression_t2097____expressions_0_FieldInfo = 
{
	"expressions"/* name */
	, &ExpressionCollection_t2095_0_0_1/* type */
	, &CompositeExpression_t2097_il2cpp_TypeInfo/* parent */
	, offsetof(CompositeExpression_t2097, ___expressions_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CompositeExpression_t2097_FieldInfos[] =
{
	&CompositeExpression_t2097____expressions_0_FieldInfo,
	NULL
};
extern MethodInfo CompositeExpression_get_Expressions_m8565_MethodInfo;
static PropertyInfo CompositeExpression_t2097____Expressions_PropertyInfo = 
{
	&CompositeExpression_t2097_il2cpp_TypeInfo/* parent */
	, "Expressions"/* name */
	, &CompositeExpression_get_Expressions_m8565_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* CompositeExpression_t2097_PropertyInfos[] =
{
	&CompositeExpression_t2097____Expressions_PropertyInfo,
	NULL
};
extern MethodInfo CompositeExpression_IsComplex_m8567_MethodInfo;
static Il2CppMethodReference CompositeExpression_t2097_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&CompositeExpression_IsComplex_m8567_MethodInfo,
};
static bool CompositeExpression_t2097_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType CompositeExpression_t2097_0_0_0;
extern Il2CppType CompositeExpression_t2097_1_0_0;
struct CompositeExpression_t2097;
const Il2CppTypeDefinitionMetadata CompositeExpression_t2097_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t2096_0_0_0/* parent */
	, CompositeExpression_t2097_VTable/* vtableMethods */
	, CompositeExpression_t2097_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CompositeExpression_t2097_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompositeExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CompositeExpression_t2097_MethodInfos/* methods */
	, CompositeExpression_t2097_PropertyInfos/* properties */
	, CompositeExpression_t2097_FieldInfos/* fields */
	, NULL/* events */
	, &CompositeExpression_t2097_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CompositeExpression_t2097_0_0_0/* byval_arg */
	, &CompositeExpression_t2097_1_0_0/* this_arg */
	, &CompositeExpression_t2097_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompositeExpression_t2097)/* instance_size */
	, sizeof (CompositeExpression_t2097)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_Group.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Group
extern TypeInfo Group_t2098_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_GroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::.ctor()
MethodInfo Group__ctor_m8568_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Group__ctor_m8568/* method */
	, &Group_t2098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo Group_t2098_Group_AppendExpression_m8569_ParameterInfos[] = 
{
	{"e", 0, 134218459, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::AppendExpression(System.Text.RegularExpressions.Syntax.Expression)
MethodInfo Group_AppendExpression_m8569_MethodInfo = 
{
	"AppendExpression"/* name */
	, (methodPointerType)&Group_AppendExpression_m8569/* method */
	, &Group_t2098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Group_t2098_Group_AppendExpression_m8569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Group_t2098_Group_Compile_m8570_ParameterInfos[] = 
{
	{"cmp", 0, 134218460, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218461, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo Group_Compile_m8570_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Group_Compile_m8570/* method */
	, &Group_t2098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Group_t2098_Group_Compile_m8570_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Group_t2098_Group_GetWidth_m8571_ParameterInfos[] = 
{
	{"min", 0, 134218462, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218463, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::GetWidth(System.Int32&,System.Int32&)
MethodInfo Group_GetWidth_m8571_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Group_GetWidth_m8571/* method */
	, &Group_t2098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Group_t2098_Group_GetWidth_m8571_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Group_t2098_Group_GetAnchorInfo_m8572_ParameterInfos[] = 
{
	{"reverse", 0, 134218464, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType AnchorInfo_t2114_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Group::GetAnchorInfo(System.Boolean)
MethodInfo Group_GetAnchorInfo_m8572_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Group_GetAnchorInfo_m8572/* method */
	, &Group_t2098_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t2114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t236/* invoker_method */
	, Group_t2098_Group_GetAnchorInfo_m8572_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Group_t2098_MethodInfos[] =
{
	&Group__ctor_m8568_MethodInfo,
	&Group_AppendExpression_m8569_MethodInfo,
	&Group_Compile_m8570_MethodInfo,
	&Group_GetWidth_m8571_MethodInfo,
	&Group_GetAnchorInfo_m8572_MethodInfo,
	NULL
};
extern MethodInfo Group_Compile_m8570_MethodInfo;
extern MethodInfo Group_GetWidth_m8571_MethodInfo;
extern MethodInfo Group_GetAnchorInfo_m8572_MethodInfo;
static Il2CppMethodReference Group_t2098_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Group_Compile_m8570_MethodInfo,
	&Group_GetWidth_m8571_MethodInfo,
	&Group_GetAnchorInfo_m8572_MethodInfo,
	&CompositeExpression_IsComplex_m8567_MethodInfo,
};
static bool Group_t2098_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Group_t2098_1_0_0;
struct Group_t2098;
const Il2CppTypeDefinitionMetadata Group_t2098_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t2097_0_0_0/* parent */
	, Group_t2098_VTable/* vtableMethods */
	, Group_t2098_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Group_t2098_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Group"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Group_t2098_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Group_t2098_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Group_t2098_0_0_0/* byval_arg */
	, &Group_t2098_1_0_0/* this_arg */
	, &Group_t2098_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Group_t2098)/* instance_size */
	, sizeof (Group_t2098)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressi.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.RegularExpression
extern TypeInfo RegularExpression_t2099_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressiMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::.ctor()
MethodInfo RegularExpression__ctor_m8573_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RegularExpression__ctor_m8573/* method */
	, &RegularExpression_t2099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo RegularExpression_t2099_RegularExpression_set_GroupCount_m8574_ParameterInfos[] = 
{
	{"value", 0, 134218465, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::set_GroupCount(System.Int32)
MethodInfo RegularExpression_set_GroupCount_m8574_MethodInfo = 
{
	"set_GroupCount"/* name */
	, (methodPointerType)&RegularExpression_set_GroupCount_m8574/* method */
	, &RegularExpression_t2099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, RegularExpression_t2099_RegularExpression_set_GroupCount_m8574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo RegularExpression_t2099_RegularExpression_Compile_m8575_ParameterInfos[] = 
{
	{"cmp", 0, 134218466, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218467, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo RegularExpression_Compile_m8575_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&RegularExpression_Compile_m8575/* method */
	, &RegularExpression_t2099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, RegularExpression_t2099_RegularExpression_Compile_m8575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RegularExpression_t2099_MethodInfos[] =
{
	&RegularExpression__ctor_m8573_MethodInfo,
	&RegularExpression_set_GroupCount_m8574_MethodInfo,
	&RegularExpression_Compile_m8575_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo RegularExpression_t2099____group_count_1_FieldInfo = 
{
	"group_count"/* name */
	, &Int32_t189_0_0_1/* type */
	, &RegularExpression_t2099_il2cpp_TypeInfo/* parent */
	, offsetof(RegularExpression_t2099, ___group_count_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RegularExpression_t2099_FieldInfos[] =
{
	&RegularExpression_t2099____group_count_1_FieldInfo,
	NULL
};
extern MethodInfo RegularExpression_set_GroupCount_m8574_MethodInfo;
static PropertyInfo RegularExpression_t2099____GroupCount_PropertyInfo = 
{
	&RegularExpression_t2099_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, NULL/* get */
	, &RegularExpression_set_GroupCount_m8574_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* RegularExpression_t2099_PropertyInfos[] =
{
	&RegularExpression_t2099____GroupCount_PropertyInfo,
	NULL
};
extern MethodInfo RegularExpression_Compile_m8575_MethodInfo;
static Il2CppMethodReference RegularExpression_t2099_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&RegularExpression_Compile_m8575_MethodInfo,
	&Group_GetWidth_m8571_MethodInfo,
	&Group_GetAnchorInfo_m8572_MethodInfo,
	&CompositeExpression_IsComplex_m8567_MethodInfo,
};
static bool RegularExpression_t2099_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType RegularExpression_t2099_0_0_0;
extern Il2CppType RegularExpression_t2099_1_0_0;
struct RegularExpression_t2099;
const Il2CppTypeDefinitionMetadata RegularExpression_t2099_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t2098_0_0_0/* parent */
	, RegularExpression_t2099_VTable/* vtableMethods */
	, RegularExpression_t2099_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RegularExpression_t2099_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RegularExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, RegularExpression_t2099_MethodInfos/* methods */
	, RegularExpression_t2099_PropertyInfos/* properties */
	, RegularExpression_t2099_FieldInfos/* fields */
	, NULL/* events */
	, &RegularExpression_t2099_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RegularExpression_t2099_0_0_0/* byval_arg */
	, &RegularExpression_t2099_1_0_0/* this_arg */
	, &RegularExpression_t2099_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RegularExpression_t2099)/* instance_size */
	, sizeof (RegularExpression_t2099)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CapturingGroup
extern TypeInfo CapturingGroup_t2100_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::.ctor()
MethodInfo CapturingGroup__ctor_m8576_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CapturingGroup__ctor_m8576/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::get_Index()
MethodInfo CapturingGroup_get_Index_m8577_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&CapturingGroup_get_Index_m8577/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo CapturingGroup_t2100_CapturingGroup_set_Index_m8578_ParameterInfos[] = 
{
	{"value", 0, 134218468, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Index(System.Int32)
MethodInfo CapturingGroup_set_Index_m8578_MethodInfo = 
{
	"set_Index"/* name */
	, (methodPointerType)&CapturingGroup_set_Index_m8578/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, CapturingGroup_t2100_CapturingGroup_set_Index_m8578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.CapturingGroup::get_Name()
MethodInfo CapturingGroup_get_Name_m8579_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&CapturingGroup_get_Name_m8579/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo CapturingGroup_t2100_CapturingGroup_set_Name_m8580_ParameterInfos[] = 
{
	{"value", 0, 134218469, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Name(System.String)
MethodInfo CapturingGroup_set_Name_m8580_MethodInfo = 
{
	"set_Name"/* name */
	, (methodPointerType)&CapturingGroup_set_Name_m8580/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, CapturingGroup_t2100_CapturingGroup_set_Name_m8580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::get_IsNamed()
MethodInfo CapturingGroup_get_IsNamed_m8581_MethodInfo = 
{
	"get_IsNamed"/* name */
	, (methodPointerType)&CapturingGroup_get_IsNamed_m8581/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo CapturingGroup_t2100_CapturingGroup_Compile_m8582_ParameterInfos[] = 
{
	{"cmp", 0, 134218470, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218471, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo CapturingGroup_Compile_m8582_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CapturingGroup_Compile_m8582/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, CapturingGroup_t2100_CapturingGroup_Compile_m8582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::IsComplex()
MethodInfo CapturingGroup_IsComplex_m8583_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CapturingGroup_IsComplex_m8583/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CapturingGroup_t2100_CapturingGroup_CompareTo_m8584_ParameterInfos[] = 
{
	{"other", 0, 134218472, 0, &Object_t_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::CompareTo(System.Object)
MethodInfo CapturingGroup_CompareTo_m8584_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&CapturingGroup_CompareTo_m8584/* method */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t/* invoker_method */
	, CapturingGroup_t2100_CapturingGroup_CompareTo_m8584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CapturingGroup_t2100_MethodInfos[] =
{
	&CapturingGroup__ctor_m8576_MethodInfo,
	&CapturingGroup_get_Index_m8577_MethodInfo,
	&CapturingGroup_set_Index_m8578_MethodInfo,
	&CapturingGroup_get_Name_m8579_MethodInfo,
	&CapturingGroup_set_Name_m8580_MethodInfo,
	&CapturingGroup_get_IsNamed_m8581_MethodInfo,
	&CapturingGroup_Compile_m8582_MethodInfo,
	&CapturingGroup_IsComplex_m8583_MethodInfo,
	&CapturingGroup_CompareTo_m8584_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo CapturingGroup_t2100____gid_1_FieldInfo = 
{
	"gid"/* name */
	, &Int32_t189_0_0_1/* type */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* parent */
	, offsetof(CapturingGroup_t2100, ___gid_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo CapturingGroup_t2100____name_2_FieldInfo = 
{
	"name"/* name */
	, &String_t_0_0_1/* type */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* parent */
	, offsetof(CapturingGroup_t2100, ___name_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CapturingGroup_t2100_FieldInfos[] =
{
	&CapturingGroup_t2100____gid_1_FieldInfo,
	&CapturingGroup_t2100____name_2_FieldInfo,
	NULL
};
extern MethodInfo CapturingGroup_get_Index_m8577_MethodInfo;
extern MethodInfo CapturingGroup_set_Index_m8578_MethodInfo;
static PropertyInfo CapturingGroup_t2100____Index_PropertyInfo = 
{
	&CapturingGroup_t2100_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &CapturingGroup_get_Index_m8577_MethodInfo/* get */
	, &CapturingGroup_set_Index_m8578_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CapturingGroup_get_Name_m8579_MethodInfo;
extern MethodInfo CapturingGroup_set_Name_m8580_MethodInfo;
static PropertyInfo CapturingGroup_t2100____Name_PropertyInfo = 
{
	&CapturingGroup_t2100_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &CapturingGroup_get_Name_m8579_MethodInfo/* get */
	, &CapturingGroup_set_Name_m8580_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CapturingGroup_get_IsNamed_m8581_MethodInfo;
static PropertyInfo CapturingGroup_t2100____IsNamed_PropertyInfo = 
{
	&CapturingGroup_t2100_il2cpp_TypeInfo/* parent */
	, "IsNamed"/* name */
	, &CapturingGroup_get_IsNamed_m8581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* CapturingGroup_t2100_PropertyInfos[] =
{
	&CapturingGroup_t2100____Index_PropertyInfo,
	&CapturingGroup_t2100____Name_PropertyInfo,
	&CapturingGroup_t2100____IsNamed_PropertyInfo,
	NULL
};
extern MethodInfo CapturingGroup_Compile_m8582_MethodInfo;
extern MethodInfo CapturingGroup_IsComplex_m8583_MethodInfo;
extern MethodInfo CapturingGroup_CompareTo_m8584_MethodInfo;
static Il2CppMethodReference CapturingGroup_t2100_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&CapturingGroup_Compile_m8582_MethodInfo,
	&Group_GetWidth_m8571_MethodInfo,
	&Group_GetAnchorInfo_m8572_MethodInfo,
	&CapturingGroup_IsComplex_m8583_MethodInfo,
	&CapturingGroup_CompareTo_m8584_MethodInfo,
};
static bool CapturingGroup_t2100_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* CapturingGroup_t2100_InterfacesTypeInfos[] = 
{
	&IComparable_t314_0_0_0,
};
static Il2CppInterfaceOffsetPair CapturingGroup_t2100_InterfacesOffsets[] = 
{
	{ &IComparable_t314_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType CapturingGroup_t2100_0_0_0;
extern Il2CppType CapturingGroup_t2100_1_0_0;
struct CapturingGroup_t2100;
const Il2CppTypeDefinitionMetadata CapturingGroup_t2100_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CapturingGroup_t2100_InterfacesTypeInfos/* implementedInterfaces */
	, CapturingGroup_t2100_InterfacesOffsets/* interfaceOffsets */
	, &Group_t2098_0_0_0/* parent */
	, CapturingGroup_t2100_VTable/* vtableMethods */
	, CapturingGroup_t2100_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CapturingGroup_t2100_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapturingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CapturingGroup_t2100_MethodInfos/* methods */
	, CapturingGroup_t2100_PropertyInfos/* properties */
	, CapturingGroup_t2100_FieldInfos/* fields */
	, NULL/* events */
	, &CapturingGroup_t2100_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapturingGroup_t2100_0_0_0/* byval_arg */
	, &CapturingGroup_t2100_1_0_0/* this_arg */
	, &CapturingGroup_t2100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapturingGroup_t2100)/* instance_size */
	, sizeof (CapturingGroup_t2100)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BalancingGroup
extern TypeInfo BalancingGroup_t2101_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::.ctor()
MethodInfo BalancingGroup__ctor_m8585_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BalancingGroup__ctor_m8585/* method */
	, &BalancingGroup_t2101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CapturingGroup_t2100_0_0_0;
static ParameterInfo BalancingGroup_t2101_BalancingGroup_set_Balance_m8586_ParameterInfos[] = 
{
	{"value", 0, 134218473, 0, &CapturingGroup_t2100_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::set_Balance(System.Text.RegularExpressions.Syntax.CapturingGroup)
MethodInfo BalancingGroup_set_Balance_m8586_MethodInfo = 
{
	"set_Balance"/* name */
	, (methodPointerType)&BalancingGroup_set_Balance_m8586/* method */
	, &BalancingGroup_t2101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, BalancingGroup_t2101_BalancingGroup_set_Balance_m8586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo BalancingGroup_t2101_BalancingGroup_Compile_m8587_ParameterInfos[] = 
{
	{"cmp", 0, 134218474, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218475, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo BalancingGroup_Compile_m8587_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BalancingGroup_Compile_m8587/* method */
	, &BalancingGroup_t2101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, BalancingGroup_t2101_BalancingGroup_Compile_m8587_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BalancingGroup_t2101_MethodInfos[] =
{
	&BalancingGroup__ctor_m8585_MethodInfo,
	&BalancingGroup_set_Balance_m8586_MethodInfo,
	&BalancingGroup_Compile_m8587_MethodInfo,
	NULL
};
extern Il2CppType CapturingGroup_t2100_0_0_1;
FieldInfo BalancingGroup_t2101____balance_3_FieldInfo = 
{
	"balance"/* name */
	, &CapturingGroup_t2100_0_0_1/* type */
	, &BalancingGroup_t2101_il2cpp_TypeInfo/* parent */
	, offsetof(BalancingGroup_t2101, ___balance_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* BalancingGroup_t2101_FieldInfos[] =
{
	&BalancingGroup_t2101____balance_3_FieldInfo,
	NULL
};
extern MethodInfo BalancingGroup_set_Balance_m8586_MethodInfo;
static PropertyInfo BalancingGroup_t2101____Balance_PropertyInfo = 
{
	&BalancingGroup_t2101_il2cpp_TypeInfo/* parent */
	, "Balance"/* name */
	, NULL/* get */
	, &BalancingGroup_set_Balance_m8586_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* BalancingGroup_t2101_PropertyInfos[] =
{
	&BalancingGroup_t2101____Balance_PropertyInfo,
	NULL
};
extern MethodInfo BalancingGroup_Compile_m8587_MethodInfo;
static Il2CppMethodReference BalancingGroup_t2101_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&BalancingGroup_Compile_m8587_MethodInfo,
	&Group_GetWidth_m8571_MethodInfo,
	&Group_GetAnchorInfo_m8572_MethodInfo,
	&CapturingGroup_IsComplex_m8583_MethodInfo,
	&CapturingGroup_CompareTo_m8584_MethodInfo,
};
static bool BalancingGroup_t2101_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BalancingGroup_t2101_InterfacesOffsets[] = 
{
	{ &IComparable_t314_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType BalancingGroup_t2101_0_0_0;
extern Il2CppType BalancingGroup_t2101_1_0_0;
struct BalancingGroup_t2101;
const Il2CppTypeDefinitionMetadata BalancingGroup_t2101_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BalancingGroup_t2101_InterfacesOffsets/* interfaceOffsets */
	, &CapturingGroup_t2100_0_0_0/* parent */
	, BalancingGroup_t2101_VTable/* vtableMethods */
	, BalancingGroup_t2101_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo BalancingGroup_t2101_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BalancingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BalancingGroup_t2101_MethodInfos/* methods */
	, BalancingGroup_t2101_PropertyInfos/* properties */
	, BalancingGroup_t2101_FieldInfos/* fields */
	, NULL/* events */
	, &BalancingGroup_t2101_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BalancingGroup_t2101_0_0_0/* byval_arg */
	, &BalancingGroup_t2101_1_0_0/* this_arg */
	, &BalancingGroup_t2101_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BalancingGroup_t2101)/* instance_size */
	, sizeof (BalancingGroup_t2101)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktracking.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
extern TypeInfo NonBacktrackingGroup_t2102_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktrackingMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::.ctor()
MethodInfo NonBacktrackingGroup__ctor_m8588_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NonBacktrackingGroup__ctor_m8588/* method */
	, &NonBacktrackingGroup_t2102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo NonBacktrackingGroup_t2102_NonBacktrackingGroup_Compile_m8589_ParameterInfos[] = 
{
	{"cmp", 0, 134218476, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218477, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo NonBacktrackingGroup_Compile_m8589_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&NonBacktrackingGroup_Compile_m8589/* method */
	, &NonBacktrackingGroup_t2102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, NonBacktrackingGroup_t2102_NonBacktrackingGroup_Compile_m8589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::IsComplex()
MethodInfo NonBacktrackingGroup_IsComplex_m8590_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&NonBacktrackingGroup_IsComplex_m8590/* method */
	, &NonBacktrackingGroup_t2102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* NonBacktrackingGroup_t2102_MethodInfos[] =
{
	&NonBacktrackingGroup__ctor_m8588_MethodInfo,
	&NonBacktrackingGroup_Compile_m8589_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m8590_MethodInfo,
	NULL
};
extern MethodInfo NonBacktrackingGroup_Compile_m8589_MethodInfo;
extern MethodInfo NonBacktrackingGroup_IsComplex_m8590_MethodInfo;
static Il2CppMethodReference NonBacktrackingGroup_t2102_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&NonBacktrackingGroup_Compile_m8589_MethodInfo,
	&Group_GetWidth_m8571_MethodInfo,
	&Group_GetAnchorInfo_m8572_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m8590_MethodInfo,
};
static bool NonBacktrackingGroup_t2102_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType NonBacktrackingGroup_t2102_0_0_0;
extern Il2CppType NonBacktrackingGroup_t2102_1_0_0;
struct NonBacktrackingGroup_t2102;
const Il2CppTypeDefinitionMetadata NonBacktrackingGroup_t2102_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t2098_0_0_0/* parent */
	, NonBacktrackingGroup_t2102_VTable/* vtableMethods */
	, NonBacktrackingGroup_t2102_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo NonBacktrackingGroup_t2102_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonBacktrackingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NonBacktrackingGroup_t2102_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &NonBacktrackingGroup_t2102_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NonBacktrackingGroup_t2102_0_0_0/* byval_arg */
	, &NonBacktrackingGroup_t2102_1_0_0/* this_arg */
	, &NonBacktrackingGroup_t2102_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonBacktrackingGroup_t2102)/* instance_size */
	, sizeof (NonBacktrackingGroup_t2102)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_Repetition.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Repetition
extern TypeInfo Repetition_t2103_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_RepetitionMethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Repetition_t2103_Repetition__ctor_m8591_ParameterInfos[] = 
{
	{"min", 0, 134218478, 0, &Int32_t189_0_0_0},
	{"max", 1, 134218479, 0, &Int32_t189_0_0_0},
	{"lazy", 2, 134218480, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::.ctor(System.Int32,System.Int32,System.Boolean)
MethodInfo Repetition__ctor_m8591_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Repetition__ctor_m8591/* method */
	, &Repetition_t2103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236/* invoker_method */
	, Repetition_t2103_Repetition__ctor_m8591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Repetition::get_Expression()
MethodInfo Repetition_get_Expression_m8592_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&Repetition_get_Expression_m8592/* method */
	, &Repetition_t2103_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo Repetition_t2103_Repetition_set_Expression_m8593_ParameterInfos[] = 
{
	{"value", 0, 134218481, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::set_Expression(System.Text.RegularExpressions.Syntax.Expression)
MethodInfo Repetition_set_Expression_m8593_MethodInfo = 
{
	"set_Expression"/* name */
	, (methodPointerType)&Repetition_set_Expression_m8593/* method */
	, &Repetition_t2103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Repetition_t2103_Repetition_set_Expression_m8593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Repetition::get_Minimum()
MethodInfo Repetition_get_Minimum_m8594_MethodInfo = 
{
	"get_Minimum"/* name */
	, (methodPointerType)&Repetition_get_Minimum_m8594/* method */
	, &Repetition_t2103_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Repetition_t2103_Repetition_Compile_m8595_ParameterInfos[] = 
{
	{"cmp", 0, 134218482, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218483, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo Repetition_Compile_m8595_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Repetition_Compile_m8595/* method */
	, &Repetition_t2103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Repetition_t2103_Repetition_Compile_m8595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Repetition_t2103_Repetition_GetWidth_m8596_ParameterInfos[] = 
{
	{"min", 0, 134218484, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218485, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::GetWidth(System.Int32&,System.Int32&)
MethodInfo Repetition_GetWidth_m8596_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Repetition_GetWidth_m8596/* method */
	, &Repetition_t2103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Repetition_t2103_Repetition_GetWidth_m8596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Repetition_t2103_Repetition_GetAnchorInfo_m8597_ParameterInfos[] = 
{
	{"reverse", 0, 134218486, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType AnchorInfo_t2114_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Repetition::GetAnchorInfo(System.Boolean)
MethodInfo Repetition_GetAnchorInfo_m8597_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Repetition_GetAnchorInfo_m8597/* method */
	, &Repetition_t2103_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t2114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t236/* invoker_method */
	, Repetition_t2103_Repetition_GetAnchorInfo_m8597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Repetition_t2103_MethodInfos[] =
{
	&Repetition__ctor_m8591_MethodInfo,
	&Repetition_get_Expression_m8592_MethodInfo,
	&Repetition_set_Expression_m8593_MethodInfo,
	&Repetition_get_Minimum_m8594_MethodInfo,
	&Repetition_Compile_m8595_MethodInfo,
	&Repetition_GetWidth_m8596_MethodInfo,
	&Repetition_GetAnchorInfo_m8597_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Repetition_t2103____min_1_FieldInfo = 
{
	"min"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Repetition_t2103_il2cpp_TypeInfo/* parent */
	, offsetof(Repetition_t2103, ___min_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Repetition_t2103____max_2_FieldInfo = 
{
	"max"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Repetition_t2103_il2cpp_TypeInfo/* parent */
	, offsetof(Repetition_t2103, ___max_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Repetition_t2103____lazy_3_FieldInfo = 
{
	"lazy"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Repetition_t2103_il2cpp_TypeInfo/* parent */
	, offsetof(Repetition_t2103, ___lazy_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Repetition_t2103_FieldInfos[] =
{
	&Repetition_t2103____min_1_FieldInfo,
	&Repetition_t2103____max_2_FieldInfo,
	&Repetition_t2103____lazy_3_FieldInfo,
	NULL
};
extern MethodInfo Repetition_get_Expression_m8592_MethodInfo;
extern MethodInfo Repetition_set_Expression_m8593_MethodInfo;
static PropertyInfo Repetition_t2103____Expression_PropertyInfo = 
{
	&Repetition_t2103_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &Repetition_get_Expression_m8592_MethodInfo/* get */
	, &Repetition_set_Expression_m8593_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Repetition_get_Minimum_m8594_MethodInfo;
static PropertyInfo Repetition_t2103____Minimum_PropertyInfo = 
{
	&Repetition_t2103_il2cpp_TypeInfo/* parent */
	, "Minimum"/* name */
	, &Repetition_get_Minimum_m8594_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Repetition_t2103_PropertyInfos[] =
{
	&Repetition_t2103____Expression_PropertyInfo,
	&Repetition_t2103____Minimum_PropertyInfo,
	NULL
};
extern MethodInfo Repetition_Compile_m8595_MethodInfo;
extern MethodInfo Repetition_GetWidth_m8596_MethodInfo;
extern MethodInfo Repetition_GetAnchorInfo_m8597_MethodInfo;
static Il2CppMethodReference Repetition_t2103_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Repetition_Compile_m8595_MethodInfo,
	&Repetition_GetWidth_m8596_MethodInfo,
	&Repetition_GetAnchorInfo_m8597_MethodInfo,
	&CompositeExpression_IsComplex_m8567_MethodInfo,
};
static bool Repetition_t2103_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Repetition_t2103_0_0_0;
extern Il2CppType Repetition_t2103_1_0_0;
struct Repetition_t2103;
const Il2CppTypeDefinitionMetadata Repetition_t2103_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t2097_0_0_0/* parent */
	, Repetition_t2103_VTable/* vtableMethods */
	, Repetition_t2103_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Repetition_t2103_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Repetition"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Repetition_t2103_MethodInfos/* methods */
	, Repetition_t2103_PropertyInfos/* properties */
	, Repetition_t2103_FieldInfos/* fields */
	, NULL/* events */
	, &Repetition_t2103_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Repetition_t2103_0_0_0/* byval_arg */
	, &Repetition_t2103_1_0_0/* this_arg */
	, &Repetition_t2103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Repetition_t2103)/* instance_size */
	, sizeof (Repetition_t2103)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_Assertion.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Assertion
extern TypeInfo Assertion_t2104_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_AssertionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::.ctor()
MethodInfo Assertion__ctor_m8598_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Assertion__ctor_m8598/* method */
	, &Assertion_t2104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_TrueExpression()
MethodInfo Assertion_get_TrueExpression_m8599_MethodInfo = 
{
	"get_TrueExpression"/* name */
	, (methodPointerType)&Assertion_get_TrueExpression_m8599/* method */
	, &Assertion_t2104_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo Assertion_t2104_Assertion_set_TrueExpression_m8600_ParameterInfos[] = 
{
	{"value", 0, 134218487, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_TrueExpression(System.Text.RegularExpressions.Syntax.Expression)
MethodInfo Assertion_set_TrueExpression_m8600_MethodInfo = 
{
	"set_TrueExpression"/* name */
	, (methodPointerType)&Assertion_set_TrueExpression_m8600/* method */
	, &Assertion_t2104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Assertion_t2104_Assertion_set_TrueExpression_m8600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_FalseExpression()
MethodInfo Assertion_get_FalseExpression_m8601_MethodInfo = 
{
	"get_FalseExpression"/* name */
	, (methodPointerType)&Assertion_get_FalseExpression_m8601/* method */
	, &Assertion_t2104_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo Assertion_t2104_Assertion_set_FalseExpression_m8602_ParameterInfos[] = 
{
	{"value", 0, 134218488, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_FalseExpression(System.Text.RegularExpressions.Syntax.Expression)
MethodInfo Assertion_set_FalseExpression_m8602_MethodInfo = 
{
	"set_FalseExpression"/* name */
	, (methodPointerType)&Assertion_set_FalseExpression_m8602/* method */
	, &Assertion_t2104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Assertion_t2104_Assertion_set_FalseExpression_m8602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Assertion_t2104_Assertion_GetWidth_m8603_ParameterInfos[] = 
{
	{"min", 0, 134218489, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218490, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::GetWidth(System.Int32&,System.Int32&)
MethodInfo Assertion_GetWidth_m8603_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Assertion_GetWidth_m8603/* method */
	, &Assertion_t2104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Assertion_t2104_Assertion_GetWidth_m8603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Assertion_t2104_MethodInfos[] =
{
	&Assertion__ctor_m8598_MethodInfo,
	&Assertion_get_TrueExpression_m8599_MethodInfo,
	&Assertion_set_TrueExpression_m8600_MethodInfo,
	&Assertion_get_FalseExpression_m8601_MethodInfo,
	&Assertion_set_FalseExpression_m8602_MethodInfo,
	&Assertion_GetWidth_m8603_MethodInfo,
	NULL
};
extern MethodInfo Assertion_get_TrueExpression_m8599_MethodInfo;
extern MethodInfo Assertion_set_TrueExpression_m8600_MethodInfo;
static PropertyInfo Assertion_t2104____TrueExpression_PropertyInfo = 
{
	&Assertion_t2104_il2cpp_TypeInfo/* parent */
	, "TrueExpression"/* name */
	, &Assertion_get_TrueExpression_m8599_MethodInfo/* get */
	, &Assertion_set_TrueExpression_m8600_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Assertion_get_FalseExpression_m8601_MethodInfo;
extern MethodInfo Assertion_set_FalseExpression_m8602_MethodInfo;
static PropertyInfo Assertion_t2104____FalseExpression_PropertyInfo = 
{
	&Assertion_t2104_il2cpp_TypeInfo/* parent */
	, "FalseExpression"/* name */
	, &Assertion_get_FalseExpression_m8601_MethodInfo/* get */
	, &Assertion_set_FalseExpression_m8602_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Assertion_t2104_PropertyInfos[] =
{
	&Assertion_t2104____TrueExpression_PropertyInfo,
	&Assertion_t2104____FalseExpression_PropertyInfo,
	NULL
};
extern MethodInfo Assertion_GetWidth_m8603_MethodInfo;
static Il2CppMethodReference Assertion_t2104_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	NULL,
	&Assertion_GetWidth_m8603_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&CompositeExpression_IsComplex_m8567_MethodInfo,
};
static bool Assertion_t2104_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Assertion_t2104_1_0_0;
struct Assertion_t2104;
const Il2CppTypeDefinitionMetadata Assertion_t2104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t2097_0_0_0/* parent */
	, Assertion_t2104_VTable/* vtableMethods */
	, Assertion_t2104_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Assertion_t2104_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Assertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Assertion_t2104_MethodInfos/* methods */
	, Assertion_t2104_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Assertion_t2104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Assertion_t2104_0_0_0/* byval_arg */
	, &Assertion_t2104_1_0_0/* this_arg */
	, &Assertion_t2104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Assertion_t2104)/* instance_size */
	, sizeof (Assertion_t2104)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertio.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CaptureAssertion
extern TypeInfo CaptureAssertion_t2107_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertioMethodDeclarations.h"
extern Il2CppType Literal_t2106_0_0_0;
extern Il2CppType Literal_t2106_0_0_0;
static ParameterInfo CaptureAssertion_t2107_CaptureAssertion__ctor_m8604_ParameterInfos[] = 
{
	{"l", 0, 134218491, 0, &Literal_t2106_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::.ctor(System.Text.RegularExpressions.Syntax.Literal)
MethodInfo CaptureAssertion__ctor_m8604_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CaptureAssertion__ctor_m8604/* method */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, CaptureAssertion_t2107_CaptureAssertion__ctor_m8604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CapturingGroup_t2100_0_0_0;
static ParameterInfo CaptureAssertion_t2107_CaptureAssertion_set_CapturingGroup_m8605_ParameterInfos[] = 
{
	{"value", 0, 134218492, 0, &CapturingGroup_t2100_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
MethodInfo CaptureAssertion_set_CapturingGroup_m8605_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&CaptureAssertion_set_CapturingGroup_m8605/* method */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, CaptureAssertion_t2107_CaptureAssertion_set_CapturingGroup_m8605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo CaptureAssertion_t2107_CaptureAssertion_Compile_m8606_ParameterInfos[] = 
{
	{"cmp", 0, 134218493, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218494, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo CaptureAssertion_Compile_m8606_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CaptureAssertion_Compile_m8606/* method */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, CaptureAssertion_t2107_CaptureAssertion_Compile_m8606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CaptureAssertion::IsComplex()
MethodInfo CaptureAssertion_IsComplex_m8607_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CaptureAssertion_IsComplex_m8607/* method */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ExpressionAssertion_t2105_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionAssertion System.Text.RegularExpressions.Syntax.CaptureAssertion::get_Alternate()
MethodInfo CaptureAssertion_get_Alternate_m8608_MethodInfo = 
{
	"get_Alternate"/* name */
	, (methodPointerType)&CaptureAssertion_get_Alternate_m8608/* method */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionAssertion_t2105_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CaptureAssertion_t2107_MethodInfos[] =
{
	&CaptureAssertion__ctor_m8604_MethodInfo,
	&CaptureAssertion_set_CapturingGroup_m8605_MethodInfo,
	&CaptureAssertion_Compile_m8606_MethodInfo,
	&CaptureAssertion_IsComplex_m8607_MethodInfo,
	&CaptureAssertion_get_Alternate_m8608_MethodInfo,
	NULL
};
extern Il2CppType ExpressionAssertion_t2105_0_0_1;
FieldInfo CaptureAssertion_t2107____alternate_1_FieldInfo = 
{
	"alternate"/* name */
	, &ExpressionAssertion_t2105_0_0_1/* type */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* parent */
	, offsetof(CaptureAssertion_t2107, ___alternate_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CapturingGroup_t2100_0_0_1;
FieldInfo CaptureAssertion_t2107____group_2_FieldInfo = 
{
	"group"/* name */
	, &CapturingGroup_t2100_0_0_1/* type */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* parent */
	, offsetof(CaptureAssertion_t2107, ___group_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Literal_t2106_0_0_1;
FieldInfo CaptureAssertion_t2107____literal_3_FieldInfo = 
{
	"literal"/* name */
	, &Literal_t2106_0_0_1/* type */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* parent */
	, offsetof(CaptureAssertion_t2107, ___literal_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CaptureAssertion_t2107_FieldInfos[] =
{
	&CaptureAssertion_t2107____alternate_1_FieldInfo,
	&CaptureAssertion_t2107____group_2_FieldInfo,
	&CaptureAssertion_t2107____literal_3_FieldInfo,
	NULL
};
extern MethodInfo CaptureAssertion_set_CapturingGroup_m8605_MethodInfo;
static PropertyInfo CaptureAssertion_t2107____CapturingGroup_PropertyInfo = 
{
	&CaptureAssertion_t2107_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, NULL/* get */
	, &CaptureAssertion_set_CapturingGroup_m8605_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CaptureAssertion_get_Alternate_m8608_MethodInfo;
static PropertyInfo CaptureAssertion_t2107____Alternate_PropertyInfo = 
{
	&CaptureAssertion_t2107_il2cpp_TypeInfo/* parent */
	, "Alternate"/* name */
	, &CaptureAssertion_get_Alternate_m8608_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* CaptureAssertion_t2107_PropertyInfos[] =
{
	&CaptureAssertion_t2107____CapturingGroup_PropertyInfo,
	&CaptureAssertion_t2107____Alternate_PropertyInfo,
	NULL
};
extern MethodInfo CaptureAssertion_Compile_m8606_MethodInfo;
extern MethodInfo CaptureAssertion_IsComplex_m8607_MethodInfo;
static Il2CppMethodReference CaptureAssertion_t2107_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&CaptureAssertion_Compile_m8606_MethodInfo,
	&Assertion_GetWidth_m8603_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&CaptureAssertion_IsComplex_m8607_MethodInfo,
};
static bool CaptureAssertion_t2107_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType CaptureAssertion_t2107_0_0_0;
extern Il2CppType CaptureAssertion_t2107_1_0_0;
struct CaptureAssertion_t2107;
const Il2CppTypeDefinitionMetadata CaptureAssertion_t2107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t2104_0_0_0/* parent */
	, CaptureAssertion_t2107_VTable/* vtableMethods */
	, CaptureAssertion_t2107_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CaptureAssertion_t2107_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaptureAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CaptureAssertion_t2107_MethodInfos/* methods */
	, CaptureAssertion_t2107_PropertyInfos/* properties */
	, CaptureAssertion_t2107_FieldInfos/* fields */
	, NULL/* events */
	, &CaptureAssertion_t2107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CaptureAssertion_t2107_0_0_0/* byval_arg */
	, &CaptureAssertion_t2107_1_0_0/* this_arg */
	, &CaptureAssertion_t2107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaptureAssertion_t2107)/* instance_size */
	, sizeof (CaptureAssertion_t2107)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionAssertion
extern TypeInfo ExpressionAssertion_t2105_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsserMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::.ctor()
MethodInfo ExpressionAssertion__ctor_m8609_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionAssertion__ctor_m8609/* method */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ExpressionAssertion_t2105_ExpressionAssertion_set_Reverse_m8610_ParameterInfos[] = 
{
	{"value", 0, 134218495, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Reverse(System.Boolean)
MethodInfo ExpressionAssertion_set_Reverse_m8610_MethodInfo = 
{
	"set_Reverse"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Reverse_m8610/* method */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ExpressionAssertion_t2105_ExpressionAssertion_set_Reverse_m8610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ExpressionAssertion_t2105_ExpressionAssertion_set_Negate_m8611_ParameterInfos[] = 
{
	{"value", 0, 134218496, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Negate(System.Boolean)
MethodInfo ExpressionAssertion_set_Negate_m8611_MethodInfo = 
{
	"set_Negate"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Negate_m8611/* method */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ExpressionAssertion_t2105_ExpressionAssertion_set_Negate_m8611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionAssertion::get_TestExpression()
MethodInfo ExpressionAssertion_get_TestExpression_m8612_MethodInfo = 
{
	"get_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_get_TestExpression_m8612/* method */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t2096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo ExpressionAssertion_t2105_ExpressionAssertion_set_TestExpression_m8613_ParameterInfos[] = 
{
	{"value", 0, 134218497, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_TestExpression(System.Text.RegularExpressions.Syntax.Expression)
MethodInfo ExpressionAssertion_set_TestExpression_m8613_MethodInfo = 
{
	"set_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_set_TestExpression_m8613/* method */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ExpressionAssertion_t2105_ExpressionAssertion_set_TestExpression_m8613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ExpressionAssertion_t2105_ExpressionAssertion_Compile_m8614_ParameterInfos[] = 
{
	{"cmp", 0, 134218498, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218499, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo ExpressionAssertion_Compile_m8614_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&ExpressionAssertion_Compile_m8614/* method */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, ExpressionAssertion_t2105_ExpressionAssertion_Compile_m8614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::IsComplex()
MethodInfo ExpressionAssertion_IsComplex_m8615_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&ExpressionAssertion_IsComplex_m8615/* method */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ExpressionAssertion_t2105_MethodInfos[] =
{
	&ExpressionAssertion__ctor_m8609_MethodInfo,
	&ExpressionAssertion_set_Reverse_m8610_MethodInfo,
	&ExpressionAssertion_set_Negate_m8611_MethodInfo,
	&ExpressionAssertion_get_TestExpression_m8612_MethodInfo,
	&ExpressionAssertion_set_TestExpression_m8613_MethodInfo,
	&ExpressionAssertion_Compile_m8614_MethodInfo,
	&ExpressionAssertion_IsComplex_m8615_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ExpressionAssertion_t2105____reverse_1_FieldInfo = 
{
	"reverse"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* parent */
	, offsetof(ExpressionAssertion_t2105, ___reverse_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ExpressionAssertion_t2105____negate_2_FieldInfo = 
{
	"negate"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* parent */
	, offsetof(ExpressionAssertion_t2105, ___negate_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ExpressionAssertion_t2105_FieldInfos[] =
{
	&ExpressionAssertion_t2105____reverse_1_FieldInfo,
	&ExpressionAssertion_t2105____negate_2_FieldInfo,
	NULL
};
extern MethodInfo ExpressionAssertion_set_Reverse_m8610_MethodInfo;
static PropertyInfo ExpressionAssertion_t2105____Reverse_PropertyInfo = 
{
	&ExpressionAssertion_t2105_il2cpp_TypeInfo/* parent */
	, "Reverse"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Reverse_m8610_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ExpressionAssertion_set_Negate_m8611_MethodInfo;
static PropertyInfo ExpressionAssertion_t2105____Negate_PropertyInfo = 
{
	&ExpressionAssertion_t2105_il2cpp_TypeInfo/* parent */
	, "Negate"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Negate_m8611_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ExpressionAssertion_get_TestExpression_m8612_MethodInfo;
extern MethodInfo ExpressionAssertion_set_TestExpression_m8613_MethodInfo;
static PropertyInfo ExpressionAssertion_t2105____TestExpression_PropertyInfo = 
{
	&ExpressionAssertion_t2105_il2cpp_TypeInfo/* parent */
	, "TestExpression"/* name */
	, &ExpressionAssertion_get_TestExpression_m8612_MethodInfo/* get */
	, &ExpressionAssertion_set_TestExpression_m8613_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ExpressionAssertion_t2105_PropertyInfos[] =
{
	&ExpressionAssertion_t2105____Reverse_PropertyInfo,
	&ExpressionAssertion_t2105____Negate_PropertyInfo,
	&ExpressionAssertion_t2105____TestExpression_PropertyInfo,
	NULL
};
extern MethodInfo ExpressionAssertion_Compile_m8614_MethodInfo;
extern MethodInfo ExpressionAssertion_IsComplex_m8615_MethodInfo;
static Il2CppMethodReference ExpressionAssertion_t2105_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ExpressionAssertion_Compile_m8614_MethodInfo,
	&Assertion_GetWidth_m8603_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&ExpressionAssertion_IsComplex_m8615_MethodInfo,
};
static bool ExpressionAssertion_t2105_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType ExpressionAssertion_t2105_1_0_0;
struct ExpressionAssertion_t2105;
const Il2CppTypeDefinitionMetadata ExpressionAssertion_t2105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t2104_0_0_0/* parent */
	, ExpressionAssertion_t2105_VTable/* vtableMethods */
	, ExpressionAssertion_t2105_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ExpressionAssertion_t2105_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionAssertion_t2105_MethodInfos/* methods */
	, ExpressionAssertion_t2105_PropertyInfos/* properties */
	, ExpressionAssertion_t2105_FieldInfos/* fields */
	, NULL/* events */
	, &ExpressionAssertion_t2105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExpressionAssertion_t2105_0_0_0/* byval_arg */
	, &ExpressionAssertion_t2105_1_0_0/* this_arg */
	, &ExpressionAssertion_t2105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionAssertion_t2105)/* instance_size */
	, sizeof (ExpressionAssertion_t2105)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_Alternation.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Alternation
extern TypeInfo Alternation_t2108_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_AlternationMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::.ctor()
MethodInfo Alternation__ctor_m8616_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Alternation__ctor_m8616/* method */
	, &Alternation_t2108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ExpressionCollection_t2095_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.Alternation::get_Alternatives()
MethodInfo Alternation_get_Alternatives_m8617_MethodInfo = 
{
	"get_Alternatives"/* name */
	, (methodPointerType)&Alternation_get_Alternatives_m8617/* method */
	, &Alternation_t2108_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t2095_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
static ParameterInfo Alternation_t2108_Alternation_AddAlternative_m8618_ParameterInfos[] = 
{
	{"e", 0, 134218500, 0, &Expression_t2096_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::AddAlternative(System.Text.RegularExpressions.Syntax.Expression)
MethodInfo Alternation_AddAlternative_m8618_MethodInfo = 
{
	"AddAlternative"/* name */
	, (methodPointerType)&Alternation_AddAlternative_m8618/* method */
	, &Alternation_t2108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Alternation_t2108_Alternation_AddAlternative_m8618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Alternation_t2108_Alternation_Compile_m8619_ParameterInfos[] = 
{
	{"cmp", 0, 134218501, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218502, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo Alternation_Compile_m8619_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Alternation_Compile_m8619/* method */
	, &Alternation_t2108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Alternation_t2108_Alternation_Compile_m8619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Alternation_t2108_Alternation_GetWidth_m8620_ParameterInfos[] = 
{
	{"min", 0, 134218503, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218504, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::GetWidth(System.Int32&,System.Int32&)
MethodInfo Alternation_GetWidth_m8620_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Alternation_GetWidth_m8620/* method */
	, &Alternation_t2108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Alternation_t2108_Alternation_GetWidth_m8620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Alternation_t2108_MethodInfos[] =
{
	&Alternation__ctor_m8616_MethodInfo,
	&Alternation_get_Alternatives_m8617_MethodInfo,
	&Alternation_AddAlternative_m8618_MethodInfo,
	&Alternation_Compile_m8619_MethodInfo,
	&Alternation_GetWidth_m8620_MethodInfo,
	NULL
};
extern MethodInfo Alternation_get_Alternatives_m8617_MethodInfo;
static PropertyInfo Alternation_t2108____Alternatives_PropertyInfo = 
{
	&Alternation_t2108_il2cpp_TypeInfo/* parent */
	, "Alternatives"/* name */
	, &Alternation_get_Alternatives_m8617_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Alternation_t2108_PropertyInfos[] =
{
	&Alternation_t2108____Alternatives_PropertyInfo,
	NULL
};
extern MethodInfo Alternation_Compile_m8619_MethodInfo;
extern MethodInfo Alternation_GetWidth_m8620_MethodInfo;
static Il2CppMethodReference Alternation_t2108_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Alternation_Compile_m8619_MethodInfo,
	&Alternation_GetWidth_m8620_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&CompositeExpression_IsComplex_m8567_MethodInfo,
};
static bool Alternation_t2108_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Alternation_t2108_0_0_0;
extern Il2CppType Alternation_t2108_1_0_0;
struct Alternation_t2108;
const Il2CppTypeDefinitionMetadata Alternation_t2108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t2097_0_0_0/* parent */
	, Alternation_t2108_VTable/* vtableMethods */
	, Alternation_t2108_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Alternation_t2108_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Alternation"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Alternation_t2108_MethodInfos/* methods */
	, Alternation_t2108_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Alternation_t2108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Alternation_t2108_0_0_0/* byval_arg */
	, &Alternation_t2108_1_0_0/* this_arg */
	, &Alternation_t2108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Alternation_t2108)/* instance_size */
	, sizeof (Alternation_t2108)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_Literal.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Literal
extern TypeInfo Literal_t2106_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_LiteralMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Literal_t2106_Literal__ctor_m8621_ParameterInfos[] = 
{
	{"str", 0, 134218505, 0, &String_t_0_0_0},
	{"ignore", 1, 134218506, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::.ctor(System.String,System.Boolean)
MethodInfo Literal__ctor_m8621_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Literal__ctor_m8621/* method */
	, &Literal_t2106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Literal_t2106_Literal__ctor_m8621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Literal_t2106_Literal_CompileLiteral_m8622_ParameterInfos[] = 
{
	{"str", 0, 134218507, 0, &String_t_0_0_0},
	{"cmp", 1, 134218508, 0, &ICompiler_t2137_0_0_0},
	{"ignore", 2, 134218509, 0, &Boolean_t203_0_0_0},
	{"reverse", 3, 134218510, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::CompileLiteral(System.String,System.Text.RegularExpressions.ICompiler,System.Boolean,System.Boolean)
MethodInfo Literal_CompileLiteral_m8622_MethodInfo = 
{
	"CompileLiteral"/* name */
	, (methodPointerType)&Literal_CompileLiteral_m8622/* method */
	, &Literal_t2106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_SByte_t236/* invoker_method */
	, Literal_t2106_Literal_CompileLiteral_m8622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Literal_t2106_Literal_Compile_m8623_ParameterInfos[] = 
{
	{"cmp", 0, 134218511, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218512, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo Literal_Compile_m8623_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Literal_Compile_m8623/* method */
	, &Literal_t2106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Literal_t2106_Literal_Compile_m8623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Literal_t2106_Literal_GetWidth_m8624_ParameterInfos[] = 
{
	{"min", 0, 134218513, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218514, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::GetWidth(System.Int32&,System.Int32&)
MethodInfo Literal_GetWidth_m8624_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Literal_GetWidth_m8624/* method */
	, &Literal_t2106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Literal_t2106_Literal_GetWidth_m8624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Literal_t2106_Literal_GetAnchorInfo_m8625_ParameterInfos[] = 
{
	{"reverse", 0, 134218515, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType AnchorInfo_t2114_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Literal::GetAnchorInfo(System.Boolean)
MethodInfo Literal_GetAnchorInfo_m8625_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Literal_GetAnchorInfo_m8625/* method */
	, &Literal_t2106_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t2114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t236/* invoker_method */
	, Literal_t2106_Literal_GetAnchorInfo_m8625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Literal::IsComplex()
MethodInfo Literal_IsComplex_m8626_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Literal_IsComplex_m8626/* method */
	, &Literal_t2106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Literal_t2106_MethodInfos[] =
{
	&Literal__ctor_m8621_MethodInfo,
	&Literal_CompileLiteral_m8622_MethodInfo,
	&Literal_Compile_m8623_MethodInfo,
	&Literal_GetWidth_m8624_MethodInfo,
	&Literal_GetAnchorInfo_m8625_MethodInfo,
	&Literal_IsComplex_m8626_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo Literal_t2106____str_0_FieldInfo = 
{
	"str"/* name */
	, &String_t_0_0_1/* type */
	, &Literal_t2106_il2cpp_TypeInfo/* parent */
	, offsetof(Literal_t2106, ___str_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Literal_t2106____ignore_1_FieldInfo = 
{
	"ignore"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Literal_t2106_il2cpp_TypeInfo/* parent */
	, offsetof(Literal_t2106, ___ignore_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Literal_t2106_FieldInfos[] =
{
	&Literal_t2106____str_0_FieldInfo,
	&Literal_t2106____ignore_1_FieldInfo,
	NULL
};
extern MethodInfo Literal_Compile_m8623_MethodInfo;
extern MethodInfo Literal_GetWidth_m8624_MethodInfo;
extern MethodInfo Literal_GetAnchorInfo_m8625_MethodInfo;
extern MethodInfo Literal_IsComplex_m8626_MethodInfo;
static Il2CppMethodReference Literal_t2106_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Literal_Compile_m8623_MethodInfo,
	&Literal_GetWidth_m8624_MethodInfo,
	&Literal_GetAnchorInfo_m8625_MethodInfo,
	&Literal_IsComplex_m8626_MethodInfo,
};
static bool Literal_t2106_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Literal_t2106_1_0_0;
struct Literal_t2106;
const Il2CppTypeDefinitionMetadata Literal_t2106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t2096_0_0_0/* parent */
	, Literal_t2106_VTable/* vtableMethods */
	, Literal_t2106_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Literal_t2106_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Literal"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Literal_t2106_MethodInfos/* methods */
	, NULL/* properties */
	, Literal_t2106_FieldInfos/* fields */
	, NULL/* events */
	, &Literal_t2106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Literal_t2106_0_0_0/* byval_arg */
	, &Literal_t2106_1_0_0/* this_arg */
	, &Literal_t2106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Literal_t2106)/* instance_size */
	, sizeof (Literal_t2106)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAsserti.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.PositionAssertion
extern TypeInfo PositionAssertion_t2109_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAssertiMethodDeclarations.h"
extern Il2CppType Position_t2071_0_0_0;
static ParameterInfo PositionAssertion_t2109_PositionAssertion__ctor_m8627_ParameterInfos[] = 
{
	{"pos", 0, 134218516, 0, &Position_t2071_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::.ctor(System.Text.RegularExpressions.Position)
MethodInfo PositionAssertion__ctor_m8627_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAssertion__ctor_m8627/* method */
	, &PositionAssertion_t2109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194/* invoker_method */
	, PositionAssertion_t2109_PositionAssertion__ctor_m8627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PositionAssertion_t2109_PositionAssertion_Compile_m8628_ParameterInfos[] = 
{
	{"cmp", 0, 134218517, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218518, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo PositionAssertion_Compile_m8628_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&PositionAssertion_Compile_m8628/* method */
	, &PositionAssertion_t2109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, PositionAssertion_t2109_PositionAssertion_Compile_m8628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo PositionAssertion_t2109_PositionAssertion_GetWidth_m8629_ParameterInfos[] = 
{
	{"min", 0, 134218519, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218520, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::GetWidth(System.Int32&,System.Int32&)
MethodInfo PositionAssertion_GetWidth_m8629_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&PositionAssertion_GetWidth_m8629/* method */
	, &PositionAssertion_t2109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, PositionAssertion_t2109_PositionAssertion_GetWidth_m8629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.PositionAssertion::IsComplex()
MethodInfo PositionAssertion_IsComplex_m8630_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&PositionAssertion_IsComplex_m8630/* method */
	, &PositionAssertion_t2109_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo PositionAssertion_t2109_PositionAssertion_GetAnchorInfo_m8631_ParameterInfos[] = 
{
	{"revers", 0, 134218521, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType AnchorInfo_t2114_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.PositionAssertion::GetAnchorInfo(System.Boolean)
MethodInfo PositionAssertion_GetAnchorInfo_m8631_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&PositionAssertion_GetAnchorInfo_m8631/* method */
	, &PositionAssertion_t2109_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t2114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t236/* invoker_method */
	, PositionAssertion_t2109_PositionAssertion_GetAnchorInfo_m8631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PositionAssertion_t2109_MethodInfos[] =
{
	&PositionAssertion__ctor_m8627_MethodInfo,
	&PositionAssertion_Compile_m8628_MethodInfo,
	&PositionAssertion_GetWidth_m8629_MethodInfo,
	&PositionAssertion_IsComplex_m8630_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m8631_MethodInfo,
	NULL
};
extern Il2CppType Position_t2071_0_0_1;
FieldInfo PositionAssertion_t2109____pos_0_FieldInfo = 
{
	"pos"/* name */
	, &Position_t2071_0_0_1/* type */
	, &PositionAssertion_t2109_il2cpp_TypeInfo/* parent */
	, offsetof(PositionAssertion_t2109, ___pos_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* PositionAssertion_t2109_FieldInfos[] =
{
	&PositionAssertion_t2109____pos_0_FieldInfo,
	NULL
};
extern MethodInfo PositionAssertion_Compile_m8628_MethodInfo;
extern MethodInfo PositionAssertion_GetWidth_m8629_MethodInfo;
extern MethodInfo PositionAssertion_GetAnchorInfo_m8631_MethodInfo;
extern MethodInfo PositionAssertion_IsComplex_m8630_MethodInfo;
static Il2CppMethodReference PositionAssertion_t2109_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&PositionAssertion_Compile_m8628_MethodInfo,
	&PositionAssertion_GetWidth_m8629_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m8631_MethodInfo,
	&PositionAssertion_IsComplex_m8630_MethodInfo,
};
static bool PositionAssertion_t2109_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType PositionAssertion_t2109_0_0_0;
extern Il2CppType PositionAssertion_t2109_1_0_0;
struct PositionAssertion_t2109;
const Il2CppTypeDefinitionMetadata PositionAssertion_t2109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t2096_0_0_0/* parent */
	, PositionAssertion_t2109_VTable/* vtableMethods */
	, PositionAssertion_t2109_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PositionAssertion_t2109_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, PositionAssertion_t2109_MethodInfos/* methods */
	, NULL/* properties */
	, PositionAssertion_t2109_FieldInfos/* fields */
	, NULL/* events */
	, &PositionAssertion_t2109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PositionAssertion_t2109_0_0_0/* byval_arg */
	, &PositionAssertion_t2109_1_0_0/* this_arg */
	, &PositionAssertion_t2109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAssertion_t2109)/* instance_size */
	, sizeof (PositionAssertion_t2109)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_Reference.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Reference
extern TypeInfo Reference_t2110_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_ReferenceMethodDeclarations.h"
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Reference_t2110_Reference__ctor_m8632_ParameterInfos[] = 
{
	{"ignore", 0, 134218522, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::.ctor(System.Boolean)
MethodInfo Reference__ctor_m8632_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Reference__ctor_m8632/* method */
	, &Reference_t2110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Reference_t2110_Reference__ctor_m8632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CapturingGroup_t2100_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.Reference::get_CapturingGroup()
MethodInfo Reference_get_CapturingGroup_m8633_MethodInfo = 
{
	"get_CapturingGroup"/* name */
	, (methodPointerType)&Reference_get_CapturingGroup_m8633/* method */
	, &Reference_t2110_il2cpp_TypeInfo/* declaring_type */
	, &CapturingGroup_t2100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CapturingGroup_t2100_0_0_0;
static ParameterInfo Reference_t2110_Reference_set_CapturingGroup_m8634_ParameterInfos[] = 
{
	{"value", 0, 134218523, 0, &CapturingGroup_t2100_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
MethodInfo Reference_set_CapturingGroup_m8634_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&Reference_set_CapturingGroup_m8634/* method */
	, &Reference_t2110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Reference_t2110_Reference_set_CapturingGroup_m8634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::get_IgnoreCase()
MethodInfo Reference_get_IgnoreCase_m8635_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&Reference_get_IgnoreCase_m8635/* method */
	, &Reference_t2110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Reference_t2110_Reference_Compile_m8636_ParameterInfos[] = 
{
	{"cmp", 0, 134218524, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218525, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo Reference_Compile_m8636_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Reference_Compile_m8636/* method */
	, &Reference_t2110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Reference_t2110_Reference_Compile_m8636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo Reference_t2110_Reference_GetWidth_m8637_ParameterInfos[] = 
{
	{"min", 0, 134218526, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218527, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::GetWidth(System.Int32&,System.Int32&)
MethodInfo Reference_GetWidth_m8637_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Reference_GetWidth_m8637/* method */
	, &Reference_t2110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, Reference_t2110_Reference_GetWidth_m8637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::IsComplex()
MethodInfo Reference_IsComplex_m8638_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Reference_IsComplex_m8638/* method */
	, &Reference_t2110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Reference_t2110_MethodInfos[] =
{
	&Reference__ctor_m8632_MethodInfo,
	&Reference_get_CapturingGroup_m8633_MethodInfo,
	&Reference_set_CapturingGroup_m8634_MethodInfo,
	&Reference_get_IgnoreCase_m8635_MethodInfo,
	&Reference_Compile_m8636_MethodInfo,
	&Reference_GetWidth_m8637_MethodInfo,
	&Reference_IsComplex_m8638_MethodInfo,
	NULL
};
extern Il2CppType CapturingGroup_t2100_0_0_1;
FieldInfo Reference_t2110____group_0_FieldInfo = 
{
	"group"/* name */
	, &CapturingGroup_t2100_0_0_1/* type */
	, &Reference_t2110_il2cpp_TypeInfo/* parent */
	, offsetof(Reference_t2110, ___group_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Reference_t2110____ignore_1_FieldInfo = 
{
	"ignore"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Reference_t2110_il2cpp_TypeInfo/* parent */
	, offsetof(Reference_t2110, ___ignore_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Reference_t2110_FieldInfos[] =
{
	&Reference_t2110____group_0_FieldInfo,
	&Reference_t2110____ignore_1_FieldInfo,
	NULL
};
extern MethodInfo Reference_get_CapturingGroup_m8633_MethodInfo;
extern MethodInfo Reference_set_CapturingGroup_m8634_MethodInfo;
static PropertyInfo Reference_t2110____CapturingGroup_PropertyInfo = 
{
	&Reference_t2110_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, &Reference_get_CapturingGroup_m8633_MethodInfo/* get */
	, &Reference_set_CapturingGroup_m8634_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Reference_get_IgnoreCase_m8635_MethodInfo;
static PropertyInfo Reference_t2110____IgnoreCase_PropertyInfo = 
{
	&Reference_t2110_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &Reference_get_IgnoreCase_m8635_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Reference_t2110_PropertyInfos[] =
{
	&Reference_t2110____CapturingGroup_PropertyInfo,
	&Reference_t2110____IgnoreCase_PropertyInfo,
	NULL
};
extern MethodInfo Reference_Compile_m8636_MethodInfo;
extern MethodInfo Reference_GetWidth_m8637_MethodInfo;
extern MethodInfo Reference_IsComplex_m8638_MethodInfo;
static Il2CppMethodReference Reference_t2110_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Reference_Compile_m8636_MethodInfo,
	&Reference_GetWidth_m8637_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&Reference_IsComplex_m8638_MethodInfo,
};
static bool Reference_t2110_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Reference_t2110_0_0_0;
extern Il2CppType Reference_t2110_1_0_0;
struct Reference_t2110;
const Il2CppTypeDefinitionMetadata Reference_t2110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t2096_0_0_0/* parent */
	, Reference_t2110_VTable/* vtableMethods */
	, Reference_t2110_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Reference_t2110_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Reference"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Reference_t2110_MethodInfos/* methods */
	, Reference_t2110_PropertyInfos/* properties */
	, Reference_t2110_FieldInfos/* fields */
	, NULL/* events */
	, &Reference_t2110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Reference_t2110_0_0_0/* byval_arg */
	, &Reference_t2110_1_0_0/* this_arg */
	, &Reference_t2110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Reference_t2110)/* instance_size */
	, sizeof (Reference_t2110)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumber.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BackslashNumber
extern TypeInfo BackslashNumber_t2111_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumberMethodDeclarations.h"
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo BackslashNumber_t2111_BackslashNumber__ctor_m8639_ParameterInfos[] = 
{
	{"ignore", 0, 134218528, 0, &Boolean_t203_0_0_0},
	{"ecma", 1, 134218529, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::.ctor(System.Boolean,System.Boolean)
MethodInfo BackslashNumber__ctor_m8639_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BackslashNumber__ctor_m8639/* method */
	, &BackslashNumber_t2111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236_SByte_t236/* invoker_method */
	, BackslashNumber_t2111_BackslashNumber__ctor_m8639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Hashtable_t1667_0_0_0;
static ParameterInfo BackslashNumber_t2111_BackslashNumber_ResolveReference_m8640_ParameterInfos[] = 
{
	{"num_str", 0, 134218530, 0, &String_t_0_0_0},
	{"groups", 1, 134218531, 0, &Hashtable_t1667_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.BackslashNumber::ResolveReference(System.String,System.Collections.Hashtable)
MethodInfo BackslashNumber_ResolveReference_m8640_MethodInfo = 
{
	"ResolveReference"/* name */
	, (methodPointerType)&BackslashNumber_ResolveReference_m8640/* method */
	, &BackslashNumber_t2111_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, BackslashNumber_t2111_BackslashNumber_ResolveReference_m8640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo BackslashNumber_t2111_BackslashNumber_Compile_m8641_ParameterInfos[] = 
{
	{"cmp", 0, 134218532, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218533, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo BackslashNumber_Compile_m8641_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BackslashNumber_Compile_m8641/* method */
	, &BackslashNumber_t2111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, BackslashNumber_t2111_BackslashNumber_Compile_m8641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BackslashNumber_t2111_MethodInfos[] =
{
	&BackslashNumber__ctor_m8639_MethodInfo,
	&BackslashNumber_ResolveReference_m8640_MethodInfo,
	&BackslashNumber_Compile_m8641_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo BackslashNumber_t2111____literal_2_FieldInfo = 
{
	"literal"/* name */
	, &String_t_0_0_1/* type */
	, &BackslashNumber_t2111_il2cpp_TypeInfo/* parent */
	, offsetof(BackslashNumber_t2111, ___literal_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo BackslashNumber_t2111____ecma_3_FieldInfo = 
{
	"ecma"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &BackslashNumber_t2111_il2cpp_TypeInfo/* parent */
	, offsetof(BackslashNumber_t2111, ___ecma_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* BackslashNumber_t2111_FieldInfos[] =
{
	&BackslashNumber_t2111____literal_2_FieldInfo,
	&BackslashNumber_t2111____ecma_3_FieldInfo,
	NULL
};
extern MethodInfo BackslashNumber_Compile_m8641_MethodInfo;
static Il2CppMethodReference BackslashNumber_t2111_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&BackslashNumber_Compile_m8641_MethodInfo,
	&Reference_GetWidth_m8637_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&Reference_IsComplex_m8638_MethodInfo,
};
static bool BackslashNumber_t2111_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType BackslashNumber_t2111_0_0_0;
extern Il2CppType BackslashNumber_t2111_1_0_0;
struct BackslashNumber_t2111;
const Il2CppTypeDefinitionMetadata BackslashNumber_t2111_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Reference_t2110_0_0_0/* parent */
	, BackslashNumber_t2111_VTable/* vtableMethods */
	, BackslashNumber_t2111_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo BackslashNumber_t2111_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackslashNumber"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BackslashNumber_t2111_MethodInfos/* methods */
	, NULL/* properties */
	, BackslashNumber_t2111_FieldInfos/* fields */
	, NULL/* events */
	, &BackslashNumber_t2111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackslashNumber_t2111_0_0_0/* byval_arg */
	, &BackslashNumber_t2111_1_0_0/* this_arg */
	, &BackslashNumber_t2111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackslashNumber_t2111)/* instance_size */
	, sizeof (BackslashNumber_t2111)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClass.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CharacterClass
extern TypeInfo CharacterClass_t2113_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClassMethodDeclarations.h"
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo CharacterClass_t2113_CharacterClass__ctor_m8642_ParameterInfos[] = 
{
	{"negate", 0, 134218534, 0, &Boolean_t203_0_0_0},
	{"ignore", 1, 134218535, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Boolean,System.Boolean)
MethodInfo CharacterClass__ctor_m8642_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m8642/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236_SByte_t236/* invoker_method */
	, CharacterClass_t2113_CharacterClass__ctor_m8642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo CharacterClass_t2113_CharacterClass__ctor_m8643_ParameterInfos[] = 
{
	{"cat", 0, 134218536, 0, &Category_t2075_0_0_0},
	{"negate", 1, 134218537, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Text.RegularExpressions.Category,System.Boolean)
MethodInfo CharacterClass__ctor_m8643_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m8643/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236/* invoker_method */
	, CharacterClass_t2113_CharacterClass__ctor_m8643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.cctor()
MethodInfo CharacterClass__cctor_m8644_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CharacterClass__cctor_m8644/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Category_t2075_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo CharacterClass_t2113_CharacterClass_AddCategory_m8645_ParameterInfos[] = 
{
	{"cat", 0, 134218538, 0, &Category_t2075_0_0_0},
	{"negate", 1, 134218539, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCategory(System.Text.RegularExpressions.Category,System.Boolean)
MethodInfo CharacterClass_AddCategory_m8645_MethodInfo = 
{
	"AddCategory"/* name */
	, (methodPointerType)&CharacterClass_AddCategory_m8645/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236/* invoker_method */
	, CharacterClass_t2113_CharacterClass_AddCategory_m8645_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo CharacterClass_t2113_CharacterClass_AddCharacter_m8646_ParameterInfos[] = 
{
	{"c", 0, 134218540, 0, &Char_t193_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCharacter(System.Char)
MethodInfo CharacterClass_AddCharacter_m8646_MethodInfo = 
{
	"AddCharacter"/* name */
	, (methodPointerType)&CharacterClass_AddCharacter_m8646/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238/* invoker_method */
	, CharacterClass_t2113_CharacterClass_AddCharacter_m8646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo CharacterClass_t2113_CharacterClass_AddRange_m8647_ParameterInfos[] = 
{
	{"lo", 0, 134218541, 0, &Char_t193_0_0_0},
	{"hi", 1, 134218542, 0, &Char_t193_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddRange(System.Char,System.Char)
MethodInfo CharacterClass_AddRange_m8647_MethodInfo = 
{
	"AddRange"/* name */
	, (methodPointerType)&CharacterClass_AddRange_m8647/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238_Int16_t238/* invoker_method */
	, CharacterClass_t2113_CharacterClass_AddRange_m8647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICompiler_t2137_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo CharacterClass_t2113_CharacterClass_Compile_m8648_ParameterInfos[] = 
{
	{"cmp", 0, 134218543, 0, &ICompiler_t2137_0_0_0},
	{"reverse", 1, 134218544, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
MethodInfo CharacterClass_Compile_m8648_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CharacterClass_Compile_m8648/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, CharacterClass_t2113_CharacterClass_Compile_m8648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_1_0_2;
extern Il2CppType Int32_t189_1_0_2;
static ParameterInfo CharacterClass_t2113_CharacterClass_GetWidth_m8649_ParameterInfos[] = 
{
	{"min", 0, 134218545, 0, &Int32_t189_1_0_2},
	{"max", 1, 134218546, 0, &Int32_t189_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::GetWidth(System.Int32&,System.Int32&)
MethodInfo CharacterClass_GetWidth_m8649_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CharacterClass_GetWidth_m8649/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317/* invoker_method */
	, CharacterClass_t2113_CharacterClass_GetWidth_m8649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CharacterClass::IsComplex()
MethodInfo CharacterClass_IsComplex_m8650_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CharacterClass_IsComplex_m8650/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Interval_t2090_0_0_0;
static ParameterInfo CharacterClass_t2113_CharacterClass_GetIntervalCost_m8651_ParameterInfos[] = 
{
	{"i", 0, 134218547, 0, &Interval_t2090_0_0_0},
};
extern Il2CppType Double_t234_0_0_0;
extern void* RuntimeInvoker_Double_t234_Interval_t2090 (MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.Syntax.CharacterClass::GetIntervalCost(System.Text.RegularExpressions.Interval)
MethodInfo CharacterClass_GetIntervalCost_m8651_MethodInfo = 
{
	"GetIntervalCost"/* name */
	, (methodPointerType)&CharacterClass_GetIntervalCost_m8651/* method */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* declaring_type */
	, &Double_t234_0_0_0/* return_type */
	, RuntimeInvoker_Double_t234_Interval_t2090/* invoker_method */
	, CharacterClass_t2113_CharacterClass_GetIntervalCost_m8651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CharacterClass_t2113_MethodInfos[] =
{
	&CharacterClass__ctor_m8642_MethodInfo,
	&CharacterClass__ctor_m8643_MethodInfo,
	&CharacterClass__cctor_m8644_MethodInfo,
	&CharacterClass_AddCategory_m8645_MethodInfo,
	&CharacterClass_AddCharacter_m8646_MethodInfo,
	&CharacterClass_AddRange_m8647_MethodInfo,
	&CharacterClass_Compile_m8648_MethodInfo,
	&CharacterClass_GetWidth_m8649_MethodInfo,
	&CharacterClass_IsComplex_m8650_MethodInfo,
	&CharacterClass_GetIntervalCost_m8651_MethodInfo,
	NULL
};
extern Il2CppType Interval_t2090_0_0_17;
FieldInfo CharacterClass_t2113____upper_case_characters_0_FieldInfo = 
{
	"upper_case_characters"/* name */
	, &Interval_t2090_0_0_17/* type */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* parent */
	, offsetof(CharacterClass_t2113_StaticFields, ___upper_case_characters_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo CharacterClass_t2113____negate_1_FieldInfo = 
{
	"negate"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* parent */
	, offsetof(CharacterClass_t2113, ___negate_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo CharacterClass_t2113____ignore_2_FieldInfo = 
{
	"ignore"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* parent */
	, offsetof(CharacterClass_t2113, ___ignore_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BitArray_t2112_0_0_1;
FieldInfo CharacterClass_t2113____pos_cats_3_FieldInfo = 
{
	"pos_cats"/* name */
	, &BitArray_t2112_0_0_1/* type */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* parent */
	, offsetof(CharacterClass_t2113, ___pos_cats_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BitArray_t2112_0_0_1;
FieldInfo CharacterClass_t2113____neg_cats_4_FieldInfo = 
{
	"neg_cats"/* name */
	, &BitArray_t2112_0_0_1/* type */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* parent */
	, offsetof(CharacterClass_t2113, ___neg_cats_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IntervalCollection_t2093_0_0_1;
FieldInfo CharacterClass_t2113____intervals_5_FieldInfo = 
{
	"intervals"/* name */
	, &IntervalCollection_t2093_0_0_1/* type */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* parent */
	, offsetof(CharacterClass_t2113, ___intervals_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CharacterClass_t2113_FieldInfos[] =
{
	&CharacterClass_t2113____upper_case_characters_0_FieldInfo,
	&CharacterClass_t2113____negate_1_FieldInfo,
	&CharacterClass_t2113____ignore_2_FieldInfo,
	&CharacterClass_t2113____pos_cats_3_FieldInfo,
	&CharacterClass_t2113____neg_cats_4_FieldInfo,
	&CharacterClass_t2113____intervals_5_FieldInfo,
	NULL
};
extern MethodInfo CharacterClass_Compile_m8648_MethodInfo;
extern MethodInfo CharacterClass_GetWidth_m8649_MethodInfo;
extern MethodInfo CharacterClass_IsComplex_m8650_MethodInfo;
static Il2CppMethodReference CharacterClass_t2113_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&CharacterClass_Compile_m8648_MethodInfo,
	&CharacterClass_GetWidth_m8649_MethodInfo,
	&Expression_GetAnchorInfo_m8563_MethodInfo,
	&CharacterClass_IsComplex_m8650_MethodInfo,
};
static bool CharacterClass_t2113_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType CharacterClass_t2113_0_0_0;
extern Il2CppType CharacterClass_t2113_1_0_0;
struct CharacterClass_t2113;
const Il2CppTypeDefinitionMetadata CharacterClass_t2113_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t2096_0_0_0/* parent */
	, CharacterClass_t2113_VTable/* vtableMethods */
	, CharacterClass_t2113_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CharacterClass_t2113_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterClass"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CharacterClass_t2113_MethodInfos/* methods */
	, NULL/* properties */
	, CharacterClass_t2113_FieldInfos/* fields */
	, NULL/* events */
	, &CharacterClass_t2113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterClass_t2113_0_0_0/* byval_arg */
	, &CharacterClass_t2113_1_0_0/* this_arg */
	, &CharacterClass_t2113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharacterClass_t2113)/* instance_size */
	, sizeof (CharacterClass_t2113)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CharacterClass_t2113_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfo.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.AnchorInfo
extern TypeInfo AnchorInfo_t2114_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfoMethodDeclarations.h"
extern Il2CppType Expression_t2096_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo AnchorInfo_t2114_AnchorInfo__ctor_m8652_ParameterInfos[] = 
{
	{"expr", 0, 134218548, 0, &Expression_t2096_0_0_0},
	{"width", 1, 134218549, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32)
MethodInfo AnchorInfo__ctor_m8652_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m8652/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189/* invoker_method */
	, AnchorInfo_t2114_AnchorInfo__ctor_m8652_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo AnchorInfo_t2114_AnchorInfo__ctor_m8653_ParameterInfos[] = 
{
	{"expr", 0, 134218550, 0, &Expression_t2096_0_0_0},
	{"offset", 1, 134218551, 0, &Int32_t189_0_0_0},
	{"width", 2, 134218552, 0, &Int32_t189_0_0_0},
	{"str", 3, 134218553, 0, &String_t_0_0_0},
	{"ignore", 4, 134218554, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.String,System.Boolean)
MethodInfo AnchorInfo__ctor_m8653_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m8653/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t_SByte_t236/* invoker_method */
	, AnchorInfo_t2114_AnchorInfo__ctor_m8653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Expression_t2096_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Position_t2071_0_0_0;
static ParameterInfo AnchorInfo_t2114_AnchorInfo__ctor_m8654_ParameterInfos[] = 
{
	{"expr", 0, 134218555, 0, &Expression_t2096_0_0_0},
	{"offset", 1, 134218556, 0, &Int32_t189_0_0_0},
	{"width", 2, 134218557, 0, &Int32_t189_0_0_0},
	{"pos", 3, 134218558, 0, &Position_t2071_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_UInt16_t194 (MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.Text.RegularExpressions.Position)
MethodInfo AnchorInfo__ctor_m8654_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m8654/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_UInt16_t194/* invoker_method */
	, AnchorInfo_t2114_AnchorInfo__ctor_m8654_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Offset()
MethodInfo AnchorInfo_get_Offset_m8655_MethodInfo = 
{
	"get_Offset"/* name */
	, (methodPointerType)&AnchorInfo_get_Offset_m8655/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Width()
MethodInfo AnchorInfo_get_Width_m8656_MethodInfo = 
{
	"get_Width"/* name */
	, (methodPointerType)&AnchorInfo_get_Width_m8656/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Length()
MethodInfo AnchorInfo_get_Length_m8657_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&AnchorInfo_get_Length_m8657/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsUnknownWidth()
MethodInfo AnchorInfo_get_IsUnknownWidth_m8658_MethodInfo = 
{
	"get_IsUnknownWidth"/* name */
	, (methodPointerType)&AnchorInfo_get_IsUnknownWidth_m8658/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsComplete()
MethodInfo AnchorInfo_get_IsComplete_m8659_MethodInfo = 
{
	"get_IsComplete"/* name */
	, (methodPointerType)&AnchorInfo_get_IsComplete_m8659/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.AnchorInfo::get_Substring()
MethodInfo AnchorInfo_get_Substring_m8660_MethodInfo = 
{
	"get_Substring"/* name */
	, (methodPointerType)&AnchorInfo_get_Substring_m8660/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IgnoreCase()
MethodInfo AnchorInfo_get_IgnoreCase_m8661_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&AnchorInfo_get_IgnoreCase_m8661/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Position_t2071_0_0_0;
extern void* RuntimeInvoker_Position_t2071 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Position System.Text.RegularExpressions.Syntax.AnchorInfo::get_Position()
MethodInfo AnchorInfo_get_Position_m8662_MethodInfo = 
{
	"get_Position"/* name */
	, (methodPointerType)&AnchorInfo_get_Position_m8662/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Position_t2071_0_0_0/* return_type */
	, RuntimeInvoker_Position_t2071/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsSubstring()
MethodInfo AnchorInfo_get_IsSubstring_m8663_MethodInfo = 
{
	"get_IsSubstring"/* name */
	, (methodPointerType)&AnchorInfo_get_IsSubstring_m8663/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsPosition()
MethodInfo AnchorInfo_get_IsPosition_m8664_MethodInfo = 
{
	"get_IsPosition"/* name */
	, (methodPointerType)&AnchorInfo_get_IsPosition_m8664/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo AnchorInfo_t2114_AnchorInfo_GetInterval_m8665_ParameterInfos[] = 
{
	{"start", 0, 134218559, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Interval_t2090_0_0_0;
extern void* RuntimeInvoker_Interval_t2090_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Syntax.AnchorInfo::GetInterval(System.Int32)
MethodInfo AnchorInfo_GetInterval_m8665_MethodInfo = 
{
	"GetInterval"/* name */
	, (methodPointerType)&AnchorInfo_GetInterval_m8665/* method */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t2090_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t2090_Int32_t189/* invoker_method */
	, AnchorInfo_t2114_AnchorInfo_GetInterval_m8665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AnchorInfo_t2114_MethodInfos[] =
{
	&AnchorInfo__ctor_m8652_MethodInfo,
	&AnchorInfo__ctor_m8653_MethodInfo,
	&AnchorInfo__ctor_m8654_MethodInfo,
	&AnchorInfo_get_Offset_m8655_MethodInfo,
	&AnchorInfo_get_Width_m8656_MethodInfo,
	&AnchorInfo_get_Length_m8657_MethodInfo,
	&AnchorInfo_get_IsUnknownWidth_m8658_MethodInfo,
	&AnchorInfo_get_IsComplete_m8659_MethodInfo,
	&AnchorInfo_get_Substring_m8660_MethodInfo,
	&AnchorInfo_get_IgnoreCase_m8661_MethodInfo,
	&AnchorInfo_get_Position_m8662_MethodInfo,
	&AnchorInfo_get_IsSubstring_m8663_MethodInfo,
	&AnchorInfo_get_IsPosition_m8664_MethodInfo,
	&AnchorInfo_GetInterval_m8665_MethodInfo,
	NULL
};
extern Il2CppType Expression_t2096_0_0_1;
FieldInfo AnchorInfo_t2114____expr_0_FieldInfo = 
{
	"expr"/* name */
	, &Expression_t2096_0_0_1/* type */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, offsetof(AnchorInfo_t2114, ___expr_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Position_t2071_0_0_1;
FieldInfo AnchorInfo_t2114____pos_1_FieldInfo = 
{
	"pos"/* name */
	, &Position_t2071_0_0_1/* type */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, offsetof(AnchorInfo_t2114, ___pos_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo AnchorInfo_t2114____offset_2_FieldInfo = 
{
	"offset"/* name */
	, &Int32_t189_0_0_1/* type */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, offsetof(AnchorInfo_t2114, ___offset_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo AnchorInfo_t2114____str_3_FieldInfo = 
{
	"str"/* name */
	, &String_t_0_0_1/* type */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, offsetof(AnchorInfo_t2114, ___str_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo AnchorInfo_t2114____width_4_FieldInfo = 
{
	"width"/* name */
	, &Int32_t189_0_0_1/* type */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, offsetof(AnchorInfo_t2114, ___width_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo AnchorInfo_t2114____ignore_5_FieldInfo = 
{
	"ignore"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, offsetof(AnchorInfo_t2114, ___ignore_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* AnchorInfo_t2114_FieldInfos[] =
{
	&AnchorInfo_t2114____expr_0_FieldInfo,
	&AnchorInfo_t2114____pos_1_FieldInfo,
	&AnchorInfo_t2114____offset_2_FieldInfo,
	&AnchorInfo_t2114____str_3_FieldInfo,
	&AnchorInfo_t2114____width_4_FieldInfo,
	&AnchorInfo_t2114____ignore_5_FieldInfo,
	NULL
};
extern MethodInfo AnchorInfo_get_Offset_m8655_MethodInfo;
static PropertyInfo AnchorInfo_t2114____Offset_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "Offset"/* name */
	, &AnchorInfo_get_Offset_m8655_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_Width_m8656_MethodInfo;
static PropertyInfo AnchorInfo_t2114____Width_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "Width"/* name */
	, &AnchorInfo_get_Width_m8656_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_Length_m8657_MethodInfo;
static PropertyInfo AnchorInfo_t2114____Length_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &AnchorInfo_get_Length_m8657_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_IsUnknownWidth_m8658_MethodInfo;
static PropertyInfo AnchorInfo_t2114____IsUnknownWidth_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "IsUnknownWidth"/* name */
	, &AnchorInfo_get_IsUnknownWidth_m8658_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_IsComplete_m8659_MethodInfo;
static PropertyInfo AnchorInfo_t2114____IsComplete_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "IsComplete"/* name */
	, &AnchorInfo_get_IsComplete_m8659_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_Substring_m8660_MethodInfo;
static PropertyInfo AnchorInfo_t2114____Substring_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "Substring"/* name */
	, &AnchorInfo_get_Substring_m8660_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_IgnoreCase_m8661_MethodInfo;
static PropertyInfo AnchorInfo_t2114____IgnoreCase_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &AnchorInfo_get_IgnoreCase_m8661_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_Position_m8662_MethodInfo;
static PropertyInfo AnchorInfo_t2114____Position_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "Position"/* name */
	, &AnchorInfo_get_Position_m8662_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_IsSubstring_m8663_MethodInfo;
static PropertyInfo AnchorInfo_t2114____IsSubstring_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "IsSubstring"/* name */
	, &AnchorInfo_get_IsSubstring_m8663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AnchorInfo_get_IsPosition_m8664_MethodInfo;
static PropertyInfo AnchorInfo_t2114____IsPosition_PropertyInfo = 
{
	&AnchorInfo_t2114_il2cpp_TypeInfo/* parent */
	, "IsPosition"/* name */
	, &AnchorInfo_get_IsPosition_m8664_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* AnchorInfo_t2114_PropertyInfos[] =
{
	&AnchorInfo_t2114____Offset_PropertyInfo,
	&AnchorInfo_t2114____Width_PropertyInfo,
	&AnchorInfo_t2114____Length_PropertyInfo,
	&AnchorInfo_t2114____IsUnknownWidth_PropertyInfo,
	&AnchorInfo_t2114____IsComplete_PropertyInfo,
	&AnchorInfo_t2114____Substring_PropertyInfo,
	&AnchorInfo_t2114____IgnoreCase_PropertyInfo,
	&AnchorInfo_t2114____Position_PropertyInfo,
	&AnchorInfo_t2114____IsSubstring_PropertyInfo,
	&AnchorInfo_t2114____IsPosition_PropertyInfo,
	NULL
};
static Il2CppMethodReference AnchorInfo_t2114_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool AnchorInfo_t2114_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType AnchorInfo_t2114_0_0_0;
extern Il2CppType AnchorInfo_t2114_1_0_0;
struct AnchorInfo_t2114;
const Il2CppTypeDefinitionMetadata AnchorInfo_t2114_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnchorInfo_t2114_VTable/* vtableMethods */
	, AnchorInfo_t2114_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo AnchorInfo_t2114_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnchorInfo"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, AnchorInfo_t2114_MethodInfos/* methods */
	, AnchorInfo_t2114_PropertyInfos/* properties */
	, AnchorInfo_t2114_FieldInfos/* fields */
	, NULL/* events */
	, &AnchorInfo_t2114_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnchorInfo_t2114_0_0_0/* byval_arg */
	, &AnchorInfo_t2114_1_0_0/* this_arg */
	, &AnchorInfo_t2114_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnchorInfo_t2114)/* instance_size */
	, sizeof (AnchorInfo_t2114)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 10/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DefaultUriParser
#include "System_System_DefaultUriParser.h"
// Metadata Definition System.DefaultUriParser
extern TypeInfo DefaultUriParser_t2115_il2cpp_TypeInfo;
// System.DefaultUriParser
#include "System_System_DefaultUriParserMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor()
MethodInfo DefaultUriParser__ctor_m8666_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m8666/* method */
	, &DefaultUriParser_t2115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo DefaultUriParser_t2115_DefaultUriParser__ctor_m8667_ParameterInfos[] = 
{
	{"scheme", 0, 134218560, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor(System.String)
MethodInfo DefaultUriParser__ctor_m8667_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m8667/* method */
	, &DefaultUriParser_t2115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, DefaultUriParser_t2115_DefaultUriParser__ctor_m8667_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DefaultUriParser_t2115_MethodInfos[] =
{
	&DefaultUriParser__ctor_m8666_MethodInfo,
	&DefaultUriParser__ctor_m8667_MethodInfo,
	NULL
};
extern MethodInfo UriParser_InitializeAndValidate_m8723_MethodInfo;
extern MethodInfo UriParser_OnRegister_m8724_MethodInfo;
static Il2CppMethodReference DefaultUriParser_t2115_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&UriParser_InitializeAndValidate_m8723_MethodInfo,
	&UriParser_OnRegister_m8724_MethodInfo,
};
static bool DefaultUriParser_t2115_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType DefaultUriParser_t2115_0_0_0;
extern Il2CppType DefaultUriParser_t2115_1_0_0;
extern Il2CppType UriParser_t2116_0_0_0;
struct DefaultUriParser_t2115;
const Il2CppTypeDefinitionMetadata DefaultUriParser_t2115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t2116_0_0_0/* parent */
	, DefaultUriParser_t2115_VTable/* vtableMethods */
	, DefaultUriParser_t2115_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo DefaultUriParser_t2115_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultUriParser"/* name */
	, "System"/* namespaze */
	, DefaultUriParser_t2115_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &DefaultUriParser_t2115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultUriParser_t2115_0_0_0/* byval_arg */
	, &DefaultUriParser_t2115_1_0_0/* this_arg */
	, &DefaultUriParser_t2115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultUriParser_t2115)/* instance_size */
	, sizeof (DefaultUriParser_t2115)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.GenericUriParser
#include "System_System_GenericUriParser.h"
// Metadata Definition System.GenericUriParser
extern TypeInfo GenericUriParser_t2117_il2cpp_TypeInfo;
// System.GenericUriParser
#include "System_System_GenericUriParserMethodDeclarations.h"
static MethodInfo* GenericUriParser_t2117_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference GenericUriParser_t2117_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&UriParser_InitializeAndValidate_m8723_MethodInfo,
	&UriParser_OnRegister_m8724_MethodInfo,
};
static bool GenericUriParser_t2117_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType GenericUriParser_t2117_0_0_0;
extern Il2CppType GenericUriParser_t2117_1_0_0;
struct GenericUriParser_t2117;
const Il2CppTypeDefinitionMetadata GenericUriParser_t2117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t2116_0_0_0/* parent */
	, GenericUriParser_t2117_VTable/* vtableMethods */
	, GenericUriParser_t2117_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo GenericUriParser_t2117_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericUriParser"/* name */
	, "System"/* namespaze */
	, GenericUriParser_t2117_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &GenericUriParser_t2117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericUriParser_t2117_0_0_0/* byval_arg */
	, &GenericUriParser_t2117_1_0_0/* this_arg */
	, &GenericUriParser_t2117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericUriParser_t2117)/* instance_size */
	, sizeof (GenericUriParser_t2117)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
// Metadata Definition System.Uri/UriScheme
extern TypeInfo UriScheme_t2118_il2cpp_TypeInfo;
// System.Uri/UriScheme
#include "System_System_Uri_UriSchemeMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo UriScheme_t2118_UriScheme__ctor_m8668_ParameterInfos[] = 
{
	{"s", 0, 134218609, 0, &String_t_0_0_0},
	{"d", 1, 134218610, 0, &String_t_0_0_0},
	{"p", 2, 134218611, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri/UriScheme::.ctor(System.String,System.String,System.Int32)
MethodInfo UriScheme__ctor_m8668_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriScheme__ctor_m8668/* method */
	, &UriScheme_t2118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189/* invoker_method */
	, UriScheme_t2118_UriScheme__ctor_m8668_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UriScheme_t2118_MethodInfos[] =
{
	&UriScheme__ctor_m8668_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_6;
FieldInfo UriScheme_t2118____scheme_0_FieldInfo = 
{
	"scheme"/* name */
	, &String_t_0_0_6/* type */
	, &UriScheme_t2118_il2cpp_TypeInfo/* parent */
	, offsetof(UriScheme_t2118, ___scheme_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo UriScheme_t2118____delimiter_1_FieldInfo = 
{
	"delimiter"/* name */
	, &String_t_0_0_6/* type */
	, &UriScheme_t2118_il2cpp_TypeInfo/* parent */
	, offsetof(UriScheme_t2118, ___delimiter_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo UriScheme_t2118____defaultPort_2_FieldInfo = 
{
	"defaultPort"/* name */
	, &Int32_t189_0_0_6/* type */
	, &UriScheme_t2118_il2cpp_TypeInfo/* parent */
	, offsetof(UriScheme_t2118, ___defaultPort_2) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UriScheme_t2118_FieldInfos[] =
{
	&UriScheme_t2118____scheme_0_FieldInfo,
	&UriScheme_t2118____delimiter_1_FieldInfo,
	&UriScheme_t2118____defaultPort_2_FieldInfo,
	NULL
};
static Il2CppMethodReference UriScheme_t2118_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool UriScheme_t2118_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType UriScheme_t2118_0_0_0;
extern Il2CppType UriScheme_t2118_1_0_0;
extern TypeInfo Uri_t1986_il2cpp_TypeInfo;
extern Il2CppType Uri_t1986_0_0_0;
const Il2CppTypeDefinitionMetadata UriScheme_t2118_DefinitionMetadata = 
{
	&Uri_t1986_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, UriScheme_t2118_VTable/* vtableMethods */
	, UriScheme_t2118_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UriScheme_t2118_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriScheme"/* name */
	, ""/* namespaze */
	, UriScheme_t2118_MethodInfos/* methods */
	, NULL/* properties */
	, UriScheme_t2118_FieldInfos/* fields */
	, NULL/* events */
	, &UriScheme_t2118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriScheme_t2118_0_0_0/* byval_arg */
	, &UriScheme_t2118_1_0_0/* this_arg */
	, &UriScheme_t2118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)UriScheme_t2118_marshal/* marshal_to_native_func */
	, (methodPointerType)UriScheme_t2118_marshal_back/* marshal_from_native_func */
	, (methodPointerType)UriScheme_t2118_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (UriScheme_t2118)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriScheme_t2118)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UriScheme_t2118_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri
#include "System_System_Uri.h"
// Metadata Definition System.Uri
// System.Uri
#include "System_System_UriMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri__ctor_m8669_ParameterInfos[] = 
{
	{"uriString", 0, 134218561, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String)
MethodInfo Uri__ctor_m8669_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m8669/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Uri_t1986_Uri__ctor_m8669_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo Uri_t1986_Uri__ctor_m8670_ParameterInfos[] = 
{
	{"serializationInfo", 0, 134218562, 0, &SerializationInfo_t1673_0_0_0},
	{"streamingContext", 1, 134218563, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo Uri__ctor_m8670_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m8670/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, Uri_t1986_Uri__ctor_m8670_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Uri_t1986_Uri__ctor_m8671_ParameterInfos[] = 
{
	{"uriString", 0, 134218564, 0, &String_t_0_0_0},
	{"dontEscape", 1, 134218565, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String,System.Boolean)
MethodInfo Uri__ctor_m8671_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m8671/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Uri_t1986_Uri__ctor_m8671_ParameterInfos/* parameters */
	, 62/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.cctor()
MethodInfo Uri__cctor_m8672_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Uri__cctor_m8672/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo Uri_t1986_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m8673_ParameterInfos[] = 
{
	{"info", 0, 134218566, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134218567, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m8673_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m8673/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, Uri_t1986_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m8673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_AbsoluteUri()
MethodInfo Uri_get_AbsoluteUri_m8674_MethodInfo = 
{
	"get_AbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_AbsoluteUri_m8674/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Authority()
MethodInfo Uri_get_Authority_m8675_MethodInfo = 
{
	"get_Authority"/* name */
	, (methodPointerType)&Uri_get_Authority_m8675/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Host()
MethodInfo Uri_get_Host_m8676_MethodInfo = 
{
	"get_Host"/* name */
	, (methodPointerType)&Uri_get_Host_m8676/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsFile()
MethodInfo Uri_get_IsFile_m8677_MethodInfo = 
{
	"get_IsFile"/* name */
	, (methodPointerType)&Uri_get_IsFile_m8677/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsLoopback()
MethodInfo Uri_get_IsLoopback_m8678_MethodInfo = 
{
	"get_IsLoopback"/* name */
	, (methodPointerType)&Uri_get_IsLoopback_m8678/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsUnc()
MethodInfo Uri_get_IsUnc_m8679_MethodInfo = 
{
	"get_IsUnc"/* name */
	, (methodPointerType)&Uri_get_IsUnc_m8679/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Scheme()
MethodInfo Uri_get_Scheme_m8680_MethodInfo = 
{
	"get_Scheme"/* name */
	, (methodPointerType)&Uri_get_Scheme_m8680/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsAbsoluteUri()
MethodInfo Uri_get_IsAbsoluteUri_m8681_MethodInfo = 
{
	"get_IsAbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_IsAbsoluteUri_m8681/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_CheckHostName_m8682_ParameterInfos[] = 
{
	{"name", 0, 134218568, 0, &String_t_0_0_0},
};
extern Il2CppType UriHostNameType_t2121_0_0_0;
extern void* RuntimeInvoker_UriHostNameType_t2121_Object_t (MethodInfo* method, void* obj, void** args);
// System.UriHostNameType System.Uri::CheckHostName(System.String)
MethodInfo Uri_CheckHostName_m8682_MethodInfo = 
{
	"CheckHostName"/* name */
	, (methodPointerType)&Uri_CheckHostName_m8682/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &UriHostNameType_t2121_0_0_0/* return_type */
	, RuntimeInvoker_UriHostNameType_t2121_Object_t/* invoker_method */
	, Uri_t1986_Uri_CheckHostName_m8682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_IsIPv4Address_m8683_ParameterInfos[] = 
{
	{"name", 0, 134218569, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsIPv4Address(System.String)
MethodInfo Uri_IsIPv4Address_m8683_MethodInfo = 
{
	"IsIPv4Address"/* name */
	, (methodPointerType)&Uri_IsIPv4Address_m8683/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Uri_t1986_Uri_IsIPv4Address_m8683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_IsDomainAddress_m8684_ParameterInfos[] = 
{
	{"name", 0, 134218570, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsDomainAddress(System.String)
MethodInfo Uri_IsDomainAddress_m8684_MethodInfo = 
{
	"IsDomainAddress"/* name */
	, (methodPointerType)&Uri_IsDomainAddress_m8684/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Uri_t1986_Uri_IsDomainAddress_m8684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_CheckSchemeName_m8685_ParameterInfos[] = 
{
	{"schemeName", 0, 134218571, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CheckSchemeName(System.String)
MethodInfo Uri_CheckSchemeName_m8685_MethodInfo = 
{
	"CheckSchemeName"/* name */
	, (methodPointerType)&Uri_CheckSchemeName_m8685/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Uri_t1986_Uri_CheckSchemeName_m8685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo Uri_t1986_Uri_IsAlpha_m8686_ParameterInfos[] = 
{
	{"c", 0, 134218572, 0, &Char_t193_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsAlpha(System.Char)
MethodInfo Uri_IsAlpha_m8686_MethodInfo = 
{
	"IsAlpha"/* name */
	, (methodPointerType)&Uri_IsAlpha_m8686/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int16_t238/* invoker_method */
	, Uri_t1986_Uri_IsAlpha_m8686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_Equals_m8687_ParameterInfos[] = 
{
	{"comparant", 0, 134218573, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::Equals(System.Object)
MethodInfo Uri_Equals_m8687_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Uri_Equals_m8687/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Uri_t1986_Uri_Equals_m8687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Uri_t1986_0_0_0;
static ParameterInfo Uri_t1986_Uri_InternalEquals_m8688_ParameterInfos[] = 
{
	{"uri", 0, 134218574, 0, &Uri_t1986_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::InternalEquals(System.Uri)
MethodInfo Uri_InternalEquals_m8688_MethodInfo = 
{
	"InternalEquals"/* name */
	, (methodPointerType)&Uri_InternalEquals_m8688/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Uri_t1986_Uri_InternalEquals_m8688_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetHashCode()
MethodInfo Uri_GetHashCode_m8689_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Uri_GetHashCode_m8689/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UriPartial_t2123_0_0_0;
extern Il2CppType UriPartial_t2123_0_0_0;
static ParameterInfo Uri_t1986_Uri_GetLeftPart_m8690_ParameterInfos[] = 
{
	{"part", 0, 134218575, 0, &UriPartial_t2123_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetLeftPart(System.UriPartial)
MethodInfo Uri_GetLeftPart_m8690_MethodInfo = 
{
	"GetLeftPart"/* name */
	, (methodPointerType)&Uri_GetLeftPart_m8690/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189/* invoker_method */
	, Uri_t1986_Uri_GetLeftPart_m8690_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo Uri_t1986_Uri_FromHex_m8691_ParameterInfos[] = 
{
	{"digit", 0, 134218576, 0, &Char_t193_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::FromHex(System.Char)
MethodInfo Uri_FromHex_m8691_MethodInfo = 
{
	"FromHex"/* name */
	, (methodPointerType)&Uri_FromHex_m8691/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Int16_t238/* invoker_method */
	, Uri_t1986_Uri_FromHex_m8691_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo Uri_t1986_Uri_HexEscape_m8692_ParameterInfos[] = 
{
	{"character", 0, 134218577, 0, &Char_t193_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::HexEscape(System.Char)
MethodInfo Uri_HexEscape_m8692_MethodInfo = 
{
	"HexEscape"/* name */
	, (methodPointerType)&Uri_HexEscape_m8692/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int16_t238/* invoker_method */
	, Uri_t1986_Uri_HexEscape_m8692_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo Uri_t1986_Uri_IsHexDigit_m8693_ParameterInfos[] = 
{
	{"digit", 0, 134218578, 0, &Char_t193_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexDigit(System.Char)
MethodInfo Uri_IsHexDigit_m8693_MethodInfo = 
{
	"IsHexDigit"/* name */
	, (methodPointerType)&Uri_IsHexDigit_m8693/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int16_t238/* invoker_method */
	, Uri_t1986_Uri_IsHexDigit_m8693_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Uri_t1986_Uri_IsHexEncoding_m8694_ParameterInfos[] = 
{
	{"pattern", 0, 134218579, 0, &String_t_0_0_0},
	{"index", 1, 134218580, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexEncoding(System.String,System.Int32)
MethodInfo Uri_IsHexEncoding_m8694_MethodInfo = 
{
	"IsHexEncoding"/* name */
	, (methodPointerType)&Uri_IsHexEncoding_m8694/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Int32_t189/* invoker_method */
	, Uri_t1986_Uri_IsHexEncoding_m8694_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_1_0_0;
extern Il2CppType String_t_1_0_0;
static ParameterInfo Uri_t1986_Uri_AppendQueryAndFragment_m8695_ParameterInfos[] = 
{
	{"result", 0, 134218581, 0, &String_t_1_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_StringU26_t319 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::AppendQueryAndFragment(System.String&)
MethodInfo Uri_AppendQueryAndFragment_m8695_MethodInfo = 
{
	"AppendQueryAndFragment"/* name */
	, (methodPointerType)&Uri_AppendQueryAndFragment_m8695/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_StringU26_t319/* invoker_method */
	, Uri_t1986_Uri_AppendQueryAndFragment_m8695_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ToString()
MethodInfo Uri_ToString_m8696_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Uri_ToString_m8696/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_EscapeString_m8697_ParameterInfos[] = 
{
	{"str", 0, 134218582, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String)
MethodInfo Uri_EscapeString_m8697_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m8697/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1986_Uri_EscapeString_m8697_ParameterInfos/* parameters */
	, 63/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Uri_t1986_Uri_EscapeString_m8698_ParameterInfos[] = 
{
	{"str", 0, 134218583, 0, &String_t_0_0_0},
	{"escapeReserved", 1, 134218584, 0, &Boolean_t203_0_0_0},
	{"escapeHex", 2, 134218585, 0, &Boolean_t203_0_0_0},
	{"escapeBrackets", 3, 134218586, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String,System.Boolean,System.Boolean,System.Boolean)
MethodInfo Uri_EscapeString_m8698_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m8698/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236_SByte_t236/* invoker_method */
	, Uri_t1986_Uri_EscapeString_m8698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UriKind_t2122_0_0_0;
extern Il2CppType UriKind_t2122_0_0_0;
static ParameterInfo Uri_t1986_Uri_ParseUri_m8699_ParameterInfos[] = 
{
	{"kind", 0, 134218587, 0, &UriKind_t2122_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseUri(System.UriKind)
MethodInfo Uri_ParseUri_m8699_MethodInfo = 
{
	"ParseUri"/* name */
	, (methodPointerType)&Uri_ParseUri_m8699/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Uri_t1986_Uri_ParseUri_m8699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_Unescape_m8700_ParameterInfos[] = 
{
	{"str", 0, 134218588, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String)
MethodInfo Uri_Unescape_m8700_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m8700/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1986_Uri_Unescape_m8700_ParameterInfos/* parameters */
	, 64/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Uri_t1986_Uri_Unescape_m8701_ParameterInfos[] = 
{
	{"str", 0, 134218589, 0, &String_t_0_0_0},
	{"excludeSpecial", 1, 134218590, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String,System.Boolean)
MethodInfo Uri_Unescape_m8701_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m8701/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236/* invoker_method */
	, Uri_t1986_Uri_Unescape_m8701_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_ParseAsWindowsUNC_m8702_ParameterInfos[] = 
{
	{"uriString", 0, 134218591, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsWindowsUNC(System.String)
MethodInfo Uri_ParseAsWindowsUNC_m8702_MethodInfo = 
{
	"ParseAsWindowsUNC"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsUNC_m8702/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Uri_t1986_Uri_ParseAsWindowsUNC_m8702_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_ParseAsWindowsAbsoluteFilePath_m8703_ParameterInfos[] = 
{
	{"uriString", 0, 134218592, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseAsWindowsAbsoluteFilePath(System.String)
MethodInfo Uri_ParseAsWindowsAbsoluteFilePath_m8703_MethodInfo = 
{
	"ParseAsWindowsAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsAbsoluteFilePath_m8703/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1986_Uri_ParseAsWindowsAbsoluteFilePath_m8703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_ParseAsUnixAbsoluteFilePath_m8704_ParameterInfos[] = 
{
	{"uriString", 0, 134218593, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsUnixAbsoluteFilePath(System.String)
MethodInfo Uri_ParseAsUnixAbsoluteFilePath_m8704_MethodInfo = 
{
	"ParseAsUnixAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsUnixAbsoluteFilePath_m8704/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Uri_t1986_Uri_ParseAsUnixAbsoluteFilePath_m8704_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UriKind_t2122_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_Parse_m8705_ParameterInfos[] = 
{
	{"kind", 0, 134218594, 0, &UriKind_t2122_0_0_0},
	{"uriString", 1, 134218595, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::Parse(System.UriKind,System.String)
MethodInfo Uri_Parse_m8705_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&Uri_Parse_m8705/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Object_t/* invoker_method */
	, Uri_t1986_Uri_Parse_m8705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UriKind_t2122_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_ParseNoExceptions_m8706_ParameterInfos[] = 
{
	{"kind", 0, 134218596, 0, &UriKind_t2122_0_0_0},
	{"uriString", 1, 134218597, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseNoExceptions(System.UriKind,System.String)
MethodInfo Uri_ParseNoExceptions_m8706_MethodInfo = 
{
	"ParseNoExceptions"/* name */
	, (methodPointerType)&Uri_ParseNoExceptions_m8706/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189_Object_t/* invoker_method */
	, Uri_t1986_Uri_ParseNoExceptions_m8706_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_CompactEscaped_m8707_ParameterInfos[] = 
{
	{"scheme", 0, 134218598, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CompactEscaped(System.String)
MethodInfo Uri_CompactEscaped_m8707_MethodInfo = 
{
	"CompactEscaped"/* name */
	, (methodPointerType)&Uri_CompactEscaped_m8707/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Uri_t1986_Uri_CompactEscaped_m8707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Uri_t1986_Uri_Reduce_m8708_ParameterInfos[] = 
{
	{"path", 0, 134218599, 0, &String_t_0_0_0},
	{"compact_escaped", 1, 134218600, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Reduce(System.String,System.Boolean)
MethodInfo Uri_Reduce_m8708_MethodInfo = 
{
	"Reduce"/* name */
	, (methodPointerType)&Uri_Reduce_m8708/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236/* invoker_method */
	, Uri_t1986_Uri_Reduce_m8708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_1_0_0;
extern Il2CppType Char_t193_1_0_2;
extern Il2CppType Char_t193_1_0_0;
static ParameterInfo Uri_t1986_Uri_HexUnescapeMultiByte_m8709_ParameterInfos[] = 
{
	{"pattern", 0, 134218601, 0, &String_t_0_0_0},
	{"index", 1, 134218602, 0, &Int32_t189_1_0_0},
	{"surrogate", 2, 134218603, 0, &Char_t193_1_0_2},
};
extern Il2CppType Char_t193_0_0_0;
extern void* RuntimeInvoker_Char_t193_Object_t_Int32U26_t317_CharU26_t2181 (MethodInfo* method, void* obj, void** args);
// System.Char System.Uri::HexUnescapeMultiByte(System.String,System.Int32&,System.Char&)
MethodInfo Uri_HexUnescapeMultiByte_m8709_MethodInfo = 
{
	"HexUnescapeMultiByte"/* name */
	, (methodPointerType)&Uri_HexUnescapeMultiByte_m8709/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Char_t193_0_0_0/* return_type */
	, RuntimeInvoker_Char_t193_Object_t_Int32U26_t317_CharU26_t2181/* invoker_method */
	, Uri_t1986_Uri_HexUnescapeMultiByte_m8709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_GetSchemeDelimiter_m8710_ParameterInfos[] = 
{
	{"scheme", 0, 134218604, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetSchemeDelimiter(System.String)
MethodInfo Uri_GetSchemeDelimiter_m8710_MethodInfo = 
{
	"GetSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetSchemeDelimiter_m8710/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1986_Uri_GetSchemeDelimiter_m8710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_GetDefaultPort_m8711_ParameterInfos[] = 
{
	{"scheme", 0, 134218605, 0, &String_t_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetDefaultPort(System.String)
MethodInfo Uri_GetDefaultPort_m8711_MethodInfo = 
{
	"GetDefaultPort"/* name */
	, (methodPointerType)&Uri_GetDefaultPort_m8711/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t/* invoker_method */
	, Uri_t1986_Uri_GetDefaultPort_m8711_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetOpaqueWiseSchemeDelimiter()
MethodInfo Uri_GetOpaqueWiseSchemeDelimiter_m8712_MethodInfo = 
{
	"GetOpaqueWiseSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetOpaqueWiseSchemeDelimiter_m8712/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Uri_t1986_Uri_IsPredefinedScheme_m8713_ParameterInfos[] = 
{
	{"scheme", 0, 134218606, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsPredefinedScheme(System.String)
MethodInfo Uri_IsPredefinedScheme_m8713_MethodInfo = 
{
	"IsPredefinedScheme"/* name */
	, (methodPointerType)&Uri_IsPredefinedScheme_m8713/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Uri_t1986_Uri_IsPredefinedScheme_m8713_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UriParser_t2116_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.UriParser System.Uri::get_Parser()
MethodInfo Uri_get_Parser_m8714_MethodInfo = 
{
	"get_Parser"/* name */
	, (methodPointerType)&Uri_get_Parser_m8714/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t2116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::EnsureAbsoluteUri()
MethodInfo Uri_EnsureAbsoluteUri_m8715_MethodInfo = 
{
	"EnsureAbsoluteUri"/* name */
	, (methodPointerType)&Uri_EnsureAbsoluteUri_m8715/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Uri_t1986_0_0_0;
extern Il2CppType Uri_t1986_0_0_0;
static ParameterInfo Uri_t1986_Uri_op_Equality_m8716_ParameterInfos[] = 
{
	{"u1", 0, 134218607, 0, &Uri_t1986_0_0_0},
	{"u2", 1, 134218608, 0, &Uri_t1986_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::op_Equality(System.Uri,System.Uri)
MethodInfo Uri_op_Equality_m8716_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Uri_op_Equality_m8716/* method */
	, &Uri_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, Uri_t1986_Uri_op_Equality_m8716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Uri_t1986_MethodInfos[] =
{
	&Uri__ctor_m8669_MethodInfo,
	&Uri__ctor_m8670_MethodInfo,
	&Uri__ctor_m8671_MethodInfo,
	&Uri__cctor_m8672_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m8673_MethodInfo,
	&Uri_get_AbsoluteUri_m8674_MethodInfo,
	&Uri_get_Authority_m8675_MethodInfo,
	&Uri_get_Host_m8676_MethodInfo,
	&Uri_get_IsFile_m8677_MethodInfo,
	&Uri_get_IsLoopback_m8678_MethodInfo,
	&Uri_get_IsUnc_m8679_MethodInfo,
	&Uri_get_Scheme_m8680_MethodInfo,
	&Uri_get_IsAbsoluteUri_m8681_MethodInfo,
	&Uri_CheckHostName_m8682_MethodInfo,
	&Uri_IsIPv4Address_m8683_MethodInfo,
	&Uri_IsDomainAddress_m8684_MethodInfo,
	&Uri_CheckSchemeName_m8685_MethodInfo,
	&Uri_IsAlpha_m8686_MethodInfo,
	&Uri_Equals_m8687_MethodInfo,
	&Uri_InternalEquals_m8688_MethodInfo,
	&Uri_GetHashCode_m8689_MethodInfo,
	&Uri_GetLeftPart_m8690_MethodInfo,
	&Uri_FromHex_m8691_MethodInfo,
	&Uri_HexEscape_m8692_MethodInfo,
	&Uri_IsHexDigit_m8693_MethodInfo,
	&Uri_IsHexEncoding_m8694_MethodInfo,
	&Uri_AppendQueryAndFragment_m8695_MethodInfo,
	&Uri_ToString_m8696_MethodInfo,
	&Uri_EscapeString_m8697_MethodInfo,
	&Uri_EscapeString_m8698_MethodInfo,
	&Uri_ParseUri_m8699_MethodInfo,
	&Uri_Unescape_m8700_MethodInfo,
	&Uri_Unescape_m8701_MethodInfo,
	&Uri_ParseAsWindowsUNC_m8702_MethodInfo,
	&Uri_ParseAsWindowsAbsoluteFilePath_m8703_MethodInfo,
	&Uri_ParseAsUnixAbsoluteFilePath_m8704_MethodInfo,
	&Uri_Parse_m8705_MethodInfo,
	&Uri_ParseNoExceptions_m8706_MethodInfo,
	&Uri_CompactEscaped_m8707_MethodInfo,
	&Uri_Reduce_m8708_MethodInfo,
	&Uri_HexUnescapeMultiByte_m8709_MethodInfo,
	&Uri_GetSchemeDelimiter_m8710_MethodInfo,
	&Uri_GetDefaultPort_m8711_MethodInfo,
	&Uri_GetOpaqueWiseSchemeDelimiter_m8712_MethodInfo,
	&Uri_IsPredefinedScheme_m8713_MethodInfo,
	&Uri_get_Parser_m8714_MethodInfo,
	&Uri_EnsureAbsoluteUri_m8715_MethodInfo,
	&Uri_op_Equality_m8716_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Uri_t1986____isUnixFilePath_0_FieldInfo = 
{
	"isUnixFilePath"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___isUnixFilePath_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____source_1_FieldInfo = 
{
	"source"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___source_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____scheme_2_FieldInfo = 
{
	"scheme"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___scheme_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____host_3_FieldInfo = 
{
	"host"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___host_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Uri_t1986____port_4_FieldInfo = 
{
	"port"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___port_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____path_5_FieldInfo = 
{
	"path"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___path_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____query_6_FieldInfo = 
{
	"query"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___query_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____fragment_7_FieldInfo = 
{
	"fragment"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___fragment_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____userinfo_8_FieldInfo = 
{
	"userinfo"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___userinfo_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Uri_t1986____isUnc_9_FieldInfo = 
{
	"isUnc"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___isUnc_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Uri_t1986____isOpaquePart_10_FieldInfo = 
{
	"isOpaquePart"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___isOpaquePart_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Uri_t1986____isAbsoluteUri_11_FieldInfo = 
{
	"isAbsoluteUri"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___isAbsoluteUri_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Uri_t1986____userEscaped_12_FieldInfo = 
{
	"userEscaped"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___userEscaped_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____cachedAbsoluteUri_13_FieldInfo = 
{
	"cachedAbsoluteUri"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___cachedAbsoluteUri_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Uri_t1986____cachedToString_14_FieldInfo = 
{
	"cachedToString"/* name */
	, &String_t_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___cachedToString_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Uri_t1986____cachedHashCode_15_FieldInfo = 
{
	"cachedHashCode"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___cachedHashCode_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_49;
FieldInfo Uri_t1986____hexUpperChars_16_FieldInfo = 
{
	"hexUpperChars"/* name */
	, &String_t_0_0_49/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___hexUpperChars_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____SchemeDelimiter_17_FieldInfo = 
{
	"SchemeDelimiter"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___SchemeDelimiter_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeFile_18_FieldInfo = 
{
	"UriSchemeFile"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeFile_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeFtp_19_FieldInfo = 
{
	"UriSchemeFtp"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeFtp_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeGopher_20_FieldInfo = 
{
	"UriSchemeGopher"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeGopher_20)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeHttp_21_FieldInfo = 
{
	"UriSchemeHttp"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeHttp_21)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeHttps_22_FieldInfo = 
{
	"UriSchemeHttps"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeHttps_22)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeMailto_23_FieldInfo = 
{
	"UriSchemeMailto"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeMailto_23)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeNews_24_FieldInfo = 
{
	"UriSchemeNews"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeNews_24)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeNntp_25_FieldInfo = 
{
	"UriSchemeNntp"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeNntp_25)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeNetPipe_26_FieldInfo = 
{
	"UriSchemeNetPipe"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeNetPipe_26)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_54;
FieldInfo Uri_t1986____UriSchemeNetTcp_27_FieldInfo = 
{
	"UriSchemeNetTcp"/* name */
	, &String_t_0_0_54/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___UriSchemeNetTcp_27)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriSchemeU5BU5D_t2119_0_0_17;
FieldInfo Uri_t1986____schemes_28_FieldInfo = 
{
	"schemes"/* name */
	, &UriSchemeU5BU5D_t2119_0_0_17/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___schemes_28)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriParser_t2116_0_0_129;
FieldInfo Uri_t1986____parser_29_FieldInfo = 
{
	"parser"/* name */
	, &UriParser_t2116_0_0_129/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986, ___parser_29)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo Uri_t1986____U3CU3Ef__switchU24map14_30_FieldInfo = 
{
	"<>f__switch$map14"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___U3CU3Ef__switchU24map14_30)/* offset */
	, 59/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo Uri_t1986____U3CU3Ef__switchU24map15_31_FieldInfo = 
{
	"<>f__switch$map15"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___U3CU3Ef__switchU24map15_31)/* offset */
	, 60/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo Uri_t1986____U3CU3Ef__switchU24map16_32_FieldInfo = 
{
	"<>f__switch$map16"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &Uri_t1986_il2cpp_TypeInfo/* parent */
	, offsetof(Uri_t1986_StaticFields, ___U3CU3Ef__switchU24map16_32)/* offset */
	, 61/* custom_attributes_cache */

};
static FieldInfo* Uri_t1986_FieldInfos[] =
{
	&Uri_t1986____isUnixFilePath_0_FieldInfo,
	&Uri_t1986____source_1_FieldInfo,
	&Uri_t1986____scheme_2_FieldInfo,
	&Uri_t1986____host_3_FieldInfo,
	&Uri_t1986____port_4_FieldInfo,
	&Uri_t1986____path_5_FieldInfo,
	&Uri_t1986____query_6_FieldInfo,
	&Uri_t1986____fragment_7_FieldInfo,
	&Uri_t1986____userinfo_8_FieldInfo,
	&Uri_t1986____isUnc_9_FieldInfo,
	&Uri_t1986____isOpaquePart_10_FieldInfo,
	&Uri_t1986____isAbsoluteUri_11_FieldInfo,
	&Uri_t1986____userEscaped_12_FieldInfo,
	&Uri_t1986____cachedAbsoluteUri_13_FieldInfo,
	&Uri_t1986____cachedToString_14_FieldInfo,
	&Uri_t1986____cachedHashCode_15_FieldInfo,
	&Uri_t1986____hexUpperChars_16_FieldInfo,
	&Uri_t1986____SchemeDelimiter_17_FieldInfo,
	&Uri_t1986____UriSchemeFile_18_FieldInfo,
	&Uri_t1986____UriSchemeFtp_19_FieldInfo,
	&Uri_t1986____UriSchemeGopher_20_FieldInfo,
	&Uri_t1986____UriSchemeHttp_21_FieldInfo,
	&Uri_t1986____UriSchemeHttps_22_FieldInfo,
	&Uri_t1986____UriSchemeMailto_23_FieldInfo,
	&Uri_t1986____UriSchemeNews_24_FieldInfo,
	&Uri_t1986____UriSchemeNntp_25_FieldInfo,
	&Uri_t1986____UriSchemeNetPipe_26_FieldInfo,
	&Uri_t1986____UriSchemeNetTcp_27_FieldInfo,
	&Uri_t1986____schemes_28_FieldInfo,
	&Uri_t1986____parser_29_FieldInfo,
	&Uri_t1986____U3CU3Ef__switchU24map14_30_FieldInfo,
	&Uri_t1986____U3CU3Ef__switchU24map15_31_FieldInfo,
	&Uri_t1986____U3CU3Ef__switchU24map16_32_FieldInfo,
	NULL
};
extern MethodInfo Uri_get_AbsoluteUri_m8674_MethodInfo;
static PropertyInfo Uri_t1986____AbsoluteUri_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "AbsoluteUri"/* name */
	, &Uri_get_AbsoluteUri_m8674_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_Authority_m8675_MethodInfo;
static PropertyInfo Uri_t1986____Authority_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "Authority"/* name */
	, &Uri_get_Authority_m8675_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_Host_m8676_MethodInfo;
static PropertyInfo Uri_t1986____Host_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "Host"/* name */
	, &Uri_get_Host_m8676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_IsFile_m8677_MethodInfo;
static PropertyInfo Uri_t1986____IsFile_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "IsFile"/* name */
	, &Uri_get_IsFile_m8677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_IsLoopback_m8678_MethodInfo;
static PropertyInfo Uri_t1986____IsLoopback_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "IsLoopback"/* name */
	, &Uri_get_IsLoopback_m8678_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_IsUnc_m8679_MethodInfo;
static PropertyInfo Uri_t1986____IsUnc_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "IsUnc"/* name */
	, &Uri_get_IsUnc_m8679_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_Scheme_m8680_MethodInfo;
static PropertyInfo Uri_t1986____Scheme_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "Scheme"/* name */
	, &Uri_get_Scheme_m8680_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_IsAbsoluteUri_m8681_MethodInfo;
static PropertyInfo Uri_t1986____IsAbsoluteUri_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "IsAbsoluteUri"/* name */
	, &Uri_get_IsAbsoluteUri_m8681_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Uri_get_Parser_m8714_MethodInfo;
static PropertyInfo Uri_t1986____Parser_PropertyInfo = 
{
	&Uri_t1986_il2cpp_TypeInfo/* parent */
	, "Parser"/* name */
	, &Uri_get_Parser_m8714_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Uri_t1986_PropertyInfos[] =
{
	&Uri_t1986____AbsoluteUri_PropertyInfo,
	&Uri_t1986____Authority_PropertyInfo,
	&Uri_t1986____Host_PropertyInfo,
	&Uri_t1986____IsFile_PropertyInfo,
	&Uri_t1986____IsLoopback_PropertyInfo,
	&Uri_t1986____IsUnc_PropertyInfo,
	&Uri_t1986____Scheme_PropertyInfo,
	&Uri_t1986____IsAbsoluteUri_PropertyInfo,
	&Uri_t1986____Parser_PropertyInfo,
	NULL
};
static const Il2CppType* Uri_t1986_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UriScheme_t2118_0_0_0,
};
extern MethodInfo Uri_Equals_m8687_MethodInfo;
extern MethodInfo Uri_GetHashCode_m8689_MethodInfo;
extern MethodInfo Uri_ToString_m8696_MethodInfo;
extern MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m8673_MethodInfo;
extern MethodInfo Uri_Unescape_m8700_MethodInfo;
static Il2CppMethodReference Uri_t1986_VTable[] =
{
	&Uri_Equals_m8687_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Uri_GetHashCode_m8689_MethodInfo,
	&Uri_ToString_m8696_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m8673_MethodInfo,
	&Uri_Unescape_m8700_MethodInfo,
};
static bool Uri_t1986_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Uri_t1986_InterfacesTypeInfos[] = 
{
	&ISerializable_t310_0_0_0,
};
static Il2CppInterfaceOffsetPair Uri_t1986_InterfacesOffsets[] = 
{
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Uri_t1986_1_0_0;
struct Uri_t1986;
const Il2CppTypeDefinitionMetadata Uri_t1986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Uri_t1986_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Uri_t1986_InterfacesTypeInfos/* implementedInterfaces */
	, Uri_t1986_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Uri_t1986_VTable/* vtableMethods */
	, Uri_t1986_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Uri_t1986_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Uri"/* name */
	, "System"/* namespaze */
	, Uri_t1986_MethodInfos/* methods */
	, Uri_t1986_PropertyInfos/* properties */
	, Uri_t1986_FieldInfos/* fields */
	, NULL/* events */
	, &Uri_t1986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 58/* custom_attributes_cache */
	, &Uri_t1986_0_0_0/* byval_arg */
	, &Uri_t1986_1_0_0/* this_arg */
	, &Uri_t1986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Uri_t1986)/* instance_size */
	, sizeof (Uri_t1986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Uri_t1986_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 48/* method_count */
	, 9/* property_count */
	, 33/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.UriFormatException
#include "System_System_UriFormatException.h"
// Metadata Definition System.UriFormatException
extern TypeInfo UriFormatException_t2120_il2cpp_TypeInfo;
// System.UriFormatException
#include "System_System_UriFormatExceptionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor()
MethodInfo UriFormatException__ctor_m8717_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m8717/* method */
	, &UriFormatException_t2120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UriFormatException_t2120_UriFormatException__ctor_m8718_ParameterInfos[] = 
{
	{"message", 0, 134218612, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.String)
MethodInfo UriFormatException__ctor_m8718_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m8718/* method */
	, &UriFormatException_t2120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UriFormatException_t2120_UriFormatException__ctor_m8718_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo UriFormatException_t2120_UriFormatException__ctor_m8719_ParameterInfos[] = 
{
	{"info", 0, 134218613, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134218614, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo UriFormatException__ctor_m8719_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m8719/* method */
	, &UriFormatException_t2120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, UriFormatException_t2120_UriFormatException__ctor_m8719_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo UriFormatException_t2120_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m8720_ParameterInfos[] = 
{
	{"info", 0, 134218615, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134218616, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m8720_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m8720/* method */
	, &UriFormatException_t2120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, UriFormatException_t2120_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m8720_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UriFormatException_t2120_MethodInfos[] =
{
	&UriFormatException__ctor_m8717_MethodInfo,
	&UriFormatException__ctor_m8718_MethodInfo,
	&UriFormatException__ctor_m8719_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m8720_MethodInfo,
	NULL
};
extern MethodInfo Exception_ToString_m1311_MethodInfo;
extern MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m8720_MethodInfo;
extern MethodInfo Exception_get_InnerException_m1313_MethodInfo;
extern MethodInfo Exception_get_Message_m1314_MethodInfo;
extern MethodInfo Exception_get_Source_m1315_MethodInfo;
extern MethodInfo Exception_get_StackTrace_m1316_MethodInfo;
extern MethodInfo Exception_GetObjectData_m1312_MethodInfo;
extern MethodInfo Exception_GetType_m1317_MethodInfo;
static Il2CppMethodReference UriFormatException_t2120_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Exception_ToString_m1311_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m8720_MethodInfo,
	&Exception_get_InnerException_m1313_MethodInfo,
	&Exception_get_Message_m1314_MethodInfo,
	&Exception_get_Source_m1315_MethodInfo,
	&Exception_get_StackTrace_m1316_MethodInfo,
	&Exception_GetObjectData_m1312_MethodInfo,
	&Exception_GetType_m1317_MethodInfo,
};
static bool UriFormatException_t2120_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UriFormatException_t2120_InterfacesTypeInfos[] = 
{
	&ISerializable_t310_0_0_0,
};
extern Il2CppType _Exception_t324_0_0_0;
static Il2CppInterfaceOffsetPair UriFormatException_t2120_InterfacesOffsets[] = 
{
	{ &ISerializable_t310_0_0_0, 4},
	{ &_Exception_t324_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType UriFormatException_t2120_0_0_0;
extern Il2CppType UriFormatException_t2120_1_0_0;
extern Il2CppType FormatException_t201_0_0_0;
struct UriFormatException_t2120;
const Il2CppTypeDefinitionMetadata UriFormatException_t2120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UriFormatException_t2120_InterfacesTypeInfos/* implementedInterfaces */
	, UriFormatException_t2120_InterfacesOffsets/* interfaceOffsets */
	, &FormatException_t201_0_0_0/* parent */
	, UriFormatException_t2120_VTable/* vtableMethods */
	, UriFormatException_t2120_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UriFormatException_t2120_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriFormatException"/* name */
	, "System"/* namespaze */
	, UriFormatException_t2120_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UriFormatException_t2120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriFormatException_t2120_0_0_0/* byval_arg */
	, &UriFormatException_t2120_1_0_0/* this_arg */
	, &UriFormatException_t2120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriFormatException_t2120)/* instance_size */
	, sizeof (UriFormatException_t2120)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
// Metadata Definition System.UriHostNameType
extern TypeInfo UriHostNameType_t2121_il2cpp_TypeInfo;
// System.UriHostNameType
#include "System_System_UriHostNameTypeMethodDeclarations.h"
static MethodInfo* UriHostNameType_t2121_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo UriHostNameType_t2121____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &UriHostNameType_t2121_il2cpp_TypeInfo/* parent */
	, offsetof(UriHostNameType_t2121, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriHostNameType_t2121_0_0_32854;
FieldInfo UriHostNameType_t2121____Unknown_2_FieldInfo = 
{
	"Unknown"/* name */
	, &UriHostNameType_t2121_0_0_32854/* type */
	, &UriHostNameType_t2121_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriHostNameType_t2121_0_0_32854;
FieldInfo UriHostNameType_t2121____Basic_3_FieldInfo = 
{
	"Basic"/* name */
	, &UriHostNameType_t2121_0_0_32854/* type */
	, &UriHostNameType_t2121_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriHostNameType_t2121_0_0_32854;
FieldInfo UriHostNameType_t2121____Dns_4_FieldInfo = 
{
	"Dns"/* name */
	, &UriHostNameType_t2121_0_0_32854/* type */
	, &UriHostNameType_t2121_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriHostNameType_t2121_0_0_32854;
FieldInfo UriHostNameType_t2121____IPv4_5_FieldInfo = 
{
	"IPv4"/* name */
	, &UriHostNameType_t2121_0_0_32854/* type */
	, &UriHostNameType_t2121_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriHostNameType_t2121_0_0_32854;
FieldInfo UriHostNameType_t2121____IPv6_6_FieldInfo = 
{
	"IPv6"/* name */
	, &UriHostNameType_t2121_0_0_32854/* type */
	, &UriHostNameType_t2121_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UriHostNameType_t2121_FieldInfos[] =
{
	&UriHostNameType_t2121____value___1_FieldInfo,
	&UriHostNameType_t2121____Unknown_2_FieldInfo,
	&UriHostNameType_t2121____Basic_3_FieldInfo,
	&UriHostNameType_t2121____Dns_4_FieldInfo,
	&UriHostNameType_t2121____IPv4_5_FieldInfo,
	&UriHostNameType_t2121____IPv6_6_FieldInfo,
	NULL
};
static const int32_t UriHostNameType_t2121____Unknown_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UriHostNameType_t2121____Unknown_2_DefaultValue = 
{
	&UriHostNameType_t2121____Unknown_2_FieldInfo/* field */
	, { (char*)&UriHostNameType_t2121____Unknown_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriHostNameType_t2121____Basic_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UriHostNameType_t2121____Basic_3_DefaultValue = 
{
	&UriHostNameType_t2121____Basic_3_FieldInfo/* field */
	, { (char*)&UriHostNameType_t2121____Basic_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriHostNameType_t2121____Dns_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UriHostNameType_t2121____Dns_4_DefaultValue = 
{
	&UriHostNameType_t2121____Dns_4_FieldInfo/* field */
	, { (char*)&UriHostNameType_t2121____Dns_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriHostNameType_t2121____IPv4_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry UriHostNameType_t2121____IPv4_5_DefaultValue = 
{
	&UriHostNameType_t2121____IPv4_5_FieldInfo/* field */
	, { (char*)&UriHostNameType_t2121____IPv4_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriHostNameType_t2121____IPv6_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry UriHostNameType_t2121____IPv6_6_DefaultValue = 
{
	&UriHostNameType_t2121____IPv6_6_FieldInfo/* field */
	, { (char*)&UriHostNameType_t2121____IPv6_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UriHostNameType_t2121_FieldDefaultValues[] = 
{
	&UriHostNameType_t2121____Unknown_2_DefaultValue,
	&UriHostNameType_t2121____Basic_3_DefaultValue,
	&UriHostNameType_t2121____Dns_4_DefaultValue,
	&UriHostNameType_t2121____IPv4_5_DefaultValue,
	&UriHostNameType_t2121____IPv6_6_DefaultValue,
	NULL
};
static Il2CppMethodReference UriHostNameType_t2121_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool UriHostNameType_t2121_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriHostNameType_t2121_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType UriHostNameType_t2121_0_0_0;
extern Il2CppType UriHostNameType_t2121_1_0_0;
const Il2CppTypeDefinitionMetadata UriHostNameType_t2121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriHostNameType_t2121_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, UriHostNameType_t2121_VTable/* vtableMethods */
	, UriHostNameType_t2121_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UriHostNameType_t2121_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriHostNameType"/* name */
	, "System"/* namespaze */
	, UriHostNameType_t2121_MethodInfos/* methods */
	, NULL/* properties */
	, UriHostNameType_t2121_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriHostNameType_t2121_0_0_0/* byval_arg */
	, &UriHostNameType_t2121_1_0_0/* this_arg */
	, &UriHostNameType_t2121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UriHostNameType_t2121_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriHostNameType_t2121)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriHostNameType_t2121)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriKind
#include "System_System_UriKind.h"
// Metadata Definition System.UriKind
extern TypeInfo UriKind_t2122_il2cpp_TypeInfo;
// System.UriKind
#include "System_System_UriKindMethodDeclarations.h"
static MethodInfo* UriKind_t2122_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo UriKind_t2122____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &UriKind_t2122_il2cpp_TypeInfo/* parent */
	, offsetof(UriKind_t2122, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriKind_t2122_0_0_32854;
FieldInfo UriKind_t2122____RelativeOrAbsolute_2_FieldInfo = 
{
	"RelativeOrAbsolute"/* name */
	, &UriKind_t2122_0_0_32854/* type */
	, &UriKind_t2122_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriKind_t2122_0_0_32854;
FieldInfo UriKind_t2122____Absolute_3_FieldInfo = 
{
	"Absolute"/* name */
	, &UriKind_t2122_0_0_32854/* type */
	, &UriKind_t2122_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriKind_t2122_0_0_32854;
FieldInfo UriKind_t2122____Relative_4_FieldInfo = 
{
	"Relative"/* name */
	, &UriKind_t2122_0_0_32854/* type */
	, &UriKind_t2122_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UriKind_t2122_FieldInfos[] =
{
	&UriKind_t2122____value___1_FieldInfo,
	&UriKind_t2122____RelativeOrAbsolute_2_FieldInfo,
	&UriKind_t2122____Absolute_3_FieldInfo,
	&UriKind_t2122____Relative_4_FieldInfo,
	NULL
};
static const int32_t UriKind_t2122____RelativeOrAbsolute_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UriKind_t2122____RelativeOrAbsolute_2_DefaultValue = 
{
	&UriKind_t2122____RelativeOrAbsolute_2_FieldInfo/* field */
	, { (char*)&UriKind_t2122____RelativeOrAbsolute_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriKind_t2122____Absolute_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UriKind_t2122____Absolute_3_DefaultValue = 
{
	&UriKind_t2122____Absolute_3_FieldInfo/* field */
	, { (char*)&UriKind_t2122____Absolute_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriKind_t2122____Relative_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UriKind_t2122____Relative_4_DefaultValue = 
{
	&UriKind_t2122____Relative_4_FieldInfo/* field */
	, { (char*)&UriKind_t2122____Relative_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UriKind_t2122_FieldDefaultValues[] = 
{
	&UriKind_t2122____RelativeOrAbsolute_2_DefaultValue,
	&UriKind_t2122____Absolute_3_DefaultValue,
	&UriKind_t2122____Relative_4_DefaultValue,
	NULL
};
static Il2CppMethodReference UriKind_t2122_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool UriKind_t2122_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriKind_t2122_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType UriKind_t2122_1_0_0;
const Il2CppTypeDefinitionMetadata UriKind_t2122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriKind_t2122_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, UriKind_t2122_VTable/* vtableMethods */
	, UriKind_t2122_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UriKind_t2122_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriKind"/* name */
	, "System"/* namespaze */
	, UriKind_t2122_MethodInfos/* methods */
	, NULL/* properties */
	, UriKind_t2122_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriKind_t2122_0_0_0/* byval_arg */
	, &UriKind_t2122_1_0_0/* this_arg */
	, &UriKind_t2122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UriKind_t2122_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriKind_t2122)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriKind_t2122)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriParser
#include "System_System_UriParser.h"
// Metadata Definition System.UriParser
extern TypeInfo UriParser_t2116_il2cpp_TypeInfo;
// System.UriParser
#include "System_System_UriParserMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.ctor()
MethodInfo UriParser__ctor_m8721_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriParser__ctor_m8721/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.cctor()
MethodInfo UriParser__cctor_m8722_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&UriParser__cctor_m8722/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Uri_t1986_0_0_0;
extern Il2CppType UriFormatException_t2120_1_0_2;
static ParameterInfo UriParser_t2116_UriParser_InitializeAndValidate_m8723_ParameterInfos[] = 
{
	{"uri", 0, 134218617, 0, &Uri_t1986_0_0_0},
	{"parsingError", 1, 134218618, 0, &UriFormatException_t2120_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_UriFormatExceptionU26_t2182 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&)
MethodInfo UriParser_InitializeAndValidate_m8723_MethodInfo = 
{
	"InitializeAndValidate"/* name */
	, (methodPointerType)&UriParser_InitializeAndValidate_m8723/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_UriFormatExceptionU26_t2182/* invoker_method */
	, UriParser_t2116_UriParser_InitializeAndValidate_m8723_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 453/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo UriParser_t2116_UriParser_OnRegister_m8724_ParameterInfos[] = 
{
	{"schemeName", 0, 134218619, 0, &String_t_0_0_0},
	{"defaultPort", 1, 134218620, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::OnRegister(System.String,System.Int32)
MethodInfo UriParser_OnRegister_m8724_MethodInfo = 
{
	"OnRegister"/* name */
	, (methodPointerType)&UriParser_OnRegister_m8724/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189/* invoker_method */
	, UriParser_t2116_UriParser_OnRegister_m8724_ParameterInfos/* parameters */
	, 65/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UriParser_t2116_UriParser_set_SchemeName_m8725_ParameterInfos[] = 
{
	{"value", 0, 134218621, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_SchemeName(System.String)
MethodInfo UriParser_set_SchemeName_m8725_MethodInfo = 
{
	"set_SchemeName"/* name */
	, (methodPointerType)&UriParser_set_SchemeName_m8725/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UriParser_t2116_UriParser_set_SchemeName_m8725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.UriParser::get_DefaultPort()
MethodInfo UriParser_get_DefaultPort_m8726_MethodInfo = 
{
	"get_DefaultPort"/* name */
	, (methodPointerType)&UriParser_get_DefaultPort_m8726/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo UriParser_t2116_UriParser_set_DefaultPort_m8727_ParameterInfos[] = 
{
	{"value", 0, 134218622, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_DefaultPort(System.Int32)
MethodInfo UriParser_set_DefaultPort_m8727_MethodInfo = 
{
	"set_DefaultPort"/* name */
	, (methodPointerType)&UriParser_set_DefaultPort_m8727/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, UriParser_t2116_UriParser_set_DefaultPort_m8727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::CreateDefaults()
MethodInfo UriParser_CreateDefaults_m8728_MethodInfo = 
{
	"CreateDefaults"/* name */
	, (methodPointerType)&UriParser_CreateDefaults_m8728/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Hashtable_t1667_0_0_0;
extern Il2CppType UriParser_t2116_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo UriParser_t2116_UriParser_InternalRegister_m8729_ParameterInfos[] = 
{
	{"table", 0, 134218623, 0, &Hashtable_t1667_0_0_0},
	{"uriParser", 1, 134218624, 0, &UriParser_t2116_0_0_0},
	{"schemeName", 2, 134218625, 0, &String_t_0_0_0},
	{"defaultPort", 3, 134218626, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InternalRegister(System.Collections.Hashtable,System.UriParser,System.String,System.Int32)
MethodInfo UriParser_InternalRegister_m8729_MethodInfo = 
{
	"InternalRegister"/* name */
	, (methodPointerType)&UriParser_InternalRegister_m8729/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189/* invoker_method */
	, UriParser_t2116_UriParser_InternalRegister_m8729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UriParser_t2116_UriParser_GetParser_m8730_ParameterInfos[] = 
{
	{"schemeName", 0, 134218627, 0, &String_t_0_0_0},
};
extern Il2CppType UriParser_t2116_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.UriParser System.UriParser::GetParser(System.String)
MethodInfo UriParser_GetParser_m8730_MethodInfo = 
{
	"GetParser"/* name */
	, (methodPointerType)&UriParser_GetParser_m8730/* method */
	, &UriParser_t2116_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t2116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UriParser_t2116_UriParser_GetParser_m8730_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UriParser_t2116_MethodInfos[] =
{
	&UriParser__ctor_m8721_MethodInfo,
	&UriParser__cctor_m8722_MethodInfo,
	&UriParser_InitializeAndValidate_m8723_MethodInfo,
	&UriParser_OnRegister_m8724_MethodInfo,
	&UriParser_set_SchemeName_m8725_MethodInfo,
	&UriParser_get_DefaultPort_m8726_MethodInfo,
	&UriParser_set_DefaultPort_m8727_MethodInfo,
	&UriParser_CreateDefaults_m8728_MethodInfo,
	&UriParser_InternalRegister_m8729_MethodInfo,
	&UriParser_GetParser_m8730_MethodInfo,
	NULL
};
extern Il2CppType Object_t_0_0_17;
FieldInfo UriParser_t2116____lock_object_0_FieldInfo = 
{
	"lock_object"/* name */
	, &Object_t_0_0_17/* type */
	, &UriParser_t2116_il2cpp_TypeInfo/* parent */
	, offsetof(UriParser_t2116_StaticFields, ___lock_object_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo UriParser_t2116____table_1_FieldInfo = 
{
	"table"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &UriParser_t2116_il2cpp_TypeInfo/* parent */
	, offsetof(UriParser_t2116_StaticFields, ___table_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_3;
FieldInfo UriParser_t2116____scheme_name_2_FieldInfo = 
{
	"scheme_name"/* name */
	, &String_t_0_0_3/* type */
	, &UriParser_t2116_il2cpp_TypeInfo/* parent */
	, offsetof(UriParser_t2116, ___scheme_name_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo UriParser_t2116____default_port_3_FieldInfo = 
{
	"default_port"/* name */
	, &Int32_t189_0_0_1/* type */
	, &UriParser_t2116_il2cpp_TypeInfo/* parent */
	, offsetof(UriParser_t2116, ___default_port_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Regex_t200_0_0_49;
FieldInfo UriParser_t2116____uri_regex_4_FieldInfo = 
{
	"uri_regex"/* name */
	, &Regex_t200_0_0_49/* type */
	, &UriParser_t2116_il2cpp_TypeInfo/* parent */
	, offsetof(UriParser_t2116_StaticFields, ___uri_regex_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Regex_t200_0_0_49;
FieldInfo UriParser_t2116____auth_regex_5_FieldInfo = 
{
	"auth_regex"/* name */
	, &Regex_t200_0_0_49/* type */
	, &UriParser_t2116_il2cpp_TypeInfo/* parent */
	, offsetof(UriParser_t2116_StaticFields, ___auth_regex_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UriParser_t2116_FieldInfos[] =
{
	&UriParser_t2116____lock_object_0_FieldInfo,
	&UriParser_t2116____table_1_FieldInfo,
	&UriParser_t2116____scheme_name_2_FieldInfo,
	&UriParser_t2116____default_port_3_FieldInfo,
	&UriParser_t2116____uri_regex_4_FieldInfo,
	&UriParser_t2116____auth_regex_5_FieldInfo,
	NULL
};
extern MethodInfo UriParser_set_SchemeName_m8725_MethodInfo;
static PropertyInfo UriParser_t2116____SchemeName_PropertyInfo = 
{
	&UriParser_t2116_il2cpp_TypeInfo/* parent */
	, "SchemeName"/* name */
	, NULL/* get */
	, &UriParser_set_SchemeName_m8725_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo UriParser_get_DefaultPort_m8726_MethodInfo;
extern MethodInfo UriParser_set_DefaultPort_m8727_MethodInfo;
static PropertyInfo UriParser_t2116____DefaultPort_PropertyInfo = 
{
	&UriParser_t2116_il2cpp_TypeInfo/* parent */
	, "DefaultPort"/* name */
	, &UriParser_get_DefaultPort_m8726_MethodInfo/* get */
	, &UriParser_set_DefaultPort_m8727_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* UriParser_t2116_PropertyInfos[] =
{
	&UriParser_t2116____SchemeName_PropertyInfo,
	&UriParser_t2116____DefaultPort_PropertyInfo,
	NULL
};
static Il2CppMethodReference UriParser_t2116_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&UriParser_InitializeAndValidate_m8723_MethodInfo,
	&UriParser_OnRegister_m8724_MethodInfo,
};
static bool UriParser_t2116_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType UriParser_t2116_1_0_0;
struct UriParser_t2116;
const Il2CppTypeDefinitionMetadata UriParser_t2116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UriParser_t2116_VTable/* vtableMethods */
	, UriParser_t2116_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UriParser_t2116_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriParser"/* name */
	, "System"/* namespaze */
	, UriParser_t2116_MethodInfos/* methods */
	, UriParser_t2116_PropertyInfos/* properties */
	, UriParser_t2116_FieldInfos/* fields */
	, NULL/* events */
	, &UriParser_t2116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriParser_t2116_0_0_0/* byval_arg */
	, &UriParser_t2116_1_0_0/* this_arg */
	, &UriParser_t2116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriParser_t2116)/* instance_size */
	, sizeof (UriParser_t2116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UriParser_t2116_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UriPartial
#include "System_System_UriPartial.h"
// Metadata Definition System.UriPartial
extern TypeInfo UriPartial_t2123_il2cpp_TypeInfo;
// System.UriPartial
#include "System_System_UriPartialMethodDeclarations.h"
static MethodInfo* UriPartial_t2123_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo UriPartial_t2123____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &UriPartial_t2123_il2cpp_TypeInfo/* parent */
	, offsetof(UriPartial_t2123, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriPartial_t2123_0_0_32854;
FieldInfo UriPartial_t2123____Scheme_2_FieldInfo = 
{
	"Scheme"/* name */
	, &UriPartial_t2123_0_0_32854/* type */
	, &UriPartial_t2123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriPartial_t2123_0_0_32854;
FieldInfo UriPartial_t2123____Authority_3_FieldInfo = 
{
	"Authority"/* name */
	, &UriPartial_t2123_0_0_32854/* type */
	, &UriPartial_t2123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriPartial_t2123_0_0_32854;
FieldInfo UriPartial_t2123____Path_4_FieldInfo = 
{
	"Path"/* name */
	, &UriPartial_t2123_0_0_32854/* type */
	, &UriPartial_t2123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UriPartial_t2123_0_0_32854;
FieldInfo UriPartial_t2123____Query_5_FieldInfo = 
{
	"Query"/* name */
	, &UriPartial_t2123_0_0_32854/* type */
	, &UriPartial_t2123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UriPartial_t2123_FieldInfos[] =
{
	&UriPartial_t2123____value___1_FieldInfo,
	&UriPartial_t2123____Scheme_2_FieldInfo,
	&UriPartial_t2123____Authority_3_FieldInfo,
	&UriPartial_t2123____Path_4_FieldInfo,
	&UriPartial_t2123____Query_5_FieldInfo,
	NULL
};
static const int32_t UriPartial_t2123____Scheme_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UriPartial_t2123____Scheme_2_DefaultValue = 
{
	&UriPartial_t2123____Scheme_2_FieldInfo/* field */
	, { (char*)&UriPartial_t2123____Scheme_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriPartial_t2123____Authority_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UriPartial_t2123____Authority_3_DefaultValue = 
{
	&UriPartial_t2123____Authority_3_FieldInfo/* field */
	, { (char*)&UriPartial_t2123____Authority_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriPartial_t2123____Path_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UriPartial_t2123____Path_4_DefaultValue = 
{
	&UriPartial_t2123____Path_4_FieldInfo/* field */
	, { (char*)&UriPartial_t2123____Path_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UriPartial_t2123____Query_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry UriPartial_t2123____Query_5_DefaultValue = 
{
	&UriPartial_t2123____Query_5_FieldInfo/* field */
	, { (char*)&UriPartial_t2123____Query_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UriPartial_t2123_FieldDefaultValues[] = 
{
	&UriPartial_t2123____Scheme_2_DefaultValue,
	&UriPartial_t2123____Authority_3_DefaultValue,
	&UriPartial_t2123____Path_4_DefaultValue,
	&UriPartial_t2123____Query_5_DefaultValue,
	NULL
};
static Il2CppMethodReference UriPartial_t2123_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool UriPartial_t2123_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriPartial_t2123_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType UriPartial_t2123_1_0_0;
const Il2CppTypeDefinitionMetadata UriPartial_t2123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriPartial_t2123_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, UriPartial_t2123_VTable/* vtableMethods */
	, UriPartial_t2123_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UriPartial_t2123_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriPartial"/* name */
	, "System"/* namespaze */
	, UriPartial_t2123_MethodInfos/* methods */
	, NULL/* properties */
	, UriPartial_t2123_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriPartial_t2123_0_0_0/* byval_arg */
	, &UriPartial_t2123_1_0_0/* this_arg */
	, &UriPartial_t2123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UriPartial_t2123_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriPartial_t2123)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriPartial_t2123)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriTypeConverter
#include "System_System_UriTypeConverter.h"
// Metadata Definition System.UriTypeConverter
extern TypeInfo UriTypeConverter_t2124_il2cpp_TypeInfo;
// System.UriTypeConverter
#include "System_System_UriTypeConverterMethodDeclarations.h"
static MethodInfo* UriTypeConverter_t2124_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference UriTypeConverter_t2124_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool UriTypeConverter_t2124_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType UriTypeConverter_t2124_0_0_0;
extern Il2CppType UriTypeConverter_t2124_1_0_0;
extern Il2CppType TypeConverter_t1980_0_0_0;
struct UriTypeConverter_t2124;
const Il2CppTypeDefinitionMetadata UriTypeConverter_t2124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeConverter_t1980_0_0_0/* parent */
	, UriTypeConverter_t2124_VTable/* vtableMethods */
	, UriTypeConverter_t2124_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UriTypeConverter_t2124_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriTypeConverter"/* name */
	, "System"/* namespaze */
	, UriTypeConverter_t2124_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UriTypeConverter_t2124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriTypeConverter_t2124_0_0_0/* byval_arg */
	, &UriTypeConverter_t2124_1_0_0/* this_arg */
	, &UriTypeConverter_t2124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriTypeConverter_t2124)/* instance_size */
	, sizeof (UriTypeConverter_t2124)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallba.h"
// Metadata Definition System.Net.Security.RemoteCertificateValidationCallback
extern TypeInfo RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo;
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallbaMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback__ctor_m8731_ParameterInfos[] = 
{
	{"object", 0, 134218628, 0, &Object_t_0_0_0},
	{"method", 1, 134218629, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Net.Security.RemoteCertificateValidationCallback::.ctor(System.Object,System.IntPtr)
MethodInfo RemoteCertificateValidationCallback__ctor_m8731_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback__ctor_m8731/* method */
	, &RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback__ctor_m8731_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType X509Chain_t2037_0_0_0;
extern Il2CppType X509Chain_t2037_0_0_0;
extern Il2CppType SslPolicyErrors_t1983_0_0_0;
extern Il2CppType SslPolicyErrors_t1983_0_0_0;
static ParameterInfo RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback_Invoke_m8732_ParameterInfos[] = 
{
	{"sender", 0, 134218630, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218631, 0, &X509Certificate_t2026_0_0_0},
	{"chain", 2, 134218632, 0, &X509Chain_t2037_0_0_0},
	{"sslPolicyErrors", 3, 134218633, 0, &SslPolicyErrors_t1983_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::Invoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
MethodInfo RemoteCertificateValidationCallback_Invoke_m8732_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_Invoke_m8732/* method */
	, &RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_Int32_t189/* invoker_method */
	, RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback_Invoke_m8732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType X509Chain_t2037_0_0_0;
extern Il2CppType SslPolicyErrors_t1983_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback_BeginInvoke_m8733_ParameterInfos[] = 
{
	{"sender", 0, 134218634, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218635, 0, &X509Certificate_t2026_0_0_0},
	{"chain", 2, 134218636, 0, &X509Chain_t2037_0_0_0},
	{"sslPolicyErrors", 3, 134218637, 0, &SslPolicyErrors_t1983_0_0_0},
	{"callback", 4, 134218638, 0, &AsyncCallback_t20_0_0_0},
	{"object", 5, 134218639, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Net.Security.RemoteCertificateValidationCallback::BeginInvoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors,System.AsyncCallback,System.Object)
MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m8733_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_BeginInvoke_m8733/* method */
	, &RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t189_Object_t_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback_BeginInvoke_m8733_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback_EndInvoke_m8734_ParameterInfos[] = 
{
	{"result", 0, 134218640, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::EndInvoke(System.IAsyncResult)
MethodInfo RemoteCertificateValidationCallback_EndInvoke_m8734_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_EndInvoke_m8734/* method */
	, &RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1993_RemoteCertificateValidationCallback_EndInvoke_m8734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemoteCertificateValidationCallback_t1993_MethodInfos[] =
{
	&RemoteCertificateValidationCallback__ctor_m8731_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m8732_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m8733_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m8734_MethodInfo,
	NULL
};
extern MethodInfo RemoteCertificateValidationCallback_Invoke_m8732_MethodInfo;
extern MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m8733_MethodInfo;
extern MethodInfo RemoteCertificateValidationCallback_EndInvoke_m8734_MethodInfo;
static Il2CppMethodReference RemoteCertificateValidationCallback_t1993_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m8732_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m8733_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m8734_MethodInfo,
};
static bool RemoteCertificateValidationCallback_t1993_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RemoteCertificateValidationCallback_t1993_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType RemoteCertificateValidationCallback_t1993_0_0_0;
extern Il2CppType RemoteCertificateValidationCallback_t1993_1_0_0;
struct RemoteCertificateValidationCallback_t1993;
const Il2CppTypeDefinitionMetadata RemoteCertificateValidationCallback_t1993_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemoteCertificateValidationCallback_t1993_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, RemoteCertificateValidationCallback_t1993_VTable/* vtableMethods */
	, RemoteCertificateValidationCallback_t1993_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteCertificateValidationCallback"/* name */
	, "System.Net.Security"/* namespaze */
	, RemoteCertificateValidationCallback_t1993_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &RemoteCertificateValidationCallback_t1993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteCertificateValidationCallback_t1993_0_0_0/* byval_arg */
	, &RemoteCertificateValidationCallback_t1993_1_0_0/* this_arg */
	, &RemoteCertificateValidationCallback_t1993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1993/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteCertificateValidationCallback_t1993)/* instance_size */
	, sizeof (RemoteCertificateValidationCallback_t1993)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t2125_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU24128_t2125_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU24128_t2125_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU24128_t2125_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType U24ArrayTypeU24128_t2125_0_0_0;
extern Il2CppType U24ArrayTypeU24128_t2125_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t2127_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t2125_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2127_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU24128_t2125_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t2125_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU24128_t2125_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t2125_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU24128_t2125_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t2125_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t2125_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t2125_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t2125_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2125_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2125_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t2125)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t2125)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t2125_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t2126_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU2412_t2126_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU2412_t2126_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU2412_t2126_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType U24ArrayTypeU2412_t2126_0_0_0;
extern Il2CppType U24ArrayTypeU2412_t2126_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t2126_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2127_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU2412_t2126_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t2126_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU2412_t2126_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t2126_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU2412_t2126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t2126_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t2126_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t2126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t2126_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2126_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2126_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t2126)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t2126)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t2126_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static MethodInfo* U3CPrivateImplementationDetailsU3E_t2127_MethodInfos[] =
{
	NULL
};
extern Il2CppType U24ArrayTypeU24128_t2125_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D2_0_FieldInfo = 
{
	"$$field-2"/* name */
	, &U24ArrayTypeU24128_t2125_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2127_StaticFields, ___U24U24fieldU2D2_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2412_t2126_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D3_1_FieldInfo = 
{
	"$$field-3"/* name */
	, &U24ArrayTypeU2412_t2126_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2127_StaticFields, ___U24U24fieldU2D3_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2412_t2126_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D4_2_FieldInfo = 
{
	"$$field-4"/* name */
	, &U24ArrayTypeU2412_t2126_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2127_StaticFields, ___U24U24fieldU2D4_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CPrivateImplementationDetailsU3E_t2127_FieldInfos[] =
{
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D2_0_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D3_1_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D4_2_FieldInfo,
	NULL
};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D2_0_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x1, 0x1, 0x0, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D2_0_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D2_0_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D2_0_DefaultValueData, &U24ArrayTypeU24128_t2125_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D3_1_DefaultValueData[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x9, 0x1, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D3_1_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D3_1_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D3_1_DefaultValueData, &U24ArrayTypeU2412_t2126_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D4_2_DefaultValueData[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x7, 0x2, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D4_2_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D4_2_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D4_2_DefaultValueData, &U24ArrayTypeU2412_t2126_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* U3CPrivateImplementationDetailsU3E_t2127_FieldDefaultValues[] = 
{
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D2_0_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D3_1_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2127____U24U24fieldU2D4_2_DefaultValue,
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U24ArrayTypeU24128_t2125_0_0_0,
	&U24ArrayTypeU2412_t2126_0_0_0,
};
static Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t2127_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t2127_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t2127_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t2127;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t2127_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t2127_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t2127_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t2127_MethodInfos/* methods */
	, NULL/* properties */
	, U3CPrivateImplementationDetailsU3E_t2127_FieldInfos/* fields */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t2127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 66/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t2127_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t2127_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t2127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, U3CPrivateImplementationDetailsU3E_t2127_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2127)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2127)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t2127_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
