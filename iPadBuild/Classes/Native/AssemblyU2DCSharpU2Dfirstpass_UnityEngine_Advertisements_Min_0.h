﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IO.StringReader
struct StringReader_t147;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t75;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Advertisements.MiniJSON.Json/Parser
struct  Parser_t148  : public Object_t
{
	// System.IO.StringReader UnityEngine.Advertisements.MiniJSON.Json/Parser::json
	StringReader_t147 * ___json_1;
};
struct Parser_t148_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnityEngine.Advertisements.MiniJSON.Json/Parser::<>f__switch$map2
	Dictionary_2_t75 * ___U3CU3Ef__switchU24map2_2;
};
