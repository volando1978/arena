﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>
struct InternalEnumerator_1_t3490;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16315_gshared (InternalEnumerator_1_t3490 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m16315(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3490 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m16315_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16316_gshared (InternalEnumerator_1_t3490 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16316(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3490 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16316_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16317_gshared (InternalEnumerator_1_t3490 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16317(__this, method) (( void (*) (InternalEnumerator_1_t3490 *, MethodInfo*))InternalEnumerator_1_Dispose_m16317_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16318_gshared (InternalEnumerator_1_t3490 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16318(__this, method) (( bool (*) (InternalEnumerator_1_t3490 *, MethodInfo*))InternalEnumerator_1_MoveNext_m16318_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern "C" DictionaryEntry_t2128  InternalEnumerator_1_get_Current_m16319_gshared (InternalEnumerator_1_t3490 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16319(__this, method) (( DictionaryEntry_t2128  (*) (InternalEnumerator_1_t3490 *, MethodInfo*))InternalEnumerator_1_get_Current_m16319_gshared)(__this, method)
