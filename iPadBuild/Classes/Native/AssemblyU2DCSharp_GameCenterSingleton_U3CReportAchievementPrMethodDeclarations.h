﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GameCenterSingleton/<ReportAchievementProgress>c__AnonStorey65
struct U3CReportAchievementProgressU3Ec__AnonStorey65_t731;

// System.Void GameCenterSingleton/<ReportAchievementProgress>c__AnonStorey65::.ctor()
extern "C" void U3CReportAchievementProgressU3Ec__AnonStorey65__ctor_m3159 (U3CReportAchievementProgressU3Ec__AnonStorey65_t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton/<ReportAchievementProgress>c__AnonStorey65::<>m__A4(System.Boolean)
extern "C" void U3CReportAchievementProgressU3Ec__AnonStorey65_U3CU3Em__A4_m3160 (U3CReportAchievementProgressU3Ec__AnonStorey65_t731 * __this, bool ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
