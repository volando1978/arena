﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>
struct Dictionary_2_t110;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>
struct  Enumerator_t3604 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::dictionary
	Dictionary_2_t110 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::current
	KeyValuePair_2_t3601  ___current_3;
};
