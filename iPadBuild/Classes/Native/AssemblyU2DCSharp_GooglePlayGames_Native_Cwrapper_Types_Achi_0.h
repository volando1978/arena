﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi_0.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementState
struct  AchievementState_t507 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/AchievementState::value__
	int32_t ___value___1;
};
