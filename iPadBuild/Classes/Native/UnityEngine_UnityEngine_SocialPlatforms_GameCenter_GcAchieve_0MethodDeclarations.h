﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_t1617;
struct GcAchievementData_t1617_marshaled;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1625;

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern "C" Achievement_t1625 * GcAchievementData_ToAchievement_m7315 (GcAchievementData_t1617 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcAchievementData_t1617_marshal(const GcAchievementData_t1617& unmarshaled, GcAchievementData_t1617_marshaled& marshaled);
void GcAchievementData_t1617_marshal_back(const GcAchievementData_t1617_marshaled& marshaled, GcAchievementData_t1617& unmarshaled);
void GcAchievementData_t1617_marshal_cleanup(GcAchievementData_t1617_marshaled& marshaled);
