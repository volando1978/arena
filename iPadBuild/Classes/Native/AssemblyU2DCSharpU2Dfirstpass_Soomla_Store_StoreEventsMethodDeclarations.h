﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreEvents
struct StoreEvents_t90;
// Soomla.Store.StoreEvents/RunLaterDelegate
struct RunLaterDelegate_t87;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.String
struct String_t;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// System.Collections.Generic.List`1<Soomla.Store.MarketItem>
struct List_1_t177;

// System.Void Soomla.Store.StoreEvents::.ctor()
extern "C" void StoreEvents__ctor_m428 (StoreEvents_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::.cctor()
extern "C" void StoreEvents__cctor_m429 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::eventDispatcher_Init()
extern "C" void StoreEvents_eventDispatcher_Init_m430 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::Awake()
extern "C" void StoreEvents_Awake_m431 (StoreEvents_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::RunLater(Soomla.Store.StoreEvents/RunLaterDelegate)
extern "C" void StoreEvents_RunLater_m432 (StoreEvents_t90 * __this, RunLaterDelegate_t87 * ___runLaterDelegate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Soomla.Store.StoreEvents::RunLaterPriv(Soomla.Store.StoreEvents/RunLaterDelegate)
extern "C" Object_t * StoreEvents_RunLaterPriv_m433 (StoreEvents_t90 * __this, RunLaterDelegate_t87 * ___runLaterDelegate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::Initialize()
extern "C" void StoreEvents_Initialize_m434 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onBillingSupported(System.String)
extern "C" void StoreEvents_onBillingSupported_m435 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onBillingNotSupported(System.String)
extern "C" void StoreEvents_onBillingNotSupported_m436 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onCurrencyBalanceChanged(System.String)
extern "C" void StoreEvents_onCurrencyBalanceChanged_m437 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onCurrencyBalanceChanged(System.String,System.Boolean)
extern "C" void StoreEvents_onCurrencyBalanceChanged_m438 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodBalanceChanged(System.String)
extern "C" void StoreEvents_onGoodBalanceChanged_m439 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodBalanceChanged(System.String,System.Boolean)
extern "C" void StoreEvents_onGoodBalanceChanged_m440 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodEquipped(System.String)
extern "C" void StoreEvents_onGoodEquipped_m441 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodEquipped(System.String,System.Boolean)
extern "C" void StoreEvents_onGoodEquipped_m442 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodUnequipped(System.String)
extern "C" void StoreEvents_onGoodUnequipped_m443 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodUnequipped(System.String,System.Boolean)
extern "C" void StoreEvents_onGoodUnequipped_m444 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodUpgrade(System.String)
extern "C" void StoreEvents_onGoodUpgrade_m445 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onGoodUpgrade(System.String,System.Boolean)
extern "C" void StoreEvents_onGoodUpgrade_m446 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onItemPurchased(System.String)
extern "C" void StoreEvents_onItemPurchased_m447 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onItemPurchased(System.String,System.Boolean)
extern "C" void StoreEvents_onItemPurchased_m448 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onItemPurchaseStarted(System.String)
extern "C" void StoreEvents_onItemPurchaseStarted_m449 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onItemPurchaseStarted(System.String,System.Boolean)
extern "C" void StoreEvents_onItemPurchaseStarted_m450 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onMarketPurchaseCancelled(System.String)
extern "C" void StoreEvents_onMarketPurchaseCancelled_m451 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onMarketPurchase(System.String)
extern "C" void StoreEvents_onMarketPurchase_m452 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onMarketPurchaseStarted(System.String)
extern "C" void StoreEvents_onMarketPurchaseStarted_m453 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onMarketRefund(System.String)
extern "C" void StoreEvents_onMarketRefund_m454 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onRestoreTransactionsFinished(System.String)
extern "C" void StoreEvents_onRestoreTransactionsFinished_m455 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onRestoreTransactionsStarted(System.String)
extern "C" void StoreEvents_onRestoreTransactionsStarted_m456 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onMarketItemsRefreshStarted(System.String)
extern "C" void StoreEvents_onMarketItemsRefreshStarted_m457 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onMarketItemsRefreshFailed(System.String)
extern "C" void StoreEvents_onMarketItemsRefreshFailed_m458 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onMarketItemsRefreshFinished(System.String)
extern "C" void StoreEvents_onMarketItemsRefreshFinished_m459 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onUnexpectedErrorInStore(System.String)
extern "C" void StoreEvents_onUnexpectedErrorInStore_m460 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onUnexpectedErrorInStore(System.String,System.Boolean)
extern "C" void StoreEvents_onUnexpectedErrorInStore_m461 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onSoomlaStoreInitialized(System.String)
extern "C" void StoreEvents_onSoomlaStoreInitialized_m462 (StoreEvents_t90 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::onSoomlaStoreInitialized(System.String,System.Boolean)
extern "C" void StoreEvents_onSoomlaStoreInitialized_m463 (StoreEvents_t90 * __this, String_t* ___message, bool ___alsoPush, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnBillingNotSupported>m__D()
extern "C" void StoreEvents_U3COnBillingNotSupportedU3Em__D_m464 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnBillingSupported>m__E()
extern "C" void StoreEvents_U3COnBillingSupportedU3Em__E_m465 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnCurrencyBalanceChanged>m__F(Soomla.Store.VirtualCurrency,System.Int32,System.Int32)
extern "C" void StoreEvents_U3COnCurrencyBalanceChangedU3Em__F_m466 (Object_t * __this /* static, unused */, VirtualCurrency_t77 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnGoodBalanceChanged>m__10(Soomla.Store.VirtualGood,System.Int32,System.Int32)
extern "C" void StoreEvents_U3COnGoodBalanceChangedU3Em__10_m467 (Object_t * __this /* static, unused */, VirtualGood_t79 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnGoodEquipped>m__11(Soomla.Store.EquippableVG)
extern "C" void StoreEvents_U3COnGoodEquippedU3Em__11_m468 (Object_t * __this /* static, unused */, EquippableVG_t129 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnGoodUnEquipped>m__12(Soomla.Store.EquippableVG)
extern "C" void StoreEvents_U3COnGoodUnEquippedU3Em__12_m469 (Object_t * __this /* static, unused */, EquippableVG_t129 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnGoodUpgrade>m__13(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG)
extern "C" void StoreEvents_U3COnGoodUpgradeU3Em__13_m470 (Object_t * __this /* static, unused */, VirtualGood_t79 * p0, UpgradeVG_t133 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnItemPurchased>m__14(Soomla.Store.PurchasableVirtualItem,System.String)
extern "C" void StoreEvents_U3COnItemPurchasedU3Em__14_m471 (Object_t * __this /* static, unused */, PurchasableVirtualItem_t124 * p0, String_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnItemPurchaseStarted>m__15(Soomla.Store.PurchasableVirtualItem)
extern "C" void StoreEvents_U3COnItemPurchaseStartedU3Em__15_m472 (Object_t * __this /* static, unused */, PurchasableVirtualItem_t124 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnMarketPurchaseCancelled>m__16(Soomla.Store.PurchasableVirtualItem)
extern "C" void StoreEvents_U3COnMarketPurchaseCancelledU3Em__16_m473 (Object_t * __this /* static, unused */, PurchasableVirtualItem_t124 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnMarketPurchase>m__17(Soomla.Store.PurchasableVirtualItem,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" void StoreEvents_U3COnMarketPurchaseU3Em__17_m474 (Object_t * __this /* static, unused */, PurchasableVirtualItem_t124 * p0, String_t* p1, Dictionary_2_t165 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnMarketPurchaseStarted>m__18(Soomla.Store.PurchasableVirtualItem)
extern "C" void StoreEvents_U3COnMarketPurchaseStartedU3Em__18_m475 (Object_t * __this /* static, unused */, PurchasableVirtualItem_t124 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnMarketRefund>m__19(Soomla.Store.PurchasableVirtualItem)
extern "C" void StoreEvents_U3COnMarketRefundU3Em__19_m476 (Object_t * __this /* static, unused */, PurchasableVirtualItem_t124 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnRestoreTransactionsFinished>m__1A(System.Boolean)
extern "C" void StoreEvents_U3COnRestoreTransactionsFinishedU3Em__1A_m477 (Object_t * __this /* static, unused */, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnRestoreTransactionsStarted>m__1B()
extern "C" void StoreEvents_U3COnRestoreTransactionsStartedU3Em__1B_m478 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnMarketItemsRefreshStarted>m__1C()
extern "C" void StoreEvents_U3COnMarketItemsRefreshStartedU3Em__1C_m479 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnMarketItemsRefreshFailed>m__1D(System.String)
extern "C" void StoreEvents_U3COnMarketItemsRefreshFailedU3Em__1D_m480 (Object_t * __this /* static, unused */, String_t* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnMarketItemsRefreshFinished>m__1E(System.Collections.Generic.List`1<Soomla.Store.MarketItem>)
extern "C" void StoreEvents_U3COnMarketItemsRefreshFinishedU3Em__1E_m481 (Object_t * __this /* static, unused */, List_1_t177 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnUnexpectedErrorInStore>m__1F(System.String)
extern "C" void StoreEvents_U3COnUnexpectedErrorInStoreU3Em__1F_m482 (Object_t * __this /* static, unused */, String_t* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents::<OnSoomlaStoreInitialized>m__20()
extern "C" void StoreEvents_U3COnSoomlaStoreInitializedU3Em__20_m483 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
