﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.DateTime>
struct Nullable_1_t873;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Nullable`1<System.DateTime>
#include "mscorlib_System_Nullable_1_gen_0.h"

// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C" void Nullable_1__ctor_m3928_gshared (Nullable_1_t873 * __this, DateTime_t48  ___value, MethodInfo* method);
#define Nullable_1__ctor_m3928(__this, ___value, method) (( void (*) (Nullable_1_t873 *, DateTime_t48 , MethodInfo*))Nullable_1__ctor_m3928_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m19230_gshared (Nullable_1_t873 * __this, MethodInfo* method);
#define Nullable_1_get_HasValue_m19230(__this, method) (( bool (*) (Nullable_1_t873 *, MethodInfo*))Nullable_1_get_HasValue_m19230_gshared)(__this, method)
// T System.Nullable`1<System.DateTime>::get_Value()
extern "C" DateTime_t48  Nullable_1_get_Value_m19231_gshared (Nullable_1_t873 * __this, MethodInfo* method);
#define Nullable_1_get_Value_m19231(__this, method) (( DateTime_t48  (*) (Nullable_1_t873 *, MethodInfo*))Nullable_1_get_Value_m19231_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m19232_gshared (Nullable_1_t873 * __this, Object_t * ___other, MethodInfo* method);
#define Nullable_1_Equals_m19232(__this, ___other, method) (( bool (*) (Nullable_1_t873 *, Object_t *, MethodInfo*))Nullable_1_Equals_m19232_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m19233_gshared (Nullable_1_t873 * __this, Nullable_1_t873  ___other, MethodInfo* method);
#define Nullable_1_Equals_m19233(__this, ___other, method) (( bool (*) (Nullable_1_t873 *, Nullable_1_t873 , MethodInfo*))Nullable_1_Equals_m19233_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m19234_gshared (Nullable_1_t873 * __this, MethodInfo* method);
#define Nullable_1_GetHashCode_m19234(__this, method) (( int32_t (*) (Nullable_1_t873 *, MethodInfo*))Nullable_1_GetHashCode_m19234_gshared)(__this, method)
// System.String System.Nullable`1<System.DateTime>::ToString()
extern "C" String_t* Nullable_1_ToString_m19235_gshared (Nullable_1_t873 * __this, MethodInfo* method);
#define Nullable_1_ToString_m19235(__this, method) (( String_t* (*) (Nullable_1_t873 *, MethodInfo*))Nullable_1_ToString_m19235_gshared)(__this, method)
