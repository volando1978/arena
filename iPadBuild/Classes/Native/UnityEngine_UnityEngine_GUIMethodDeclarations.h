﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUI
struct GUI_t981;
// UnityEngine.GUISkin
struct GUISkin_t985;
// UnityEngine.Material
struct Material_t989;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.Texture
struct Texture_t736;
// UnityEngine.GUIContent
struct GUIContent_t986;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t1516;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleMode.h"

// System.Void UnityEngine.GUI::.cctor()
extern "C" void GUI__cctor_m6495 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
extern "C" void GUI_set_nextScrollStepTime_m6496 (Object_t * __this /* static, unused */, DateTime_t48  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern "C" void GUI_set_skin_m6497 (Object_t * __this /* static, unused */, GUISkin_t985 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C" GUISkin_t985 * GUI_get_skin_m4067 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_get_color_m6498 (Object_t * __this /* static, unused */, Color_t747 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_set_color_m6499 (Object_t * __this /* static, unused */, Color_t747 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUI::get_color()
extern "C" Color_t747  GUI_get_color_m6500 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern "C" void GUI_set_color_m4076 (Object_t * __this /* static, unused */, Color_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
extern "C" void GUI_set_changed_m6501 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.GUI::get_matrix()
extern "C" Matrix4x4_t997  GUI_get_matrix_m4241 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_matrix(UnityEngine.Matrix4x4)
extern "C" void GUI_set_matrix_m4245 (Object_t * __this /* static, unused */, Matrix4x4_t997  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
extern "C" void GUI_Label_m4104 (Object_t * __this /* static, unused */, Rect_t738  ___position, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" void GUI_Label_m4042 (Object_t * __this /* static, unused */, Rect_t738  ___position, String_t* ___text, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.GUIStyle)
extern "C" void GUI_Label_m4086 (Object_t * __this /* static, unused */, Rect_t738  ___position, Texture_t736 * ___image, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_Label_m6502 (Object_t * __this /* static, unused */, Rect_t738  ___position, GUIContent_t986 * ___content, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_DoLabel_m6503 (Object_t * __this /* static, unused */, Rect_t738  ___position, GUIContent_t986 * ___content, IntPtr_t ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m6504 (Object_t * __this /* static, unused */, Rect_t738 * ___position, GUIContent_t986 * ___content, IntPtr_t ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode)
extern "C" void GUI_DrawTexture_m4089 (Object_t * __this /* static, unused */, Rect_t738  ___position, Texture_t736 * ___image, int32_t ___scaleMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C" void GUI_DrawTexture_m4154 (Object_t * __this /* static, unused */, Rect_t738  ___position, Texture_t736 * ___image, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode,System.Boolean,System.Single)
extern "C" void GUI_DrawTexture_m6505 (Object_t * __this /* static, unused */, Rect_t738  ___position, Texture_t736 * ___image, int32_t ___scaleMode, bool ___alphaBlend, float ___imageAspect, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
extern "C" Material_t989 * GUI_get_blendMaterial_m6506 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
extern "C" Material_t989 * GUI_get_blitMaterial_m6507 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" void GUI_Box_m4105 (Object_t * __this /* static, unused */, Rect_t738  ___position, String_t* ___text, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.GUIStyle)
extern "C" void GUI_Box_m4155 (Object_t * __this /* static, unused */, Rect_t738  ___position, Texture_t736 * ___image, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_Box_m6508 (Object_t * __this /* static, unused */, Rect_t738  ___position, GUIContent_t986 * ___content, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" bool GUI_Button_m4090 (Object_t * __this /* static, unused */, Rect_t738  ___position, String_t* ___text, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_DoButton_m6509 (Object_t * __this /* static, unused */, Rect_t738  ___position, GUIContent_t986 * ___content, IntPtr_t ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m6510 (Object_t * __this /* static, unused */, Rect_t738 * ___position, GUIContent_t986 * ___content, IntPtr_t ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Toggle(UnityEngine.Rect,System.Boolean,UnityEngine.Texture,UnityEngine.GUIStyle)
extern "C" bool GUI_Toggle_m4153 (Object_t * __this /* static, unused */, Rect_t738  ___position, bool ___value, Texture_t736 * ___image, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Toggle(UnityEngine.Rect,System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" bool GUI_Toggle_m6511 (Object_t * __this /* static, unused */, Rect_t738  ___position, bool ___value, GUIContent_t986 * ___content, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoToggle(UnityEngine.Rect,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_DoToggle_m6512 (Object_t * __this /* static, unused */, Rect_t738  ___position, int32_t ___id, bool ___value, GUIContent_t986 * ___content, IntPtr_t ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoToggle_m6513 (Object_t * __this /* static, unused */, Rect_t738 * ___position, int32_t ___id, bool ___value, GUIContent_t986 * ___content, IntPtr_t ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect)
extern "C" void GUI_BeginGroup_m4066 (Object_t * __this /* static, unused */, Rect_t738  ___position, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_BeginGroup_m6514 (Object_t * __this /* static, unused */, Rect_t738  ___position, GUIContent_t986 * ___content, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::EndGroup()
extern "C" void GUI_EndGroup_m4087 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
extern "C" void GUI_CallWindowDelegate_m6515 (Object_t * __this /* static, unused */, WindowFunction_t1516 * ___func, int32_t ___id, GUISkin_t985 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t724 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
