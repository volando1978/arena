﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Quests.QuestUiResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestUiRes.h"
// GooglePlayGames.BasicApi.Quests.QuestUiResult
struct  QuestUiResult_t372 
{
	// System.Int32 GooglePlayGames.BasicApi.Quests.QuestUiResult::value__
	int32_t ___value___1;
};
