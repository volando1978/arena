﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>
struct Enumerator_t3604;
// System.Object
struct Object_t;
// System.String
struct String_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>
struct Dictionary_2_t110;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m18031(__this, ___dictionary, method) (( void (*) (Enumerator_t3604 *, Dictionary_2_t110 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18032(__this, method) (( Object_t * (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18033(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18034(__this, method) (( Object_t * (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18035(__this, method) (( Object_t * (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::MoveNext()
#define Enumerator_MoveNext_m18036(__this, method) (( bool (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::get_Current()
#define Enumerator_get_Current_m18037(__this, method) (( KeyValuePair_2_t3601  (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18038(__this, method) (( String_t* (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18039(__this, method) (( VirtualItem_t125 * (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::VerifyState()
#define Enumerator_VerifyState_m18040(__this, method) (( void (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18041(__this, method) (( void (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualItem>::Dispose()
#define Enumerator_Dispose_m18042(__this, method) (( void (*) (Enumerator_t3604 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
