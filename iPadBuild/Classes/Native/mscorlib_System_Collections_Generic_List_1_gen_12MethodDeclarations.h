﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>
struct List_1_t117;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.Collections.Generic.IEnumerable`1<Soomla.Store.VirtualCategory>
struct IEnumerable_1_t4346;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.VirtualCategory>
struct IEnumerator_1_t4337;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Store.VirtualCategory>
struct ICollection_1_t4347;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.VirtualCategory>
struct ReadOnlyCollection_1_t3628;
// Soomla.Store.VirtualCategory[]
struct VirtualCategoryU5BU5D_t175;
// System.Predicate`1<Soomla.Store.VirtualCategory>
struct Predicate_1_t3629;
// System.Action`1<Soomla.Store.VirtualCategory>
struct Action_1_t3630;
// System.Comparison`1<Soomla.Store.VirtualCategory>
struct Comparison_1_t3631;
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"

// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m1012(__this, method) (( void (*) (List_1_t117 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18424(__this, ___collection, method) (( void (*) (List_1_t117 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::.ctor(System.Int32)
#define List_1__ctor_m18425(__this, ___capacity, method) (( void (*) (List_1_t117 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::.cctor()
#define List_1__cctor_m18426(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18427(__this, method) (( Object_t* (*) (List_1_t117 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18428(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t117 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18429(__this, method) (( Object_t * (*) (List_1_t117 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18430(__this, ___item, method) (( int32_t (*) (List_1_t117 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18431(__this, ___item, method) (( bool (*) (List_1_t117 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18432(__this, ___item, method) (( int32_t (*) (List_1_t117 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18433(__this, ___index, ___item, method) (( void (*) (List_1_t117 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18434(__this, ___item, method) (( void (*) (List_1_t117 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18435(__this, method) (( bool (*) (List_1_t117 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18436(__this, method) (( bool (*) (List_1_t117 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18437(__this, method) (( Object_t * (*) (List_1_t117 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18438(__this, method) (( bool (*) (List_1_t117 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18439(__this, method) (( bool (*) (List_1_t117 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18440(__this, ___index, method) (( Object_t * (*) (List_1_t117 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18441(__this, ___index, ___value, method) (( void (*) (List_1_t117 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Add(T)
#define List_1_Add_m18442(__this, ___item, method) (( void (*) (List_1_t117 *, VirtualCategory_t126 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18443(__this, ___newCount, method) (( void (*) (List_1_t117 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18444(__this, ___collection, method) (( void (*) (List_1_t117 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18445(__this, ___enumerable, method) (( void (*) (List_1_t117 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18446(__this, ___collection, method) (( void (*) (List_1_t117 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::AsReadOnly()
#define List_1_AsReadOnly_m18447(__this, method) (( ReadOnlyCollection_1_t3628 * (*) (List_1_t117 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Clear()
#define List_1_Clear_m18448(__this, method) (( void (*) (List_1_t117 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Contains(T)
#define List_1_Contains_m18449(__this, ___item, method) (( bool (*) (List_1_t117 *, VirtualCategory_t126 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18450(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t117 *, VirtualCategoryU5BU5D_t175*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Find(System.Predicate`1<T>)
#define List_1_Find_m18451(__this, ___match, method) (( VirtualCategory_t126 * (*) (List_1_t117 *, Predicate_1_t3629 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18452(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3629 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m18453(__this, ___match, method) (( int32_t (*) (List_1_t117 *, Predicate_1_t3629 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18454(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t117 *, int32_t, int32_t, Predicate_1_t3629 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m18455(__this, ___action, method) (( void (*) (List_1_t117 *, Action_1_t3630 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::GetEnumerator()
#define List_1_GetEnumerator_m1022(__this, method) (( Enumerator_t228  (*) (List_1_t117 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::IndexOf(T)
#define List_1_IndexOf_m18456(__this, ___item, method) (( int32_t (*) (List_1_t117 *, VirtualCategory_t126 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18457(__this, ___start, ___delta, method) (( void (*) (List_1_t117 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18458(__this, ___index, method) (( void (*) (List_1_t117 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Insert(System.Int32,T)
#define List_1_Insert_m18459(__this, ___index, ___item, method) (( void (*) (List_1_t117 *, int32_t, VirtualCategory_t126 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18460(__this, ___collection, method) (( void (*) (List_1_t117 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Remove(T)
#define List_1_Remove_m18461(__this, ___item, method) (( bool (*) (List_1_t117 *, VirtualCategory_t126 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18462(__this, ___match, method) (( int32_t (*) (List_1_t117 *, Predicate_1_t3629 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18463(__this, ___index, method) (( void (*) (List_1_t117 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Reverse()
#define List_1_Reverse_m18464(__this, method) (( void (*) (List_1_t117 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Sort()
#define List_1_Sort_m18465(__this, method) (( void (*) (List_1_t117 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18466(__this, ___comparison, method) (( void (*) (List_1_t117 *, Comparison_1_t3631 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::ToArray()
#define List_1_ToArray_m18467(__this, method) (( VirtualCategoryU5BU5D_t175* (*) (List_1_t117 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::TrimExcess()
#define List_1_TrimExcess_m18468(__this, method) (( void (*) (List_1_t117 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::get_Capacity()
#define List_1_get_Capacity_m18469(__this, method) (( int32_t (*) (List_1_t117 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18470(__this, ___value, method) (( void (*) (List_1_t117 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::get_Count()
#define List_1_get_Count_m18471(__this, method) (( int32_t (*) (List_1_t117 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::get_Item(System.Int32)
#define List_1_get_Item_m18472(__this, ___index, method) (( VirtualCategory_t126 * (*) (List_1_t117 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>::set_Item(System.Int32,T)
#define List_1_set_Item_m18473(__this, ___index, ___value, method) (( void (*) (List_1_t117 *, int32_t, VirtualCategory_t126 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
