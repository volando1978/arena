﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>
struct KeyValuePair_2_t3525;
// System.String
struct String_t;
// Soomla.Reward
struct Reward_t55;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m16956(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3525 *, String_t*, Reward_t55 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>::get_Key()
#define KeyValuePair_2_get_Key_m16957(__this, method) (( String_t* (*) (KeyValuePair_2_t3525 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16958(__this, ___value, method) (( void (*) (KeyValuePair_2_t3525 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>::get_Value()
#define KeyValuePair_2_get_Value_m16959(__this, method) (( Reward_t55 * (*) (KeyValuePair_2_t3525 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16960(__this, ___value, method) (( void (*) (KeyValuePair_2_t3525 *, Reward_t55 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>::ToString()
#define KeyValuePair_2_ToString_m16961(__this, method) (( String_t* (*) (KeyValuePair_2_t3525 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
