﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration
struct IosPlatformConfiguration_t668;
// System.String
struct String_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::.ctor(System.IntPtr)
extern "C" void IosPlatformConfiguration__ctor_m2691 (IosPlatformConfiguration_t668 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::SetClientId(System.String)
extern "C" void IosPlatformConfiguration_SetClientId_m2692 (IosPlatformConfiguration_t668 * __this, String_t* ___clientId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void IosPlatformConfiguration_CallDispose_m2693 (IosPlatformConfiguration_t668 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::Create()
extern "C" IosPlatformConfiguration_t668 * IosPlatformConfiguration_Create_m2694 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
