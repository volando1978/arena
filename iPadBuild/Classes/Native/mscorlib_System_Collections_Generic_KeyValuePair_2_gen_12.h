﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>
struct  KeyValuePair_2_t3656 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::key
	Object_t * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::value
	uint32_t ___value_1;
};
