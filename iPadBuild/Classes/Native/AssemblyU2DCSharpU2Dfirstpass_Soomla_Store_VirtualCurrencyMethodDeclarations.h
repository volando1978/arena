﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.VirtualCurrency::.ctor(System.String,System.String,System.String)
extern "C" void VirtualCurrency__ctor_m603 (VirtualCurrency_t77 * __this, String_t* ___name, String_t* ___description, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualCurrency::.ctor(JSONObject)
extern "C" void VirtualCurrency__ctor_m604 (VirtualCurrency_t77 * __this, JSONObject_t30 * ___jsonVc, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.VirtualCurrency::toJSONObject()
extern "C" JSONObject_t30 * VirtualCurrency_toJSONObject_m605 (VirtualCurrency_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrency::Give(System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrency_Give_m606 (VirtualCurrency_t77 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrency::Take(System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrency_Take_m607 (VirtualCurrency_t77 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrency::ResetBalance(System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrency_ResetBalance_m608 (VirtualCurrency_t77 * __this, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrency::GetBalance()
extern "C" int32_t VirtualCurrency_GetBalance_m609 (VirtualCurrency_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
