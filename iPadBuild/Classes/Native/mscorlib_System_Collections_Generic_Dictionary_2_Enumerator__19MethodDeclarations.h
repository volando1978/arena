﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Enumerator_t3804;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t671;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18MethodDeclarations.h"
#define Enumerator__ctor_m20655(__this, ___dictionary, method) (( void (*) (Enumerator_t3804 *, Dictionary_2_t671 *, MethodInfo*))Enumerator__ctor_m20564_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20656(__this, method) (( Object_t * (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20565_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20657(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20566_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20658(__this, method) (( Object_t * (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20659(__this, method) (( Object_t * (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::MoveNext()
#define Enumerator_MoveNext_m20660(__this, method) (( bool (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_MoveNext_m20569_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Current()
#define Enumerator_get_Current_m20661(__this, method) (( KeyValuePair_2_t3801  (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_get_Current_m20570_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20662(__this, method) (( int32_t (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_get_CurrentKey_m20571_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20663(__this, method) (( int32_t (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_get_CurrentValue_m20572_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::VerifyState()
#define Enumerator_VerifyState_m20664(__this, method) (( void (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_VerifyState_m20573_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20665(__this, method) (( void (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_VerifyCurrent_m20574_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Dispose()
#define Enumerator_Dispose_m20666(__this, method) (( void (*) (Enumerator_t3804 *, MethodInfo*))Enumerator_Dispose_m20575_gshared)(__this, method)
