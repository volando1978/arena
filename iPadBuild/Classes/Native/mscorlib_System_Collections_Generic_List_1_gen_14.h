﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.Participant[]
struct ParticipantU5BU5D_t3647;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct  List_1_t351  : public Object_t
{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::_items
	ParticipantU5BU5D_t3647* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::_version
	int32_t ____version_3;
};
struct List_1_t351_StaticFields{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::EmptyArray
	ParticipantU5BU5D_t3647* ___EmptyArray_4;
};
