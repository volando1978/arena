﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RaycastScr
struct RaycastScr_t751;
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void RaycastScr::.ctor()
extern "C" void RaycastScr__ctor_m3246 (RaycastScr_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaycastScr::OnExplosion(UnityEngine.GameObject)
extern "C" void RaycastScr_OnExplosion_m3247 (RaycastScr_t751 * __this, GameObject_t144 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaycastScr::Update()
extern "C" void RaycastScr_Update_m3248 (RaycastScr_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaycastScr::leavePow(UnityEngine.Vector2)
extern "C" void RaycastScr_leavePow_m3249 (RaycastScr_t751 * __this, Vector2_t739  ___pos, MethodInfo* method) IL2CPP_METHOD_ATTR;
