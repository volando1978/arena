﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t4206;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Exception
struct Exception_t135;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m25839_gshared (ArrayReadOnlyList_1_t4206 * __this, ObjectU5BU5D_t208* ___array, MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m25839(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t4206 *, ObjectU5BU5D_t208*, MethodInfo*))ArrayReadOnlyList_1__ctor_m25839_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25840_gshared (ArrayReadOnlyList_1_t4206 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25840(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t4206 *, MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25840_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m25841_gshared (ArrayReadOnlyList_1_t4206 * __this, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m25841(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t4206 *, int32_t, MethodInfo*))ArrayReadOnlyList_1_get_Item_m25841_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m25842_gshared (ArrayReadOnlyList_1_t4206 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m25842(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t4206 *, int32_t, Object_t *, MethodInfo*))ArrayReadOnlyList_1_set_Item_m25842_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m25843_gshared (ArrayReadOnlyList_1_t4206 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m25843(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t4206 *, MethodInfo*))ArrayReadOnlyList_1_get_Count_m25843_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m25844_gshared (ArrayReadOnlyList_1_t4206 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m25844(__this, method) (( bool (*) (ArrayReadOnlyList_1_t4206 *, MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m25844_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m25845_gshared (ArrayReadOnlyList_1_t4206 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m25845(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t4206 *, Object_t *, MethodInfo*))ArrayReadOnlyList_1_Add_m25845_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m25846_gshared (ArrayReadOnlyList_1_t4206 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m25846(__this, method) (( void (*) (ArrayReadOnlyList_1_t4206 *, MethodInfo*))ArrayReadOnlyList_1_Clear_m25846_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m25847_gshared (ArrayReadOnlyList_1_t4206 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m25847(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t4206 *, Object_t *, MethodInfo*))ArrayReadOnlyList_1_Contains_m25847_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m25848_gshared (ArrayReadOnlyList_1_t4206 * __this, ObjectU5BU5D_t208* ___array, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m25848(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t4206 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))ArrayReadOnlyList_1_CopyTo_m25848_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m25849_gshared (ArrayReadOnlyList_1_t4206 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m25849(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t4206 *, MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m25849_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m25850_gshared (ArrayReadOnlyList_1_t4206 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m25850(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t4206 *, Object_t *, MethodInfo*))ArrayReadOnlyList_1_IndexOf_m25850_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m25851_gshared (ArrayReadOnlyList_1_t4206 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m25851(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t4206 *, int32_t, Object_t *, MethodInfo*))ArrayReadOnlyList_1_Insert_m25851_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m25852_gshared (ArrayReadOnlyList_1_t4206 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m25852(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t4206 *, Object_t *, MethodInfo*))ArrayReadOnlyList_1_Remove_m25852_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m25853_gshared (ArrayReadOnlyList_1_t4206 * __this, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m25853(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t4206 *, int32_t, MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m25853_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t135 * ArrayReadOnlyList_1_ReadOnlyError_m25854_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m25854(__this /* static, unused */, method) (( Exception_t135 * (*) (Object_t * /* static, unused */, MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m25854_gshared)(__this /* static, unused */, method)
