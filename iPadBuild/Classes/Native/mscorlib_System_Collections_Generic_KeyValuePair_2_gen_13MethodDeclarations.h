﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>
struct KeyValuePair_2_t3671;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12MethodDeclarations.h"
#define KeyValuePair_2__ctor_m19055(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3671 *, String_t*, uint32_t, MethodInfo*))KeyValuePair_2__ctor_m18957_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::get_Key()
#define KeyValuePair_2_get_Key_m19056(__this, method) (( String_t* (*) (KeyValuePair_2_t3671 *, MethodInfo*))KeyValuePair_2_get_Key_m18958_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m19057(__this, ___value, method) (( void (*) (KeyValuePair_2_t3671 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m18959_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::get_Value()
#define KeyValuePair_2_get_Value_m19058(__this, method) (( uint32_t (*) (KeyValuePair_2_t3671 *, MethodInfo*))KeyValuePair_2_get_Value_m18960_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m19059(__this, ___value, method) (( void (*) (KeyValuePair_2_t3671 *, uint32_t, MethodInfo*))KeyValuePair_2_set_Value_m18961_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::ToString()
#define KeyValuePair_2_ToString_m19060(__this, method) (( String_t* (*) (KeyValuePair_2_t3671 *, MethodInfo*))KeyValuePair_2_ToString_m18962_gshared)(__this, method)
