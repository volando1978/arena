﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.InternalHooks
struct InternalHooks_t424;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.Cwrapper.InternalHooks::InternalHooks_ConfigureForUnityPlugin(System.Runtime.InteropServices.HandleRef)
extern "C" void InternalHooks_InternalHooks_ConfigureForUnityPlugin_m1725 (Object_t * __this /* static, unused */, HandleRef_t657  ___builder, MethodInfo* method) IL2CPP_METHOD_ATTR;
