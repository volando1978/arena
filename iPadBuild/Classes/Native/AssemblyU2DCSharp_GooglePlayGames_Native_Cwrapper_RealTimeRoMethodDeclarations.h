﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeRoom
struct RealTimeRoom_t466;
// System.Text.StringBuilder
struct StringBuilder_t36;
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Real.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Status(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t RealTimeRoom_RealTimeRoom_Status_m2008 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  RealTimeRoom_RealTimeRoom_Description_m2009 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t RealTimeRoom_RealTimeRoom_Variant_m2010 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t RealTimeRoom_RealTimeRoom_CreationTime_m2011 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  RealTimeRoom_RealTimeRoom_Participants_Length_m2012 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t RealTimeRoom_RealTimeRoom_Participants_GetElement_m2013 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool RealTimeRoom_RealTimeRoom_Valid_m2014 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_RemainingAutomatchingSlots(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t RealTimeRoom_RealTimeRoom_RemainingAutomatchingSlots_m2015 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_AutomatchWaitEstimate(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t RealTimeRoom_RealTimeRoom_AutomatchWaitEstimate_m2016 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t RealTimeRoom_RealTimeRoom_CreatingParticipant_m2017 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  RealTimeRoom_RealTimeRoom_Id_m2018 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RealTimeRoom_RealTimeRoom_Dispose_m2019 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
