﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// enemyMov/enemyState
#include "AssemblyU2DCSharp_enemyMov_enemyState.h"
// enemyMov
struct  enemyMov_t790  : public MonoBehaviour_t26
{
	// System.Single enemyMov::maxSpeed
	float ___maxSpeed_2;
	// System.Single enemyMov::minSpeed
	float ___minSpeed_3;
	// System.Single enemyMov::speed
	float ___speed_4;
	// System.Single enemyMov::turningSpeedMax
	float ___turningSpeedMax_5;
	// System.Single enemyMov::turningSpeed
	float ___turningSpeed_6;
	// UnityEngine.Vector2 enemyMov::target
	Vector2_t739  ___target_7;
	// UnityEngine.Vector2 enemyMov::randomPos
	Vector2_t739  ___randomPos_8;
	// UnityEngine.Vector3 enemyMov::pos
	Vector3_t758  ___pos_9;
	// UnityEngine.Quaternion enemyMov::lookRotation
	Quaternion_t771  ___lookRotation_10;
	// UnityEngine.Vector2 enemyMov::direction
	Vector2_t739  ___direction_11;
	// System.Int32 enemyMov::timer
	int32_t ___timer_12;
	// enemyMov/enemyState enemyMov::optionState
	int32_t ___optionState_13;
	// UnityEngine.GameObject enemyMov::player
	GameObject_t144 * ___player_14;
};
