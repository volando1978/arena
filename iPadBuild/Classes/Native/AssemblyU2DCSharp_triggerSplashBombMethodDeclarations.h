﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// triggerSplashBomb
struct triggerSplashBomb_t836;

// System.Void triggerSplashBomb::.ctor()
extern "C" void triggerSplashBomb__ctor_m3654 (triggerSplashBomb_t836 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void triggerSplashBomb::triggerSplash()
extern "C" void triggerSplashBomb_triggerSplash_m3655 (triggerSplashBomb_t836 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void triggerSplashBomb::rayobomba()
extern "C" void triggerSplashBomb_rayobomba_m3656 (triggerSplashBomb_t836 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
