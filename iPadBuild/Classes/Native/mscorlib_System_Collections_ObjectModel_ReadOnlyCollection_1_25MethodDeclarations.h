﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>
struct ReadOnlyCollection_1_t3834;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.Matrix4x4>
struct IList_1_t3833;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t3830;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Matrix4x4>
struct IEnumerator_1_t4401;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m21092_gshared (ReadOnlyCollection_1_t3834 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m21092(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3834 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m21092_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21093_gshared (ReadOnlyCollection_1_t3834 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21093(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3834 *, Matrix4x4_t997 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21093_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21094_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21094(__this, method) (( void (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21094_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21095_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, Matrix4x4_t997  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21095(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3834 *, int32_t, Matrix4x4_t997 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21095_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21096_gshared (ReadOnlyCollection_1_t3834 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21096(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3834 *, Matrix4x4_t997 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21096_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21097_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21097(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3834 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21097_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Matrix4x4_t997  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21098_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21098(__this, ___index, method) (( Matrix4x4_t997  (*) (ReadOnlyCollection_1_t3834 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21098_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21099_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, Matrix4x4_t997  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21099(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3834 *, int32_t, Matrix4x4_t997 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21099_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21100_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21100(__this, method) (( bool (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21101_gshared (ReadOnlyCollection_1_t3834 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21101(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3834 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21101_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21102_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21102(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21102_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m21103_gshared (ReadOnlyCollection_1_t3834 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m21103(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3834 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m21103_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m21104_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m21104(__this, method) (( void (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m21104_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m21105_gshared (ReadOnlyCollection_1_t3834 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m21105(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3834 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m21105_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21106_gshared (ReadOnlyCollection_1_t3834 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21106(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3834 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21106_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m21107_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m21107(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3834 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m21107_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m21108_gshared (ReadOnlyCollection_1_t3834 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m21108(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3834 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m21108_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21109_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21109(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3834 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21109_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21110_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21110(__this, method) (( bool (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21110_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21111_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21111(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21111_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21112_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21112(__this, method) (( bool (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21112_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21113_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21113(__this, method) (( bool (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21113_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m21114_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m21114(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3834 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m21114_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m21115_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m21115(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3834 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m21115_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m21116_gshared (ReadOnlyCollection_1_t3834 * __this, Matrix4x4_t997  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m21116(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3834 *, Matrix4x4_t997 , MethodInfo*))ReadOnlyCollection_1_Contains_m21116_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m21117_gshared (ReadOnlyCollection_1_t3834 * __this, Matrix4x4U5BU5D_t3830* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m21117(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3834 *, Matrix4x4U5BU5D_t3830*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m21117_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m21118_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m21118(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m21118_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m21119_gshared (ReadOnlyCollection_1_t3834 * __this, Matrix4x4_t997  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m21119(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3834 *, Matrix4x4_t997 , MethodInfo*))ReadOnlyCollection_1_IndexOf_m21119_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m21120_gshared (ReadOnlyCollection_1_t3834 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m21120(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3834 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m21120_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>::get_Item(System.Int32)
extern "C" Matrix4x4_t997  ReadOnlyCollection_1_get_Item_m21121_gshared (ReadOnlyCollection_1_t3834 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m21121(__this, ___index, method) (( Matrix4x4_t997  (*) (ReadOnlyCollection_1_t3834 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m21121_gshared)(__this, ___index, method)
