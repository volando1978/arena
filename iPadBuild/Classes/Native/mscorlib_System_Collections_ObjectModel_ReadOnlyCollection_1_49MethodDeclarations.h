﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t4110;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t1399;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1670;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4517;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m24947_gshared (ReadOnlyCollection_1_t4110 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m24947(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t4110 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m24947_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24948_gshared (ReadOnlyCollection_1_t4110 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24948(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t4110 *, UILineInfo_t1398 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24948_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24949_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24949(__this, method) (( void (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24949_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24950_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, UILineInfo_t1398  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24950(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t4110 *, int32_t, UILineInfo_t1398 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24950_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24951_gshared (ReadOnlyCollection_1_t4110 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24951(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t4110 *, UILineInfo_t1398 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24951_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24952_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24952(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t4110 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24952_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UILineInfo_t1398  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24953_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24953(__this, ___index, method) (( UILineInfo_t1398  (*) (ReadOnlyCollection_1_t4110 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24953_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24954_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, UILineInfo_t1398  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24954(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t4110 *, int32_t, UILineInfo_t1398 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24954_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24955_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24955(__this, method) (( bool (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24955_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24956_gshared (ReadOnlyCollection_1_t4110 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24956(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t4110 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24956_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24957_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24957(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24957_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m24958_gshared (ReadOnlyCollection_1_t4110 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m24958(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t4110 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m24958_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m24959_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m24959(__this, method) (( void (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m24959_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m24960_gshared (ReadOnlyCollection_1_t4110 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m24960(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t4110 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m24960_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24961_gshared (ReadOnlyCollection_1_t4110 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24961(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t4110 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24961_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m24962_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m24962(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t4110 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m24962_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m24963_gshared (ReadOnlyCollection_1_t4110 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m24963(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t4110 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m24963_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24964_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24964(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t4110 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24964_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24965_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24965(__this, method) (( bool (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24965_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24966_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24966(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24966_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24967_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24967(__this, method) (( bool (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24967_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24968_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24968(__this, method) (( bool (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24968_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m24969_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m24969(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t4110 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m24969_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m24970_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m24970(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t4110 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m24970_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m24971_gshared (ReadOnlyCollection_1_t4110 * __this, UILineInfo_t1398  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m24971(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t4110 *, UILineInfo_t1398 , MethodInfo*))ReadOnlyCollection_1_Contains_m24971_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m24972_gshared (ReadOnlyCollection_1_t4110 * __this, UILineInfoU5BU5D_t1670* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m24972(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t4110 *, UILineInfoU5BU5D_t1670*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m24972_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m24973_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m24973(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m24973_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m24974_gshared (ReadOnlyCollection_1_t4110 * __this, UILineInfo_t1398  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m24974(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t4110 *, UILineInfo_t1398 , MethodInfo*))ReadOnlyCollection_1_IndexOf_m24974_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m24975_gshared (ReadOnlyCollection_1_t4110 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m24975(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t4110 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m24975_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t1398  ReadOnlyCollection_1_get_Item_m24976_gshared (ReadOnlyCollection_1_t4110 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m24976(__this, ___index, method) (( UILineInfo_t1398  (*) (ReadOnlyCollection_1_t4110 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m24976_gshared)(__this, ___index, method)
