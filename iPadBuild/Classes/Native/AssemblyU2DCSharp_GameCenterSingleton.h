﻿#pragma once
#include <stdint.h>
// GameCenterSingleton
struct GameCenterSingleton_t730;
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t732;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.Object
#include "mscorlib_System_Object.h"
// GameCenterSingleton
struct  GameCenterSingleton_t730  : public Object_t
{
	// UnityEngine.SocialPlatforms.IAchievement[] GameCenterSingleton::achievements
	IAchievementU5BU5D_t732* ___achievements_1;
};
struct GameCenterSingleton_t730_StaticFields{
	// GameCenterSingleton GameCenterSingleton::instance
	GameCenterSingleton_t730 * ___instance_0;
	// System.Action`1<System.Boolean> GameCenterSingleton::<>f__am$cache2
	Action_1_t98 * ___U3CU3Ef__amU24cache2_2;
};
