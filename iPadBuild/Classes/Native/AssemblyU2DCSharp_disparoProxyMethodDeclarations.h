﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// disparoProxy
struct disparoProxy_t786;

// System.Void disparoProxy::.ctor()
extern "C" void disparoProxy__ctor_m3375 (disparoProxy_t786 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void disparoProxy::Start()
extern "C" void disparoProxy_Start_m3376 (disparoProxy_t786 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void disparoProxy::dispara()
extern "C" void disparoProxy_dispara_m3377 (disparoProxy_t786 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
