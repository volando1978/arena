﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.RewardStorageIOS
struct RewardStorageIOS_t28;
// System.String
struct String_t;
// Soomla.SequenceReward
struct SequenceReward_t60;
// Soomla.Reward
struct Reward_t55;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Soomla.RewardStorageIOS::.ctor()
extern "C" void RewardStorageIOS__ctor_m51 (RewardStorageIOS_t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Soomla.RewardStorageIOS::rewardStorage_GetLastGivenTimeMillis(System.String)
extern "C" int64_t RewardStorageIOS_rewardStorage_GetLastGivenTimeMillis_m52 (Object_t * __this /* static, unused */, String_t* ___rewardId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorageIOS::rewardStorage_GetTimesGiven(System.String)
extern "C" int32_t RewardStorageIOS_rewardStorage_GetTimesGiven_m53 (Object_t * __this /* static, unused */, String_t* ___rewardId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorageIOS::rewardStorage_SetTimesGiven(System.String,System.Boolean,System.Boolean)
extern "C" void RewardStorageIOS_rewardStorage_SetTimesGiven_m54 (Object_t * __this /* static, unused */, String_t* ___rewardId, bool ___up, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorageIOS::rewardStorage_GetLastSeqIdxGiven(System.String)
extern "C" int32_t RewardStorageIOS_rewardStorage_GetLastSeqIdxGiven_m55 (Object_t * __this /* static, unused */, String_t* ___rewardId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorageIOS::rewardStorage_SetLastSeqIdxGiven(System.String,System.Int32)
extern "C" void RewardStorageIOS_rewardStorage_SetLastSeqIdxGiven_m56 (Object_t * __this /* static, unused */, String_t* ___rewardId, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorageIOS::_getLastSeqIdxGiven(Soomla.SequenceReward)
extern "C" int32_t RewardStorageIOS__getLastSeqIdxGiven_m57 (RewardStorageIOS_t28 * __this, SequenceReward_t60 * ___seqReward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorageIOS::_setLastSeqIdxGiven(Soomla.SequenceReward,System.Int32)
extern "C" void RewardStorageIOS__setLastSeqIdxGiven_m58 (RewardStorageIOS_t28 * __this, SequenceReward_t60 * ___seqReward, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorageIOS::_setTimesGiven(Soomla.Reward,System.Boolean,System.Boolean)
extern "C" void RewardStorageIOS__setTimesGiven_m59 (RewardStorageIOS_t28 * __this, Reward_t55 * ___reward, bool ___up, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorageIOS::_getTimesGiven(Soomla.Reward)
extern "C" int32_t RewardStorageIOS__getTimesGiven_m60 (RewardStorageIOS_t28 * __this, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Soomla.RewardStorageIOS::_getLastGivenTime(Soomla.Reward)
extern "C" DateTime_t48  RewardStorageIOS__getLastGivenTime_m61 (RewardStorageIOS_t28 * __this, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
