﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
struct  IL2CPPStructAlignmentAttribute_t1607  : public Attribute_t1546
{
	// System.Int32 UnityEngine.IL2CPPStructAlignmentAttribute::Align
	int32_t ___Align_0;
};
