﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit.h"
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
struct  InvType_t339 
{
	// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType::value__
	int32_t ___value___1;
};
