﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20
struct U3CIncrementAchievementU3Ec__AnonStorey20_t529;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
struct FetchResponse_t653;

// System.Void GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20::.ctor()
extern "C" void U3CIncrementAchievementU3Ec__AnonStorey20__ctor_m2247 (U3CIncrementAchievementU3Ec__AnonStorey20_t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20::<>m__18(GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse)
extern "C" void U3CIncrementAchievementU3Ec__AnonStorey20_U3CU3Em__18_m2248 (U3CIncrementAchievementU3Ec__AnonStorey20_t529 * __this, FetchResponse_t653 * ___rsp, MethodInfo* method) IL2CPP_METHOD_ATTR;
