﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
struct SequentialSearchPrimeGeneratorBase_t2199;
// Mono.Math.BigInteger
struct BigInteger_t2191;
// System.Object
struct Object_t;

// System.Void Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase::.ctor()
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m9064 (SequentialSearchPrimeGeneratorBase_t2199 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase::GenerateSearchBase(System.Int32,System.Object)
extern "C" BigInteger_t2191 * SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m9065 (SequentialSearchPrimeGeneratorBase_t2199 * __this, int32_t ___bits, Object_t * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase::GenerateNewPrime(System.Int32)
extern "C" BigInteger_t2191 * SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m9066 (SequentialSearchPrimeGeneratorBase_t2199 * __this, int32_t ___bits, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase::GenerateNewPrime(System.Int32,System.Object)
extern "C" BigInteger_t2191 * SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m9067 (SequentialSearchPrimeGeneratorBase_t2199 * __this, int32_t ___bits, Object_t * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase::IsPrimeAcceptable(Mono.Math.BigInteger,System.Object)
extern "C" bool SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m9068 (SequentialSearchPrimeGeneratorBase_t2199 * __this, BigInteger_t2191 * ___bi, Object_t * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
