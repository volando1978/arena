﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t3440;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3439;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15669_gshared (Enumerator_t3440 * __this, List_1_t3439 * ___l, MethodInfo* method);
#define Enumerator__ctor_m15669(__this, ___l, method) (( void (*) (Enumerator_t3440 *, List_1_t3439 *, MethodInfo*))Enumerator__ctor_m15669_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15670_gshared (Enumerator_t3440 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15670(__this, method) (( Object_t * (*) (Enumerator_t3440 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15670_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m15671_gshared (Enumerator_t3440 * __this, MethodInfo* method);
#define Enumerator_Dispose_m15671(__this, method) (( void (*) (Enumerator_t3440 *, MethodInfo*))Enumerator_Dispose_m15671_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern "C" void Enumerator_VerifyState_m15672_gshared (Enumerator_t3440 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m15672(__this, method) (( void (*) (Enumerator_t3440 *, MethodInfo*))Enumerator_VerifyState_m15672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15673_gshared (Enumerator_t3440 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m15673(__this, method) (( bool (*) (Enumerator_t3440 *, MethodInfo*))Enumerator_MoveNext_m15673_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t3407  Enumerator_get_Current_m15674_gshared (Enumerator_t3440 * __this, MethodInfo* method);
#define Enumerator_get_Current_m15674(__this, method) (( KeyValuePair_2_t3407  (*) (Enumerator_t3440 *, MethodInfo*))Enumerator_get_Current_m15674_gshared)(__this, method)
