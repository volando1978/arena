﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t2355;
// System.String
struct String_t;

// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m10734 (MonoTODOAttribute_t2355 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C" void MonoTODOAttribute__ctor_m10735 (MonoTODOAttribute_t2355 * __this, String_t* ___comment, MethodInfo* method) IL2CPP_METHOD_ATTR;
