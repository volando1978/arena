﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualGoodsStorageAndroid
struct VirtualGoodsStorageAndroid_t70;

// System.Void Soomla.Store.VirtualGoodsStorageAndroid::.ctor()
extern "C" void VirtualGoodsStorageAndroid__ctor_m258 (VirtualGoodsStorageAndroid_t70 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
