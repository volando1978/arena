﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ObjectDictionary/<SetKeyValuePair>c__AnonStorey7
struct U3CSetKeyValuePairU3Ec__AnonStorey7_t8;
// ObjectKvp
struct ObjectKvp_t6;

// System.Void ObjectDictionary/<SetKeyValuePair>c__AnonStorey7::.ctor()
extern "C" void U3CSetKeyValuePairU3Ec__AnonStorey7__ctor_m13 (U3CSetKeyValuePairU3Ec__AnonStorey7_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ObjectDictionary/<SetKeyValuePair>c__AnonStorey7::<>m__9(ObjectKvp)
extern "C" bool U3CSetKeyValuePairU3Ec__AnonStorey7_U3CU3Em__9_m14 (U3CSetKeyValuePairU3Ec__AnonStorey7_t8 * __this, ObjectKvp_t6 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
