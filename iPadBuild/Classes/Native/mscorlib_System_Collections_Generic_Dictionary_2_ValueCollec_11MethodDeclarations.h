﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t3489;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t920;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16310_gshared (Enumerator_t3489 * __this, Dictionary_2_t920 * ___host, MethodInfo* method);
#define Enumerator__ctor_m16310(__this, ___host, method) (( void (*) (Enumerator_t3489 *, Dictionary_2_t920 *, MethodInfo*))Enumerator__ctor_m16310_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared (Enumerator_t3489 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16311(__this, method) (( Object_t * (*) (Enumerator_t3489 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16312_gshared (Enumerator_t3489 * __this, MethodInfo* method);
#define Enumerator_Dispose_m16312(__this, method) (( void (*) (Enumerator_t3489 *, MethodInfo*))Enumerator_Dispose_m16312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16313_gshared (Enumerator_t3489 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m16313(__this, method) (( bool (*) (Enumerator_t3489 *, MethodInfo*))Enumerator_MoveNext_m16313_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16314_gshared (Enumerator_t3489 * __this, MethodInfo* method);
#define Enumerator_get_Current_m16314(__this, method) (( Object_t * (*) (Enumerator_t3489 *, MethodInfo*))Enumerator_get_Current_m16314_gshared)(__this, method)
