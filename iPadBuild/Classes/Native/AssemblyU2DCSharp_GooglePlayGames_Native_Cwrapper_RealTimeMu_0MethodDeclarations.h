﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback
struct LeaveRoomCallback_t459;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LeaveRoomCallback__ctor_m1959 (LeaveRoomCallback_t459 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus,System.IntPtr)
extern "C" void LeaveRoomCallback_Invoke_m1960 (LeaveRoomCallback_t459 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LeaveRoomCallback_t459(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * LeaveRoomCallback_BeginInvoke_m1961 (LeaveRoomCallback_t459 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::EndInvoke(System.IAsyncResult)
extern "C" void LeaveRoomCallback_EndInvoke_m1962 (LeaveRoomCallback_t459 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
