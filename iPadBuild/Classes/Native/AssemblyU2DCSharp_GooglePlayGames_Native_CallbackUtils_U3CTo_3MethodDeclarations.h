﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Object,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey18_3_t3710;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Object,System.Object,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19617_gshared (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3710 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19617(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3710 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19617_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Object,System.Object,System.Object>::<>m__9(T1,T2,T3)
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19618_gshared (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3710 * __this, Object_t * ___val1, Object_t * ___val2, Object_t * ___val3, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19618(__this, ___val1, ___val2, ___val3, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3710 *, Object_t *, Object_t *, Object_t *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19618_gshared)(__this, ___val1, ___val2, ___val3, method)
