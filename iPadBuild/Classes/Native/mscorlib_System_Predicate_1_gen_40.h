﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t1229;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Canvas>
struct  Predicate_1_t3979  : public MulticastDelegate_t22
{
};
