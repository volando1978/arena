﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback
struct FetchScoreSummaryCallback_t430;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchScoreSummaryCallback__ctor_m1748 (FetchScoreSummaryCallback_t430 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchScoreSummaryCallback_Invoke_m1749 (FetchScoreSummaryCallback_t430 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchScoreSummaryCallback_t430(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchScoreSummaryCallback_BeginInvoke_m1750 (FetchScoreSummaryCallback_t430 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchScoreSummaryCallback_EndInvoke_m1751 (FetchScoreSummaryCallback_t430 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
