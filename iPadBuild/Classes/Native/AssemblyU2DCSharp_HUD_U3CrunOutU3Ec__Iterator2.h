﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// HUD
struct HUD_t733;
// System.Object
#include "mscorlib_System_Object.h"
// HUD/<runOut>c__Iterator2
struct  U3CrunOutU3Ec__Iterator2_t735  : public Object_t
{
	// System.Int32 HUD/<runOut>c__Iterator2::$PC
	int32_t ___U24PC_0;
	// System.Object HUD/<runOut>c__Iterator2::$current
	Object_t * ___U24current_1;
	// HUD HUD/<runOut>c__Iterator2::<>f__this
	HUD_t733 * ___U3CU3Ef__this_2;
};
