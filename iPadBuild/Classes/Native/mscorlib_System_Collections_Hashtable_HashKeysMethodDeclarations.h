﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/HashKeys
struct HashKeys_t2440;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t1667;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void System.Collections.Hashtable/HashKeys::.ctor(System.Collections.Hashtable)
extern "C" void HashKeys__ctor_m11370 (HashKeys_t2440 * __this, Hashtable_t1667 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Hashtable/HashKeys::get_Count()
extern "C" int32_t HashKeys_get_Count_m11371 (HashKeys_t2440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Hashtable/HashKeys::get_IsSynchronized()
extern "C" bool HashKeys_get_IsSynchronized_m11372 (HashKeys_t2440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/HashKeys::get_SyncRoot()
extern "C" Object_t * HashKeys_get_SyncRoot_m11373 (HashKeys_t2440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/HashKeys::CopyTo(System.Array,System.Int32)
extern "C" void HashKeys_CopyTo_m11374 (HashKeys_t2440 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Hashtable/HashKeys::GetEnumerator()
extern "C" Object_t * HashKeys_GetEnumerator_m11375 (HashKeys_t2440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
