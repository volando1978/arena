﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.UIVertex>
struct Action_1_t3966;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Action`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m22924_gshared (Action_1_t3966 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m22924(__this, ___object, ___method, method) (( void (*) (Action_1_t3966 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m22924_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.UIVertex>::Invoke(T)
extern "C" void Action_1_Invoke_m22925_gshared (Action_1_t3966 * __this, UIVertex_t1265  ___obj, MethodInfo* method);
#define Action_1_Invoke_m22925(__this, ___obj, method) (( void (*) (Action_1_t3966 *, UIVertex_t1265 , MethodInfo*))Action_1_Invoke_m22925_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m22926_gshared (Action_1_t3966 * __this, UIVertex_t1265  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m22926(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t3966 *, UIVertex_t1265 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m22926_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m22927_gshared (Action_1_t3966 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m22927(__this, ___result, method) (( void (*) (Action_1_t3966 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m22927_gshared)(__this, ___result, method)
