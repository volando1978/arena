﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t563;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40
struct  U3CParticipantLeftU3Ec__AnonStorey40_t572  : public Object_t
{
	// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40::participant
	Participant_t340 * ___participant_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40::<>f__this
	OnGameThreadForwardingListener_t563 * ___U3CU3Ef__this_1;
};
