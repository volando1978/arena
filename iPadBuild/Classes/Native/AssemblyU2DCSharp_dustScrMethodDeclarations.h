﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// dustScr
struct dustScr_t787;

// System.Void dustScr::.ctor()
extern "C" void dustScr__ctor_m3378 (dustScr_t787 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dustScr::Start()
extern "C" void dustScr_Start_m3379 (dustScr_t787 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dustScr::Update()
extern "C" void dustScr_Update_m3380 (dustScr_t787 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
