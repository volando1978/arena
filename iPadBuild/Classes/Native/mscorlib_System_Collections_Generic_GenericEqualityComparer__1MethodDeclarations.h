﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct GenericEqualityComparer_1_t2918;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14428_gshared (GenericEqualityComparer_1_t2918 * __this, MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14428(__this, method) (( void (*) (GenericEqualityComparer_1_t2918 *, MethodInfo*))GenericEqualityComparer_1__ctor_m14428_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m26143_gshared (GenericEqualityComparer_1_t2918 * __this, Guid_t2814  ___obj, MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m26143(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2918 *, Guid_t2814 , MethodInfo*))GenericEqualityComparer_1_GetHashCode_m26143_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m26144_gshared (GenericEqualityComparer_1_t2918 * __this, Guid_t2814  ___x, Guid_t2814  ___y, MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m26144(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2918 *, Guid_t2814 , Guid_t2814 , MethodInfo*))GenericEqualityComparer_1_Equals_m26144_gshared)(__this, ___x, ___y, method)
