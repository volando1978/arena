﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.GameServicesBuilder
struct GameServicesBuilder_t667;
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback
struct AuthFinishedCallback_t665;
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback
struct AuthStartedCallback_t666;
// System.String
struct String_t;
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct Action_3_t871;
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct Action_3_t872;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// GooglePlayGames.Native.PInvoke.PlatformConfiguration
struct PlatformConfiguration_t669;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"

// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::.ctor(System.IntPtr)
extern "C" void GameServicesBuilder__ctor_m2678 (GameServicesBuilder_t667 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnAuthFinishedCallback(GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback)
extern "C" void GameServicesBuilder_SetOnAuthFinishedCallback_m2679 (GameServicesBuilder_t667 * __this, AuthFinishedCallback_t665 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::EnableSnapshots()
extern "C" void GameServicesBuilder_EnableSnapshots_m2680 (GameServicesBuilder_t667 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalAuthFinishedCallback(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr)
extern "C" void GameServicesBuilder_InternalAuthFinishedCallback_m2681 (Object_t * __this /* static, unused */, int32_t ___op, int32_t ___status, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnAuthStartedCallback(GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback)
extern "C" void GameServicesBuilder_SetOnAuthStartedCallback_m2682 (GameServicesBuilder_t667 * __this, AuthStartedCallback_t666 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalAuthStartedCallback(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr)
extern "C" void GameServicesBuilder_InternalAuthStartedCallback_m2683 (Object_t * __this /* static, unused */, int32_t ___op, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void GameServicesBuilder_CallDispose_m2684 (GameServicesBuilder_t667 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalOnTurnBasedMatchEventCallback(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
extern "C" void GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685 (Object_t * __this /* static, unused */, int32_t ___eventType, String_t* ___matchId, IntPtr_t ___match, IntPtr_t ___userData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnTurnBasedMatchEventCallback(System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>)
extern "C" void GameServicesBuilder_SetOnTurnBasedMatchEventCallback_m2686 (GameServicesBuilder_t667 * __this, Action_3_t871 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::InternalOnMultiplayerInvitationEventCallback(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
extern "C" void GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687 (Object_t * __this /* static, unused */, int32_t ___eventType, String_t* ___matchId, IntPtr_t ___match, IntPtr_t ___userData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder::SetOnMultiplayerInvitationEventCallback(System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>)
extern "C" void GameServicesBuilder_SetOnMultiplayerInvitationEventCallback_m2688 (GameServicesBuilder_t667 * __this, Action_3_t872 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.PInvoke.GameServicesBuilder::Build(GooglePlayGames.Native.PInvoke.PlatformConfiguration)
extern "C" GameServices_t534 * GameServicesBuilder_Build_m2689 (GameServicesBuilder_t667 * __this, PlatformConfiguration_t669 * ___configRef, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.GameServicesBuilder GooglePlayGames.Native.PInvoke.GameServicesBuilder::Create()
extern "C" GameServicesBuilder_t667 * GameServicesBuilder_Create_m2690 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
