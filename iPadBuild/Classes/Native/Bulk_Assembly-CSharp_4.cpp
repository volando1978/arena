﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// logoScene/<wait>c__Iterator10
#include "AssemblyU2DCSharp_logoScene_U3CwaitU3Ec__Iterator10.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// logoScene/<wait>c__Iterator10
#include "AssemblyU2DCSharp_logoScene_U3CwaitU3Ec__Iterator10MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// logoScene
#include "AssemblyU2DCSharp_logoScene.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void logoScene/<wait>c__Iterator10::.ctor()
extern "C" void U3CwaitU3Ec__Iterator10__ctor_m3555 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object logoScene/<wait>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object logoScene/<wait>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean logoScene/<wait>c__Iterator10::MoveNext()
extern TypeInfo* WaitForSeconds_t213_il2cpp_TypeInfo_var;
extern "C" bool U3CwaitU3Ec__Iterator10_MoveNext_m3558 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t213_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
	}
	{
		goto IL_0078;
	}

IL_0021:
	{
		logoScene_t812 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___fadeOut_3);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		logoScene_t812 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		L_4->___fadeIn_4 = 1;
	}

IL_003d:
	{
		WaitForSeconds_t213 * L_5 = (WaitForSeconds_t213 *)il2cpp_codegen_object_new (WaitForSeconds_t213_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m965(L_5, (0.8f), /*hidden argument*/NULL);
		__this->___U24current_1 = L_5;
		__this->___U24PC_0 = 1;
		goto IL_007a;
	}

IL_0059:
	{
		logoScene_t812 * L_6 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_6);
		L_6->___fadeIn_4 = 0;
		logoScene_t812 * L_7 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_7);
		L_7->___fadeOut_3 = 1;
		__this->___U24PC_0 = (-1);
	}

IL_0078:
	{
		return 0;
	}

IL_007a:
	{
		return 1;
	}
	// Dead block : IL_007c: ldloc.1
}
// System.Void logoScene/<wait>c__Iterator10::Dispose()
extern "C" void U3CwaitU3Ec__Iterator10_Dispose_m3559 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void logoScene/<wait>c__Iterator10::Reset()
extern TypeInfo* NotSupportedException_t192_il2cpp_TypeInfo_var;
extern "C" void U3CwaitU3Ec__Iterator10_Reset_m3560 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t192 * L_0 = (NotSupportedException_t192 *)il2cpp_codegen_object_new (NotSupportedException_t192_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m865(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// logoScene
#include "AssemblyU2DCSharp_logoSceneMethodDeclarations.h"

// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRendererMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
struct Component_t230;
struct SpriteRenderer_t744;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t230;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m4012_gshared (Component_t230 * __this, MethodInfo* method);
#define Component_GetComponent_TisObject_t_m4012(__this, method) (( Object_t * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t744_m4109(__this, method) (( SpriteRenderer_t744 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)


// System.Void logoScene::.ctor()
extern "C" void logoScene__ctor_m3561 (logoScene_t812 * __this, MethodInfo* method)
{
	{
		__this->___fadeTime_2 = (2.0f);
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void logoScene::Start()
extern "C" void logoScene_Start_m3562 (logoScene_t812 * __this, MethodInfo* method)
{
	{
		__this->___fadeTime_2 = (0.0f);
		MonoBehaviour_StartCoroutine_m4065(__this, (String_t*) &_stringLiteral735, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator logoScene::wait()
extern TypeInfo* U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo_var;
extern "C" Object_t * logoScene_wait_m3563 (logoScene_t812 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1047);
		s_Il2CppMethodIntialized = true;
	}
	U3CwaitU3Ec__Iterator10_t813 * V_0 = {0};
	{
		U3CwaitU3Ec__Iterator10_t813 * L_0 = (U3CwaitU3Ec__Iterator10_t813 *)il2cpp_codegen_object_new (U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo_var);
		U3CwaitU3Ec__Iterator10__ctor_m3555(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CwaitU3Ec__Iterator10_t813 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CwaitU3Ec__Iterator10_t813 * L_2 = V_0;
		return L_2;
	}
}
// System.Void logoScene::Update()
extern MethodInfo* Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var;
extern "C" void logoScene_Update_m3564 (logoScene_t812 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484280);
		s_Il2CppMethodIntialized = true;
	}
	Color_t747  V_0 = {0};
	{
		bool L_0 = (__this->___fadeIn_4);
		if (!L_0)
		{
			goto IL_0062;
		}
	}
	{
		float L_1 = (__this->___fadeTime_2);
		Color__ctor_m4141((&V_0), (1.0f), (1.0f), (1.0f), L_1, /*hidden argument*/NULL);
		SpriteRenderer_t744 * L_2 = Component_GetComponent_TisSpriteRenderer_t744_m4109(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var);
		Color_t747  L_3 = V_0;
		NullCheck(L_2);
		SpriteRenderer_set_color_m4213(L_2, L_3, /*hidden argument*/NULL);
		float L_4 = (__this->___fadeTime_2);
		float L_5 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___fadeTime_2 = ((float)((float)L_4+(float)((float)((float)(1.0f)*(float)L_5))));
		float L_6 = (__this->___fadeTime_2);
		if ((!(((float)L_6) > ((float)(40.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		__this->___fadeIn_4 = 0;
	}

IL_0062:
	{
		bool L_7 = (__this->___fadeOut_3);
		if (!L_7)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_8 = (__this->___levelName_5);
		Application_LoadLevel_m4258(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// menu
#include "AssemblyU2DCSharp_menu.h"
#ifndef _MSC_VER
#else
#endif
// menu
#include "AssemblyU2DCSharp_menuMethodDeclarations.h"

// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// globales
#include "AssemblyU2DCSharp_globales.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleMode.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// globales
#include "AssemblyU2DCSharp_globalesMethodDeclarations.h"
// SoundManager
#include "AssemblyU2DCSharp_SoundManagerMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"


// System.Void menu::.ctor()
extern TypeInfo* GUIStyle_t724_il2cpp_TypeInfo_var;
extern "C" void menu__ctor_m3565 (menu_t814 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle_t724 * L_0 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_0, /*hidden argument*/NULL);
		__this->___marqueeSt_3 = L_0;
		GUIStyle_t724 * L_1 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_1, /*hidden argument*/NULL);
		__this->___startSt_4 = L_1;
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void menu::Start()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern "C" void menu_Start_m3566 (menu_t814 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t724 * L_0 = (__this->___startSt_4);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_1 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		NullCheck(L_0);
		GUIStyle_set_fontSize_m4045(L_0, ((int32_t)((int32_t)(((int32_t)L_1))/(int32_t)((int32_t)48))), /*hidden argument*/NULL);
		GUIStyle_t724 * L_2 = (__this->___marqueeSt_3);
		float L_3 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		NullCheck(L_2);
		GUIStyle_set_fontSize_m4045(L_2, ((int32_t)((int32_t)(((int32_t)L_3))/(int32_t)((int32_t)24))), /*hidden argument*/NULL);
		SoundManager_playArenaWoman_m3284(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void menu::OnGUI()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t980_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t981_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t986_il2cpp_TypeInfo_var;
extern "C" void menu_OnGUI_m3567 (menu_t814 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		Mathf_t980_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(981);
		GUI_t981_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		GUIContent_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(995);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t739  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		globales_BeginGUI_m3520(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_0 = (__this->___v_9);
		float L_1 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___v_9 = ((float)((float)L_0+(float)((float)((float)(8.0f)*(float)L_1))));
		float L_2 = (__this->___v_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t980_il2cpp_TypeInfo_var);
		float L_3 = sinf(L_2);
		__this->___yU_10 = L_3;
		Color_t747  L_4 = Color_get_grey_m4236(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_set_color_m4076(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_5 = (__this->___yU_10);
		float L_6 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_7 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___x_1);
		float L_8 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_9 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___y_2);
		Rect_t738  L_10 = {0};
		Rect__ctor_m4041(&L_10, (0.0f), ((float)((float)(-60.0f)+(float)L_5)), ((float)((float)L_6*(float)L_7)), ((float)((float)L_8*(float)L_9)), /*hidden argument*/NULL);
		Texture_t736 * L_11 = (__this->___splash_11);
		GUI_DrawTexture_m4089(NULL /*static, unused*/, L_10, L_11, 2, /*hidden argument*/NULL);
		Color_t747  L_12 = Color_get_black_m4075(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_color_m4076(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		float L_13 = (__this->___yU_10);
		float L_14 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_15 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___x_1);
		float L_16 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_17 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___y_2);
		Rect_t738  L_18 = {0};
		Rect__ctor_m4041(&L_18, (0.0f), ((float)((float)(-80.0f)+(float)L_13)), ((float)((float)L_14*(float)L_15)), ((float)((float)L_16*(float)L_17)), /*hidden argument*/NULL);
		Texture_t736 * L_19 = (__this->___splash_11);
		GUI_DrawTexture_m4089(NULL /*static, unused*/, L_18, L_19, 2, /*hidden argument*/NULL);
		Color_t747  L_20 = Color_get_white_m4084(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_color_m4076(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		float L_21 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_22 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___x_1);
		float L_23 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_24 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___y_2);
		Rect_t738  L_25 = {0};
		Rect__ctor_m4041(&L_25, (0.0f), (-110.0f), ((float)((float)L_21*(float)L_22)), ((float)((float)L_23*(float)L_24)), /*hidden argument*/NULL);
		Texture_t736 * L_26 = (__this->___splash_11);
		GUI_DrawTexture_m4089(NULL /*static, unused*/, L_25, L_26, 2, /*hidden argument*/NULL);
		GUISkin_t985 * L_27 = GUI_get_skin_m4067(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		GUIStyle_t724 * L_28 = GUISkin_get_label_m4068(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t986_il2cpp_TypeInfo_var);
		GUIContent_t986 * L_29 = (GUIContent_t986 *)il2cpp_codegen_object_new (GUIContent_t986_il2cpp_TypeInfo_var);
		GUIContent__ctor_m4069(L_29, (String_t*) &_stringLiteral785, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector2_t739  L_30 = GUIStyle_CalcSize_m4070(L_28, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		int32_t L_31 = (__this->___t_7);
		__this->___t_7 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_32 = (__this->___t_7);
		if ((((int32_t)((int32_t)((int32_t)L_32%(int32_t)((int32_t)100)))) >= ((int32_t)((int32_t)50))))
		{
			goto IL_0244;
		}
	}
	{
		Color_t747  L_33 = Color_get_black_m4075(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_set_color_m4076(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_34 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_35 = ((&V_0)->___x_1);
		float L_36 = (__this->___offset_5);
		float L_37 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_38 = (__this->___offset_5);
		float L_39 = ((&V_0)->___x_1);
		float L_40 = ((&V_0)->___y_2);
		Rect_t738  L_41 = {0};
		Rect__ctor_m4041(&L_41, ((float)((float)((float)((float)((float)((float)L_34/(float)(2.0f)))-(float)((float)((float)L_35/(float)(2.0f)))))+(float)L_36)), ((float)((float)((float)((float)L_37/(float)(1.2f)))+(float)L_38)), L_39, L_40, /*hidden argument*/NULL);
		GUIStyle_t724 * L_42 = (__this->___startSt_4);
		GUI_Label_m4042(NULL /*static, unused*/, L_41, (String_t*) &_stringLiteral785, L_42, /*hidden argument*/NULL);
		Color_t747  L_43 = Color_get_white_m4084(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_color_m4076(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		float L_44 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_45 = ((&V_0)->___x_1);
		float L_46 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_47 = ((&V_0)->___x_1);
		float L_48 = ((&V_0)->___y_2);
		Rect_t738  L_49 = {0};
		Rect__ctor_m4041(&L_49, ((float)((float)((float)((float)L_44/(float)(2.0f)))-(float)((float)((float)L_45/(float)(2.0f))))), ((float)((float)L_46/(float)(1.2f))), L_47, L_48, /*hidden argument*/NULL);
		GUIStyle_t724 * L_50 = (__this->___startSt_4);
		GUI_Label_m4042(NULL /*static, unused*/, L_49, (String_t*) &_stringLiteral785, L_50, /*hidden argument*/NULL);
		Color_t747  L_51 = Color_get_white_m4084(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_color_m4076(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		float L_52 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_53 = ((&V_0)->___x_1);
		float L_54 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_55 = ((&V_0)->___x_1);
		float L_56 = ((&V_0)->___y_2);
		Rect_t738  L_57 = {0};
		Rect__ctor_m4041(&L_57, ((float)((float)((float)((float)L_52/(float)(2.0f)))-(float)((float)((float)L_53/(float)(2.0f))))), ((float)((float)L_54/(float)(1.2f))), L_55, L_56, /*hidden argument*/NULL);
		GUIStyle_t724 * L_58 = (__this->___startSt_4);
		GUI_Label_m4042(NULL /*static, unused*/, L_57, (String_t*) &_stringLiteral785, L_58, /*hidden argument*/NULL);
	}

IL_0244:
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		globales_EndGUI_m3521(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void menu::OnApplicationQuit()
extern "C" void menu_OnApplicationQuit_m3568 (menu_t814 * __this, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// movementProxy
#include "AssemblyU2DCSharp_movementProxy.h"
#ifndef _MSC_VER
#else
#endif
// movementProxy
#include "AssemblyU2DCSharp_movementProxyMethodDeclarations.h"

// enemyController
#include "AssemblyU2DCSharp_enemyController.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// enemyController
#include "AssemblyU2DCSharp_enemyControllerMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
struct GameObject_t144;
struct enemyController_t785;
struct GameObject_t144;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m4017_gshared (GameObject_t144 * __this, MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m4017(__this, method) (( Object_t * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<enemyController>()
// !!0 UnityEngine.GameObject::GetComponent<enemyController>()
#define GameObject_GetComponent_TisenemyController_t785_m4130(__this, method) (( enemyController_t785 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)


// System.Void movementProxy::.ctor()
extern "C" void movementProxy__ctor_m3569 (movementProxy_t815 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void movementProxy::rotateShipDirection()
extern MethodInfo* GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var;
extern "C" void movementProxy_rotateShipDirection_m3570 (movementProxy_t815 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484283);
		s_Il2CppMethodIntialized = true;
	}
	enemyController_t785 * V_0 = {0};
	GameObject_t144 * V_1 = {0};
	float V_2 = 0.0f;
	Vector3_t758  V_3 = {0};
	Vector3_t758  V_4 = {0};
	Vector3_t758  V_5 = {0};
	{
		GameObject_t144 * L_0 = (__this->___enemyController_2);
		NullCheck(L_0);
		enemyController_t785 * L_1 = GameObject_GetComponent_TisenemyController_t785_m4130(L_0, /*hidden argument*/GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var);
		V_0 = L_1;
		enemyController_t785 * L_2 = V_0;
		Transform_t809 * L_3 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t758  L_4 = Transform_get_position_m4034(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t144 * L_5 = enemyController_getClosest_m3395(L_2, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		GameObject_t144 * L_6 = V_1;
		bool L_7 = Object_op_Implicit_m1059(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t144 * L_8 = V_1;
		NullCheck(L_8);
		Transform_t809 * L_9 = GameObject_get_transform_m4029(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t758  L_10 = Transform_get_position_m4034(L_9, /*hidden argument*/NULL);
		Vector2_t739  L_11 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		float L_12 = movementProxy_calculateAngle_m3572(__this, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		Transform_t809 * L_13 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t758  L_14 = Transform_get_position_m4034(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = ((&V_4)->___x_1);
		Transform_t809 * L_16 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t758  L_17 = Transform_get_position_m4034(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		float L_18 = ((&V_5)->___y_2);
		float L_19 = V_2;
		Vector3__ctor_m4026((&V_3), L_15, L_18, ((float)((float)(57.29578f)*(float)L_19)), /*hidden argument*/NULL);
		Transform_t809 * L_20 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_21 = V_3;
		Quaternion_t771  L_22 = Quaternion_Euler_m4157(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_rotation_m4171(L_20, L_22, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Void movementProxy::rotateInactive()
extern "C" void movementProxy_rotateInactive_m3571 (movementProxy_t815 * __this, MethodInfo* method)
{
	{
		Transform_t809 * L_0 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_1 = Vector3_get_forward_m4179(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m4259(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single movementProxy::calculateAngle(UnityEngine.Vector2)
extern TypeInfo* Mathf_t980_il2cpp_TypeInfo_var;
extern "C" float movementProxy_calculateAngle_m3572 (movementProxy_t815 * __this, Vector2_t739  ___target, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t980_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(981);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t758  V_5 = {0};
	Vector3_t758  V_6 = {0};
	{
		Transform_t809 * L_0 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t758  L_1 = Transform_get_position_m4034(L_0, /*hidden argument*/NULL);
		V_5 = L_1;
		float L_2 = ((&V_5)->___y_2);
		V_0 = L_2;
		Transform_t809 * L_3 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t758  L_4 = Transform_get_position_m4034(L_3, /*hidden argument*/NULL);
		V_6 = L_4;
		float L_5 = ((&V_6)->___x_1);
		V_1 = L_5;
		float L_6 = ((&___target)->___x_1);
		V_2 = L_6;
		float L_7 = ((&___target)->___y_2);
		V_3 = L_7;
		float L_8 = V_3;
		float L_9 = V_0;
		float L_10 = V_2;
		float L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t980_il2cpp_TypeInfo_var);
		float L_12 = atan2f(((float)((float)L_8-(float)L_9)), ((float)((float)L_10-(float)L_11)));
		V_4 = L_12;
		float L_13 = V_4;
		return L_13;
	}
}
// movingTextFeedback
#include "AssemblyU2DCSharp_movingTextFeedback.h"
#ifndef _MSC_VER
#else
#endif
// movingTextFeedback
#include "AssemblyU2DCSharp_movingTextFeedbackMethodDeclarations.h"



// System.Void movingTextFeedback::.ctor()
extern "C" void movingTextFeedback__ctor_m3573 (movingTextFeedback_t816 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void movingTextFeedback::Start()
extern "C" void movingTextFeedback_Start_m3574 (movingTextFeedback_t816 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void movingTextFeedback::Update()
extern "C" void movingTextFeedback_Update_m3575 (movingTextFeedback_t816 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void movingTextFeedback::OnGUI()
extern "C" void movingTextFeedback_OnGUI_m3576 (movingTextFeedback_t816 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// movingTextKillsScr
#include "AssemblyU2DCSharp_movingTextKillsScr.h"
#ifndef _MSC_VER
#else
#endif
// movingTextKillsScr
#include "AssemblyU2DCSharp_movingTextKillsScrMethodDeclarations.h"

// HUD
#include "AssemblyU2DCSharp_HUD.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// HUD
#include "AssemblyU2DCSharp_HUDMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"


// System.Void movingTextKillsScr::.ctor()
extern TypeInfo* GUIStyle_t724_il2cpp_TypeInfo_var;
extern "C" void movingTextKillsScr__ctor_m3577 (movingTextKillsScr_t817 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle_t724 * L_0 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_0, /*hidden argument*/NULL);
		__this->___st_2 = L_0;
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void movingTextKillsScr::Start()
extern "C" void movingTextKillsScr_Start_m3578 (movingTextKillsScr_t817 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void movingTextKillsScr::Update()
extern TypeInfo* HUD_t733_il2cpp_TypeInfo_var;
extern "C" void movingTextKillsScr_Update_m3579 (movingTextKillsScr_t817 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HUD_t733_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t758  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(HUD_t733_il2cpp_TypeInfo_var);
		float L_0 = ((&((HUD_t733_StaticFields*)HUD_t733_il2cpp_TypeInfo_var->static_fields)->___killsHUDPos_19)->___x_1);
		float L_1 = ((&((HUD_t733_StaticFields*)HUD_t733_il2cpp_TypeInfo_var->static_fields)->___killsHUDPos_19)->___y_2);
		Vector3__ctor_m4026((&V_0), L_0, L_1, (0.0f), /*hidden argument*/NULL);
		Transform_t809 * L_2 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Transform_t809 * L_3 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t758  L_4 = Transform_get_position_m4034(L_3, /*hidden argument*/NULL);
		Vector3_t758  L_5 = V_0;
		float L_6 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t758  L_7 = Vector3_MoveTowards_m4169(NULL /*static, unused*/, L_4, L_5, ((float)((float)L_6*(float)(20.0f))), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m4039(L_2, L_7, /*hidden argument*/NULL);
		Transform_t809 * L_8 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t758  L_9 = Transform_get_position_m4034(L_8, /*hidden argument*/NULL);
		Vector3_t758  L_10 = V_0;
		bool L_11 = Vector3_op_Equality_m4260(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0068;
		}
	}
	{
		GameObject_t144 * L_12 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void movingTextKillsScr::OnGUI()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t981_il2cpp_TypeInfo_var;
extern "C" void movingTextKillsScr_OnGUI_m3580 (movingTextKillsScr_t817 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		GUI_t981_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t739  V_0 = {0};
	{
		Camera_t978 * L_0 = Camera_get_main_m4021(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t809 * L_1 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t758  L_2 = Transform_get_position_m4034(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t758  L_3 = Camera_WorldToScreenPoint_m4040(L_0, L_2, /*hidden argument*/NULL);
		Vector2_t739  L_4 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_5 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_6 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = ((float)((float)L_5-(float)L_6));
		float L_7 = ((&V_0)->___x_1);
		float L_8 = ((&V_0)->___y_2);
		Rect_t738  L_9 = {0};
		Rect__ctor_m4041(&L_9, L_7, L_8, (100.0f), (30.0f), /*hidden argument*/NULL);
		String_t* L_10 = Int32_ToString_m4081((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___kills_4), /*hidden argument*/NULL);
		GUIStyle_t724 * L_11 = (__this->___st_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_Label_m4042(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		GUIStyle_t724 * L_12 = (__this->___st_2);
		float L_13 = ((&V_0)->___y_2);
		NullCheck(L_12);
		GUIStyle_set_fontSize_m4045(L_12, ((int32_t)((int32_t)(((int32_t)L_13))/(int32_t)6)), /*hidden argument*/NULL);
		return;
	}
}
// movingTextScr
#include "AssemblyU2DCSharp_movingTextScr.h"
#ifndef _MSC_VER
#else
#endif
// movingTextScr
#include "AssemblyU2DCSharp_movingTextScrMethodDeclarations.h"

// WeaponsController
#include "AssemblyU2DCSharpU2Dfirstpass_WeaponsController.h"
#include "mscorlib_ArrayTypes.h"
// WeaponsController
#include "AssemblyU2DCSharpU2Dfirstpass_WeaponsControllerMethodDeclarations.h"


// System.Void movingTextScr::.ctor()
extern TypeInfo* GUIStyle_t724_il2cpp_TypeInfo_var;
extern "C" void movingTextScr__ctor_m3581 (movingTextScr_t818 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle_t724 * L_0 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_0, /*hidden argument*/NULL);
		__this->___st_2 = L_0;
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void movingTextScr::Update()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern "C" void movingTextScr_Update_m3582 (movingTextScr_t818 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t739  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_0 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_1 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		Vector2__ctor_m4033((&V_0), ((float)((float)L_0-(float)(30.0f))), ((float)((float)L_1-(float)(30.0f))), /*hidden argument*/NULL);
		Transform_t809 * L_2 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Transform_t809 * L_3 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t758  L_4 = Transform_get_position_m4034(L_3, /*hidden argument*/NULL);
		Vector2_t739  L_5 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector2_t739  L_6 = V_0;
		float L_7 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t739  L_8 = Vector2_MoveTowards_m4037(NULL /*static, unused*/, L_5, L_6, ((float)((float)L_7*(float)(20.0f))), /*hidden argument*/NULL);
		Vector3_t758  L_9 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m4039(L_2, L_9, /*hidden argument*/NULL);
		Transform_t809 * L_10 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t758  L_11 = Transform_get_position_m4034(L_10, /*hidden argument*/NULL);
		Vector2_t739  L_12 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Vector2_t739  L_13 = V_0;
		bool L_14 = Vector2_op_Equality_m4198(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0074;
		}
	}
	{
		GameObject_t144 * L_15 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void movingTextScr::OnGUI()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* WeaponsController_t109_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t981_il2cpp_TypeInfo_var;
extern "C" void movingTextScr_OnGUI_m3583 (movingTextScr_t818 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		WeaponsController_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		GUI_t981_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t739  V_0 = {0};
	{
		Camera_t978 * L_0 = Camera_get_main_m4021(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t809 * L_1 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t758  L_2 = Transform_get_position_m4034(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t758  L_3 = Camera_WorldToScreenPoint_m4040(L_0, L_2, /*hidden argument*/NULL);
		Vector2_t739  L_4 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_5 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_6 = ((&V_0)->___x_1);
		(&V_0)->___x_1 = ((float)((float)L_5-(float)L_6));
		float L_7 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_8 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = ((float)((float)L_7-(float)L_8));
		float L_9 = ((&V_0)->___x_1);
		float L_10 = ((&V_0)->___y_2);
		Rect_t738  L_11 = {0};
		Rect__ctor_m4041(&L_11, L_9, L_10, (100.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		Int32U5BU5D_t107* L_12 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___bullets_2;
		int32_t L_13 = (__this->___bulletType_3);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		String_t* L_14 = Int32_ToString_m4081(((int32_t*)(int32_t*)SZArrayLdElema(L_12, L_13)), /*hidden argument*/NULL);
		GUIStyle_t724 * L_15 = (__this->___st_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_Label_m4042(NULL /*static, unused*/, L_11, L_14, L_15, /*hidden argument*/NULL);
		GUIStyle_t724 * L_16 = (__this->___st_2);
		float L_17 = ((&V_0)->___y_2);
		NullCheck(L_16);
		GUIStyle_set_fontSize_m4045(L_16, ((int32_t)((int32_t)(((int32_t)L_17))/(int32_t)6)), /*hidden argument*/NULL);
		return;
	}
}
// musicToggle
#include "AssemblyU2DCSharp_musicToggle.h"
#ifndef _MSC_VER
#else
#endif
// musicToggle
#include "AssemblyU2DCSharp_musicToggleMethodDeclarations.h"

// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// UnityEngine.Events.UnityAction`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_genMethodDeclarations.h"
// UnityEngine.Events.UnityEvent`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_genMethodDeclarations.h"
struct Component_t230;
struct Toggle_t720;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Toggle>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Toggle>()
#define Component_GetComponent_TisToggle_t720_m4011(__this, method) (( Toggle_t720 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)


// System.Void musicToggle::.ctor()
extern "C" void musicToggle__ctor_m3584 (musicToggle_t819 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void musicToggle::Start()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* musicToggle_t819_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_1_t721_il2cpp_TypeInfo_var;
extern MethodInfo* Component_GetComponent_TisToggle_t720_m4011_MethodInfo_var;
extern MethodInfo* musicToggle_U3CStartU3Em__A6_m3586_MethodInfo_var;
extern MethodInfo* UnityAction_1__ctor_m4014_MethodInfo_var;
extern MethodInfo* UnityEvent_1_AddListener_m4015_MethodInfo_var;
extern "C" void musicToggle_Start_m3585 (musicToggle_t819 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		musicToggle_t819_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1048);
		UnityAction_1_t721_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		Component_GetComponent_TisToggle_t720_m4011_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484265);
		musicToggle_U3CStartU3Em__A6_m3586_MethodInfo_var = il2cpp_codegen_method_info_from_index(667);
		UnityAction_1__ctor_m4014_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484267);
		UnityEvent_1_AddListener_m4015_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484268);
		s_Il2CppMethodIntialized = true;
	}
	ToggleEvent_t975 * G_B2_0 = {0};
	ToggleEvent_t975 * G_B1_0 = {0};
	{
		Toggle_t720 * L_0 = Component_GetComponent_TisToggle_t720_m4011(__this, /*hidden argument*/Component_GetComponent_TisToggle_t720_m4011_MethodInfo_var);
		__this->___tog_2 = L_0;
		Toggle_t720 * L_1 = (__this->___tog_2);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		bool L_2 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___musicSwitch_38;
		NullCheck(L_1);
		Toggle_set_isOn_m4013(L_1, L_2, /*hidden argument*/NULL);
		Toggle_t720 * L_3 = (__this->___tog_2);
		NullCheck(L_3);
		ToggleEvent_t975 * L_4 = (L_3->___onValueChanged_19);
		UnityAction_1_t721 * L_5 = ((musicToggle_t819_StaticFields*)musicToggle_t819_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_3;
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_003f;
		}
	}
	{
		IntPtr_t L_6 = { musicToggle_U3CStartU3Em__A6_m3586_MethodInfo_var };
		UnityAction_1_t721 * L_7 = (UnityAction_1_t721 *)il2cpp_codegen_object_new (UnityAction_1_t721_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4014(L_7, NULL, L_6, /*hidden argument*/UnityAction_1__ctor_m4014_MethodInfo_var);
		((musicToggle_t819_StaticFields*)musicToggle_t819_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_3 = L_7;
		G_B2_0 = G_B1_0;
	}

IL_003f:
	{
		UnityAction_1_t721 * L_8 = ((musicToggle_t819_StaticFields*)musicToggle_t819_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_3;
		NullCheck(G_B2_0);
		UnityEvent_1_AddListener_m4015(G_B2_0, L_8, /*hidden argument*/UnityEvent_1_AddListener_m4015_MethodInfo_var);
		return;
	}
}
// System.Void musicToggle::<Start>m__A6(System.Boolean)
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern "C" void musicToggle_U3CStartU3Em__A6_m3586 (Object_t * __this /* static, unused */, bool p0, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		globales_musicToggle_m3540(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// naveProxyController
#include "AssemblyU2DCSharp_naveProxyController.h"
#ifndef _MSC_VER
#else
#endif
// naveProxyController
#include "AssemblyU2DCSharp_naveProxyControllerMethodDeclarations.h"

// disparoProxy
#include "AssemblyU2DCSharp_disparoProxy.h"
// gizmosProxy
#include "AssemblyU2DCSharp_gizmosProxy.h"
// UnityEngine.Animation
#include "UnityEngine_UnityEngine_Animation.h"
// randomColorScr
#include "AssemblyU2DCSharp_randomColorScr.h"
// collideAndDie
#include "AssemblyU2DCSharp_collideAndDie.h"
// paintBlue
#include "AssemblyU2DCSharp_paintBlue.h"
// disparoProxy
#include "AssemblyU2DCSharp_disparoProxyMethodDeclarations.h"
// gizmosProxy
#include "AssemblyU2DCSharp_gizmosProxyMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Animation
#include "UnityEngine_UnityEngine_AnimationMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
struct GameObject_t144;
struct movementProxy_t815;
// Declaration !!0 UnityEngine.GameObject::GetComponent<movementProxy>()
// !!0 UnityEngine.GameObject::GetComponent<movementProxy>()
#define GameObject_GetComponent_TismovementProxy_t815_m4261(__this, method) (( movementProxy_t815 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)
struct GameObject_t144;
struct disparoProxy_t786;
// Declaration !!0 UnityEngine.GameObject::GetComponent<disparoProxy>()
// !!0 UnityEngine.GameObject::GetComponent<disparoProxy>()
#define GameObject_GetComponent_TisdisparoProxy_t786_m4262(__this, method) (( disparoProxy_t786 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)
struct GameObject_t144;
struct gizmosProxy_t804;
// Declaration !!0 UnityEngine.GameObject::GetComponent<gizmosProxy>()
// !!0 UnityEngine.GameObject::GetComponent<gizmosProxy>()
#define GameObject_GetComponent_TisgizmosProxy_t804_m4263(__this, method) (( gizmosProxy_t804 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)
struct Component_t230;
struct Animation_t982;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t982_m4264(__this, method) (( Animation_t982 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)
struct Component_t230;
struct randomColorScr_t832;
// Declaration !!0 UnityEngine.Component::GetComponent<randomColorScr>()
// !!0 UnityEngine.Component::GetComponent<randomColorScr>()
#define Component_GetComponent_TisrandomColorScr_t832_m4265(__this, method) (( randomColorScr_t832 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)
struct Component_t230;
struct collideAndDie_t779;
// Declaration !!0 UnityEngine.Component::GetComponent<collideAndDie>()
// !!0 UnityEngine.Component::GetComponent<collideAndDie>()
#define Component_GetComponent_TiscollideAndDie_t779_m4266(__this, method) (( collideAndDie_t779 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)
struct Component_t230;
struct paintBlue_t827;
// Declaration !!0 UnityEngine.Component::GetComponent<paintBlue>()
// !!0 UnityEngine.Component::GetComponent<paintBlue>()
#define Component_GetComponent_TispaintBlue_t827_m4267(__this, method) (( paintBlue_t827 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)


// System.Void naveProxyController::.ctor()
extern "C" void naveProxyController__ctor_m3587 (naveProxyController_t820 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void naveProxyController::.cctor()
extern "C" void naveProxyController__cctor_m3588 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void naveProxyController::Start()
extern "C" void naveProxyController_Start_m3589 (naveProxyController_t820 * __this, MethodInfo* method)
{
	{
		__this->___working_2 = 0;
		return;
	}
}
// System.Void naveProxyController::Update()
extern TypeInfo* naveProxyController_t820_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_GetComponent_TismovementProxy_t815_m4261_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TisdisparoProxy_t786_m4262_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var;
extern "C" void naveProxyController_Update_m3590 (naveProxyController_t820 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		naveProxyController_t820_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		GameObject_GetComponent_TismovementProxy_t815_m4261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484316);
		GameObject_GetComponent_TisdisparoProxy_t786_m4262_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484317);
		GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484318);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___working_2);
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		GameObject_t144 * L_1 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		movementProxy_t815 * L_2 = GameObject_GetComponent_TismovementProxy_t815_m4261(L_1, /*hidden argument*/GameObject_GetComponent_TismovementProxy_t815_m4261_MethodInfo_var);
		NullCheck(L_2);
		movementProxy_rotateShipDirection_m3570(L_2, /*hidden argument*/NULL);
		GameObject_t144 * L_3 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		disparoProxy_t786 * L_4 = GameObject_GetComponent_TisdisparoProxy_t786_m4262(L_3, /*hidden argument*/GameObject_GetComponent_TisdisparoProxy_t786_m4262_MethodInfo_var);
		NullCheck(L_4);
		disparoProxy_dispara_m3377(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(naveProxyController_t820_il2cpp_TypeInfo_var);
		bool L_5 = ((naveProxyController_t820_StaticFields*)naveProxyController_t820_il2cpp_TypeInfo_var->static_fields)->___gizmosReady_3;
		if (L_5)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t144 * L_6 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		gizmosProxy_t804 * L_7 = GameObject_GetComponent_TisgizmosProxy_t804_m4263(L_6, /*hidden argument*/GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var);
		NullCheck(L_7);
		gizmosProxy_createGizmos_m3510(L_7, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_005a;
	}

IL_004a:
	{
		GameObject_t144 * L_8 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		movementProxy_t815 * L_9 = GameObject_GetComponent_TismovementProxy_t815_m4261(L_8, /*hidden argument*/GameObject_GetComponent_TismovementProxy_t815_m4261_MethodInfo_var);
		NullCheck(L_9);
		movementProxy_rotateInactive_m3571(L_9, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Boolean naveProxyController::isWorking()
extern "C" bool naveProxyController_isWorking_m3591 (naveProxyController_t820 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___working_2);
		return L_0;
	}
}
// System.Void naveProxyController::setWorking()
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern MethodInfo* Component_GetComponent_TisAnimation_t982_m4264_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TisdisparoProxy_t786_m4262_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TisrandomColorScr_t832_m4265_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TiscollideAndDie_t779_m4266_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TispaintBlue_t827_m4267_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var;
extern "C" void naveProxyController_setWorking_m3592 (naveProxyController_t820 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Component_GetComponent_TisAnimation_t982_m4264_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484319);
		GameObject_GetComponent_TisdisparoProxy_t786_m4262_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484317);
		Component_GetComponent_TisrandomColorScr_t832_m4265_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484320);
		Component_GetComponent_TiscollideAndDie_t779_m4266_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484321);
		Component_GetComponent_TispaintBlue_t827_m4267_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484322);
		GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484318);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___working_2);
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral786, L_2, /*hidden argument*/NULL);
		MonoBehaviour_print_m994(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		bool L_4 = (__this->___working_2);
		if (L_4)
		{
			goto IL_0061;
		}
	}
	{
		GameObject_t144 * L_5 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t809 * L_6 = GameObject_get_transform_m4029(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t809 * L_7 = Transform_GetChild_m4050(L_6, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		Animation_t982 * L_8 = Component_GetComponent_TisAnimation_t982_m4264(L_7, /*hidden argument*/Component_GetComponent_TisAnimation_t982_m4264_MethodInfo_var);
		NullCheck(L_8);
		Animation_Play_m4268(L_8, /*hidden argument*/NULL);
		GameObject_t144 * L_9 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t809 * L_10 = GameObject_get_transform_m4029(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t809 * L_11 = Transform_GetChild_m4050(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		Animation_t982 * L_12 = Component_GetComponent_TisAnimation_t982_m4264(L_11, /*hidden argument*/Component_GetComponent_TisAnimation_t982_m4264_MethodInfo_var);
		NullCheck(L_12);
		Animation_Rewind_m4269(L_12, /*hidden argument*/NULL);
		SoundManager_playRepeteaClip_m3273(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0061:
	{
		__this->___working_2 = 1;
		GameObject_t144 * L_13 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		disparoProxy_t786 * L_14 = GameObject_GetComponent_TisdisparoProxy_t786_m4262(L_13, /*hidden argument*/GameObject_GetComponent_TisdisparoProxy_t786_m4262_MethodInfo_var);
		NullCheck(L_14);
		Behaviour_set_enabled_m4228(L_14, 1, /*hidden argument*/NULL);
		GameObject_t144 * L_15 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t809 * L_16 = GameObject_get_transform_m4029(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t809 * L_17 = Transform_GetChild_m4050(L_16, 0, /*hidden argument*/NULL);
		NullCheck(L_17);
		randomColorScr_t832 * L_18 = Component_GetComponent_TisrandomColorScr_t832_m4265(L_17, /*hidden argument*/Component_GetComponent_TisrandomColorScr_t832_m4265_MethodInfo_var);
		NullCheck(L_18);
		Behaviour_set_enabled_m4228(L_18, 0, /*hidden argument*/NULL);
		GameObject_t144 * L_19 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t809 * L_20 = GameObject_get_transform_m4029(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		collideAndDie_t779 * L_21 = Component_GetComponent_TiscollideAndDie_t779_m4266(L_20, /*hidden argument*/Component_GetComponent_TiscollideAndDie_t779_m4266_MethodInfo_var);
		NullCheck(L_21);
		Behaviour_set_enabled_m4228(L_21, 1, /*hidden argument*/NULL);
		GameObject_t144 * L_22 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t809 * L_23 = GameObject_get_transform_m4029(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t809 * L_24 = Transform_GetChild_m4050(L_23, 0, /*hidden argument*/NULL);
		NullCheck(L_24);
		paintBlue_t827 * L_25 = Component_GetComponent_TispaintBlue_t827_m4267(L_24, /*hidden argument*/Component_GetComponent_TispaintBlue_t827_m4267_MethodInfo_var);
		NullCheck(L_25);
		Behaviour_set_enabled_m4228(L_25, 1, /*hidden argument*/NULL);
		GameObject_t144 * L_26 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		gizmosProxy_t804 * L_27 = GameObject_GetComponent_TisgizmosProxy_t804_m4263(L_26, /*hidden argument*/GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var);
		NullCheck(L_27);
		gizmosProxy_createGizmos_m3510(L_27, /*hidden argument*/NULL);
		return;
	}
}
// newWeaponRoomSrc/<animSpin>c__Iterator11
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CanimSpinU3Ec__Iterator.h"
#ifndef _MSC_VER
#else
#endif
// newWeaponRoomSrc/<animSpin>c__Iterator11
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CanimSpinU3Ec__IteratorMethodDeclarations.h"

// newWeaponRoomSrc
#include "AssemblyU2DCSharp_newWeaponRoomSrc.h"


// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::.ctor()
extern "C" void U3CanimSpinU3Ec__Iterator11__ctor_m3593 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object newWeaponRoomSrc/<animSpin>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object newWeaponRoomSrc/<animSpin>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean newWeaponRoomSrc/<animSpin>c__Iterator11::MoveNext()
extern TypeInfo* WaitForSeconds_t213_il2cpp_TypeInfo_var;
extern "C" bool U3CanimSpinU3Ec__Iterator11_MoveNext_m3596 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t213_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t758  V_1 = {0};
	bool V_2 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0100;
		}
		if (L_1 == 2)
		{
			goto IL_014d;
		}
	}
	{
		goto IL_018b;
	}

IL_0025:
	{
		newWeaponRoomSrc_t821 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		Transform_t809 * L_3 = Component_get_transform_m4030(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t809 * L_4 = Transform_GetChild_m4050(L_3, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t809 * L_5 = Component_get_transform_m4030(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t758  L_6 = Transform_get_localScale_m4158(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = ((&V_1)->___y_2);
		if ((!(((float)L_7) < ((float)(1.0f)))))
		{
			goto IL_0105;
		}
	}
	{
		newWeaponRoomSrc_t821 * L_8 = (__this->___U3CU3Ef__this_3);
		newWeaponRoomSrc_t821 * L_9 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_9);
		float L_10 = (L_9->___v_7);
		NullCheck(L_8);
		L_8->___v_7 = ((float)((float)L_10+(float)(0.05f)));
		newWeaponRoomSrc_t821 * L_11 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_11);
		Transform_t809 * L_12 = Component_get_transform_m4030(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t809 * L_13 = Transform_GetChild_m4050(L_12, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t809 * L_14 = Component_get_transform_m4030(L_13, /*hidden argument*/NULL);
		newWeaponRoomSrc_t821 * L_15 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_15);
		float L_16 = (L_15->___v_7);
		newWeaponRoomSrc_t821 * L_17 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_17);
		float L_18 = (L_17->___v_7);
		Vector3_t758  L_19 = {0};
		Vector3__ctor_m4026(&L_19, L_16, L_18, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localScale_m4159(L_14, L_19, /*hidden argument*/NULL);
		newWeaponRoomSrc_t821 * L_20 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_20);
		Transform_t809 * L_21 = Component_get_transform_m4030(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t809 * L_22 = Transform_GetChild_m4050(L_21, 0, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t809 * L_23 = Component_get_transform_m4030(L_22, /*hidden argument*/NULL);
		newWeaponRoomSrc_t821 * L_24 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_24);
		float L_25 = (L_24->___v_7);
		Vector3_t758  L_26 = {0};
		Vector3__ctor_m4026(&L_26, (0.0f), (0.0f), ((float)((float)L_25*(float)(30.0f))), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_Rotate_m4259(L_23, L_26, /*hidden argument*/NULL);
		WaitForSeconds_t213 * L_27 = (WaitForSeconds_t213 *)il2cpp_codegen_object_new (WaitForSeconds_t213_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m965(L_27, (0.0f), /*hidden argument*/NULL);
		__this->___U24current_2 = L_27;
		__this->___U24PC_1 = 1;
		goto IL_018d;
	}

IL_0100:
	{
		goto IL_0184;
	}

IL_0105:
	{
		newWeaponRoomSrc_t821 * L_28 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_28);
		Transform_t809 * L_29 = Component_get_transform_m4030(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t809 * L_30 = Transform_GetChild_m4050(L_29, 0, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t809 * L_31 = Component_get_transform_m4030(L_30, /*hidden argument*/NULL);
		Vector3_t758  L_32 = Vector3_get_one_m4242(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localScale_m4159(L_31, L_32, /*hidden argument*/NULL);
		Vector3_t758  L_33 = Vector3_get_zero_m4166(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CzU3E__0_0 = L_33;
		MonoBehaviour_print_m994(NULL /*static, unused*/, (String_t*) &_stringLiteral790, /*hidden argument*/NULL);
		__this->___U24current_2 = NULL;
		__this->___U24PC_1 = 2;
		goto IL_018d;
	}

IL_014d:
	{
		newWeaponRoomSrc_t821 * L_34 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_34);
		Transform_t809 * L_35 = Component_get_transform_m4030(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t809 * L_36 = Transform_GetChild_m4050(L_35, 0, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t809 * L_37 = Component_get_transform_m4030(L_36, /*hidden argument*/NULL);
		Vector3_t758  L_38 = (__this->___U3CzU3E__0_0);
		Quaternion_t771  L_39 = Quaternion_Euler_m4157(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_localRotation_m4270(L_37, L_39, /*hidden argument*/NULL);
		newWeaponRoomSrc_t821 * L_40 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_40);
		MonoBehaviour_StartCoroutine_m4065(L_40, (String_t*) &_stringLiteral787, /*hidden argument*/NULL);
	}

IL_0184:
	{
		__this->___U24PC_1 = (-1);
	}

IL_018b:
	{
		return 0;
	}

IL_018d:
	{
		return 1;
	}
	// Dead block : IL_018f: ldloc.2
}
// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::Dispose()
extern "C" void U3CanimSpinU3Ec__Iterator11_Dispose_m3597 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::Reset()
extern TypeInfo* NotSupportedException_t192_il2cpp_TypeInfo_var;
extern "C" void U3CanimSpinU3Ec__Iterator11_Reset_m3598 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t192 * L_0 = (NotSupportedException_t192 *)il2cpp_codegen_object_new (NotSupportedException_t192_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m865(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// newWeaponRoomSrc/<waiting>c__Iterator12
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CwaitingU3Ec__Iterator1.h"
#ifndef _MSC_VER
#else
#endif
// newWeaponRoomSrc/<waiting>c__Iterator12
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CwaitingU3Ec__Iterator1MethodDeclarations.h"

// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
// newWeaponRoomSrc
#include "AssemblyU2DCSharp_newWeaponRoomSrcMethodDeclarations.h"


// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::.ctor()
extern "C" void U3CwaitingU3Ec__Iterator12__ctor_m3599 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object newWeaponRoomSrc/<waiting>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object newWeaponRoomSrc/<waiting>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean newWeaponRoomSrc/<waiting>c__Iterator12::MoveNext()
extern TypeInfo* Input_t987_il2cpp_TypeInfo_var;
extern TypeInfo* newWeaponRoomSrc_t821_il2cpp_TypeInfo_var;
extern "C" bool U3CwaitingU3Ec__Iterator12_MoveNext_m3602 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t987_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		newWeaponRoomSrc_t821_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Touch_t901  V_1 = {0};
	bool V_2 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0058;
		}
	}
	{
		goto IL_005f;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_touchCount_m4096(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		Touch_t901  L_3 = Input_GetTouch_m4097(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Touch_get_phase_m4098((&V_1), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0058;
		}
	}
	{
		((newWeaponRoomSrc_t821_StaticFields*)newWeaponRoomSrc_t821_il2cpp_TypeInfo_var->static_fields)->___isNewWeaponReady_5 = 1;
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_0061;
	}

IL_0058:
	{
		__this->___U24PC_0 = (-1);
	}

IL_005f:
	{
		return 0;
	}

IL_0061:
	{
		return 1;
	}
	// Dead block : IL_0063: ldloc.2
}
// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::Dispose()
extern "C" void U3CwaitingU3Ec__Iterator12_Dispose_m3603 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::Reset()
extern TypeInfo* NotSupportedException_t192_il2cpp_TypeInfo_var;
extern "C" void U3CwaitingU3Ec__Iterator12_Reset_m3604 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t192 * L_0 = (NotSupportedException_t192 *)il2cpp_codegen_object_new (NotSupportedException_t192_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m865(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// WeaponsController/WEAPONS
#include "AssemblyU2DCSharpU2Dfirstpass_WeaponsController_WEAPONS.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
// System.Enum
#include "mscorlib_System_Enum.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
struct Component_t230;
struct SpriteRenderer_t744;
struct Component_t230;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m4044_gshared (Component_t230 * __this, MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m4044(__this, method) (( Object_t * (*) (Component_t230 *, MethodInfo*))Component_GetComponentInChildren_TisObject_t_m4044_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.SpriteRenderer>()
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.SpriteRenderer>()
#define Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160(__this, method) (( SpriteRenderer_t744 * (*) (Component_t230 *, MethodInfo*))Component_GetComponentInChildren_TisObject_t_m4044_gshared)(__this, method)


// System.Void newWeaponRoomSrc::.ctor()
extern TypeInfo* GUIStyle_t724_il2cpp_TypeInfo_var;
extern "C" void newWeaponRoomSrc__ctor_m3605 (newWeaponRoomSrc_t821 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle_t724 * L_0 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_0, /*hidden argument*/NULL);
		__this->___newWeapon_2 = L_0;
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void newWeaponRoomSrc::Start()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* WeaponsController_t109_il2cpp_TypeInfo_var;
extern MethodInfo* Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160_MethodInfo_var;
extern "C" void newWeaponRoomSrc_Start_m3606 (newWeaponRoomSrc_t821 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		WeaponsController_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484287);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_0 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_1 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		Rect_t738  L_2 = {0};
		Rect__ctor_m4041(&L_2, ((float)((float)L_0/(float)(2.0f))), ((float)((float)L_1/(float)(2.0f))), (300.0f), (100.0f), /*hidden argument*/NULL);
		__this->___halfScreen_6 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		int32_t L_3 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___unlockedWeapons_9;
		V_0 = L_3;
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = V_0;
		((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___unlockedWeapons_9 = L_5;
		SpriteRenderer_t744 * L_6 = Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160(__this, /*hidden argument*/Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160_MethodInfo_var);
		SpriteU5BU5D_t824* L_7 = (__this->___weaponsSprites_4);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck(L_6);
		SpriteRenderer_set_sprite_m4164(L_6, (*(Sprite_t766 **)(Sprite_t766 **)SZArrayLdElema(L_7, L_9)), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m4065(__this, (String_t*) &_stringLiteral787, /*hidden argument*/NULL);
		Transform_t809 * L_10 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t809 * L_11 = Transform_GetChild_m4050(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t809 * L_12 = Component_get_transform_m4030(L_11, /*hidden argument*/NULL);
		Vector3_t758  L_13 = Vector3_get_zero_m4166(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_m4159(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void newWeaponRoomSrc::Update()
extern "C" void newWeaponRoomSrc_Update_m3607 (newWeaponRoomSrc_t821 * __this, MethodInfo* method)
{
	{
		MonoBehaviour_StartCoroutine_m4065(__this, (String_t*) &_stringLiteral788, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator newWeaponRoomSrc::animSpin()
extern TypeInfo* U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo_var;
extern "C" Object_t * newWeaponRoomSrc_animSpin_m3608 (newWeaponRoomSrc_t821 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1056);
		s_Il2CppMethodIntialized = true;
	}
	U3CanimSpinU3Ec__Iterator11_t822 * V_0 = {0};
	{
		U3CanimSpinU3Ec__Iterator11_t822 * L_0 = (U3CanimSpinU3Ec__Iterator11_t822 *)il2cpp_codegen_object_new (U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo_var);
		U3CanimSpinU3Ec__Iterator11__ctor_m3593(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CanimSpinU3Ec__Iterator11_t822 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CanimSpinU3Ec__Iterator11_t822 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator newWeaponRoomSrc::waiting()
extern TypeInfo* U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo_var;
extern "C" Object_t * newWeaponRoomSrc_waiting_m3609 (newWeaponRoomSrc_t821 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		s_Il2CppMethodIntialized = true;
	}
	U3CwaitingU3Ec__Iterator12_t823 * V_0 = {0};
	{
		U3CwaitingU3Ec__Iterator12_t823 * L_0 = (U3CwaitingU3Ec__Iterator12_t823 *)il2cpp_codegen_object_new (U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo_var);
		U3CwaitingU3Ec__Iterator12__ctor_m3599(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CwaitingU3Ec__Iterator12_t823 * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean newWeaponRoomSrc::isReady()
extern TypeInfo* newWeaponRoomSrc_t821_il2cpp_TypeInfo_var;
extern "C" bool newWeaponRoomSrc_isReady_m3610 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		newWeaponRoomSrc_t821_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((newWeaponRoomSrc_t821_StaticFields*)newWeaponRoomSrc_t821_il2cpp_TypeInfo_var->static_fields)->___isNewWeaponReady_5;
		return L_0;
	}
}
// System.Void newWeaponRoomSrc::setReady(System.Boolean)
extern TypeInfo* newWeaponRoomSrc_t821_il2cpp_TypeInfo_var;
extern "C" void newWeaponRoomSrc_setReady_m3611 (Object_t * __this /* static, unused */, bool ___readyState, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		newWeaponRoomSrc_t821_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___readyState;
		((newWeaponRoomSrc_t821_StaticFields*)newWeaponRoomSrc_t821_il2cpp_TypeInfo_var->static_fields)->___isNewWeaponReady_5 = L_0;
		return;
	}
}
// System.Void newWeaponRoomSrc::OnGUI()
extern TypeInfo* GUI_t981_il2cpp_TypeInfo_var;
extern TypeInfo* WeaponsController_t109_il2cpp_TypeInfo_var;
extern TypeInfo* WEAPONS_t105_il2cpp_TypeInfo_var;
extern "C" void newWeaponRoomSrc_OnGUI_m3612 (newWeaponRoomSrc_t821 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t981_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		WeaponsController_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		WEAPONS_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(124);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t738 * L_0 = &(__this->___halfScreen_6);
		float L_1 = Rect_get_x_m4077(L_0, /*hidden argument*/NULL);
		Rect_t738 * L_2 = &(__this->___halfScreen_6);
		float L_3 = Rect_get_y_m4078(L_2, /*hidden argument*/NULL);
		Rect_t738  L_4 = {0};
		Rect__ctor_m4041(&L_4, L_1, L_3, (100.0f), (100.0f), /*hidden argument*/NULL);
		GUIStyle_t724 * L_5 = (__this->___newWeapon_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_Label_m4042(NULL /*static, unused*/, L_4, (String_t*) &_stringLiteral789, L_5, /*hidden argument*/NULL);
		Rect_t738 * L_6 = &(__this->___halfScreen_6);
		float L_7 = Rect_get_x_m4077(L_6, /*hidden argument*/NULL);
		Rect_t738 * L_8 = &(__this->___halfScreen_6);
		float L_9 = Rect_get_y_m4078(L_8, /*hidden argument*/NULL);
		Rect_t738  L_10 = {0};
		Rect__ctor_m4041(&L_10, L_7, ((float)((float)L_9+(float)(50.0f))), (100.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		int32_t L_11 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___unlockedWeapons_9;
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(WEAPONS_t105_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_13);
		String_t* L_14 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_13);
		GUIStyle_t724 * L_15 = (__this->___newWeapon_2);
		GUI_Label_m4042(NULL /*static, unused*/, L_10, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void newWeaponRoomSrc::OnApplicationQuit()
extern "C" void newWeaponRoomSrc_OnApplicationQuit_m3613 (newWeaponRoomSrc_t821 * __this, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// number
#include "AssemblyU2DCSharp_number.h"
#ifndef _MSC_VER
#else
#endif
// number
#include "AssemblyU2DCSharp_numberMethodDeclarations.h"



// System.Void number::.ctor()
extern "C" void number__ctor_m3614 (number_t825 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// orlaScr
#include "AssemblyU2DCSharp_orlaScr.h"
#ifndef _MSC_VER
#else
#endif
// orlaScr
#include "AssemblyU2DCSharp_orlaScrMethodDeclarations.h"



// System.Void orlaScr::.ctor()
extern TypeInfo* GUIStyle_t724_il2cpp_TypeInfo_var;
extern "C" void orlaScr__ctor_m3615 (orlaScr_t826 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle_t724 * L_0 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_0, /*hidden argument*/NULL);
		__this->___orlitaSt_2 = L_0;
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void orlaScr::Start()
extern "C" void orlaScr_Start_m3616 (orlaScr_t826 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void orlaScr::Update()
extern "C" void orlaScr_Update_m3617 (orlaScr_t826 * __this, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t809 * L_1 = GameObject_get_transform_m4029(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t809 * L_2 = Transform_get_parent_m4132(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t809 * L_3 = Component_get_transform_m4030(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t144 * L_4 = Component_get_gameObject_m853(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeInHierarchy_m4271(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t144 * L_6 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m4272(L_6, 1, /*hidden argument*/NULL);
		goto IL_0040;
	}

IL_0035:
	{
		GameObject_t144 * L_7 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void orlaScr::OnGUI()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t981_il2cpp_TypeInfo_var;
extern "C" void orlaScr_OnGUI_m3618 (orlaScr_t826 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUI_t981_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t758  V_0 = {0};
	int32_t V_1 = 0;
	{
		Camera_t978 * L_0 = Camera_get_main_m4021(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t809 * L_1 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t758  L_2 = Transform_get_position_m4034(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t758  L_3 = Camera_WorldToScreenPoint_m4040(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_4 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_5 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = ((float)((float)((float)((float)L_4-(float)L_5))-(float)(60.0f)));
		float L_6 = ((&V_0)->___x_1);
		(&V_0)->___x_1 = ((float)((float)L_6-(float)(10.0f)));
		float L_7 = ((&V_0)->___x_1);
		float L_8 = ((&V_0)->___y_2);
		Rect_t738  L_9 = {0};
		Rect__ctor_m4041(&L_9, L_7, L_8, (20.0f), (20.0f), /*hidden argument*/NULL);
		int32_t L_10 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___currentStage_14;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
		String_t* L_11 = Int32_ToString_m4081((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral791, L_11, /*hidden argument*/NULL);
		GUIStyle_t724 * L_13 = (__this->___orlitaSt_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_Label_m4042(NULL /*static, unused*/, L_9, L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// paintBlue
#include "AssemblyU2DCSharp_paintBlueMethodDeclarations.h"



// System.Void paintBlue::.ctor()
extern "C" void paintBlue__ctor_m3619 (paintBlue_t827 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void paintBlue::Start()
extern MethodInfo* Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var;
extern "C" void paintBlue_Start_m3620 (paintBlue_t827 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484280);
		s_Il2CppMethodIntialized = true;
	}
	Color_t747  V_0 = {0};
	{
		Color_t747  L_0 = Color_get_blue_m4118(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		SpriteRenderer_t744 * L_1 = Component_GetComponent_TisSpriteRenderer_t744_m4109(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var);
		Color_t747  L_2 = V_0;
		NullCheck(L_1);
		SpriteRenderer_set_color_m4213(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// paqueteScr
#include "AssemblyU2DCSharp_paqueteScr.h"
#ifndef _MSC_VER
#else
#endif
// paqueteScr
#include "AssemblyU2DCSharp_paqueteScrMethodDeclarations.h"

struct Component_t230;
struct Animation_t982;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animation>()
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animation>()
#define Component_GetComponentInChildren_TisAnimation_t982_m4043(__this, method) (( Animation_t982 * (*) (Component_t230 *, MethodInfo*))Component_GetComponentInChildren_TisObject_t_m4044_gshared)(__this, method)


// System.Void paqueteScr::.ctor()
extern "C" void paqueteScr__ctor_m3621 (paqueteScr_t828 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void paqueteScr::Start()
extern TypeInfo* WeaponsController_t109_il2cpp_TypeInfo_var;
extern "C" void paqueteScr_Start_m3622 (paqueteScr_t828 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WeaponsController_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t758  V_0 = {0};
	Vector3_t758  V_1 = {0};
	{
		Transform_t809 * L_0 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Quaternion_t771  L_1 = Quaternion_get_identity_m4027(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_rotation_m4171(L_0, L_1, /*hidden argument*/NULL);
		Transform_t809 * L_2 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Transform_t809 * L_3 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t758  L_4 = Transform_get_position_m4034(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = ((&V_0)->___x_1);
		Transform_t809 * L_6 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t758  L_7 = Transform_get_position_m4034(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = ((&V_1)->___y_2);
		Vector3_t758  L_9 = {0};
		Vector3__ctor_m4026(&L_9, L_5, L_8, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m4039(L_2, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		int32_t L_10 = WeaponsController_getWeaponType_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___bulletType_12 = L_10;
		__this->___lift_14 = 0;
		return;
	}
}
// System.Void paqueteScr::Update()
extern "C" void paqueteScr_Update_m3623 (paqueteScr_t828 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___lifeTime_13);
		__this->___lifeTime_13 = ((int32_t)((int32_t)L_0-(int32_t)1));
		int32_t L_1 = (__this->___lifeTime_13);
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		GameObject_t144 * L_2 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		bool L_3 = (__this->___lift_14);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		Transform_t809 * L_4 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		float L_5 = (__this->___aumento_10);
		NullCheck(L_4);
		Transform_Translate_m4047(L_4, (0.0f), L_5, (0.0f), /*hidden argument*/NULL);
		int32_t L_6 = (__this->___lifeTime_13);
		__this->___lifeTime_13 = ((int32_t)((int32_t)L_6-(int32_t)((int32_t)30)));
	}

IL_005a:
	{
		return;
	}
}
// System.Void paqueteScr::giveBullets()
extern TypeInfo* WeaponsController_t109_il2cpp_TypeInfo_var;
extern "C" void paqueteScr_giveBullets_m3624 (paqueteScr_t828 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WeaponsController_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = (__this->___bulletType_12);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0026;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
		if (L_1 == 2)
		{
			goto IL_0064;
		}
		if (L_1 == 3)
		{
			goto IL_0083;
		}
		if (L_1 == 4)
		{
			goto IL_00a2;
		}
	}
	{
		goto IL_00c1;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		Int32U5BU5D_t107* L_2 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___bullets_2;
		int32_t L_3 = (__this->___bulletType_12);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t* L_4 = ((int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3));
		int32_t L_5 = (__this->___wBlockBonus_2);
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_4))+(int32_t)L_5));
		goto IL_00c1;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		Int32U5BU5D_t107* L_6 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___bullets_2;
		int32_t L_7 = (__this->___bulletType_12);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t* L_8 = ((int32_t*)(int32_t*)SZArrayLdElema(L_6, L_7));
		int32_t L_9 = (__this->___laserBonus_3);
		*((int32_t*)(L_8)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)L_9));
		goto IL_00c1;
	}

IL_0064:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		Int32U5BU5D_t107* L_10 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___bullets_2;
		int32_t L_11 = (__this->___bulletType_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t* L_12 = ((int32_t*)(int32_t*)SZArrayLdElema(L_10, L_11));
		int32_t L_13 = (__this->___TWayBonus_4);
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_12))+(int32_t)L_13));
		goto IL_00c1;
	}

IL_0083:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		Int32U5BU5D_t107* L_14 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___bullets_2;
		int32_t L_15 = (__this->___bulletType_12);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t* L_16 = ((int32_t*)(int32_t*)SZArrayLdElema(L_14, L_15));
		int32_t L_17 = (__this->___circularBonus_5);
		*((int32_t*)(L_16)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_16))+(int32_t)L_17));
		goto IL_00c1;
	}

IL_00a2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		Int32U5BU5D_t107* L_18 = ((WeaponsController_t109_StaticFields*)WeaponsController_t109_il2cpp_TypeInfo_var->static_fields)->___bullets_2;
		int32_t L_19 = (__this->___bulletType_12);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t* L_20 = ((int32_t*)(int32_t*)SZArrayLdElema(L_18, L_19));
		int32_t L_21 = (__this->___moireBonus_7);
		*((int32_t*)(L_20)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_20))+(int32_t)L_21));
		goto IL_00c1;
	}

IL_00c1:
	{
		return;
	}
}
// System.Void paqueteScr::triggerAnim()
extern MethodInfo* Component_GetComponentInChildren_TisAnimation_t982_m4043_MethodInfo_var;
extern "C" void paqueteScr_triggerAnim_m3625 (paqueteScr_t828 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisAnimation_t982_m4043_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484270);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___lift_14 = 1;
		Animation_t982 * L_0 = Component_GetComponentInChildren_TisAnimation_t982_m4043(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimation_t982_m4043_MethodInfo_var);
		NullCheck(L_0);
		Animation_Play_m4049(L_0, (String_t*) &_stringLiteral633, /*hidden argument*/NULL);
		return;
	}
}
// playerMovement/<moveCoin>c__Iterator13
#include "AssemblyU2DCSharp_playerMovement_U3CmoveCoinU3Ec__Iterator13.h"
#ifndef _MSC_VER
#else
#endif
// playerMovement/<moveCoin>c__Iterator13
#include "AssemblyU2DCSharp_playerMovement_U3CmoveCoinU3Ec__Iterator13MethodDeclarations.h"



// System.Void playerMovement/<moveCoin>c__Iterator13::.ctor()
extern "C" void U3CmoveCoinU3Ec__Iterator13__ctor_m3626 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object playerMovement/<moveCoin>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object playerMovement/<moveCoin>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean playerMovement/<moveCoin>c__Iterator13::MoveNext()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t213_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t739_il2cpp_TypeInfo_var;
extern "C" bool U3CmoveCoinU3Ec__Iterator13_MoveNext_m3629 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		WaitForSeconds_t213_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		Vector2_t739_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t758  V_1 = {0};
	Vector3_t758  V_2 = {0};
	bool V_3 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_010a;
		}
	}
	{
		goto IL_0149;
	}

IL_0021:
	{
		Camera_t978 * L_2 = Camera_get_main_m4021(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_3 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_4 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		Vector2_t739  L_5 = {0};
		Vector2__ctor_m4033(&L_5, ((float)((float)L_3/(float)(2.0f))), ((-L_4)), /*hidden argument*/NULL);
		Vector3_t758  L_6 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t758  L_7 = Camera_ScreenToWorldPoint_m4196(L_2, L_6, /*hidden argument*/NULL);
		Vector2_t739  L_8 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->___U3CtargetU3E__0_0 = L_8;
		goto IL_010a;
	}

IL_0056:
	{
		GameObject_t144 * L_9 = (__this->___coin_1);
		NullCheck(L_9);
		Transform_t809 * L_10 = GameObject_get_transform_m4029(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t758  L_11 = Transform_get_position_m4034(L_10, /*hidden argument*/NULL);
		Vector2_t739  L_12 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Vector2_t739  L_13 = (__this->___U3CtargetU3E__0_0);
		float L_14 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t739  L_15 = Vector2_MoveTowards_m4037(NULL /*static, unused*/, L_12, L_13, ((float)((float)L_14*(float)(8.0f))), /*hidden argument*/NULL);
		__this->___U3CmovU3E__1_2 = L_15;
		GameObject_t144 * L_16 = (__this->___coin_1);
		NullCheck(L_16);
		Transform_t809 * L_17 = GameObject_get_transform_m4029(L_16, /*hidden argument*/NULL);
		Vector2_t739  L_18 = (__this->___U3CmovU3E__1_2);
		Vector3_t758  L_19 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m4039(L_17, L_19, /*hidden argument*/NULL);
		GameObject_t144 * L_20 = (__this->___coin_1);
		NullCheck(L_20);
		Transform_t809 * L_21 = GameObject_get_transform_m4029(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t758  L_22 = Transform_get_localScale_m4158(L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		float L_23 = ((&V_1)->___x_1);
		if ((!(((float)L_23) < ((float)(2.0f)))))
		{
			goto IL_00ee;
		}
	}
	{
		GameObject_t144 * L_24 = (__this->___coin_1);
		NullCheck(L_24);
		Transform_t809 * L_25 = GameObject_get_transform_m4029(L_24, /*hidden argument*/NULL);
		GameObject_t144 * L_26 = (__this->___coin_1);
		NullCheck(L_26);
		Transform_t809 * L_27 = GameObject_get_transform_m4029(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t758  L_28 = Transform_get_localScale_m4158(L_27, /*hidden argument*/NULL);
		Vector3_t758  L_29 = Vector3_op_Multiply_m4134(NULL /*static, unused*/, L_28, (1.1f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localScale_m4159(L_25, L_29, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		WaitForSeconds_t213 * L_30 = (WaitForSeconds_t213 *)il2cpp_codegen_object_new (WaitForSeconds_t213_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m965(L_30, (0.0f), /*hidden argument*/NULL);
		__this->___U24current_4 = L_30;
		__this->___U24PC_3 = 1;
		goto IL_014b;
	}

IL_010a:
	{
		GameObject_t144 * L_31 = (__this->___coin_1);
		bool L_32 = Object_op_Implicit_m1059(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0142;
		}
	}
	{
		GameObject_t144 * L_33 = (__this->___coin_1);
		NullCheck(L_33);
		Transform_t809 * L_34 = GameObject_get_transform_m4029(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t758  L_35 = Transform_get_position_m4034(L_34, /*hidden argument*/NULL);
		V_2 = L_35;
		Vector2_t739  L_36 = (__this->___U3CtargetU3E__0_0);
		Vector2_t739  L_37 = L_36;
		Object_t * L_38 = Box(Vector2_t739_il2cpp_TypeInfo_var, &L_37);
		bool L_39 = Vector3_Equals_m4273((&V_2), L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0056;
		}
	}

IL_0142:
	{
		__this->___U24PC_3 = (-1);
	}

IL_0149:
	{
		return 0;
	}

IL_014b:
	{
		return 1;
	}
	// Dead block : IL_014d: ldloc.3
}
// System.Void playerMovement/<moveCoin>c__Iterator13::Dispose()
extern "C" void U3CmoveCoinU3Ec__Iterator13_Dispose_m3630 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void playerMovement/<moveCoin>c__Iterator13::Reset()
extern TypeInfo* NotSupportedException_t192_il2cpp_TypeInfo_var;
extern "C" void U3CmoveCoinU3Ec__Iterator13_Reset_m3631 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t192 * L_0 = (NotSupportedException_t192 *)il2cpp_codegen_object_new (NotSupportedException_t192_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m865(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// playerMovement
#include "AssemblyU2DCSharp_playerMovement.h"
#ifndef _MSC_VER
#else
#endif
// playerMovement
#include "AssemblyU2DCSharp_playerMovementMethodDeclarations.h"

// cameraScript
#include "AssemblyU2DCSharp_cameraScript.h"
// disparo
#include "AssemblyU2DCSharp_disparo.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// gameControl
#include "AssemblyU2DCSharp_gameControl.h"
// gameControl/State
#include "AssemblyU2DCSharp_gameControl_State.h"
// Coin
#include "AssemblyU2DCSharp_Coin.h"
// triggerSplashBomb
#include "AssemblyU2DCSharp_triggerSplashBomb.h"
// InputHelper
#include "AssemblyU2DCSharp_InputHelperMethodDeclarations.h"
// gameControl
#include "AssemblyU2DCSharp_gameControlMethodDeclarations.h"
// CoinsManager
#include "AssemblyU2DCSharpU2Dfirstpass_CoinsManagerMethodDeclarations.h"
// System.Threading.Thread
#include "mscorlib_System_Threading_ThreadMethodDeclarations.h"
// triggerSplashBomb
#include "AssemblyU2DCSharp_triggerSplashBombMethodDeclarations.h"
struct GameObject_t144;
struct disparo_t784;
// Declaration !!0 UnityEngine.GameObject::GetComponent<disparo>()
// !!0 UnityEngine.GameObject::GetComponent<disparo>()
#define GameObject_GetComponent_Tisdisparo_t784_m4218(__this, method) (( disparo_t784 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)
struct Component_t230;
struct cameraScript_t774;
// Declaration !!0 UnityEngine.Component::GetComponent<cameraScript>()
// !!0 UnityEngine.Component::GetComponent<cameraScript>()
#define Component_GetComponent_TiscameraScript_t774_m4177(__this, method) (( cameraScript_t774 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)
struct GameObject_t144;
struct gameControl_t794;
// Declaration !!0 UnityEngine.GameObject::GetComponent<gameControl>()
// !!0 UnityEngine.GameObject::GetComponent<gameControl>()
#define GameObject_GetComponent_TisgameControl_t794_m4062(__this, method) (( gameControl_t794 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)
struct GameObject_t144;
struct paqueteScr_t828;
// Declaration !!0 UnityEngine.GameObject::GetComponent<paqueteScr>()
// !!0 UnityEngine.GameObject::GetComponent<paqueteScr>()
#define GameObject_GetComponent_TispaqueteScr_t828_m4274(__this, method) (( paqueteScr_t828 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)
struct Component_t230;
struct Coin_t727;
// Declaration !!0 UnityEngine.Component::GetComponent<Coin>()
// !!0 UnityEngine.Component::GetComponent<Coin>()
#define Component_GetComponent_TisCoin_t727_m4275(__this, method) (( Coin_t727 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)
struct Component_t230;
struct triggerSplashBomb_t836;
// Declaration !!0 UnityEngine.Component::GetComponent<triggerSplashBomb>()
// !!0 UnityEngine.Component::GetComponent<triggerSplashBomb>()
#define Component_GetComponent_TistriggerSplashBomb_t836_m4276(__this, method) (( triggerSplashBomb_t836 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)
struct Component_t230;
struct naveProxyController_t820;
// Declaration !!0 UnityEngine.Component::GetComponent<naveProxyController>()
// !!0 UnityEngine.Component::GetComponent<naveProxyController>()
#define Component_GetComponent_TisnaveProxyController_t820_m4277(__this, method) (( naveProxyController_t820 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)


// System.Void playerMovement::.ctor()
extern "C" void playerMovement__ctor_m3632 (playerMovement_t830 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void playerMovement::move()
extern TypeInfo* InputHelper_t740_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t987_il2cpp_TypeInfo_var;
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_GetComponent_Tisdisparo_t784_m4218_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TiscameraScript_t774_m4177_MethodInfo_var;
extern "C" void playerMovement_move_m3633 (playerMovement_t830 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InputHelper_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(997);
		Input_t987_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		GameObject_GetComponent_Tisdisparo_t784_m4218_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484306);
		Component_GetComponent_TiscameraScript_t774_m4177_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484292);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t739  V_0 = {0};
	Touch_t901  V_1 = {0};
	Touch_t901  V_2 = {0};
	cameraScript_t774 * V_3 = {0};
	Vector3_t758  V_4 = {0};
	Vector3_t758  V_5 = {0};
	Vector3_t758  V_6 = {0};
	Vector3_t758  V_7 = {0};
	Vector3_t758  V_8 = {0};
	Vector3_t758  V_9 = {0};
	Vector3_t758  V_10 = {0};
	Vector3_t758  V_11 = {0};
	Vector3_t758  V_12 = {0};
	Vector3_t758  V_13 = {0};
	Vector3_t758  V_14 = {0};
	Vector3_t758  V_15 = {0};
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		disparo_t784 * L_1 = GameObject_GetComponent_Tisdisparo_t784_m4218(L_0, /*hidden argument*/GameObject_GetComponent_Tisdisparo_t784_m4218_MethodInfo_var);
		NullCheck(L_1);
		bool L_2 = (L_1->___isLaserState_12);
		if (L_2)
		{
			goto IL_0182;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InputHelper_t740_il2cpp_TypeInfo_var);
		Vector2_t739  L_3 = InputHelper_touch_m3210(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = ((&V_0)->___x_1);
		float L_5 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = (__this->___acceleartionTouch_19);
		__this->___accX_14 = ((float)((float)((float)((float)L_4*(float)L_5))*(float)L_6));
		float L_7 = ((&V_0)->___y_2);
		float L_8 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (__this->___acceleartionTouch_19);
		__this->___accY_15 = ((float)((float)((float)((float)L_7*(float)L_8))*(float)L_9));
		float L_10 = (__this->___accX_14);
		float L_11 = (__this->___speed_2);
		__this->___speedX_16 = ((float)((float)L_10*(float)L_11));
		float L_12 = (__this->___accY_15);
		float L_13 = (__this->___speed_2);
		__this->___speedY_17 = ((float)((float)L_12*(float)L_13));
		Transform_t809 * L_14 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Transform_t809 * L_15 = L_14;
		NullCheck(L_15);
		Vector3_t758  L_16 = Transform_get_position_m4034(L_15, /*hidden argument*/NULL);
		float L_17 = (__this->___speedX_16);
		float L_18 = (__this->___speedY_17);
		Vector3_t758  L_19 = {0};
		Vector3__ctor_m4026(&L_19, L_17, L_18, (0.0f), /*hidden argument*/NULL);
		Vector3_t758  L_20 = Vector3_op_Addition_m4278(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_position_m4039(L_15, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		int32_t L_21 = Input_get_touchCount_m4096(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_0182;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t987_il2cpp_TypeInfo_var);
		TouchU5BU5D_t998* L_22 = Input_get_touches_m4279(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		V_1 = (*(Touch_t901 *)((Touch_t901 *)(Touch_t901 *)SZArrayLdElema(L_22, 0)));
		TouchU5BU5D_t998* L_23 = Input_get_touches_m4279(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		V_2 = (*(Touch_t901 *)((Touch_t901 *)(Touch_t901 *)SZArrayLdElema(L_23, 1)));
		int32_t L_24 = Touch_get_phase_m4098((&V_1), /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)1)))
		{
			goto IL_00e8;
		}
	}
	{
		int32_t L_25 = Touch_get_phase_m4098((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)1))))
		{
			goto IL_0182;
		}
	}

IL_00e8:
	{
		Vector2_t739  L_26 = Touch_get_deltaPosition_m4099((&V_1), /*hidden argument*/NULL);
		Vector2_t739  L_27 = Touch_get_deltaPosition_m4099((&V_2), /*hidden argument*/NULL);
		Vector2_t739  L_28 = Vector2_op_Addition_m4280(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		float L_29 = ((&V_0)->___x_1);
		float L_30 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_31 = (__this->___acceleartionTouch_19);
		__this->___accX_14 = ((float)((float)((float)((float)L_29*(float)L_30))*(float)L_31));
		float L_32 = ((&V_0)->___y_2);
		float L_33 = Time_get_deltaTime_m4036(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_34 = (__this->___acceleartionTouch_19);
		__this->___accY_15 = ((float)((float)((float)((float)L_32*(float)L_33))*(float)L_34));
		float L_35 = (__this->___accX_14);
		float L_36 = (__this->___speed_2);
		__this->___speedX_16 = ((float)((float)L_35*(float)L_36));
		float L_37 = (__this->___accY_15);
		float L_38 = (__this->___speed_2);
		__this->___speedY_17 = ((float)((float)L_37*(float)L_38));
		Transform_t809 * L_39 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Transform_t809 * L_40 = L_39;
		NullCheck(L_40);
		Vector3_t758  L_41 = Transform_get_position_m4034(L_40, /*hidden argument*/NULL);
		float L_42 = (__this->___speedX_16);
		float L_43 = (__this->___speedY_17);
		Vector3_t758  L_44 = {0};
		Vector3__ctor_m4026(&L_44, L_42, L_43, (0.0f), /*hidden argument*/NULL);
		Vector3_t758  L_45 = Vector3_op_Addition_m4278(NULL /*static, unused*/, L_41, L_44, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_set_position_m4039(L_40, L_45, /*hidden argument*/NULL);
	}

IL_0182:
	{
		Camera_t978 * L_46 = Camera_get_main_m4021(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_46);
		cameraScript_t774 * L_47 = Component_GetComponent_TiscameraScript_t774_m4177(L_46, /*hidden argument*/Component_GetComponent_TiscameraScript_t774_m4177_MethodInfo_var);
		V_3 = L_47;
		Transform_t809 * L_48 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t758  L_49 = Transform_get_position_m4034(L_48, /*hidden argument*/NULL);
		V_8 = L_49;
		float L_50 = ((&V_8)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_51 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___maxX_19;
		if ((!(((float)L_50) > ((float)L_51))))
		{
			goto IL_01dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_52 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___minX_18;
		Transform_t809 * L_53 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t758  L_54 = Transform_get_position_m4034(L_53, /*hidden argument*/NULL);
		V_9 = L_54;
		float L_55 = ((&V_9)->___y_2);
		Vector3__ctor_m4026((&V_4), L_52, L_55, (0.0f), /*hidden argument*/NULL);
		Transform_t809 * L_56 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_57 = V_4;
		NullCheck(L_56);
		Transform_set_position_m4039(L_56, L_57, /*hidden argument*/NULL);
	}

IL_01dd:
	{
		Transform_t809 * L_58 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		Vector3_t758  L_59 = Transform_get_position_m4034(L_58, /*hidden argument*/NULL);
		V_10 = L_59;
		float L_60 = ((&V_10)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_61 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___minX_18;
		if ((!(((float)L_60) < ((float)L_61))))
		{
			goto IL_022d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_62 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___maxX_19;
		Transform_t809 * L_63 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_63);
		Vector3_t758  L_64 = Transform_get_position_m4034(L_63, /*hidden argument*/NULL);
		V_11 = L_64;
		float L_65 = ((&V_11)->___y_2);
		Vector3__ctor_m4026((&V_5), L_62, L_65, (0.0f), /*hidden argument*/NULL);
		Transform_t809 * L_66 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_67 = V_5;
		NullCheck(L_66);
		Transform_set_position_m4039(L_66, L_67, /*hidden argument*/NULL);
	}

IL_022d:
	{
		Transform_t809 * L_68 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_68);
		Vector3_t758  L_69 = Transform_get_position_m4034(L_68, /*hidden argument*/NULL);
		V_12 = L_69;
		float L_70 = ((&V_12)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_71 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___minY_20;
		if ((!(((float)L_70) > ((float)L_71))))
		{
			goto IL_027d;
		}
	}
	{
		Transform_t809 * L_72 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t758  L_73 = Transform_get_position_m4034(L_72, /*hidden argument*/NULL);
		V_13 = L_73;
		float L_74 = ((&V_13)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_75 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___maxY_21;
		Vector3__ctor_m4026((&V_6), L_74, L_75, (0.0f), /*hidden argument*/NULL);
		Transform_t809 * L_76 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_77 = V_6;
		NullCheck(L_76);
		Transform_set_position_m4039(L_76, L_77, /*hidden argument*/NULL);
	}

IL_027d:
	{
		Transform_t809 * L_78 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t758  L_79 = Transform_get_position_m4034(L_78, /*hidden argument*/NULL);
		V_14 = L_79;
		float L_80 = ((&V_14)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_81 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___maxY_21;
		if ((!(((float)L_80) < ((float)L_81))))
		{
			goto IL_02cd;
		}
	}
	{
		Transform_t809 * L_82 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_82);
		Vector3_t758  L_83 = Transform_get_position_m4034(L_82, /*hidden argument*/NULL);
		V_15 = L_83;
		float L_84 = ((&V_15)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_85 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___minY_20;
		Vector3__ctor_m4026((&V_7), L_84, L_85, (0.0f), /*hidden argument*/NULL);
		Transform_t809 * L_86 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_87 = V_7;
		NullCheck(L_86);
		Transform_set_position_m4039(L_86, L_87, /*hidden argument*/NULL);
	}

IL_02cd:
	{
		return;
	}
}
// System.Void playerMovement::rotateShipDirection(enemyController)
extern "C" void playerMovement_rotateShipDirection_m3634 (playerMovement_t830 * __this, enemyController_t785 * ____enemyController, MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t758  V_1 = {0};
	Vector3_t758  V_2 = {0};
	Vector3_t758  V_3 = {0};
	{
		enemyController_t785 * L_0 = ____enemyController;
		Transform_t809 * L_1 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t758  L_2 = Transform_get_position_m4034(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t144 * L_3 = enemyController_getClosest_m3395(L_0, L_2, /*hidden argument*/NULL);
		__this->___closest_8 = L_3;
		GameObject_t144 * L_4 = (__this->___closest_8);
		bool L_5 = Object_op_Implicit_m1059(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0088;
		}
	}
	{
		GameObject_t144 * L_6 = (__this->___closest_8);
		NullCheck(L_6);
		Transform_t809 * L_7 = GameObject_get_transform_m4029(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t758  L_8 = Transform_get_position_m4034(L_7, /*hidden argument*/NULL);
		Vector2_t739  L_9 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		float L_10 = playerMovement_calculateAngle_m3637(__this, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Transform_t809 * L_11 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t758  L_12 = Transform_get_position_m4034(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = ((&V_2)->___x_1);
		Transform_t809 * L_14 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t758  L_15 = Transform_get_position_m4034(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = ((&V_3)->___y_2);
		float L_17 = V_0;
		Vector3__ctor_m4026((&V_1), L_13, L_16, ((float)((float)(57.29578f)*(float)L_17)), /*hidden argument*/NULL);
		Transform_t809 * L_18 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_19 = V_1;
		Quaternion_t771  L_20 = Quaternion_Euler_m4157(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_rotation_m4171(L_18, L_20, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// System.Void playerMovement::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* gameControl_t794_il2cpp_TypeInfo_var;
extern TypeInfo* CoinsManager_t72_il2cpp_TypeInfo_var;
extern TypeInfo* Thread_t993_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t202_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TispaqueteScr_t828_m4274_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TisCoin_t727_m4275_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TistriggerSplashBomb_t836_m4276_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TiscameraScript_t774_m4177_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TisnaveProxyController_t820_m4277_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var;
extern "C" void playerMovement_OnTriggerEnter_m3635 (playerMovement_t830 * __this, Collider_t900 * ___other, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		gameControl_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		CoinsManager_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Thread_t993_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Single_t202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484277);
		GameObject_GetComponent_TispaqueteScr_t828_m4274_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484323);
		Component_GetComponent_TisCoin_t727_m4275_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484324);
		Component_GetComponent_TistriggerSplashBomb_t836_m4276_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484325);
		GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484283);
		Component_GetComponent_TiscameraScript_t774_m4177_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484292);
		Component_GetComponent_TisnaveProxyController_t820_m4277_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484326);
		GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484318);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t144 * V_0 = {0};
	GameObject_t144 * V_1 = {0};
	GameObject_t144 * V_2 = {0};
	{
		Collider_t900 * L_0 = ___other;
		NullCheck(L_0);
		GameObject_t144 * L_1 = Component_get_gameObject_m853(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m4048(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m835(NULL /*static, unused*/, L_2, (String_t*) &_stringLiteral700, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_004e;
		}
	}
	{
		Collider_t900 * L_4 = ___other;
		NullCheck(L_4);
		GameObject_t144 * L_5 = Component_get_gameObject_m853(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m4048(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m835(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral701, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004e;
		}
	}
	{
		Collider_t900 * L_8 = ___other;
		NullCheck(L_8);
		GameObject_t144 * L_9 = Component_get_gameObject_m853(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = GameObject_get_tag_m4048(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m835(NULL /*static, unused*/, L_10, (String_t*) &_stringLiteral792, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006f;
		}
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		int32_t L_12 = ((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6;
		if ((!(((uint32_t)L_12) == ((uint32_t)4))))
		{
			goto IL_006f;
		}
	}
	{
		GameObject_t144 * L_13 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral661, /*hidden argument*/NULL);
		V_0 = L_13;
		GameObject_t144 * L_14 = V_0;
		NullCheck(L_14);
		gameControl_t794 * L_15 = GameObject_GetComponent_TisgameControl_t794_m4062(L_14, /*hidden argument*/GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var);
		NullCheck(L_15);
		gameControl_finishGame_m3460(L_15, /*hidden argument*/NULL);
	}

IL_006f:
	{
		Collider_t900 * L_16 = ___other;
		NullCheck(L_16);
		GameObject_t144 * L_17 = Component_get_gameObject_m853(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = GameObject_get_tag_m4048(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m835(NULL /*static, unused*/, L_18, (String_t*) &_stringLiteral740, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00ae;
		}
	}
	{
		Collider_t900 * L_20 = ___other;
		NullCheck(L_20);
		GameObject_t144 * L_21 = Component_get_gameObject_m853(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		paqueteScr_t828 * L_22 = GameObject_GetComponent_TispaqueteScr_t828_m4274(L_21, /*hidden argument*/GameObject_GetComponent_TispaqueteScr_t828_m4274_MethodInfo_var);
		NullCheck(L_22);
		paqueteScr_triggerAnim_m3625(L_22, /*hidden argument*/NULL);
		SoundManager_playPillarPaqueteClip_m3281(NULL /*static, unused*/, /*hidden argument*/NULL);
		Collider_t900 * L_23 = ___other;
		NullCheck(L_23);
		GameObject_t144 * L_24 = Component_get_gameObject_m853(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		paqueteScr_t828 * L_25 = GameObject_GetComponent_TispaqueteScr_t828_m4274(L_24, /*hidden argument*/GameObject_GetComponent_TispaqueteScr_t828_m4274_MethodInfo_var);
		NullCheck(L_25);
		paqueteScr_giveBullets_m3624(L_25, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		Collider_t900 * L_26 = ___other;
		NullCheck(L_26);
		GameObject_t144 * L_27 = Component_get_gameObject_m853(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_28 = GameObject_get_tag_m4048(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m835(NULL /*static, unused*/, L_28, (String_t*) &_stringLiteral632, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00f0;
		}
	}
	{
		SoundManager_playPillarPaqueteClip_m3281(NULL /*static, unused*/, /*hidden argument*/NULL);
		Collider_t900 * L_30 = ___other;
		NullCheck(L_30);
		Coin_t727 * L_31 = Component_GetComponent_TisCoin_t727_m4275(L_30, /*hidden argument*/Component_GetComponent_TisCoin_t727_m4275_MethodInfo_var);
		NullCheck(L_31);
		int32_t L_32 = (L_31->___coinTypeVar_3);
		IL2CPP_RUNTIME_CLASS_INIT(CoinsManager_t72_il2cpp_TypeInfo_var);
		CoinsManager_addCoins_m263(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		Collider_t900 * L_33 = ___other;
		NullCheck(L_33);
		GameObject_t144 * L_34 = Component_get_gameObject_m853(L_33, /*hidden argument*/NULL);
		Object_t * L_35 = playerMovement_moveCoin_m3636(__this, L_34, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m968(__this, L_35, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		Collider_t900 * L_36 = ___other;
		NullCheck(L_36);
		GameObject_t144 * L_37 = Component_get_gameObject_m853(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_38 = GameObject_get_tag_m4048(L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_39 = String_op_Equality_m835(NULL /*static, unused*/, L_38, (String_t*) &_stringLiteral739, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0175;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t993_il2cpp_TypeInfo_var);
		Thread_Sleep_m4163(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		SoundManager_playBoomClip_m3277(NULL /*static, unused*/, /*hidden argument*/NULL);
		triggerSplashBomb_t836 * L_40 = Component_GetComponent_TistriggerSplashBomb_t836_m4276(__this, /*hidden argument*/Component_GetComponent_TistriggerSplashBomb_t836_m4276_MethodInfo_var);
		NullCheck(L_40);
		triggerSplashBomb_triggerSplash_m3655(L_40, /*hidden argument*/NULL);
		GameObject_t144 * L_41 = (__this->___enemyController_5);
		NullCheck(L_41);
		enemyController_t785 * L_42 = GameObject_GetComponent_TisenemyController_t785_m4130(L_41, /*hidden argument*/GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var);
		NullCheck(L_42);
		enemyController_clearEnemies_m3393(L_42, /*hidden argument*/NULL);
		Camera_t978 * L_43 = Camera_get_main_m4021(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_43);
		cameraScript_t774 * L_44 = Component_GetComponent_TiscameraScript_t774_m4177(L_43, /*hidden argument*/Component_GetComponent_TiscameraScript_t774_m4177_MethodInfo_var);
		float L_45 = (1.0f);
		Object_t * L_46 = Box(Single_t202_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_44);
		MonoBehaviour_StartCoroutine_m4178(L_44, (String_t*) &_stringLiteral730, L_46, /*hidden argument*/NULL);
		Collider_t900 * L_47 = ___other;
		NullCheck(L_47);
		GameObject_t144 * L_48 = Component_get_gameObject_m853(L_47, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Transform_t809 * L_49 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		Vector3_t758  L_50 = Transform_get_position_m4034(L_49, /*hidden argument*/NULL);
		Vector2_t739  L_51 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		SoundManager_playBombaAmarillaClip_m3267(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		SoundManager_playXplosionsWoman_m3285(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0175:
	{
		Collider_t900 * L_52 = ___other;
		NullCheck(L_52);
		GameObject_t144 * L_53 = Component_get_gameObject_m853(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		String_t* L_54 = GameObject_get_tag_m4048(L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m835(NULL /*static, unused*/, L_54, (String_t*) &_stringLiteral738, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_01e5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t993_il2cpp_TypeInfo_var);
		Thread_Sleep_m4163(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		Transform_t809 * L_56 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_t758  L_57 = Transform_get_position_m4034(L_56, /*hidden argument*/NULL);
		Vector2_t739  L_58 = Vector2_op_Implicit_m4035(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		SoundManager_playBombaAmarillaClip_m3267(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		triggerSplashBomb_t836 * L_59 = Component_GetComponent_TistriggerSplashBomb_t836_m4276(__this, /*hidden argument*/Component_GetComponent_TistriggerSplashBomb_t836_m4276_MethodInfo_var);
		NullCheck(L_59);
		triggerSplashBomb_rayobomba_m3656(L_59, /*hidden argument*/NULL);
		Camera_t978 * L_60 = Camera_get_main_m4021(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_60);
		cameraScript_t774 * L_61 = Component_GetComponent_TiscameraScript_t774_m4177(L_60, /*hidden argument*/Component_GetComponent_TiscameraScript_t774_m4177_MethodInfo_var);
		float L_62 = (10.0f);
		Object_t * L_63 = Box(Single_t202_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_61);
		MonoBehaviour_StartCoroutine_m4178(L_61, (String_t*) &_stringLiteral724, L_63, /*hidden argument*/NULL);
		SoundManager_playRaysWoman_m3286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Collider_t900 * L_64 = ___other;
		NullCheck(L_64);
		GameObject_t144 * L_65 = Component_get_gameObject_m853(L_64, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
	}

IL_01e5:
	{
		Collider_t900 * L_66 = ___other;
		NullCheck(L_66);
		GameObject_t144 * L_67 = Component_get_gameObject_m853(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		String_t* L_68 = GameObject_get_tag_m4048(L_67, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_69 = String_op_Equality_m835(NULL /*static, unused*/, L_68, (String_t*) &_stringLiteral727, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_021a;
		}
	}
	{
		Collider_t900 * L_70 = ___other;
		NullCheck(L_70);
		naveProxyController_t820 * L_71 = Component_GetComponent_TisnaveProxyController_t820_m4277(L_70, /*hidden argument*/Component_GetComponent_TisnaveProxyController_t820_m4277_MethodInfo_var);
		NullCheck(L_71);
		naveProxyController_setWorking_m3592(L_71, /*hidden argument*/NULL);
		GameObject_t144 * L_72 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_72);
		gizmosProxy_t804 * L_73 = GameObject_GetComponent_TisgizmosProxy_t804_m4263(L_72, /*hidden argument*/GameObject_GetComponent_TisgizmosProxy_t804_m4263_MethodInfo_var);
		NullCheck(L_73);
		gizmosProxy_createGizmos_m3510(L_73, /*hidden argument*/NULL);
	}

IL_021a:
	{
		Collider_t900 * L_74 = ___other;
		NullCheck(L_74);
		GameObject_t144 * L_75 = Component_get_gameObject_m853(L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		String_t* L_76 = GameObject_get_tag_m4048(L_75, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_77 = String_op_Equality_m835(NULL /*static, unused*/, L_76, (String_t*) &_stringLiteral700, /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_0255;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		int32_t L_78 = ((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6;
		if ((!(((uint32_t)L_78) == ((uint32_t)8))))
		{
			goto IL_0255;
		}
	}
	{
		GameObject_t144 * L_79 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral661, /*hidden argument*/NULL);
		V_1 = L_79;
		GameObject_t144 * L_80 = V_1;
		NullCheck(L_80);
		gameControl_t794 * L_81 = GameObject_GetComponent_TisgameControl_t794_m4062(L_80, /*hidden argument*/GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var);
		NullCheck(L_81);
		gameControl_failingTutorial_m3486(L_81, /*hidden argument*/NULL);
	}

IL_0255:
	{
		Collider_t900 * L_82 = ___other;
		NullCheck(L_82);
		GameObject_t144 * L_83 = Component_get_gameObject_m853(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		String_t* L_84 = GameObject_get_tag_m4048(L_83, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_85 = String_op_Equality_m835(NULL /*static, unused*/, L_84, (String_t*) &_stringLiteral701, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_0290;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		int32_t L_86 = ((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6;
		if ((!(((uint32_t)L_86) == ((uint32_t)8))))
		{
			goto IL_0290;
		}
	}
	{
		GameObject_t144 * L_87 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral661, /*hidden argument*/NULL);
		V_2 = L_87;
		GameObject_t144 * L_88 = V_2;
		NullCheck(L_88);
		gameControl_t794 * L_89 = GameObject_GetComponent_TisgameControl_t794_m4062(L_88, /*hidden argument*/GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var);
		NullCheck(L_89);
		gameControl_failingTutorial_m3486(L_89, /*hidden argument*/NULL);
	}

IL_0290:
	{
		return;
	}
}
// System.Collections.IEnumerator playerMovement::moveCoin(UnityEngine.GameObject)
extern TypeInfo* U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo_var;
extern "C" Object_t * playerMovement_moveCoin_m3636 (playerMovement_t830 * __this, GameObject_t144 * ___coin, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1062);
		s_Il2CppMethodIntialized = true;
	}
	U3CmoveCoinU3Ec__Iterator13_t829 * V_0 = {0};
	{
		U3CmoveCoinU3Ec__Iterator13_t829 * L_0 = (U3CmoveCoinU3Ec__Iterator13_t829 *)il2cpp_codegen_object_new (U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo_var);
		U3CmoveCoinU3Ec__Iterator13__ctor_m3626(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CmoveCoinU3Ec__Iterator13_t829 * L_1 = V_0;
		GameObject_t144 * L_2 = ___coin;
		NullCheck(L_1);
		L_1->___coin_1 = L_2;
		U3CmoveCoinU3Ec__Iterator13_t829 * L_3 = V_0;
		GameObject_t144 * L_4 = ___coin;
		NullCheck(L_3);
		L_3->___U3CU24U3Ecoin_5 = L_4;
		U3CmoveCoinU3Ec__Iterator13_t829 * L_5 = V_0;
		return L_5;
	}
}
// System.Single playerMovement::calculateAngle(UnityEngine.Vector2)
extern TypeInfo* Mathf_t980_il2cpp_TypeInfo_var;
extern "C" float playerMovement_calculateAngle_m3637 (playerMovement_t830 * __this, Vector2_t739  ___target, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t980_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(981);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t758  V_5 = {0};
	Vector3_t758  V_6 = {0};
	{
		Transform_t809 * L_0 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t758  L_1 = Transform_get_position_m4034(L_0, /*hidden argument*/NULL);
		V_5 = L_1;
		float L_2 = ((&V_5)->___y_2);
		V_0 = L_2;
		Transform_t809 * L_3 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t758  L_4 = Transform_get_position_m4034(L_3, /*hidden argument*/NULL);
		V_6 = L_4;
		float L_5 = ((&V_6)->___x_1);
		V_1 = L_5;
		float L_6 = ((&___target)->___x_1);
		V_2 = L_6;
		float L_7 = ((&___target)->___y_2);
		V_3 = L_7;
		float L_8 = V_3;
		float L_9 = V_0;
		float L_10 = V_2;
		float L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t980_il2cpp_TypeInfo_var);
		float L_12 = atan2f(((float)((float)L_8-(float)L_9)), ((float)((float)L_10-(float)L_11)));
		V_4 = L_12;
		float L_13 = V_4;
		return L_13;
	}
}
// playerState
#include "AssemblyU2DCSharp_playerState.h"
#ifndef _MSC_VER
#else
#endif
// playerState
#include "AssemblyU2DCSharp_playerStateMethodDeclarations.h"



// System.Void playerState::.ctor()
extern "C" void playerState__ctor_m3638 (playerState_t831 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void playerState::Start()
extern "C" void playerState_Start_m3639 (playerState_t831 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void playerState::Update()
extern "C" void playerState_Update_m3640 (playerState_t831 * __this, MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// randomColorScr
#include "AssemblyU2DCSharp_randomColorScrMethodDeclarations.h"

// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"


// System.Void randomColorScr::.ctor()
extern "C" void randomColorScr__ctor_m3641 (randomColorScr_t832 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void randomColorScr::Start()
extern MethodInfo* Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var;
extern "C" void randomColorScr_Start_m3642 (randomColorScr_t832 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484280);
		s_Il2CppMethodIntialized = true;
	}
	{
		SpriteRenderer_t744 * L_0 = Component_GetComponent_TisSpriteRenderer_t744_m4109(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var);
		NullCheck(L_0);
		Color_t747  L_1 = SpriteRenderer_get_color_m4281(L_0, /*hidden argument*/NULL);
		__this->___c_2 = L_1;
		return;
	}
}
// System.Void randomColorScr::Update()
extern MethodInfo* Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var;
extern "C" void randomColorScr_Update_m3643 (randomColorScr_t832 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484280);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t747 * L_0 = &(__this->___c_2);
		float L_1 = Random_Range_m4128(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		L_0->___r_0 = L_1;
		Color_t747 * L_2 = &(__this->___c_2);
		float L_3 = Random_Range_m4128(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		L_2->___g_1 = L_3;
		Color_t747 * L_4 = &(__this->___c_2);
		float L_5 = Random_Range_m4128(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		L_4->___b_2 = L_5;
		SpriteRenderer_t744 * L_6 = Component_GetComponent_TisSpriteRenderer_t744_m4109(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t744_m4109_MethodInfo_var);
		Color_t747  L_7 = (__this->___c_2);
		NullCheck(L_6);
		SpriteRenderer_set_color_m4213(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// rotateItself
#include "AssemblyU2DCSharp_rotateItself.h"
#ifndef _MSC_VER
#else
#endif
// rotateItself
#include "AssemblyU2DCSharp_rotateItselfMethodDeclarations.h"



// System.Void rotateItself::.ctor()
extern "C" void rotateItself__ctor_m3644 (rotateItself_t833 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void rotateItself::Start()
extern "C" void rotateItself_Start_m3645 (rotateItself_t833 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void rotateItself::Update()
extern "C" void rotateItself_Update_m3646 (rotateItself_t833 * __this, MethodInfo* method)
{
	{
		Transform_t809 * L_0 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		Vector3_t758  L_1 = (__this->___dir_2);
		NullCheck(L_0);
		Transform_Rotate_m4259(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// songsBatch
#include "AssemblyU2DCSharp_songsBatch.h"
#ifndef _MSC_VER
#else
#endif
// songsBatch
#include "AssemblyU2DCSharp_songsBatchMethodDeclarations.h"

// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayList.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
struct Component_t230;
struct AudioSource_t755;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t755_m4143(__this, method) (( AudioSource_t755 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)


// System.Void songsBatch::.ctor()
extern TypeInfo* ArrayList_t737_il2cpp_TypeInfo_var;
extern "C" void songsBatch__ctor_m3647 (songsBatch_t834 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ArrayList_t737_il2cpp_TypeInfo_var);
		ArrayList_t737 * L_0 = (ArrayList_t737 *)il2cpp_codegen_object_new (ArrayList_t737_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4063(L_0, /*hidden argument*/NULL);
		__this->___songs_2 = L_0;
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void songsBatch::Awake()
extern MethodInfo* Component_GetComponent_TisAudioSource_t755_m4143_MethodInfo_var;
extern "C" void songsBatch_Awake_m3648 (songsBatch_t834 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAudioSource_t755_m4143_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484284);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t755 * L_0 = Component_GetComponent_TisAudioSource_t755_m4143(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t755_m4143_MethodInfo_var);
		__this->___musicPlayer_9 = L_0;
		ArrayList_t737 * L_1 = (__this->___songs_2);
		AudioClip_t753 * L_2 = (__this->___track_1_4);
		NullCheck(L_1);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(27 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_1, L_2);
		ArrayList_t737 * L_3 = (__this->___songs_2);
		AudioClip_t753 * L_4 = (__this->___track_2_5);
		NullCheck(L_3);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(27 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_3, L_4);
		ArrayList_t737 * L_5 = (__this->___songs_2);
		AudioClip_t753 * L_6 = (__this->___track_3_6);
		NullCheck(L_5);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(27 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_5, L_6);
		ArrayList_t737 * L_7 = (__this->___songs_2);
		AudioClip_t753 * L_8 = (__this->___track_4_7);
		NullCheck(L_7);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(27 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_7, L_8);
		ArrayList_t737 * L_9 = (__this->___songs_2);
		AudioClip_t753 * L_10 = (__this->___track_5_8);
		NullCheck(L_9);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(27 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_9, L_10);
		songsBatch_selectSong_m3649(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void songsBatch::selectSong()
extern TypeInfo* AudioClip_t753_il2cpp_TypeInfo_var;
extern "C" void songsBatch_selectSong_m3649 (songsBatch_t834 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioClip_t753_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t755 * L_0 = (__this->___musicPlayer_9);
		ArrayList_t737 * L_1 = (__this->___songs_2);
		ArrayList_t737 * L_2 = (__this->___songs_2);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_2);
		int32_t L_4 = Random_Range_m4046(NULL /*static, unused*/, 0, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(20 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, L_4);
		NullCheck(L_0);
		AudioSource_set_clip_m4282(L_0, ((AudioClip_t753 *)IsInst(L_5, AudioClip_t753_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		AudioSource_t755 * L_6 = (__this->___musicPlayer_9);
		NullCheck(L_6);
		AudioSource_Play_m4283(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void songsBatch::switcherButton()
extern "C" void songsBatch_switcherButton_m3650 (songsBatch_t834 * __this, MethodInfo* method)
{
	{
		AudioSource_t755 * L_0 = (__this->___musicPlayer_9);
		AudioClip_t753 * L_1 = (__this->___switcherSound_3);
		NullCheck(L_0);
		AudioSource_PlayOneShot_m4146(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// tilePrefabScr
#include "AssemblyU2DCSharp_tilePrefabScr.h"
#ifndef _MSC_VER
#else
#endif
// tilePrefabScr
#include "AssemblyU2DCSharp_tilePrefabScrMethodDeclarations.h"

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
struct GameObject_t144;
struct Animator_t749;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t749_m4220(__this, method) (( Animator_t749 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)


// System.Void tilePrefabScr::.ctor()
extern "C" void tilePrefabScr__ctor_m3651 (tilePrefabScr_t835 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void tilePrefabScr::Awake()
extern "C" void tilePrefabScr_Awake_m3652 (tilePrefabScr_t835 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = Random_Range_m4046(NULL /*static, unused*/, 0, ((int32_t)60), /*hidden argument*/NULL);
		__this->___t_2 = L_0;
		return;
	}
}
// System.Void tilePrefabScr::Update()
extern MethodInfo* GameObject_GetComponent_TisAnimator_t749_m4220_MethodInfo_var;
extern "C" void tilePrefabScr_Update_m3653 (tilePrefabScr_t835 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisAnimator_t749_m4220_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484308);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___t_2);
		__this->___t_2 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = (__this->___t_2);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)30))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_2 = (__this->___t_2);
		if ((((int32_t)L_2) >= ((int32_t)((int32_t)60))))
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t144 * L_3 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Animator_t749 * L_4 = GameObject_GetComponent_TisAnimator_t749_m4220(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t749_m4220_MethodInfo_var);
		float L_5 = Random_Range_m4128(NULL /*static, unused*/, (0.0f), (0.2f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Animator_set_speed_m4129(L_4, L_5, /*hidden argument*/NULL);
		__this->___t_2 = 0;
	}

IL_004e:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// RayoScr
#include "AssemblyU2DCSharp_RayoScr.h"
struct GameObject_t144;
struct RayoScr_t752;
// Declaration !!0 UnityEngine.GameObject::GetComponent<RayoScr>()
// !!0 UnityEngine.GameObject::GetComponent<RayoScr>()
#define GameObject_GetComponent_TisRayoScr_t752_m4284(__this, method) (( RayoScr_t752 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponent_TisObject_t_m4017_gshared)(__this, method)


// System.Void triggerSplashBomb::.ctor()
extern "C" void triggerSplashBomb__ctor_m3654 (triggerSplashBomb_t836 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void triggerSplashBomb::triggerSplash()
extern "C" void triggerSplashBomb_triggerSplash_m3655 (triggerSplashBomb_t836 * __this, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = (__this->___bombaSplash_2);
		Transform_t809 * L_1 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t758  L_2 = Transform_get_position_m4034(L_1, /*hidden argument*/NULL);
		Quaternion_t771  L_3 = Quaternion_get_identity_m4027(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m4028(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void triggerSplashBomb::rayobomba()
extern TypeInfo* GameObject_t144_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_GetComponent_TisRayoScr_t752_m4284_MethodInfo_var;
extern "C" void triggerSplashBomb_rayobomba_m3656 (triggerSplashBomb_t836 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		GameObject_GetComponent_TisRayoScr_t752_m4284_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484327);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t977* V_0 = {0};
	GameObjectU5BU5D_t977* V_1 = {0};
	GameObject_t144 * V_2 = {0};
	GameObjectU5BU5D_t977* V_3 = {0};
	int32_t V_4 = 0;
	GameObject_t144 * V_5 = {0};
	GameObject_t144 * V_6 = {0};
	GameObjectU5BU5D_t977* V_7 = {0};
	int32_t V_8 = 0;
	GameObject_t144 * V_9 = {0};
	{
		GameObjectU5BU5D_t977* L_0 = GameObject_FindGameObjectsWithTag_m4018(NULL /*static, unused*/, (String_t*) &_stringLiteral700, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObjectU5BU5D_t977* L_1 = GameObject_FindGameObjectsWithTag_m4018(NULL /*static, unused*/, (String_t*) &_stringLiteral701, /*hidden argument*/NULL);
		V_1 = L_1;
		GameObjectU5BU5D_t977* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_0072;
	}

IL_0020:
	{
		GameObjectU5BU5D_t977* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = (*(GameObject_t144 **)(GameObject_t144 **)SZArrayLdElema(L_3, L_5));
		GameObject_t144 * L_6 = (__this->___rayo_4);
		Transform_t809 * L_7 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t758  L_8 = Transform_get_position_m4034(L_7, /*hidden argument*/NULL);
		Transform_t809 * L_9 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t771  L_10 = Transform_get_rotation_m4131(L_9, /*hidden argument*/NULL);
		Object_t187 * L_11 = Object_Instantiate_m4028(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		V_5 = ((GameObject_t144 *)IsInst(L_11, GameObject_t144_il2cpp_TypeInfo_var));
		GameObject_t144 * L_12 = V_5;
		NullCheck(L_12);
		RayoScr_t752 * L_13 = GameObject_GetComponent_TisRayoScr_t752_m4284(L_12, /*hidden argument*/GameObject_GetComponent_TisRayoScr_t752_m4284_MethodInfo_var);
		GameObject_t144 * L_14 = V_2;
		NullCheck(L_13);
		L_13->___target_3 = L_14;
		GameObject_t144 * L_15 = V_5;
		NullCheck(L_15);
		RayoScr_t752 * L_16 = GameObject_GetComponent_TisRayoScr_t752_m4284(L_15, /*hidden argument*/GameObject_GetComponent_TisRayoScr_t752_m4284_MethodInfo_var);
		int32_t L_17 = (__this->___timerRays_6);
		NullCheck(L_16);
		L_16->___timer_5 = L_17;
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_19 = V_4;
		GameObjectU5BU5D_t977* L_20 = V_3;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)(((Array_t *)L_20)->max_length))))))
		{
			goto IL_0020;
		}
	}
	{
		GameObjectU5BU5D_t977* L_21 = V_1;
		V_7 = L_21;
		V_8 = 0;
		goto IL_00dc;
	}

IL_0087:
	{
		GameObjectU5BU5D_t977* L_22 = V_7;
		int32_t L_23 = V_8;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		V_6 = (*(GameObject_t144 **)(GameObject_t144 **)SZArrayLdElema(L_22, L_24));
		GameObject_t144 * L_25 = (__this->___rayo_4);
		Transform_t809 * L_26 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t758  L_27 = Transform_get_position_m4034(L_26, /*hidden argument*/NULL);
		Transform_t809 * L_28 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Quaternion_t771  L_29 = Transform_get_rotation_m4131(L_28, /*hidden argument*/NULL);
		Object_t187 * L_30 = Object_Instantiate_m4028(NULL /*static, unused*/, L_25, L_27, L_29, /*hidden argument*/NULL);
		V_9 = ((GameObject_t144 *)IsInst(L_30, GameObject_t144_il2cpp_TypeInfo_var));
		GameObject_t144 * L_31 = V_9;
		NullCheck(L_31);
		RayoScr_t752 * L_32 = GameObject_GetComponent_TisRayoScr_t752_m4284(L_31, /*hidden argument*/GameObject_GetComponent_TisRayoScr_t752_m4284_MethodInfo_var);
		GameObject_t144 * L_33 = V_6;
		NullCheck(L_32);
		L_32->___target_3 = L_33;
		GameObject_t144 * L_34 = V_9;
		NullCheck(L_34);
		RayoScr_t752 * L_35 = GameObject_GetComponent_TisRayoScr_t752_m4284(L_34, /*hidden argument*/GameObject_GetComponent_TisRayoScr_t752_m4284_MethodInfo_var);
		int32_t L_36 = (__this->___timerRays_6);
		NullCheck(L_35);
		L_35->___timer_5 = L_36;
		int32_t L_37 = V_8;
		V_8 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00dc:
	{
		int32_t L_38 = V_8;
		GameObjectU5BU5D_t977* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)(((Array_t *)L_39)->max_length))))))
		{
			goto IL_0087;
		}
	}
	{
		return;
	}
}
// tutorialController
#include "AssemblyU2DCSharp_tutorialController.h"
#ifndef _MSC_VER
#else
#endif
// tutorialController
#include "AssemblyU2DCSharp_tutorialControllerMethodDeclarations.h"

// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_SpriteMethodDeclarations.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
struct GameObject_t144;
struct SpriteRenderer_t744;
struct GameObject_t144;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C" Object_t * GameObject_GetComponentInChildren_TisObject_t_m4210_gshared (GameObject_t144 * __this, MethodInfo* method);
#define GameObject_GetComponentInChildren_TisObject_t_m4210(__this, method) (( Object_t * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponentInChildren_TisObject_t_m4210_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.SpriteRenderer>()
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponentInChildren_TisSpriteRenderer_t744_m4209(__this, method) (( SpriteRenderer_t744 * (*) (GameObject_t144 *, MethodInfo*))GameObject_GetComponentInChildren_TisObject_t_m4210_gshared)(__this, method)


// System.Void tutorialController::.ctor()
extern "C" void tutorialController__ctor_m3657 (tutorialController_t837 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void tutorialController::Start()
extern TypeInfo* GameObject_t144_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t744_m4209_MethodInfo_var;
extern MethodInfo* GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var;
extern "C" void tutorialController_Start_m3658 (tutorialController_t837 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		GameObject_GetComponentInChildren_TisSpriteRenderer_t744_m4209_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484301);
		GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484283);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t979  V_0 = {0};
	Vector3_t758  V_1 = {0};
	{
		GameObject_t144 * L_0 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral338, /*hidden argument*/NULL);
		__this->___player_9 = L_0;
		GameObject_t144 * L_1 = (__this->___player_9);
		NullCheck(L_1);
		SpriteRenderer_t744 * L_2 = GameObject_GetComponentInChildren_TisSpriteRenderer_t744_m4209(L_1, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t744_m4209_MethodInfo_var);
		NullCheck(L_2);
		Sprite_t766 * L_3 = SpriteRenderer_get_sprite_m4186(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Bounds_t979  L_4 = Sprite_get_bounds_m4187(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t758  L_5 = Bounds_get_size_m4020((&V_0), /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = ((&V_1)->___y_2);
		__this->___size_12 = ((float)((float)L_6*(float)(2.5f)));
		GameObject_t144 * L_7 = (__this->___bocadillo_16);
		Vector2_t739  L_8 = (__this->___bocaPos_11);
		Vector3_t758  L_9 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Quaternion_t771  L_10 = Quaternion_get_identity_m4027(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t187 * L_11 = Object_Instantiate_m4028(NULL /*static, unused*/, L_7, L_9, L_10, /*hidden argument*/NULL);
		__this->___bocadilloObj_10 = ((GameObject_t144 *)IsInst(L_11, GameObject_t144_il2cpp_TypeInfo_var));
		GameObject_t144 * L_12 = (__this->___bocadilloObj_10);
		NullCheck(L_12);
		Transform_t809 * L_13 = GameObject_get_transform_m4029(L_12, /*hidden argument*/NULL);
		Transform_t809 * L_14 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_parent_m4031(L_13, L_14, /*hidden argument*/NULL);
		GameObject_t144 * L_15 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral723, /*hidden argument*/NULL);
		__this->___enemyControllerObj_7 = L_15;
		GameObject_t144 * L_16 = (__this->___enemyControllerObj_7);
		NullCheck(L_16);
		enemyController_t785 * L_17 = GameObject_GetComponent_TisenemyController_t785_m4130(L_16, /*hidden argument*/GameObject_GetComponent_TisenemyController_t785_m4130_MethodInfo_var);
		__this->____enemyController_8 = L_17;
		return;
	}
}
// System.Void tutorialController::Update()
extern TypeInfo* gameControl_t794_il2cpp_TypeInfo_var;
extern "C" void tutorialController_Update_m3659 (tutorialController_t837 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		gameControl_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t758  V_0 = {0};
	Vector3_t758  V_1 = {0};
	{
		GameObject_t144 * L_0 = (__this->___player_9);
		bool L_1 = Object_op_Implicit_m1059(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		int32_t L_2 = ((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6;
		if ((!(((uint32_t)L_2) == ((uint32_t)8))))
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		bool L_3 = ((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___slowMotion_46;
		if (L_3)
		{
			goto IL_009a;
		}
	}
	{
		GameObject_t144 * L_4 = (__this->___bocadilloObj_10);
		NullCheck(L_4);
		GameObject_SetActive_m4272(L_4, 1, /*hidden argument*/NULL);
		GameObject_t144 * L_5 = (__this->___player_9);
		NullCheck(L_5);
		Transform_t809 * L_6 = GameObject_get_transform_m4029(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t758  L_7 = Transform_get_position_m4034(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		float L_8 = ((&V_0)->___x_1);
		float L_9 = (__this->___size_12);
		GameObject_t144 * L_10 = (__this->___player_9);
		NullCheck(L_10);
		Transform_t809 * L_11 = GameObject_get_transform_m4029(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t758  L_12 = Transform_get_position_m4034(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = ((&V_1)->___y_2);
		float L_14 = (__this->___size_12);
		Vector2_t739  L_15 = {0};
		Vector2__ctor_m4033(&L_15, ((float)((float)L_8+(float)L_9)), ((float)((float)L_13+(float)L_14)), /*hidden argument*/NULL);
		__this->___bocaPos_11 = L_15;
		GameObject_t144 * L_16 = (__this->___bocadilloObj_10);
		NullCheck(L_16);
		Transform_t809 * L_17 = GameObject_get_transform_m4029(L_16, /*hidden argument*/NULL);
		Vector2_t739  L_18 = (__this->___bocaPos_11);
		Vector3_t758  L_19 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m4039(L_17, L_19, /*hidden argument*/NULL);
		goto IL_00a6;
	}

IL_009a:
	{
		GameObject_t144 * L_20 = (__this->___bocadilloObj_10);
		NullCheck(L_20);
		GameObject_SetActive_m4272(L_20, 0, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		return;
	}
}
// tutorialPod
#include "AssemblyU2DCSharp_tutorialPod.h"
#ifndef _MSC_VER
#else
#endif
// tutorialPod
#include "AssemblyU2DCSharp_tutorialPodMethodDeclarations.h"

struct Component_t230;
struct Animator_t749;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t749_m4127(__this, method) (( Animator_t749 * (*) (Component_t230 *, MethodInfo*))Component_GetComponent_TisObject_t_m4012_gshared)(__this, method)


// System.Void tutorialPod::.ctor()
extern "C" void tutorialPod__ctor_m3660 (tutorialPod_t838 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void tutorialPod::Start()
extern MethodInfo* Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160_MethodInfo_var;
extern MethodInfo* Component_GetComponent_TisAnimator_t749_m4127_MethodInfo_var;
extern "C" void tutorialPod_Start_m3661 (tutorialPod_t838 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484287);
		Component_GetComponent_TisAnimator_t749_m4127_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484282);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___touched_3 = 0;
		SpriteRenderer_t744 * L_0 = Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160(__this, /*hidden argument*/Component_GetComponentInChildren_TisSpriteRenderer_t744_m4160_MethodInfo_var);
		Color_t747  L_1 = Color_get_grey_m4236(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_set_color_m4213(L_0, L_1, /*hidden argument*/NULL);
		Animator_t749 * L_2 = Component_GetComponent_TisAnimator_t749_m4127(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t749_m4127_MethodInfo_var);
		NullCheck(L_2);
		Animator_Play_m4214(L_2, (String_t*) &_stringLiteral793, /*hidden argument*/NULL);
		return;
	}
}
// System.Void tutorialPod::OnTriggerEnter(UnityEngine.Collider)
extern "C" void tutorialPod_OnTriggerEnter_m3662 (tutorialPod_t838 * __this, Collider_t900 * ___other, MethodInfo* method)
{
	{
		return;
	}
}
// weaponIcon
#include "AssemblyU2DCSharp_weaponIcon.h"
#ifndef _MSC_VER
#else
#endif
// weaponIcon
#include "AssemblyU2DCSharp_weaponIconMethodDeclarations.h"



// System.Void weaponIcon::.ctor()
extern "C" void weaponIcon__ctor_m3663 (weaponIcon_t839 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void weaponIcon::Start()
extern "C" void weaponIcon_Start_m3664 (weaponIcon_t839 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void weaponIcon::Update()
extern "C" void weaponIcon_Update_m3665 (weaponIcon_t839 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// weaponRoom
#include "AssemblyU2DCSharp_weaponRoom.h"
#ifndef _MSC_VER
#else
#endif
// weaponRoom
#include "AssemblyU2DCSharp_weaponRoomMethodDeclarations.h"



// System.Void weaponRoom::.ctor()
extern TypeInfo* GUIStyle_t724_il2cpp_TypeInfo_var;
extern "C" void weaponRoom__ctor_m3666 (weaponRoom_t840 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle_t724 * L_0 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_0, /*hidden argument*/NULL);
		__this->___gearWordSt_10 = L_0;
		GUIStyle_t724 * L_1 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_1, /*hidden argument*/NULL);
		__this->___weaponsRoomSt_11 = L_1;
		GUIStyle_t724 * L_2 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_2, /*hidden argument*/NULL);
		__this->___lowBarButtonSt_12 = L_2;
		GUIStyle_t724 * L_3 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_3, /*hidden argument*/NULL);
		__this->___lowBarPlaySt_13 = L_3;
		GUIStyle_t724 * L_4 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_4, /*hidden argument*/NULL);
		__this->___levelSt_14 = L_4;
		GUIStyle_t724 * L_5 = (GUIStyle_t724 *)il2cpp_codegen_object_new (GUIStyle_t724_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4032(L_5, /*hidden argument*/NULL);
		__this->___titleSt_15 = L_5;
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void weaponRoom::Start()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* WeaponsController_t109_il2cpp_TypeInfo_var;
extern "C" void weaponRoom_Start_m3667 (weaponRoom_t840 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		WeaponsController_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_0 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_1 = (__this->___offsetForStripe_4);
		Rect_t738  L_2 = {0};
		Rect__ctor_m4041(&L_2, (0.0f), ((float)((float)L_0/(float)L_1)), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___boxRect_8 = L_2;
		weaponRoom_setSizes_m3669(__this, /*hidden argument*/NULL);
		bool L_3 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___ISWIDE_27;
		__this->___screenPos_20 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(WeaponsController_t109_il2cpp_TypeInfo_var);
		WeaponsController_updateWeapons_m510(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void weaponRoom::Update()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern "C" void weaponRoom_Update_m3668 (weaponRoom_t840 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___screenPos_20);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		bool L_1 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___ISWIDE_27;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0016;
		}
	}
	{
		weaponRoom_setSizes_m3669(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void weaponRoom::setSizes()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern "C" void weaponRoom_setSizes_m3669 (weaponRoom_t840 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIStyle_t724 * L_0 = (__this->___gearWordSt_10);
		NullCheck(L_0);
		GUIStyle_set_fontSize_m4045(L_0, ((int32_t)22), /*hidden argument*/NULL);
		GUIStyle_t724 * L_1 = (__this->___weaponsRoomSt_11);
		NullCheck(L_1);
		GUIStyle_set_fontSize_m4045(L_1, ((int32_t)22), /*hidden argument*/NULL);
		GUIStyle_t724 * L_2 = (__this->___lowBarButtonSt_12);
		V_0 = ((int32_t)21);
		GUIStyle_t724 * L_3 = (__this->___lowBarPlaySt_13);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		GUIStyle_set_fontSize_m4045(L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		NullCheck(L_2);
		GUIStyle_set_fontSize_m4045(L_2, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		bool L_6 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___ISWIDE_27;
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t724 * L_7 = (__this->___titleSt_15);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_8 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		NullCheck(L_7);
		GUIStyle_set_fontSize_m4045(L_7, ((int32_t)((int32_t)(((int32_t)L_8))/(int32_t)((int32_t)20))), /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_0058:
	{
		GUIStyle_t724 * L_9 = (__this->___titleSt_15);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_10 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		NullCheck(L_9);
		GUIStyle_set_fontSize_m4045(L_9, ((int32_t)((int32_t)(((int32_t)L_10))/(int32_t)((int32_t)10))), /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
// System.Void weaponRoom::OnGUI()
extern TypeInfo* globales_t807_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t981_il2cpp_TypeInfo_var;
extern TypeInfo* gameControl_t794_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var;
extern "C" void weaponRoom_OnGUI_m3670 (weaponRoom_t840 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		globales_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		GUI_t981_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		gameControl_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484277);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t739  V_0 = {0};
	Vector3_t758  V_1 = {0};
	Rect_t738  V_2 = {0};
	Rect_t738  V_3 = {0};
	Rect_t738  V_4 = {0};
	Vector3_t758  V_5 = {0};
	Vector3_t758  V_6 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		globales_BeginGUI_m3520(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t809 * L_0 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t758  L_1 = Transform_get_position_m4034(L_0, /*hidden argument*/NULL);
		V_5 = L_1;
		float L_2 = ((&V_5)->___x_1);
		Transform_t809 * L_3 = Component_get_transform_m4030(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t758  L_4 = Transform_get_position_m4034(L_3, /*hidden argument*/NULL);
		V_6 = L_4;
		float L_5 = ((&V_6)->___y_2);
		float L_6 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_7 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		Rect_t738  L_8 = {0};
		Rect__ctor_m4041(&L_8, L_2, L_5, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_BeginGroup_m4066(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		int32_t L_9 = ((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6;
		if ((!(((uint32_t)L_9) == ((uint32_t)7))))
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		int32_t L_10 = ((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6;
		if ((((int32_t)L_10) == ((int32_t)6)))
		{
			goto IL_0292;
		}
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		float L_11 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_12 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		Rect_t738  L_13 = {0};
		Rect__ctor_m4041(&L_13, (0.0f), (0.0f), L_11, L_12, /*hidden argument*/NULL);
		Texture_t736 * L_14 = (__this->___keyPic_16);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m4089(NULL /*static, unused*/, L_13, L_14, 2, /*hidden argument*/NULL);
		Color_t747  L_15 = Color_get_white_m4084(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_color_m4076(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		float L_16 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_17 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		Vector2__ctor_m4033((&V_0), ((float)((float)L_16/(float)(4.0f))), ((float)((float)L_17/(float)(8.0f))), /*hidden argument*/NULL);
		float L_18 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENW_25;
		float L_19 = ((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENH_24;
		float L_20 = ((&V_0)->___y_2);
		Vector2_t739  L_21 = {0};
		Vector2__ctor_m4033(&L_21, ((float)((float)L_18/(float)(2.0f))), ((float)((float)L_19-(float)((float)((float)L_20*(float)(2.0f))))), /*hidden argument*/NULL);
		Vector3_t758  L_22 = Vector2_op_Implicit_m4038(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		float L_23 = ((&V_1)->___x_1);
		float L_24 = ((&V_1)->___y_2);
		float L_25 = ((&V_0)->___x_1);
		float L_26 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___x_1);
		float L_27 = ((&V_0)->___y_2);
		float L_28 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___y_2);
		Rect__ctor_m4041((&V_2), L_23, L_24, ((float)((float)((float)((float)L_25*(float)(2.0f)))*(float)L_26)), ((float)((float)((float)((float)L_27*(float)(2.0f)))*(float)L_28)), /*hidden argument*/NULL);
		float L_29 = ((&V_1)->___x_1);
		float L_30 = ((&V_0)->___x_1);
		float L_31 = ((&V_1)->___y_2);
		float L_32 = ((&V_0)->___x_1);
		float L_33 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___x_1);
		float L_34 = ((&V_0)->___y_2);
		float L_35 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___y_2);
		Rect__ctor_m4041((&V_3), ((float)((float)L_29+(float)((float)((float)L_30*(float)(-1.0f))))), L_31, ((float)((float)L_32*(float)L_33)), ((float)((float)((float)((float)L_34*(float)L_35))*(float)(2.0f))), /*hidden argument*/NULL);
		float L_36 = ((&V_1)->___x_1);
		float L_37 = ((&V_0)->___x_1);
		float L_38 = ((&V_1)->___y_2);
		float L_39 = ((&V_0)->___x_1);
		float L_40 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___x_1);
		float L_41 = ((&V_0)->___y_2);
		float L_42 = ((&((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___SCREENSCALE_43)->___y_2);
		Rect__ctor_m4041((&V_4), ((float)((float)L_36+(float)((float)((float)L_37*(float)(-2.0f))))), L_38, ((float)((float)L_39*(float)L_40)), ((float)((float)((float)((float)L_41*(float)L_42))*(float)(2.0f))), /*hidden argument*/NULL);
		Rect_t738  L_43 = V_3;
		GUIStyle_t724 * L_44 = (__this->___lowBarPlaySt_13);
		bool L_45 = GUI_Button_m4090(NULL /*static, unused*/, L_43, (String_t*) &_stringLiteral718, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_01e6;
		}
	}
	{
		SoundManager_playLongButton_m3283(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t144 * L_46 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral661, /*hidden argument*/NULL);
		__this->___gameControlObj_2 = L_46;
		GameObject_t144 * L_47 = (__this->___gameControlObj_2);
		NullCheck(L_47);
		gameControl_t794 * L_48 = GameObject_GetComponent_TisgameControl_t794_m4062(L_47, /*hidden argument*/GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var);
		NullCheck(L_48);
		gameControl_callScoreTable_m3453(L_48, /*hidden argument*/NULL);
	}

IL_01e6:
	{
		Rect_t738  L_49 = V_2;
		GUIStyle_t724 * L_50 = (__this->___lowBarPlaySt_13);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		bool L_51 = GUI_Button_m4090(NULL /*static, unused*/, L_49, (String_t*) &_stringLiteral719, L_50, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_0239;
		}
	}
	{
		GameObject_t144 * L_52 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral661, /*hidden argument*/NULL);
		__this->___gameControlObj_2 = L_52;
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6 = 4;
		GameObject_t144 * L_53 = (__this->___gameControlObj_2);
		NullCheck(L_53);
		gameControl_t794 * L_54 = GameObject_GetComponent_TisgameControl_t794_m4062(L_53, /*hidden argument*/GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var);
		NullCheck(L_54);
		gameControl_toGame_m3458(L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___showNewRecord_32 = 0;
		((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___showNewLevel_33 = 0;
		GameObject_t144 * L_55 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
	}

IL_0239:
	{
		Rect_t738  L_56 = V_4;
		GUIStyle_t724 * L_57 = (__this->___lowBarPlaySt_13);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		bool L_58 = GUI_Button_m4090(NULL /*static, unused*/, L_56, (String_t*) &_stringLiteral702, L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0292;
		}
	}
	{
		SoundManager_playLongButton_m3283(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t144 * L_59 = GameObject_FindGameObjectWithTag_m4082(NULL /*static, unused*/, (String_t*) &_stringLiteral661, /*hidden argument*/NULL);
		__this->___gameControlObj_2 = L_59;
		GameObject_t144 * L_60 = (__this->___gameControlObj_2);
		NullCheck(L_60);
		gameControl_t794 * L_61 = GameObject_GetComponent_TisgameControl_t794_m4062(L_60, /*hidden argument*/GameObject_GetComponent_TisgameControl_t794_m4062_MethodInfo_var);
		NullCheck(L_61);
		gameControl_toStoreRoom_m3479(L_61, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(gameControl_t794_il2cpp_TypeInfo_var);
		((gameControl_t794_StaticFields*)gameControl_t794_il2cpp_TypeInfo_var->static_fields)->___currentState_6 = 7;
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___showNewRecord_32 = 0;
		((globales_t807_StaticFields*)globales_t807_il2cpp_TypeInfo_var->static_fields)->___showNewLevel_33 = 0;
		GameObject_t144 * L_62 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
	}

IL_0292:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t981_il2cpp_TypeInfo_var);
		GUI_EndGroup_m4087(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(globales_t807_il2cpp_TypeInfo_var);
		globales_EndGUI_m3521(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void weaponRoom::OnApplicationQuit()
extern "C" void weaponRoom_OnApplicationQuit_m3671 (weaponRoom_t840 * __this, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceive.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceiveMethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.Invitation
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit_0.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void InvitationReceivedDelegate__ctor_m3672 (InvitationReceivedDelegate_t363 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C" void InvitationReceivedDelegate_Invoke_m3673 (InvitationReceivedDelegate_t363 * __this, Invitation_t341 * ___invitation, bool ___shouldAutoAccept, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		InvitationReceivedDelegate_Invoke_m3673((InvitationReceivedDelegate_t363 *)__this->___prev_9,___invitation, ___shouldAutoAccept, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Invitation_t341 * ___invitation, bool ___shouldAutoAccept, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___invitation, ___shouldAutoAccept,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Invitation_t341 * ___invitation, bool ___shouldAutoAccept, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___invitation, ___shouldAutoAccept,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___shouldAutoAccept, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___invitation, ___shouldAutoAccept,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_InvitationReceivedDelegate_t363(Il2CppObject* delegate, Invitation_t341 * ___invitation, bool ___shouldAutoAccept)
{
	// Marshaling of parameter '___invitation' to native representation
	Invitation_t341 * ____invitation_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'GooglePlayGames.BasicApi.Multiplayer.Invitation'."));
}
// System.IAsyncResult GooglePlayGames.BasicApi.InvitationReceivedDelegate::BeginInvoke(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern "C" Object_t * InvitationReceivedDelegate_BeginInvoke_m3674 (InvitationReceivedDelegate_t363 * __this, Invitation_t341 * ___invitation, bool ___shouldAutoAccept, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___invitation;
	__d_args[1] = Box(Boolean_t203_il2cpp_TypeInfo_var, &___shouldAutoAccept);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void InvitationReceivedDelegate_EndInvoke_m3675 (InvitationReceivedDelegate_t363 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1MethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_1.h"


// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void MatchDelegate__ctor_m3676 (MatchDelegate_t364 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C" void MatchDelegate_Invoke_m3677 (MatchDelegate_t364 * __this, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		MatchDelegate_Invoke_m3677((MatchDelegate_t364 *)__this->___prev_9,___match, ___shouldAutoLaunch, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___match, ___shouldAutoLaunch,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___match, ___shouldAutoLaunch,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___shouldAutoLaunch, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___match, ___shouldAutoLaunch,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_MatchDelegate_t364(Il2CppObject* delegate, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch)
{
	// Marshaling of parameter '___match' to native representation
	TurnBasedMatch_t353 * ____match_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch'."));
}
// System.IAsyncResult GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::BeginInvoke(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern "C" Object_t * MatchDelegate_BeginInvoke_m3678 (MatchDelegate_t364 * __this, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___match;
	__d_args[1] = Box(Boolean_t203_il2cpp_TypeInfo_var, &___shouldAutoLaunch);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::EndInvoke(System.IAsyncResult)
extern "C" void MatchDelegate_EndInvoke_m3679 (MatchDelegate_t364 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic_0MethodDeclarations.h"

// System.Byte
#include "mscorlib_System_Byte.h"


// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ConflictCallback__ctor_m3680 (ConflictCallback_t621 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern "C" void ConflictCallback_Invoke_m3681 (ConflictCallback_t621 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ConflictCallback_Invoke_m3681((ConflictCallback_t621 *)__this->___prev_9,___resolver, ___original, ___originalData, ___unmerged, ___unmergedData, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___resolver, ___original, ___originalData, ___unmerged, ___unmergedData,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___resolver, ___original, ___originalData, ___unmerged, ___unmergedData,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___resolver, ___original, ___originalData, ___unmerged, ___unmergedData,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ConflictCallback_t621(Il2CppObject* delegate, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData)
{
	// Marshaling of parameter '___resolver' to native representation
	Object_t * ____resolver_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'GooglePlayGames.BasicApi.SavedGame.IConflictResolver'."));
}
// System.IAsyncResult GooglePlayGames.BasicApi.SavedGame.ConflictCallback::BeginInvoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],System.AsyncCallback,System.Object)
extern "C" Object_t * ConflictCallback_BeginInvoke_m3682 (ConflictCallback_t621 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[6] = {0};
	__d_args[0] = ___resolver;
	__d_args[1] = ___original;
	__d_args[2] = ___originalData;
	__d_args[3] = ___unmerged;
	__d_args[4] = ___unmergedData;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::EndInvoke(System.IAsyncResult)
extern "C" void ConflictCallback_EndInvoke_m3683 (ConflictCallback_t621 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.ReportProgress
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgress.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.ReportProgress
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgressMethodDeclarations.h"

// System.Double
#include "mscorlib_System_Double.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"


// System.Void GooglePlayGames.ReportProgress::.ctor(System.Object,System.IntPtr)
extern "C" void ReportProgress__ctor_m3684 (ReportProgress_t380 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.ReportProgress::Invoke(System.String,System.Double,System.Action`1<System.Boolean>)
extern "C" void ReportProgress_Invoke_m3685 (ReportProgress_t380 * __this, String_t* ___id, double ___progress, Action_1_t98 * ___callback, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReportProgress_Invoke_m3685((ReportProgress_t380 *)__this->___prev_9,___id, ___progress, ___callback, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ___id, double ___progress, Action_1_t98 * ___callback, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___id, ___progress, ___callback,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___id, double ___progress, Action_1_t98 * ___callback, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___id, ___progress, ___callback,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, double ___progress, Action_1_t98 * ___callback, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___id, ___progress, ___callback,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult GooglePlayGames.ReportProgress::BeginInvoke(System.String,System.Double,System.Action`1<System.Boolean>,System.AsyncCallback,System.Object)
extern TypeInfo* Double_t234_il2cpp_TypeInfo_var;
extern "C" Object_t * ReportProgress_BeginInvoke_m3686 (ReportProgress_t380 * __this, String_t* ___id, double ___progress, Action_1_t98 * ___callback, AsyncCallback_t20 * ____callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Double_t234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(166);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___id;
	__d_args[1] = Box(Double_t234_il2cpp_TypeInfo_var, &___progress);
	__d_args[2] = ___callback;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)____callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.ReportProgress::EndInvoke(System.IAsyncResult)
extern "C" void ReportProgress_EndInvoke_m3687 (ReportProgress_t380 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
