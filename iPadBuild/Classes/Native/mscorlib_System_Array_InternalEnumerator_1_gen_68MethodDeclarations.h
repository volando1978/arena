﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int16>
struct InternalEnumerator_1_t4203;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25824_gshared (InternalEnumerator_1_t4203 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25824(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4203 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25824_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25825_gshared (InternalEnumerator_1_t4203 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25825(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4203 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25825_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25826_gshared (InternalEnumerator_1_t4203 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25826(__this, method) (( void (*) (InternalEnumerator_1_t4203 *, MethodInfo*))InternalEnumerator_1_Dispose_m25826_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25827_gshared (InternalEnumerator_1_t4203 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25827(__this, method) (( bool (*) (InternalEnumerator_1_t4203 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25827_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C" int16_t InternalEnumerator_1_get_Current_m25828_gshared (InternalEnumerator_1_t4203 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25828(__this, method) (( int16_t (*) (InternalEnumerator_1_t4203 *, MethodInfo*))InternalEnumerator_1_get_Current_m25828_gshared)(__this, method)
