﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t1157;

// System.Void UnityEngine.EventSystems.EventTrigger/TriggerEvent::.ctor()
extern "C" void TriggerEvent__ctor_m4582 (TriggerEvent_t1157 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
