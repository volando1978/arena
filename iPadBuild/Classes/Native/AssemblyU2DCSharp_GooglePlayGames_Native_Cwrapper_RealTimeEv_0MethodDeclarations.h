﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
struct OnRoomConnectedSetChangedCallback_t452;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnRoomConnectedSetChangedCallback__ctor_m1927 (OnRoomConnectedSetChangedCallback_t452 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void OnRoomConnectedSetChangedCallback_Invoke_m1928 (OnRoomConnectedSetChangedCallback_t452 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnRoomConnectedSetChangedCallback_t452(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnRoomConnectedSetChangedCallback_BeginInvoke_m1929 (OnRoomConnectedSetChangedCallback_t452 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnRoomConnectedSetChangedCallback_EndInvoke_m1930 (OnRoomConnectedSetChangedCallback_t452 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
