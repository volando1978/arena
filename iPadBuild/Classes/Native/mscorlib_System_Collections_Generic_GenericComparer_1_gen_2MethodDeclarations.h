﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t2921;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m14429_gshared (GenericComparer_1_t2921 * __this, MethodInfo* method);
#define GenericComparer_1__ctor_m14429(__this, method) (( void (*) (GenericComparer_1_t2921 *, MethodInfo*))GenericComparer_1__ctor_m14429_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m26153_gshared (GenericComparer_1_t2921 * __this, TimeSpan_t190  ___x, TimeSpan_t190  ___y, MethodInfo* method);
#define GenericComparer_1_Compare_m26153(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2921 *, TimeSpan_t190 , TimeSpan_t190 , MethodInfo*))GenericComparer_1_Compare_m26153_gshared)(__this, ___x, ___y, method)
