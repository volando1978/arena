﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t1682;
// System.Object
struct Object_t;
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1534;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11MethodDeclarations.h"
#define Enumerator__ctor_m24518(__this, ___host, method) (( void (*) (Enumerator_t1682 *, Dictionary_2_t1534 *, MethodInfo*))Enumerator__ctor_m16310_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24519(__this, method) (( Object_t * (*) (Enumerator_t1682 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m24520(__this, method) (( void (*) (Enumerator_t1682 *, MethodInfo*))Enumerator_Dispose_m16312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m24521(__this, method) (( bool (*) (Enumerator_t1682 *, MethodInfo*))Enumerator_MoveNext_m16313_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m24522(__this, method) (( GUIStyle_t724 * (*) (Enumerator_t1682 *, MethodInfo*))Enumerator_get_Current_m16314_gshared)(__this, method)
