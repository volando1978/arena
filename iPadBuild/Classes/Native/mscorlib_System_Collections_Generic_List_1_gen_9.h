﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualCurrency[]
struct VirtualCurrencyU5BU5D_t172;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>
struct  List_1_t114  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::_items
	VirtualCurrencyU5BU5D_t172* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::_version
	int32_t ____version_3;
};
struct List_1_t114_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::EmptyArray
	VirtualCurrencyU5BU5D_t172* ___EmptyArray_4;
};
