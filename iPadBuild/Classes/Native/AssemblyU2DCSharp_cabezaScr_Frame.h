﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// cabezaScr/Frame
struct  Frame_t770  : public Object_t
{
	// UnityEngine.Vector3 cabezaScr/Frame::position
	Vector3_t758  ___position_0;
	// UnityEngine.Quaternion cabezaScr/Frame::rotation
	Quaternion_t771  ___rotation_1;
};
