﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>
struct Action_2_t630;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A
struct  U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>> GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A::callback
	Action_2_t630 * ___callback_0;
};
