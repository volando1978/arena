﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InterludeScr/<run>c__Iterator3
struct U3CrunU3Ec__Iterator3_t742;
// System.Object
struct Object_t;

// System.Void InterludeScr/<run>c__Iterator3::.ctor()
extern "C" void U3CrunU3Ec__Iterator3__ctor_m3215 (U3CrunU3Ec__Iterator3_t742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InterludeScr/<run>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CrunU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3216 (U3CrunU3Ec__Iterator3_t742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InterludeScr/<run>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CrunU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3217 (U3CrunU3Ec__Iterator3_t742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InterludeScr/<run>c__Iterator3::MoveNext()
extern "C" bool U3CrunU3Ec__Iterator3_MoveNext_m3218 (U3CrunU3Ec__Iterator3_t742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterludeScr/<run>c__Iterator3::Dispose()
extern "C" void U3CrunU3Ec__Iterator3_Dispose_m3219 (U3CrunU3Ec__Iterator3_t742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterludeScr/<run>c__Iterator3::Reset()
extern "C" void U3CrunU3Ec__Iterator3_Reset_m3220 (U3CrunU3Ec__Iterator3_t742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
