﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>
struct Enumerator_t3661;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3655;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18987_gshared (Enumerator_t3661 * __this, Dictionary_2_t3655 * ___dictionary, MethodInfo* method);
#define Enumerator__ctor_m18987(__this, ___dictionary, method) (( void (*) (Enumerator_t3661 *, Dictionary_2_t3655 *, MethodInfo*))Enumerator__ctor_m18987_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18988_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18988(__this, method) (( Object_t * (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18988_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2128  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18989_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18989(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18989_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18990_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18990(__this, method) (( Object_t * (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18990_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18991_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18991(__this, method) (( Object_t * (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18991_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18992_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m18992(__this, method) (( bool (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_MoveNext_m18992_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::get_Current()
extern "C" KeyValuePair_2_t3656  Enumerator_get_Current_m18993_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_get_Current_m18993(__this, method) (( KeyValuePair_2_t3656  (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_get_Current_m18993_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m18994_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_get_CurrentKey_m18994(__this, method) (( Object_t * (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_get_CurrentKey_m18994_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::get_CurrentValue()
extern "C" uint32_t Enumerator_get_CurrentValue_m18995_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_get_CurrentValue_m18995(__this, method) (( uint32_t (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_get_CurrentValue_m18995_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::VerifyState()
extern "C" void Enumerator_VerifyState_m18996_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m18996(__this, method) (( void (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_VerifyState_m18996_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m18997_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_VerifyCurrent_m18997(__this, method) (( void (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_VerifyCurrent_m18997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::Dispose()
extern "C" void Enumerator_Dispose_m18998_gshared (Enumerator_t3661 * __this, MethodInfo* method);
#define Enumerator_Dispose_m18998(__this, method) (( void (*) (Enumerator_t3661 *, MethodInfo*))Enumerator_Dispose_m18998_gshared)(__this, method)
