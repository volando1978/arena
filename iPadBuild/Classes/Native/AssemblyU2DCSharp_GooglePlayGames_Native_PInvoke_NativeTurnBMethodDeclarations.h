﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct IEnumerable_1_t874;
// GooglePlayGames.Native.PInvoke.ParticipantResults
struct ParticipantResults_t683;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc_0.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_0.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::.ctor(System.IntPtr)
extern "C" void NativeTurnBasedMatch__ctor_m2826 (NativeTurnBasedMatch_t680 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::AvailableAutomatchSlots()
extern "C" uint32_t NativeTurnBasedMatch_AvailableAutomatchSlots_m2827 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::CreationTime()
extern "C" uint64_t NativeTurnBasedMatch_CreationTime_m2828 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant> GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Participants()
extern "C" Object_t* NativeTurnBasedMatch_Participants_m2829 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Version()
extern "C" uint32_t NativeTurnBasedMatch_Version_m2830 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Variant()
extern "C" uint32_t NativeTurnBasedMatch_Variant_m2831 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.ParticipantResults GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Results()
extern "C" ParticipantResults_t683 * NativeTurnBasedMatch_Results_m2832 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::ParticipantWithId(System.String)
extern "C" MultiplayerParticipant_t672 * NativeTurnBasedMatch_ParticipantWithId_m2833 (NativeTurnBasedMatch_t680 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::PendingParticipant()
extern "C" MultiplayerParticipant_t672 * NativeTurnBasedMatch_PendingParticipant_m2834 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::MatchStatus()
extern "C" int32_t NativeTurnBasedMatch_MatchStatus_m2835 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Description()
extern "C" String_t* NativeTurnBasedMatch_Description_m2836 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::HasRematchId()
extern "C" bool NativeTurnBasedMatch_HasRematchId_m2837 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::RematchId()
extern "C" String_t* NativeTurnBasedMatch_RematchId_m2838 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Data()
extern "C" ByteU5BU5D_t350* NativeTurnBasedMatch_Data_m2839 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::Id()
extern "C" String_t* NativeTurnBasedMatch_Id_m2840 (NativeTurnBasedMatch_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeTurnBasedMatch_CallDispose_m2841 (NativeTurnBasedMatch_t680 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::AsTurnBasedMatch(System.String)
extern "C" TurnBasedMatch_t353 * NativeTurnBasedMatch_AsTurnBasedMatch_m2842 (NativeTurnBasedMatch_t680 * __this, String_t* ___selfPlayerId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::ToTurnStatus(GooglePlayGames.Native.Cwrapper.Types/MatchStatus)
extern "C" int32_t NativeTurnBasedMatch_ToTurnStatus_m2843 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::ToMatchStatus(System.String,GooglePlayGames.Native.Cwrapper.Types/MatchStatus)
extern "C" int32_t NativeTurnBasedMatch_ToMatchStatus_m2844 (Object_t * __this /* static, unused */, String_t* ___pendingParticipantId, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::FromPointer(System.IntPtr)
extern "C" NativeTurnBasedMatch_t680 * NativeTurnBasedMatch_FromPointer_m2845 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Participants>m__93(System.UIntPtr)
extern "C" MultiplayerParticipant_t672 * NativeTurnBasedMatch_U3CParticipantsU3Em__93_m2846 (NativeTurnBasedMatch_t680 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Description>m__94(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CDescriptionU3Em__94_m2847 (NativeTurnBasedMatch_t680 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<RematchId>m__95(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CRematchIdU3Em__95_m2848 (NativeTurnBasedMatch_t680 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Data>m__96(System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CDataU3Em__96_m2849 (NativeTurnBasedMatch_t680 * __this, ByteU5BU5D_t350* ___bytes, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch::<Id>m__97(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeTurnBasedMatch_U3CIdU3Em__97_m2850 (NativeTurnBasedMatch_t680 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
