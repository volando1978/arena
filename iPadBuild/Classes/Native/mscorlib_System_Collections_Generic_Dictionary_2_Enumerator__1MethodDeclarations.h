﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Enumerator_t1374;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1191;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t1197;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__20MethodDeclarations.h"
#define Enumerator__ctor_m22420(__this, ___dictionary, method) (( void (*) (Enumerator_t1374 *, Dictionary_2_t1197 *, MethodInfo*))Enumerator__ctor_m22333_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22421(__this, method) (( Object_t * (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22334_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22422(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22423(__this, method) (( Object_t * (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22336_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22424(__this, method) (( Object_t * (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22337_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m5798(__this, method) (( bool (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_MoveNext_m22338_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m5795(__this, method) (( KeyValuePair_2_t1373  (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_get_Current_m22339_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22425(__this, method) (( int32_t (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_get_CurrentKey_m22340_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22426(__this, method) (( PointerEventData_t1191 * (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_get_CurrentValue_m22341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m22427(__this, method) (( void (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_VerifyState_m22342_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22428(__this, method) (( void (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_VerifyCurrent_m22343_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m22429(__this, method) (( void (*) (Enumerator_t1374 *, MethodInfo*))Enumerator_Dispose_m22344_gshared)(__this, method)
