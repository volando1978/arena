﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// JSONObject/GetFieldResponse
struct GetFieldResponse_t33;
// System.Object
struct Object_t;
// JSONObject
struct JSONObject_t30;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void JSONObject/GetFieldResponse::.ctor(System.Object,System.IntPtr)
extern "C" void GetFieldResponse__ctor_m70 (GetFieldResponse_t33 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/GetFieldResponse::Invoke(JSONObject)
extern "C" void GetFieldResponse_Invoke_m71 (GetFieldResponse_t33 * __this, JSONObject_t30 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetFieldResponse_t33(Il2CppObject* delegate, JSONObject_t30 * ___obj);
// System.IAsyncResult JSONObject/GetFieldResponse::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C" Object_t * GetFieldResponse_BeginInvoke_m72 (GetFieldResponse_t33 * __this, JSONObject_t30 * ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/GetFieldResponse::EndInvoke(System.IAsyncResult)
extern "C" void GetFieldResponse_EndInvoke_m73 (GetFieldResponse_t33 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
