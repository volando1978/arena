﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState
struct MessagingEnabledState_t580;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession,GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState__ctor_m2389 (MessagingEnabledState_t580 * __this, RoomSession_t566 * ___session, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::UpdateCurrentRoom(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_UpdateCurrentRoom_m2390 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_OnRoomStatusChanged_m2391 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_HandleRoomStatusChanged_m2392 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_OnConnectedSetChanged_m2393 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void MessagingEnabledState_HandleConnectedSetChanged_m2394 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void MessagingEnabledState_OnParticipantStatusChanged_m2395 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::HandleParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void MessagingEnabledState_HandleParticipantStatusChanged_m2396 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::GetConnectedParticipants()
extern "C" List_1_t351 * MessagingEnabledState_GetConnectedParticipants_m2397 (MessagingEnabledState_t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::SendToSpecificRecipient(System.String,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C" void MessagingEnabledState_SendToSpecificRecipient_m2398 (MessagingEnabledState_t580 * __this, String_t* ___recipientId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::SendToAll(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C" void MessagingEnabledState_SendToAll_m2399 (MessagingEnabledState_t580 * __this, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::OnDataReceived(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C" void MessagingEnabledState_OnDataReceived_m2400 (MessagingEnabledState_t580 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___sender, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<UpdateCurrentRoom>m__37(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" String_t* MessagingEnabledState_U3CUpdateCurrentRoomU3Em__37_m2401 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<UpdateCurrentRoom>m__38(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" Participant_t340 * MessagingEnabledState_U3CUpdateCurrentRoomU3Em__38_m2402 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<UpdateCurrentRoom>m__39(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* MessagingEnabledState_U3CUpdateCurrentRoomU3Em__39_m2403 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState::<GetConnectedParticipants>m__3A(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" bool MessagingEnabledState_U3CGetConnectedParticipantsU3Em__3A_m2404 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
