﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Advertisements.ShowResult
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho_0.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct  Action_1_t155  : public MulticastDelegate_t22
{
};
