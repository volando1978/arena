﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// Soomla.Store.VirtualItem
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_VirtualItem.h"
// Soomla.Store.PurchasableVirtualItem
struct  PurchasableVirtualItem_t124  : public VirtualItem_t125
{
	// Soomla.Store.PurchaseType Soomla.Store.PurchasableVirtualItem::PurchaseType
	PurchaseType_t123 * ___PurchaseType_6;
};
