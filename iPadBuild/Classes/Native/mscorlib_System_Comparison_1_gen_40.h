﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t1229;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Canvas>
struct  Comparison_1_t3982  : public MulticastDelegate_t22
{
};
