﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// weaponRoom
struct weaponRoom_t840;

// System.Void weaponRoom::.ctor()
extern "C" void weaponRoom__ctor_m3666 (weaponRoom_t840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void weaponRoom::Start()
extern "C" void weaponRoom_Start_m3667 (weaponRoom_t840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void weaponRoom::Update()
extern "C" void weaponRoom_Update_m3668 (weaponRoom_t840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void weaponRoom::setSizes()
extern "C" void weaponRoom_setSizes_m3669 (weaponRoom_t840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void weaponRoom::OnGUI()
extern "C" void weaponRoom_OnGUI_m3670 (weaponRoom_t840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void weaponRoom::OnApplicationQuit()
extern "C" void weaponRoom_OnApplicationQuit_m3671 (weaponRoom_t840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
