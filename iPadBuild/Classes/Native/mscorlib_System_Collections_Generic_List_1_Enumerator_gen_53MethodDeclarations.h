﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>
struct Enumerator_t4244;
// System.Object
struct Object_t;
// System.Security.Policy.StrongName
struct StrongName_t2727;
// System.Collections.Generic.List`1<System.Security.Policy.StrongName>
struct List_1_t2908;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m26092(__this, ___l, method) (( void (*) (Enumerator_t4244 *, List_1_t2908 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26093(__this, method) (( Object_t * (*) (Enumerator_t4244 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::Dispose()
#define Enumerator_Dispose_m26094(__this, method) (( void (*) (Enumerator_t4244 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::VerifyState()
#define Enumerator_VerifyState_m26095(__this, method) (( void (*) (Enumerator_t4244 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::MoveNext()
#define Enumerator_MoveNext_m26096(__this, method) (( bool (*) (Enumerator_t4244 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::get_Current()
#define Enumerator_get_Current_m26097(__this, method) (( StrongName_t2727 * (*) (Enumerator_t4244 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
