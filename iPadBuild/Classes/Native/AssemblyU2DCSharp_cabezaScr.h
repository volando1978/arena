﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// System.Collections.Generic.List`1<cabezaScr/Frame>
struct List_1_t772;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// cabezaScr
struct  cabezaScr_t773  : public MonoBehaviour_t26
{
	// System.Int32 cabezaScr::mybody
	int32_t ___mybody_2;
	// UnityEngine.GameObject cabezaScr::bodyObj
	GameObject_t144 * ___bodyObj_3;
	// System.Collections.Generic.List`1<cabezaScr/Frame> cabezaScr::formerPositions
	List_1_t772 * ___formerPositions_4;
	// System.Int32 cabezaScr::frameDelay
	int32_t ___frameDelay_5;
	// System.Single cabezaScr::minimumDistance
	float ___minimumDistance_6;
	// UnityEngine.Vector3 cabezaScr::formerPosition
	Vector3_t758  ___formerPosition_7;
};
