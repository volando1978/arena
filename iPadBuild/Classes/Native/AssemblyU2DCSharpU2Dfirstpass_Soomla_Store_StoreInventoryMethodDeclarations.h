﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreInventory
struct StoreInventory_t104;
// System.String
struct String_t;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;

// System.Void Soomla.Store.StoreInventory::.ctor()
extern "C" void StoreInventory__ctor_m485 (StoreInventory_t104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::.cctor()
extern "C" void StoreInventory__cctor_m486 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreInventory::CanAfford(System.String)
extern "C" bool StoreInventory_CanAfford_m487 (Object_t * __this /* static, unused */, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::BuyItem(System.String)
extern "C" void StoreInventory_BuyItem_m488 (Object_t * __this /* static, unused */, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::BuyItem(System.String,System.String)
extern "C" void StoreInventory_BuyItem_m489 (Object_t * __this /* static, unused */, String_t* ___itemId, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.StoreInventory::GetItemBalance(System.String)
extern "C" int32_t StoreInventory_GetItemBalance_m490 (Object_t * __this /* static, unused */, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::GiveItem(System.String,System.Int32)
extern "C" void StoreInventory_GiveItem_m491 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::TakeItem(System.String,System.Int32)
extern "C" void StoreInventory_TakeItem_m492 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::EquipVirtualGood(System.String)
extern "C" void StoreInventory_EquipVirtualGood_m493 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::UnEquipVirtualGood(System.String)
extern "C" void StoreInventory_UnEquipVirtualGood_m494 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreInventory::IsVirtualGoodEquipped(System.String)
extern "C" bool StoreInventory_IsVirtualGoodEquipped_m495 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.StoreInventory::GetGoodUpgradeLevel(System.String)
extern "C" int32_t StoreInventory_GetGoodUpgradeLevel_m496 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.Store.StoreInventory::GetGoodCurrentUpgrade(System.String)
extern "C" String_t* StoreInventory_GetGoodCurrentUpgrade_m497 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::UpgradeGood(System.String)
extern "C" void StoreInventory_UpgradeGood_m498 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::RemoveGoodUpgrades(System.String)
extern "C" void StoreInventory_RemoveGoodUpgrades_m499 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::RefreshLocalInventory()
extern "C" void StoreInventory_RefreshLocalInventory_m500 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::RefreshOnGoodUpgrade(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG)
extern "C" void StoreInventory_RefreshOnGoodUpgrade_m501 (Object_t * __this /* static, unused */, VirtualGood_t79 * ___vg, UpgradeVG_t133 * ___uvg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::RefreshOnGoodEquipped(Soomla.Store.EquippableVG)
extern "C" void StoreInventory_RefreshOnGoodEquipped_m502 (Object_t * __this /* static, unused */, EquippableVG_t129 * ___equippable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::RefreshOnGoodUnEquipped(Soomla.Store.EquippableVG)
extern "C" void StoreInventory_RefreshOnGoodUnEquipped_m503 (Object_t * __this /* static, unused */, EquippableVG_t129 * ___equippable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::RefreshOnCurrencyBalanceChanged(Soomla.Store.VirtualCurrency,System.Int32,System.Int32)
extern "C" void StoreInventory_RefreshOnCurrencyBalanceChanged_m504 (Object_t * __this /* static, unused */, VirtualCurrency_t77 * ___virtualCurrency, int32_t ___balance, int32_t ___amountAdded, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::RefreshOnGoodBalanceChanged(Soomla.Store.VirtualGood,System.Int32,System.Int32)
extern "C" void StoreInventory_RefreshOnGoodBalanceChanged_m505 (Object_t * __this /* static, unused */, VirtualGood_t79 * ___good, int32_t ___balance, int32_t ___amountAdded, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInventory::UpdateLocalBalance(System.String,System.Int32)
extern "C" void StoreInventory_UpdateLocalBalance_m506 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___balance, MethodInfo* method) IL2CPP_METHOD_ATTR;
