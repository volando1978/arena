﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreManifestTools
struct StoreManifestTools_t73;

// System.Void Soomla.Store.StoreManifestTools::.ctor()
extern "C" void StoreManifestTools__ctor_m264 (StoreManifestTools_t73 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
