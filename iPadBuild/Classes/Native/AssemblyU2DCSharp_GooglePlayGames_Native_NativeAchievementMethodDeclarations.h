﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeAchievement
struct NativeAchievement_t673;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi_0.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.NativeAchievement::.ctor(System.IntPtr)
extern "C" void NativeAchievement__ctor_m2725 (NativeAchievement_t673 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.NativeAchievement::CurrentSteps()
extern "C" uint32_t NativeAchievement_CurrentSteps_m2726 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeAchievement::Description()
extern "C" String_t* NativeAchievement_Description_m2727 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeAchievement::Id()
extern "C" String_t* NativeAchievement_Id_m2728 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeAchievement::Name()
extern "C" String_t* NativeAchievement_Name_m2729 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/AchievementState GooglePlayGames.Native.NativeAchievement::State()
extern "C" int32_t NativeAchievement_State_m2730 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.NativeAchievement::TotalSteps()
extern "C" uint32_t NativeAchievement_TotalSteps_m2731 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/AchievementType GooglePlayGames.Native.NativeAchievement::Type()
extern "C" int32_t NativeAchievement_Type_m2732 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeAchievement::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeAchievement_CallDispose_m2733 (NativeAchievement_t673 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.Native.NativeAchievement::AsAchievement()
extern "C" Achievement_t333 * NativeAchievement_AsAchievement_m2734 (NativeAchievement_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeAchievement::<Description>m__6E(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeAchievement_U3CDescriptionU3Em__6E_m2735 (NativeAchievement_t673 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeAchievement::<Id>m__6F(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeAchievement_U3CIdU3Em__6F_m2736 (NativeAchievement_t673 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeAchievement::<Name>m__70(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeAchievement_U3CNameU3Em__70_m2737 (NativeAchievement_t673 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
