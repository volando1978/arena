﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<System.IntPtr>
struct Comparison_1_t3817;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m20869_gshared (Comparison_1_t3817 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Comparison_1__ctor_m20869(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3817 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m20869_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.IntPtr>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m20870_gshared (Comparison_1_t3817 * __this, IntPtr_t ___x, IntPtr_t ___y, MethodInfo* method);
#define Comparison_1_Invoke_m20870(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3817 *, IntPtr_t, IntPtr_t, MethodInfo*))Comparison_1_Invoke_m20870_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.IntPtr>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m20871_gshared (Comparison_1_t3817 * __this, IntPtr_t ___x, IntPtr_t ___y, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Comparison_1_BeginInvoke_m20871(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3817 *, IntPtr_t, IntPtr_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m20871_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m20872_gshared (Comparison_1_t3817 * __this, Object_t * ___result, MethodInfo* method);
#define Comparison_1_EndInvoke_m20872(__this, ___result, method) (( int32_t (*) (Comparison_1_t3817 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m20872_gshared)(__this, ___result, method)
