﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Even.h"
// GooglePlayGames.Native.Cwrapper.Types/EventVisibility
struct  EventVisibility_t508 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/EventVisibility::value__
	int32_t ___value___1;
};
