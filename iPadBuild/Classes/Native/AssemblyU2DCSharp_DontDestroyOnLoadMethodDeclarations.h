﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DontDestroyOnLoad
struct DontDestroyOnLoad_t728;

// System.Void DontDestroyOnLoad::.ctor()
extern "C" void DontDestroyOnLoad__ctor_m3156 (DontDestroyOnLoad_t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DontDestroyOnLoad::Start()
extern "C" void DontDestroyOnLoad_Start_m3157 (DontDestroyOnLoad_t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
