﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.ComImportAttribute
struct ComImportAttribute_t2345;

// System.Void System.Runtime.InteropServices.ComImportAttribute::.ctor()
extern "C" void ComImportAttribute__ctor_m10718 (ComImportAttribute_t2345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
