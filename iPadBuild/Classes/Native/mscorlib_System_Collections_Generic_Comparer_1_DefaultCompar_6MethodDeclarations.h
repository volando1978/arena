﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t4107;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m24884_gshared (DefaultComparer_t4107 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m24884(__this, method) (( void (*) (DefaultComparer_t4107 *, MethodInfo*))DefaultComparer__ctor_m24884_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m24885_gshared (DefaultComparer_t4107 * __this, UICharInfo_t1400  ___x, UICharInfo_t1400  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m24885(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4107 *, UICharInfo_t1400 , UICharInfo_t1400 , MethodInfo*))DefaultComparer_Compare_m24885_gshared)(__this, ___x, ___y, method)
