﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.RewardStorageAndroid
struct RewardStorageAndroid_t3;

// System.Void Soomla.RewardStorageAndroid::.ctor()
extern "C" void RewardStorageAndroid__ctor_m1 (RewardStorageAndroid_t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
