﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// cabezaScr
struct cabezaScr_t773;
// UnityEngine.GameObject
struct GameObject_t144;

// System.Void cabezaScr::.ctor()
extern "C" void cabezaScr__ctor_m3330 (cabezaScr_t773 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cabezaScr::Start()
extern "C" void cabezaScr_Start_m3331 (cabezaScr_t773 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cabezaScr::Update()
extern "C" void cabezaScr_Update_m3332 (cabezaScr_t773 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cabezaScr::MoveFollower(UnityEngine.GameObject,System.Single,System.Single)
extern "C" void cabezaScr_MoveFollower_m3333 (cabezaScr_t773 * __this, GameObject_t144 * ___target, float ___maxSpeed, float ___maxAngleSpeed, MethodInfo* method) IL2CPP_METHOD_ATTR;
