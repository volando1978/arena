﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct ReadOnlyCollection_1_t3431;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct IList_1_t3430;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>[]
struct UnityKeyValuePair_2U5BU5D_t3411;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t4261;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m15551(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3431 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m15428_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15552(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3431 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15553(__this, method) (( void (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15554(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3431 *, int32_t, UnityKeyValuePair_2_t3412 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15555(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3431 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15556(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3431 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15557(__this, ___index, method) (( UnityKeyValuePair_2_t3412 * (*) (ReadOnlyCollection_1_t3431 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15558(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3431 *, int32_t, UnityKeyValuePair_2_t3412 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15559(__this, method) (( bool (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15560(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3431 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15561(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m15562(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3431 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15439_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m15563(__this, method) (( void (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15440_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m15564(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3431 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15441_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15565(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3431 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m15566(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3431 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15443_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m15567(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3431 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15444_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15568(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3431 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15569(__this, method) (( bool (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15570(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15571(__this, method) (( bool (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15572(__this, method) (( bool (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m15573(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3431 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m15574(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3431 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Contains(T)
#define ReadOnlyCollection_1_Contains_m15575(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3431 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))ReadOnlyCollection_1_Contains_m15452_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m15576(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3431 *, UnityKeyValuePair_2U5BU5D_t3411*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m15453_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m15577(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15454_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m15578(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3431 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))ReadOnlyCollection_1_IndexOf_m15455_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::get_Count()
#define ReadOnlyCollection_1_get_Count_m15579(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3431 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m15456_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m15580(__this, ___index, method) (( UnityKeyValuePair_2_t3412 * (*) (ReadOnlyCollection_1_t3431 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m15457_gshared)(__this, ___index, method)
