﻿#pragma once
#include <stdint.h>
// gameControl
struct gameControl_t794;
// UnityEngine.GameObject
struct GameObject_t144;
// enemyController
struct enemyController_t785;
// UnityEngine.AudioSource
struct AudioSource_t755;
// GameCenterSingleton
struct GameCenterSingleton_t730;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// gameControl/State
#include "AssemblyU2DCSharp_gameControl_State.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// gameControl
struct  gameControl_t794  : public MonoBehaviour_t26
{
	// gameControl/State gameControl::status
	int32_t ___status_3;
	// System.Int32 gameControl::factorLevel
	int32_t ___factorLevel_4;
	// System.Int32 gameControl::tempLevel
	int32_t ___tempLevel_5;
	// UnityEngine.Vector2 gameControl::posH
	Vector2_t739  ___posH_9;
	// UnityEngine.GameObject gameControl::admob
	GameObject_t144 * ___admob_10;
	// UnityEngine.GameObject gameControl::player
	GameObject_t144 * ___player_11;
	// UnityEngine.GameObject gameControl::currentPlayer
	GameObject_t144 * ___currentPlayer_12;
	// UnityEngine.GameObject gameControl::enemyController
	GameObject_t144 * ___enemyController_13;
	// UnityEngine.GameObject gameControl::currentEnemyController
	GameObject_t144 * ___currentEnemyController_14;
	// enemyController gameControl::_enemyController
	enemyController_t785 * ____enemyController_15;
	// UnityEngine.GameObject gameControl::menuObj
	GameObject_t144 * ___menuObj_16;
	// UnityEngine.GameObject gameControl::currentMenu
	GameObject_t144 * ___currentMenu_17;
	// UnityEngine.GameObject gameControl::currentWeaponRoom
	GameObject_t144 * ___currentWeaponRoom_18;
	// UnityEngine.GameObject gameControl::currentStoreRoom
	GameObject_t144 * ___currentStoreRoom_19;
	// UnityEngine.GameObject gameControl::tutorialControllerObj
	GameObject_t144 * ___tutorialControllerObj_20;
	// UnityEngine.GameObject gameControl::weaponRoom
	GameObject_t144 * ___weaponRoom_21;
	// UnityEngine.GameObject gameControl::storeObj
	GameObject_t144 * ___storeObj_22;
	// UnityEngine.GameObject gameControl::bombaObj
	GameObject_t144 * ___bombaObj_23;
	// UnityEngine.GameObject gameControl::dustObj
	GameObject_t144 * ___dustObj_24;
	// UnityEngine.GameObject gameControl::explosionSmall
	GameObject_t144 * ___explosionSmall_25;
	// UnityEngine.GameObject gameControl::paquete
	GameObject_t144 * ___paquete_26;
	// UnityEngine.GameObject gameControl::gameOverObj
	GameObject_t144 * ___gameOverObj_27;
	// UnityEngine.GameObject gameControl::currentGameOver
	GameObject_t144 * ___currentGameOver_28;
	// UnityEngine.GameObject gameControl::bullet
	GameObject_t144 * ___bullet_29;
	// UnityEngine.GameObject gameControl::bulletThin
	GameObject_t144 * ___bulletThin_30;
	// UnityEngine.GameObject gameControl::bulletLaser
	GameObject_t144 * ___bulletLaser_31;
	// UnityEngine.GameObject gameControl::raycastObj
	GameObject_t144 * ___raycastObj_32;
	// UnityEngine.GameObject gameControl::circController
	GameObject_t144 * ___circController_33;
	// UnityEngine.GameObject gameControl::rayo
	GameObject_t144 * ___rayo_34;
	// UnityEngine.GameObject gameControl::music
	GameObject_t144 * ___music_35;
	// UnityEngine.AudioSource gameControl::_music
	AudioSource_t755 * ____music_36;
	// UnityEngine.GameObject gameControl::bomba
	GameObject_t144 * ___bomba_37;
	// UnityEngine.GameObject gameControl::rayoBomba
	GameObject_t144 * ___rayoBomba_38;
	// UnityEngine.GameObject gameControl::naveProxy
	GameObject_t144 * ___naveProxy_39;
	// UnityEngine.GameObject gameControl::currentNaveProxy
	GameObject_t144 * ___currentNaveProxy_40;
	// System.Single gameControl::gatheringTime
	float ___gatheringTime_41;
	// System.Int32 gameControl::maxNaveProxy
	int32_t ___maxNaveProxy_43;
	// System.Boolean gameControl::newWeaponFlag
	bool ___newWeaponFlag_44;
	// GameCenterSingleton gameControl::gameCenter
	GameCenterSingleton_t730 * ___gameCenter_45;
};
struct gameControl_t794_StaticFields{
	// gameControl gameControl::instance
	gameControl_t794 * ___instance_2;
	// gameControl/State gameControl::currentState
	int32_t ___currentState_6;
	// UnityEngine.Vector2 gameControl::midPos
	Vector2_t739  ___midPos_7;
	// UnityEngine.Vector2 gameControl::highPos
	Vector2_t739  ___highPos_8;
	// System.Single gameControl::stageTime
	float ___stageTime_42;
	// System.Boolean gameControl::slowMotion
	bool ___slowMotion_46;
	// System.Boolean gameControl::slowDead
	bool ___slowDead_47;
};
