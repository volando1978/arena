﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>
struct  KeyValuePair_2_t198 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::value
	JSONObject_t30 * ___value_1;
};
