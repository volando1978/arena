﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct PlayGamesHelperObject_t392;
// System.Action
struct Action_t588;
// System.Action`1<System.Boolean>
struct Action_1_t98;

// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.ctor()
extern "C" void PlayGamesHelperObject__ctor_m1598 (PlayGamesHelperObject_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.cctor()
extern "C" void PlayGamesHelperObject__cctor_m1599 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::CreateObject()
extern "C" void PlayGamesHelperObject_CreateObject_m1600 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Awake()
extern "C" void PlayGamesHelperObject_Awake_m1601 (PlayGamesHelperObject_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnDisable()
extern "C" void PlayGamesHelperObject_OnDisable_m1602 (PlayGamesHelperObject_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunOnGameThread(System.Action)
extern "C" void PlayGamesHelperObject_RunOnGameThread_m1603 (Object_t * __this /* static, unused */, Action_t588 * ___action, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Update()
extern "C" void PlayGamesHelperObject_Update_m1604 (PlayGamesHelperObject_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationFocus(System.Boolean)
extern "C" void PlayGamesHelperObject_OnApplicationFocus_m1605 (PlayGamesHelperObject_t392 * __this, bool ___focused, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationPause(System.Boolean)
extern "C" void PlayGamesHelperObject_OnApplicationPause_m1606 (PlayGamesHelperObject_t392 * __this, bool ___paused, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::SetFocusCallback(System.Action`1<System.Boolean>)
extern "C" void PlayGamesHelperObject_SetFocusCallback_m1607 (Object_t * __this /* static, unused */, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::SetPauseCallback(System.Action`1<System.Boolean>)
extern "C" void PlayGamesHelperObject_SetPauseCallback_m1608 (Object_t * __this /* static, unused */, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::<Update>m__3(System.Action)
extern "C" void PlayGamesHelperObject_U3CUpdateU3Em__3_m1609 (Object_t * __this /* static, unused */, Action_t588 * ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
