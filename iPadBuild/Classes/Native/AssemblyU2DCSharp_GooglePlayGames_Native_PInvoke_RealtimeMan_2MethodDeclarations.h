﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
struct FetchInvitationsResponse_t698;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct IEnumerable_1_t883;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::.ctor(System.IntPtr)
extern "C" void FetchInvitationsResponse__ctor_m2969 (FetchInvitationsResponse_t698 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::RequestSucceeded()
extern "C" bool FetchInvitationsResponse_RequestSucceeded_m2970 (FetchInvitationsResponse_t698 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::ResponseStatus()
extern "C" int32_t FetchInvitationsResponse_ResponseStatus_m2971 (FetchInvitationsResponse_t698 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::Invitations()
extern "C" Object_t* FetchInvitationsResponse_Invitations_m2972 (FetchInvitationsResponse_t698 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchInvitationsResponse_CallDispose_m2973 (FetchInvitationsResponse_t698 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::FromPointer(System.IntPtr)
extern "C" FetchInvitationsResponse_t698 * FetchInvitationsResponse_FromPointer_m2974 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::<Invitations>m__9C(System.UIntPtr)
extern "C" MultiplayerInvitation_t601 * FetchInvitationsResponse_U3CInvitationsU3Em__9C_m2975 (FetchInvitationsResponse_t698 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
