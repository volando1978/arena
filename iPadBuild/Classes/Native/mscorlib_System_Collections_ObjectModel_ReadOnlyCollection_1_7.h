﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Soomla.Reward>
struct IList_1_t3520;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Reward>
struct  ReadOnlyCollection_1_t3521  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Reward>::list
	Object_t* ___list_0;
};
