﻿#pragma once
#include <stdint.h>
// System.IO.TextReader
struct TextReader_t231;
// System.IO.TextReader
#include "mscorlib_System_IO_TextReader.h"
// System.IO.SynchronizedReader
struct  SynchronizedReader_t2500  : public TextReader_t231
{
	// System.IO.TextReader System.IO.SynchronizedReader::reader
	TextReader_t231 * ___reader_1;
};
