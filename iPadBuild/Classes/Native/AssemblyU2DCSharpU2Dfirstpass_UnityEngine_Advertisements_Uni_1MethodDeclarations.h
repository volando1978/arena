﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.UnityAdsIos
struct UnityAdsIos_t158;
// System.String
struct String_t;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"

// System.Void UnityEngine.Advertisements.UnityAdsIos::.ctor()
extern "C" void UnityAdsIos__ctor_m788 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIos::init(System.String,System.Boolean,System.String,System.String)
extern "C" void UnityAdsIos_init_m789 (UnityAdsIos_t158 * __this, String_t* ___gameId, bool ___testModeEnabled, String_t* ___gameObjectName, String_t* ___unityVersion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::show(System.String,System.String,System.String)
extern "C" bool UnityAdsIos_show_m790 (UnityAdsIos_t158 * __this, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIos::hide()
extern "C" void UnityAdsIos_hide_m791 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::isSupported()
extern "C" bool UnityAdsIos_isSupported_m792 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIos::getSDKVersion()
extern "C" String_t* UnityAdsIos_getSDKVersion_m793 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::canShowZone(System.String)
extern "C" bool UnityAdsIos_canShowZone_m794 (UnityAdsIos_t158 * __this, String_t* ___zone, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::hasMultipleRewardItems()
extern "C" bool UnityAdsIos_hasMultipleRewardItems_m795 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemKeys()
extern "C" String_t* UnityAdsIos_getRewardItemKeys_m796 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIos::getDefaultRewardItemKey()
extern "C" String_t* UnityAdsIos_getDefaultRewardItemKey_m797 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIos::getCurrentRewardItemKey()
extern "C" String_t* UnityAdsIos_getCurrentRewardItemKey_m798 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::setRewardItemKey(System.String)
extern "C" bool UnityAdsIos_setRewardItemKey_m799 (UnityAdsIos_t158 * __this, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIos::setDefaultRewardItemAsRewardItem()
extern "C" void UnityAdsIos_setDefaultRewardItemAsRewardItem_m800 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemDetailsWithKey(System.String)
extern "C" String_t* UnityAdsIos_getRewardItemDetailsWithKey_m801 (UnityAdsIos_t158 * __this, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemDetailsKeys()
extern "C" String_t* UnityAdsIos_getRewardItemDetailsKeys_m802 (UnityAdsIos_t158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIos::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern "C" void UnityAdsIos_setLogLevel_m803 (UnityAdsIos_t158 * __this, int32_t ___logLevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
