﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t4145;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t208;

// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_4__ctor_m25290_gshared (InvokableCall_4_t4145 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, MethodInfo* method);
#define InvokableCall_4__ctor_m25290(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_4_t4145 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_4__ctor_m25290_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern "C" void InvokableCall_4_Invoke_m25291_gshared (InvokableCall_4_t4145 * __this, ObjectU5BU5D_t208* ___args, MethodInfo* method);
#define InvokableCall_4_Invoke_m25291(__this, ___args, method) (( void (*) (InvokableCall_4_t4145 *, ObjectU5BU5D_t208*, MethodInfo*))InvokableCall_4_Invoke_m25291_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_4_Find_m25292_gshared (InvokableCall_4_t4145 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, MethodInfo* method);
#define InvokableCall_4_Find_m25292(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_4_t4145 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_4_Find_m25292_gshared)(__this, ___targetObj, ___method, method)
