﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t1429;
// System.String
struct String_t;

// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C" void AddComponentMenu__ctor_m6201 (AddComponentMenu_t1429 * __this, String_t* ___menuName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C" void AddComponentMenu__ctor_m6233 (AddComponentMenu_t1429 * __this, String_t* ___menuName, int32_t ___order, MethodInfo* method) IL2CPP_METHOD_ATTR;
