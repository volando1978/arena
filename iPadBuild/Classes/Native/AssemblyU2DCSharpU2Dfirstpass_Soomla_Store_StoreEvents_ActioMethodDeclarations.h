﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreEvents/Action
struct Action_t88;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Soomla.Store.StoreEvents/Action::.ctor(System.Object,System.IntPtr)
extern "C" void Action__ctor_m418 (Action_t88 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/Action::Invoke()
extern "C" void Action_Invoke_m419 (Action_t88 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_Action_t88(Il2CppObject* delegate);
// System.IAsyncResult Soomla.Store.StoreEvents/Action::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * Action_BeginInvoke_m420 (Action_t88 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/Action::EndInvoke(System.IAsyncResult)
extern "C" void Action_EndInvoke_m421 (Action_t88 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
