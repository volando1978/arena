﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t3855;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C" void DefaultComparer__ctor_m21415_gshared (DefaultComparer_t3855 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m21415(__this, method) (( void (*) (DefaultComparer_t3855 *, MethodInfo*))DefaultComparer__ctor_m21415_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m21416_gshared (DefaultComparer_t3855 * __this, Vector3_t758  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m21416(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3855 *, Vector3_t758 , MethodInfo*))DefaultComparer_GetHashCode_m21416_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m21417_gshared (DefaultComparer_t3855 * __this, Vector3_t758  ___x, Vector3_t758  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m21417(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3855 *, Vector3_t758 , Vector3_t758 , MethodInfo*))DefaultComparer_Equals_m21417_gshared)(__this, ___x, ___y, method)
