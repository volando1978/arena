﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Int32,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey18_3_t3736;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey19_3_t3737  : public Object_t
{
	// T1 GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>::val1
	int32_t ___val1_0;
	// T2 GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>::val2
	Object_t * ___val2_1;
	// T3 GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>::val3
	Object_t * ___val3_2;
	// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<T1,T2,T3> GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3/<ToOnGameThread>c__AnonStorey19`3<System.Int32,System.Object,System.Object>::<>f__ref$24
	U3CToOnGameThreadU3Ec__AnonStorey18_3_t3736 * ___U3CU3Ef__refU2424_3;
};
