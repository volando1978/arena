﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>
struct InternalEnumerator_1_t3831;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21081_gshared (InternalEnumerator_1_t3831 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m21081(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3831 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m21081_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21082_gshared (InternalEnumerator_1_t3831 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21082(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3831 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21082_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21083_gshared (InternalEnumerator_1_t3831 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21083(__this, method) (( void (*) (InternalEnumerator_1_t3831 *, MethodInfo*))InternalEnumerator_1_Dispose_m21083_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21084_gshared (InternalEnumerator_1_t3831 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21084(__this, method) (( bool (*) (InternalEnumerator_1_t3831 *, MethodInfo*))InternalEnumerator_1_MoveNext_m21084_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::get_Current()
extern "C" Matrix4x4_t997  InternalEnumerator_1_get_Current_m21085_gshared (InternalEnumerator_1_t3831 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21085(__this, method) (( Matrix4x4_t997  (*) (InternalEnumerator_1_t3831 *, MethodInfo*))InternalEnumerator_1_get_Current_m21085_gshared)(__this, method)
