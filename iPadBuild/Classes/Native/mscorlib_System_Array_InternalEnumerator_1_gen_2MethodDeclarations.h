﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Type>
struct InternalEnumerator_1_t3419;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Type>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15499(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3419 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Type>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3419 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Type>::Dispose()
#define InternalEnumerator_1_Dispose_m15501(__this, method) (( void (*) (InternalEnumerator_1_t3419 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Type>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15502(__this, method) (( bool (*) (InternalEnumerator_1_t3419 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Type>::get_Current()
#define InternalEnumerator_1_get_Current_m15503(__this, method) (( Type_t * (*) (InternalEnumerator_1_t3419 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
