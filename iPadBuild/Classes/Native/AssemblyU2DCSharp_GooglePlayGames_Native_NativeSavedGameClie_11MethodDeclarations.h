﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2<System.Int32,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3767;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2<System.Int32,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4B_2__ctor_m20313_gshared (U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3767 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey4B_2__ctor_m20313(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3767 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey4B_2__ctor_m20313_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2<System.Int32,System.Object>::<>m__4F(T1,T2)
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4B_2_U3CU3Em__4F_m20314_gshared (U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3767 * __this, int32_t ___val1, Object_t * ___val2, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey4B_2_U3CU3Em__4F_m20314(__this, ___val1, ___val2, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3767 *, int32_t, Object_t *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey4B_2_U3CU3Em__4F_m20314_gshared)(__this, ___val1, ___val2, method)
