﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t686;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey4D__ctor_m2542 (U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D::<>m__57(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_U3CU3Em__57_m2543 (U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633 * __this, PlayerSelectUIResponse_t686 * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
