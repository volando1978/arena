﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// enemyController
struct enemyController_t785;
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void enemyController::.ctor()
extern "C" void enemyController__ctor_m3381 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::.cctor()
extern "C" void enemyController__cctor_m3382 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::Awake()
extern "C" void enemyController_Awake_m3383 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::initHolesTutorial()
extern "C" void enemyController_initHolesTutorial_m3384 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::initHolesGame()
extern "C" void enemyController_initHolesGame_m3385 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::updatePosHoles()
extern "C" void enemyController_updatePosHoles_m3386 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::moveEnemies()
extern "C" void enemyController_moveEnemies_m3387 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::deleteHoles()
extern "C" void enemyController_deleteHoles_m3388 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::createSpider()
extern "C" void enemyController_createSpider_m3389 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::deleteSpider(UnityEngine.GameObject)
extern "C" void enemyController_deleteSpider_m3390 (enemyController_t785 * __this, GameObject_t144 * ___sp, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::createSnake()
extern "C" void enemyController_createSnake_m3391 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::deleteSnake(UnityEngine.GameObject)
extern "C" void enemyController_deleteSnake_m3392 (enemyController_t785 * __this, GameObject_t144 * ___sn, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::clearEnemies()
extern "C" void enemyController_clearEnemies_m3393 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 enemyController::getNumberEnemies()
extern "C" int32_t enemyController_getNumberEnemies_m3394 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject enemyController::getClosest(UnityEngine.Vector3)
extern "C" GameObject_t144 * enemyController_getClosest_m3395 (enemyController_t785 * __this, Vector3_t758  ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyController::setMaxNumSnakes()
extern "C" void enemyController_setMaxNumSnakes_m3396 (enemyController_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
