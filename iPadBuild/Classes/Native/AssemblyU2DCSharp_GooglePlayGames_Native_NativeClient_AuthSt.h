﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.NativeClient/AuthState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_AuthSt.h"
// GooglePlayGames.Native.NativeClient/AuthState
struct  AuthState_t523 
{
	// System.Int32 GooglePlayGames.Native.NativeClient/AuthState::value__
	int32_t ___value___1;
};
