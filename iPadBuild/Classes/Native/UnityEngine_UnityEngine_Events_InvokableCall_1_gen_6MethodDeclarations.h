﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t4150;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t721;
// System.Object[]
struct ObjectU5BU5D_t208;

// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.InvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_genMethodDeclarations.h"
#define InvokableCall_1__ctor_m25317(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t4150 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_1__ctor_m20922_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m25318(__this, ___callback, method) (( void (*) (InvokableCall_1_t4150 *, UnityAction_1_t721 *, MethodInfo*))InvokableCall_1__ctor_m20923_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m25319(__this, ___args, method) (( void (*) (InvokableCall_1_t4150 *, ObjectU5BU5D_t208*, MethodInfo*))InvokableCall_1_Invoke_m20924_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m25320(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t4150 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_1_Find_m20925_gshared)(__this, ___targetObj, ___method, method)
