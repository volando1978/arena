﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameControl/<waitTime>c__IteratorA
struct U3CwaitTimeU3Ec__IteratorA_t798;
// System.Object
struct Object_t;

// System.Void gameControl/<waitTime>c__IteratorA::.ctor()
extern "C" void U3CwaitTimeU3Ec__IteratorA__ctor_m3431 (U3CwaitTimeU3Ec__IteratorA_t798 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<waitTime>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432 (U3CwaitTimeU3Ec__IteratorA_t798 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<waitTime>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433 (U3CwaitTimeU3Ec__IteratorA_t798 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameControl/<waitTime>c__IteratorA::MoveNext()
extern "C" bool U3CwaitTimeU3Ec__IteratorA_MoveNext_m3434 (U3CwaitTimeU3Ec__IteratorA_t798 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<waitTime>c__IteratorA::Dispose()
extern "C" void U3CwaitTimeU3Ec__IteratorA_Dispose_m3435 (U3CwaitTimeU3Ec__IteratorA_t798 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<waitTime>c__IteratorA::Reset()
extern "C" void U3CwaitTimeU3Ec__IteratorA_Reset_m3436 (U3CwaitTimeU3Ec__IteratorA_t798 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
