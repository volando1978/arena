﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`2<System.String,System.String>
struct UnityDictionary_2_t3403;
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct List_1_t163;
// System.String
struct String_t;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t4254;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct ICollection_1_t262;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t4255;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerator_1_t4256;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::.ctor()
// UnityEngine.UnityDictionary`2<System.Object,System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_UnityDictionary_2__0MethodDeclarations.h"
#define UnityDictionary_2__ctor_m15263(__this, method) (( void (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2__ctor_m15264_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UnityDictionary`2<System.String,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1182(__this, method) (( Object_t * (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15265_gshared)(__this, method)
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.String>::get_KeyValuePairs()
// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::set_KeyValuePairs(System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<K,V>>)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::SetKeyValuePair(K,V)
// V UnityEngine.UnityDictionary`2<System.String,System.String>::get_Item(K)
#define UnityDictionary_2_get_Item_m1192(__this, ___key, method) (( String_t* (*) (UnityDictionary_2_t3403 *, String_t*, MethodInfo*))UnityDictionary_2_get_Item_m15266_gshared)(__this, ___key, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::set_Item(K,V)
#define UnityDictionary_2_set_Item_m1193(__this, ___key, ___value, method) (( void (*) (UnityDictionary_2_t3403 *, String_t*, String_t*, MethodInfo*))UnityDictionary_2_set_Item_m15267_gshared)(__this, ___key, ___value, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::Add(K,V)
#define UnityDictionary_2_Add_m1194(__this, ___key, ___value, method) (( void (*) (UnityDictionary_2_t3403 *, String_t*, String_t*, MethodInfo*))UnityDictionary_2_Add_m15268_gshared)(__this, ___key, ___value, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::Add(System.Collections.Generic.KeyValuePair`2<K,V>)
#define UnityDictionary_2_Add_m1186(__this, ___kvp, method) (( void (*) (UnityDictionary_2_t3403 *, KeyValuePair_2_t196 , MethodInfo*))UnityDictionary_2_Add_m15269_gshared)(__this, ___kvp, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::TryGetValue(K,V&)
#define UnityDictionary_2_TryGetValue_m1195(__this, ___key, ___value, method) (( bool (*) (UnityDictionary_2_t3403 *, String_t*, String_t**, MethodInfo*))UnityDictionary_2_TryGetValue_m15270_gshared)(__this, ___key, ___value, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::Remove(System.Collections.Generic.KeyValuePair`2<K,V>)
#define UnityDictionary_2_Remove_m1190(__this, ___item, method) (( bool (*) (UnityDictionary_2_t3403 *, KeyValuePair_2_t196 , MethodInfo*))UnityDictionary_2_Remove_m15271_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::Remove(K)
#define UnityDictionary_2_Remove_m1183(__this, ___key, method) (( bool (*) (UnityDictionary_2_t3403 *, String_t*, MethodInfo*))UnityDictionary_2_Remove_m15272_gshared)(__this, ___key, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::Clear()
#define UnityDictionary_2_Clear_m1187(__this, method) (( void (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_Clear_m15273_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::ContainsKey(K)
#define UnityDictionary_2_ContainsKey_m1196(__this, ___key, method) (( bool (*) (UnityDictionary_2_t3403 *, String_t*, MethodInfo*))UnityDictionary_2_ContainsKey_m15274_gshared)(__this, ___key, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::Contains(System.Collections.Generic.KeyValuePair`2<K,V>)
#define UnityDictionary_2_Contains_m1188(__this, ___kvp, method) (( bool (*) (UnityDictionary_2_t3403 *, KeyValuePair_2_t196 , MethodInfo*))UnityDictionary_2_Contains_m15275_gshared)(__this, ___kvp, method)
// System.Int32 UnityEngine.UnityDictionary`2<System.String,System.String>::get_Count()
#define UnityDictionary_2_get_Count_m1184(__this, method) (( int32_t (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_Count_m15276_gshared)(__this, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.String>::CopyTo(System.Collections.Generic.KeyValuePair`2<K,V>[],System.Int32)
#define UnityDictionary_2_CopyTo_m1189(__this, ___array, ___index, method) (( void (*) (UnityDictionary_2_t3403 *, KeyValuePair_2U5BU5D_t4255*, int32_t, MethodInfo*))UnityDictionary_2_CopyTo_m15277_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.String>::GetEnumerator()
#define UnityDictionary_2_GetEnumerator_m1191(__this, method) (( Object_t* (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_GetEnumerator_m15278_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<K> UnityEngine.UnityDictionary`2<System.String,System.String>::get_Keys()
#define UnityDictionary_2_get_Keys_m1197(__this, method) (( Object_t* (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_Keys_m15279_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<V> UnityEngine.UnityDictionary`2<System.String,System.String>::get_Values()
#define UnityDictionary_2_get_Values_m1198(__this, method) (( Object_t* (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_Values_m15280_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.String>::get_Items()
#define UnityDictionary_2_get_Items_m15281(__this, method) (( Object_t* (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_Items_m15282_gshared)(__this, method)
// V UnityEngine.UnityDictionary`2<System.String,System.String>::get_SyncRoot()
#define UnityDictionary_2_get_SyncRoot_m15283(__this, method) (( String_t* (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_SyncRoot_m15284_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::get_IsFixedSize()
#define UnityDictionary_2_get_IsFixedSize_m15285(__this, method) (( bool (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_IsFixedSize_m15286_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::get_IsReadOnly()
#define UnityDictionary_2_get_IsReadOnly_m1185(__this, method) (( bool (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_IsReadOnly_m15287_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.String>::get_IsSynchronized()
#define UnityDictionary_2_get_IsSynchronized_m15288(__this, method) (( bool (*) (UnityDictionary_2_t3403 *, MethodInfo*))UnityDictionary_2_get_IsSynchronized_m15289_gshared)(__this, method)
// K UnityEngine.UnityDictionary`2<System.String,System.String>::<get_Keys>m__0(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3Cget_KeysU3Em__0_m15290(__this /* static, unused */, ___x, method) (( String_t* (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t164 *, MethodInfo*))UnityDictionary_2_U3Cget_KeysU3Em__0_m15291_gshared)(__this /* static, unused */, ___x, method)
// V UnityEngine.UnityDictionary`2<System.String,System.String>::<get_Values>m__1(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3Cget_ValuesU3Em__1_m15292(__this /* static, unused */, ___x, method) (( String_t* (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t164 *, MethodInfo*))UnityDictionary_2_U3Cget_ValuesU3Em__1_m15293_gshared)(__this /* static, unused */, ___x, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2<System.String,System.String>::<get_Items>m__2(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3Cget_ItemsU3Em__2_m15294(__this /* static, unused */, ___x, method) (( KeyValuePair_2_t196  (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t164 *, MethodInfo*))UnityDictionary_2_U3Cget_ItemsU3Em__2_m15295_gshared)(__this /* static, unused */, ___x, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2<System.String,System.String>::<CopyTo>m__6(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3CCopyToU3Em__6_m15296(__this /* static, unused */, ___x, method) (( KeyValuePair_2_t196  (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t164 *, MethodInfo*))UnityDictionary_2_U3CCopyToU3Em__6_m15297_gshared)(__this /* static, unused */, ___x, method)
