﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t1806_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1807_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1808_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t1809_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t1810_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var;
void g_System_Core_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		AssemblyInformationalVersionAttribute_t1806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3623);
		SatelliteContractVersionAttribute_t1807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3624);
		AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2347);
		AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2346);
		AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2345);
		AssemblyDefaultAliasAttribute_t1808_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3625);
		AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2343);
		AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2342);
		AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2348);
		RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		DebuggableAttribute_t1809_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3626);
		CompilationRelaxationsAttribute_t1810_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3627);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3628);
		AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3629);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3631);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 18;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1806 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1806 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1806_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m7688(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1807 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1807 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1807_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m7689(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t1423 * tmp;
		tmp = (AssemblyCopyrightAttribute_t1423 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m6179(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t1422 * tmp;
		tmp = (AssemblyProductAttribute_t1422 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m6178(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t1421 * tmp;
		tmp = (AssemblyCompanyAttribute_t1421 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m6177(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1808 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1808 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1808_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m7690(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t1419 * tmp;
		tmp = (AssemblyDescriptionAttribute_t1419 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m6175(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t1418 * tmp;
		tmp = (AssemblyTitleAttribute_t1418 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m6174(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t1424 * tmp;
		tmp = (AssemblyFileVersionAttribute_t1424 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m6180(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t241 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t241 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1069(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070(tmp, true, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t1809 * tmp;
		tmp = (DebuggableAttribute_t1809 *)il2cpp_codegen_object_new (DebuggableAttribute_t1809_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m7691(tmp, 2, NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t1810 * tmp;
		tmp = (CompilationRelaxationsAttribute_t1810 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t1810_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m7692(tmp, 8, NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1811 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1811 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7693(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1812 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1812 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7694(tmp, true, NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, true, NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1814 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1814 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7696(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ExtensionAttribute_t242_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 69, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t1801_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* MonoTODOAttribute_t1801_il2cpp_TypeInfo_var;
void HashSet_1_t1815_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m7718(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1801_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3632);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1801 * tmp;
		tmp = (MonoTODOAttribute_t1801 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1801_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m7679(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1801_il2cpp_TypeInfo_var;
void HashSet_1_t1815_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m7719(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1801_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3632);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1801 * tmp;
		tmp = (MonoTODOAttribute_t1801 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1801_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m7679(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Cast_m7730(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m7731(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Contains_m7732(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Count_m7733(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Except_m7734(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Except_m7735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateExceptIterator_m7736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_First_m7737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_FirstOrDefault_m7738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_LongCount_m7739(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Select_m7740(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateSelectIterator_m7741(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToArray_m7742(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToDictionary_m7743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToDictionary_m7744(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToDictionary_m7745(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToList_m7746(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Where_m7747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m7748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Function_1_t1819_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Function_1_t1819_CustomAttributesCacheGenerator_Function_1_U3CIdentityU3Em__77_m7750(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7752(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7753(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7754(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7755(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m7760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m7761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m7764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m7767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m7768(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m7771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7775(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t1805_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_System_Core_Assembly_AttributeGenerators[53] = 
{
	NULL,
	g_System_Core_Assembly_CustomAttributesCacheGenerator,
	ExtensionAttribute_t242_CustomAttributesCacheGenerator,
	MonoTODOAttribute_t1801_CustomAttributesCacheGenerator,
	HashSet_1_t1815_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m7718,
	HashSet_1_t1815_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m7719,
	Enumerable_t219_CustomAttributesCacheGenerator,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Cast_m7730,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m7731,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Contains_m7732,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Count_m7733,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Except_m7734,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Except_m7735,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateExceptIterator_m7736,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_First_m7737,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_FirstOrDefault_m7738,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_LongCount_m7739,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Select_m7740,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateSelectIterator_m7741,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToArray_m7742,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToDictionary_m7743,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToDictionary_m7744,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToDictionary_m7745,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_ToList_m7746,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_Where_m7747,
	Enumerable_t219_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m7748,
	Function_1_t1819_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	Function_1_t1819_CustomAttributesCacheGenerator_Function_1_U3CIdentityU3Em__77_m7750,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7752,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7753,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7754,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7755,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7757,
	U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator,
	U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7759,
	U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m7760,
	U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m7761,
	U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7762,
	U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_CustomAttributesCacheGenerator_U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m7764,
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator,
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7766,
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m7767,
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m7768,
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7769,
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_CustomAttributesCacheGenerator_U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m7771,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7773,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7774,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7775,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7776,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7778,
	U3CPrivateImplementationDetailsU3E_t1805_CustomAttributesCacheGenerator,
};
