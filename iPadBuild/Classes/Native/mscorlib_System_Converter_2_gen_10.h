﻿#pragma once
#include <stdint.h>
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct  Converter_2_t3462  : public MulticastDelegate_t22
{
};
