﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LaserControl/<showLaser>c__Iterator4
struct U3CshowLaserU3Ec__Iterator4_t746;
// System.Object
struct Object_t;

// System.Void LaserControl/<showLaser>c__Iterator4::.ctor()
extern "C" void U3CshowLaserU3Ec__Iterator4__ctor_m3229 (U3CshowLaserU3Ec__Iterator4_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaserControl/<showLaser>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CshowLaserU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3230 (U3CshowLaserU3Ec__Iterator4_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaserControl/<showLaser>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CshowLaserU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3231 (U3CshowLaserU3Ec__Iterator4_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaserControl/<showLaser>c__Iterator4::MoveNext()
extern "C" bool U3CshowLaserU3Ec__Iterator4_MoveNext_m3232 (U3CshowLaserU3Ec__Iterator4_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserControl/<showLaser>c__Iterator4::Dispose()
extern "C" void U3CshowLaserU3Ec__Iterator4_Dispose_m3233 (U3CshowLaserU3Ec__Iterator4_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserControl/<showLaser>c__Iterator4::Reset()
extern "C" void U3CshowLaserU3Ec__Iterator4_Reset_m3234 (U3CshowLaserU3Ec__Iterator4_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
