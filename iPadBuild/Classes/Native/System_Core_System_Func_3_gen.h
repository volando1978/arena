﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object
struct Object_t;
// System.Action`1<System.Object>
struct Action_1_t3422;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`3<System.Object,System.Action`1<System.Object>,System.Collections.IEnumerator>
struct  Func_3_t3633  : public MulticastDelegate_t22
{
};
