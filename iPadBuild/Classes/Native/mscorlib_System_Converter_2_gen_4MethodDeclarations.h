﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Converter_2_t3437;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Converter_2__ctor_m15610_gshared (Converter_2_t3437 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Converter_2__ctor_m15610(__this, ___object, ___method, method) (( void (*) (Converter_2_t3437 *, Object_t *, IntPtr_t, MethodInfo*))Converter_2__ctor_m15610_gshared)(__this, ___object, ___method, method)
// TOutput System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TInput)
extern "C" KeyValuePair_2_t3407  Converter_2_Invoke_m15612_gshared (Converter_2_t3437 * __this, Object_t * ___input, MethodInfo* method);
#define Converter_2_Invoke_m15612(__this, ___input, method) (( KeyValuePair_2_t3407  (*) (Converter_2_t3437 *, Object_t *, MethodInfo*))Converter_2_Invoke_m15612_gshared)(__this, ___input, method)
// System.IAsyncResult System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern "C" Object_t * Converter_2_BeginInvoke_m15614_gshared (Converter_2_t3437 * __this, Object_t * ___input, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Converter_2_BeginInvoke_m15614(__this, ___input, ___callback, ___object, method) (( Object_t * (*) (Converter_2_t3437 *, Object_t *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Converter_2_BeginInvoke_m15614_gshared)(__this, ___input, ___callback, ___object, method)
// TOutput System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3407  Converter_2_EndInvoke_m15616_gshared (Converter_2_t3437 * __this, Object_t * ___result, MethodInfo* method);
#define Converter_2_EndInvoke_m15616(__this, ___result, method) (( KeyValuePair_2_t3407  (*) (Converter_2_t3437 *, Object_t *, MethodInfo*))Converter_2_EndInvoke_m15616_gshared)(__this, ___result, method)
