﻿#pragma once
#include <stdint.h>
// ObjectKvp[]
struct ObjectKvpU5BU5D_t3472;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<ObjectKvp>
struct  List_1_t9  : public Object_t
{
	// T[] System.Collections.Generic.List`1<ObjectKvp>::_items
	ObjectKvpU5BU5D_t3472* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::_version
	int32_t ____version_3;
};
struct List_1_t9_StaticFields{
	// T[] System.Collections.Generic.List`1<ObjectKvp>::EmptyArray
	ObjectKvpU5BU5D_t3472* ___EmptyArray_4;
};
