﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>
struct List_1_t117;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>
struct  Enumerator_t228 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::l
	List_1_t117 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::current
	VirtualCategory_t126 * ___current_3;
};
