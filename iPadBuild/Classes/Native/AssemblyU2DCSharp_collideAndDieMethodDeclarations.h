﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// collideAndDie
struct collideAndDie_t779;
// UnityEngine.Collider
struct Collider_t900;

// System.Void collideAndDie::.ctor()
extern "C" void collideAndDie__ctor_m3360 (collideAndDie_t779 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void collideAndDie::OnExplosion()
extern "C" void collideAndDie_OnExplosion_m3361 (collideAndDie_t779 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void collideAndDie::OnTriggerEnter(UnityEngine.Collider)
extern "C" void collideAndDie_OnTriggerEnter_m3362 (collideAndDie_t779 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
