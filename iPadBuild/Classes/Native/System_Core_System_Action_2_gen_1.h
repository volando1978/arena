﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`2<Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG>
struct  Action_2_t94  : public MulticastDelegate_t22
{
};
