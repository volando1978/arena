﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>
struct KeyCollection_t3672;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t343;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.String[]
struct StringU5BU5D_t169;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_47.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_15MethodDeclarations.h"
#define KeyCollection__ctor_m19061(__this, ___dictionary, method) (( void (*) (KeyCollection_t3672 *, Dictionary_2_t343 *, MethodInfo*))KeyCollection__ctor_m18968_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m19062(__this, ___item, method) (( void (*) (KeyCollection_t3672 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18969_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m19063(__this, method) (( void (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18970_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m19064(__this, ___item, method) (( bool (*) (KeyCollection_t3672 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18971_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m19065(__this, ___item, method) (( bool (*) (KeyCollection_t3672 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18972_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m19066(__this, method) (( Object_t* (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18973_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m19067(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3672 *, Array_t *, int32_t, MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m18974_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m19068(__this, method) (( Object_t * (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18975_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m19069(__this, method) (( bool (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m19070(__this, method) (( bool (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18977_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m19071(__this, method) (( Object_t * (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m18978_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m19072(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3672 *, StringU5BU5D_t169*, int32_t, MethodInfo*))KeyCollection_CopyTo_m18979_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::GetEnumerator()
#define KeyCollection_GetEnumerator_m19073(__this, method) (( Enumerator_t4360  (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_GetEnumerator_m18980_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::get_Count()
#define KeyCollection_get_Count_m19074(__this, method) (( int32_t (*) (KeyCollection_t3672 *, MethodInfo*))KeyCollection_get_Count_m18981_gshared)(__this, method)
