﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<ObjectKvp>
struct List_1_t9;
// System.Object
struct Object_t;
// ObjectKvp
struct ObjectKvp_t6;
// System.Collections.Generic.IEnumerable`1<ObjectKvp>
struct IEnumerable_1_t4278;
// System.Collections.Generic.IEnumerator`1<ObjectKvp>
struct IEnumerator_1_t4279;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<ObjectKvp>
struct ICollection_1_t4280;
// System.Collections.ObjectModel.ReadOnlyCollection`1<ObjectKvp>
struct ReadOnlyCollection_1_t3474;
// ObjectKvp[]
struct ObjectKvpU5BU5D_t3472;
// System.Predicate`1<ObjectKvp>
struct Predicate_1_t186;
// System.Action`1<ObjectKvp>
struct Action_1_t3475;
// System.Comparison`1<ObjectKvp>
struct Comparison_1_t3477;
// System.Collections.Generic.List`1/Enumerator<ObjectKvp>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"

// System.Void System.Collections.Generic.List`1<ObjectKvp>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m840(__this, method) (( void (*) (List_1_t9 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16012(__this, ___collection, method) (( void (*) (List_1_t9 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::.ctor(System.Int32)
#define List_1__ctor_m16013(__this, ___capacity, method) (( void (*) (List_1_t9 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::.cctor()
#define List_1__cctor_m16014(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<ObjectKvp>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16015(__this, method) (( Object_t* (*) (List_1_t9 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16016(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t9 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16017(__this, method) (( Object_t * (*) (List_1_t9 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16018(__this, ___item, method) (( int32_t (*) (List_1_t9 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16019(__this, ___item, method) (( bool (*) (List_1_t9 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16020(__this, ___item, method) (( int32_t (*) (List_1_t9 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16021(__this, ___index, ___item, method) (( void (*) (List_1_t9 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16022(__this, ___item, method) (( void (*) (List_1_t9 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<ObjectKvp>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16023(__this, method) (( bool (*) (List_1_t9 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ObjectKvp>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16024(__this, method) (( bool (*) (List_1_t9 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ObjectKvp>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16025(__this, method) (( Object_t * (*) (List_1_t9 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16026(__this, method) (( bool (*) (List_1_t9 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16027(__this, method) (( bool (*) (List_1_t9 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16028(__this, ___index, method) (( Object_t * (*) (List_1_t9 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16029(__this, ___index, ___value, method) (( void (*) (List_1_t9 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::Add(T)
#define List_1_Add_m16030(__this, ___item, method) (( void (*) (List_1_t9 *, ObjectKvp_t6 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16031(__this, ___newCount, method) (( void (*) (List_1_t9 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16032(__this, ___collection, method) (( void (*) (List_1_t9 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16033(__this, ___enumerable, method) (( void (*) (List_1_t9 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16034(__this, ___collection, method) (( void (*) (List_1_t9 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<ObjectKvp>::AsReadOnly()
#define List_1_AsReadOnly_m16035(__this, method) (( ReadOnlyCollection_1_t3474 * (*) (List_1_t9 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::Clear()
#define List_1_Clear_m16036(__this, method) (( void (*) (List_1_t9 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ObjectKvp>::Contains(T)
#define List_1_Contains_m16037(__this, ___item, method) (( bool (*) (List_1_t9 *, ObjectKvp_t6 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16038(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t9 *, ObjectKvpU5BU5D_t3472*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<ObjectKvp>::Find(System.Predicate`1<T>)
#define List_1_Find_m16039(__this, ___match, method) (( ObjectKvp_t6 * (*) (List_1_t9 *, Predicate_1_t186 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16040(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t186 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m844(__this, ___match, method) (( int32_t (*) (List_1_t9 *, Predicate_1_t186 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16041(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t9 *, int32_t, int32_t, Predicate_1_t186 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m16042(__this, ___action, method) (( void (*) (List_1_t9 *, Action_1_t3475 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<ObjectKvp>::GetEnumerator()
#define List_1_GetEnumerator_m16043(__this, method) (( Enumerator_t3476  (*) (List_1_t9 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::IndexOf(T)
#define List_1_IndexOf_m16044(__this, ___item, method) (( int32_t (*) (List_1_t9 *, ObjectKvp_t6 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16045(__this, ___start, ___delta, method) (( void (*) (List_1_t9 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16046(__this, ___index, method) (( void (*) (List_1_t9 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::Insert(System.Int32,T)
#define List_1_Insert_m16047(__this, ___index, ___item, method) (( void (*) (List_1_t9 *, int32_t, ObjectKvp_t6 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16048(__this, ___collection, method) (( void (*) (List_1_t9 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<ObjectKvp>::Remove(T)
#define List_1_Remove_m16049(__this, ___item, method) (( bool (*) (List_1_t9 *, ObjectKvp_t6 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16050(__this, ___match, method) (( int32_t (*) (List_1_t9 *, Predicate_1_t186 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16051(__this, ___index, method) (( void (*) (List_1_t9 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::Reverse()
#define List_1_Reverse_m16052(__this, method) (( void (*) (List_1_t9 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::Sort()
#define List_1_Sort_m16053(__this, method) (( void (*) (List_1_t9 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16054(__this, ___comparison, method) (( void (*) (List_1_t9 *, Comparison_1_t3477 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<ObjectKvp>::ToArray()
#define List_1_ToArray_m16055(__this, method) (( ObjectKvpU5BU5D_t3472* (*) (List_1_t9 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::TrimExcess()
#define List_1_TrimExcess_m16056(__this, method) (( void (*) (List_1_t9 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::get_Capacity()
#define List_1_get_Capacity_m16057(__this, method) (( int32_t (*) (List_1_t9 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16058(__this, ___value, method) (( void (*) (List_1_t9 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<ObjectKvp>::get_Count()
#define List_1_get_Count_m16059(__this, method) (( int32_t (*) (List_1_t9 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<ObjectKvp>::get_Item(System.Int32)
#define List_1_get_Item_m16060(__this, ___index, method) (( ObjectKvp_t6 * (*) (List_1_t9 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ObjectKvp>::set_Item(System.Int32,T)
#define List_1_set_Item_m16061(__this, ___index, ___value, method) (( void (*) (List_1_t9 *, int32_t, ObjectKvp_t6 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
