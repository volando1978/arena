﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t103;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t170;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.String[]
struct StringU5BU5D_t169;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Collections.Generic.HashSet`1<System.String>::.ctor()
// System.Collections.Generic.HashSet`1<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_gen_1MethodDeclarations.h"
#define HashSet_1__ctor_m983(__this, method) (( void (*) (HashSet_1_t103 *, MethodInfo*))HashSet_1__ctor_m17601_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1__ctor_m17602(__this, ___collection, ___comparer, method) (( void (*) (HashSet_1_t103 *, Object_t*, Object_t*, MethodInfo*))HashSet_1__ctor_m17603_gshared)(__this, ___collection, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m17604(__this, ___info, ___context, method) (( void (*) (HashSet_1_t103 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1__ctor_m17605_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.String>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17606(__this, method) (( Object_t* (*) (HashSet_1_t103 *, MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17607_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17608(__this, method) (( bool (*) (HashSet_1_t103 *, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17609_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m17610(__this, ___array, ___index, method) (( void (*) (HashSet_1_t103 *, StringU5BU5D_t169*, int32_t, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m17611_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17612(__this, ___item, method) (( void (*) (HashSet_1_t103 *, String_t*, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17613_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m17614(__this, method) (( Object_t * (*) (HashSet_1_t103 *, MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m17615_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.String>::get_Count()
#define HashSet_1_get_Count_m17616(__this, method) (( int32_t (*) (HashSet_1_t103 *, MethodInfo*))HashSet_1_get_Count_m17617_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m17618(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t103 *, int32_t, Object_t*, MethodInfo*))HashSet_1_Init_m17619_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m17620(__this, ___size, method) (( void (*) (HashSet_1_t103 *, int32_t, MethodInfo*))HashSet_1_InitArrays_m17621_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m17622(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t103 *, int32_t, int32_t, String_t*, MethodInfo*))HashSet_1_SlotsContainsAt_m17623_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m17624(__this, ___array, ___index, method) (( void (*) (HashSet_1_t103 *, StringU5BU5D_t169*, int32_t, MethodInfo*))HashSet_1_CopyTo_m17625_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m17626(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t103 *, StringU5BU5D_t169*, int32_t, int32_t, MethodInfo*))HashSet_1_CopyTo_m17627_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::Resize()
#define HashSet_1_Resize_m17628(__this, method) (( void (*) (HashSet_1_t103 *, MethodInfo*))HashSet_1_Resize_m17629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.String>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m17630(__this, ___index, method) (( int32_t (*) (HashSet_1_t103 *, int32_t, MethodInfo*))HashSet_1_GetLinkHashCode_m17631_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.String>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m17632(__this, ___item, method) (( int32_t (*) (HashSet_1_t103 *, String_t*, MethodInfo*))HashSet_1_GetItemHashCode_m17633_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Add(T)
#define HashSet_1_Add_m989(__this, ___item, method) (( bool (*) (HashSet_1_t103 *, String_t*, MethodInfo*))HashSet_1_Add_m17634_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::Clear()
#define HashSet_1_Clear_m17635(__this, method) (( void (*) (HashSet_1_t103 *, MethodInfo*))HashSet_1_Clear_m17636_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Contains(T)
#define HashSet_1_Contains_m17637(__this, ___item, method) (( bool (*) (HashSet_1_t103 *, String_t*, MethodInfo*))HashSet_1_Contains_m17638_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Remove(T)
#define HashSet_1_Remove_m17639(__this, ___item, method) (( bool (*) (HashSet_1_t103 *, String_t*, MethodInfo*))HashSet_1_Remove_m17640_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m17641(__this, ___info, ___context, method) (( void (*) (HashSet_1_t103 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1_GetObjectData_m17642_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m17643(__this, ___sender, method) (( void (*) (HashSet_1_t103 *, Object_t *, MethodInfo*))HashSet_1_OnDeserialization_m17644_gshared)(__this, ___sender, method)
