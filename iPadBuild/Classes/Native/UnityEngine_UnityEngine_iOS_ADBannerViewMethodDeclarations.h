﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.iOS.ADBannerView
struct ADBannerView_t717;
// UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate
struct BannerWasLoadedDelegate_t974;
// UnityEngine.iOS.ADBannerView/Type
#include "UnityEngine_UnityEngine_iOS_ADBannerView_Type.h"
// UnityEngine.iOS.ADBannerView/Layout
#include "UnityEngine_UnityEngine_iOS_ADBannerView_Layout.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.iOS.ADBannerView::.ctor(UnityEngine.iOS.ADBannerView/Type,UnityEngine.iOS.ADBannerView/Layout)
extern "C" void ADBannerView__ctor_m4008 (ADBannerView_t717 * __this, int32_t ___type, int32_t ___layout, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADBannerView::.cctor()
extern "C" void ADBannerView__cctor_m7106 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADBannerView::add_onBannerWasLoaded(UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate)
extern "C" void ADBannerView_add_onBannerWasLoaded_m4010 (Object_t * __this /* static, unused */, BannerWasLoadedDelegate_t974 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADBannerView::remove_onBannerWasLoaded(UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate)
extern "C" void ADBannerView_remove_onBannerWasLoaded_m7107 (Object_t * __this /* static, unused */, BannerWasLoadedDelegate_t974 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.iOS.ADBannerView::Native_CreateBanner(System.Int32,System.Int32)
extern "C" IntPtr_t ADBannerView_Native_CreateBanner_m7108 (Object_t * __this /* static, unused */, int32_t ___type, int32_t ___layout, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADBannerView::Native_DestroyBanner(System.IntPtr)
extern "C" void ADBannerView_Native_DestroyBanner_m7109 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADBannerView::Finalize()
extern "C" void ADBannerView_Finalize_m7110 (ADBannerView_t717 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADBannerView::FireBannerWasClicked()
extern "C" void ADBannerView_FireBannerWasClicked_m7111 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADBannerView::FireBannerWasLoaded()
extern "C" void ADBannerView_FireBannerWasLoaded_m7112 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
