﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.Object>::.ctor()
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_UnityKeyValuePair__0MethodDeclarations.h"
#define UnityKeyValuePair_2__ctor_m15861(__this, method) (( void (*) (UnityKeyValuePair_2_t3457 *, MethodInfo*))UnityKeyValuePair_2__ctor_m15545_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.Object>::.ctor(K,V)
#define UnityKeyValuePair_2__ctor_m15862(__this, ___key, ___value, method) (( void (*) (UnityKeyValuePair_2_t3457 *, String_t*, Object_t *, MethodInfo*))UnityKeyValuePair_2__ctor_m15546_gshared)(__this, ___key, ___value, method)
// K UnityEngine.UnityKeyValuePair`2<System.String,System.Object>::get_Key()
#define UnityKeyValuePair_2_get_Key_m15863(__this, method) (( String_t* (*) (UnityKeyValuePair_2_t3457 *, MethodInfo*))UnityKeyValuePair_2_get_Key_m15547_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.Object>::set_Key(K)
#define UnityKeyValuePair_2_set_Key_m15864(__this, ___value, method) (( void (*) (UnityKeyValuePair_2_t3457 *, String_t*, MethodInfo*))UnityKeyValuePair_2_set_Key_m15548_gshared)(__this, ___value, method)
// V UnityEngine.UnityKeyValuePair`2<System.String,System.Object>::get_Value()
#define UnityKeyValuePair_2_get_Value_m15865(__this, method) (( Object_t * (*) (UnityKeyValuePair_2_t3457 *, MethodInfo*))UnityKeyValuePair_2_get_Value_m15549_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.Object>::set_Value(V)
#define UnityKeyValuePair_2_set_Value_m15866(__this, ___value, method) (( void (*) (UnityKeyValuePair_2_t3457 *, Object_t *, MethodInfo*))UnityKeyValuePair_2_set_Value_m15550_gshared)(__this, ___value, method)
