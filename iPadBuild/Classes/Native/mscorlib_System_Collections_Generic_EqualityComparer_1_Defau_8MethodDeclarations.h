﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3972;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m23040_gshared (DefaultComparer_t3972 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m23040(__this, method) (( void (*) (DefaultComparer_t3972 *, MethodInfo*))DefaultComparer__ctor_m23040_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m23041_gshared (DefaultComparer_t3972 * __this, UIVertex_t1265  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m23041(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3972 *, UIVertex_t1265 , MethodInfo*))DefaultComparer_GetHashCode_m23041_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m23042_gshared (DefaultComparer_t3972 * __this, UIVertex_t1265  ___x, UIVertex_t1265  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m23042(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3972 *, UIVertex_t1265 , UIVertex_t1265 , MethodInfo*))DefaultComparer_Equals_m23042_gshared)(__this, ___x, ___y, method)
