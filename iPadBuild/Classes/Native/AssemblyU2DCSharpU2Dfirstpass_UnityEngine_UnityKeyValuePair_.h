﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct  UnityKeyValuePair_2_t164  : public Object_t
{
	// K UnityEngine.UnityKeyValuePair`2<System.String,System.String>::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_0;
	// V UnityEngine.UnityKeyValuePair`2<System.String,System.String>::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_1;
};
