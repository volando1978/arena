﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49
struct  U3CCommitUpdateU3Ec__AnonStorey49_t629  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata> GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49::callback
	Action_2_t612 * ___callback_0;
};
