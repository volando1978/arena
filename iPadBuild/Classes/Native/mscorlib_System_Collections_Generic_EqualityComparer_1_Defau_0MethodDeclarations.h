﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t3445;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void DefaultComparer__ctor_m15746_gshared (DefaultComparer_t3445 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m15746(__this, method) (( void (*) (DefaultComparer_t3445 *, MethodInfo*))DefaultComparer__ctor_m15746_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m15747_gshared (DefaultComparer_t3445 * __this, KeyValuePair_2_t3407  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m15747(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3445 *, KeyValuePair_2_t3407 , MethodInfo*))DefaultComparer_GetHashCode_m15747_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m15748_gshared (DefaultComparer_t3445 * __this, KeyValuePair_2_t3407  ___x, KeyValuePair_2_t3407  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m15748(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3445 *, KeyValuePair_2_t3407 , KeyValuePair_2_t3407 , MethodInfo*))DefaultComparer_Equals_m15748_gshared)(__this, ___x, ___y, method)
