﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey50
struct U3CAcceptInvitationU3Ec__AnonStorey50_t636;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey50::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey50__ctor_m2548 (U3CAcceptInvitationU3Ec__AnonStorey50_t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey50::<>m__5A(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey50_U3CU3Em__5A_m2549 (U3CAcceptInvitationU3Ec__AnonStorey50_t636 * __this, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method) IL2CPP_METHOD_ATTR;
