﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>
struct GenericEqualityComparer_1_t3669;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m19045_gshared (GenericEqualityComparer_1_t3669 * __this, MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m19045(__this, method) (( void (*) (GenericEqualityComparer_1_t3669 *, MethodInfo*))GenericEqualityComparer_1__ctor_m19045_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m19046_gshared (GenericEqualityComparer_1_t3669 * __this, uint32_t ___obj, MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m19046(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3669 *, uint32_t, MethodInfo*))GenericEqualityComparer_1_GetHashCode_m19046_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m19047_gshared (GenericEqualityComparer_1_t3669 * __this, uint32_t ___x, uint32_t ___y, MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m19047(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3669 *, uint32_t, uint32_t, MethodInfo*))GenericEqualityComparer_1_Equals_m19047_gshared)(__this, ___x, ___y, method)
