﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
struct KeyValuePair_2_t3618;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18282(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3618 *, String_t*, List_1_t178 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::get_Key()
#define KeyValuePair_2_get_Key_m18283(__this, method) (( String_t* (*) (KeyValuePair_2_t3618 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18284(__this, ___value, method) (( void (*) (KeyValuePair_2_t3618 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::get_Value()
#define KeyValuePair_2_get_Value_m18285(__this, method) (( List_1_t178 * (*) (KeyValuePair_2_t3618 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18286(__this, ___value, method) (( void (*) (KeyValuePair_2_t3618 *, List_1_t178 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::ToString()
#define KeyValuePair_2_ToString_m18287(__this, method) (( String_t* (*) (KeyValuePair_2_t3618 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
