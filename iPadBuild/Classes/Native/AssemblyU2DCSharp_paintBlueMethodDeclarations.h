﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// paintBlue
struct paintBlue_t827;

// System.Void paintBlue::.ctor()
extern "C" void paintBlue__ctor_m3619 (paintBlue_t827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void paintBlue::Start()
extern "C" void paintBlue_Start_m3620 (paintBlue_t827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
