﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Byte>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Byte>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m19758_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m19758(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m19758_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Byte>::<>m__76(T1,T2)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m19759_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724 * __this, Object_t * ___result1, uint8_t ___result2, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m19759(__this, ___result1, ___result2, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724 *, Object_t *, uint8_t, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m19759_gshared)(__this, ___result1, ___result2, method)
