﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t4081;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1534;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m24506(__this, ___dictionary, method) (( void (*) (Enumerator_t4081 *, Dictionary_2_t1534 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24507(__this, method) (( Object_t * (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24508(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24509(__this, method) (( Object_t * (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24510(__this, method) (( Object_t * (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m24511(__this, method) (( bool (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m24512(__this, method) (( KeyValuePair_2_t4079  (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24513(__this, method) (( String_t* (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24514(__this, method) (( GUIStyle_t724 * (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m24515(__this, method) (( void (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24516(__this, method) (( void (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m24517(__this, method) (( void (*) (Enumerator_t4081 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
