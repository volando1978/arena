﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3438  : public Array_t
{
};
// System.Object[]
// System.Object[]
struct  ObjectU5BU5D_t208  : public Array_t
{
};
// System.ValueType[]
// System.ValueType[]
struct  ValueTypeU5BU5D_t4544  : public Array_t
{
};
// System.String[]
// System.String[]
struct  StringU5BU5D_t169  : public Array_t
{
};
struct StringU5BU5D_t169_StaticFields{
};
// System.IConvertible[]
// System.IConvertible[]
struct  IConvertibleU5BU5D_t4545  : public Array_t
{
};
// System.IComparable[]
// System.IComparable[]
struct  IComparableU5BU5D_t4546  : public Array_t
{
};
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct  IEnumerableU5BU5D_t4547  : public Array_t
{
};
// System.ICloneable[]
// System.ICloneable[]
struct  ICloneableU5BU5D_t4548  : public Array_t
{
};
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct  IComparable_1U5BU5D_t4549  : public Array_t
{
};
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct  IEquatable_1U5BU5D_t4550  : public Array_t
{
};
// System.Type[]
// System.Type[]
struct  TypeU5BU5D_t1671  : public Array_t
{
};
struct TypeU5BU5D_t1671_StaticFields{
};
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct  IReflectU5BU5D_t4551  : public Array_t
{
};
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct  _TypeU5BU5D_t4552  : public Array_t
{
};
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct  MemberInfoU5BU5D_t2654  : public Array_t
{
};
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct  ICustomAttributeProviderU5BU5D_t4553  : public Array_t
{
};
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct  _MemberInfoU5BU5D_t4554  : public Array_t
{
};
// System.Int32[]
// System.Int32[]
struct  Int32U5BU5D_t107  : public Array_t
{
};
// System.IFormattable[]
// System.IFormattable[]
struct  IFormattableU5BU5D_t4555  : public Array_t
{
};
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct  IComparable_1U5BU5D_t4556  : public Array_t
{
};
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct  IEquatable_1U5BU5D_t4557  : public Array_t
{
};
// System.Double[]
// System.Double[]
struct  DoubleU5BU5D_t2877  : public Array_t
{
};
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct  IComparable_1U5BU5D_t4558  : public Array_t
{
};
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct  IEquatable_1U5BU5D_t4559  : public Array_t
{
};
// System.Char[]
// System.Char[]
struct  CharU5BU5D_t41  : public Array_t
{
};
struct CharU5BU5D_t41_StaticFields{
};
// System.UInt16[]
// System.UInt16[]
struct  UInt16U5BU5D_t195  : public Array_t
{
};
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct  IComparable_1U5BU5D_t4560  : public Array_t
{
};
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct  IEquatable_1U5BU5D_t4561  : public Array_t
{
};
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct  IComparable_1U5BU5D_t4562  : public Array_t
{
};
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct  IEquatable_1U5BU5D_t4563  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct  KeyValuePair_2U5BU5D_t4255  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct  KeyValuePair_2U5BU5D_t4268  : public Array_t
{
};
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct  LinkU5BU5D_t3281  : public Array_t
{
};
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct  DictionaryEntryU5BU5D_t4564  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>[]
// System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>[]
struct  KeyValuePair_2U5BU5D_t4286  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>[]
struct  KeyValuePair_2U5BU5D_t4298  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4303  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4301  : public Array_t
{
};
// System.Boolean[]
// System.Boolean[]
struct  BooleanU5BU5D_t108  : public Array_t
{
};
struct BooleanU5BU5D_t108_StaticFields{
};
// System.Byte[]
// System.Byte[]
struct  ByteU5BU5D_t350  : public Array_t
{
};
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct  IComparable_1U5BU5D_t4565  : public Array_t
{
};
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct  IEquatable_1U5BU5D_t4566  : public Array_t
{
};
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct  IComparable_1U5BU5D_t4567  : public Array_t
{
};
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct  IEquatable_1U5BU5D_t4568  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>[]
struct  KeyValuePair_2U5BU5D_t4314  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>[]
struct  KeyValuePair_2U5BU5D_t4325  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>[]
struct  KeyValuePair_2U5BU5D_t4329  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualCategory>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualCategory>[]
struct  KeyValuePair_2U5BU5D_t4334  : public Array_t
{
};
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>[]
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>[]
struct  List_1U5BU5D_t3616  : public Array_t
{
};
struct List_1U5BU5D_t3616_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>[]
struct  KeyValuePair_2U5BU5D_t4339  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>[]
struct  KeyValuePair_2U5BU5D_t4357  : public Array_t
{
};
// System.UInt32[]
// System.UInt32[]
struct  UInt32U5BU5D_t2194  : public Array_t
{
};
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct  IComparable_1U5BU5D_t4569  : public Array_t
{
};
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct  IEquatable_1U5BU5D_t4570  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>[]
struct  KeyValuePair_2U5BU5D_t4355  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>[]
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>[]
struct  KeyValuePair_2U5BU5D_t4362  : public Array_t
{
};
// System.IntPtr[]
// System.IntPtr[]
struct  IntPtrU5BU5D_t859  : public Array_t
{
};
struct IntPtrU5BU5D_t859_StaticFields{
};
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct  ISerializableU5BU5D_t4571  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>[]
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>[]
struct  KeyValuePair_2U5BU5D_t4374  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>[]
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>[]
struct  KeyValuePair_2U5BU5D_t4380  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>[]
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>[]
struct  KeyValuePair_2U5BU5D_t4382  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4391  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>[]
// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>[]
struct  KeyValuePair_2U5BU5D_t4389  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct  List_1U5BU5D_t3881  : public Array_t
{
};
struct List_1U5BU5D_t3881_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct  KeyValuePair_2U5BU5D_t4430  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct  KeyValuePair_2U5BU5D_t4428  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct  List_1U5BU5D_t3949  : public Array_t
{
};
struct List_1U5BU5D_t3949_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct  KeyValuePair_2U5BU5D_t4445  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct  List_1U5BU5D_t3969  : public Array_t
{
};
struct List_1U5BU5D_t3969_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct  KeyValuePair_2U5BU5D_t4463  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4465  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4443  : public Array_t
{
};
// System.Enum[]
// System.Enum[]
struct  EnumU5BU5D_t4572  : public Array_t
{
};
struct EnumU5BU5D_t4572_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct  List_1U5BU5D_t4049  : public Array_t
{
};
struct List_1U5BU5D_t4049_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct  List_1U5BU5D_t4051  : public Array_t
{
};
struct List_1U5BU5D_t4051_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct  KeyValuePair_2U5BU5D_t4495  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct  KeyValuePair_2U5BU5D_t4503  : public Array_t
{
};
// System.Byte[][]
// System.Byte[][]
struct  ByteU5BU5DU5BU5D_t2320  : public Array_t
{
};
// System.Single[]
// System.Single[]
struct  SingleU5BU5D_t1582  : public Array_t
{
};
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct  IComparable_1U5BU5D_t4573  : public Array_t
{
};
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct  IEquatable_1U5BU5D_t4574  : public Array_t
{
};
// System.Attribute[]
// System.Attribute[]
struct  AttributeU5BU5D_t4575  : public Array_t
{
};
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct  _AttributeU5BU5D_t4576  : public Array_t
{
};
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct  ParameterModifierU5BU5D_t1689  : public Array_t
{
};
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct  ParameterInfoU5BU5D_t1692  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct  _ParameterInfoU5BU5D_t4577  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct  KeyValuePair_2U5BU5D_t4521  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
struct  KeyValuePair_2U5BU5D_t4535  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t4533  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct  X509CertificateU5BU5D_t2132  : public Array_t
{
};
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct  IDeserializationCallbackU5BU5D_t4578  : public Array_t
{
};
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct  KeySizesU5BU5D_t2216  : public Array_t
{
};
// System.Single[,]
// System.Single[,]
struct  SingleU5BU2CU5D_t4579  : public Array_t
{
};
// System.Delegate[]
// System.Delegate[]
struct  DelegateU5BU5D_t2875  : public Array_t
{
};
// System.UInt64[]
// System.UInt64[]
struct  UInt64U5BU5D_t2711  : public Array_t
{
};
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct  IComparable_1U5BU5D_t4580  : public Array_t
{
};
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct  IEquatable_1U5BU5D_t4581  : public Array_t
{
};
// System.Int16[]
// System.Int16[]
struct  Int16U5BU5D_t2892  : public Array_t
{
};
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct  IComparable_1U5BU5D_t4582  : public Array_t
{
};
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct  IEquatable_1U5BU5D_t4583  : public Array_t
{
};
// System.SByte[]
// System.SByte[]
struct  SByteU5BU5D_t2754  : public Array_t
{
};
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct  IComparable_1U5BU5D_t4584  : public Array_t
{
};
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct  IEquatable_1U5BU5D_t4585  : public Array_t
{
};
// System.Int64[]
// System.Int64[]
struct  Int64U5BU5D_t2876  : public Array_t
{
};
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct  IComparable_1U5BU5D_t4586  : public Array_t
{
};
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct  IEquatable_1U5BU5D_t4587  : public Array_t
{
};
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct  MethodInfoU5BU5D_t2541  : public Array_t
{
};
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct  _MethodInfoU5BU5D_t4588  : public Array_t
{
};
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct  MethodBaseU5BU5D_t2882  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct  _MethodBaseU5BU5D_t4589  : public Array_t
{
};
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct  ConstructorInfoU5BU5D_t2878  : public Array_t
{
};
struct ConstructorInfoU5BU5D_t2878_StaticFields{
};
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct  _ConstructorInfoU5BU5D_t4590  : public Array_t
{
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct  TableRangeU5BU5D_t2361  : public Array_t
{
};
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct  TailoringInfoU5BU5D_t2368  : public Array_t
{
};
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct  ContractionU5BU5D_t2377  : public Array_t
{
};
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct  Level2MapU5BU5D_t2378  : public Array_t
{
};
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct  BigIntegerU5BU5D_t2879  : public Array_t
{
};
struct BigIntegerU5BU5D_t2879_StaticFields{
};
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct  SlotU5BU5D_t2442  : public Array_t
{
};
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct  SlotU5BU5D_t2447  : public Array_t
{
};
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct  StackFrameU5BU5D_t2455  : public Array_t
{
};
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct  CalendarU5BU5D_t2463  : public Array_t
{
};
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct  ModuleBuilderU5BU5D_t2505  : public Array_t
{
};
struct ModuleBuilderU5BU5D_t2505_StaticFields{
};
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct  _ModuleBuilderU5BU5D_t4591  : public Array_t
{
};
// System.Reflection.Module[]
// System.Reflection.Module[]
struct  ModuleU5BU5D_t2881  : public Array_t
{
};
struct ModuleU5BU5D_t2881_StaticFields{
};
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct  _ModuleU5BU5D_t4592  : public Array_t
{
};
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct  ParameterBuilderU5BU5D_t2508  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct  _ParameterBuilderU5BU5D_t4593  : public Array_t
{
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct  GenericTypeParameterBuilderU5BU5D_t2515  : public Array_t
{
};
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct  MethodBuilderU5BU5D_t2519  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct  _MethodBuilderU5BU5D_t4594  : public Array_t
{
};
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct  ConstructorBuilderU5BU5D_t2520  : public Array_t
{
};
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct  _ConstructorBuilderU5BU5D_t4595  : public Array_t
{
};
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct  FieldBuilderU5BU5D_t2521  : public Array_t
{
};
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct  _FieldBuilderU5BU5D_t4596  : public Array_t
{
};
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct  FieldInfoU5BU5D_t4597  : public Array_t
{
};
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct  _FieldInfoU5BU5D_t4598  : public Array_t
{
};
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct  PropertyInfoU5BU5D_t2883  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct  _PropertyInfoU5BU5D_t4599  : public Array_t
{
};
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct  HeaderU5BU5D_t2850  : public Array_t
{
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct  ITrackingHandlerU5BU5D_t2900  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct  IContextAttributeU5BU5D_t2889  : public Array_t
{
};
// System.DateTime[]
// System.DateTime[]
struct  DateTimeU5BU5D_t2902  : public Array_t
{
};
struct DateTimeU5BU5D_t2902_StaticFields{
};
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct  IComparable_1U5BU5D_t4600  : public Array_t
{
};
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct  IEquatable_1U5BU5D_t4601  : public Array_t
{
};
// System.Decimal[]
// System.Decimal[]
struct  DecimalU5BU5D_t2903  : public Array_t
{
};
struct DecimalU5BU5D_t2903_StaticFields{
};
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct  IComparable_1U5BU5D_t4602  : public Array_t
{
};
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct  IEquatable_1U5BU5D_t4603  : public Array_t
{
};
// System.TimeSpan[]
// System.TimeSpan[]
struct  TimeSpanU5BU5D_t2904  : public Array_t
{
};
struct TimeSpanU5BU5D_t2904_StaticFields{
};
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct  IComparable_1U5BU5D_t4604  : public Array_t
{
};
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct  IEquatable_1U5BU5D_t4605  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct  TypeTagU5BU5D_t2905  : public Array_t
{
};
// System.MonoType[]
// System.MonoType[]
struct  MonoTypeU5BU5D_t2907  : public Array_t
{
};
// System.Byte[,]
// System.Byte[,]
struct  ByteU5BU2CU5D_t2685  : public Array_t
{
};
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct  StrongNameU5BU5D_t4240  : public Array_t
{
};
