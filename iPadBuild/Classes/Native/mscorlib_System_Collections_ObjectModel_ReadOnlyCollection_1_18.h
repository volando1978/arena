﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct IList_1_t3683;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct  ReadOnlyCollection_1_t3684  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Quests.IQuest>::list
	Object_t* ___list_0;
};
