﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.PlayGamesAchievement
struct PlayGamesAchievement_t381;
// System.String
struct String_t;
// GooglePlayGames.ReportProgress
struct ReportProgress_t380;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void GooglePlayGames.PlayGamesAchievement::.ctor()
extern "C" void PlayGamesAchievement__ctor_m1497 (PlayGamesAchievement_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::.ctor(GooglePlayGames.ReportProgress)
extern "C" void PlayGamesAchievement__ctor_m1498 (PlayGamesAchievement_t381 * __this, ReportProgress_t380 * ___progressCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::.cctor()
extern "C" void PlayGamesAchievement__cctor_m1499 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::ReportProgress(System.Action`1<System.Boolean>)
extern "C" void PlayGamesAchievement_ReportProgress_m1500 (PlayGamesAchievement_t381 * __this, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesAchievement::get_id()
extern "C" String_t* PlayGamesAchievement_get_id_m1501 (PlayGamesAchievement_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::set_id(System.String)
extern "C" void PlayGamesAchievement_set_id_m1502 (PlayGamesAchievement_t381 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GooglePlayGames.PlayGamesAchievement::get_percentCompleted()
extern "C" double PlayGamesAchievement_get_percentCompleted_m1503 (PlayGamesAchievement_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::set_percentCompleted(System.Double)
extern "C" void PlayGamesAchievement_set_percentCompleted_m1504 (PlayGamesAchievement_t381 * __this, double ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_completed()
extern "C" bool PlayGamesAchievement_get_completed_m1505 (PlayGamesAchievement_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_hidden()
extern "C" bool PlayGamesAchievement_get_hidden_m1506 (PlayGamesAchievement_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.PlayGamesAchievement::get_lastReportedDate()
extern "C" DateTime_t48  PlayGamesAchievement_get_lastReportedDate_m1507 (PlayGamesAchievement_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
