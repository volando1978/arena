﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
struct Enumerator_t3850;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t811;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m21338_gshared (Enumerator_t3850 * __this, List_1_t811 * ___l, MethodInfo* method);
#define Enumerator__ctor_m21338(__this, ___l, method) (( void (*) (Enumerator_t3850 *, List_1_t811 *, MethodInfo*))Enumerator__ctor_m21338_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21339_gshared (Enumerator_t3850 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21339(__this, method) (( Object_t * (*) (Enumerator_t3850 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C" void Enumerator_Dispose_m21340_gshared (Enumerator_t3850 * __this, MethodInfo* method);
#define Enumerator_Dispose_m21340(__this, method) (( void (*) (Enumerator_t3850 *, MethodInfo*))Enumerator_Dispose_m21340_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern "C" void Enumerator_VerifyState_m21341_gshared (Enumerator_t3850 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m21341(__this, method) (( void (*) (Enumerator_t3850 *, MethodInfo*))Enumerator_VerifyState_m21341_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21342_gshared (Enumerator_t3850 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m21342(__this, method) (( bool (*) (Enumerator_t3850 *, MethodInfo*))Enumerator_MoveNext_m21342_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C" Vector3_t758  Enumerator_get_Current_m21343_gshared (Enumerator_t3850 * __this, MethodInfo* method);
#define Enumerator_get_Current_m21343(__this, method) (( Vector3_t758  (*) (Enumerator_t3850 *, MethodInfo*))Enumerator_get_Current_m21343_gshared)(__this, method)
