﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t1381;
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Canvas
struct  Canvas_t1229  : public Behaviour_t1416
{
};
struct Canvas_t1229_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t1381 * ___willRenderCanvases_2;
};
