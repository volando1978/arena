﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameOverSrc
struct gameOverSrc_t801;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void gameOverSrc::.ctor()
extern "C" void gameOverSrc__ctor_m3500 (gameOverSrc_t801 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc::Start()
extern "C" void gameOverSrc_Start_m3501 (gameOverSrc_t801 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc::Update()
extern "C" void gameOverSrc_Update_m3502 (gameOverSrc_t801 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameOverSrc::run()
extern "C" Object_t * gameOverSrc_run_m3503 (gameOverSrc_t801 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gameOverSrc::Show()
extern "C" Object_t * gameOverSrc_Show_m3504 (gameOverSrc_t801 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc::OnGUI()
extern "C" void gameOverSrc_OnGUI_m3505 (gameOverSrc_t801 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 gameOverSrc::getFontsize()
extern "C" int32_t gameOverSrc_getFontsize_m3506 (gameOverSrc_t801 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameOverSrc::isReady()
extern "C" bool gameOverSrc_isReady_m3507 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc::setReady(System.Boolean)
extern "C" void gameOverSrc_setReady_m3508 (Object_t * __this /* static, unused */, bool ___readyState, MethodInfo* method) IL2CPP_METHOD_ATTR;
