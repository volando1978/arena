﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t1215;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Selectable>
struct  Predicate_1_t4016  : public MulticastDelegate_t22
{
};
