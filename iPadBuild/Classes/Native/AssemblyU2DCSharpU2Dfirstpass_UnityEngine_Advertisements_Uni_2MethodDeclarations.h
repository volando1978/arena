﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.UnityAdsPlatform
struct UnityAdsPlatform_t156;
// System.String
struct String_t;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"

// System.Void UnityEngine.Advertisements.UnityAdsPlatform::.ctor()
extern "C" void UnityAdsPlatform__ctor_m821 (UnityAdsPlatform_t156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::init(System.String,System.Boolean,System.String,System.String)
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::show(System.String,System.String,System.String)
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::hide()
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::isSupported()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getSDKVersion()
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::canShowZone(System.String)
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::hasMultipleRewardItems()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemKeys()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getDefaultRewardItemKey()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getCurrentRewardItemKey()
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::setRewardItemKey(System.String)
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::setDefaultRewardItemAsRewardItem()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsWithKey(System.String)
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsKeys()
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
