﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t1263;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Text>
struct  Predicate_1_t3955  : public MulticastDelegate_t22
{
};
