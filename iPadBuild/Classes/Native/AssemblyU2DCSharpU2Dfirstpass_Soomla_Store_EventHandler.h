﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t75;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.EventHandler
struct  EventHandler_t76  : public Object_t
{
};
struct EventHandler_t76_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Soomla.Store.EventHandler::<>f__switch$map0
	Dictionary_2_t75 * ___U3CU3Ef__switchU24map0_0;
};
