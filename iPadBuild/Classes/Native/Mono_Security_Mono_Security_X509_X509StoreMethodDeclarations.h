﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.X509Store
struct X509Store_t2049;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t2153;
// System.Collections.ArrayList
struct ArrayList_t737;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t2024;
// Mono.Security.X509.X509Crl
struct X509Crl_t2133;

// System.Void Mono.Security.X509.X509Store::.ctor(System.String,System.Boolean)
extern "C" void X509Store__ctor_m9265 (X509Store_t2049 * __this, String_t* ___path, bool ___crl, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::get_Certificates()
extern "C" X509CertificateCollection_t2153 * X509Store_get_Certificates_m8854 (X509Store_t2049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509Store::get_Crls()
extern "C" ArrayList_t737 * X509Store_get_Crls_m8842 (X509Store_t2049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Store::Load(System.String)
extern "C" ByteU5BU5D_t350* X509Store_Load_m9266 (X509Store_t2049 * __this, String_t* ___filename, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Store::LoadCertificate(System.String)
extern "C" X509Certificate_t2024 * X509Store_LoadCertificate_m9267 (X509Store_t2049 * __this, String_t* ___filename, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl Mono.Security.X509.X509Store::LoadCrl(System.String)
extern "C" X509Crl_t2133 * X509Store_LoadCrl_m9268 (X509Store_t2049 * __this, String_t* ___filename, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Store::CheckStore(System.String,System.Boolean)
extern "C" bool X509Store_CheckStore_m9269 (X509Store_t2049 * __this, String_t* ___path, bool ___throwException, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::BuildCertificatesCollection(System.String)
extern "C" X509CertificateCollection_t2153 * X509Store_BuildCertificatesCollection_m9270 (X509Store_t2049 * __this, String_t* ___storeName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509Store::BuildCrlsCollection(System.String)
extern "C" ArrayList_t737 * X509Store_BuildCrlsCollection_m9271 (X509Store_t2049 * __this, String_t* ___storeName, MethodInfo* method) IL2CPP_METHOD_ATTR;
