﻿#pragma once
#include <stdint.h>
// UnityEngine.Font
struct Font_t1222;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t1382;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct  KeyValuePair_2_t3959 
{
	// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::key
	Font_t1222 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::value
	List_1_t1382 * ___value_1;
};
