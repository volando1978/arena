﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Quests.MilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_MilestoneS.h"
// GooglePlayGames.BasicApi.Quests.MilestoneState
struct  MilestoneState_t368 
{
	// System.Int32 GooglePlayGames.BasicApi.Quests.MilestoneState::value__
	int32_t ___value___1;
};
