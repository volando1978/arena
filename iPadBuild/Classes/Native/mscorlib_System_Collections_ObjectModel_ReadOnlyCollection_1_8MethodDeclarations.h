﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>
struct ReadOnlyCollection_1_t3559;
// Soomla.Store.MarketItem
struct MarketItem_t122;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Soomla.Store.MarketItem>
struct IList_1_t3558;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// Soomla.Store.MarketItem[]
struct MarketItemU5BU5D_t3557;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.MarketItem>
struct IEnumerator_1_t4308;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m17311(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3559 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m15428_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17312(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3559 *, MarketItem_t122 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17313(__this, method) (( void (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17314(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3559 *, int32_t, MarketItem_t122 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17315(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3559 *, MarketItem_t122 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17316(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3559 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17317(__this, ___index, method) (( MarketItem_t122 * (*) (ReadOnlyCollection_1_t3559 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17318(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3559 *, int32_t, MarketItem_t122 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17319(__this, method) (( bool (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17320(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3559 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17321(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m17322(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3559 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15439_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m17323(__this, method) (( void (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15440_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m17324(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3559 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15441_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17325(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3559 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m17326(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3559 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15443_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m17327(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3559 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15444_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17328(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3559 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17329(__this, method) (( bool (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17330(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17331(__this, method) (( bool (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17332(__this, method) (( bool (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m17333(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3559 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m17334(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3559 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::Contains(T)
#define ReadOnlyCollection_1_Contains_m17335(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3559 *, MarketItem_t122 *, MethodInfo*))ReadOnlyCollection_1_Contains_m15452_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m17336(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3559 *, MarketItemU5BU5D_t3557*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m15453_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m17337(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15454_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m17338(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3559 *, MarketItem_t122 *, MethodInfo*))ReadOnlyCollection_1_IndexOf_m15455_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::get_Count()
#define ReadOnlyCollection_1_get_Count_m17339(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3559 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m15456_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m17340(__this, ___index, method) (( MarketItem_t122 * (*) (ReadOnlyCollection_1_t3559 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m15457_gshared)(__this, ___index, method)
