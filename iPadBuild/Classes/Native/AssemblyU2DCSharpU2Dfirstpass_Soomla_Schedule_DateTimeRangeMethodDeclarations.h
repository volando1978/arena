﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Schedule/DateTimeRange
struct DateTimeRange_t47;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Soomla.Schedule/DateTimeRange::.ctor(System.DateTime,System.DateTime)
extern "C" void DateTimeRange__ctor_m183 (DateTimeRange_t47 * __this, DateTime_t48  ___start, DateTime_t48  ___end, MethodInfo* method) IL2CPP_METHOD_ATTR;
