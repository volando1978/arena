﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.MultiplayerParticipant
struct MultiplayerParticipant_t436;
// System.Text.StringBuilder
struct StringBuilder_t36;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Imag.h"
// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Status(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t MultiplayerParticipant_MultiplayerParticipant_Status_m1800 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchRank(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t MultiplayerParticipant_MultiplayerParticipant_MatchRank_m1801 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_IsConnectedToRoom(System.Runtime.InteropServices.HandleRef)
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_IsConnectedToRoom_m1802 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_DisplayName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_DisplayName_m1803 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasPlayer(System.Runtime.InteropServices.HandleRef)
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_HasPlayer_m1804 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_AvatarUrl_m1805 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___resolution, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchResult(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t MultiplayerParticipant_MultiplayerParticipant_MatchResult_m1806 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Player(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t MultiplayerParticipant_MultiplayerParticipant_Player_m1807 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void MultiplayerParticipant_MultiplayerParticipant_Dispose_m1808 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_Valid_m1809 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasMatchResult(System.Runtime.InteropServices.HandleRef)
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_HasMatchResult_m1810 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_Id_m1811 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
