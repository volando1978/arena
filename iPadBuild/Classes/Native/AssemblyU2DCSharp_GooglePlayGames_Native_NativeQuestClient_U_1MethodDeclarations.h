﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28
struct U3CFromQuestUICallbackU3Ec__AnonStorey28_t555;
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
struct QuestUIResponse_t692;

// System.Void GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28::.ctor()
extern "C" void U3CFromQuestUICallbackU3Ec__AnonStorey28__ctor_m2303 (U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28::<>m__20(GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse)
extern "C" void U3CFromQuestUICallbackU3Ec__AnonStorey28_U3CU3Em__20_m2304 (U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * __this, QuestUIResponse_t692 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
