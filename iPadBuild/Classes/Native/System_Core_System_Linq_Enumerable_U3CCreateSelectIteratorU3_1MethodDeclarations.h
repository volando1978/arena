﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t3784;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::.ctor()
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20720_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20720(__this, method) (( void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20720_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C" IntPtr_t U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20721_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20721(__this, method) (( IntPtr_t (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20721_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20722_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20722(__this, method) (( Object_t * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20722_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20723_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20723(__this, method) (( Object_t * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20723_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C" Object_t* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20724_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20724(__this, method) (( Object_t* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20724_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::MoveNext()
extern "C" bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20725_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20725(__this, method) (( bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20725_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::Dispose()
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20726_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20726(__this, method) (( void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20726_gshared)(__this, method)
