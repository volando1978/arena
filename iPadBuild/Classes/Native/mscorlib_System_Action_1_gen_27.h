﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
struct AcceptResponse_t691;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse>
struct  Action_1_t879  : public MulticastDelegate_t22
{
};
