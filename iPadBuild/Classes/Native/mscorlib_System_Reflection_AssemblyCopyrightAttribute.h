﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_t1423  : public Attribute_t1546
{
	// System.String System.Reflection.AssemblyCopyrightAttribute::name
	String_t* ___name_0;
};
