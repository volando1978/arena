﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Random
struct Random_t207;

// System.Void System.Random::.ctor()
extern "C" void Random__ctor_m937 (Random_t207 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Random::.ctor(System.Int32)
extern "C" void Random__ctor_m14261 (Random_t207 * __this, int32_t ___Seed, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Random::Sample()
extern "C" double Random_Sample_m14262 (Random_t207 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Random::Next(System.Int32)
extern "C" int32_t Random_Next_m14263 (Random_t207 * __this, int32_t ___maxValue, MethodInfo* method) IL2CPP_METHOD_ATTR;
