﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct  X509ChainStatusU5BU5D_t2033  : public Array_t
{
};
// System.Text.RegularExpressions.Capture[]
// System.Text.RegularExpressions.Capture[]
struct  CaptureU5BU5D_t2057  : public Array_t
{
};
// System.Text.RegularExpressions.Group[]
// System.Text.RegularExpressions.Group[]
struct  GroupU5BU5D_t2060  : public Array_t
{
};
struct GroupU5BU5D_t2060_StaticFields{
};
// System.Text.RegularExpressions.Mark[]
// System.Text.RegularExpressions.Mark[]
struct  MarkU5BU5D_t2088  : public Array_t
{
};
// System.Uri/UriScheme[]
// System.Uri/UriScheme[]
struct  UriSchemeU5BU5D_t2119  : public Array_t
{
};
