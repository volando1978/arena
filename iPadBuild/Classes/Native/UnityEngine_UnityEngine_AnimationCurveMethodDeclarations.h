﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t1592;
struct AnimationCurve_t1592_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1668;

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m7198 (AnimationCurve_t1592 * __this, KeyframeU5BU5D_t1668* ___keys, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m7199 (AnimationCurve_t1592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m7200 (AnimationCurve_t1592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m7201 (AnimationCurve_t1592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m7202 (AnimationCurve_t1592 * __this, KeyframeU5BU5D_t1668* ___keys, MethodInfo* method) IL2CPP_METHOD_ATTR;
void AnimationCurve_t1592_marshal(const AnimationCurve_t1592& unmarshaled, AnimationCurve_t1592_marshaled& marshaled);
void AnimationCurve_t1592_marshal_back(const AnimationCurve_t1592_marshaled& marshaled, AnimationCurve_t1592& unmarshaled);
void AnimationCurve_t1592_marshal_cleanup(AnimationCurve_t1592_marshaled& marshaled);
