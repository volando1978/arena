﻿#pragma once
#include <stdint.h>
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct  Predicate_1_t3432  : public MulticastDelegate_t22
{
};
