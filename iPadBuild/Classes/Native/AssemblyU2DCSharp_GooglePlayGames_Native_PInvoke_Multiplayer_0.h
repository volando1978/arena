﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t671;
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_BaseReferen.h"
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct  MultiplayerParticipant_t672  : public BaseReferenceHolder_t654
{
};
struct MultiplayerParticipant_t672_StaticFields{
	// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus> GooglePlayGames.Native.PInvoke.MultiplayerParticipant::StatusConversion
	Dictionary_2_t671 * ___StatusConversion_1;
};
