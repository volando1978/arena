﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct U24ArrayTypeU24256_t2867;
struct U24ArrayTypeU24256_t2867_marshaled;

void U24ArrayTypeU24256_t2867_marshal(const U24ArrayTypeU24256_t2867& unmarshaled, U24ArrayTypeU24256_t2867_marshaled& marshaled);
void U24ArrayTypeU24256_t2867_marshal_back(const U24ArrayTypeU24256_t2867_marshaled& marshaled, U24ArrayTypeU24256_t2867& unmarshaled);
void U24ArrayTypeU24256_t2867_marshal_cleanup(U24ArrayTypeU24256_t2867_marshaled& marshaled);
