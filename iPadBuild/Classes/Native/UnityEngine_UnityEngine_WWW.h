﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.WWW
struct  WWW_t383  : public Object_t
{
	// System.IntPtr UnityEngine.WWW::m_Ptr
	IntPtr_t ___m_Ptr_0;
};
