﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ExtensionMethods
struct ExtensionMethods_t729;
// UnityEngine.Transform
struct Transform_t809;
// System.String
struct String_t;

// UnityEngine.Transform ExtensionMethods::Search(UnityEngine.Transform,System.String)
extern "C" Transform_t809 * ExtensionMethods_Search_m3158 (Object_t * __this /* static, unused */, Transform_t809 * ___target, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
