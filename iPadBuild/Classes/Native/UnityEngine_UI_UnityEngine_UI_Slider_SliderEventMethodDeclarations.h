﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t1293;

// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
extern "C" void SliderEvent__ctor_m5374 (SliderEvent_t1293 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
