﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Double>
struct InternalEnumerator_1_t3426;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15526_gshared (InternalEnumerator_1_t3426 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m15526(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3426 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15526_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15527_gshared (InternalEnumerator_1_t3426 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15527(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3426 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15527_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15528_gshared (InternalEnumerator_1_t3426 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15528(__this, method) (( void (*) (InternalEnumerator_1_t3426 *, MethodInfo*))InternalEnumerator_1_Dispose_m15528_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15529_gshared (InternalEnumerator_1_t3426 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15529(__this, method) (( bool (*) (InternalEnumerator_1_t3426 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15529_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C" double InternalEnumerator_1_get_Current_m15530_gshared (InternalEnumerator_1_t3426 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15530(__this, method) (( double (*) (InternalEnumerator_1_t3426 *, MethodInfo*))InternalEnumerator_1_get_Current_m15530_gshared)(__this, method)
