﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$4
struct U24ArrayTypeU244_t2305;
struct U24ArrayTypeU244_t2305_marshaled;

void U24ArrayTypeU244_t2305_marshal(const U24ArrayTypeU244_t2305& unmarshaled, U24ArrayTypeU244_t2305_marshaled& marshaled);
void U24ArrayTypeU244_t2305_marshal_back(const U24ArrayTypeU244_t2305_marshaled& marshaled, U24ArrayTypeU244_t2305& unmarshaled);
void U24ArrayTypeU244_t2305_marshal_cleanup(U24ArrayTypeU244_t2305_marshaled& marshaled);
