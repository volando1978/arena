﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// gameOverSrc
struct  gameOverSrc_t801  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject gameOverSrc::gameControlObj
	GameObject_t144 * ___gameControlObj_2;
	// UnityEngine.GUIStyle gameOverSrc::marqueeSt
	GUIStyle_t724 * ___marqueeSt_4;
	// UnityEngine.GUIStyle gameOverSrc::marqueeStLeft
	GUIStyle_t724 * ___marqueeStLeft_5;
	// UnityEngine.GUIStyle gameOverSrc::lowBarSt
	GUIStyle_t724 * ___lowBarSt_6;
	// UnityEngine.GUIStyle gameOverSrc::lowBarPlaySt
	GUIStyle_t724 * ___lowBarPlaySt_7;
	// UnityEngine.GUIStyle gameOverSrc::storeButtonSt
	GUIStyle_t724 * ___storeButtonSt_8;
	// UnityEngine.GUIStyle gameOverSrc::bulletsSt
	GUIStyle_t724 * ___bulletsSt_10;
	// UnityEngine.GUIStyle gameOverSrc::marqueeBigSt
	GUIStyle_t724 * ___marqueeBigSt_11;
	// UnityEngine.GUIStyle gameOverSrc::boxStyle
	GUIStyle_t724 * ___boxStyle_12;
	// System.String gameOverSrc::maxText
	String_t* ___maxText_13;
	// System.String gameOverSrc::lastText
	String_t* ___lastText_14;
	// System.String gameOverSrc::killsText
	String_t* ___killsText_15;
	// System.String gameOverSrc::stageTimeText
	String_t* ___stageTimeText_16;
	// System.String gameOverSrc::timeText
	String_t* ___timeText_17;
	// System.String gameOverSrc::coinsText
	String_t* ___coinsText_18;
	// UnityEngine.Rect gameOverSrc::messageRect
	Rect_t738  ___messageRect_19;
	// UnityEngine.Rect gameOverSrc::messageMax
	Rect_t738  ___messageMax_20;
	// UnityEngine.Rect gameOverSrc::messageLast
	Rect_t738  ___messageLast_21;
	// UnityEngine.Rect gameOverSrc::boxRect
	Rect_t738  ___boxRect_22;
	// System.Single gameOverSrc::offsetLetter
	float ___offsetLetter_23;
	// UnityEngine.Vector2 gameOverSrc::startPos
	Vector2_t739  ___startPos_24;
	// UnityEngine.Vector2 gameOverSrc::endPos
	Vector2_t739  ___endPos_25;
	// System.Int32 gameOverSrc::counterK
	int32_t ___counterK_26;
	// System.Int32 gameOverSrc::counterL
	int32_t ___counterL_27;
	// System.Int32 gameOverSrc::counterM
	int32_t ___counterM_28;
	// System.Int32 gameOverSrc::counterC
	int32_t ___counterC_29;
	// System.Single gameOverSrc::counterDeathColor
	float ___counterDeathColor_30;
	// System.Single gameOverSrc::v
	float ___v_31;
	// System.Single gameOverSrc::xU
	float ___xU_32;
	// System.Single gameOverSrc::yU
	float ___yU_33;
	// UnityEngine.Color gameOverSrc::cc
	Color_t747  ___cc_34;
};
struct gameOverSrc_t801_StaticFields{
	// System.Boolean gameOverSrc::isGameOverReady
	bool ___isGameOverReady_3;
	// UnityEngine.Rect gameOverSrc::bulletsHUD
	Rect_t738  ___bulletsHUD_9;
};
