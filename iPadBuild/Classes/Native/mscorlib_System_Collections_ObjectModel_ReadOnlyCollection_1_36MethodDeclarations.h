﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t3964;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t1407;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1264;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t4454;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m22890_gshared (ReadOnlyCollection_1_t3964 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m22890(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3964 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m22890_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22891_gshared (ReadOnlyCollection_1_t3964 * __this, UIVertex_t1265  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22891(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3964 *, UIVertex_t1265 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22891_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22892_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22892(__this, method) (( void (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22892_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22893_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, UIVertex_t1265  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22893(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3964 *, int32_t, UIVertex_t1265 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22893_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22894_gshared (ReadOnlyCollection_1_t3964 * __this, UIVertex_t1265  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22894(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3964 *, UIVertex_t1265 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22894_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22895_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22895(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3964 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22895_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UIVertex_t1265  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22896_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22896(__this, ___index, method) (( UIVertex_t1265  (*) (ReadOnlyCollection_1_t3964 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22896_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22897_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, UIVertex_t1265  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22897(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3964 *, int32_t, UIVertex_t1265 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22897_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22898_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22898(__this, method) (( bool (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22898_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22899_gshared (ReadOnlyCollection_1_t3964 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22899(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3964 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22899_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22900_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22900(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22900_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m22901_gshared (ReadOnlyCollection_1_t3964 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m22901(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3964 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m22901_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m22902_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m22902(__this, method) (( void (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m22902_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m22903_gshared (ReadOnlyCollection_1_t3964 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m22903(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3964 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m22903_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22904_gshared (ReadOnlyCollection_1_t3964 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22904(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3964 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22904_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m22905_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m22905(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3964 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m22905_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m22906_gshared (ReadOnlyCollection_1_t3964 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m22906(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3964 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m22906_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22907_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22907(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3964 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22907_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22908_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22908(__this, method) (( bool (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22908_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22909_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22909(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22909_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22910_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22910(__this, method) (( bool (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22910_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22911_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22911(__this, method) (( bool (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22911_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m22912_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m22912(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3964 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m22912_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m22913_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m22913(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3964 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m22913_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m22914_gshared (ReadOnlyCollection_1_t3964 * __this, UIVertex_t1265  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m22914(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3964 *, UIVertex_t1265 , MethodInfo*))ReadOnlyCollection_1_Contains_m22914_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m22915_gshared (ReadOnlyCollection_1_t3964 * __this, UIVertexU5BU5D_t1264* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m22915(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3964 *, UIVertexU5BU5D_t1264*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m22915_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m22916_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m22916(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m22916_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m22917_gshared (ReadOnlyCollection_1_t3964 * __this, UIVertex_t1265  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m22917(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3964 *, UIVertex_t1265 , MethodInfo*))ReadOnlyCollection_1_IndexOf_m22917_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m22918_gshared (ReadOnlyCollection_1_t3964 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m22918(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3964 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m22918_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t1265  ReadOnlyCollection_1_get_Item_m22919_gshared (ReadOnlyCollection_1_t3964 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m22919(__this, ___index, method) (( UIVertex_t1265  (*) (ReadOnlyCollection_1_t3964 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m22919_gshared)(__this, ___index, method)
