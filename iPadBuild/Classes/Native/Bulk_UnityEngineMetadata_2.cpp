﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern TypeInfo GcAchievementDescriptionData_t1616_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
extern Il2CppType AchievementDescription_t1626_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
MethodInfo GcAchievementDescriptionData_ToAchievementDescription_m7314_MethodInfo = 
{
	"ToAchievementDescription"/* name */
	, (methodPointerType)&GcAchievementDescriptionData_ToAchievementDescription_m7314/* method */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* declaring_type */
	, &AchievementDescription_t1626_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GcAchievementDescriptionData_t1616_MethodInfos[] =
{
	&GcAchievementDescriptionData_ToAchievementDescription_m7314_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_6;
FieldInfo GcAchievementDescriptionData_t1616____m_Identifier_0_FieldInfo = 
{
	"m_Identifier"/* name */
	, &String_t_0_0_6/* type */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementDescriptionData_t1616, ___m_Identifier_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo GcAchievementDescriptionData_t1616____m_Title_1_FieldInfo = 
{
	"m_Title"/* name */
	, &String_t_0_0_6/* type */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementDescriptionData_t1616, ___m_Title_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Texture2D_t384_0_0_6;
FieldInfo GcAchievementDescriptionData_t1616____m_Image_2_FieldInfo = 
{
	"m_Image"/* name */
	, &Texture2D_t384_0_0_6/* type */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementDescriptionData_t1616, ___m_Image_2) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo GcAchievementDescriptionData_t1616____m_AchievedDescription_3_FieldInfo = 
{
	"m_AchievedDescription"/* name */
	, &String_t_0_0_6/* type */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementDescriptionData_t1616, ___m_AchievedDescription_3) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo GcAchievementDescriptionData_t1616____m_UnachievedDescription_4_FieldInfo = 
{
	"m_UnachievedDescription"/* name */
	, &String_t_0_0_6/* type */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementDescriptionData_t1616, ___m_UnachievedDescription_4) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcAchievementDescriptionData_t1616____m_Hidden_5_FieldInfo = 
{
	"m_Hidden"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementDescriptionData_t1616, ___m_Hidden_5) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcAchievementDescriptionData_t1616____m_Points_6_FieldInfo = 
{
	"m_Points"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementDescriptionData_t1616, ___m_Points_6) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* GcAchievementDescriptionData_t1616_FieldInfos[] =
{
	&GcAchievementDescriptionData_t1616____m_Identifier_0_FieldInfo,
	&GcAchievementDescriptionData_t1616____m_Title_1_FieldInfo,
	&GcAchievementDescriptionData_t1616____m_Image_2_FieldInfo,
	&GcAchievementDescriptionData_t1616____m_AchievedDescription_3_FieldInfo,
	&GcAchievementDescriptionData_t1616____m_UnachievedDescription_4_FieldInfo,
	&GcAchievementDescriptionData_t1616____m_Hidden_5_FieldInfo,
	&GcAchievementDescriptionData_t1616____m_Points_6_FieldInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1319_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1320_MethodInfo;
extern MethodInfo ValueType_ToString_m1321_MethodInfo;
static Il2CppMethodReference GcAchievementDescriptionData_t1616_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool GcAchievementDescriptionData_t1616_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GcAchievementDescriptionData_t1616_0_0_0;
extern Il2CppType GcAchievementDescriptionData_t1616_1_0_0;
extern Il2CppType ValueType_t329_0_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementDescriptionData_t1616_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, GcAchievementDescriptionData_t1616_VTable/* vtableMethods */
	, GcAchievementDescriptionData_t1616_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo GcAchievementDescriptionData_t1616_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementDescriptionData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementDescriptionData_t1616_MethodInfos/* methods */
	, NULL/* properties */
	, GcAchievementDescriptionData_t1616_FieldInfos/* fields */
	, NULL/* events */
	, &GcAchievementDescriptionData_t1616_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementDescriptionData_t1616_0_0_0/* byval_arg */
	, &GcAchievementDescriptionData_t1616_1_0_0/* this_arg */
	, &GcAchievementDescriptionData_t1616_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcAchievementDescriptionData_t1616)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementDescriptionData_t1616)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern TypeInfo GcAchievementData_t1617_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
extern Il2CppType Achievement_t1625_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
MethodInfo GcAchievementData_ToAchievement_m7315_MethodInfo = 
{
	"ToAchievement"/* name */
	, (methodPointerType)&GcAchievementData_ToAchievement_m7315/* method */
	, &GcAchievementData_t1617_il2cpp_TypeInfo/* declaring_type */
	, &Achievement_t1625_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GcAchievementData_t1617_MethodInfos[] =
{
	&GcAchievementData_ToAchievement_m7315_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_6;
FieldInfo GcAchievementData_t1617____m_Identifier_0_FieldInfo = 
{
	"m_Identifier"/* name */
	, &String_t_0_0_6/* type */
	, &GcAchievementData_t1617_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementData_t1617, ___m_Identifier_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Double_t234_0_0_6;
FieldInfo GcAchievementData_t1617____m_PercentCompleted_1_FieldInfo = 
{
	"m_PercentCompleted"/* name */
	, &Double_t234_0_0_6/* type */
	, &GcAchievementData_t1617_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementData_t1617, ___m_PercentCompleted_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcAchievementData_t1617____m_Completed_2_FieldInfo = 
{
	"m_Completed"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcAchievementData_t1617_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementData_t1617, ___m_Completed_2) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcAchievementData_t1617____m_Hidden_3_FieldInfo = 
{
	"m_Hidden"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcAchievementData_t1617_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementData_t1617, ___m_Hidden_3) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcAchievementData_t1617____m_LastReportedDate_4_FieldInfo = 
{
	"m_LastReportedDate"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcAchievementData_t1617_il2cpp_TypeInfo/* parent */
	, offsetof(GcAchievementData_t1617, ___m_LastReportedDate_4) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* GcAchievementData_t1617_FieldInfos[] =
{
	&GcAchievementData_t1617____m_Identifier_0_FieldInfo,
	&GcAchievementData_t1617____m_PercentCompleted_1_FieldInfo,
	&GcAchievementData_t1617____m_Completed_2_FieldInfo,
	&GcAchievementData_t1617____m_Hidden_3_FieldInfo,
	&GcAchievementData_t1617____m_LastReportedDate_4_FieldInfo,
	NULL
};
static Il2CppMethodReference GcAchievementData_t1617_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool GcAchievementData_t1617_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GcAchievementData_t1617_0_0_0;
extern Il2CppType GcAchievementData_t1617_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementData_t1617_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, GcAchievementData_t1617_VTable/* vtableMethods */
	, GcAchievementData_t1617_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo GcAchievementData_t1617_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementData_t1617_MethodInfos/* methods */
	, NULL/* properties */
	, GcAchievementData_t1617_FieldInfos/* fields */
	, NULL/* events */
	, &GcAchievementData_t1617_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementData_t1617_0_0_0/* byval_arg */
	, &GcAchievementData_t1617_1_0_0/* this_arg */
	, &GcAchievementData_t1617_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcAchievementData_t1617_marshal/* marshal_to_native_func */
	, (methodPointerType)GcAchievementData_t1617_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcAchievementData_t1617_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcAchievementData_t1617)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementData_t1617)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcAchievementData_t1617_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern TypeInfo GcScoreData_t1618_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
extern Il2CppType Score_t1627_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
MethodInfo GcScoreData_ToScore_m7316_MethodInfo = 
{
	"ToScore"/* name */
	, (methodPointerType)&GcScoreData_ToScore_m7316/* method */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* declaring_type */
	, &Score_t1627_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GcScoreData_t1618_MethodInfos[] =
{
	&GcScoreData_ToScore_m7316_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_6;
FieldInfo GcScoreData_t1618____m_Category_0_FieldInfo = 
{
	"m_Category"/* name */
	, &String_t_0_0_6/* type */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* parent */
	, offsetof(GcScoreData_t1618, ___m_Category_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcScoreData_t1618____m_ValueLow_1_FieldInfo = 
{
	"m_ValueLow"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* parent */
	, offsetof(GcScoreData_t1618, ___m_ValueLow_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcScoreData_t1618____m_ValueHigh_2_FieldInfo = 
{
	"m_ValueHigh"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* parent */
	, offsetof(GcScoreData_t1618, ___m_ValueHigh_2) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcScoreData_t1618____m_Date_3_FieldInfo = 
{
	"m_Date"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* parent */
	, offsetof(GcScoreData_t1618, ___m_Date_3) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo GcScoreData_t1618____m_FormattedValue_4_FieldInfo = 
{
	"m_FormattedValue"/* name */
	, &String_t_0_0_6/* type */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* parent */
	, offsetof(GcScoreData_t1618, ___m_FormattedValue_4) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo GcScoreData_t1618____m_PlayerID_5_FieldInfo = 
{
	"m_PlayerID"/* name */
	, &String_t_0_0_6/* type */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* parent */
	, offsetof(GcScoreData_t1618, ___m_PlayerID_5) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo GcScoreData_t1618____m_Rank_6_FieldInfo = 
{
	"m_Rank"/* name */
	, &Int32_t189_0_0_6/* type */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* parent */
	, offsetof(GcScoreData_t1618, ___m_Rank_6) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* GcScoreData_t1618_FieldInfos[] =
{
	&GcScoreData_t1618____m_Category_0_FieldInfo,
	&GcScoreData_t1618____m_ValueLow_1_FieldInfo,
	&GcScoreData_t1618____m_ValueHigh_2_FieldInfo,
	&GcScoreData_t1618____m_Date_3_FieldInfo,
	&GcScoreData_t1618____m_FormattedValue_4_FieldInfo,
	&GcScoreData_t1618____m_PlayerID_5_FieldInfo,
	&GcScoreData_t1618____m_Rank_6_FieldInfo,
	NULL
};
static Il2CppMethodReference GcScoreData_t1618_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool GcScoreData_t1618_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GcScoreData_t1618_0_0_0;
extern Il2CppType GcScoreData_t1618_1_0_0;
const Il2CppTypeDefinitionMetadata GcScoreData_t1618_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, GcScoreData_t1618_VTable/* vtableMethods */
	, GcScoreData_t1618_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo GcScoreData_t1618_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcScoreData_t1618_MethodInfos/* methods */
	, NULL/* properties */
	, GcScoreData_t1618_FieldInfos/* fields */
	, NULL/* events */
	, &GcScoreData_t1618_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcScoreData_t1618_0_0_0/* byval_arg */
	, &GcScoreData_t1618_1_0_0/* this_arg */
	, &GcScoreData_t1618_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcScoreData_t1618_marshal/* marshal_to_native_func */
	, (methodPointerType)GcScoreData_t1618_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcScoreData_t1618_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcScoreData_t1618)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcScoreData_t1618)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcScoreData_t1618_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// Metadata Definition UnityEngine.RenderBuffer
extern TypeInfo RenderBuffer_t1619_il2cpp_TypeInfo;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
static MethodInfo* RenderBuffer_t1619_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo RenderBuffer_t1619____m_RenderTextureInstanceID_0_FieldInfo = 
{
	"m_RenderTextureInstanceID"/* name */
	, &Int32_t189_0_0_3/* type */
	, &RenderBuffer_t1619_il2cpp_TypeInfo/* parent */
	, offsetof(RenderBuffer_t1619, ___m_RenderTextureInstanceID_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t_0_0_3;
FieldInfo RenderBuffer_t1619____m_BufferPtr_1_FieldInfo = 
{
	"m_BufferPtr"/* name */
	, &IntPtr_t_0_0_3/* type */
	, &RenderBuffer_t1619_il2cpp_TypeInfo/* parent */
	, offsetof(RenderBuffer_t1619, ___m_BufferPtr_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RenderBuffer_t1619_FieldInfos[] =
{
	&RenderBuffer_t1619____m_RenderTextureInstanceID_0_FieldInfo,
	&RenderBuffer_t1619____m_BufferPtr_1_FieldInfo,
	NULL
};
static Il2CppMethodReference RenderBuffer_t1619_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool RenderBuffer_t1619_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RenderBuffer_t1619_0_0_0;
extern Il2CppType RenderBuffer_t1619_1_0_0;
const Il2CppTypeDefinitionMetadata RenderBuffer_t1619_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, RenderBuffer_t1619_VTable/* vtableMethods */
	, RenderBuffer_t1619_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RenderBuffer_t1619_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderBuffer"/* name */
	, "UnityEngine"/* namespaze */
	, RenderBuffer_t1619_MethodInfos/* methods */
	, NULL/* properties */
	, RenderBuffer_t1619_FieldInfos/* fields */
	, NULL/* events */
	, &RenderBuffer_t1619_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderBuffer_t1619_0_0_0/* byval_arg */
	, &RenderBuffer_t1619_1_0_0/* this_arg */
	, &RenderBuffer_t1619_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderBuffer_t1619)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderBuffer_t1619)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RenderBuffer_t1619 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// Metadata Definition UnityEngine.CameraClearFlags
extern TypeInfo CameraClearFlags_t1620_il2cpp_TypeInfo;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
static MethodInfo* CameraClearFlags_t1620_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo CameraClearFlags_t1620____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &CameraClearFlags_t1620_il2cpp_TypeInfo/* parent */
	, offsetof(CameraClearFlags_t1620, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CameraClearFlags_t1620_0_0_32854;
FieldInfo CameraClearFlags_t1620____Skybox_2_FieldInfo = 
{
	"Skybox"/* name */
	, &CameraClearFlags_t1620_0_0_32854/* type */
	, &CameraClearFlags_t1620_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CameraClearFlags_t1620_0_0_32854;
FieldInfo CameraClearFlags_t1620____Color_3_FieldInfo = 
{
	"Color"/* name */
	, &CameraClearFlags_t1620_0_0_32854/* type */
	, &CameraClearFlags_t1620_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CameraClearFlags_t1620_0_0_32854;
FieldInfo CameraClearFlags_t1620____SolidColor_4_FieldInfo = 
{
	"SolidColor"/* name */
	, &CameraClearFlags_t1620_0_0_32854/* type */
	, &CameraClearFlags_t1620_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CameraClearFlags_t1620_0_0_32854;
FieldInfo CameraClearFlags_t1620____Depth_5_FieldInfo = 
{
	"Depth"/* name */
	, &CameraClearFlags_t1620_0_0_32854/* type */
	, &CameraClearFlags_t1620_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CameraClearFlags_t1620_0_0_32854;
FieldInfo CameraClearFlags_t1620____Nothing_6_FieldInfo = 
{
	"Nothing"/* name */
	, &CameraClearFlags_t1620_0_0_32854/* type */
	, &CameraClearFlags_t1620_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CameraClearFlags_t1620_FieldInfos[] =
{
	&CameraClearFlags_t1620____value___1_FieldInfo,
	&CameraClearFlags_t1620____Skybox_2_FieldInfo,
	&CameraClearFlags_t1620____Color_3_FieldInfo,
	&CameraClearFlags_t1620____SolidColor_4_FieldInfo,
	&CameraClearFlags_t1620____Depth_5_FieldInfo,
	&CameraClearFlags_t1620____Nothing_6_FieldInfo,
	NULL
};
static const int32_t CameraClearFlags_t1620____Skybox_2_DefaultValueData = 1;
extern Il2CppType Int32_t189_0_0_0;
static Il2CppFieldDefaultValueEntry CameraClearFlags_t1620____Skybox_2_DefaultValue = 
{
	&CameraClearFlags_t1620____Skybox_2_FieldInfo/* field */
	, { (char*)&CameraClearFlags_t1620____Skybox_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t CameraClearFlags_t1620____Color_3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry CameraClearFlags_t1620____Color_3_DefaultValue = 
{
	&CameraClearFlags_t1620____Color_3_FieldInfo/* field */
	, { (char*)&CameraClearFlags_t1620____Color_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t CameraClearFlags_t1620____SolidColor_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry CameraClearFlags_t1620____SolidColor_4_DefaultValue = 
{
	&CameraClearFlags_t1620____SolidColor_4_FieldInfo/* field */
	, { (char*)&CameraClearFlags_t1620____SolidColor_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t CameraClearFlags_t1620____Depth_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry CameraClearFlags_t1620____Depth_5_DefaultValue = 
{
	&CameraClearFlags_t1620____Depth_5_FieldInfo/* field */
	, { (char*)&CameraClearFlags_t1620____Depth_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t CameraClearFlags_t1620____Nothing_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry CameraClearFlags_t1620____Nothing_6_DefaultValue = 
{
	&CameraClearFlags_t1620____Nothing_6_FieldInfo/* field */
	, { (char*)&CameraClearFlags_t1620____Nothing_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* CameraClearFlags_t1620_FieldDefaultValues[] = 
{
	&CameraClearFlags_t1620____Skybox_2_DefaultValue,
	&CameraClearFlags_t1620____Color_3_DefaultValue,
	&CameraClearFlags_t1620____SolidColor_4_DefaultValue,
	&CameraClearFlags_t1620____Depth_5_DefaultValue,
	&CameraClearFlags_t1620____Nothing_6_DefaultValue,
	NULL
};
extern MethodInfo Enum_Equals_m1279_MethodInfo;
extern MethodInfo Enum_GetHashCode_m1280_MethodInfo;
extern MethodInfo Enum_ToString_m1281_MethodInfo;
extern MethodInfo Enum_ToString_m1282_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m1283_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m1284_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m1285_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m1286_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m1287_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m1288_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m1289_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m1290_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m1291_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m1292_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m1293_MethodInfo;
extern MethodInfo Enum_ToString_m1294_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m1295_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m1296_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m1297_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m1298_MethodInfo;
extern MethodInfo Enum_CompareTo_m1299_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m1300_MethodInfo;
static Il2CppMethodReference CameraClearFlags_t1620_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool CameraClearFlags_t1620_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormattable_t312_0_0_0;
extern Il2CppType IConvertible_t313_0_0_0;
extern Il2CppType IComparable_t314_0_0_0;
static Il2CppInterfaceOffsetPair CameraClearFlags_t1620_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CameraClearFlags_t1620_0_0_0;
extern Il2CppType CameraClearFlags_t1620_1_0_0;
extern Il2CppType Enum_t218_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t189_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata CameraClearFlags_t1620_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraClearFlags_t1620_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, CameraClearFlags_t1620_VTable/* vtableMethods */
	, CameraClearFlags_t1620_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CameraClearFlags_t1620_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraClearFlags"/* name */
	, "UnityEngine"/* namespaze */
	, CameraClearFlags_t1620_MethodInfos/* methods */
	, NULL/* properties */
	, CameraClearFlags_t1620_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraClearFlags_t1620_0_0_0/* byval_arg */
	, &CameraClearFlags_t1620_1_0_0/* this_arg */
	, &CameraClearFlags_t1620_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, CameraClearFlags_t1620_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraClearFlags_t1620)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CameraClearFlags_t1620)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Metadata Definition UnityEngine.ScreenOrientation
extern TypeInfo ScreenOrientation_t1621_il2cpp_TypeInfo;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientationMethodDeclarations.h"
static MethodInfo* ScreenOrientation_t1621_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo ScreenOrientation_t1621____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, offsetof(ScreenOrientation_t1621, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t1621_0_0_32854;
FieldInfo ScreenOrientation_t1621____Unknown_2_FieldInfo = 
{
	"Unknown"/* name */
	, &ScreenOrientation_t1621_0_0_32854/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t1621_0_0_32854;
FieldInfo ScreenOrientation_t1621____Portrait_3_FieldInfo = 
{
	"Portrait"/* name */
	, &ScreenOrientation_t1621_0_0_32854/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t1621_0_0_32854;
FieldInfo ScreenOrientation_t1621____PortraitUpsideDown_4_FieldInfo = 
{
	"PortraitUpsideDown"/* name */
	, &ScreenOrientation_t1621_0_0_32854/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t1621_0_0_32854;
FieldInfo ScreenOrientation_t1621____LandscapeLeft_5_FieldInfo = 
{
	"LandscapeLeft"/* name */
	, &ScreenOrientation_t1621_0_0_32854/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t1621_0_0_32854;
FieldInfo ScreenOrientation_t1621____LandscapeRight_6_FieldInfo = 
{
	"LandscapeRight"/* name */
	, &ScreenOrientation_t1621_0_0_32854/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t1621_0_0_32854;
FieldInfo ScreenOrientation_t1621____AutoRotation_7_FieldInfo = 
{
	"AutoRotation"/* name */
	, &ScreenOrientation_t1621_0_0_32854/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t1621_0_0_32854;
FieldInfo ScreenOrientation_t1621____Landscape_8_FieldInfo = 
{
	"Landscape"/* name */
	, &ScreenOrientation_t1621_0_0_32854/* type */
	, &ScreenOrientation_t1621_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ScreenOrientation_t1621_FieldInfos[] =
{
	&ScreenOrientation_t1621____value___1_FieldInfo,
	&ScreenOrientation_t1621____Unknown_2_FieldInfo,
	&ScreenOrientation_t1621____Portrait_3_FieldInfo,
	&ScreenOrientation_t1621____PortraitUpsideDown_4_FieldInfo,
	&ScreenOrientation_t1621____LandscapeLeft_5_FieldInfo,
	&ScreenOrientation_t1621____LandscapeRight_6_FieldInfo,
	&ScreenOrientation_t1621____AutoRotation_7_FieldInfo,
	&ScreenOrientation_t1621____Landscape_8_FieldInfo,
	NULL
};
static const int32_t ScreenOrientation_t1621____Unknown_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t1621____Unknown_2_DefaultValue = 
{
	&ScreenOrientation_t1621____Unknown_2_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t1621____Unknown_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t1621____Portrait_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t1621____Portrait_3_DefaultValue = 
{
	&ScreenOrientation_t1621____Portrait_3_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t1621____Portrait_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t1621____PortraitUpsideDown_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t1621____PortraitUpsideDown_4_DefaultValue = 
{
	&ScreenOrientation_t1621____PortraitUpsideDown_4_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t1621____PortraitUpsideDown_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t1621____LandscapeLeft_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t1621____LandscapeLeft_5_DefaultValue = 
{
	&ScreenOrientation_t1621____LandscapeLeft_5_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t1621____LandscapeLeft_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t1621____LandscapeRight_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t1621____LandscapeRight_6_DefaultValue = 
{
	&ScreenOrientation_t1621____LandscapeRight_6_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t1621____LandscapeRight_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t1621____AutoRotation_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t1621____AutoRotation_7_DefaultValue = 
{
	&ScreenOrientation_t1621____AutoRotation_7_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t1621____AutoRotation_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t1621____Landscape_8_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t1621____Landscape_8_DefaultValue = 
{
	&ScreenOrientation_t1621____Landscape_8_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t1621____Landscape_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ScreenOrientation_t1621_FieldDefaultValues[] = 
{
	&ScreenOrientation_t1621____Unknown_2_DefaultValue,
	&ScreenOrientation_t1621____Portrait_3_DefaultValue,
	&ScreenOrientation_t1621____PortraitUpsideDown_4_DefaultValue,
	&ScreenOrientation_t1621____LandscapeLeft_5_DefaultValue,
	&ScreenOrientation_t1621____LandscapeRight_6_DefaultValue,
	&ScreenOrientation_t1621____AutoRotation_7_DefaultValue,
	&ScreenOrientation_t1621____Landscape_8_DefaultValue,
	NULL
};
static Il2CppMethodReference ScreenOrientation_t1621_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool ScreenOrientation_t1621_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenOrientation_t1621_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ScreenOrientation_t1621_0_0_0;
extern Il2CppType ScreenOrientation_t1621_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenOrientation_t1621_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenOrientation_t1621_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, ScreenOrientation_t1621_VTable/* vtableMethods */
	, ScreenOrientation_t1621_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ScreenOrientation_t1621_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenOrientation"/* name */
	, "UnityEngine"/* namespaze */
	, ScreenOrientation_t1621_MethodInfos/* methods */
	, NULL/* properties */
	, ScreenOrientation_t1621_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenOrientation_t1621_0_0_0/* byval_arg */
	, &ScreenOrientation_t1621_1_0_0/* this_arg */
	, &ScreenOrientation_t1621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ScreenOrientation_t1621_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenOrientation_t1621)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenOrientation_t1621)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// Metadata Definition UnityEngine.TextureFormat
extern TypeInfo TextureFormat_t1622_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
static MethodInfo* TextureFormat_t1622_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo TextureFormat_t1622____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, offsetof(TextureFormat_t1622, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____Alpha8_2_FieldInfo = 
{
	"Alpha8"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ARGB4444_3_FieldInfo = 
{
	"ARGB4444"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGB24_4_FieldInfo = 
{
	"RGB24"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGBA32_5_FieldInfo = 
{
	"RGBA32"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ARGB32_6_FieldInfo = 
{
	"ARGB32"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGB565_7_FieldInfo = 
{
	"RGB565"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____R16_8_FieldInfo = 
{
	"R16"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____DXT1_9_FieldInfo = 
{
	"DXT1"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____DXT5_10_FieldInfo = 
{
	"DXT5"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGBA4444_11_FieldInfo = 
{
	"RGBA4444"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____BGRA32_12_FieldInfo = 
{
	"BGRA32"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RHalf_13_FieldInfo = 
{
	"RHalf"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGHalf_14_FieldInfo = 
{
	"RGHalf"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGBAHalf_15_FieldInfo = 
{
	"RGBAHalf"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RFloat_16_FieldInfo = 
{
	"RFloat"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGFloat_17_FieldInfo = 
{
	"RGFloat"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____RGBAFloat_18_FieldInfo = 
{
	"RGBAFloat"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____YUY2_19_FieldInfo = 
{
	"YUY2"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____PVRTC_RGB2_20_FieldInfo = 
{
	"PVRTC_RGB2"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____PVRTC_RGBA2_21_FieldInfo = 
{
	"PVRTC_RGBA2"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____PVRTC_RGB4_22_FieldInfo = 
{
	"PVRTC_RGB4"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____PVRTC_RGBA4_23_FieldInfo = 
{
	"PVRTC_RGBA4"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ETC_RGB4_24_FieldInfo = 
{
	"ETC_RGB4"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ATC_RGB4_25_FieldInfo = 
{
	"ATC_RGB4"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ATC_RGBA8_26_FieldInfo = 
{
	"ATC_RGBA8"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____EAC_R_27_FieldInfo = 
{
	"EAC_R"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____EAC_R_SIGNED_28_FieldInfo = 
{
	"EAC_R_SIGNED"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____EAC_RG_29_FieldInfo = 
{
	"EAC_RG"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____EAC_RG_SIGNED_30_FieldInfo = 
{
	"EAC_RG_SIGNED"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ETC2_RGB_31_FieldInfo = 
{
	"ETC2_RGB"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ETC2_RGBA1_32_FieldInfo = 
{
	"ETC2_RGBA1"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ETC2_RGBA8_33_FieldInfo = 
{
	"ETC2_RGBA8"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGB_4x4_34_FieldInfo = 
{
	"ASTC_RGB_4x4"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGB_5x5_35_FieldInfo = 
{
	"ASTC_RGB_5x5"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGB_6x6_36_FieldInfo = 
{
	"ASTC_RGB_6x6"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGB_8x8_37_FieldInfo = 
{
	"ASTC_RGB_8x8"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGB_10x10_38_FieldInfo = 
{
	"ASTC_RGB_10x10"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGB_12x12_39_FieldInfo = 
{
	"ASTC_RGB_12x12"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGBA_4x4_40_FieldInfo = 
{
	"ASTC_RGBA_4x4"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGBA_5x5_41_FieldInfo = 
{
	"ASTC_RGBA_5x5"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGBA_6x6_42_FieldInfo = 
{
	"ASTC_RGBA_6x6"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGBA_8x8_43_FieldInfo = 
{
	"ASTC_RGBA_8x8"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGBA_10x10_44_FieldInfo = 
{
	"ASTC_RGBA_10x10"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t1622_0_0_32854;
FieldInfo TextureFormat_t1622____ASTC_RGBA_12x12_45_FieldInfo = 
{
	"ASTC_RGBA_12x12"/* name */
	, &TextureFormat_t1622_0_0_32854/* type */
	, &TextureFormat_t1622_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TextureFormat_t1622_FieldInfos[] =
{
	&TextureFormat_t1622____value___1_FieldInfo,
	&TextureFormat_t1622____Alpha8_2_FieldInfo,
	&TextureFormat_t1622____ARGB4444_3_FieldInfo,
	&TextureFormat_t1622____RGB24_4_FieldInfo,
	&TextureFormat_t1622____RGBA32_5_FieldInfo,
	&TextureFormat_t1622____ARGB32_6_FieldInfo,
	&TextureFormat_t1622____RGB565_7_FieldInfo,
	&TextureFormat_t1622____R16_8_FieldInfo,
	&TextureFormat_t1622____DXT1_9_FieldInfo,
	&TextureFormat_t1622____DXT5_10_FieldInfo,
	&TextureFormat_t1622____RGBA4444_11_FieldInfo,
	&TextureFormat_t1622____BGRA32_12_FieldInfo,
	&TextureFormat_t1622____RHalf_13_FieldInfo,
	&TextureFormat_t1622____RGHalf_14_FieldInfo,
	&TextureFormat_t1622____RGBAHalf_15_FieldInfo,
	&TextureFormat_t1622____RFloat_16_FieldInfo,
	&TextureFormat_t1622____RGFloat_17_FieldInfo,
	&TextureFormat_t1622____RGBAFloat_18_FieldInfo,
	&TextureFormat_t1622____YUY2_19_FieldInfo,
	&TextureFormat_t1622____PVRTC_RGB2_20_FieldInfo,
	&TextureFormat_t1622____PVRTC_RGBA2_21_FieldInfo,
	&TextureFormat_t1622____PVRTC_RGB4_22_FieldInfo,
	&TextureFormat_t1622____PVRTC_RGBA4_23_FieldInfo,
	&TextureFormat_t1622____ETC_RGB4_24_FieldInfo,
	&TextureFormat_t1622____ATC_RGB4_25_FieldInfo,
	&TextureFormat_t1622____ATC_RGBA8_26_FieldInfo,
	&TextureFormat_t1622____EAC_R_27_FieldInfo,
	&TextureFormat_t1622____EAC_R_SIGNED_28_FieldInfo,
	&TextureFormat_t1622____EAC_RG_29_FieldInfo,
	&TextureFormat_t1622____EAC_RG_SIGNED_30_FieldInfo,
	&TextureFormat_t1622____ETC2_RGB_31_FieldInfo,
	&TextureFormat_t1622____ETC2_RGBA1_32_FieldInfo,
	&TextureFormat_t1622____ETC2_RGBA8_33_FieldInfo,
	&TextureFormat_t1622____ASTC_RGB_4x4_34_FieldInfo,
	&TextureFormat_t1622____ASTC_RGB_5x5_35_FieldInfo,
	&TextureFormat_t1622____ASTC_RGB_6x6_36_FieldInfo,
	&TextureFormat_t1622____ASTC_RGB_8x8_37_FieldInfo,
	&TextureFormat_t1622____ASTC_RGB_10x10_38_FieldInfo,
	&TextureFormat_t1622____ASTC_RGB_12x12_39_FieldInfo,
	&TextureFormat_t1622____ASTC_RGBA_4x4_40_FieldInfo,
	&TextureFormat_t1622____ASTC_RGBA_5x5_41_FieldInfo,
	&TextureFormat_t1622____ASTC_RGBA_6x6_42_FieldInfo,
	&TextureFormat_t1622____ASTC_RGBA_8x8_43_FieldInfo,
	&TextureFormat_t1622____ASTC_RGBA_10x10_44_FieldInfo,
	&TextureFormat_t1622____ASTC_RGBA_12x12_45_FieldInfo,
	NULL
};
static const int32_t TextureFormat_t1622____Alpha8_2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____Alpha8_2_DefaultValue = 
{
	&TextureFormat_t1622____Alpha8_2_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____Alpha8_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ARGB4444_3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ARGB4444_3_DefaultValue = 
{
	&TextureFormat_t1622____ARGB4444_3_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ARGB4444_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGB24_4_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGB24_4_DefaultValue = 
{
	&TextureFormat_t1622____RGB24_4_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGB24_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGBA32_5_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGBA32_5_DefaultValue = 
{
	&TextureFormat_t1622____RGBA32_5_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGBA32_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ARGB32_6_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ARGB32_6_DefaultValue = 
{
	&TextureFormat_t1622____ARGB32_6_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ARGB32_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGB565_7_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGB565_7_DefaultValue = 
{
	&TextureFormat_t1622____RGB565_7_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGB565_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____R16_8_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____R16_8_DefaultValue = 
{
	&TextureFormat_t1622____R16_8_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____R16_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____DXT1_9_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____DXT1_9_DefaultValue = 
{
	&TextureFormat_t1622____DXT1_9_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____DXT1_9_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____DXT5_10_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____DXT5_10_DefaultValue = 
{
	&TextureFormat_t1622____DXT5_10_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____DXT5_10_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGBA4444_11_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGBA4444_11_DefaultValue = 
{
	&TextureFormat_t1622____RGBA4444_11_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGBA4444_11_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____BGRA32_12_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____BGRA32_12_DefaultValue = 
{
	&TextureFormat_t1622____BGRA32_12_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____BGRA32_12_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RHalf_13_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RHalf_13_DefaultValue = 
{
	&TextureFormat_t1622____RHalf_13_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RHalf_13_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGHalf_14_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGHalf_14_DefaultValue = 
{
	&TextureFormat_t1622____RGHalf_14_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGHalf_14_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGBAHalf_15_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGBAHalf_15_DefaultValue = 
{
	&TextureFormat_t1622____RGBAHalf_15_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGBAHalf_15_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RFloat_16_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RFloat_16_DefaultValue = 
{
	&TextureFormat_t1622____RFloat_16_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RFloat_16_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGFloat_17_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGFloat_17_DefaultValue = 
{
	&TextureFormat_t1622____RGFloat_17_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGFloat_17_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____RGBAFloat_18_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____RGBAFloat_18_DefaultValue = 
{
	&TextureFormat_t1622____RGBAFloat_18_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____RGBAFloat_18_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____YUY2_19_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____YUY2_19_DefaultValue = 
{
	&TextureFormat_t1622____YUY2_19_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____YUY2_19_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____PVRTC_RGB2_20_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____PVRTC_RGB2_20_DefaultValue = 
{
	&TextureFormat_t1622____PVRTC_RGB2_20_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____PVRTC_RGB2_20_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____PVRTC_RGBA2_21_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____PVRTC_RGBA2_21_DefaultValue = 
{
	&TextureFormat_t1622____PVRTC_RGBA2_21_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____PVRTC_RGBA2_21_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____PVRTC_RGB4_22_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____PVRTC_RGB4_22_DefaultValue = 
{
	&TextureFormat_t1622____PVRTC_RGB4_22_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____PVRTC_RGB4_22_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____PVRTC_RGBA4_23_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____PVRTC_RGBA4_23_DefaultValue = 
{
	&TextureFormat_t1622____PVRTC_RGBA4_23_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____PVRTC_RGBA4_23_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ETC_RGB4_24_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ETC_RGB4_24_DefaultValue = 
{
	&TextureFormat_t1622____ETC_RGB4_24_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ETC_RGB4_24_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ATC_RGB4_25_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ATC_RGB4_25_DefaultValue = 
{
	&TextureFormat_t1622____ATC_RGB4_25_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ATC_RGB4_25_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ATC_RGBA8_26_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ATC_RGBA8_26_DefaultValue = 
{
	&TextureFormat_t1622____ATC_RGBA8_26_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ATC_RGBA8_26_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____EAC_R_27_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____EAC_R_27_DefaultValue = 
{
	&TextureFormat_t1622____EAC_R_27_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____EAC_R_27_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____EAC_R_SIGNED_28_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____EAC_R_SIGNED_28_DefaultValue = 
{
	&TextureFormat_t1622____EAC_R_SIGNED_28_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____EAC_R_SIGNED_28_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____EAC_RG_29_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____EAC_RG_29_DefaultValue = 
{
	&TextureFormat_t1622____EAC_RG_29_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____EAC_RG_29_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____EAC_RG_SIGNED_30_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____EAC_RG_SIGNED_30_DefaultValue = 
{
	&TextureFormat_t1622____EAC_RG_SIGNED_30_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____EAC_RG_SIGNED_30_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ETC2_RGB_31_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ETC2_RGB_31_DefaultValue = 
{
	&TextureFormat_t1622____ETC2_RGB_31_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ETC2_RGB_31_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ETC2_RGBA1_32_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ETC2_RGBA1_32_DefaultValue = 
{
	&TextureFormat_t1622____ETC2_RGBA1_32_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ETC2_RGBA1_32_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ETC2_RGBA8_33_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ETC2_RGBA8_33_DefaultValue = 
{
	&TextureFormat_t1622____ETC2_RGBA8_33_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ETC2_RGBA8_33_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGB_4x4_34_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGB_4x4_34_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGB_4x4_34_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGB_4x4_34_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGB_5x5_35_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGB_5x5_35_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGB_5x5_35_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGB_5x5_35_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGB_6x6_36_DefaultValueData = 50;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGB_6x6_36_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGB_6x6_36_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGB_6x6_36_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGB_8x8_37_DefaultValueData = 51;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGB_8x8_37_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGB_8x8_37_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGB_8x8_37_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGB_10x10_38_DefaultValueData = 52;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGB_10x10_38_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGB_10x10_38_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGB_10x10_38_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGB_12x12_39_DefaultValueData = 53;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGB_12x12_39_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGB_12x12_39_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGB_12x12_39_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGBA_4x4_40_DefaultValueData = 54;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGBA_4x4_40_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGBA_4x4_40_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGBA_4x4_40_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGBA_5x5_41_DefaultValueData = 55;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGBA_5x5_41_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGBA_5x5_41_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGBA_5x5_41_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGBA_6x6_42_DefaultValueData = 56;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGBA_6x6_42_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGBA_6x6_42_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGBA_6x6_42_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGBA_8x8_43_DefaultValueData = 57;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGBA_8x8_43_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGBA_8x8_43_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGBA_8x8_43_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGBA_10x10_44_DefaultValueData = 58;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGBA_10x10_44_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGBA_10x10_44_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGBA_10x10_44_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextureFormat_t1622____ASTC_RGBA_12x12_45_DefaultValueData = 59;
static Il2CppFieldDefaultValueEntry TextureFormat_t1622____ASTC_RGBA_12x12_45_DefaultValue = 
{
	&TextureFormat_t1622____ASTC_RGBA_12x12_45_FieldInfo/* field */
	, { (char*)&TextureFormat_t1622____ASTC_RGBA_12x12_45_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TextureFormat_t1622_FieldDefaultValues[] = 
{
	&TextureFormat_t1622____Alpha8_2_DefaultValue,
	&TextureFormat_t1622____ARGB4444_3_DefaultValue,
	&TextureFormat_t1622____RGB24_4_DefaultValue,
	&TextureFormat_t1622____RGBA32_5_DefaultValue,
	&TextureFormat_t1622____ARGB32_6_DefaultValue,
	&TextureFormat_t1622____RGB565_7_DefaultValue,
	&TextureFormat_t1622____R16_8_DefaultValue,
	&TextureFormat_t1622____DXT1_9_DefaultValue,
	&TextureFormat_t1622____DXT5_10_DefaultValue,
	&TextureFormat_t1622____RGBA4444_11_DefaultValue,
	&TextureFormat_t1622____BGRA32_12_DefaultValue,
	&TextureFormat_t1622____RHalf_13_DefaultValue,
	&TextureFormat_t1622____RGHalf_14_DefaultValue,
	&TextureFormat_t1622____RGBAHalf_15_DefaultValue,
	&TextureFormat_t1622____RFloat_16_DefaultValue,
	&TextureFormat_t1622____RGFloat_17_DefaultValue,
	&TextureFormat_t1622____RGBAFloat_18_DefaultValue,
	&TextureFormat_t1622____YUY2_19_DefaultValue,
	&TextureFormat_t1622____PVRTC_RGB2_20_DefaultValue,
	&TextureFormat_t1622____PVRTC_RGBA2_21_DefaultValue,
	&TextureFormat_t1622____PVRTC_RGB4_22_DefaultValue,
	&TextureFormat_t1622____PVRTC_RGBA4_23_DefaultValue,
	&TextureFormat_t1622____ETC_RGB4_24_DefaultValue,
	&TextureFormat_t1622____ATC_RGB4_25_DefaultValue,
	&TextureFormat_t1622____ATC_RGBA8_26_DefaultValue,
	&TextureFormat_t1622____EAC_R_27_DefaultValue,
	&TextureFormat_t1622____EAC_R_SIGNED_28_DefaultValue,
	&TextureFormat_t1622____EAC_RG_29_DefaultValue,
	&TextureFormat_t1622____EAC_RG_SIGNED_30_DefaultValue,
	&TextureFormat_t1622____ETC2_RGB_31_DefaultValue,
	&TextureFormat_t1622____ETC2_RGBA1_32_DefaultValue,
	&TextureFormat_t1622____ETC2_RGBA8_33_DefaultValue,
	&TextureFormat_t1622____ASTC_RGB_4x4_34_DefaultValue,
	&TextureFormat_t1622____ASTC_RGB_5x5_35_DefaultValue,
	&TextureFormat_t1622____ASTC_RGB_6x6_36_DefaultValue,
	&TextureFormat_t1622____ASTC_RGB_8x8_37_DefaultValue,
	&TextureFormat_t1622____ASTC_RGB_10x10_38_DefaultValue,
	&TextureFormat_t1622____ASTC_RGB_12x12_39_DefaultValue,
	&TextureFormat_t1622____ASTC_RGBA_4x4_40_DefaultValue,
	&TextureFormat_t1622____ASTC_RGBA_5x5_41_DefaultValue,
	&TextureFormat_t1622____ASTC_RGBA_6x6_42_DefaultValue,
	&TextureFormat_t1622____ASTC_RGBA_8x8_43_DefaultValue,
	&TextureFormat_t1622____ASTC_RGBA_10x10_44_DefaultValue,
	&TextureFormat_t1622____ASTC_RGBA_12x12_45_DefaultValue,
	NULL
};
static Il2CppMethodReference TextureFormat_t1622_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool TextureFormat_t1622_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureFormat_t1622_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextureFormat_t1622_0_0_0;
extern Il2CppType TextureFormat_t1622_1_0_0;
const Il2CppTypeDefinitionMetadata TextureFormat_t1622_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureFormat_t1622_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, TextureFormat_t1622_VTable/* vtableMethods */
	, TextureFormat_t1622_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TextureFormat_t1622_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, TextureFormat_t1622_MethodInfos/* methods */
	, NULL/* properties */
	, TextureFormat_t1622_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureFormat_t1622_0_0_0/* byval_arg */
	, &TextureFormat_t1622_1_0_0/* this_arg */
	, &TextureFormat_t1622_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TextureFormat_t1622_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t1622)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureFormat_t1622)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern TypeInfo ReflectionProbeBlendInfo_t1623_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
static MethodInfo* ReflectionProbeBlendInfo_t1623_MethodInfos[] =
{
	NULL
};
extern Il2CppType ReflectionProbe_t1508_0_0_6;
FieldInfo ReflectionProbeBlendInfo_t1623____probe_0_FieldInfo = 
{
	"probe"/* name */
	, &ReflectionProbe_t1508_0_0_6/* type */
	, &ReflectionProbeBlendInfo_t1623_il2cpp_TypeInfo/* parent */
	, offsetof(ReflectionProbeBlendInfo_t1623, ___probe_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo ReflectionProbeBlendInfo_t1623____weight_1_FieldInfo = 
{
	"weight"/* name */
	, &Single_t202_0_0_6/* type */
	, &ReflectionProbeBlendInfo_t1623_il2cpp_TypeInfo/* parent */
	, offsetof(ReflectionProbeBlendInfo_t1623, ___weight_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ReflectionProbeBlendInfo_t1623_FieldInfos[] =
{
	&ReflectionProbeBlendInfo_t1623____probe_0_FieldInfo,
	&ReflectionProbeBlendInfo_t1623____weight_1_FieldInfo,
	NULL
};
static Il2CppMethodReference ReflectionProbeBlendInfo_t1623_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool ReflectionProbeBlendInfo_t1623_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ReflectionProbeBlendInfo_t1623_0_0_0;
extern Il2CppType ReflectionProbeBlendInfo_t1623_1_0_0;
const Il2CppTypeDefinitionMetadata ReflectionProbeBlendInfo_t1623_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, ReflectionProbeBlendInfo_t1623_VTable/* vtableMethods */
	, ReflectionProbeBlendInfo_t1623_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ReflectionProbeBlendInfo_t1623_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, ReflectionProbeBlendInfo_t1623_MethodInfos/* methods */
	, NULL/* properties */
	, ReflectionProbeBlendInfo_t1623_FieldInfos/* fields */
	, NULL/* events */
	, &ReflectionProbeBlendInfo_t1623_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t1623_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t1623_1_0_0/* this_arg */
	, &ReflectionProbeBlendInfo_t1623_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t1623)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReflectionProbeBlendInfo_t1623)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern TypeInfo LocalUser_t1498_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
MethodInfo LocalUser__ctor_m7317_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LocalUser__ctor_m7317/* method */
	, &LocalUser_t1498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_1_t98_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo LocalUser_t1498_LocalUser_Authenticate_m7318_ParameterInfos[] = 
{
	{"callback", 0, 134219337, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::Authenticate(System.Action`1<System.Boolean>)
MethodInfo LocalUser_Authenticate_m7318_MethodInfo = 
{
	"Authenticate"/* name */
	, (methodPointerType)&LocalUser_Authenticate_m7318/* method */
	, &LocalUser_t1498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LocalUser_t1498_LocalUser_Authenticate_m7318_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IUserProfileU5BU5D_t850_0_0_0;
extern Il2CppType IUserProfileU5BU5D_t850_0_0_0;
static ParameterInfo LocalUser_t1498_LocalUser_SetFriends_m7319_ParameterInfos[] = 
{
	{"friends", 0, 134219338, 0, &IUserProfileU5BU5D_t850_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
MethodInfo LocalUser_SetFriends_m7319_MethodInfo = 
{
	"SetFriends"/* name */
	, (methodPointerType)&LocalUser_SetFriends_m7319/* method */
	, &LocalUser_t1498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LocalUser_t1498_LocalUser_SetFriends_m7319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo LocalUser_t1498_LocalUser_SetAuthenticated_m7320_ParameterInfos[] = 
{
	{"value", 0, 134219339, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
MethodInfo LocalUser_SetAuthenticated_m7320_MethodInfo = 
{
	"SetAuthenticated"/* name */
	, (methodPointerType)&LocalUser_SetAuthenticated_m7320/* method */
	, &LocalUser_t1498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, LocalUser_t1498_LocalUser_SetAuthenticated_m7320_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo LocalUser_t1498_LocalUser_SetUnderage_m7321_ParameterInfos[] = 
{
	{"value", 0, 134219340, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
MethodInfo LocalUser_SetUnderage_m7321_MethodInfo = 
{
	"SetUnderage"/* name */
	, (methodPointerType)&LocalUser_SetUnderage_m7321/* method */
	, &LocalUser_t1498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, LocalUser_t1498_LocalUser_SetUnderage_m7321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
MethodInfo LocalUser_get_authenticated_m7322_MethodInfo = 
{
	"get_authenticated"/* name */
	, (methodPointerType)&LocalUser_get_authenticated_m7322/* method */
	, &LocalUser_t1498_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LocalUser_t1498_MethodInfos[] =
{
	&LocalUser__ctor_m7317_MethodInfo,
	&LocalUser_Authenticate_m7318_MethodInfo,
	&LocalUser_SetFriends_m7319_MethodInfo,
	&LocalUser_SetAuthenticated_m7320_MethodInfo,
	&LocalUser_SetUnderage_m7321_MethodInfo,
	&LocalUser_get_authenticated_m7322_MethodInfo,
	NULL
};
extern Il2CppType IUserProfileU5BU5D_t850_0_0_1;
FieldInfo LocalUser_t1498____m_Friends_5_FieldInfo = 
{
	"m_Friends"/* name */
	, &IUserProfileU5BU5D_t850_0_0_1/* type */
	, &LocalUser_t1498_il2cpp_TypeInfo/* parent */
	, offsetof(LocalUser_t1498, ___m_Friends_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo LocalUser_t1498____m_Authenticated_6_FieldInfo = 
{
	"m_Authenticated"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &LocalUser_t1498_il2cpp_TypeInfo/* parent */
	, offsetof(LocalUser_t1498, ___m_Authenticated_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo LocalUser_t1498____m_Underage_7_FieldInfo = 
{
	"m_Underage"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &LocalUser_t1498_il2cpp_TypeInfo/* parent */
	, offsetof(LocalUser_t1498, ___m_Underage_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* LocalUser_t1498_FieldInfos[] =
{
	&LocalUser_t1498____m_Friends_5_FieldInfo,
	&LocalUser_t1498____m_Authenticated_6_FieldInfo,
	&LocalUser_t1498____m_Underage_7_FieldInfo,
	NULL
};
extern MethodInfo LocalUser_get_authenticated_m7322_MethodInfo;
static PropertyInfo LocalUser_t1498____authenticated_PropertyInfo = 
{
	&LocalUser_t1498_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &LocalUser_get_authenticated_m7322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* LocalUser_t1498_PropertyInfos[] =
{
	&LocalUser_t1498____authenticated_PropertyInfo,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo UserProfile_ToString_m7325_MethodInfo;
extern MethodInfo UserProfile_get_id_m7330_MethodInfo;
extern MethodInfo UserProfile_get_userName_m7329_MethodInfo;
extern MethodInfo UserProfile_get_isFriend_m7331_MethodInfo;
extern MethodInfo UserProfile_get_state_m7332_MethodInfo;
extern MethodInfo LocalUser_Authenticate_m7318_MethodInfo;
static Il2CppMethodReference LocalUser_t1498_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UserProfile_ToString_m7325_MethodInfo,
	&UserProfile_get_id_m7330_MethodInfo,
	&UserProfile_get_userName_m7329_MethodInfo,
	&UserProfile_get_isFriend_m7331_MethodInfo,
	&UserProfile_get_state_m7332_MethodInfo,
	&LocalUser_Authenticate_m7318_MethodInfo,
	&LocalUser_get_authenticated_m7322_MethodInfo,
};
static bool LocalUser_t1498_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ILocalUser_t851_0_0_0;
extern Il2CppType IUserProfile_t983_0_0_0;
static const Il2CppType* LocalUser_t1498_InterfacesTypeInfos[] = 
{
	&ILocalUser_t851_0_0_0,
	&IUserProfile_t983_0_0_0,
};
static Il2CppInterfaceOffsetPair LocalUser_t1498_InterfacesOffsets[] = 
{
	{ &IUserProfile_t983_0_0_0, 4},
	{ &ILocalUser_t851_0_0_0, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType LocalUser_t1498_0_0_0;
extern Il2CppType LocalUser_t1498_1_0_0;
extern Il2CppType UserProfile_t1624_0_0_0;
struct LocalUser_t1498;
const Il2CppTypeDefinitionMetadata LocalUser_t1498_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LocalUser_t1498_InterfacesTypeInfos/* implementedInterfaces */
	, LocalUser_t1498_InterfacesOffsets/* interfaceOffsets */
	, &UserProfile_t1624_0_0_0/* parent */
	, LocalUser_t1498_VTable/* vtableMethods */
	, LocalUser_t1498_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LocalUser_t1498_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, LocalUser_t1498_MethodInfos/* methods */
	, LocalUser_t1498_PropertyInfos/* properties */
	, LocalUser_t1498_FieldInfos/* fields */
	, NULL/* events */
	, &LocalUser_t1498_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LocalUser_t1498_0_0_0/* byval_arg */
	, &LocalUser_t1498_1_0_0/* this_arg */
	, &LocalUser_t1498_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t1498)/* instance_size */
	, sizeof (LocalUser_t1498)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern TypeInfo UserProfile_t1624_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
MethodInfo UserProfile__ctor_m7323_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m7323/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType UserState_t1020_0_0_0;
extern Il2CppType UserState_t1020_0_0_0;
extern Il2CppType Texture2D_t384_0_0_0;
extern Il2CppType Texture2D_t384_0_0_0;
static ParameterInfo UserProfile_t1624_UserProfile__ctor_m7324_ParameterInfos[] = 
{
	{"name", 0, 134219341, 0, &String_t_0_0_0},
	{"id", 1, 134219342, 0, &String_t_0_0_0},
	{"friend", 2, 134219343, 0, &Boolean_t203_0_0_0},
	{"state", 3, 134219344, 0, &UserState_t1020_0_0_0},
	{"image", 4, 134219345, 0, &Texture2D_t384_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
MethodInfo UserProfile__ctor_m7324_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m7324/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Int32_t189_Object_t/* invoker_method */
	, UserProfile_t1624_UserProfile__ctor_m7324_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
MethodInfo UserProfile_ToString_m7325_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UserProfile_ToString_m7325/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UserProfile_t1624_UserProfile_SetUserName_m7326_ParameterInfos[] = 
{
	{"name", 0, 134219346, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
MethodInfo UserProfile_SetUserName_m7326_MethodInfo = 
{
	"SetUserName"/* name */
	, (methodPointerType)&UserProfile_SetUserName_m7326/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UserProfile_t1624_UserProfile_SetUserName_m7326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UserProfile_t1624_UserProfile_SetUserID_m7327_ParameterInfos[] = 
{
	{"id", 0, 134219347, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
MethodInfo UserProfile_SetUserID_m7327_MethodInfo = 
{
	"SetUserID"/* name */
	, (methodPointerType)&UserProfile_SetUserID_m7327/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UserProfile_t1624_UserProfile_SetUserID_m7327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Texture2D_t384_0_0_0;
static ParameterInfo UserProfile_t1624_UserProfile_SetImage_m7328_ParameterInfos[] = 
{
	{"image", 0, 134219348, 0, &Texture2D_t384_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
MethodInfo UserProfile_SetImage_m7328_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&UserProfile_SetImage_m7328/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UserProfile_t1624_UserProfile_SetImage_m7328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
MethodInfo UserProfile_get_userName_m7329_MethodInfo = 
{
	"get_userName"/* name */
	, (methodPointerType)&UserProfile_get_userName_m7329/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
MethodInfo UserProfile_get_id_m7330_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&UserProfile_get_id_m7330/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
MethodInfo UserProfile_get_isFriend_m7331_MethodInfo = 
{
	"get_isFriend"/* name */
	, (methodPointerType)&UserProfile_get_isFriend_m7331/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserState_t1020_0_0_0;
extern void* RuntimeInvoker_UserState_t1020 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
MethodInfo UserProfile_get_state_m7332_MethodInfo = 
{
	"get_state"/* name */
	, (methodPointerType)&UserProfile_get_state_m7332/* method */
	, &UserProfile_t1624_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t1020_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t1020/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserProfile_t1624_MethodInfos[] =
{
	&UserProfile__ctor_m7323_MethodInfo,
	&UserProfile__ctor_m7324_MethodInfo,
	&UserProfile_ToString_m7325_MethodInfo,
	&UserProfile_SetUserName_m7326_MethodInfo,
	&UserProfile_SetUserID_m7327_MethodInfo,
	&UserProfile_SetImage_m7328_MethodInfo,
	&UserProfile_get_userName_m7329_MethodInfo,
	&UserProfile_get_id_m7330_MethodInfo,
	&UserProfile_get_isFriend_m7331_MethodInfo,
	&UserProfile_get_state_m7332_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_4;
FieldInfo UserProfile_t1624____m_UserName_0_FieldInfo = 
{
	"m_UserName"/* name */
	, &String_t_0_0_4/* type */
	, &UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1624, ___m_UserName_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_4;
FieldInfo UserProfile_t1624____m_ID_1_FieldInfo = 
{
	"m_ID"/* name */
	, &String_t_0_0_4/* type */
	, &UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1624, ___m_ID_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_4;
FieldInfo UserProfile_t1624____m_IsFriend_2_FieldInfo = 
{
	"m_IsFriend"/* name */
	, &Boolean_t203_0_0_4/* type */
	, &UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1624, ___m_IsFriend_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserState_t1020_0_0_4;
FieldInfo UserProfile_t1624____m_State_3_FieldInfo = 
{
	"m_State"/* name */
	, &UserState_t1020_0_0_4/* type */
	, &UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1624, ___m_State_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Texture2D_t384_0_0_4;
FieldInfo UserProfile_t1624____m_Image_4_FieldInfo = 
{
	"m_Image"/* name */
	, &Texture2D_t384_0_0_4/* type */
	, &UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1624, ___m_Image_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UserProfile_t1624_FieldInfos[] =
{
	&UserProfile_t1624____m_UserName_0_FieldInfo,
	&UserProfile_t1624____m_ID_1_FieldInfo,
	&UserProfile_t1624____m_IsFriend_2_FieldInfo,
	&UserProfile_t1624____m_State_3_FieldInfo,
	&UserProfile_t1624____m_Image_4_FieldInfo,
	NULL
};
static PropertyInfo UserProfile_t1624____userName_PropertyInfo = 
{
	&UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, "userName"/* name */
	, &UserProfile_get_userName_m7329_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo UserProfile_t1624____id_PropertyInfo = 
{
	&UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &UserProfile_get_id_m7330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo UserProfile_t1624____isFriend_PropertyInfo = 
{
	&UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, "isFriend"/* name */
	, &UserProfile_get_isFriend_m7331_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo UserProfile_t1624____state_PropertyInfo = 
{
	&UserProfile_t1624_il2cpp_TypeInfo/* parent */
	, "state"/* name */
	, &UserProfile_get_state_m7332_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* UserProfile_t1624_PropertyInfos[] =
{
	&UserProfile_t1624____userName_PropertyInfo,
	&UserProfile_t1624____id_PropertyInfo,
	&UserProfile_t1624____isFriend_PropertyInfo,
	&UserProfile_t1624____state_PropertyInfo,
	NULL
};
static Il2CppMethodReference UserProfile_t1624_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UserProfile_ToString_m7325_MethodInfo,
	&UserProfile_get_id_m7330_MethodInfo,
	&UserProfile_get_userName_m7329_MethodInfo,
	&UserProfile_get_isFriend_m7331_MethodInfo,
	&UserProfile_get_state_m7332_MethodInfo,
};
static bool UserProfile_t1624_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UserProfile_t1624_InterfacesTypeInfos[] = 
{
	&IUserProfile_t983_0_0_0,
};
static Il2CppInterfaceOffsetPair UserProfile_t1624_InterfacesOffsets[] = 
{
	{ &IUserProfile_t983_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserProfile_t1624_1_0_0;
extern Il2CppType Object_t_0_0_0;
struct UserProfile_t1624;
const Il2CppTypeDefinitionMetadata UserProfile_t1624_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UserProfile_t1624_InterfacesTypeInfos/* implementedInterfaces */
	, UserProfile_t1624_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UserProfile_t1624_VTable/* vtableMethods */
	, UserProfile_t1624_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UserProfile_t1624_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, UserProfile_t1624_MethodInfos/* methods */
	, UserProfile_t1624_PropertyInfos/* properties */
	, UserProfile_t1624_FieldInfos/* fields */
	, NULL/* events */
	, &UserProfile_t1624_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserProfile_t1624_0_0_0/* byval_arg */
	, &UserProfile_t1624_1_0_0/* this_arg */
	, &UserProfile_t1624_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t1624)/* instance_size */
	, sizeof (UserProfile_t1624)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern TypeInfo Achievement_t1625_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType DateTime_t48_0_0_0;
extern Il2CppType DateTime_t48_0_0_0;
static ParameterInfo Achievement_t1625_Achievement__ctor_m7333_ParameterInfos[] = 
{
	{"id", 0, 134219349, 0, &String_t_0_0_0},
	{"percentCompleted", 1, 134219350, 0, &Double_t234_0_0_0},
	{"completed", 2, 134219351, 0, &Boolean_t203_0_0_0},
	{"hidden", 3, 134219352, 0, &Boolean_t203_0_0_0},
	{"lastReportedDate", 4, 134219353, 0, &DateTime_t48_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Double_t234_SByte_t236_SByte_t236_DateTime_t48 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
MethodInfo Achievement__ctor_m7333_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m7333/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Double_t234_SByte_t236_SByte_t236_DateTime_t48/* invoker_method */
	, Achievement_t1625_Achievement__ctor_m7333_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t234_0_0_0;
static ParameterInfo Achievement_t1625_Achievement__ctor_m7334_ParameterInfos[] = 
{
	{"id", 0, 134219354, 0, &String_t_0_0_0},
	{"percent", 1, 134219355, 0, &Double_t234_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Double_t234 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
MethodInfo Achievement__ctor_m7334_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m7334/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Double_t234/* invoker_method */
	, Achievement_t1625_Achievement__ctor_m7334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
MethodInfo Achievement__ctor_m7335_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m7335/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
MethodInfo Achievement_ToString_m7336_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Achievement_ToString_m7336/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
MethodInfo Achievement_get_id_m7337_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Achievement_get_id_m7337/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 611/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Achievement_t1625_Achievement_set_id_m7338_ParameterInfos[] = 
{
	{"value", 0, 134219356, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
MethodInfo Achievement_set_id_m7338_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Achievement_set_id_m7338/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Achievement_t1625_Achievement_set_id_m7338_ParameterInfos/* parameters */
	, 612/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Double_t234_0_0_0;
extern void* RuntimeInvoker_Double_t234 (MethodInfo* method, void* obj, void** args);
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
MethodInfo Achievement_get_percentCompleted_m7339_MethodInfo = 
{
	"get_percentCompleted"/* name */
	, (methodPointerType)&Achievement_get_percentCompleted_m7339/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Double_t234_0_0_0/* return_type */
	, RuntimeInvoker_Double_t234/* invoker_method */
	, NULL/* parameters */
	, 613/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Double_t234_0_0_0;
static ParameterInfo Achievement_t1625_Achievement_set_percentCompleted_m7340_ParameterInfos[] = 
{
	{"value", 0, 134219357, 0, &Double_t234_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Double_t234 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
MethodInfo Achievement_set_percentCompleted_m7340_MethodInfo = 
{
	"set_percentCompleted"/* name */
	, (methodPointerType)&Achievement_set_percentCompleted_m7340/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Double_t234/* invoker_method */
	, Achievement_t1625_Achievement_set_percentCompleted_m7340_ParameterInfos/* parameters */
	, 614/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
MethodInfo Achievement_get_completed_m7341_MethodInfo = 
{
	"get_completed"/* name */
	, (methodPointerType)&Achievement_get_completed_m7341/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
MethodInfo Achievement_get_hidden_m7342_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&Achievement_get_hidden_m7342/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DateTime_t48_0_0_0;
extern void* RuntimeInvoker_DateTime_t48 (MethodInfo* method, void* obj, void** args);
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
MethodInfo Achievement_get_lastReportedDate_m7343_MethodInfo = 
{
	"get_lastReportedDate"/* name */
	, (methodPointerType)&Achievement_get_lastReportedDate_m7343/* method */
	, &Achievement_t1625_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t48_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t48/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Achievement_t1625_MethodInfos[] =
{
	&Achievement__ctor_m7333_MethodInfo,
	&Achievement__ctor_m7334_MethodInfo,
	&Achievement__ctor_m7335_MethodInfo,
	&Achievement_ToString_m7336_MethodInfo,
	&Achievement_get_id_m7337_MethodInfo,
	&Achievement_set_id_m7338_MethodInfo,
	&Achievement_get_percentCompleted_m7339_MethodInfo,
	&Achievement_set_percentCompleted_m7340_MethodInfo,
	&Achievement_get_completed_m7341_MethodInfo,
	&Achievement_get_hidden_m7342_MethodInfo,
	&Achievement_get_lastReportedDate_m7343_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Achievement_t1625____m_Completed_0_FieldInfo = 
{
	"m_Completed"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Achievement_t1625_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1625, ___m_Completed_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Achievement_t1625____m_Hidden_1_FieldInfo = 
{
	"m_Hidden"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Achievement_t1625_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1625, ___m_Hidden_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DateTime_t48_0_0_1;
FieldInfo Achievement_t1625____m_LastReportedDate_2_FieldInfo = 
{
	"m_LastReportedDate"/* name */
	, &DateTime_t48_0_0_1/* type */
	, &Achievement_t1625_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1625, ___m_LastReportedDate_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Achievement_t1625____U3CidU3Ek__BackingField_3_FieldInfo = 
{
	"<id>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &Achievement_t1625_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1625, ___U3CidU3Ek__BackingField_3)/* offset */
	, 609/* custom_attributes_cache */

};
extern Il2CppType Double_t234_0_0_1;
FieldInfo Achievement_t1625____U3CpercentCompletedU3Ek__BackingField_4_FieldInfo = 
{
	"<percentCompleted>k__BackingField"/* name */
	, &Double_t234_0_0_1/* type */
	, &Achievement_t1625_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1625, ___U3CpercentCompletedU3Ek__BackingField_4)/* offset */
	, 610/* custom_attributes_cache */

};
static FieldInfo* Achievement_t1625_FieldInfos[] =
{
	&Achievement_t1625____m_Completed_0_FieldInfo,
	&Achievement_t1625____m_Hidden_1_FieldInfo,
	&Achievement_t1625____m_LastReportedDate_2_FieldInfo,
	&Achievement_t1625____U3CidU3Ek__BackingField_3_FieldInfo,
	&Achievement_t1625____U3CpercentCompletedU3Ek__BackingField_4_FieldInfo,
	NULL
};
extern MethodInfo Achievement_get_id_m7337_MethodInfo;
extern MethodInfo Achievement_set_id_m7338_MethodInfo;
static PropertyInfo Achievement_t1625____id_PropertyInfo = 
{
	&Achievement_t1625_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Achievement_get_id_m7337_MethodInfo/* get */
	, &Achievement_set_id_m7338_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Achievement_get_percentCompleted_m7339_MethodInfo;
extern MethodInfo Achievement_set_percentCompleted_m7340_MethodInfo;
static PropertyInfo Achievement_t1625____percentCompleted_PropertyInfo = 
{
	&Achievement_t1625_il2cpp_TypeInfo/* parent */
	, "percentCompleted"/* name */
	, &Achievement_get_percentCompleted_m7339_MethodInfo/* get */
	, &Achievement_set_percentCompleted_m7340_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Achievement_get_completed_m7341_MethodInfo;
static PropertyInfo Achievement_t1625____completed_PropertyInfo = 
{
	&Achievement_t1625_il2cpp_TypeInfo/* parent */
	, "completed"/* name */
	, &Achievement_get_completed_m7341_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Achievement_get_hidden_m7342_MethodInfo;
static PropertyInfo Achievement_t1625____hidden_PropertyInfo = 
{
	&Achievement_t1625_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &Achievement_get_hidden_m7342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Achievement_get_lastReportedDate_m7343_MethodInfo;
static PropertyInfo Achievement_t1625____lastReportedDate_PropertyInfo = 
{
	&Achievement_t1625_il2cpp_TypeInfo/* parent */
	, "lastReportedDate"/* name */
	, &Achievement_get_lastReportedDate_m7343_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Achievement_t1625_PropertyInfos[] =
{
	&Achievement_t1625____id_PropertyInfo,
	&Achievement_t1625____percentCompleted_PropertyInfo,
	&Achievement_t1625____completed_PropertyInfo,
	&Achievement_t1625____hidden_PropertyInfo,
	&Achievement_t1625____lastReportedDate_PropertyInfo,
	NULL
};
extern MethodInfo Achievement_ToString_m7336_MethodInfo;
static Il2CppMethodReference Achievement_t1625_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Achievement_ToString_m7336_MethodInfo,
	&Achievement_get_id_m7337_MethodInfo,
	&Achievement_get_percentCompleted_m7339_MethodInfo,
	&Achievement_get_completed_m7341_MethodInfo,
	&Achievement_set_id_m7338_MethodInfo,
	&Achievement_set_percentCompleted_m7340_MethodInfo,
	&Achievement_get_hidden_m7342_MethodInfo,
	&Achievement_get_lastReportedDate_m7343_MethodInfo,
};
static bool Achievement_t1625_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IAchievement_t856_0_0_0;
static const Il2CppType* Achievement_t1625_InterfacesTypeInfos[] = 
{
	&IAchievement_t856_0_0_0,
};
static Il2CppInterfaceOffsetPair Achievement_t1625_InterfacesOffsets[] = 
{
	{ &IAchievement_t856_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Achievement_t1625_0_0_0;
extern Il2CppType Achievement_t1625_1_0_0;
struct Achievement_t1625;
const Il2CppTypeDefinitionMetadata Achievement_t1625_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Achievement_t1625_InterfacesTypeInfos/* implementedInterfaces */
	, Achievement_t1625_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Achievement_t1625_VTable/* vtableMethods */
	, Achievement_t1625_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Achievement_t1625_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Achievement_t1625_MethodInfos/* methods */
	, Achievement_t1625_PropertyInfos/* properties */
	, Achievement_t1625_FieldInfos/* fields */
	, NULL/* events */
	, &Achievement_t1625_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Achievement_t1625_0_0_0/* byval_arg */
	, &Achievement_t1625_1_0_0/* this_arg */
	, &Achievement_t1625_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t1625)/* instance_size */
	, sizeof (Achievement_t1625)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern TypeInfo AchievementDescription_t1626_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Texture2D_t384_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo AchievementDescription_t1626_AchievementDescription__ctor_m7344_ParameterInfos[] = 
{
	{"id", 0, 134219358, 0, &String_t_0_0_0},
	{"title", 1, 134219359, 0, &String_t_0_0_0},
	{"image", 2, 134219360, 0, &Texture2D_t384_0_0_0},
	{"achievedDescription", 3, 134219361, 0, &String_t_0_0_0},
	{"unachievedDescription", 4, 134219362, 0, &String_t_0_0_0},
	{"hidden", 5, 134219363, 0, &Boolean_t203_0_0_0},
	{"points", 6, 134219364, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
MethodInfo AchievementDescription__ctor_m7344_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AchievementDescription__ctor_m7344/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t236_Int32_t189/* invoker_method */
	, AchievementDescription_t1626_AchievementDescription__ctor_m7344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
MethodInfo AchievementDescription_ToString_m7345_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AchievementDescription_ToString_m7345/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Texture2D_t384_0_0_0;
static ParameterInfo AchievementDescription_t1626_AchievementDescription_SetImage_m7346_ParameterInfos[] = 
{
	{"image", 0, 134219365, 0, &Texture2D_t384_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
MethodInfo AchievementDescription_SetImage_m7346_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&AchievementDescription_SetImage_m7346/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, AchievementDescription_t1626_AchievementDescription_SetImage_m7346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
MethodInfo AchievementDescription_get_id_m7347_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&AchievementDescription_get_id_m7347/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 616/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo AchievementDescription_t1626_AchievementDescription_set_id_m7348_ParameterInfos[] = 
{
	{"value", 0, 134219366, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
MethodInfo AchievementDescription_set_id_m7348_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&AchievementDescription_set_id_m7348/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, AchievementDescription_t1626_AchievementDescription_set_id_m7348_ParameterInfos/* parameters */
	, 617/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
MethodInfo AchievementDescription_get_title_m7349_MethodInfo = 
{
	"get_title"/* name */
	, (methodPointerType)&AchievementDescription_get_title_m7349/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
MethodInfo AchievementDescription_get_achievedDescription_m7350_MethodInfo = 
{
	"get_achievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_achievedDescription_m7350/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
MethodInfo AchievementDescription_get_unachievedDescription_m7351_MethodInfo = 
{
	"get_unachievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_unachievedDescription_m7351/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
MethodInfo AchievementDescription_get_hidden_m7352_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&AchievementDescription_get_hidden_m7352/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
MethodInfo AchievementDescription_get_points_m7353_MethodInfo = 
{
	"get_points"/* name */
	, (methodPointerType)&AchievementDescription_get_points_m7353/* method */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AchievementDescription_t1626_MethodInfos[] =
{
	&AchievementDescription__ctor_m7344_MethodInfo,
	&AchievementDescription_ToString_m7345_MethodInfo,
	&AchievementDescription_SetImage_m7346_MethodInfo,
	&AchievementDescription_get_id_m7347_MethodInfo,
	&AchievementDescription_set_id_m7348_MethodInfo,
	&AchievementDescription_get_title_m7349_MethodInfo,
	&AchievementDescription_get_achievedDescription_m7350_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m7351_MethodInfo,
	&AchievementDescription_get_hidden_m7352_MethodInfo,
	&AchievementDescription_get_points_m7353_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo AchievementDescription_t1626____m_Title_0_FieldInfo = 
{
	"m_Title"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1626, ___m_Title_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Texture2D_t384_0_0_1;
FieldInfo AchievementDescription_t1626____m_Image_1_FieldInfo = 
{
	"m_Image"/* name */
	, &Texture2D_t384_0_0_1/* type */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1626, ___m_Image_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo AchievementDescription_t1626____m_AchievedDescription_2_FieldInfo = 
{
	"m_AchievedDescription"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1626, ___m_AchievedDescription_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo AchievementDescription_t1626____m_UnachievedDescription_3_FieldInfo = 
{
	"m_UnachievedDescription"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1626, ___m_UnachievedDescription_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo AchievementDescription_t1626____m_Hidden_4_FieldInfo = 
{
	"m_Hidden"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1626, ___m_Hidden_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo AchievementDescription_t1626____m_Points_5_FieldInfo = 
{
	"m_Points"/* name */
	, &Int32_t189_0_0_1/* type */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1626, ___m_Points_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo AchievementDescription_t1626____U3CidU3Ek__BackingField_6_FieldInfo = 
{
	"<id>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1626, ___U3CidU3Ek__BackingField_6)/* offset */
	, 615/* custom_attributes_cache */

};
static FieldInfo* AchievementDescription_t1626_FieldInfos[] =
{
	&AchievementDescription_t1626____m_Title_0_FieldInfo,
	&AchievementDescription_t1626____m_Image_1_FieldInfo,
	&AchievementDescription_t1626____m_AchievedDescription_2_FieldInfo,
	&AchievementDescription_t1626____m_UnachievedDescription_3_FieldInfo,
	&AchievementDescription_t1626____m_Hidden_4_FieldInfo,
	&AchievementDescription_t1626____m_Points_5_FieldInfo,
	&AchievementDescription_t1626____U3CidU3Ek__BackingField_6_FieldInfo,
	NULL
};
extern MethodInfo AchievementDescription_get_id_m7347_MethodInfo;
extern MethodInfo AchievementDescription_set_id_m7348_MethodInfo;
static PropertyInfo AchievementDescription_t1626____id_PropertyInfo = 
{
	&AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &AchievementDescription_get_id_m7347_MethodInfo/* get */
	, &AchievementDescription_set_id_m7348_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AchievementDescription_get_title_m7349_MethodInfo;
static PropertyInfo AchievementDescription_t1626____title_PropertyInfo = 
{
	&AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, "title"/* name */
	, &AchievementDescription_get_title_m7349_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AchievementDescription_get_achievedDescription_m7350_MethodInfo;
static PropertyInfo AchievementDescription_t1626____achievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, "achievedDescription"/* name */
	, &AchievementDescription_get_achievedDescription_m7350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AchievementDescription_get_unachievedDescription_m7351_MethodInfo;
static PropertyInfo AchievementDescription_t1626____unachievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, "unachievedDescription"/* name */
	, &AchievementDescription_get_unachievedDescription_m7351_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AchievementDescription_get_hidden_m7352_MethodInfo;
static PropertyInfo AchievementDescription_t1626____hidden_PropertyInfo = 
{
	&AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &AchievementDescription_get_hidden_m7352_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AchievementDescription_get_points_m7353_MethodInfo;
static PropertyInfo AchievementDescription_t1626____points_PropertyInfo = 
{
	&AchievementDescription_t1626_il2cpp_TypeInfo/* parent */
	, "points"/* name */
	, &AchievementDescription_get_points_m7353_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* AchievementDescription_t1626_PropertyInfos[] =
{
	&AchievementDescription_t1626____id_PropertyInfo,
	&AchievementDescription_t1626____title_PropertyInfo,
	&AchievementDescription_t1626____achievedDescription_PropertyInfo,
	&AchievementDescription_t1626____unachievedDescription_PropertyInfo,
	&AchievementDescription_t1626____hidden_PropertyInfo,
	&AchievementDescription_t1626____points_PropertyInfo,
	NULL
};
extern MethodInfo AchievementDescription_ToString_m7345_MethodInfo;
static Il2CppMethodReference AchievementDescription_t1626_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&AchievementDescription_ToString_m7345_MethodInfo,
	&AchievementDescription_get_id_m7347_MethodInfo,
	&AchievementDescription_set_id_m7348_MethodInfo,
	&AchievementDescription_get_title_m7349_MethodInfo,
	&AchievementDescription_get_achievedDescription_m7350_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m7351_MethodInfo,
	&AchievementDescription_get_hidden_m7352_MethodInfo,
	&AchievementDescription_get_points_m7353_MethodInfo,
};
static bool AchievementDescription_t1626_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IAchievementDescription_t1707_0_0_0;
static const Il2CppType* AchievementDescription_t1626_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t1707_0_0_0,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t1626_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t1707_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType AchievementDescription_t1626_0_0_0;
extern Il2CppType AchievementDescription_t1626_1_0_0;
struct AchievementDescription_t1626;
const Il2CppTypeDefinitionMetadata AchievementDescription_t1626_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AchievementDescription_t1626_InterfacesTypeInfos/* implementedInterfaces */
	, AchievementDescription_t1626_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AchievementDescription_t1626_VTable/* vtableMethods */
	, AchievementDescription_t1626_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo AchievementDescription_t1626_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementDescription_t1626_MethodInfos/* methods */
	, AchievementDescription_t1626_PropertyInfos/* properties */
	, AchievementDescription_t1626_FieldInfos/* fields */
	, NULL/* events */
	, &AchievementDescription_t1626_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AchievementDescription_t1626_0_0_0/* byval_arg */
	, &AchievementDescription_t1626_1_0_0/* this_arg */
	, &AchievementDescription_t1626_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t1626)/* instance_size */
	, sizeof (AchievementDescription_t1626)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern TypeInfo Score_t1627_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
static ParameterInfo Score_t1627_Score__ctor_m7354_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219367, 0, &String_t_0_0_0},
	{"value", 1, 134219368, 0, &Int64_t233_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64_t233 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
MethodInfo Score__ctor_m7354_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m7354/* method */
	, &Score_t1627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64_t233/* invoker_method */
	, Score_t1627_Score__ctor_m7354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType DateTime_t48_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Score_t1627_Score__ctor_m7355_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219369, 0, &String_t_0_0_0},
	{"value", 1, 134219370, 0, &Int64_t233_0_0_0},
	{"userID", 2, 134219371, 0, &String_t_0_0_0},
	{"date", 3, 134219372, 0, &DateTime_t48_0_0_0},
	{"formattedValue", 4, 134219373, 0, &String_t_0_0_0},
	{"rank", 5, 134219374, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64_t233_Object_t_DateTime_t48_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
MethodInfo Score__ctor_m7355_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m7355/* method */
	, &Score_t1627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64_t233_Object_t_DateTime_t48_Object_t_Int32_t189/* invoker_method */
	, Score_t1627_Score__ctor_m7355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
MethodInfo Score_ToString_m7356_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Score_ToString_m7356/* method */
	, &Score_t1627_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
MethodInfo Score_get_leaderboardID_m7357_MethodInfo = 
{
	"get_leaderboardID"/* name */
	, (methodPointerType)&Score_get_leaderboardID_m7357/* method */
	, &Score_t1627_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 620/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Score_t1627_Score_set_leaderboardID_m7358_ParameterInfos[] = 
{
	{"value", 0, 134219375, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
MethodInfo Score_set_leaderboardID_m7358_MethodInfo = 
{
	"set_leaderboardID"/* name */
	, (methodPointerType)&Score_set_leaderboardID_m7358/* method */
	, &Score_t1627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Score_t1627_Score_set_leaderboardID_m7358_ParameterInfos/* parameters */
	, 621/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t233_0_0_0;
extern void* RuntimeInvoker_Int64_t233 (MethodInfo* method, void* obj, void** args);
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
MethodInfo Score_get_value_m7359_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Score_get_value_m7359/* method */
	, &Score_t1627_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t233_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t233/* invoker_method */
	, NULL/* parameters */
	, 622/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t233_0_0_0;
static ParameterInfo Score_t1627_Score_set_value_m7360_ParameterInfos[] = 
{
	{"value", 0, 134219376, 0, &Int64_t233_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int64_t233 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
MethodInfo Score_set_value_m7360_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Score_set_value_m7360/* method */
	, &Score_t1627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int64_t233/* invoker_method */
	, Score_t1627_Score_set_value_m7360_ParameterInfos/* parameters */
	, 623/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Score_t1627_MethodInfos[] =
{
	&Score__ctor_m7354_MethodInfo,
	&Score__ctor_m7355_MethodInfo,
	&Score_ToString_m7356_MethodInfo,
	&Score_get_leaderboardID_m7357_MethodInfo,
	&Score_set_leaderboardID_m7358_MethodInfo,
	&Score_get_value_m7359_MethodInfo,
	&Score_set_value_m7360_MethodInfo,
	NULL
};
extern Il2CppType DateTime_t48_0_0_1;
FieldInfo Score_t1627____m_Date_0_FieldInfo = 
{
	"m_Date"/* name */
	, &DateTime_t48_0_0_1/* type */
	, &Score_t1627_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1627, ___m_Date_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Score_t1627____m_FormattedValue_1_FieldInfo = 
{
	"m_FormattedValue"/* name */
	, &String_t_0_0_1/* type */
	, &Score_t1627_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1627, ___m_FormattedValue_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Score_t1627____m_UserID_2_FieldInfo = 
{
	"m_UserID"/* name */
	, &String_t_0_0_1/* type */
	, &Score_t1627_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1627, ___m_UserID_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Score_t1627____m_Rank_3_FieldInfo = 
{
	"m_Rank"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Score_t1627_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1627, ___m_Rank_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Score_t1627____U3CleaderboardIDU3Ek__BackingField_4_FieldInfo = 
{
	"<leaderboardID>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &Score_t1627_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1627, ___U3CleaderboardIDU3Ek__BackingField_4)/* offset */
	, 618/* custom_attributes_cache */

};
extern Il2CppType Int64_t233_0_0_1;
FieldInfo Score_t1627____U3CvalueU3Ek__BackingField_5_FieldInfo = 
{
	"<value>k__BackingField"/* name */
	, &Int64_t233_0_0_1/* type */
	, &Score_t1627_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1627, ___U3CvalueU3Ek__BackingField_5)/* offset */
	, 619/* custom_attributes_cache */

};
static FieldInfo* Score_t1627_FieldInfos[] =
{
	&Score_t1627____m_Date_0_FieldInfo,
	&Score_t1627____m_FormattedValue_1_FieldInfo,
	&Score_t1627____m_UserID_2_FieldInfo,
	&Score_t1627____m_Rank_3_FieldInfo,
	&Score_t1627____U3CleaderboardIDU3Ek__BackingField_4_FieldInfo,
	&Score_t1627____U3CvalueU3Ek__BackingField_5_FieldInfo,
	NULL
};
extern MethodInfo Score_get_leaderboardID_m7357_MethodInfo;
extern MethodInfo Score_set_leaderboardID_m7358_MethodInfo;
static PropertyInfo Score_t1627____leaderboardID_PropertyInfo = 
{
	&Score_t1627_il2cpp_TypeInfo/* parent */
	, "leaderboardID"/* name */
	, &Score_get_leaderboardID_m7357_MethodInfo/* get */
	, &Score_set_leaderboardID_m7358_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Score_get_value_m7359_MethodInfo;
extern MethodInfo Score_set_value_m7360_MethodInfo;
static PropertyInfo Score_t1627____value_PropertyInfo = 
{
	&Score_t1627_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Score_get_value_m7359_MethodInfo/* get */
	, &Score_set_value_m7360_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Score_t1627_PropertyInfos[] =
{
	&Score_t1627____leaderboardID_PropertyInfo,
	&Score_t1627____value_PropertyInfo,
	NULL
};
extern MethodInfo Score_ToString_m7356_MethodInfo;
static Il2CppMethodReference Score_t1627_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Score_ToString_m7356_MethodInfo,
	&Score_get_leaderboardID_m7357_MethodInfo,
	&Score_set_leaderboardID_m7358_MethodInfo,
	&Score_get_value_m7359_MethodInfo,
	&Score_set_value_m7360_MethodInfo,
};
static bool Score_t1627_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IScore_t1022_0_0_0;
static const Il2CppType* Score_t1627_InterfacesTypeInfos[] = 
{
	&IScore_t1022_0_0_0,
};
static Il2CppInterfaceOffsetPair Score_t1627_InterfacesOffsets[] = 
{
	{ &IScore_t1022_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Score_t1627_0_0_0;
extern Il2CppType Score_t1627_1_0_0;
struct Score_t1627;
const Il2CppTypeDefinitionMetadata Score_t1627_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Score_t1627_InterfacesTypeInfos/* implementedInterfaces */
	, Score_t1627_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Score_t1627_VTable/* vtableMethods */
	, Score_t1627_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Score_t1627_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Score_t1627_MethodInfos/* methods */
	, Score_t1627_PropertyInfos/* properties */
	, Score_t1627_FieldInfos/* fields */
	, NULL/* events */
	, &Score_t1627_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Score_t1627_0_0_0/* byval_arg */
	, &Score_t1627_1_0_0/* this_arg */
	, &Score_t1627_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t1627)/* instance_size */
	, sizeof (Score_t1627)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern TypeInfo Leaderboard_t1500_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
MethodInfo Leaderboard__ctor_m7361_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Leaderboard__ctor_m7361/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
MethodInfo Leaderboard_ToString_m7362_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Leaderboard_ToString_m7362/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IScore_t1022_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_SetLocalUserScore_m7363_ParameterInfos[] = 
{
	{"score", 0, 134219377, 0, &IScore_t1022_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
MethodInfo Leaderboard_SetLocalUserScore_m7363_MethodInfo = 
{
	"SetLocalUserScore"/* name */
	, (methodPointerType)&Leaderboard_SetLocalUserScore_m7363/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Leaderboard_t1500_Leaderboard_SetLocalUserScore_m7363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UInt32_t235_0_0_0;
extern Il2CppType UInt32_t235_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_SetMaxRange_m7364_ParameterInfos[] = 
{
	{"maxRange", 0, 134219378, 0, &UInt32_t235_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
MethodInfo Leaderboard_SetMaxRange_m7364_MethodInfo = 
{
	"SetMaxRange"/* name */
	, (methodPointerType)&Leaderboard_SetMaxRange_m7364/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Leaderboard_t1500_Leaderboard_SetMaxRange_m7364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IScoreU5BU5D_t907_0_0_0;
extern Il2CppType IScoreU5BU5D_t907_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_SetScores_m7365_ParameterInfos[] = 
{
	{"scores", 0, 134219379, 0, &IScoreU5BU5D_t907_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
MethodInfo Leaderboard_SetScores_m7365_MethodInfo = 
{
	"SetScores"/* name */
	, (methodPointerType)&Leaderboard_SetScores_m7365/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Leaderboard_t1500_Leaderboard_SetScores_m7365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_SetTitle_m7366_ParameterInfos[] = 
{
	{"title", 0, 134219380, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
MethodInfo Leaderboard_SetTitle_m7366_MethodInfo = 
{
	"SetTitle"/* name */
	, (methodPointerType)&Leaderboard_SetTitle_m7366/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Leaderboard_t1500_Leaderboard_SetTitle_m7366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StringU5BU5D_t169_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
MethodInfo Leaderboard_GetUserFilter_m7367_MethodInfo = 
{
	"GetUserFilter"/* name */
	, (methodPointerType)&Leaderboard_GetUserFilter_m7367/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
MethodInfo Leaderboard_get_id_m7368_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Leaderboard_get_id_m7368/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 628/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_set_id_m7369_ParameterInfos[] = 
{
	{"value", 0, 134219381, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
MethodInfo Leaderboard_set_id_m7369_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Leaderboard_set_id_m7369/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Leaderboard_t1500_Leaderboard_set_id_m7369_ParameterInfos/* parameters */
	, 629/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserScope_t1635_0_0_0;
extern void* RuntimeInvoker_UserScope_t1635 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
MethodInfo Leaderboard_get_userScope_m7370_MethodInfo = 
{
	"get_userScope"/* name */
	, (methodPointerType)&Leaderboard_get_userScope_m7370/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1635_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1635/* invoker_method */
	, NULL/* parameters */
	, 630/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserScope_t1635_0_0_0;
extern Il2CppType UserScope_t1635_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_set_userScope_m7371_ParameterInfos[] = 
{
	{"value", 0, 134219382, 0, &UserScope_t1635_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
MethodInfo Leaderboard_set_userScope_m7371_MethodInfo = 
{
	"set_userScope"/* name */
	, (methodPointerType)&Leaderboard_set_userScope_m7371/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Leaderboard_t1500_Leaderboard_set_userScope_m7371_ParameterInfos/* parameters */
	, 631/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Range_t1628_0_0_0;
extern void* RuntimeInvoker_Range_t1628 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
MethodInfo Leaderboard_get_range_m7372_MethodInfo = 
{
	"get_range"/* name */
	, (methodPointerType)&Leaderboard_get_range_m7372/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1628_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1628/* invoker_method */
	, NULL/* parameters */
	, 632/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Range_t1628_0_0_0;
extern Il2CppType Range_t1628_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_set_range_m7373_ParameterInfos[] = 
{
	{"value", 0, 134219383, 0, &Range_t1628_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Range_t1628 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
MethodInfo Leaderboard_set_range_m7373_MethodInfo = 
{
	"set_range"/* name */
	, (methodPointerType)&Leaderboard_set_range_m7373/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Range_t1628/* invoker_method */
	, Leaderboard_t1500_Leaderboard_set_range_m7373_ParameterInfos/* parameters */
	, 633/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TimeScope_t1636_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1636 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
MethodInfo Leaderboard_get_timeScope_m7374_MethodInfo = 
{
	"get_timeScope"/* name */
	, (methodPointerType)&Leaderboard_get_timeScope_m7374/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1636_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1636/* invoker_method */
	, NULL/* parameters */
	, 634/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TimeScope_t1636_0_0_0;
extern Il2CppType TimeScope_t1636_0_0_0;
static ParameterInfo Leaderboard_t1500_Leaderboard_set_timeScope_m7375_ParameterInfos[] = 
{
	{"value", 0, 134219384, 0, &TimeScope_t1636_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
MethodInfo Leaderboard_set_timeScope_m7375_MethodInfo = 
{
	"set_timeScope"/* name */
	, (methodPointerType)&Leaderboard_set_timeScope_m7375/* method */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Leaderboard_t1500_Leaderboard_set_timeScope_m7375_ParameterInfos/* parameters */
	, 635/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Leaderboard_t1500_MethodInfos[] =
{
	&Leaderboard__ctor_m7361_MethodInfo,
	&Leaderboard_ToString_m7362_MethodInfo,
	&Leaderboard_SetLocalUserScore_m7363_MethodInfo,
	&Leaderboard_SetMaxRange_m7364_MethodInfo,
	&Leaderboard_SetScores_m7365_MethodInfo,
	&Leaderboard_SetTitle_m7366_MethodInfo,
	&Leaderboard_GetUserFilter_m7367_MethodInfo,
	&Leaderboard_get_id_m7368_MethodInfo,
	&Leaderboard_set_id_m7369_MethodInfo,
	&Leaderboard_get_userScope_m7370_MethodInfo,
	&Leaderboard_set_userScope_m7371_MethodInfo,
	&Leaderboard_get_range_m7372_MethodInfo,
	&Leaderboard_set_range_m7373_MethodInfo,
	&Leaderboard_get_timeScope_m7374_MethodInfo,
	&Leaderboard_set_timeScope_m7375_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Leaderboard_t1500____m_Loading_0_FieldInfo = 
{
	"m_Loading"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___m_Loading_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IScore_t1022_0_0_1;
FieldInfo Leaderboard_t1500____m_LocalUserScore_1_FieldInfo = 
{
	"m_LocalUserScore"/* name */
	, &IScore_t1022_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___m_LocalUserScore_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UInt32_t235_0_0_1;
FieldInfo Leaderboard_t1500____m_MaxRange_2_FieldInfo = 
{
	"m_MaxRange"/* name */
	, &UInt32_t235_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___m_MaxRange_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IScoreU5BU5D_t907_0_0_1;
FieldInfo Leaderboard_t1500____m_Scores_3_FieldInfo = 
{
	"m_Scores"/* name */
	, &IScoreU5BU5D_t907_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___m_Scores_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Leaderboard_t1500____m_Title_4_FieldInfo = 
{
	"m_Title"/* name */
	, &String_t_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___m_Title_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_1;
FieldInfo Leaderboard_t1500____m_UserIDs_5_FieldInfo = 
{
	"m_UserIDs"/* name */
	, &StringU5BU5D_t169_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___m_UserIDs_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Leaderboard_t1500____U3CidU3Ek__BackingField_6_FieldInfo = 
{
	"<id>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___U3CidU3Ek__BackingField_6)/* offset */
	, 624/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1635_0_0_1;
FieldInfo Leaderboard_t1500____U3CuserScopeU3Ek__BackingField_7_FieldInfo = 
{
	"<userScope>k__BackingField"/* name */
	, &UserScope_t1635_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___U3CuserScopeU3Ek__BackingField_7)/* offset */
	, 625/* custom_attributes_cache */

};
extern Il2CppType Range_t1628_0_0_1;
FieldInfo Leaderboard_t1500____U3CrangeU3Ek__BackingField_8_FieldInfo = 
{
	"<range>k__BackingField"/* name */
	, &Range_t1628_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___U3CrangeU3Ek__BackingField_8)/* offset */
	, 626/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1636_0_0_1;
FieldInfo Leaderboard_t1500____U3CtimeScopeU3Ek__BackingField_9_FieldInfo = 
{
	"<timeScope>k__BackingField"/* name */
	, &TimeScope_t1636_0_0_1/* type */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t1500, ___U3CtimeScopeU3Ek__BackingField_9)/* offset */
	, 627/* custom_attributes_cache */

};
static FieldInfo* Leaderboard_t1500_FieldInfos[] =
{
	&Leaderboard_t1500____m_Loading_0_FieldInfo,
	&Leaderboard_t1500____m_LocalUserScore_1_FieldInfo,
	&Leaderboard_t1500____m_MaxRange_2_FieldInfo,
	&Leaderboard_t1500____m_Scores_3_FieldInfo,
	&Leaderboard_t1500____m_Title_4_FieldInfo,
	&Leaderboard_t1500____m_UserIDs_5_FieldInfo,
	&Leaderboard_t1500____U3CidU3Ek__BackingField_6_FieldInfo,
	&Leaderboard_t1500____U3CuserScopeU3Ek__BackingField_7_FieldInfo,
	&Leaderboard_t1500____U3CrangeU3Ek__BackingField_8_FieldInfo,
	&Leaderboard_t1500____U3CtimeScopeU3Ek__BackingField_9_FieldInfo,
	NULL
};
extern MethodInfo Leaderboard_get_id_m7368_MethodInfo;
extern MethodInfo Leaderboard_set_id_m7369_MethodInfo;
static PropertyInfo Leaderboard_t1500____id_PropertyInfo = 
{
	&Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Leaderboard_get_id_m7368_MethodInfo/* get */
	, &Leaderboard_set_id_m7369_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Leaderboard_get_userScope_m7370_MethodInfo;
extern MethodInfo Leaderboard_set_userScope_m7371_MethodInfo;
static PropertyInfo Leaderboard_t1500____userScope_PropertyInfo = 
{
	&Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &Leaderboard_get_userScope_m7370_MethodInfo/* get */
	, &Leaderboard_set_userScope_m7371_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Leaderboard_get_range_m7372_MethodInfo;
extern MethodInfo Leaderboard_set_range_m7373_MethodInfo;
static PropertyInfo Leaderboard_t1500____range_PropertyInfo = 
{
	&Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &Leaderboard_get_range_m7372_MethodInfo/* get */
	, &Leaderboard_set_range_m7373_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Leaderboard_get_timeScope_m7374_MethodInfo;
extern MethodInfo Leaderboard_set_timeScope_m7375_MethodInfo;
static PropertyInfo Leaderboard_t1500____timeScope_PropertyInfo = 
{
	&Leaderboard_t1500_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &Leaderboard_get_timeScope_m7374_MethodInfo/* get */
	, &Leaderboard_set_timeScope_m7375_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Leaderboard_t1500_PropertyInfos[] =
{
	&Leaderboard_t1500____id_PropertyInfo,
	&Leaderboard_t1500____userScope_PropertyInfo,
	&Leaderboard_t1500____range_PropertyInfo,
	&Leaderboard_t1500____timeScope_PropertyInfo,
	NULL
};
extern MethodInfo Leaderboard_ToString_m7362_MethodInfo;
static Il2CppMethodReference Leaderboard_t1500_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Leaderboard_ToString_m7362_MethodInfo,
	&Leaderboard_get_id_m7368_MethodInfo,
	&Leaderboard_get_userScope_m7370_MethodInfo,
	&Leaderboard_get_range_m7372_MethodInfo,
	&Leaderboard_get_timeScope_m7374_MethodInfo,
	&Leaderboard_set_id_m7369_MethodInfo,
	&Leaderboard_set_userScope_m7371_MethodInfo,
	&Leaderboard_set_range_m7373_MethodInfo,
	&Leaderboard_set_timeScope_m7375_MethodInfo,
};
static bool Leaderboard_t1500_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ILeaderboard_t858_0_0_0;
static const Il2CppType* Leaderboard_t1500_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t858_0_0_0,
};
static Il2CppInterfaceOffsetPair Leaderboard_t1500_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t858_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Leaderboard_t1500_0_0_0;
extern Il2CppType Leaderboard_t1500_1_0_0;
struct Leaderboard_t1500;
const Il2CppTypeDefinitionMetadata Leaderboard_t1500_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Leaderboard_t1500_InterfacesTypeInfos/* implementedInterfaces */
	, Leaderboard_t1500_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Leaderboard_t1500_VTable/* vtableMethods */
	, Leaderboard_t1500_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Leaderboard_t1500_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Leaderboard_t1500_MethodInfos/* methods */
	, Leaderboard_t1500_PropertyInfos/* properties */
	, Leaderboard_t1500_FieldInfos/* fields */
	, NULL/* events */
	, &Leaderboard_t1500_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Leaderboard_t1500_0_0_0/* byval_arg */
	, &Leaderboard_t1500_1_0_0/* this_arg */
	, &Leaderboard_t1500_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t1500)/* instance_size */
	, sizeof (Leaderboard_t1500)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern TypeInfo HitInfo_t1629_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo HitInfo_t1629_HitInfo_SendMessage_m7376_ParameterInfos[] = 
{
	{"name", 0, 134219389, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
MethodInfo HitInfo_SendMessage_m7376_MethodInfo = 
{
	"SendMessage"/* name */
	, (methodPointerType)&HitInfo_SendMessage_m7376/* method */
	, &HitInfo_t1629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, HitInfo_t1629_HitInfo_SendMessage_m7376_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HitInfo_t1629_0_0_0;
extern Il2CppType HitInfo_t1629_0_0_0;
extern Il2CppType HitInfo_t1629_0_0_0;
static ParameterInfo HitInfo_t1629_HitInfo_Compare_m7377_ParameterInfos[] = 
{
	{"lhs", 0, 134219390, 0, &HitInfo_t1629_0_0_0},
	{"rhs", 1, 134219391, 0, &HitInfo_t1629_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_HitInfo_t1629_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
MethodInfo HitInfo_Compare_m7377_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&HitInfo_Compare_m7377/* method */
	, &HitInfo_t1629_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_HitInfo_t1629_HitInfo_t1629/* invoker_method */
	, HitInfo_t1629_HitInfo_Compare_m7377_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HitInfo_t1629_0_0_0;
static ParameterInfo HitInfo_t1629_HitInfo_op_Implicit_m7378_ParameterInfos[] = 
{
	{"exists", 0, 134219392, 0, &HitInfo_t1629_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
MethodInfo HitInfo_op_Implicit_m7378_MethodInfo = 
{
	"op_Implicit"/* name */
	, (methodPointerType)&HitInfo_op_Implicit_m7378/* method */
	, &HitInfo_t1629_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_HitInfo_t1629/* invoker_method */
	, HitInfo_t1629_HitInfo_op_Implicit_m7378_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* HitInfo_t1629_MethodInfos[] =
{
	&HitInfo_SendMessage_m7376_MethodInfo,
	&HitInfo_Compare_m7377_MethodInfo,
	&HitInfo_op_Implicit_m7378_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo HitInfo_t1629____target_0_FieldInfo = 
{
	"target"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &HitInfo_t1629_il2cpp_TypeInfo/* parent */
	, offsetof(HitInfo_t1629, ___target_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Camera_t978_0_0_6;
FieldInfo HitInfo_t1629____camera_1_FieldInfo = 
{
	"camera"/* name */
	, &Camera_t978_0_0_6/* type */
	, &HitInfo_t1629_il2cpp_TypeInfo/* parent */
	, offsetof(HitInfo_t1629, ___camera_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* HitInfo_t1629_FieldInfos[] =
{
	&HitInfo_t1629____target_0_FieldInfo,
	&HitInfo_t1629____camera_1_FieldInfo,
	NULL
};
static Il2CppMethodReference HitInfo_t1629_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool HitInfo_t1629_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType HitInfo_t1629_1_0_0;
extern TypeInfo SendMouseEvents_t1632_il2cpp_TypeInfo;
extern Il2CppType SendMouseEvents_t1632_0_0_0;
const Il2CppTypeDefinitionMetadata HitInfo_t1629_DefinitionMetadata = 
{
	&SendMouseEvents_t1632_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, HitInfo_t1629_VTable/* vtableMethods */
	, HitInfo_t1629_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo HitInfo_t1629_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, HitInfo_t1629_MethodInfos/* methods */
	, NULL/* properties */
	, HitInfo_t1629_FieldInfos/* fields */
	, NULL/* events */
	, &HitInfo_t1629_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HitInfo_t1629_0_0_0/* byval_arg */
	, &HitInfo_t1629_1_0_0/* this_arg */
	, &HitInfo_t1629_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t1629)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HitInfo_t1629)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// Metadata Definition UnityEngine.SendMouseEvents
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::.cctor()
MethodInfo SendMouseEvents__cctor_m7379_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SendMouseEvents__cctor_m7379/* method */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo SendMouseEvents_t1632_SendMouseEvents_DoSendMouseEvents_m7380_ParameterInfos[] = 
{
	{"mouseUsed", 0, 134219385, 0, &Int32_t189_0_0_0},
	{"skipRTCameras", 1, 134219386, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
MethodInfo SendMouseEvents_DoSendMouseEvents_m7380_MethodInfo = 
{
	"DoSendMouseEvents"/* name */
	, (methodPointerType)&SendMouseEvents_DoSendMouseEvents_m7380/* method */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189/* invoker_method */
	, SendMouseEvents_t1632_SendMouseEvents_DoSendMouseEvents_m7380_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType HitInfo_t1629_0_0_0;
static ParameterInfo SendMouseEvents_t1632_SendMouseEvents_SendEvents_m7381_ParameterInfos[] = 
{
	{"i", 0, 134219387, 0, &Int32_t189_0_0_0},
	{"hit", 1, 134219388, 0, &HitInfo_t1629_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
MethodInfo SendMouseEvents_SendEvents_m7381_MethodInfo = 
{
	"SendEvents"/* name */
	, (methodPointerType)&SendMouseEvents_SendEvents_m7381/* method */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_HitInfo_t1629/* invoker_method */
	, SendMouseEvents_t1632_SendMouseEvents_SendEvents_m7381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SendMouseEvents_t1632_MethodInfos[] =
{
	&SendMouseEvents__cctor_m7379_MethodInfo,
	&SendMouseEvents_DoSendMouseEvents_m7380_MethodInfo,
	&SendMouseEvents_SendEvents_m7381_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_32849;
FieldInfo SendMouseEvents_t1632____m_HitIndexGUI_0_FieldInfo = 
{
	"m_HitIndexGUI"/* name */
	, &Int32_t189_0_0_32849/* type */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_32849;
FieldInfo SendMouseEvents_t1632____m_HitIndexPhysics3D_1_FieldInfo = 
{
	"m_HitIndexPhysics3D"/* name */
	, &Int32_t189_0_0_32849/* type */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_32849;
FieldInfo SendMouseEvents_t1632____m_HitIndexPhysics2D_2_FieldInfo = 
{
	"m_HitIndexPhysics2D"/* name */
	, &Int32_t189_0_0_32849/* type */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType HitInfoU5BU5D_t1630_0_0_49;
FieldInfo SendMouseEvents_t1632____m_LastHit_3_FieldInfo = 
{
	"m_LastHit"/* name */
	, &HitInfoU5BU5D_t1630_0_0_49/* type */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1632_StaticFields, ___m_LastHit_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType HitInfoU5BU5D_t1630_0_0_49;
FieldInfo SendMouseEvents_t1632____m_MouseDownHit_4_FieldInfo = 
{
	"m_MouseDownHit"/* name */
	, &HitInfoU5BU5D_t1630_0_0_49/* type */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1632_StaticFields, ___m_MouseDownHit_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType HitInfoU5BU5D_t1630_0_0_49;
FieldInfo SendMouseEvents_t1632____m_CurrentHit_5_FieldInfo = 
{
	"m_CurrentHit"/* name */
	, &HitInfoU5BU5D_t1630_0_0_49/* type */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1632_StaticFields, ___m_CurrentHit_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CameraU5BU5D_t1631_0_0_17;
FieldInfo SendMouseEvents_t1632____m_Cameras_6_FieldInfo = 
{
	"m_Cameras"/* name */
	, &CameraU5BU5D_t1631_0_0_17/* type */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1632_StaticFields, ___m_Cameras_6)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* SendMouseEvents_t1632_FieldInfos[] =
{
	&SendMouseEvents_t1632____m_HitIndexGUI_0_FieldInfo,
	&SendMouseEvents_t1632____m_HitIndexPhysics3D_1_FieldInfo,
	&SendMouseEvents_t1632____m_HitIndexPhysics2D_2_FieldInfo,
	&SendMouseEvents_t1632____m_LastHit_3_FieldInfo,
	&SendMouseEvents_t1632____m_MouseDownHit_4_FieldInfo,
	&SendMouseEvents_t1632____m_CurrentHit_5_FieldInfo,
	&SendMouseEvents_t1632____m_Cameras_6_FieldInfo,
	NULL
};
static const int32_t SendMouseEvents_t1632____m_HitIndexGUI_0_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry SendMouseEvents_t1632____m_HitIndexGUI_0_DefaultValue = 
{
	&SendMouseEvents_t1632____m_HitIndexGUI_0_FieldInfo/* field */
	, { (char*)&SendMouseEvents_t1632____m_HitIndexGUI_0_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t SendMouseEvents_t1632____m_HitIndexPhysics3D_1_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry SendMouseEvents_t1632____m_HitIndexPhysics3D_1_DefaultValue = 
{
	&SendMouseEvents_t1632____m_HitIndexPhysics3D_1_FieldInfo/* field */
	, { (char*)&SendMouseEvents_t1632____m_HitIndexPhysics3D_1_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t SendMouseEvents_t1632____m_HitIndexPhysics2D_2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry SendMouseEvents_t1632____m_HitIndexPhysics2D_2_DefaultValue = 
{
	&SendMouseEvents_t1632____m_HitIndexPhysics2D_2_FieldInfo/* field */
	, { (char*)&SendMouseEvents_t1632____m_HitIndexPhysics2D_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* SendMouseEvents_t1632_FieldDefaultValues[] = 
{
	&SendMouseEvents_t1632____m_HitIndexGUI_0_DefaultValue,
	&SendMouseEvents_t1632____m_HitIndexPhysics3D_1_DefaultValue,
	&SendMouseEvents_t1632____m_HitIndexPhysics2D_2_DefaultValue,
	NULL
};
static const Il2CppType* SendMouseEvents_t1632_il2cpp_TypeInfo__nestedTypes[1] =
{
	&HitInfo_t1629_0_0_0,
};
extern MethodInfo Object_ToString_m1179_MethodInfo;
static Il2CppMethodReference SendMouseEvents_t1632_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool SendMouseEvents_t1632_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SendMouseEvents_t1632_1_0_0;
struct SendMouseEvents_t1632;
const Il2CppTypeDefinitionMetadata SendMouseEvents_t1632_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SendMouseEvents_t1632_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendMouseEvents_t1632_VTable/* vtableMethods */
	, SendMouseEvents_t1632_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SendMouseEvents_t1632_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, SendMouseEvents_t1632_MethodInfos/* methods */
	, NULL/* properties */
	, SendMouseEvents_t1632_FieldInfos/* fields */
	, NULL/* events */
	, &SendMouseEvents_t1632_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMouseEvents_t1632_0_0_0/* byval_arg */
	, &SendMouseEvents_t1632_1_0_0/* this_arg */
	, &SendMouseEvents_t1632_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, SendMouseEvents_t1632_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t1632)/* instance_size */
	, sizeof (SendMouseEvents_t1632)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t1632_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Social
#include "UnityEngine_UnityEngine_Social.h"
// Metadata Definition UnityEngine.Social
extern TypeInfo Social_t1633_il2cpp_TypeInfo;
// UnityEngine.Social
#include "UnityEngine_UnityEngine_SocialMethodDeclarations.h"
extern Il2CppType ISocialPlatform_t1021_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.Social::get_Active()
MethodInfo Social_get_Active_m3724_MethodInfo = 
{
	"get_Active"/* name */
	, (methodPointerType)&Social_get_Active_m3724/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &ISocialPlatform_t1021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ISocialPlatform_t1021_0_0_0;
extern Il2CppType ISocialPlatform_t1021_0_0_0;
static ParameterInfo Social_t1633_Social_set_Active_m3723_ParameterInfos[] = 
{
	{"value", 0, 134219393, 0, &ISocialPlatform_t1021_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Social::set_Active(UnityEngine.SocialPlatforms.ISocialPlatform)
MethodInfo Social_set_Active_m3723_MethodInfo = 
{
	"set_Active"/* name */
	, (methodPointerType)&Social_set_Active_m3723/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Social_t1633_Social_set_Active_m3723_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILocalUser_t851_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.Social::get_localUser()
MethodInfo Social_get_localUser_m4052_MethodInfo = 
{
	"get_localUser"/* name */
	, (methodPointerType)&Social_get_localUser_m4052/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &ILocalUser_t851_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo Social_t1633_Social_ReportProgress_m4056_ParameterInfos[] = 
{
	{"achievementID", 0, 134219394, 0, &String_t_0_0_0},
	{"progress", 1, 134219395, 0, &Double_t234_0_0_0},
	{"callback", 2, 134219396, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Double_t234_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Social::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
MethodInfo Social_ReportProgress_m4056_MethodInfo = 
{
	"ReportProgress"/* name */
	, (methodPointerType)&Social_ReportProgress_m4056/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Double_t234_Object_t/* invoker_method */
	, Social_t1633_Social_ReportProgress_m4056_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_1_t855_0_0_0;
extern Il2CppType Action_1_t855_0_0_0;
static ParameterInfo Social_t1633_Social_LoadAchievements_m4059_ParameterInfos[] = 
{
	{"callback", 0, 134219397, 0, &Action_1_t855_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Social::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
MethodInfo Social_LoadAchievements_m4059_MethodInfo = 
{
	"LoadAchievements"/* name */
	, (methodPointerType)&Social_LoadAchievements_m4059/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Social_t1633_Social_LoadAchievements_m4059_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo Social_t1633_Social_ReportScore_m4055_ParameterInfos[] = 
{
	{"score", 0, 134219398, 0, &Int64_t233_0_0_0},
	{"board", 1, 134219399, 0, &String_t_0_0_0},
	{"callback", 2, 134219400, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Social::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
MethodInfo Social_ReportScore_m4055_MethodInfo = 
{
	"ReportScore"/* name */
	, (methodPointerType)&Social_ReportScore_m4055/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t/* invoker_method */
	, Social_t1633_Social_ReportScore_m4055_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Social::ShowAchievementsUI()
MethodInfo Social_ShowAchievementsUI_m4053_MethodInfo = 
{
	"ShowAchievementsUI"/* name */
	, (methodPointerType)&Social_ShowAchievementsUI_m4053/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Social::ShowLeaderboardUI()
MethodInfo Social_ShowLeaderboardUI_m4054_MethodInfo = 
{
	"ShowLeaderboardUI"/* name */
	, (methodPointerType)&Social_ShowLeaderboardUI_m4054/* method */
	, &Social_t1633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Social_t1633_MethodInfos[] =
{
	&Social_get_Active_m3724_MethodInfo,
	&Social_set_Active_m3723_MethodInfo,
	&Social_get_localUser_m4052_MethodInfo,
	&Social_ReportProgress_m4056_MethodInfo,
	&Social_LoadAchievements_m4059_MethodInfo,
	&Social_ReportScore_m4055_MethodInfo,
	&Social_ShowAchievementsUI_m4053_MethodInfo,
	&Social_ShowLeaderboardUI_m4054_MethodInfo,
	NULL
};
extern MethodInfo Social_get_Active_m3724_MethodInfo;
extern MethodInfo Social_set_Active_m3723_MethodInfo;
static PropertyInfo Social_t1633____Active_PropertyInfo = 
{
	&Social_t1633_il2cpp_TypeInfo/* parent */
	, "Active"/* name */
	, &Social_get_Active_m3724_MethodInfo/* get */
	, &Social_set_Active_m3723_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Social_get_localUser_m4052_MethodInfo;
static PropertyInfo Social_t1633____localUser_PropertyInfo = 
{
	&Social_t1633_il2cpp_TypeInfo/* parent */
	, "localUser"/* name */
	, &Social_get_localUser_m4052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Social_t1633_PropertyInfos[] =
{
	&Social_t1633____Active_PropertyInfo,
	&Social_t1633____localUser_PropertyInfo,
	NULL
};
static Il2CppMethodReference Social_t1633_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Social_t1633_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Social_t1633_0_0_0;
extern Il2CppType Social_t1633_1_0_0;
struct Social_t1633;
const Il2CppTypeDefinitionMetadata Social_t1633_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Social_t1633_VTable/* vtableMethods */
	, Social_t1633_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Social_t1633_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Social"/* name */
	, "UnityEngine"/* namespaze */
	, Social_t1633_MethodInfos/* methods */
	, Social_t1633_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Social_t1633_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Social_t1633_0_0_0/* byval_arg */
	, &Social_t1633_1_0_0/* this_arg */
	, &Social_t1633_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Social_t1633)/* instance_size */
	, sizeof (Social_t1633)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.ActivePlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_ActivePlatform.h"
// Metadata Definition UnityEngine.SocialPlatforms.ActivePlatform
extern TypeInfo ActivePlatform_t1634_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.ActivePlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_ActivePlatformMethodDeclarations.h"
extern Il2CppType ISocialPlatform_t1021_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::get_Instance()
MethodInfo ActivePlatform_get_Instance_m7382_MethodInfo = 
{
	"get_Instance"/* name */
	, (methodPointerType)&ActivePlatform_get_Instance_m7382/* method */
	, &ActivePlatform_t1634_il2cpp_TypeInfo/* declaring_type */
	, &ISocialPlatform_t1021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2195/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ISocialPlatform_t1021_0_0_0;
static ParameterInfo ActivePlatform_t1634_ActivePlatform_set_Instance_m7383_ParameterInfos[] = 
{
	{"value", 0, 134219401, 0, &ISocialPlatform_t1021_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ActivePlatform::set_Instance(UnityEngine.SocialPlatforms.ISocialPlatform)
MethodInfo ActivePlatform_set_Instance_m7383_MethodInfo = 
{
	"set_Instance"/* name */
	, (methodPointerType)&ActivePlatform_set_Instance_m7383/* method */
	, &ActivePlatform_t1634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ActivePlatform_t1634_ActivePlatform_set_Instance_m7383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2195/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ISocialPlatform_t1021_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::SelectSocialPlatform()
MethodInfo ActivePlatform_SelectSocialPlatform_m7384_MethodInfo = 
{
	"SelectSocialPlatform"/* name */
	, (methodPointerType)&ActivePlatform_SelectSocialPlatform_m7384/* method */
	, &ActivePlatform_t1634_il2cpp_TypeInfo/* declaring_type */
	, &ISocialPlatform_t1021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ActivePlatform_t1634_MethodInfos[] =
{
	&ActivePlatform_get_Instance_m7382_MethodInfo,
	&ActivePlatform_set_Instance_m7383_MethodInfo,
	&ActivePlatform_SelectSocialPlatform_m7384_MethodInfo,
	NULL
};
extern Il2CppType ISocialPlatform_t1021_0_0_17;
FieldInfo ActivePlatform_t1634_____active_0_FieldInfo = 
{
	"_active"/* name */
	, &ISocialPlatform_t1021_0_0_17/* type */
	, &ActivePlatform_t1634_il2cpp_TypeInfo/* parent */
	, offsetof(ActivePlatform_t1634_StaticFields, ____active_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ActivePlatform_t1634_FieldInfos[] =
{
	&ActivePlatform_t1634_____active_0_FieldInfo,
	NULL
};
extern MethodInfo ActivePlatform_get_Instance_m7382_MethodInfo;
extern MethodInfo ActivePlatform_set_Instance_m7383_MethodInfo;
static PropertyInfo ActivePlatform_t1634____Instance_PropertyInfo = 
{
	&ActivePlatform_t1634_il2cpp_TypeInfo/* parent */
	, "Instance"/* name */
	, &ActivePlatform_get_Instance_m7382_MethodInfo/* get */
	, &ActivePlatform_set_Instance_m7383_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ActivePlatform_t1634_PropertyInfos[] =
{
	&ActivePlatform_t1634____Instance_PropertyInfo,
	NULL
};
static Il2CppMethodReference ActivePlatform_t1634_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ActivePlatform_t1634_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ActivePlatform_t1634_0_0_0;
extern Il2CppType ActivePlatform_t1634_1_0_0;
struct ActivePlatform_t1634;
const Il2CppTypeDefinitionMetadata ActivePlatform_t1634_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivePlatform_t1634_VTable/* vtableMethods */
	, ActivePlatform_t1634_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ActivePlatform_t1634_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivePlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ActivePlatform_t1634_MethodInfos/* methods */
	, ActivePlatform_t1634_PropertyInfos/* properties */
	, ActivePlatform_t1634_FieldInfos/* fields */
	, NULL/* events */
	, &ActivePlatform_t1634_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivePlatform_t1634_0_0_0/* byval_arg */
	, &ActivePlatform_t1634_1_0_0/* this_arg */
	, &ActivePlatform_t1634_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivePlatform_t1634)/* instance_size */
	, sizeof (ActivePlatform_t1634)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ActivePlatform_t1634_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern TypeInfo ISocialPlatform_t1021_il2cpp_TypeInfo;
extern Il2CppType ILocalUser_t851_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.ISocialPlatform::get_localUser()
MethodInfo ISocialPlatform_get_localUser_m7574_MethodInfo = 
{
	"get_localUser"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &ILocalUser_t851_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo ISocialPlatform_t1021_ISocialPlatform_ReportProgress_m7575_ParameterInfos[] = 
{
	{"achievementID", 0, 134219402, 0, &String_t_0_0_0},
	{"progress", 1, 134219403, 0, &Double_t234_0_0_0},
	{"callback", 2, 134219404, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Double_t234_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
MethodInfo ISocialPlatform_ReportProgress_m7575_MethodInfo = 
{
	"ReportProgress"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Double_t234_Object_t/* invoker_method */
	, ISocialPlatform_t1021_ISocialPlatform_ReportProgress_m7575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_1_t855_0_0_0;
static ParameterInfo ISocialPlatform_t1021_ISocialPlatform_LoadAchievements_m7576_ParameterInfos[] = 
{
	{"callback", 0, 134219405, 0, &Action_1_t855_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
MethodInfo ISocialPlatform_LoadAchievements_m7576_MethodInfo = 
{
	"LoadAchievements"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ISocialPlatform_t1021_ISocialPlatform_LoadAchievements_m7576_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo ISocialPlatform_t1021_ISocialPlatform_ReportScore_m7577_ParameterInfos[] = 
{
	{"score", 0, 134219406, 0, &Int64_t233_0_0_0},
	{"board", 1, 134219407, 0, &String_t_0_0_0},
	{"callback", 2, 134219408, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
MethodInfo ISocialPlatform_ReportScore_m7577_MethodInfo = 
{
	"ReportScore"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1021_ISocialPlatform_ReportScore_m7577_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::ShowAchievementsUI()
MethodInfo ISocialPlatform_ShowAchievementsUI_m7578_MethodInfo = 
{
	"ShowAchievementsUI"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::ShowLeaderboardUI()
MethodInfo ISocialPlatform_ShowLeaderboardUI_m7579_MethodInfo = 
{
	"ShowLeaderboardUI"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILocalUser_t851_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo ISocialPlatform_t1021_ISocialPlatform_Authenticate_m7580_ParameterInfos[] = 
{
	{"user", 0, 134219409, 0, &ILocalUser_t851_0_0_0},
	{"callback", 1, 134219410, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
MethodInfo ISocialPlatform_Authenticate_m7580_MethodInfo = 
{
	"Authenticate"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1021_ISocialPlatform_Authenticate_m7580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILocalUser_t851_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo ISocialPlatform_t1021_ISocialPlatform_LoadFriends_m7581_ParameterInfos[] = 
{
	{"user", 0, 134219411, 0, &ILocalUser_t851_0_0_0},
	{"callback", 1, 134219412, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
MethodInfo ISocialPlatform_LoadFriends_m7581_MethodInfo = 
{
	"LoadFriends"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1021_ISocialPlatform_LoadFriends_m7581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ISocialPlatform_t1021_MethodInfos[] =
{
	&ISocialPlatform_get_localUser_m7574_MethodInfo,
	&ISocialPlatform_ReportProgress_m7575_MethodInfo,
	&ISocialPlatform_LoadAchievements_m7576_MethodInfo,
	&ISocialPlatform_ReportScore_m7577_MethodInfo,
	&ISocialPlatform_ShowAchievementsUI_m7578_MethodInfo,
	&ISocialPlatform_ShowLeaderboardUI_m7579_MethodInfo,
	&ISocialPlatform_Authenticate_m7580_MethodInfo,
	&ISocialPlatform_LoadFriends_m7581_MethodInfo,
	NULL
};
extern MethodInfo ISocialPlatform_get_localUser_m7574_MethodInfo;
static PropertyInfo ISocialPlatform_t1021____localUser_PropertyInfo = 
{
	&ISocialPlatform_t1021_il2cpp_TypeInfo/* parent */
	, "localUser"/* name */
	, &ISocialPlatform_get_localUser_m7574_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ISocialPlatform_t1021_PropertyInfos[] =
{
	&ISocialPlatform_t1021____localUser_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ISocialPlatform_t1021_1_0_0;
struct ISocialPlatform_t1021;
const Il2CppTypeDefinitionMetadata ISocialPlatform_t1021_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ISocialPlatform_t1021_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ISocialPlatform_t1021_MethodInfos/* methods */
	, ISocialPlatform_t1021_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ISocialPlatform_t1021_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISocialPlatform_t1021_0_0_0/* byval_arg */
	, &ISocialPlatform_t1021_1_0_0/* this_arg */
	, &ISocialPlatform_t1021_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern TypeInfo ILocalUser_t851_il2cpp_TypeInfo;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo ILocalUser_t851_ILocalUser_Authenticate_m7582_ParameterInfos[] = 
{
	{"callback", 0, 134219413, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ILocalUser::Authenticate(System.Action`1<System.Boolean>)
MethodInfo ILocalUser_Authenticate_m7582_MethodInfo = 
{
	"Authenticate"/* name */
	, NULL/* method */
	, &ILocalUser_t851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ILocalUser_t851_ILocalUser_Authenticate_m7582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
MethodInfo ILocalUser_get_authenticated_m7583_MethodInfo = 
{
	"get_authenticated"/* name */
	, NULL/* method */
	, &ILocalUser_t851_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILocalUser_t851_MethodInfos[] =
{
	&ILocalUser_Authenticate_m7582_MethodInfo,
	&ILocalUser_get_authenticated_m7583_MethodInfo,
	NULL
};
extern MethodInfo ILocalUser_get_authenticated_m7583_MethodInfo;
static PropertyInfo ILocalUser_t851____authenticated_PropertyInfo = 
{
	&ILocalUser_t851_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &ILocalUser_get_authenticated_m7583_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ILocalUser_t851_PropertyInfos[] =
{
	&ILocalUser_t851____authenticated_PropertyInfo,
	NULL
};
static const Il2CppType* ILocalUser_t851_InterfacesTypeInfos[] = 
{
	&IUserProfile_t983_0_0_0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ILocalUser_t851_1_0_0;
struct ILocalUser_t851;
const Il2CppTypeDefinitionMetadata ILocalUser_t851_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILocalUser_t851_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ILocalUser_t851_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILocalUser_t851_MethodInfos/* methods */
	, ILocalUser_t851_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ILocalUser_t851_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILocalUser_t851_0_0_0/* byval_arg */
	, &ILocalUser_t851_1_0_0/* this_arg */
	, &ILocalUser_t851_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern TypeInfo UserState_t1020_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
static MethodInfo* UserState_t1020_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo UserState_t1020____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &UserState_t1020_il2cpp_TypeInfo/* parent */
	, offsetof(UserState_t1020, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserState_t1020_0_0_32854;
FieldInfo UserState_t1020____Online_2_FieldInfo = 
{
	"Online"/* name */
	, &UserState_t1020_0_0_32854/* type */
	, &UserState_t1020_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserState_t1020_0_0_32854;
FieldInfo UserState_t1020____OnlineAndAway_3_FieldInfo = 
{
	"OnlineAndAway"/* name */
	, &UserState_t1020_0_0_32854/* type */
	, &UserState_t1020_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserState_t1020_0_0_32854;
FieldInfo UserState_t1020____OnlineAndBusy_4_FieldInfo = 
{
	"OnlineAndBusy"/* name */
	, &UserState_t1020_0_0_32854/* type */
	, &UserState_t1020_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserState_t1020_0_0_32854;
FieldInfo UserState_t1020____Offline_5_FieldInfo = 
{
	"Offline"/* name */
	, &UserState_t1020_0_0_32854/* type */
	, &UserState_t1020_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserState_t1020_0_0_32854;
FieldInfo UserState_t1020____Playing_6_FieldInfo = 
{
	"Playing"/* name */
	, &UserState_t1020_0_0_32854/* type */
	, &UserState_t1020_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UserState_t1020_FieldInfos[] =
{
	&UserState_t1020____value___1_FieldInfo,
	&UserState_t1020____Online_2_FieldInfo,
	&UserState_t1020____OnlineAndAway_3_FieldInfo,
	&UserState_t1020____OnlineAndBusy_4_FieldInfo,
	&UserState_t1020____Offline_5_FieldInfo,
	&UserState_t1020____Playing_6_FieldInfo,
	NULL
};
static const int32_t UserState_t1020____Online_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UserState_t1020____Online_2_DefaultValue = 
{
	&UserState_t1020____Online_2_FieldInfo/* field */
	, { (char*)&UserState_t1020____Online_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UserState_t1020____OnlineAndAway_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UserState_t1020____OnlineAndAway_3_DefaultValue = 
{
	&UserState_t1020____OnlineAndAway_3_FieldInfo/* field */
	, { (char*)&UserState_t1020____OnlineAndAway_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UserState_t1020____OnlineAndBusy_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UserState_t1020____OnlineAndBusy_4_DefaultValue = 
{
	&UserState_t1020____OnlineAndBusy_4_FieldInfo/* field */
	, { (char*)&UserState_t1020____OnlineAndBusy_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UserState_t1020____Offline_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry UserState_t1020____Offline_5_DefaultValue = 
{
	&UserState_t1020____Offline_5_FieldInfo/* field */
	, { (char*)&UserState_t1020____Offline_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UserState_t1020____Playing_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry UserState_t1020____Playing_6_DefaultValue = 
{
	&UserState_t1020____Playing_6_FieldInfo/* field */
	, { (char*)&UserState_t1020____Playing_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserState_t1020_FieldDefaultValues[] = 
{
	&UserState_t1020____Online_2_DefaultValue,
	&UserState_t1020____OnlineAndAway_3_DefaultValue,
	&UserState_t1020____OnlineAndBusy_4_DefaultValue,
	&UserState_t1020____Offline_5_DefaultValue,
	&UserState_t1020____Playing_6_DefaultValue,
	NULL
};
static Il2CppMethodReference UserState_t1020_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool UserState_t1020_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserState_t1020_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserState_t1020_1_0_0;
const Il2CppTypeDefinitionMetadata UserState_t1020_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserState_t1020_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, UserState_t1020_VTable/* vtableMethods */
	, UserState_t1020_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UserState_t1020_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserState_t1020_MethodInfos/* methods */
	, NULL/* properties */
	, UserState_t1020_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserState_t1020_0_0_0/* byval_arg */
	, &UserState_t1020_1_0_0/* this_arg */
	, &UserState_t1020_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserState_t1020_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t1020)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserState_t1020)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
extern TypeInfo IUserProfile_t983_il2cpp_TypeInfo;
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.IUserProfile::get_id()
MethodInfo IUserProfile_get_id_m7584_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &IUserProfile_t983_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IUserProfile_t983_MethodInfos[] =
{
	&IUserProfile_get_id_m7584_MethodInfo,
	NULL
};
extern MethodInfo IUserProfile_get_id_m7584_MethodInfo;
static PropertyInfo IUserProfile_t983____id_PropertyInfo = 
{
	&IUserProfile_t983_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &IUserProfile_get_id_m7584_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IUserProfile_t983_PropertyInfos[] =
{
	&IUserProfile_t983____id_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IUserProfile_t983_1_0_0;
struct IUserProfile_t983;
const Il2CppTypeDefinitionMetadata IUserProfile_t983_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IUserProfile_t983_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfile_t983_MethodInfos/* methods */
	, IUserProfile_t983_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IUserProfile_t983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUserProfile_t983_0_0_0/* byval_arg */
	, &IUserProfile_t983_1_0_0/* this_arg */
	, &IUserProfile_t983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
extern TypeInfo IAchievement_t856_il2cpp_TypeInfo;
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.IAchievement::get_id()
MethodInfo IAchievement_get_id_m7585_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &IAchievement_t856_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Double_t234_0_0_0;
extern void* RuntimeInvoker_Double_t234 (MethodInfo* method, void* obj, void** args);
// System.Double UnityEngine.SocialPlatforms.IAchievement::get_percentCompleted()
MethodInfo IAchievement_get_percentCompleted_m7586_MethodInfo = 
{
	"get_percentCompleted"/* name */
	, NULL/* method */
	, &IAchievement_t856_il2cpp_TypeInfo/* declaring_type */
	, &Double_t234_0_0_0/* return_type */
	, RuntimeInvoker_Double_t234/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.IAchievement::get_completed()
MethodInfo IAchievement_get_completed_m7587_MethodInfo = 
{
	"get_completed"/* name */
	, NULL/* method */
	, &IAchievement_t856_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IAchievement_t856_MethodInfos[] =
{
	&IAchievement_get_id_m7585_MethodInfo,
	&IAchievement_get_percentCompleted_m7586_MethodInfo,
	&IAchievement_get_completed_m7587_MethodInfo,
	NULL
};
extern MethodInfo IAchievement_get_id_m7585_MethodInfo;
static PropertyInfo IAchievement_t856____id_PropertyInfo = 
{
	&IAchievement_t856_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &IAchievement_get_id_m7585_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IAchievement_get_percentCompleted_m7586_MethodInfo;
static PropertyInfo IAchievement_t856____percentCompleted_PropertyInfo = 
{
	&IAchievement_t856_il2cpp_TypeInfo/* parent */
	, "percentCompleted"/* name */
	, &IAchievement_get_percentCompleted_m7586_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IAchievement_get_completed_m7587_MethodInfo;
static PropertyInfo IAchievement_t856____completed_PropertyInfo = 
{
	&IAchievement_t856_il2cpp_TypeInfo/* parent */
	, "completed"/* name */
	, &IAchievement_get_completed_m7587_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IAchievement_t856_PropertyInfos[] =
{
	&IAchievement_t856____id_PropertyInfo,
	&IAchievement_t856____percentCompleted_PropertyInfo,
	&IAchievement_t856____completed_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievement_t856_1_0_0;
struct IAchievement_t856;
const Il2CppTypeDefinitionMetadata IAchievement_t856_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IAchievement_t856_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievement_t856_MethodInfos/* methods */
	, IAchievement_t856_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IAchievement_t856_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievement_t856_0_0_0/* byval_arg */
	, &IAchievement_t856_1_0_0/* this_arg */
	, &IAchievement_t856_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
extern TypeInfo IAchievementDescription_t1707_il2cpp_TypeInfo;
static MethodInfo* IAchievementDescription_t1707_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievementDescription_t1707_1_0_0;
struct IAchievementDescription_t1707;
const Il2CppTypeDefinitionMetadata IAchievementDescription_t1707_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IAchievementDescription_t1707_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescription_t1707_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IAchievementDescription_t1707_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievementDescription_t1707_0_0_0/* byval_arg */
	, &IAchievementDescription_t1707_1_0_0/* this_arg */
	, &IAchievementDescription_t1707_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IScore
extern TypeInfo IScore_t1022_il2cpp_TypeInfo;
static MethodInfo* IScore_t1022_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IScore_t1022_1_0_0;
struct IScore_t1022;
const Il2CppTypeDefinitionMetadata IScore_t1022_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IScore_t1022_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScore_t1022_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IScore_t1022_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScore_t1022_0_0_0/* byval_arg */
	, &IScore_t1022_1_0_0/* this_arg */
	, &IScore_t1022_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern TypeInfo UserScope_t1635_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
static MethodInfo* UserScope_t1635_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo UserScope_t1635____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &UserScope_t1635_il2cpp_TypeInfo/* parent */
	, offsetof(UserScope_t1635, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1635_0_0_32854;
FieldInfo UserScope_t1635____Global_2_FieldInfo = 
{
	"Global"/* name */
	, &UserScope_t1635_0_0_32854/* type */
	, &UserScope_t1635_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1635_0_0_32854;
FieldInfo UserScope_t1635____FriendsOnly_3_FieldInfo = 
{
	"FriendsOnly"/* name */
	, &UserScope_t1635_0_0_32854/* type */
	, &UserScope_t1635_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UserScope_t1635_FieldInfos[] =
{
	&UserScope_t1635____value___1_FieldInfo,
	&UserScope_t1635____Global_2_FieldInfo,
	&UserScope_t1635____FriendsOnly_3_FieldInfo,
	NULL
};
static const int32_t UserScope_t1635____Global_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UserScope_t1635____Global_2_DefaultValue = 
{
	&UserScope_t1635____Global_2_FieldInfo/* field */
	, { (char*)&UserScope_t1635____Global_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UserScope_t1635____FriendsOnly_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UserScope_t1635____FriendsOnly_3_DefaultValue = 
{
	&UserScope_t1635____FriendsOnly_3_FieldInfo/* field */
	, { (char*)&UserScope_t1635____FriendsOnly_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserScope_t1635_FieldDefaultValues[] = 
{
	&UserScope_t1635____Global_2_DefaultValue,
	&UserScope_t1635____FriendsOnly_3_DefaultValue,
	NULL
};
static Il2CppMethodReference UserScope_t1635_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool UserScope_t1635_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserScope_t1635_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserScope_t1635_1_0_0;
const Il2CppTypeDefinitionMetadata UserScope_t1635_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserScope_t1635_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, UserScope_t1635_VTable/* vtableMethods */
	, UserScope_t1635_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UserScope_t1635_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserScope_t1635_MethodInfos/* methods */
	, NULL/* properties */
	, UserScope_t1635_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserScope_t1635_0_0_0/* byval_arg */
	, &UserScope_t1635_1_0_0/* this_arg */
	, &UserScope_t1635_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserScope_t1635_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t1635)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserScope_t1635)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern TypeInfo TimeScope_t1636_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
static MethodInfo* TimeScope_t1636_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo TimeScope_t1636____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &TimeScope_t1636_il2cpp_TypeInfo/* parent */
	, offsetof(TimeScope_t1636, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1636_0_0_32854;
FieldInfo TimeScope_t1636____Today_2_FieldInfo = 
{
	"Today"/* name */
	, &TimeScope_t1636_0_0_32854/* type */
	, &TimeScope_t1636_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1636_0_0_32854;
FieldInfo TimeScope_t1636____Week_3_FieldInfo = 
{
	"Week"/* name */
	, &TimeScope_t1636_0_0_32854/* type */
	, &TimeScope_t1636_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1636_0_0_32854;
FieldInfo TimeScope_t1636____AllTime_4_FieldInfo = 
{
	"AllTime"/* name */
	, &TimeScope_t1636_0_0_32854/* type */
	, &TimeScope_t1636_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TimeScope_t1636_FieldInfos[] =
{
	&TimeScope_t1636____value___1_FieldInfo,
	&TimeScope_t1636____Today_2_FieldInfo,
	&TimeScope_t1636____Week_3_FieldInfo,
	&TimeScope_t1636____AllTime_4_FieldInfo,
	NULL
};
static const int32_t TimeScope_t1636____Today_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TimeScope_t1636____Today_2_DefaultValue = 
{
	&TimeScope_t1636____Today_2_FieldInfo/* field */
	, { (char*)&TimeScope_t1636____Today_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TimeScope_t1636____Week_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TimeScope_t1636____Week_3_DefaultValue = 
{
	&TimeScope_t1636____Week_3_FieldInfo/* field */
	, { (char*)&TimeScope_t1636____Week_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TimeScope_t1636____AllTime_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TimeScope_t1636____AllTime_4_DefaultValue = 
{
	&TimeScope_t1636____AllTime_4_FieldInfo/* field */
	, { (char*)&TimeScope_t1636____AllTime_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TimeScope_t1636_FieldDefaultValues[] = 
{
	&TimeScope_t1636____Today_2_DefaultValue,
	&TimeScope_t1636____Week_3_DefaultValue,
	&TimeScope_t1636____AllTime_4_DefaultValue,
	NULL
};
static Il2CppMethodReference TimeScope_t1636_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool TimeScope_t1636_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TimeScope_t1636_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TimeScope_t1636_1_0_0;
const Il2CppTypeDefinitionMetadata TimeScope_t1636_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimeScope_t1636_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, TimeScope_t1636_VTable/* vtableMethods */
	, TimeScope_t1636_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TimeScope_t1636_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, TimeScope_t1636_MethodInfos/* methods */
	, NULL/* properties */
	, TimeScope_t1636_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimeScope_t1636_0_0_0/* byval_arg */
	, &TimeScope_t1636_1_0_0/* this_arg */
	, &TimeScope_t1636_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TimeScope_t1636_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t1636)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeScope_t1636)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern TypeInfo Range_t1628_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Range_t1628_Range__ctor_m7385_ParameterInfos[] = 
{
	{"fromValue", 0, 134219414, 0, &Int32_t189_0_0_0},
	{"valueCount", 1, 134219415, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
MethodInfo Range__ctor_m7385_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Range__ctor_m7385/* method */
	, &Range_t1628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189/* invoker_method */
	, Range_t1628_Range__ctor_m7385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Range_t1628_MethodInfos[] =
{
	&Range__ctor_m7385_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Range_t1628____from_0_FieldInfo = 
{
	"from"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Range_t1628_il2cpp_TypeInfo/* parent */
	, offsetof(Range_t1628, ___from_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Range_t1628____count_1_FieldInfo = 
{
	"count"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Range_t1628_il2cpp_TypeInfo/* parent */
	, offsetof(Range_t1628, ___count_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Range_t1628_FieldInfos[] =
{
	&Range_t1628____from_0_FieldInfo,
	&Range_t1628____count_1_FieldInfo,
	NULL
};
static Il2CppMethodReference Range_t1628_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool Range_t1628_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Range_t1628_1_0_0;
const Il2CppTypeDefinitionMetadata Range_t1628_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, Range_t1628_VTable/* vtableMethods */
	, Range_t1628_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Range_t1628_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, Range_t1628_MethodInfos/* methods */
	, NULL/* properties */
	, Range_t1628_FieldInfos/* fields */
	, NULL/* events */
	, &Range_t1628_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Range_t1628_0_0_0/* byval_arg */
	, &Range_t1628_1_0_0/* this_arg */
	, &Range_t1628_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t1628)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Range_t1628)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Range_t1628 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern TypeInfo ILeaderboard_t858_il2cpp_TypeInfo;
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
MethodInfo ILeaderboard_get_id_m7588_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &ILeaderboard_t858_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserScope_t1635_0_0_0;
extern void* RuntimeInvoker_UserScope_t1635 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
MethodInfo ILeaderboard_get_userScope_m7589_MethodInfo = 
{
	"get_userScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t858_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1635_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1635/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Range_t1628_0_0_0;
extern void* RuntimeInvoker_Range_t1628 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
MethodInfo ILeaderboard_get_range_m7590_MethodInfo = 
{
	"get_range"/* name */
	, NULL/* method */
	, &ILeaderboard_t858_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1628_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1628/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TimeScope_t1636_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1636 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
MethodInfo ILeaderboard_get_timeScope_m7591_MethodInfo = 
{
	"get_timeScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t858_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1636_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1636/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILeaderboard_t858_MethodInfos[] =
{
	&ILeaderboard_get_id_m7588_MethodInfo,
	&ILeaderboard_get_userScope_m7589_MethodInfo,
	&ILeaderboard_get_range_m7590_MethodInfo,
	&ILeaderboard_get_timeScope_m7591_MethodInfo,
	NULL
};
extern MethodInfo ILeaderboard_get_id_m7588_MethodInfo;
static PropertyInfo ILeaderboard_t858____id_PropertyInfo = 
{
	&ILeaderboard_t858_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &ILeaderboard_get_id_m7588_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_userScope_m7589_MethodInfo;
static PropertyInfo ILeaderboard_t858____userScope_PropertyInfo = 
{
	&ILeaderboard_t858_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &ILeaderboard_get_userScope_m7589_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_range_m7590_MethodInfo;
static PropertyInfo ILeaderboard_t858____range_PropertyInfo = 
{
	&ILeaderboard_t858_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &ILeaderboard_get_range_m7590_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_timeScope_m7591_MethodInfo;
static PropertyInfo ILeaderboard_t858____timeScope_PropertyInfo = 
{
	&ILeaderboard_t858_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &ILeaderboard_get_timeScope_m7591_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ILeaderboard_t858_PropertyInfos[] =
{
	&ILeaderboard_t858____id_PropertyInfo,
	&ILeaderboard_t858____userScope_PropertyInfo,
	&ILeaderboard_t858____range_PropertyInfo,
	&ILeaderboard_t858____timeScope_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ILeaderboard_t858_1_0_0;
struct ILeaderboard_t858;
const Il2CppTypeDefinitionMetadata ILeaderboard_t858_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ILeaderboard_t858_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILeaderboard_t858_MethodInfos/* methods */
	, ILeaderboard_t858_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ILeaderboard_t858_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILeaderboard_t858_0_0_0/* byval_arg */
	, &ILeaderboard_t858_1_0_0/* this_arg */
	, &ILeaderboard_t858_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// Metadata Definition UnityEngine.PropertyAttribute
extern TypeInfo PropertyAttribute_t1637_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.PropertyAttribute::.ctor()
MethodInfo PropertyAttribute__ctor_m7386_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyAttribute__ctor_m7386/* method */
	, &PropertyAttribute_t1637_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PropertyAttribute_t1637_MethodInfos[] =
{
	&PropertyAttribute__ctor_m7386_MethodInfo,
	NULL
};
extern MethodInfo Attribute_Equals_m7644_MethodInfo;
extern MethodInfo Attribute_GetHashCode_m7549_MethodInfo;
static Il2CppMethodReference PropertyAttribute_t1637_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool PropertyAttribute_t1637_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppType _Attribute_t1731_0_0_0;
static Il2CppInterfaceOffsetPair PropertyAttribute_t1637_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PropertyAttribute_t1637_0_0_0;
extern Il2CppType PropertyAttribute_t1637_1_0_0;
extern Il2CppType Attribute_t1546_0_0_0;
struct PropertyAttribute_t1637;
const Il2CppTypeDefinitionMetadata PropertyAttribute_t1637_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttribute_t1637_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, PropertyAttribute_t1637_VTable/* vtableMethods */
	, PropertyAttribute_t1637_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PropertyAttribute_t1637_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, PropertyAttribute_t1637_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1637_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 636/* custom_attributes_cache */
	, &PropertyAttribute_t1637_0_0_0/* byval_arg */
	, &PropertyAttribute_t1637_1_0_0/* this_arg */
	, &PropertyAttribute_t1637_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t1637)/* instance_size */
	, sizeof (PropertyAttribute_t1637)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// Metadata Definition UnityEngine.TooltipAttribute
extern TypeInfo TooltipAttribute_t1442_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo TooltipAttribute_t1442_TooltipAttribute__ctor_m6246_ParameterInfos[] = 
{
	{"tooltip", 0, 134219416, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
MethodInfo TooltipAttribute__ctor_m6246_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TooltipAttribute__ctor_m6246/* method */
	, &TooltipAttribute_t1442_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, TooltipAttribute_t1442_TooltipAttribute__ctor_m6246_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TooltipAttribute_t1442_MethodInfos[] =
{
	&TooltipAttribute__ctor_m6246_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_38;
FieldInfo TooltipAttribute_t1442____tooltip_0_FieldInfo = 
{
	"tooltip"/* name */
	, &String_t_0_0_38/* type */
	, &TooltipAttribute_t1442_il2cpp_TypeInfo/* parent */
	, offsetof(TooltipAttribute_t1442, ___tooltip_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TooltipAttribute_t1442_FieldInfos[] =
{
	&TooltipAttribute_t1442____tooltip_0_FieldInfo,
	NULL
};
static Il2CppMethodReference TooltipAttribute_t1442_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TooltipAttribute_t1442_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t1442_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TooltipAttribute_t1442_0_0_0;
extern Il2CppType TooltipAttribute_t1442_1_0_0;
struct TooltipAttribute_t1442;
const Il2CppTypeDefinitionMetadata TooltipAttribute_t1442_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TooltipAttribute_t1442_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1637_0_0_0/* parent */
	, TooltipAttribute_t1442_VTable/* vtableMethods */
	, TooltipAttribute_t1442_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TooltipAttribute_t1442_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TooltipAttribute_t1442_MethodInfos/* methods */
	, NULL/* properties */
	, TooltipAttribute_t1442_FieldInfos/* fields */
	, NULL/* events */
	, &TooltipAttribute_t1442_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 637/* custom_attributes_cache */
	, &TooltipAttribute_t1442_0_0_0/* byval_arg */
	, &TooltipAttribute_t1442_1_0_0/* this_arg */
	, &TooltipAttribute_t1442_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t1442)/* instance_size */
	, sizeof (TooltipAttribute_t1442)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// Metadata Definition UnityEngine.SpaceAttribute
extern TypeInfo SpaceAttribute_t1440_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo SpaceAttribute_t1440_SpaceAttribute__ctor_m6244_ParameterInfos[] = 
{
	{"height", 0, 134219417, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
MethodInfo SpaceAttribute__ctor_m6244_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SpaceAttribute__ctor_m6244/* method */
	, &SpaceAttribute_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, SpaceAttribute_t1440_SpaceAttribute__ctor_m6244_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SpaceAttribute_t1440_MethodInfos[] =
{
	&SpaceAttribute__ctor_m6244_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_38;
FieldInfo SpaceAttribute_t1440____height_0_FieldInfo = 
{
	"height"/* name */
	, &Single_t202_0_0_38/* type */
	, &SpaceAttribute_t1440_il2cpp_TypeInfo/* parent */
	, offsetof(SpaceAttribute_t1440, ___height_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* SpaceAttribute_t1440_FieldInfos[] =
{
	&SpaceAttribute_t1440____height_0_FieldInfo,
	NULL
};
static Il2CppMethodReference SpaceAttribute_t1440_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool SpaceAttribute_t1440_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t1440_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SpaceAttribute_t1440_0_0_0;
extern Il2CppType SpaceAttribute_t1440_1_0_0;
struct SpaceAttribute_t1440;
const Il2CppTypeDefinitionMetadata SpaceAttribute_t1440_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpaceAttribute_t1440_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1637_0_0_0/* parent */
	, SpaceAttribute_t1440_VTable/* vtableMethods */
	, SpaceAttribute_t1440_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SpaceAttribute_t1440_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceAttribute_t1440_MethodInfos/* methods */
	, NULL/* properties */
	, SpaceAttribute_t1440_FieldInfos/* fields */
	, NULL/* events */
	, &SpaceAttribute_t1440_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 638/* custom_attributes_cache */
	, &SpaceAttribute_t1440_0_0_0/* byval_arg */
	, &SpaceAttribute_t1440_1_0_0/* this_arg */
	, &SpaceAttribute_t1440_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t1440)/* instance_size */
	, sizeof (SpaceAttribute_t1440)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// Metadata Definition UnityEngine.RangeAttribute
extern TypeInfo RangeAttribute_t1436_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo RangeAttribute_t1436_RangeAttribute__ctor_m6237_ParameterInfos[] = 
{
	{"min", 0, 134219418, 0, &Single_t202_0_0_0},
	{"max", 1, 134219419, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
MethodInfo RangeAttribute__ctor_m6237_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RangeAttribute__ctor_m6237/* method */
	, &RangeAttribute_t1436_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202_Single_t202/* invoker_method */
	, RangeAttribute_t1436_RangeAttribute__ctor_m6237_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RangeAttribute_t1436_MethodInfos[] =
{
	&RangeAttribute__ctor_m6237_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_38;
FieldInfo RangeAttribute_t1436____min_0_FieldInfo = 
{
	"min"/* name */
	, &Single_t202_0_0_38/* type */
	, &RangeAttribute_t1436_il2cpp_TypeInfo/* parent */
	, offsetof(RangeAttribute_t1436, ___min_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_38;
FieldInfo RangeAttribute_t1436____max_1_FieldInfo = 
{
	"max"/* name */
	, &Single_t202_0_0_38/* type */
	, &RangeAttribute_t1436_il2cpp_TypeInfo/* parent */
	, offsetof(RangeAttribute_t1436, ___max_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RangeAttribute_t1436_FieldInfos[] =
{
	&RangeAttribute_t1436____min_0_FieldInfo,
	&RangeAttribute_t1436____max_1_FieldInfo,
	NULL
};
static Il2CppMethodReference RangeAttribute_t1436_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool RangeAttribute_t1436_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RangeAttribute_t1436_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RangeAttribute_t1436_0_0_0;
extern Il2CppType RangeAttribute_t1436_1_0_0;
struct RangeAttribute_t1436;
const Il2CppTypeDefinitionMetadata RangeAttribute_t1436_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RangeAttribute_t1436_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1637_0_0_0/* parent */
	, RangeAttribute_t1436_VTable/* vtableMethods */
	, RangeAttribute_t1436_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RangeAttribute_t1436_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, RangeAttribute_t1436_MethodInfos/* methods */
	, NULL/* properties */
	, RangeAttribute_t1436_FieldInfos/* fields */
	, NULL/* events */
	, &RangeAttribute_t1436_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 639/* custom_attributes_cache */
	, &RangeAttribute_t1436_0_0_0/* byval_arg */
	, &RangeAttribute_t1436_1_0_0/* this_arg */
	, &RangeAttribute_t1436_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t1436)/* instance_size */
	, sizeof (RangeAttribute_t1436)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// Metadata Definition UnityEngine.TextAreaAttribute
extern TypeInfo TextAreaAttribute_t1443_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo TextAreaAttribute_t1443_TextAreaAttribute__ctor_m6249_ParameterInfos[] = 
{
	{"minLines", 0, 134219420, 0, &Int32_t189_0_0_0},
	{"maxLines", 1, 134219421, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
MethodInfo TextAreaAttribute__ctor_m6249_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextAreaAttribute__ctor_m6249/* method */
	, &TextAreaAttribute_t1443_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Int32_t189/* invoker_method */
	, TextAreaAttribute_t1443_TextAreaAttribute__ctor_m6249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextAreaAttribute_t1443_MethodInfos[] =
{
	&TextAreaAttribute__ctor_m6249_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_38;
FieldInfo TextAreaAttribute_t1443____minLines_0_FieldInfo = 
{
	"minLines"/* name */
	, &Int32_t189_0_0_38/* type */
	, &TextAreaAttribute_t1443_il2cpp_TypeInfo/* parent */
	, offsetof(TextAreaAttribute_t1443, ___minLines_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_38;
FieldInfo TextAreaAttribute_t1443____maxLines_1_FieldInfo = 
{
	"maxLines"/* name */
	, &Int32_t189_0_0_38/* type */
	, &TextAreaAttribute_t1443_il2cpp_TypeInfo/* parent */
	, offsetof(TextAreaAttribute_t1443, ___maxLines_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TextAreaAttribute_t1443_FieldInfos[] =
{
	&TextAreaAttribute_t1443____minLines_0_FieldInfo,
	&TextAreaAttribute_t1443____maxLines_1_FieldInfo,
	NULL
};
static Il2CppMethodReference TextAreaAttribute_t1443_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TextAreaAttribute_t1443_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t1443_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextAreaAttribute_t1443_0_0_0;
extern Il2CppType TextAreaAttribute_t1443_1_0_0;
struct TextAreaAttribute_t1443;
const Il2CppTypeDefinitionMetadata TextAreaAttribute_t1443_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAreaAttribute_t1443_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1637_0_0_0/* parent */
	, TextAreaAttribute_t1443_VTable/* vtableMethods */
	, TextAreaAttribute_t1443_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TextAreaAttribute_t1443_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TextAreaAttribute_t1443_MethodInfos/* methods */
	, NULL/* properties */
	, TextAreaAttribute_t1443_FieldInfos/* fields */
	, NULL/* events */
	, &TextAreaAttribute_t1443_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 640/* custom_attributes_cache */
	, &TextAreaAttribute_t1443_0_0_0/* byval_arg */
	, &TextAreaAttribute_t1443_1_0_0/* this_arg */
	, &TextAreaAttribute_t1443_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t1443)/* instance_size */
	, sizeof (TextAreaAttribute_t1443)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern TypeInfo SelectionBaseAttribute_t1441_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
MethodInfo SelectionBaseAttribute__ctor_m6245_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SelectionBaseAttribute__ctor_m6245/* method */
	, &SelectionBaseAttribute_t1441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SelectionBaseAttribute_t1441_MethodInfos[] =
{
	&SelectionBaseAttribute__ctor_m6245_MethodInfo,
	NULL
};
static Il2CppMethodReference SelectionBaseAttribute_t1441_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool SelectionBaseAttribute_t1441_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t1441_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SelectionBaseAttribute_t1441_0_0_0;
extern Il2CppType SelectionBaseAttribute_t1441_1_0_0;
struct SelectionBaseAttribute_t1441;
const Il2CppTypeDefinitionMetadata SelectionBaseAttribute_t1441_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionBaseAttribute_t1441_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, SelectionBaseAttribute_t1441_VTable/* vtableMethods */
	, SelectionBaseAttribute_t1441_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SelectionBaseAttribute_t1441_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SelectionBaseAttribute_t1441_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SelectionBaseAttribute_t1441_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 641/* custom_attributes_cache */
	, &SelectionBaseAttribute_t1441_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t1441_1_0_0/* this_arg */
	, &SelectionBaseAttribute_t1441_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t1441)/* instance_size */
	, sizeof (SelectionBaseAttribute_t1441)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// Metadata Definition UnityEngine.SliderState
extern TypeInfo SliderState_t1638_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderState::.ctor()
MethodInfo SliderState__ctor_m7387_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderState__ctor_m7387/* method */
	, &SliderState_t1638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SliderState_t1638_MethodInfos[] =
{
	&SliderState__ctor_m7387_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_6;
FieldInfo SliderState_t1638____dragStartPos_0_FieldInfo = 
{
	"dragStartPos"/* name */
	, &Single_t202_0_0_6/* type */
	, &SliderState_t1638_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1638, ___dragStartPos_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo SliderState_t1638____dragStartValue_1_FieldInfo = 
{
	"dragStartValue"/* name */
	, &Single_t202_0_0_6/* type */
	, &SliderState_t1638_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1638, ___dragStartValue_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo SliderState_t1638____isDragging_2_FieldInfo = 
{
	"isDragging"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &SliderState_t1638_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1638, ___isDragging_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* SliderState_t1638_FieldInfos[] =
{
	&SliderState_t1638____dragStartPos_0_FieldInfo,
	&SliderState_t1638____dragStartValue_1_FieldInfo,
	&SliderState_t1638____isDragging_2_FieldInfo,
	NULL
};
static Il2CppMethodReference SliderState_t1638_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool SliderState_t1638_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SliderState_t1638_0_0_0;
extern Il2CppType SliderState_t1638_1_0_0;
struct SliderState_t1638;
const Il2CppTypeDefinitionMetadata SliderState_t1638_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SliderState_t1638_VTable/* vtableMethods */
	, SliderState_t1638_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SliderState_t1638_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, SliderState_t1638_MethodInfos/* methods */
	, NULL/* properties */
	, SliderState_t1638_FieldInfos/* fields */
	, NULL/* events */
	, &SliderState_t1638_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderState_t1638_0_0_0/* byval_arg */
	, &SliderState_t1638_1_0_0/* this_arg */
	, &SliderState_t1638_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t1638)/* instance_size */
	, sizeof (SliderState_t1638)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// Metadata Definition UnityEngine.StackTraceUtility
extern TypeInfo StackTraceUtility_t1639_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.ctor()
MethodInfo StackTraceUtility__ctor_m7388_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StackTraceUtility__ctor_m7388/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.cctor()
MethodInfo StackTraceUtility__cctor_m7389_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StackTraceUtility__cctor_m7389/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo StackTraceUtility_t1639_StackTraceUtility_SetProjectFolder_m7390_ParameterInfos[] = 
{
	{"folder", 0, 134219422, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
MethodInfo StackTraceUtility_SetProjectFolder_m7390_MethodInfo = 
{
	"SetProjectFolder"/* name */
	, (methodPointerType)&StackTraceUtility_SetProjectFolder_m7390/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, StackTraceUtility_t1639_StackTraceUtility_SetProjectFolder_m7390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
MethodInfo StackTraceUtility_ExtractStackTrace_m7391_MethodInfo = 
{
	"ExtractStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStackTrace_m7391/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 642/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo StackTraceUtility_t1639_StackTraceUtility_IsSystemStacktraceType_m7392_ParameterInfos[] = 
{
	{"name", 0, 134219423, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
MethodInfo StackTraceUtility_IsSystemStacktraceType_m7392_MethodInfo = 
{
	"IsSystemStacktraceType"/* name */
	, (methodPointerType)&StackTraceUtility_IsSystemStacktraceType_m7392/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, StackTraceUtility_t1639_StackTraceUtility_IsSystemStacktraceType_m7392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo StackTraceUtility_t1639_StackTraceUtility_ExtractStringFromException_m7393_ParameterInfos[] = 
{
	{"exception", 0, 134219424, 0, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
MethodInfo StackTraceUtility_ExtractStringFromException_m7393_MethodInfo = 
{
	"ExtractStringFromException"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromException_m7393/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1639_StackTraceUtility_ExtractStringFromException_m7393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_1_0_2;
extern Il2CppType String_t_1_0_0;
extern Il2CppType String_t_1_0_2;
static ParameterInfo StackTraceUtility_t1639_StackTraceUtility_ExtractStringFromExceptionInternal_m7394_ParameterInfos[] = 
{
	{"exceptiono", 0, 134219425, 0, &Object_t_0_0_0},
	{"message", 1, 134219426, 0, &String_t_1_0_2},
	{"stackTrace", 2, 134219427, 0, &String_t_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StringU26_t319_StringU26_t319 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m7394_MethodInfo = 
{
	"ExtractStringFromExceptionInternal"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromExceptionInternal_m7394/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StringU26_t319_StringU26_t319/* invoker_method */
	, StackTraceUtility_t1639_StackTraceUtility_ExtractStringFromExceptionInternal_m7394_ParameterInfos/* parameters */
	, 643/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo StackTraceUtility_t1639_StackTraceUtility_PostprocessStacktrace_m7395_ParameterInfos[] = 
{
	{"oldString", 0, 134219428, 0, &String_t_0_0_0},
	{"stripEngineInternalInformation", 1, 134219429, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
MethodInfo StackTraceUtility_PostprocessStacktrace_m7395_MethodInfo = 
{
	"PostprocessStacktrace"/* name */
	, (methodPointerType)&StackTraceUtility_PostprocessStacktrace_m7395/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236/* invoker_method */
	, StackTraceUtility_t1639_StackTraceUtility_PostprocessStacktrace_m7395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StackTrace_t1672_0_0_0;
extern Il2CppType StackTrace_t1672_0_0_0;
static ParameterInfo StackTraceUtility_t1639_StackTraceUtility_ExtractFormattedStackTrace_m7396_ParameterInfos[] = 
{
	{"stackTrace", 0, 134219430, 0, &StackTrace_t1672_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m7396_MethodInfo = 
{
	"ExtractFormattedStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractFormattedStackTrace_m7396/* method */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1639_StackTraceUtility_ExtractFormattedStackTrace_m7396_ParameterInfos/* parameters */
	, 644/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* StackTraceUtility_t1639_MethodInfos[] =
{
	&StackTraceUtility__ctor_m7388_MethodInfo,
	&StackTraceUtility__cctor_m7389_MethodInfo,
	&StackTraceUtility_SetProjectFolder_m7390_MethodInfo,
	&StackTraceUtility_ExtractStackTrace_m7391_MethodInfo,
	&StackTraceUtility_IsSystemStacktraceType_m7392_MethodInfo,
	&StackTraceUtility_ExtractStringFromException_m7393_MethodInfo,
	&StackTraceUtility_ExtractStringFromExceptionInternal_m7394_MethodInfo,
	&StackTraceUtility_PostprocessStacktrace_m7395_MethodInfo,
	&StackTraceUtility_ExtractFormattedStackTrace_m7396_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_17;
FieldInfo StackTraceUtility_t1639____projectFolder_0_FieldInfo = 
{
	"projectFolder"/* name */
	, &String_t_0_0_17/* type */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* parent */
	, offsetof(StackTraceUtility_t1639_StaticFields, ___projectFolder_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* StackTraceUtility_t1639_FieldInfos[] =
{
	&StackTraceUtility_t1639____projectFolder_0_FieldInfo,
	NULL
};
static Il2CppMethodReference StackTraceUtility_t1639_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool StackTraceUtility_t1639_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType StackTraceUtility_t1639_0_0_0;
extern Il2CppType StackTraceUtility_t1639_1_0_0;
struct StackTraceUtility_t1639;
const Il2CppTypeDefinitionMetadata StackTraceUtility_t1639_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTraceUtility_t1639_VTable/* vtableMethods */
	, StackTraceUtility_t1639_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo StackTraceUtility_t1639_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, StackTraceUtility_t1639_MethodInfos/* methods */
	, NULL/* properties */
	, StackTraceUtility_t1639_FieldInfos/* fields */
	, NULL/* events */
	, &StackTraceUtility_t1639_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StackTraceUtility_t1639_0_0_0/* byval_arg */
	, &StackTraceUtility_t1639_1_0_0/* this_arg */
	, &StackTraceUtility_t1639_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t1639)/* instance_size */
	, sizeof (StackTraceUtility_t1639)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t1639_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// Metadata Definition UnityEngine.UnityException
extern TypeInfo UnityException_t1394_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor()
MethodInfo UnityException__ctor_m7397_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m7397/* method */
	, &UnityException_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityException_t1394_UnityException__ctor_m7398_ParameterInfos[] = 
{
	{"message", 0, 134219431, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String)
MethodInfo UnityException__ctor_m7398_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m7398/* method */
	, &UnityException_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UnityException_t1394_UnityException__ctor_m7398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Exception_t135_0_0_0;
extern Il2CppType Exception_t135_0_0_0;
static ParameterInfo UnityException_t1394_UnityException__ctor_m7399_ParameterInfos[] = 
{
	{"message", 0, 134219432, 0, &String_t_0_0_0},
	{"innerException", 1, 134219433, 0, &Exception_t135_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
MethodInfo UnityException__ctor_m7399_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m7399/* method */
	, &UnityException_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, UnityException_t1394_UnityException__ctor_m7399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo UnityException_t1394_UnityException__ctor_m7400_ParameterInfos[] = 
{
	{"info", 0, 134219434, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134219435, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo UnityException__ctor_m7400_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m7400/* method */
	, &UnityException_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, UnityException_t1394_UnityException__ctor_m7400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityException_t1394_MethodInfos[] =
{
	&UnityException__ctor_m7397_MethodInfo,
	&UnityException__ctor_m7398_MethodInfo,
	&UnityException__ctor_m7399_MethodInfo,
	&UnityException__ctor_m7400_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_32849;
FieldInfo UnityException_t1394____Result_11_FieldInfo = 
{
	"Result"/* name */
	, &Int32_t189_0_0_32849/* type */
	, &UnityException_t1394_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo UnityException_t1394____unityStackTrace_12_FieldInfo = 
{
	"unityStackTrace"/* name */
	, &String_t_0_0_1/* type */
	, &UnityException_t1394_il2cpp_TypeInfo/* parent */
	, offsetof(UnityException_t1394, ___unityStackTrace_12)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityException_t1394_FieldInfos[] =
{
	&UnityException_t1394____Result_11_FieldInfo,
	&UnityException_t1394____unityStackTrace_12_FieldInfo,
	NULL
};
static const int32_t UnityException_t1394____Result_11_DefaultValueData = -2147467261;
static Il2CppFieldDefaultValueEntry UnityException_t1394____Result_11_DefaultValue = 
{
	&UnityException_t1394____Result_11_FieldInfo/* field */
	, { (char*)&UnityException_t1394____Result_11_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UnityException_t1394_FieldDefaultValues[] = 
{
	&UnityException_t1394____Result_11_DefaultValue,
	NULL
};
extern MethodInfo Exception_ToString_m1311_MethodInfo;
extern MethodInfo Exception_GetObjectData_m1312_MethodInfo;
extern MethodInfo Exception_get_InnerException_m1313_MethodInfo;
extern MethodInfo Exception_get_Message_m1314_MethodInfo;
extern MethodInfo Exception_get_Source_m1315_MethodInfo;
extern MethodInfo Exception_get_StackTrace_m1316_MethodInfo;
extern MethodInfo Exception_GetType_m1317_MethodInfo;
static Il2CppMethodReference UnityException_t1394_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Exception_ToString_m1311_MethodInfo,
	&Exception_GetObjectData_m1312_MethodInfo,
	&Exception_get_InnerException_m1313_MethodInfo,
	&Exception_get_Message_m1314_MethodInfo,
	&Exception_get_Source_m1315_MethodInfo,
	&Exception_get_StackTrace_m1316_MethodInfo,
	&Exception_GetObjectData_m1312_MethodInfo,
	&Exception_GetType_m1317_MethodInfo,
};
static bool UnityException_t1394_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ISerializable_t310_0_0_0;
extern Il2CppType _Exception_t324_0_0_0;
static Il2CppInterfaceOffsetPair UnityException_t1394_InterfacesOffsets[] = 
{
	{ &ISerializable_t310_0_0_0, 4},
	{ &_Exception_t324_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityException_t1394_0_0_0;
extern Il2CppType UnityException_t1394_1_0_0;
struct UnityException_t1394;
const Il2CppTypeDefinitionMetadata UnityException_t1394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityException_t1394_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t135_0_0_0/* parent */
	, UnityException_t1394_VTable/* vtableMethods */
	, UnityException_t1394_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityException_t1394_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, UnityException_t1394_MethodInfos/* methods */
	, NULL/* properties */
	, UnityException_t1394_FieldInfos/* fields */
	, NULL/* events */
	, &UnityException_t1394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityException_t1394_0_0_0/* byval_arg */
	, &UnityException_t1394_1_0_0/* this_arg */
	, &UnityException_t1394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UnityException_t1394_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t1394)/* instance_size */
	, sizeof (UnityException_t1394)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern TypeInfo SharedBetweenAnimatorsAttribute_t1640_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
MethodInfo SharedBetweenAnimatorsAttribute__ctor_m7401_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SharedBetweenAnimatorsAttribute__ctor_m7401/* method */
	, &SharedBetweenAnimatorsAttribute_t1640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SharedBetweenAnimatorsAttribute_t1640_MethodInfos[] =
{
	&SharedBetweenAnimatorsAttribute__ctor_m7401_MethodInfo,
	NULL
};
static Il2CppMethodReference SharedBetweenAnimatorsAttribute_t1640_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool SharedBetweenAnimatorsAttribute_t1640_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t1640_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1640_0_0_0;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1640_1_0_0;
struct SharedBetweenAnimatorsAttribute_t1640;
const Il2CppTypeDefinitionMetadata SharedBetweenAnimatorsAttribute_t1640_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SharedBetweenAnimatorsAttribute_t1640_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, SharedBetweenAnimatorsAttribute_t1640_VTable/* vtableMethods */
	, SharedBetweenAnimatorsAttribute_t1640_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SharedBetweenAnimatorsAttribute_t1640_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SharedBetweenAnimatorsAttribute_t1640_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SharedBetweenAnimatorsAttribute_t1640_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 645/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t1640_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t1640_1_0_0/* this_arg */
	, &SharedBetweenAnimatorsAttribute_t1640_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t1640)/* instance_size */
	, sizeof (SharedBetweenAnimatorsAttribute_t1640)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// Metadata Definition UnityEngine.StateMachineBehaviour
extern TypeInfo StateMachineBehaviour_t1641_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
MethodInfo StateMachineBehaviour__ctor_m7402_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StateMachineBehaviour__ctor_m7402/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType AnimatorStateInfo_t1589_0_0_0;
extern Il2CppType AnimatorStateInfo_t1589_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateEnter_m7403_ParameterInfos[] = 
{
	{"animator", 0, 134219436, 0, &Animator_t749_0_0_0},
	{"stateInfo", 1, 134219437, 0, &AnimatorStateInfo_t1589_0_0_0},
	{"layerIndex", 2, 134219438, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateEnter_m7403_MethodInfo = 
{
	"OnStateEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateEnter_m7403/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189/* invoker_method */
	, StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateEnter_m7403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType AnimatorStateInfo_t1589_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateUpdate_m7404_ParameterInfos[] = 
{
	{"animator", 0, 134219439, 0, &Animator_t749_0_0_0},
	{"stateInfo", 1, 134219440, 0, &AnimatorStateInfo_t1589_0_0_0},
	{"layerIndex", 2, 134219441, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateUpdate_m7404_MethodInfo = 
{
	"OnStateUpdate"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateUpdate_m7404/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189/* invoker_method */
	, StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateUpdate_m7404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType AnimatorStateInfo_t1589_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateExit_m7405_ParameterInfos[] = 
{
	{"animator", 0, 134219442, 0, &Animator_t749_0_0_0},
	{"stateInfo", 1, 134219443, 0, &AnimatorStateInfo_t1589_0_0_0},
	{"layerIndex", 2, 134219444, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateExit_m7405_MethodInfo = 
{
	"OnStateExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateExit_m7405/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189/* invoker_method */
	, StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateExit_m7405_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType AnimatorStateInfo_t1589_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateMove_m7406_ParameterInfos[] = 
{
	{"animator", 0, 134219445, 0, &Animator_t749_0_0_0},
	{"stateInfo", 1, 134219446, 0, &AnimatorStateInfo_t1589_0_0_0},
	{"layerIndex", 2, 134219447, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMove_m7406_MethodInfo = 
{
	"OnStateMove"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMove_m7406/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189/* invoker_method */
	, StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateMove_m7406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType AnimatorStateInfo_t1589_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateIK_m7407_ParameterInfos[] = 
{
	{"animator", 0, 134219448, 0, &Animator_t749_0_0_0},
	{"stateInfo", 1, 134219449, 0, &AnimatorStateInfo_t1589_0_0_0},
	{"layerIndex", 2, 134219450, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateIK_m7407_MethodInfo = 
{
	"OnStateIK"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateIK_m7407/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189/* invoker_method */
	, StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateIK_m7407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateMachineEnter_m7408_ParameterInfos[] = 
{
	{"animator", 0, 134219451, 0, &Animator_t749_0_0_0},
	{"stateMachinePathHash", 1, 134219452, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMachineEnter_m7408_MethodInfo = 
{
	"OnStateMachineEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineEnter_m7408/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189/* invoker_method */
	, StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateMachineEnter_m7408_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateMachineExit_m7409_ParameterInfos[] = 
{
	{"animator", 0, 134219453, 0, &Animator_t749_0_0_0},
	{"stateMachinePathHash", 1, 134219454, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMachineExit_m7409_MethodInfo = 
{
	"OnStateMachineExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineExit_m7409/* method */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189/* invoker_method */
	, StateMachineBehaviour_t1641_StateMachineBehaviour_OnStateMachineExit_m7409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* StateMachineBehaviour_t1641_MethodInfos[] =
{
	&StateMachineBehaviour__ctor_m7402_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m7403_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m7404_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m7405_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m7406_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m7407_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m7408_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m7409_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m1199_MethodInfo;
extern MethodInfo Object_GetHashCode_m1200_MethodInfo;
extern MethodInfo Object_ToString_m1201_MethodInfo;
extern MethodInfo StateMachineBehaviour_OnStateEnter_m7403_MethodInfo;
extern MethodInfo StateMachineBehaviour_OnStateUpdate_m7404_MethodInfo;
extern MethodInfo StateMachineBehaviour_OnStateExit_m7405_MethodInfo;
extern MethodInfo StateMachineBehaviour_OnStateMove_m7406_MethodInfo;
extern MethodInfo StateMachineBehaviour_OnStateIK_m7407_MethodInfo;
extern MethodInfo StateMachineBehaviour_OnStateMachineEnter_m7408_MethodInfo;
extern MethodInfo StateMachineBehaviour_OnStateMachineExit_m7409_MethodInfo;
static Il2CppMethodReference StateMachineBehaviour_t1641_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m7403_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m7404_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m7405_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m7406_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m7407_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m7408_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m7409_MethodInfo,
};
static bool StateMachineBehaviour_t1641_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType StateMachineBehaviour_t1641_0_0_0;
extern Il2CppType StateMachineBehaviour_t1641_1_0_0;
extern Il2CppType ScriptableObject_t15_0_0_0;
struct StateMachineBehaviour_t1641;
const Il2CppTypeDefinitionMetadata StateMachineBehaviour_t1641_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t15_0_0_0/* parent */
	, StateMachineBehaviour_t1641_VTable/* vtableMethods */
	, StateMachineBehaviour_t1641_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo StateMachineBehaviour_t1641_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, StateMachineBehaviour_t1641_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &StateMachineBehaviour_t1641_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StateMachineBehaviour_t1641_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t1641_1_0_0/* this_arg */
	, &StateMachineBehaviour_t1641_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t1641)/* instance_size */
	, sizeof (StateMachineBehaviour_t1641)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern TypeInfo DblClickSnapping_t1642_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
static MethodInfo* DblClickSnapping_t1642_MethodInfos[] =
{
	NULL
};
extern Il2CppType Byte_t237_0_0_1542;
FieldInfo DblClickSnapping_t1642____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Byte_t237_0_0_1542/* type */
	, &DblClickSnapping_t1642_il2cpp_TypeInfo/* parent */
	, offsetof(DblClickSnapping_t1642, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1642_0_0_32854;
FieldInfo DblClickSnapping_t1642____WORDS_2_FieldInfo = 
{
	"WORDS"/* name */
	, &DblClickSnapping_t1642_0_0_32854/* type */
	, &DblClickSnapping_t1642_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1642_0_0_32854;
FieldInfo DblClickSnapping_t1642____PARAGRAPHS_3_FieldInfo = 
{
	"PARAGRAPHS"/* name */
	, &DblClickSnapping_t1642_0_0_32854/* type */
	, &DblClickSnapping_t1642_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* DblClickSnapping_t1642_FieldInfos[] =
{
	&DblClickSnapping_t1642____value___1_FieldInfo,
	&DblClickSnapping_t1642____WORDS_2_FieldInfo,
	&DblClickSnapping_t1642____PARAGRAPHS_3_FieldInfo,
	NULL
};
static const uint8_t DblClickSnapping_t1642____WORDS_2_DefaultValueData = 0;
extern Il2CppType Byte_t237_0_0_0;
static Il2CppFieldDefaultValueEntry DblClickSnapping_t1642____WORDS_2_DefaultValue = 
{
	&DblClickSnapping_t1642____WORDS_2_FieldInfo/* field */
	, { (char*)&DblClickSnapping_t1642____WORDS_2_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t DblClickSnapping_t1642____PARAGRAPHS_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry DblClickSnapping_t1642____PARAGRAPHS_3_DefaultValue = 
{
	&DblClickSnapping_t1642____PARAGRAPHS_3_FieldInfo/* field */
	, { (char*)&DblClickSnapping_t1642____PARAGRAPHS_3_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* DblClickSnapping_t1642_FieldDefaultValues[] = 
{
	&DblClickSnapping_t1642____WORDS_2_DefaultValue,
	&DblClickSnapping_t1642____PARAGRAPHS_3_DefaultValue,
	NULL
};
static Il2CppMethodReference DblClickSnapping_t1642_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool DblClickSnapping_t1642_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t1642_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType DblClickSnapping_t1642_0_0_0;
extern Il2CppType DblClickSnapping_t1642_1_0_0;
extern TypeInfo TextEditor_t1396_il2cpp_TypeInfo;
extern Il2CppType TextEditor_t1396_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t237_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DblClickSnapping_t1642_DefinitionMetadata = 
{
	&TextEditor_t1396_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DblClickSnapping_t1642_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, DblClickSnapping_t1642_VTable/* vtableMethods */
	, DblClickSnapping_t1642_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo DblClickSnapping_t1642_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, DblClickSnapping_t1642_MethodInfos/* methods */
	, NULL/* properties */
	, DblClickSnapping_t1642_FieldInfos/* fields */
	, NULL/* events */
	, &Byte_t237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DblClickSnapping_t1642_0_0_0/* byval_arg */
	, &DblClickSnapping_t1642_1_0_0/* this_arg */
	, &DblClickSnapping_t1642_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, DblClickSnapping_t1642_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t1642)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DblClickSnapping_t1642)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern TypeInfo TextEditOp_t1643_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
static MethodInfo* TextEditOp_t1643_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo TextEditOp_t1643____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditOp_t1643, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveLeft_2_FieldInfo = 
{
	"MoveLeft"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveRight_3_FieldInfo = 
{
	"MoveRight"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveUp_4_FieldInfo = 
{
	"MoveUp"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveDown_5_FieldInfo = 
{
	"MoveDown"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveLineStart_6_FieldInfo = 
{
	"MoveLineStart"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveLineEnd_7_FieldInfo = 
{
	"MoveLineEnd"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveTextStart_8_FieldInfo = 
{
	"MoveTextStart"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveTextEnd_9_FieldInfo = 
{
	"MoveTextEnd"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MovePageUp_10_FieldInfo = 
{
	"MovePageUp"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MovePageDown_11_FieldInfo = 
{
	"MovePageDown"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveGraphicalLineStart_12_FieldInfo = 
{
	"MoveGraphicalLineStart"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveGraphicalLineEnd_13_FieldInfo = 
{
	"MoveGraphicalLineEnd"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveWordLeft_14_FieldInfo = 
{
	"MoveWordLeft"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveWordRight_15_FieldInfo = 
{
	"MoveWordRight"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveParagraphForward_16_FieldInfo = 
{
	"MoveParagraphForward"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveParagraphBackward_17_FieldInfo = 
{
	"MoveParagraphBackward"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveToStartOfNextWord_18_FieldInfo = 
{
	"MoveToStartOfNextWord"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____MoveToEndOfPreviousWord_19_FieldInfo = 
{
	"MoveToEndOfPreviousWord"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectLeft_20_FieldInfo = 
{
	"SelectLeft"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectRight_21_FieldInfo = 
{
	"SelectRight"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectUp_22_FieldInfo = 
{
	"SelectUp"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectDown_23_FieldInfo = 
{
	"SelectDown"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectTextStart_24_FieldInfo = 
{
	"SelectTextStart"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectTextEnd_25_FieldInfo = 
{
	"SelectTextEnd"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectPageUp_26_FieldInfo = 
{
	"SelectPageUp"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectPageDown_27_FieldInfo = 
{
	"SelectPageDown"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____ExpandSelectGraphicalLineStart_28_FieldInfo = 
{
	"ExpandSelectGraphicalLineStart"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____ExpandSelectGraphicalLineEnd_29_FieldInfo = 
{
	"ExpandSelectGraphicalLineEnd"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectGraphicalLineStart_30_FieldInfo = 
{
	"SelectGraphicalLineStart"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectGraphicalLineEnd_31_FieldInfo = 
{
	"SelectGraphicalLineEnd"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectWordLeft_32_FieldInfo = 
{
	"SelectWordLeft"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectWordRight_33_FieldInfo = 
{
	"SelectWordRight"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectToEndOfPreviousWord_34_FieldInfo = 
{
	"SelectToEndOfPreviousWord"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectToStartOfNextWord_35_FieldInfo = 
{
	"SelectToStartOfNextWord"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectParagraphBackward_36_FieldInfo = 
{
	"SelectParagraphBackward"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectParagraphForward_37_FieldInfo = 
{
	"SelectParagraphForward"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____Delete_38_FieldInfo = 
{
	"Delete"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____Backspace_39_FieldInfo = 
{
	"Backspace"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____DeleteWordBack_40_FieldInfo = 
{
	"DeleteWordBack"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____DeleteWordForward_41_FieldInfo = 
{
	"DeleteWordForward"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____DeleteLineBack_42_FieldInfo = 
{
	"DeleteLineBack"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____Cut_43_FieldInfo = 
{
	"Cut"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____Copy_44_FieldInfo = 
{
	"Copy"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____Paste_45_FieldInfo = 
{
	"Paste"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectAll_46_FieldInfo = 
{
	"SelectAll"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____SelectNone_47_FieldInfo = 
{
	"SelectNone"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____ScrollStart_48_FieldInfo = 
{
	"ScrollStart"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____ScrollEnd_49_FieldInfo = 
{
	"ScrollEnd"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____ScrollPageUp_50_FieldInfo = 
{
	"ScrollPageUp"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1643_0_0_32854;
FieldInfo TextEditOp_t1643____ScrollPageDown_51_FieldInfo = 
{
	"ScrollPageDown"/* name */
	, &TextEditOp_t1643_0_0_32854/* type */
	, &TextEditOp_t1643_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TextEditOp_t1643_FieldInfos[] =
{
	&TextEditOp_t1643____value___1_FieldInfo,
	&TextEditOp_t1643____MoveLeft_2_FieldInfo,
	&TextEditOp_t1643____MoveRight_3_FieldInfo,
	&TextEditOp_t1643____MoveUp_4_FieldInfo,
	&TextEditOp_t1643____MoveDown_5_FieldInfo,
	&TextEditOp_t1643____MoveLineStart_6_FieldInfo,
	&TextEditOp_t1643____MoveLineEnd_7_FieldInfo,
	&TextEditOp_t1643____MoveTextStart_8_FieldInfo,
	&TextEditOp_t1643____MoveTextEnd_9_FieldInfo,
	&TextEditOp_t1643____MovePageUp_10_FieldInfo,
	&TextEditOp_t1643____MovePageDown_11_FieldInfo,
	&TextEditOp_t1643____MoveGraphicalLineStart_12_FieldInfo,
	&TextEditOp_t1643____MoveGraphicalLineEnd_13_FieldInfo,
	&TextEditOp_t1643____MoveWordLeft_14_FieldInfo,
	&TextEditOp_t1643____MoveWordRight_15_FieldInfo,
	&TextEditOp_t1643____MoveParagraphForward_16_FieldInfo,
	&TextEditOp_t1643____MoveParagraphBackward_17_FieldInfo,
	&TextEditOp_t1643____MoveToStartOfNextWord_18_FieldInfo,
	&TextEditOp_t1643____MoveToEndOfPreviousWord_19_FieldInfo,
	&TextEditOp_t1643____SelectLeft_20_FieldInfo,
	&TextEditOp_t1643____SelectRight_21_FieldInfo,
	&TextEditOp_t1643____SelectUp_22_FieldInfo,
	&TextEditOp_t1643____SelectDown_23_FieldInfo,
	&TextEditOp_t1643____SelectTextStart_24_FieldInfo,
	&TextEditOp_t1643____SelectTextEnd_25_FieldInfo,
	&TextEditOp_t1643____SelectPageUp_26_FieldInfo,
	&TextEditOp_t1643____SelectPageDown_27_FieldInfo,
	&TextEditOp_t1643____ExpandSelectGraphicalLineStart_28_FieldInfo,
	&TextEditOp_t1643____ExpandSelectGraphicalLineEnd_29_FieldInfo,
	&TextEditOp_t1643____SelectGraphicalLineStart_30_FieldInfo,
	&TextEditOp_t1643____SelectGraphicalLineEnd_31_FieldInfo,
	&TextEditOp_t1643____SelectWordLeft_32_FieldInfo,
	&TextEditOp_t1643____SelectWordRight_33_FieldInfo,
	&TextEditOp_t1643____SelectToEndOfPreviousWord_34_FieldInfo,
	&TextEditOp_t1643____SelectToStartOfNextWord_35_FieldInfo,
	&TextEditOp_t1643____SelectParagraphBackward_36_FieldInfo,
	&TextEditOp_t1643____SelectParagraphForward_37_FieldInfo,
	&TextEditOp_t1643____Delete_38_FieldInfo,
	&TextEditOp_t1643____Backspace_39_FieldInfo,
	&TextEditOp_t1643____DeleteWordBack_40_FieldInfo,
	&TextEditOp_t1643____DeleteWordForward_41_FieldInfo,
	&TextEditOp_t1643____DeleteLineBack_42_FieldInfo,
	&TextEditOp_t1643____Cut_43_FieldInfo,
	&TextEditOp_t1643____Copy_44_FieldInfo,
	&TextEditOp_t1643____Paste_45_FieldInfo,
	&TextEditOp_t1643____SelectAll_46_FieldInfo,
	&TextEditOp_t1643____SelectNone_47_FieldInfo,
	&TextEditOp_t1643____ScrollStart_48_FieldInfo,
	&TextEditOp_t1643____ScrollEnd_49_FieldInfo,
	&TextEditOp_t1643____ScrollPageUp_50_FieldInfo,
	&TextEditOp_t1643____ScrollPageDown_51_FieldInfo,
	NULL
};
static const int32_t TextEditOp_t1643____MoveLeft_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveLeft_2_DefaultValue = 
{
	&TextEditOp_t1643____MoveLeft_2_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveLeft_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveRight_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveRight_3_DefaultValue = 
{
	&TextEditOp_t1643____MoveRight_3_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveRight_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveUp_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveUp_4_DefaultValue = 
{
	&TextEditOp_t1643____MoveUp_4_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveUp_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveDown_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveDown_5_DefaultValue = 
{
	&TextEditOp_t1643____MoveDown_5_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveDown_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveLineStart_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveLineStart_6_DefaultValue = 
{
	&TextEditOp_t1643____MoveLineStart_6_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveLineStart_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveLineEnd_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveLineEnd_7_DefaultValue = 
{
	&TextEditOp_t1643____MoveLineEnd_7_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveLineEnd_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveTextStart_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveTextStart_8_DefaultValue = 
{
	&TextEditOp_t1643____MoveTextStart_8_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveTextStart_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveTextEnd_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveTextEnd_9_DefaultValue = 
{
	&TextEditOp_t1643____MoveTextEnd_9_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveTextEnd_9_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MovePageUp_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MovePageUp_10_DefaultValue = 
{
	&TextEditOp_t1643____MovePageUp_10_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MovePageUp_10_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MovePageDown_11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MovePageDown_11_DefaultValue = 
{
	&TextEditOp_t1643____MovePageDown_11_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MovePageDown_11_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveGraphicalLineStart_12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveGraphicalLineStart_12_DefaultValue = 
{
	&TextEditOp_t1643____MoveGraphicalLineStart_12_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveGraphicalLineStart_12_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveGraphicalLineEnd_13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveGraphicalLineEnd_13_DefaultValue = 
{
	&TextEditOp_t1643____MoveGraphicalLineEnd_13_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveGraphicalLineEnd_13_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveWordLeft_14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveWordLeft_14_DefaultValue = 
{
	&TextEditOp_t1643____MoveWordLeft_14_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveWordLeft_14_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveWordRight_15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveWordRight_15_DefaultValue = 
{
	&TextEditOp_t1643____MoveWordRight_15_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveWordRight_15_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveParagraphForward_16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveParagraphForward_16_DefaultValue = 
{
	&TextEditOp_t1643____MoveParagraphForward_16_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveParagraphForward_16_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveParagraphBackward_17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveParagraphBackward_17_DefaultValue = 
{
	&TextEditOp_t1643____MoveParagraphBackward_17_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveParagraphBackward_17_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveToStartOfNextWord_18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveToStartOfNextWord_18_DefaultValue = 
{
	&TextEditOp_t1643____MoveToStartOfNextWord_18_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveToStartOfNextWord_18_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____MoveToEndOfPreviousWord_19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____MoveToEndOfPreviousWord_19_DefaultValue = 
{
	&TextEditOp_t1643____MoveToEndOfPreviousWord_19_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____MoveToEndOfPreviousWord_19_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectLeft_20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectLeft_20_DefaultValue = 
{
	&TextEditOp_t1643____SelectLeft_20_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectLeft_20_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectRight_21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectRight_21_DefaultValue = 
{
	&TextEditOp_t1643____SelectRight_21_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectRight_21_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectUp_22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectUp_22_DefaultValue = 
{
	&TextEditOp_t1643____SelectUp_22_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectUp_22_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectDown_23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectDown_23_DefaultValue = 
{
	&TextEditOp_t1643____SelectDown_23_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectDown_23_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectTextStart_24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectTextStart_24_DefaultValue = 
{
	&TextEditOp_t1643____SelectTextStart_24_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectTextStart_24_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectTextEnd_25_DefaultValueData = 23;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectTextEnd_25_DefaultValue = 
{
	&TextEditOp_t1643____SelectTextEnd_25_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectTextEnd_25_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectPageUp_26_DefaultValueData = 24;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectPageUp_26_DefaultValue = 
{
	&TextEditOp_t1643____SelectPageUp_26_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectPageUp_26_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectPageDown_27_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectPageDown_27_DefaultValue = 
{
	&TextEditOp_t1643____SelectPageDown_27_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectPageDown_27_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____ExpandSelectGraphicalLineStart_28_DefaultValueData = 26;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____ExpandSelectGraphicalLineStart_28_DefaultValue = 
{
	&TextEditOp_t1643____ExpandSelectGraphicalLineStart_28_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____ExpandSelectGraphicalLineStart_28_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____ExpandSelectGraphicalLineEnd_29_DefaultValueData = 27;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____ExpandSelectGraphicalLineEnd_29_DefaultValue = 
{
	&TextEditOp_t1643____ExpandSelectGraphicalLineEnd_29_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____ExpandSelectGraphicalLineEnd_29_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectGraphicalLineStart_30_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectGraphicalLineStart_30_DefaultValue = 
{
	&TextEditOp_t1643____SelectGraphicalLineStart_30_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectGraphicalLineStart_30_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectGraphicalLineEnd_31_DefaultValueData = 29;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectGraphicalLineEnd_31_DefaultValue = 
{
	&TextEditOp_t1643____SelectGraphicalLineEnd_31_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectGraphicalLineEnd_31_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectWordLeft_32_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectWordLeft_32_DefaultValue = 
{
	&TextEditOp_t1643____SelectWordLeft_32_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectWordLeft_32_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectWordRight_33_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectWordRight_33_DefaultValue = 
{
	&TextEditOp_t1643____SelectWordRight_33_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectWordRight_33_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectToEndOfPreviousWord_34_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectToEndOfPreviousWord_34_DefaultValue = 
{
	&TextEditOp_t1643____SelectToEndOfPreviousWord_34_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectToEndOfPreviousWord_34_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectToStartOfNextWord_35_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectToStartOfNextWord_35_DefaultValue = 
{
	&TextEditOp_t1643____SelectToStartOfNextWord_35_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectToStartOfNextWord_35_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectParagraphBackward_36_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectParagraphBackward_36_DefaultValue = 
{
	&TextEditOp_t1643____SelectParagraphBackward_36_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectParagraphBackward_36_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectParagraphForward_37_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectParagraphForward_37_DefaultValue = 
{
	&TextEditOp_t1643____SelectParagraphForward_37_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectParagraphForward_37_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____Delete_38_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____Delete_38_DefaultValue = 
{
	&TextEditOp_t1643____Delete_38_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____Delete_38_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____Backspace_39_DefaultValueData = 37;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____Backspace_39_DefaultValue = 
{
	&TextEditOp_t1643____Backspace_39_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____Backspace_39_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____DeleteWordBack_40_DefaultValueData = 38;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____DeleteWordBack_40_DefaultValue = 
{
	&TextEditOp_t1643____DeleteWordBack_40_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____DeleteWordBack_40_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____DeleteWordForward_41_DefaultValueData = 39;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____DeleteWordForward_41_DefaultValue = 
{
	&TextEditOp_t1643____DeleteWordForward_41_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____DeleteWordForward_41_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____DeleteLineBack_42_DefaultValueData = 40;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____DeleteLineBack_42_DefaultValue = 
{
	&TextEditOp_t1643____DeleteLineBack_42_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____DeleteLineBack_42_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____Cut_43_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____Cut_43_DefaultValue = 
{
	&TextEditOp_t1643____Cut_43_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____Cut_43_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____Copy_44_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____Copy_44_DefaultValue = 
{
	&TextEditOp_t1643____Copy_44_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____Copy_44_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____Paste_45_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____Paste_45_DefaultValue = 
{
	&TextEditOp_t1643____Paste_45_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____Paste_45_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectAll_46_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectAll_46_DefaultValue = 
{
	&TextEditOp_t1643____SelectAll_46_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectAll_46_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____SelectNone_47_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____SelectNone_47_DefaultValue = 
{
	&TextEditOp_t1643____SelectNone_47_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____SelectNone_47_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____ScrollStart_48_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____ScrollStart_48_DefaultValue = 
{
	&TextEditOp_t1643____ScrollStart_48_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____ScrollStart_48_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____ScrollEnd_49_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____ScrollEnd_49_DefaultValue = 
{
	&TextEditOp_t1643____ScrollEnd_49_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____ScrollEnd_49_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____ScrollPageUp_50_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____ScrollPageUp_50_DefaultValue = 
{
	&TextEditOp_t1643____ScrollPageUp_50_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____ScrollPageUp_50_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1643____ScrollPageDown_51_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry TextEditOp_t1643____ScrollPageDown_51_DefaultValue = 
{
	&TextEditOp_t1643____ScrollPageDown_51_FieldInfo/* field */
	, { (char*)&TextEditOp_t1643____ScrollPageDown_51_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TextEditOp_t1643_FieldDefaultValues[] = 
{
	&TextEditOp_t1643____MoveLeft_2_DefaultValue,
	&TextEditOp_t1643____MoveRight_3_DefaultValue,
	&TextEditOp_t1643____MoveUp_4_DefaultValue,
	&TextEditOp_t1643____MoveDown_5_DefaultValue,
	&TextEditOp_t1643____MoveLineStart_6_DefaultValue,
	&TextEditOp_t1643____MoveLineEnd_7_DefaultValue,
	&TextEditOp_t1643____MoveTextStart_8_DefaultValue,
	&TextEditOp_t1643____MoveTextEnd_9_DefaultValue,
	&TextEditOp_t1643____MovePageUp_10_DefaultValue,
	&TextEditOp_t1643____MovePageDown_11_DefaultValue,
	&TextEditOp_t1643____MoveGraphicalLineStart_12_DefaultValue,
	&TextEditOp_t1643____MoveGraphicalLineEnd_13_DefaultValue,
	&TextEditOp_t1643____MoveWordLeft_14_DefaultValue,
	&TextEditOp_t1643____MoveWordRight_15_DefaultValue,
	&TextEditOp_t1643____MoveParagraphForward_16_DefaultValue,
	&TextEditOp_t1643____MoveParagraphBackward_17_DefaultValue,
	&TextEditOp_t1643____MoveToStartOfNextWord_18_DefaultValue,
	&TextEditOp_t1643____MoveToEndOfPreviousWord_19_DefaultValue,
	&TextEditOp_t1643____SelectLeft_20_DefaultValue,
	&TextEditOp_t1643____SelectRight_21_DefaultValue,
	&TextEditOp_t1643____SelectUp_22_DefaultValue,
	&TextEditOp_t1643____SelectDown_23_DefaultValue,
	&TextEditOp_t1643____SelectTextStart_24_DefaultValue,
	&TextEditOp_t1643____SelectTextEnd_25_DefaultValue,
	&TextEditOp_t1643____SelectPageUp_26_DefaultValue,
	&TextEditOp_t1643____SelectPageDown_27_DefaultValue,
	&TextEditOp_t1643____ExpandSelectGraphicalLineStart_28_DefaultValue,
	&TextEditOp_t1643____ExpandSelectGraphicalLineEnd_29_DefaultValue,
	&TextEditOp_t1643____SelectGraphicalLineStart_30_DefaultValue,
	&TextEditOp_t1643____SelectGraphicalLineEnd_31_DefaultValue,
	&TextEditOp_t1643____SelectWordLeft_32_DefaultValue,
	&TextEditOp_t1643____SelectWordRight_33_DefaultValue,
	&TextEditOp_t1643____SelectToEndOfPreviousWord_34_DefaultValue,
	&TextEditOp_t1643____SelectToStartOfNextWord_35_DefaultValue,
	&TextEditOp_t1643____SelectParagraphBackward_36_DefaultValue,
	&TextEditOp_t1643____SelectParagraphForward_37_DefaultValue,
	&TextEditOp_t1643____Delete_38_DefaultValue,
	&TextEditOp_t1643____Backspace_39_DefaultValue,
	&TextEditOp_t1643____DeleteWordBack_40_DefaultValue,
	&TextEditOp_t1643____DeleteWordForward_41_DefaultValue,
	&TextEditOp_t1643____DeleteLineBack_42_DefaultValue,
	&TextEditOp_t1643____Cut_43_DefaultValue,
	&TextEditOp_t1643____Copy_44_DefaultValue,
	&TextEditOp_t1643____Paste_45_DefaultValue,
	&TextEditOp_t1643____SelectAll_46_DefaultValue,
	&TextEditOp_t1643____SelectNone_47_DefaultValue,
	&TextEditOp_t1643____ScrollStart_48_DefaultValue,
	&TextEditOp_t1643____ScrollEnd_49_DefaultValue,
	&TextEditOp_t1643____ScrollPageUp_50_DefaultValue,
	&TextEditOp_t1643____ScrollPageDown_51_DefaultValue,
	NULL
};
static Il2CppMethodReference TextEditOp_t1643_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool TextEditOp_t1643_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextEditOp_t1643_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextEditOp_t1643_0_0_0;
extern Il2CppType TextEditOp_t1643_1_0_0;
const Il2CppTypeDefinitionMetadata TextEditOp_t1643_DefinitionMetadata = 
{
	&TextEditor_t1396_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextEditOp_t1643_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, TextEditOp_t1643_VTable/* vtableMethods */
	, TextEditOp_t1643_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TextEditOp_t1643_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, TextEditOp_t1643_MethodInfos/* methods */
	, NULL/* properties */
	, TextEditOp_t1643_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditOp_t1643_0_0_0/* byval_arg */
	, &TextEditOp_t1643_1_0_0/* this_arg */
	, &TextEditOp_t1643_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TextEditOp_t1643_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t1643)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextEditOp_t1643)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// Metadata Definition UnityEngine.TextEditor
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::.ctor()
MethodInfo TextEditor__ctor_m5980_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextEditor__ctor_m5980/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
MethodInfo TextEditor_ClearCursorPos_m7410_MethodInfo = 
{
	"ClearCursorPos"/* name */
	, (methodPointerType)&TextEditor_ClearCursorPos_m7410/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnFocus()
MethodInfo TextEditor_OnFocus_m5983_MethodInfo = 
{
	"OnFocus"/* name */
	, (methodPointerType)&TextEditor_OnFocus_m5983/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SelectAll()
MethodInfo TextEditor_SelectAll_m7411_MethodInfo = 
{
	"SelectAll"/* name */
	, (methodPointerType)&TextEditor_SelectAll_m7411/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
MethodInfo TextEditor_DeleteSelection_m7412_MethodInfo = 
{
	"DeleteSelection"/* name */
	, (methodPointerType)&TextEditor_DeleteSelection_m7412/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextEditor_t1396_TextEditor_ReplaceSelection_m7413_ParameterInfos[] = 
{
	{"replace", 0, 134219455, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
MethodInfo TextEditor_ReplaceSelection_m7413_MethodInfo = 
{
	"ReplaceSelection"/* name */
	, (methodPointerType)&TextEditor_ReplaceSelection_m7413/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, TextEditor_t1396_TextEditor_ReplaceSelection_m7413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
MethodInfo TextEditor_UpdateScrollOffset_m7414_MethodInfo = 
{
	"UpdateScrollOffset"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffset_m7414/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::Copy()
MethodInfo TextEditor_Copy_m5984_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&TextEditor_Copy_m5984/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextEditor_t1396_TextEditor_ReplaceNewlinesWithSpaces_m7415_ParameterInfos[] = 
{
	{"value", 0, 134219456, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m7415_MethodInfo = 
{
	"ReplaceNewlinesWithSpaces"/* name */
	, (methodPointerType)&TextEditor_ReplaceNewlinesWithSpaces_m7415/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TextEditor_t1396_TextEditor_ReplaceNewlinesWithSpaces_m7415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::Paste()
MethodInfo TextEditor_Paste_m5981_MethodInfo = 
{
	"Paste"/* name */
	, (methodPointerType)&TextEditor_Paste_m5981/* method */
	, &TextEditor_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextEditor_t1396_MethodInfos[] =
{
	&TextEditor__ctor_m5980_MethodInfo,
	&TextEditor_ClearCursorPos_m7410_MethodInfo,
	&TextEditor_OnFocus_m5983_MethodInfo,
	&TextEditor_SelectAll_m7411_MethodInfo,
	&TextEditor_DeleteSelection_m7412_MethodInfo,
	&TextEditor_ReplaceSelection_m7413_MethodInfo,
	&TextEditor_UpdateScrollOffset_m7414_MethodInfo,
	&TextEditor_Copy_m5984_MethodInfo,
	&TextEditor_ReplaceNewlinesWithSpaces_m7415_MethodInfo,
	&TextEditor_Paste_m5981_MethodInfo,
	NULL
};
extern Il2CppType TouchScreenKeyboard_t1262_0_0_6;
FieldInfo TextEditor_t1396____keyboardOnScreen_0_FieldInfo = 
{
	"keyboardOnScreen"/* name */
	, &TouchScreenKeyboard_t1262_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___keyboardOnScreen_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo TextEditor_t1396____pos_1_FieldInfo = 
{
	"pos"/* name */
	, &Int32_t189_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___pos_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo TextEditor_t1396____selectPos_2_FieldInfo = 
{
	"selectPos"/* name */
	, &Int32_t189_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___selectPos_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo TextEditor_t1396____controlID_3_FieldInfo = 
{
	"controlID"/* name */
	, &Int32_t189_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___controlID_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIContent_t986_0_0_6;
FieldInfo TextEditor_t1396____content_4_FieldInfo = 
{
	"content"/* name */
	, &GUIContent_t986_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___content_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo TextEditor_t1396____style_5_FieldInfo = 
{
	"style"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___style_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_6;
FieldInfo TextEditor_t1396____position_6_FieldInfo = 
{
	"position"/* name */
	, &Rect_t738_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___position_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TextEditor_t1396____multiline_7_FieldInfo = 
{
	"multiline"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___multiline_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TextEditor_t1396____hasHorizontalCursorPos_8_FieldInfo = 
{
	"hasHorizontalCursorPos"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___hasHorizontalCursorPos_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TextEditor_t1396____isPasswordField_9_FieldInfo = 
{
	"isPasswordField"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___isPasswordField_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_3;
FieldInfo TextEditor_t1396____m_HasFocus_10_FieldInfo = 
{
	"m_HasFocus"/* name */
	, &Boolean_t203_0_0_3/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___m_HasFocus_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_6;
FieldInfo TextEditor_t1396____scrollOffset_11_FieldInfo = 
{
	"scrollOffset"/* name */
	, &Vector2_t739_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___scrollOffset_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo TextEditor_t1396____m_TextHeightPotentiallyChanged_12_FieldInfo = 
{
	"m_TextHeightPotentiallyChanged"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___m_TextHeightPotentiallyChanged_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_6;
FieldInfo TextEditor_t1396____graphicalCursorPos_13_FieldInfo = 
{
	"graphicalCursorPos"/* name */
	, &Vector2_t739_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___graphicalCursorPos_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_6;
FieldInfo TextEditor_t1396____graphicalSelectCursorPos_14_FieldInfo = 
{
	"graphicalSelectCursorPos"/* name */
	, &Vector2_t739_0_0_6/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___graphicalSelectCursorPos_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo TextEditor_t1396____m_MouseDragSelectsWholeWords_15_FieldInfo = 
{
	"m_MouseDragSelectsWholeWords"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___m_MouseDragSelectsWholeWords_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo TextEditor_t1396____m_DblClickInitPos_16_FieldInfo = 
{
	"m_DblClickInitPos"/* name */
	, &Int32_t189_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___m_DblClickInitPos_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1642_0_0_1;
FieldInfo TextEditor_t1396____m_DblClickSnap_17_FieldInfo = 
{
	"m_DblClickSnap"/* name */
	, &DblClickSnapping_t1642_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___m_DblClickSnap_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo TextEditor_t1396____m_bJustSelected_18_FieldInfo = 
{
	"m_bJustSelected"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___m_bJustSelected_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo TextEditor_t1396____m_iAltCursorPos_19_FieldInfo = 
{
	"m_iAltCursorPos"/* name */
	, &Int32_t189_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___m_iAltCursorPos_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo TextEditor_t1396____oldText_20_FieldInfo = 
{
	"oldText"/* name */
	, &String_t_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___oldText_20)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo TextEditor_t1396____oldPos_21_FieldInfo = 
{
	"oldPos"/* name */
	, &Int32_t189_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___oldPos_21)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo TextEditor_t1396____oldSelectPos_22_FieldInfo = 
{
	"oldSelectPos"/* name */
	, &Int32_t189_0_0_1/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396, ___oldSelectPos_22)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t1644_0_0_17;
FieldInfo TextEditor_t1396____s_Keyactions_23_FieldInfo = 
{
	"s_Keyactions"/* name */
	, &Dictionary_2_t1644_0_0_17/* type */
	, &TextEditor_t1396_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t1396_StaticFields, ___s_Keyactions_23)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TextEditor_t1396_FieldInfos[] =
{
	&TextEditor_t1396____keyboardOnScreen_0_FieldInfo,
	&TextEditor_t1396____pos_1_FieldInfo,
	&TextEditor_t1396____selectPos_2_FieldInfo,
	&TextEditor_t1396____controlID_3_FieldInfo,
	&TextEditor_t1396____content_4_FieldInfo,
	&TextEditor_t1396____style_5_FieldInfo,
	&TextEditor_t1396____position_6_FieldInfo,
	&TextEditor_t1396____multiline_7_FieldInfo,
	&TextEditor_t1396____hasHorizontalCursorPos_8_FieldInfo,
	&TextEditor_t1396____isPasswordField_9_FieldInfo,
	&TextEditor_t1396____m_HasFocus_10_FieldInfo,
	&TextEditor_t1396____scrollOffset_11_FieldInfo,
	&TextEditor_t1396____m_TextHeightPotentiallyChanged_12_FieldInfo,
	&TextEditor_t1396____graphicalCursorPos_13_FieldInfo,
	&TextEditor_t1396____graphicalSelectCursorPos_14_FieldInfo,
	&TextEditor_t1396____m_MouseDragSelectsWholeWords_15_FieldInfo,
	&TextEditor_t1396____m_DblClickInitPos_16_FieldInfo,
	&TextEditor_t1396____m_DblClickSnap_17_FieldInfo,
	&TextEditor_t1396____m_bJustSelected_18_FieldInfo,
	&TextEditor_t1396____m_iAltCursorPos_19_FieldInfo,
	&TextEditor_t1396____oldText_20_FieldInfo,
	&TextEditor_t1396____oldPos_21_FieldInfo,
	&TextEditor_t1396____oldSelectPos_22_FieldInfo,
	&TextEditor_t1396____s_Keyactions_23_FieldInfo,
	NULL
};
static const Il2CppType* TextEditor_t1396_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DblClickSnapping_t1642_0_0_0,
	&TextEditOp_t1643_0_0_0,
};
static Il2CppMethodReference TextEditor_t1396_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TextEditor_t1396_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextEditor_t1396_1_0_0;
struct TextEditor_t1396;
const Il2CppTypeDefinitionMetadata TextEditor_t1396_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextEditor_t1396_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextEditor_t1396_VTable/* vtableMethods */
	, TextEditor_t1396_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TextEditor_t1396_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, TextEditor_t1396_MethodInfos/* methods */
	, NULL/* properties */
	, TextEditor_t1396_FieldInfos/* fields */
	, NULL/* events */
	, &TextEditor_t1396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditor_t1396_0_0_0/* byval_arg */
	, &TextEditor_t1396_1_0_0/* this_arg */
	, &TextEditor_t1396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t1396)/* instance_size */
	, sizeof (TextEditor_t1396)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t1396_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// Metadata Definition UnityEngine.TextGenerationSettings
extern TypeInfo TextGenerationSettings_t1364_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
extern Il2CppType Color_t747_0_0_0;
extern Il2CppType Color_t747_0_0_0;
extern Il2CppType Color_t747_0_0_0;
static ParameterInfo TextGenerationSettings_t1364_TextGenerationSettings_CompareColors_m7416_ParameterInfos[] = 
{
	{"left", 0, 134219457, 0, &Color_t747_0_0_0},
	{"right", 1, 134219458, 0, &Color_t747_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Color_t747_Color_t747 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
MethodInfo TextGenerationSettings_CompareColors_m7416_MethodInfo = 
{
	"CompareColors"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareColors_m7416/* method */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Color_t747_Color_t747/* invoker_method */
	, TextGenerationSettings_t1364_TextGenerationSettings_CompareColors_m7416_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern Il2CppType Vector2_t739_0_0_0;
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo TextGenerationSettings_t1364_TextGenerationSettings_CompareVector2_m7417_ParameterInfos[] = 
{
	{"left", 0, 134219459, 0, &Vector2_t739_0_0_0},
	{"right", 1, 134219460, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Vector2_t739_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
MethodInfo TextGenerationSettings_CompareVector2_m7417_MethodInfo = 
{
	"CompareVector2"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareVector2_m7417/* method */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Vector2_t739_Vector2_t739/* invoker_method */
	, TextGenerationSettings_t1364_TextGenerationSettings_CompareVector2_m7417_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextGenerationSettings_t1364_0_0_0;
extern Il2CppType TextGenerationSettings_t1364_0_0_0;
static ParameterInfo TextGenerationSettings_t1364_TextGenerationSettings_Equals_m7418_ParameterInfos[] = 
{
	{"other", 0, 134219461, 0, &TextGenerationSettings_t1364_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_TextGenerationSettings_t1364 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
MethodInfo TextGenerationSettings_Equals_m7418_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TextGenerationSettings_Equals_m7418/* method */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_TextGenerationSettings_t1364/* invoker_method */
	, TextGenerationSettings_t1364_TextGenerationSettings_Equals_m7418_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextGenerationSettings_t1364_MethodInfos[] =
{
	&TextGenerationSettings_CompareColors_m7416_MethodInfo,
	&TextGenerationSettings_CompareVector2_m7417_MethodInfo,
	&TextGenerationSettings_Equals_m7418_MethodInfo,
	NULL
};
extern Il2CppType Font_t1222_0_0_6;
FieldInfo TextGenerationSettings_t1364____font_0_FieldInfo = 
{
	"font"/* name */
	, &Font_t1222_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___font_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Color_t747_0_0_6;
FieldInfo TextGenerationSettings_t1364____color_1_FieldInfo = 
{
	"color"/* name */
	, &Color_t747_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___color_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo TextGenerationSettings_t1364____fontSize_2_FieldInfo = 
{
	"fontSize"/* name */
	, &Int32_t189_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___fontSize_2) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo TextGenerationSettings_t1364____lineSpacing_3_FieldInfo = 
{
	"lineSpacing"/* name */
	, &Single_t202_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___lineSpacing_3) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TextGenerationSettings_t1364____richText_4_FieldInfo = 
{
	"richText"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___richText_4) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FontStyle_t1462_0_0_6;
FieldInfo TextGenerationSettings_t1364____fontStyle_5_FieldInfo = 
{
	"fontStyle"/* name */
	, &FontStyle_t1462_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___fontStyle_5) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextAnchor_t1410_0_0_6;
FieldInfo TextGenerationSettings_t1364____textAnchor_6_FieldInfo = 
{
	"textAnchor"/* name */
	, &TextAnchor_t1410_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___textAnchor_6) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TextGenerationSettings_t1364____resizeTextForBestFit_7_FieldInfo = 
{
	"resizeTextForBestFit"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___resizeTextForBestFit_7) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo TextGenerationSettings_t1364____resizeTextMinSize_8_FieldInfo = 
{
	"resizeTextMinSize"/* name */
	, &Int32_t189_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___resizeTextMinSize_8) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo TextGenerationSettings_t1364____resizeTextMaxSize_9_FieldInfo = 
{
	"resizeTextMaxSize"/* name */
	, &Int32_t189_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___resizeTextMaxSize_9) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TextGenerationSettings_t1364____updateBounds_10_FieldInfo = 
{
	"updateBounds"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___updateBounds_10) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType VerticalWrapMode_t1464_0_0_6;
FieldInfo TextGenerationSettings_t1364____verticalOverflow_11_FieldInfo = 
{
	"verticalOverflow"/* name */
	, &VerticalWrapMode_t1464_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___verticalOverflow_11) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType HorizontalWrapMode_t1463_0_0_6;
FieldInfo TextGenerationSettings_t1364____horizontalOverflow_12_FieldInfo = 
{
	"horizontalOverflow"/* name */
	, &HorizontalWrapMode_t1463_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___horizontalOverflow_12) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_6;
FieldInfo TextGenerationSettings_t1364____generationExtents_13_FieldInfo = 
{
	"generationExtents"/* name */
	, &Vector2_t739_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___generationExtents_13) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_6;
FieldInfo TextGenerationSettings_t1364____pivot_14_FieldInfo = 
{
	"pivot"/* name */
	, &Vector2_t739_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___pivot_14) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TextGenerationSettings_t1364____generateOutOfBounds_15_FieldInfo = 
{
	"generateOutOfBounds"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t1364, ___generateOutOfBounds_15) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TextGenerationSettings_t1364_FieldInfos[] =
{
	&TextGenerationSettings_t1364____font_0_FieldInfo,
	&TextGenerationSettings_t1364____color_1_FieldInfo,
	&TextGenerationSettings_t1364____fontSize_2_FieldInfo,
	&TextGenerationSettings_t1364____lineSpacing_3_FieldInfo,
	&TextGenerationSettings_t1364____richText_4_FieldInfo,
	&TextGenerationSettings_t1364____fontStyle_5_FieldInfo,
	&TextGenerationSettings_t1364____textAnchor_6_FieldInfo,
	&TextGenerationSettings_t1364____resizeTextForBestFit_7_FieldInfo,
	&TextGenerationSettings_t1364____resizeTextMinSize_8_FieldInfo,
	&TextGenerationSettings_t1364____resizeTextMaxSize_9_FieldInfo,
	&TextGenerationSettings_t1364____updateBounds_10_FieldInfo,
	&TextGenerationSettings_t1364____verticalOverflow_11_FieldInfo,
	&TextGenerationSettings_t1364____horizontalOverflow_12_FieldInfo,
	&TextGenerationSettings_t1364____generationExtents_13_FieldInfo,
	&TextGenerationSettings_t1364____pivot_14_FieldInfo,
	&TextGenerationSettings_t1364____generateOutOfBounds_15_FieldInfo,
	NULL
};
static Il2CppMethodReference TextGenerationSettings_t1364_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool TextGenerationSettings_t1364_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextGenerationSettings_t1364_1_0_0;
const Il2CppTypeDefinitionMetadata TextGenerationSettings_t1364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, TextGenerationSettings_t1364_VTable/* vtableMethods */
	, TextGenerationSettings_t1364_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TextGenerationSettings_t1364_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, TextGenerationSettings_t1364_MethodInfos/* methods */
	, NULL/* properties */
	, TextGenerationSettings_t1364_FieldInfos/* fields */
	, NULL/* events */
	, &TextGenerationSettings_t1364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerationSettings_t1364_0_0_0/* byval_arg */
	, &TextGenerationSettings_t1364_1_0_0/* this_arg */
	, &TextGenerationSettings_t1364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t1364)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextGenerationSettings_t1364)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// Metadata Definition UnityEngine.TrackedReference
extern TypeInfo TrackedReference_t1595_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
static ParameterInfo TrackedReference_t1595_TrackedReference_Equals_m7419_ParameterInfos[] = 
{
	{"o", 0, 134219462, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
MethodInfo TrackedReference_Equals_m7419_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TrackedReference_Equals_m7419/* method */
	, &TrackedReference_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, TrackedReference_t1595_TrackedReference_Equals_m7419_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
MethodInfo TrackedReference_GetHashCode_m7420_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TrackedReference_GetHashCode_m7420/* method */
	, &TrackedReference_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TrackedReference_t1595_0_0_0;
extern Il2CppType TrackedReference_t1595_0_0_0;
extern Il2CppType TrackedReference_t1595_0_0_0;
static ParameterInfo TrackedReference_t1595_TrackedReference_op_Equality_m7421_ParameterInfos[] = 
{
	{"x", 0, 134219463, 0, &TrackedReference_t1595_0_0_0},
	{"y", 1, 134219464, 0, &TrackedReference_t1595_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
MethodInfo TrackedReference_op_Equality_m7421_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TrackedReference_op_Equality_m7421/* method */
	, &TrackedReference_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, TrackedReference_t1595_TrackedReference_op_Equality_m7421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TrackedReference_t1595_MethodInfos[] =
{
	&TrackedReference_Equals_m7419_MethodInfo,
	&TrackedReference_GetHashCode_m7420_MethodInfo,
	&TrackedReference_op_Equality_m7421_MethodInfo,
	NULL
};
extern Il2CppType IntPtr_t_0_0_3;
FieldInfo TrackedReference_t1595____m_Ptr_0_FieldInfo = 
{
	"m_Ptr"/* name */
	, &IntPtr_t_0_0_3/* type */
	, &TrackedReference_t1595_il2cpp_TypeInfo/* parent */
	, offsetof(TrackedReference_t1595, ___m_Ptr_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TrackedReference_t1595_FieldInfos[] =
{
	&TrackedReference_t1595____m_Ptr_0_FieldInfo,
	NULL
};
extern MethodInfo TrackedReference_Equals_m7419_MethodInfo;
extern MethodInfo TrackedReference_GetHashCode_m7420_MethodInfo;
static Il2CppMethodReference TrackedReference_t1595_VTable[] =
{
	&TrackedReference_Equals_m7419_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&TrackedReference_GetHashCode_m7420_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TrackedReference_t1595_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TrackedReference_t1595_1_0_0;
struct TrackedReference_t1595;
const Il2CppTypeDefinitionMetadata TrackedReference_t1595_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackedReference_t1595_VTable/* vtableMethods */
	, TrackedReference_t1595_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TrackedReference_t1595_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, TrackedReference_t1595_MethodInfos/* methods */
	, NULL/* properties */
	, TrackedReference_t1595_FieldInfos/* fields */
	, NULL/* events */
	, &TrackedReference_t1595_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrackedReference_t1595_0_0_0/* byval_arg */
	, &TrackedReference_t1595_1_0_0/* this_arg */
	, &TrackedReference_t1595_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t1595_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t1595_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t1595_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t1595)/* instance_size */
	, sizeof (TrackedReference_t1595)/* actualSize */
	, 0/* element_size */
	, sizeof(TrackedReference_t1595_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern TypeInfo PersistentListenerMode_t1645_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
static MethodInfo* PersistentListenerMode_t1645_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo PersistentListenerMode_t1645____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentListenerMode_t1645, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_32854;
FieldInfo PersistentListenerMode_t1645____EventDefined_2_FieldInfo = 
{
	"EventDefined"/* name */
	, &PersistentListenerMode_t1645_0_0_32854/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_32854;
FieldInfo PersistentListenerMode_t1645____Void_3_FieldInfo = 
{
	"Void"/* name */
	, &PersistentListenerMode_t1645_0_0_32854/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_32854;
FieldInfo PersistentListenerMode_t1645____Object_4_FieldInfo = 
{
	"Object"/* name */
	, &PersistentListenerMode_t1645_0_0_32854/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_32854;
FieldInfo PersistentListenerMode_t1645____Int_5_FieldInfo = 
{
	"Int"/* name */
	, &PersistentListenerMode_t1645_0_0_32854/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_32854;
FieldInfo PersistentListenerMode_t1645____Float_6_FieldInfo = 
{
	"Float"/* name */
	, &PersistentListenerMode_t1645_0_0_32854/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_32854;
FieldInfo PersistentListenerMode_t1645____String_7_FieldInfo = 
{
	"String"/* name */
	, &PersistentListenerMode_t1645_0_0_32854/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_32854;
FieldInfo PersistentListenerMode_t1645____Bool_8_FieldInfo = 
{
	"Bool"/* name */
	, &PersistentListenerMode_t1645_0_0_32854/* type */
	, &PersistentListenerMode_t1645_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* PersistentListenerMode_t1645_FieldInfos[] =
{
	&PersistentListenerMode_t1645____value___1_FieldInfo,
	&PersistentListenerMode_t1645____EventDefined_2_FieldInfo,
	&PersistentListenerMode_t1645____Void_3_FieldInfo,
	&PersistentListenerMode_t1645____Object_4_FieldInfo,
	&PersistentListenerMode_t1645____Int_5_FieldInfo,
	&PersistentListenerMode_t1645____Float_6_FieldInfo,
	&PersistentListenerMode_t1645____String_7_FieldInfo,
	&PersistentListenerMode_t1645____Bool_8_FieldInfo,
	NULL
};
static const int32_t PersistentListenerMode_t1645____EventDefined_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1645____EventDefined_2_DefaultValue = 
{
	&PersistentListenerMode_t1645____EventDefined_2_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1645____EventDefined_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1645____Void_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1645____Void_3_DefaultValue = 
{
	&PersistentListenerMode_t1645____Void_3_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1645____Void_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1645____Object_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1645____Object_4_DefaultValue = 
{
	&PersistentListenerMode_t1645____Object_4_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1645____Object_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1645____Int_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1645____Int_5_DefaultValue = 
{
	&PersistentListenerMode_t1645____Int_5_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1645____Int_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1645____Float_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1645____Float_6_DefaultValue = 
{
	&PersistentListenerMode_t1645____Float_6_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1645____Float_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1645____String_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1645____String_7_DefaultValue = 
{
	&PersistentListenerMode_t1645____String_7_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1645____String_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1645____Bool_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1645____Bool_8_DefaultValue = 
{
	&PersistentListenerMode_t1645____Bool_8_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1645____Bool_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* PersistentListenerMode_t1645_FieldDefaultValues[] = 
{
	&PersistentListenerMode_t1645____EventDefined_2_DefaultValue,
	&PersistentListenerMode_t1645____Void_3_DefaultValue,
	&PersistentListenerMode_t1645____Object_4_DefaultValue,
	&PersistentListenerMode_t1645____Int_5_DefaultValue,
	&PersistentListenerMode_t1645____Float_6_DefaultValue,
	&PersistentListenerMode_t1645____String_7_DefaultValue,
	&PersistentListenerMode_t1645____Bool_8_DefaultValue,
	NULL
};
static Il2CppMethodReference PersistentListenerMode_t1645_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool PersistentListenerMode_t1645_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t1645_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentListenerMode_t1645_0_0_0;
extern Il2CppType PersistentListenerMode_t1645_1_0_0;
const Il2CppTypeDefinitionMetadata PersistentListenerMode_t1645_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PersistentListenerMode_t1645_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, PersistentListenerMode_t1645_VTable/* vtableMethods */
	, PersistentListenerMode_t1645_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PersistentListenerMode_t1645_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentListenerMode_t1645_MethodInfos/* methods */
	, NULL/* properties */
	, PersistentListenerMode_t1645_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentListenerMode_t1645_0_0_0/* byval_arg */
	, &PersistentListenerMode_t1645_1_0_0/* this_arg */
	, &PersistentListenerMode_t1645_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, PersistentListenerMode_t1645_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t1645)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PersistentListenerMode_t1645)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// Metadata Definition UnityEngine.Events.ArgumentCache
extern TypeInfo ArgumentCache_t1646_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
MethodInfo ArgumentCache__ctor_m7422_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgumentCache__ctor_m7422/* method */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t187_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
MethodInfo ArgumentCache_get_unityObjectArgument_m7423_MethodInfo = 
{
	"get_unityObjectArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgument_m7423/* method */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Object_t187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7424_MethodInfo = 
{
	"get_unityObjectArgumentAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7424/* method */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
MethodInfo ArgumentCache_get_intArgument_m7425_MethodInfo = 
{
	"get_intArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_intArgument_m7425/* method */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
MethodInfo ArgumentCache_get_floatArgument_m7426_MethodInfo = 
{
	"get_floatArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_floatArgument_m7426/* method */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
MethodInfo ArgumentCache_get_stringArgument_m7427_MethodInfo = 
{
	"get_stringArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_stringArgument_m7427/* method */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
MethodInfo ArgumentCache_get_boolArgument_m7428_MethodInfo = 
{
	"get_boolArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_boolArgument_m7428/* method */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ArgumentCache_t1646_MethodInfos[] =
{
	&ArgumentCache__ctor_m7422_MethodInfo,
	&ArgumentCache_get_unityObjectArgument_m7423_MethodInfo,
	&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7424_MethodInfo,
	&ArgumentCache_get_intArgument_m7425_MethodInfo,
	&ArgumentCache_get_floatArgument_m7426_MethodInfo,
	&ArgumentCache_get_stringArgument_m7427_MethodInfo,
	&ArgumentCache_get_boolArgument_m7428_MethodInfo,
	NULL
};
extern Il2CppType Object_t187_0_0_1;
FieldInfo ArgumentCache_t1646____m_ObjectArgument_0_FieldInfo = 
{
	"m_ObjectArgument"/* name */
	, &Object_t187_0_0_1/* type */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1646, ___m_ObjectArgument_0)/* offset */
	, 646/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ArgumentCache_t1646____m_ObjectArgumentAssemblyTypeName_1_FieldInfo = 
{
	"m_ObjectArgumentAssemblyTypeName"/* name */
	, &String_t_0_0_1/* type */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1646, ___m_ObjectArgumentAssemblyTypeName_1)/* offset */
	, 647/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo ArgumentCache_t1646____m_IntArgument_2_FieldInfo = 
{
	"m_IntArgument"/* name */
	, &Int32_t189_0_0_1/* type */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1646, ___m_IntArgument_2)/* offset */
	, 648/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo ArgumentCache_t1646____m_FloatArgument_3_FieldInfo = 
{
	"m_FloatArgument"/* name */
	, &Single_t202_0_0_1/* type */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1646, ___m_FloatArgument_3)/* offset */
	, 649/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ArgumentCache_t1646____m_StringArgument_4_FieldInfo = 
{
	"m_StringArgument"/* name */
	, &String_t_0_0_1/* type */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1646, ___m_StringArgument_4)/* offset */
	, 650/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ArgumentCache_t1646____m_BoolArgument_5_FieldInfo = 
{
	"m_BoolArgument"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1646, ___m_BoolArgument_5)/* offset */
	, 651/* custom_attributes_cache */

};
static FieldInfo* ArgumentCache_t1646_FieldInfos[] =
{
	&ArgumentCache_t1646____m_ObjectArgument_0_FieldInfo,
	&ArgumentCache_t1646____m_ObjectArgumentAssemblyTypeName_1_FieldInfo,
	&ArgumentCache_t1646____m_IntArgument_2_FieldInfo,
	&ArgumentCache_t1646____m_FloatArgument_3_FieldInfo,
	&ArgumentCache_t1646____m_StringArgument_4_FieldInfo,
	&ArgumentCache_t1646____m_BoolArgument_5_FieldInfo,
	NULL
};
extern MethodInfo ArgumentCache_get_unityObjectArgument_m7423_MethodInfo;
static PropertyInfo ArgumentCache_t1646____unityObjectArgument_PropertyInfo = 
{
	&ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, "unityObjectArgument"/* name */
	, &ArgumentCache_get_unityObjectArgument_m7423_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7424_MethodInfo;
static PropertyInfo ArgumentCache_t1646____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, "unityObjectArgumentAssemblyTypeName"/* name */
	, &ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7424_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ArgumentCache_get_intArgument_m7425_MethodInfo;
static PropertyInfo ArgumentCache_t1646____intArgument_PropertyInfo = 
{
	&ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, "intArgument"/* name */
	, &ArgumentCache_get_intArgument_m7425_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ArgumentCache_get_floatArgument_m7426_MethodInfo;
static PropertyInfo ArgumentCache_t1646____floatArgument_PropertyInfo = 
{
	&ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, "floatArgument"/* name */
	, &ArgumentCache_get_floatArgument_m7426_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ArgumentCache_get_stringArgument_m7427_MethodInfo;
static PropertyInfo ArgumentCache_t1646____stringArgument_PropertyInfo = 
{
	&ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, "stringArgument"/* name */
	, &ArgumentCache_get_stringArgument_m7427_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ArgumentCache_get_boolArgument_m7428_MethodInfo;
static PropertyInfo ArgumentCache_t1646____boolArgument_PropertyInfo = 
{
	&ArgumentCache_t1646_il2cpp_TypeInfo/* parent */
	, "boolArgument"/* name */
	, &ArgumentCache_get_boolArgument_m7428_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ArgumentCache_t1646_PropertyInfos[] =
{
	&ArgumentCache_t1646____unityObjectArgument_PropertyInfo,
	&ArgumentCache_t1646____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&ArgumentCache_t1646____intArgument_PropertyInfo,
	&ArgumentCache_t1646____floatArgument_PropertyInfo,
	&ArgumentCache_t1646____stringArgument_PropertyInfo,
	&ArgumentCache_t1646____boolArgument_PropertyInfo,
	NULL
};
static Il2CppMethodReference ArgumentCache_t1646_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ArgumentCache_t1646_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ArgumentCache_t1646_0_0_0;
extern Il2CppType ArgumentCache_t1646_1_0_0;
struct ArgumentCache_t1646;
const Il2CppTypeDefinitionMetadata ArgumentCache_t1646_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgumentCache_t1646_VTable/* vtableMethods */
	, ArgumentCache_t1646_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ArgumentCache_t1646_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, ArgumentCache_t1646_MethodInfos/* methods */
	, ArgumentCache_t1646_PropertyInfos/* properties */
	, ArgumentCache_t1646_FieldInfos/* fields */
	, NULL/* events */
	, &ArgumentCache_t1646_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgumentCache_t1646_0_0_0/* byval_arg */
	, &ArgumentCache_t1646_1_0_0/* this_arg */
	, &ArgumentCache_t1646_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t1646)/* instance_size */
	, sizeof (ArgumentCache_t1646)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern TypeInfo BaseInvokableCall_t1647_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
MethodInfo BaseInvokableCall__ctor_m7429_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m7429/* method */
	, &BaseInvokableCall_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo BaseInvokableCall_t1647_BaseInvokableCall__ctor_m7430_ParameterInfos[] = 
{
	{"target", 0, 134219465, 0, &Object_t_0_0_0},
	{"function", 1, 134219466, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo BaseInvokableCall__ctor_m7430_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m7430/* method */
	, &BaseInvokableCall_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1647_BaseInvokableCall__ctor_m7430_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo BaseInvokableCall_t1647_BaseInvokableCall_Invoke_m7592_ParameterInfos[] = 
{
	{"args", 0, 134219467, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
MethodInfo BaseInvokableCall_Invoke_m7592_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, BaseInvokableCall_t1647_BaseInvokableCall_Invoke_m7592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo BaseInvokableCall_t1647_BaseInvokableCall_ThrowOnInvalidArg_m7593_ParameterInfos[] = 
{
	{"arg", 0, 134219468, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m7593_Il2CppGenericContainer;
extern TypeInfo BaseInvokableCall_ThrowOnInvalidArg_m7593_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull BaseInvokableCall_ThrowOnInvalidArg_m7593_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &BaseInvokableCall_ThrowOnInvalidArg_m7593_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* BaseInvokableCall_ThrowOnInvalidArg_m7593_Il2CppGenericParametersArray[1] = 
{
	&BaseInvokableCall_ThrowOnInvalidArg_m7593_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m7593_MethodInfo;
Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m7593_Il2CppGenericContainer = { { NULL, NULL }, NULL, &BaseInvokableCall_ThrowOnInvalidArg_m7593_MethodInfo, 1, 1, BaseInvokableCall_ThrowOnInvalidArg_m7593_Il2CppGenericParametersArray };
extern Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m7593_gp_0_0_0_0;
static Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m7593_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &BaseInvokableCall_ThrowOnInvalidArg_m7593_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, &BaseInvokableCall_ThrowOnInvalidArg_m7593_gp_0_0_0_0 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m7593_MethodInfo = 
{
	"ThrowOnInvalidArg"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseInvokableCall_t1647_BaseInvokableCall_ThrowOnInvalidArg_m7593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1618/* token */
	, BaseInvokableCall_ThrowOnInvalidArg_m7593_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &BaseInvokableCall_ThrowOnInvalidArg_m7593_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType Delegate_t211_0_0_0;
extern Il2CppType Delegate_t211_0_0_0;
static ParameterInfo BaseInvokableCall_t1647_BaseInvokableCall_AllowInvoke_m7431_ParameterInfos[] = 
{
	{"delegate", 0, 134219469, 0, &Delegate_t211_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
MethodInfo BaseInvokableCall_AllowInvoke_m7431_MethodInfo = 
{
	"AllowInvoke"/* name */
	, (methodPointerType)&BaseInvokableCall_AllowInvoke_m7431/* method */
	, &BaseInvokableCall_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, BaseInvokableCall_t1647_BaseInvokableCall_AllowInvoke_m7431_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo BaseInvokableCall_t1647_BaseInvokableCall_Find_m7594_ParameterInfos[] = 
{
	{"targetObj", 0, 134219470, 0, &Object_t_0_0_0},
	{"method", 1, 134219471, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo BaseInvokableCall_Find_m7594_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1647_BaseInvokableCall_Find_m7594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BaseInvokableCall_t1647_MethodInfos[] =
{
	&BaseInvokableCall__ctor_m7429_MethodInfo,
	&BaseInvokableCall__ctor_m7430_MethodInfo,
	&BaseInvokableCall_Invoke_m7592_MethodInfo,
	&BaseInvokableCall_ThrowOnInvalidArg_m7593_MethodInfo,
	&BaseInvokableCall_AllowInvoke_m7431_MethodInfo,
	&BaseInvokableCall_Find_m7594_MethodInfo,
	NULL
};
static Il2CppMethodReference BaseInvokableCall_t1647_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	NULL,
	NULL,
};
static bool BaseInvokableCall_t1647_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
extern Il2CppType BaseInvokableCall_t1647_1_0_0;
struct BaseInvokableCall_t1647;
const Il2CppTypeDefinitionMetadata BaseInvokableCall_t1647_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseInvokableCall_t1647_VTable/* vtableMethods */
	, BaseInvokableCall_t1647_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo BaseInvokableCall_t1647_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, BaseInvokableCall_t1647_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1647_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseInvokableCall_t1647_0_0_0/* byval_arg */
	, &BaseInvokableCall_t1647_1_0_0/* this_arg */
	, &BaseInvokableCall_t1647_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t1647)/* instance_size */
	, sizeof (BaseInvokableCall_t1647)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
// Metadata Definition UnityEngine.Events.InvokableCall
extern TypeInfo InvokableCall_t1648_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_t1648_InvokableCall__ctor_m7432_ParameterInfos[] = 
{
	{"target", 0, 134219472, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219473, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall__ctor_m7432_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m7432/* method */
	, &InvokableCall_t1648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1648_InvokableCall__ctor_m7432_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo InvokableCall_t1648_InvokableCall_Invoke_m7433_ParameterInfos[] = 
{
	{"args", 0, 134219474, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
MethodInfo InvokableCall_Invoke_m7433_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_Invoke_m7433/* method */
	, &InvokableCall_t1648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InvokableCall_t1648_InvokableCall_Invoke_m7433_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_t1648_InvokableCall_Find_m7434_ParameterInfos[] = 
{
	{"targetObj", 0, 134219475, 0, &Object_t_0_0_0},
	{"method", 1, 134219476, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_Find_m7434_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_Find_m7434/* method */
	, &InvokableCall_t1648_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1648_InvokableCall_Find_m7434_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_t1648_MethodInfos[] =
{
	&InvokableCall__ctor_m7432_MethodInfo,
	&InvokableCall_Invoke_m7433_MethodInfo,
	&InvokableCall_Find_m7434_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_t1230_0_0_1;
FieldInfo InvokableCall_t1648____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_t1230_0_0_1/* type */
	, &InvokableCall_t1648_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_t1648, ___Delegate_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_t1648_FieldInfos[] =
{
	&InvokableCall_t1648____Delegate_0_FieldInfo,
	NULL
};
extern MethodInfo InvokableCall_Invoke_m7433_MethodInfo;
extern MethodInfo InvokableCall_Find_m7434_MethodInfo;
static Il2CppMethodReference InvokableCall_t1648_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&InvokableCall_Invoke_m7433_MethodInfo,
	&InvokableCall_Find_m7434_MethodInfo,
};
static bool InvokableCall_t1648_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_t1648_0_0_0;
extern Il2CppType InvokableCall_t1648_1_0_0;
struct InvokableCall_t1648;
const Il2CppTypeDefinitionMetadata InvokableCall_t1648_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1647_0_0_0/* parent */
	, InvokableCall_t1648_VTable/* vtableMethods */
	, InvokableCall_t1648_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo InvokableCall_t1648_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_t1648_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_t1648_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_t1648_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_t1648_0_0_0/* byval_arg */
	, &InvokableCall_t1648_1_0_0/* this_arg */
	, &InvokableCall_t1648_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t1648)/* instance_size */
	, sizeof (InvokableCall_t1648)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`1
extern TypeInfo InvokableCall_1_t1708_il2cpp_TypeInfo;
extern Il2CppGenericContainer InvokableCall_1_t1708_Il2CppGenericContainer;
extern TypeInfo InvokableCall_1_t1708_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_1_t1708_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_1_t1708_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_1_t1708_Il2CppGenericParametersArray[1] = 
{
	&InvokableCall_1_t1708_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer InvokableCall_1_t1708_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_1_t1708_il2cpp_TypeInfo, 1, 0, InvokableCall_1_t1708_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_1_t1708_InvokableCall_1__ctor_m7595_ParameterInfos[] = 
{
	{"target", 0, 134219477, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219478, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m7595_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1708_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1708_InvokableCall_1__ctor_m7595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1759_0_0_0;
extern Il2CppType UnityAction_1_t1759_0_0_0;
static ParameterInfo InvokableCall_1_t1708_InvokableCall_1__ctor_m7596_ParameterInfos[] = 
{
	{"callback", 0, 134219479, 0, &UnityAction_1_t1759_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m7596_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1708_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1708_InvokableCall_1__ctor_m7596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo InvokableCall_1_t1708_InvokableCall_1_Invoke_m7597_ParameterInfos[] = 
{
	{"args", 0, 134219480, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m7597_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1708_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1708_InvokableCall_1_Invoke_m7597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_1_t1708_InvokableCall_1_Find_m7598_ParameterInfos[] = 
{
	{"targetObj", 0, 134219481, 0, &Object_t_0_0_0},
	{"method", 1, 134219482, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m7598_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1708_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1708_InvokableCall_1_Find_m7598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_1_t1708_MethodInfos[] =
{
	&InvokableCall_1__ctor_m7595_MethodInfo,
	&InvokableCall_1__ctor_m7596_MethodInfo,
	&InvokableCall_1_Invoke_m7597_MethodInfo,
	&InvokableCall_1_Find_m7598_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_1_t1759_0_0_1;
FieldInfo InvokableCall_1_t1708____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t1759_0_0_1/* type */
	, &InvokableCall_1_t1708_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t1708_FieldInfos[] =
{
	&InvokableCall_1_t1708____Delegate_0_FieldInfo,
	NULL
};
extern MethodInfo InvokableCall_1_Invoke_m7597_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m7598_MethodInfo;
static Il2CppMethodReference InvokableCall_1_t1708_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&InvokableCall_1_Invoke_m7597_MethodInfo,
	&InvokableCall_1_Find_m7598_MethodInfo,
};
static bool InvokableCall_1_t1708_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1758_m7647_GenericMethod;
extern Il2CppType InvokableCall_1_t1708_gp_0_0_0_0;
extern Il2CppGenericMethod UnityAction_1_Invoke_m7648_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_1_t1708_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityAction_1_t1759_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &UnityAction_1_t1759_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1758_m7647_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_1_t1708_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m7648_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t1708_0_0_0;
extern Il2CppType InvokableCall_1_t1708_1_0_0;
struct InvokableCall_1_t1708;
const Il2CppTypeDefinitionMetadata InvokableCall_1_t1708_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1647_0_0_0/* parent */
	, InvokableCall_1_t1708_VTable/* vtableMethods */
	, InvokableCall_1_t1708_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_1_t1708_RGCTXData/* rgctxDefinition */

};
TypeInfo InvokableCall_1_t1708_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t1708_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t1708_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t1708_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_1_t1708_0_0_0/* byval_arg */
	, &InvokableCall_1_t1708_1_0_0/* this_arg */
	, &InvokableCall_1_t1708_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_1_t1708_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`2
extern TypeInfo InvokableCall_2_t1709_il2cpp_TypeInfo;
extern Il2CppGenericContainer InvokableCall_2_t1709_Il2CppGenericContainer;
extern TypeInfo InvokableCall_2_t1709_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_2_t1709_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_2_t1709_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_2_t1709_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_2_t1709_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_2_t1709_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_2_t1709_Il2CppGenericParametersArray[2] = 
{
	&InvokableCall_2_t1709_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_2_t1709_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer InvokableCall_2_t1709_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_2_t1709_il2cpp_TypeInfo, 2, 0, InvokableCall_2_t1709_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_2_t1709_InvokableCall_2__ctor_m7599_ParameterInfos[] = 
{
	{"target", 0, 134219483, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219484, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_2__ctor_m7599_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1709_InvokableCall_2__ctor_m7599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo InvokableCall_2_t1709_InvokableCall_2_Invoke_m7600_ParameterInfos[] = 
{
	{"args", 0, 134219485, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
MethodInfo InvokableCall_2_Invoke_m7600_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1709_InvokableCall_2_Invoke_m7600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_2_t1709_InvokableCall_2_Find_m7601_ParameterInfos[] = 
{
	{"targetObj", 0, 134219486, 0, &Object_t_0_0_0},
	{"method", 1, 134219487, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_2_Find_m7601_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1709_InvokableCall_2_Find_m7601_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_2_t1709_MethodInfos[] =
{
	&InvokableCall_2__ctor_m7599_MethodInfo,
	&InvokableCall_2_Invoke_m7600_MethodInfo,
	&InvokableCall_2_Find_m7601_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_2_t1762_0_0_1;
FieldInfo InvokableCall_2_t1709____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_2_t1762_0_0_1/* type */
	, &InvokableCall_2_t1709_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_2_t1709_FieldInfos[] =
{
	&InvokableCall_2_t1709____Delegate_0_FieldInfo,
	NULL
};
extern MethodInfo InvokableCall_2_Invoke_m7600_MethodInfo;
extern MethodInfo InvokableCall_2_Find_m7601_MethodInfo;
static Il2CppMethodReference InvokableCall_2_t1709_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&InvokableCall_2_Invoke_m7600_MethodInfo,
	&InvokableCall_2_Find_m7601_MethodInfo,
};
static bool InvokableCall_2_t1709_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType UnityAction_2_t1762_0_0_0;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1760_m7649_GenericMethod;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1761_m7650_GenericMethod;
extern Il2CppType InvokableCall_2_t1709_gp_0_0_0_0;
extern Il2CppType InvokableCall_2_t1709_gp_1_0_0_0;
extern Il2CppGenericMethod UnityAction_2_Invoke_m7651_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_2_t1709_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityAction_2_t1762_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &UnityAction_2_t1762_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1760_m7649_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1761_m7650_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_2_t1709_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_2_t1709_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_2_Invoke_m7651_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_2_t1709_0_0_0;
extern Il2CppType InvokableCall_2_t1709_1_0_0;
struct InvokableCall_2_t1709;
const Il2CppTypeDefinitionMetadata InvokableCall_2_t1709_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1647_0_0_0/* parent */
	, InvokableCall_2_t1709_VTable/* vtableMethods */
	, InvokableCall_2_t1709_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_2_t1709_RGCTXData/* rgctxDefinition */

};
TypeInfo InvokableCall_2_t1709_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_2_t1709_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_2_t1709_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_2_t1709_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_2_t1709_0_0_0/* byval_arg */
	, &InvokableCall_2_t1709_1_0_0/* this_arg */
	, &InvokableCall_2_t1709_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_2_t1709_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`3
extern TypeInfo InvokableCall_3_t1710_il2cpp_TypeInfo;
extern Il2CppGenericContainer InvokableCall_3_t1710_Il2CppGenericContainer;
extern TypeInfo InvokableCall_3_t1710_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1710_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1710_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_3_t1710_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1710_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1710_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo InvokableCall_3_t1710_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1710_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1710_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_3_t1710_Il2CppGenericParametersArray[3] = 
{
	&InvokableCall_3_t1710_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1710_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1710_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer InvokableCall_3_t1710_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_3_t1710_il2cpp_TypeInfo, 3, 0, InvokableCall_3_t1710_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_3_t1710_InvokableCall_3__ctor_m7602_ParameterInfos[] = 
{
	{"target", 0, 134219488, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219489, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_3__ctor_m7602_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1710_InvokableCall_3__ctor_m7602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo InvokableCall_3_t1710_InvokableCall_3_Invoke_m7603_ParameterInfos[] = 
{
	{"args", 0, 134219490, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
MethodInfo InvokableCall_3_Invoke_m7603_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1710_InvokableCall_3_Invoke_m7603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_3_t1710_InvokableCall_3_Find_m7604_ParameterInfos[] = 
{
	{"targetObj", 0, 134219491, 0, &Object_t_0_0_0},
	{"method", 1, 134219492, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_3_Find_m7604_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1710_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1710_InvokableCall_3_Find_m7604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_3_t1710_MethodInfos[] =
{
	&InvokableCall_3__ctor_m7602_MethodInfo,
	&InvokableCall_3_Invoke_m7603_MethodInfo,
	&InvokableCall_3_Find_m7604_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_3_t1766_0_0_1;
FieldInfo InvokableCall_3_t1710____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_3_t1766_0_0_1/* type */
	, &InvokableCall_3_t1710_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_3_t1710_FieldInfos[] =
{
	&InvokableCall_3_t1710____Delegate_0_FieldInfo,
	NULL
};
extern MethodInfo InvokableCall_3_Invoke_m7603_MethodInfo;
extern MethodInfo InvokableCall_3_Find_m7604_MethodInfo;
static Il2CppMethodReference InvokableCall_3_t1710_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&InvokableCall_3_Invoke_m7603_MethodInfo,
	&InvokableCall_3_Find_m7604_MethodInfo,
};
static bool InvokableCall_3_t1710_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType UnityAction_3_t1766_0_0_0;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1763_m7652_GenericMethod;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1764_m7653_GenericMethod;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1765_m7654_GenericMethod;
extern Il2CppType InvokableCall_3_t1710_gp_0_0_0_0;
extern Il2CppType InvokableCall_3_t1710_gp_1_0_0_0;
extern Il2CppType InvokableCall_3_t1710_gp_2_0_0_0;
extern Il2CppGenericMethod UnityAction_3_Invoke_m7655_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_3_t1710_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityAction_3_t1766_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &UnityAction_3_t1766_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1763_m7652_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1764_m7653_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1765_m7654_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_3_t1710_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_3_t1710_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_3_t1710_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_3_Invoke_m7655_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_3_t1710_0_0_0;
extern Il2CppType InvokableCall_3_t1710_1_0_0;
struct InvokableCall_3_t1710;
const Il2CppTypeDefinitionMetadata InvokableCall_3_t1710_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1647_0_0_0/* parent */
	, InvokableCall_3_t1710_VTable/* vtableMethods */
	, InvokableCall_3_t1710_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_3_t1710_RGCTXData/* rgctxDefinition */

};
TypeInfo InvokableCall_3_t1710_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_3_t1710_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_3_t1710_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_3_t1710_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_3_t1710_0_0_0/* byval_arg */
	, &InvokableCall_3_t1710_1_0_0/* this_arg */
	, &InvokableCall_3_t1710_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_3_t1710_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`4
extern TypeInfo InvokableCall_4_t1711_il2cpp_TypeInfo;
extern Il2CppGenericContainer InvokableCall_4_t1711_Il2CppGenericContainer;
extern TypeInfo InvokableCall_4_t1711_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1711_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1711_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1711_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1711_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1711_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1711_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1711_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1711_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1711_gp_T4_3_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1711_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1711_Il2CppGenericContainer, 3}, {NULL, "T4", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_4_t1711_Il2CppGenericParametersArray[4] = 
{
	&InvokableCall_4_t1711_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1711_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1711_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1711_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer InvokableCall_4_t1711_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_4_t1711_il2cpp_TypeInfo, 4, 0, InvokableCall_4_t1711_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_4_t1711_InvokableCall_4__ctor_m7605_ParameterInfos[] = 
{
	{"target", 0, 134219493, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219494, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_4__ctor_m7605_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1711_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1711_InvokableCall_4__ctor_m7605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo InvokableCall_4_t1711_InvokableCall_4_Invoke_m7606_ParameterInfos[] = 
{
	{"args", 0, 134219495, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
MethodInfo InvokableCall_4_Invoke_m7606_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1711_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1711_InvokableCall_4_Invoke_m7606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCall_4_t1711_InvokableCall_4_Find_m7607_ParameterInfos[] = 
{
	{"targetObj", 0, 134219496, 0, &Object_t_0_0_0},
	{"method", 1, 134219497, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_4_Find_m7607_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1711_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1711_InvokableCall_4_Find_m7607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_4_t1711_MethodInfos[] =
{
	&InvokableCall_4__ctor_m7605_MethodInfo,
	&InvokableCall_4_Invoke_m7606_MethodInfo,
	&InvokableCall_4_Find_m7607_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_4_t1771_0_0_1;
FieldInfo InvokableCall_4_t1711____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_4_t1771_0_0_1/* type */
	, &InvokableCall_4_t1711_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_4_t1711_FieldInfos[] =
{
	&InvokableCall_4_t1711____Delegate_0_FieldInfo,
	NULL
};
extern MethodInfo InvokableCall_4_Invoke_m7606_MethodInfo;
extern MethodInfo InvokableCall_4_Find_m7607_MethodInfo;
static Il2CppMethodReference InvokableCall_4_t1711_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&InvokableCall_4_Invoke_m7606_MethodInfo,
	&InvokableCall_4_Find_m7607_MethodInfo,
};
static bool InvokableCall_4_t1711_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType UnityAction_4_t1771_0_0_0;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1767_m7656_GenericMethod;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1768_m7657_GenericMethod;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1769_m7658_GenericMethod;
extern Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1770_m7659_GenericMethod;
extern Il2CppType InvokableCall_4_t1711_gp_0_0_0_0;
extern Il2CppType InvokableCall_4_t1711_gp_1_0_0_0;
extern Il2CppType InvokableCall_4_t1711_gp_2_0_0_0;
extern Il2CppType InvokableCall_4_t1711_gp_3_0_0_0;
extern Il2CppGenericMethod UnityAction_4_Invoke_m7660_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_4_t1711_RGCTXData[12] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityAction_4_t1771_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &UnityAction_4_t1771_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1767_m7656_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1768_m7657_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1769_m7658_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1770_m7659_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_4_t1711_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_4_t1711_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_4_t1711_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_4_t1711_gp_3_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_4_Invoke_m7660_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_4_t1711_0_0_0;
extern Il2CppType InvokableCall_4_t1711_1_0_0;
struct InvokableCall_4_t1711;
const Il2CppTypeDefinitionMetadata InvokableCall_4_t1711_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1647_0_0_0/* parent */
	, InvokableCall_4_t1711_VTable/* vtableMethods */
	, InvokableCall_4_t1711_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_4_t1711_RGCTXData/* rgctxDefinition */

};
TypeInfo InvokableCall_4_t1711_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_4_t1711_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_4_t1711_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_4_t1711_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_4_t1711_0_0_0/* byval_arg */
	, &InvokableCall_4_t1711_1_0_0/* this_arg */
	, &InvokableCall_4_t1711_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_4_t1711_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1
extern TypeInfo CachedInvokableCall_1_t1699_il2cpp_TypeInfo;
extern Il2CppGenericContainer CachedInvokableCall_1_t1699_Il2CppGenericContainer;
extern TypeInfo CachedInvokableCall_1_t1699_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull CachedInvokableCall_1_t1699_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &CachedInvokableCall_1_t1699_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* CachedInvokableCall_1_t1699_Il2CppGenericParametersArray[1] = 
{
	&CachedInvokableCall_1_t1699_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer CachedInvokableCall_1_t1699_Il2CppGenericContainer = { { NULL, NULL }, NULL, &CachedInvokableCall_1_t1699_il2cpp_TypeInfo, 1, 0, CachedInvokableCall_1_t1699_Il2CppGenericParametersArray };
extern Il2CppType Object_t187_0_0_0;
extern Il2CppType Object_t187_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1699_gp_0_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1699_gp_0_0_0_0;
static ParameterInfo CachedInvokableCall_1_t1699_CachedInvokableCall_1__ctor_m7608_ParameterInfos[] = 
{
	{"target", 0, 134219498, 0, &Object_t187_0_0_0},
	{"theFunction", 1, 134219499, 0, &MethodInfo_t_0_0_0},
	{"argument", 2, 134219500, 0, &CachedInvokableCall_1_t1699_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m7608_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1699_CachedInvokableCall_1__ctor_m7608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo CachedInvokableCall_1_t1699_CachedInvokableCall_1_Invoke_m7609_ParameterInfos[] = 
{
	{"args", 0, 134219501, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m7609_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1699_CachedInvokableCall_1_Invoke_m7609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CachedInvokableCall_1_t1699_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m7608_MethodInfo,
	&CachedInvokableCall_1_Invoke_m7609_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_33;
FieldInfo CachedInvokableCall_1_t1699____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t208_0_0_33/* type */
	, &CachedInvokableCall_1_t1699_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t1699_FieldInfos[] =
{
	&CachedInvokableCall_1_t1699____m_Arg1_1_FieldInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m7609_MethodInfo;
extern Il2CppGenericMethod InvokableCall_1_Find_m7661_GenericMethod;
static Il2CppMethodReference CachedInvokableCall_1_t1699_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&CachedInvokableCall_1_Invoke_m7609_MethodInfo,
	&InvokableCall_1_Find_m7661_GenericMethod,
};
static bool CachedInvokableCall_1_t1699_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	true,
};
extern Il2CppGenericMethod InvokableCall_1__ctor_m7662_GenericMethod;
extern Il2CppGenericMethod InvokableCall_1_Invoke_m7663_GenericMethod;
static Il2CppRGCTXDefinition CachedInvokableCall_1_t1699_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7662_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &CachedInvokableCall_1_t1699_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1_Invoke_m7663_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t1699_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1699_1_0_0;
extern Il2CppType InvokableCall_1_t1773_0_0_0;
struct CachedInvokableCall_1_t1699;
const Il2CppTypeDefinitionMetadata CachedInvokableCall_1_t1699_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InvokableCall_1_t1773_0_0_0/* parent */
	, CachedInvokableCall_1_t1699_VTable/* vtableMethods */
	, CachedInvokableCall_1_t1699_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, CachedInvokableCall_1_t1699_RGCTXData/* rgctxDefinition */

};
TypeInfo CachedInvokableCall_1_t1699_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t1699_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t1699_FieldInfos/* fields */
	, NULL/* events */
	, &CachedInvokableCall_1_t1699_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CachedInvokableCall_1_t1699_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t1699_1_0_0/* this_arg */
	, &CachedInvokableCall_1_t1699_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CachedInvokableCall_1_t1699_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// Metadata Definition UnityEngine.Events.UnityEventCallState
extern TypeInfo UnityEventCallState_t1649_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
static MethodInfo* UnityEventCallState_t1649_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo UnityEventCallState_t1649____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &UnityEventCallState_t1649_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventCallState_t1649, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1649_0_0_32854;
FieldInfo UnityEventCallState_t1649____Off_2_FieldInfo = 
{
	"Off"/* name */
	, &UnityEventCallState_t1649_0_0_32854/* type */
	, &UnityEventCallState_t1649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1649_0_0_32854;
FieldInfo UnityEventCallState_t1649____EditorAndRuntime_3_FieldInfo = 
{
	"EditorAndRuntime"/* name */
	, &UnityEventCallState_t1649_0_0_32854/* type */
	, &UnityEventCallState_t1649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1649_0_0_32854;
FieldInfo UnityEventCallState_t1649____RuntimeOnly_4_FieldInfo = 
{
	"RuntimeOnly"/* name */
	, &UnityEventCallState_t1649_0_0_32854/* type */
	, &UnityEventCallState_t1649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityEventCallState_t1649_FieldInfos[] =
{
	&UnityEventCallState_t1649____value___1_FieldInfo,
	&UnityEventCallState_t1649____Off_2_FieldInfo,
	&UnityEventCallState_t1649____EditorAndRuntime_3_FieldInfo,
	&UnityEventCallState_t1649____RuntimeOnly_4_FieldInfo,
	NULL
};
static const int32_t UnityEventCallState_t1649____Off_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1649____Off_2_DefaultValue = 
{
	&UnityEventCallState_t1649____Off_2_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1649____Off_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UnityEventCallState_t1649____EditorAndRuntime_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1649____EditorAndRuntime_3_DefaultValue = 
{
	&UnityEventCallState_t1649____EditorAndRuntime_3_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1649____EditorAndRuntime_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UnityEventCallState_t1649____RuntimeOnly_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1649____RuntimeOnly_4_DefaultValue = 
{
	&UnityEventCallState_t1649____RuntimeOnly_4_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1649____RuntimeOnly_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UnityEventCallState_t1649_FieldDefaultValues[] = 
{
	&UnityEventCallState_t1649____Off_2_DefaultValue,
	&UnityEventCallState_t1649____EditorAndRuntime_3_DefaultValue,
	&UnityEventCallState_t1649____RuntimeOnly_4_DefaultValue,
	NULL
};
static Il2CppMethodReference UnityEventCallState_t1649_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool UnityEventCallState_t1649_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t1649_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEventCallState_t1649_0_0_0;
extern Il2CppType UnityEventCallState_t1649_1_0_0;
const Il2CppTypeDefinitionMetadata UnityEventCallState_t1649_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEventCallState_t1649_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, UnityEventCallState_t1649_VTable/* vtableMethods */
	, UnityEventCallState_t1649_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityEventCallState_t1649_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventCallState_t1649_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEventCallState_t1649_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventCallState_t1649_0_0_0/* byval_arg */
	, &UnityEventCallState_t1649_1_0_0/* this_arg */
	, &UnityEventCallState_t1649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UnityEventCallState_t1649_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t1649)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityEventCallState_t1649)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// Metadata Definition UnityEngine.Events.PersistentCall
extern TypeInfo PersistentCall_t1650_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCall::.ctor()
MethodInfo PersistentCall__ctor_m7435_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCall__ctor_m7435/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t187_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
MethodInfo PersistentCall_get_target_m7436_MethodInfo = 
{
	"get_target"/* name */
	, (methodPointerType)&PersistentCall_get_target_m7436/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &Object_t187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.PersistentCall::get_methodName()
MethodInfo PersistentCall_get_methodName_m7437_MethodInfo = 
{
	"get_methodName"/* name */
	, (methodPointerType)&PersistentCall_get_methodName_m7437/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_0;
extern void* RuntimeInvoker_PersistentListenerMode_t1645 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
MethodInfo PersistentCall_get_mode_m7438_MethodInfo = 
{
	"get_mode"/* name */
	, (methodPointerType)&PersistentCall_get_mode_m7438/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &PersistentListenerMode_t1645_0_0_0/* return_type */
	, RuntimeInvoker_PersistentListenerMode_t1645/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ArgumentCache_t1646_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
MethodInfo PersistentCall_get_arguments_m7439_MethodInfo = 
{
	"get_arguments"/* name */
	, (methodPointerType)&PersistentCall_get_arguments_m7439/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentCache_t1646_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
MethodInfo PersistentCall_IsValid_m7440_MethodInfo = 
{
	"IsValid"/* name */
	, (methodPointerType)&PersistentCall_IsValid_m7440/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityEventBase_t1655_0_0_0;
extern Il2CppType UnityEventBase_t1655_0_0_0;
static ParameterInfo PersistentCall_t1650_PersistentCall_GetRuntimeCall_m7441_ParameterInfos[] = 
{
	{"theEvent", 0, 134219502, 0, &UnityEventBase_t1655_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
MethodInfo PersistentCall_GetRuntimeCall_m7441_MethodInfo = 
{
	"GetRuntimeCall"/* name */
	, (methodPointerType)&PersistentCall_GetRuntimeCall_m7441/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1650_PersistentCall_GetRuntimeCall_m7441_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t187_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
extern Il2CppType ArgumentCache_t1646_0_0_0;
static ParameterInfo PersistentCall_t1650_PersistentCall_GetObjectCall_m7442_ParameterInfos[] = 
{
	{"target", 0, 134219503, 0, &Object_t187_0_0_0},
	{"method", 1, 134219504, 0, &MethodInfo_t_0_0_0},
	{"arguments", 2, 134219505, 0, &ArgumentCache_t1646_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
MethodInfo PersistentCall_GetObjectCall_m7442_MethodInfo = 
{
	"GetObjectCall"/* name */
	, (methodPointerType)&PersistentCall_GetObjectCall_m7442/* method */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1650_PersistentCall_GetObjectCall_m7442_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PersistentCall_t1650_MethodInfos[] =
{
	&PersistentCall__ctor_m7435_MethodInfo,
	&PersistentCall_get_target_m7436_MethodInfo,
	&PersistentCall_get_methodName_m7437_MethodInfo,
	&PersistentCall_get_mode_m7438_MethodInfo,
	&PersistentCall_get_arguments_m7439_MethodInfo,
	&PersistentCall_IsValid_m7440_MethodInfo,
	&PersistentCall_GetRuntimeCall_m7441_MethodInfo,
	&PersistentCall_GetObjectCall_m7442_MethodInfo,
	NULL
};
extern Il2CppType Object_t187_0_0_1;
FieldInfo PersistentCall_t1650____m_Target_0_FieldInfo = 
{
	"m_Target"/* name */
	, &Object_t187_0_0_1/* type */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1650, ___m_Target_0)/* offset */
	, 652/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo PersistentCall_t1650____m_MethodName_1_FieldInfo = 
{
	"m_MethodName"/* name */
	, &String_t_0_0_1/* type */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1650, ___m_MethodName_1)/* offset */
	, 653/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1645_0_0_1;
FieldInfo PersistentCall_t1650____m_Mode_2_FieldInfo = 
{
	"m_Mode"/* name */
	, &PersistentListenerMode_t1645_0_0_1/* type */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1650, ___m_Mode_2)/* offset */
	, 654/* custom_attributes_cache */

};
extern Il2CppType ArgumentCache_t1646_0_0_1;
FieldInfo PersistentCall_t1650____m_Arguments_3_FieldInfo = 
{
	"m_Arguments"/* name */
	, &ArgumentCache_t1646_0_0_1/* type */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1650, ___m_Arguments_3)/* offset */
	, 655/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1649_0_0_1;
FieldInfo PersistentCall_t1650____m_CallState_4_FieldInfo = 
{
	"m_CallState"/* name */
	, &UnityEventCallState_t1649_0_0_1/* type */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1650, ___m_CallState_4)/* offset */
	, 656/* custom_attributes_cache */

};
static FieldInfo* PersistentCall_t1650_FieldInfos[] =
{
	&PersistentCall_t1650____m_Target_0_FieldInfo,
	&PersistentCall_t1650____m_MethodName_1_FieldInfo,
	&PersistentCall_t1650____m_Mode_2_FieldInfo,
	&PersistentCall_t1650____m_Arguments_3_FieldInfo,
	&PersistentCall_t1650____m_CallState_4_FieldInfo,
	NULL
};
extern MethodInfo PersistentCall_get_target_m7436_MethodInfo;
static PropertyInfo PersistentCall_t1650____target_PropertyInfo = 
{
	&PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, "target"/* name */
	, &PersistentCall_get_target_m7436_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo PersistentCall_get_methodName_m7437_MethodInfo;
static PropertyInfo PersistentCall_t1650____methodName_PropertyInfo = 
{
	&PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, "methodName"/* name */
	, &PersistentCall_get_methodName_m7437_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo PersistentCall_get_mode_m7438_MethodInfo;
static PropertyInfo PersistentCall_t1650____mode_PropertyInfo = 
{
	&PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, "mode"/* name */
	, &PersistentCall_get_mode_m7438_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo PersistentCall_get_arguments_m7439_MethodInfo;
static PropertyInfo PersistentCall_t1650____arguments_PropertyInfo = 
{
	&PersistentCall_t1650_il2cpp_TypeInfo/* parent */
	, "arguments"/* name */
	, &PersistentCall_get_arguments_m7439_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* PersistentCall_t1650_PropertyInfos[] =
{
	&PersistentCall_t1650____target_PropertyInfo,
	&PersistentCall_t1650____methodName_PropertyInfo,
	&PersistentCall_t1650____mode_PropertyInfo,
	&PersistentCall_t1650____arguments_PropertyInfo,
	NULL
};
static Il2CppMethodReference PersistentCall_t1650_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool PersistentCall_t1650_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentCall_t1650_0_0_0;
extern Il2CppType PersistentCall_t1650_1_0_0;
struct PersistentCall_t1650;
const Il2CppTypeDefinitionMetadata PersistentCall_t1650_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCall_t1650_VTable/* vtableMethods */
	, PersistentCall_t1650_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PersistentCall_t1650_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCall_t1650_MethodInfos/* methods */
	, PersistentCall_t1650_PropertyInfos/* properties */
	, PersistentCall_t1650_FieldInfos/* fields */
	, NULL/* events */
	, &PersistentCall_t1650_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCall_t1650_0_0_0/* byval_arg */
	, &PersistentCall_t1650_1_0_0/* this_arg */
	, &PersistentCall_t1650_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t1650)/* instance_size */
	, sizeof (PersistentCall_t1650)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern TypeInfo PersistentCallGroup_t1652_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
MethodInfo PersistentCallGroup__ctor_m7443_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCallGroup__ctor_m7443/* method */
	, &PersistentCallGroup_t1652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType InvokableCallList_t1654_0_0_0;
extern Il2CppType InvokableCallList_t1654_0_0_0;
extern Il2CppType UnityEventBase_t1655_0_0_0;
static ParameterInfo PersistentCallGroup_t1652_PersistentCallGroup_Initialize_m7444_ParameterInfos[] = 
{
	{"invokableList", 0, 134219506, 0, &InvokableCallList_t1654_0_0_0},
	{"unityEventBase", 1, 134219507, 0, &UnityEventBase_t1655_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
MethodInfo PersistentCallGroup_Initialize_m7444_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&PersistentCallGroup_Initialize_m7444/* method */
	, &PersistentCallGroup_t1652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, PersistentCallGroup_t1652_PersistentCallGroup_Initialize_m7444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PersistentCallGroup_t1652_MethodInfos[] =
{
	&PersistentCallGroup__ctor_m7443_MethodInfo,
	&PersistentCallGroup_Initialize_m7444_MethodInfo,
	NULL
};
extern Il2CppType List_1_t1651_0_0_1;
FieldInfo PersistentCallGroup_t1652____m_Calls_0_FieldInfo = 
{
	"m_Calls"/* name */
	, &List_1_t1651_0_0_1/* type */
	, &PersistentCallGroup_t1652_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCallGroup_t1652, ___m_Calls_0)/* offset */
	, 657/* custom_attributes_cache */

};
static FieldInfo* PersistentCallGroup_t1652_FieldInfos[] =
{
	&PersistentCallGroup_t1652____m_Calls_0_FieldInfo,
	NULL
};
static Il2CppMethodReference PersistentCallGroup_t1652_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool PersistentCallGroup_t1652_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentCallGroup_t1652_0_0_0;
extern Il2CppType PersistentCallGroup_t1652_1_0_0;
struct PersistentCallGroup_t1652;
const Il2CppTypeDefinitionMetadata PersistentCallGroup_t1652_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCallGroup_t1652_VTable/* vtableMethods */
	, PersistentCallGroup_t1652_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PersistentCallGroup_t1652_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCallGroup_t1652_MethodInfos/* methods */
	, NULL/* properties */
	, PersistentCallGroup_t1652_FieldInfos/* fields */
	, NULL/* events */
	, &PersistentCallGroup_t1652_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCallGroup_t1652_0_0_0/* byval_arg */
	, &PersistentCallGroup_t1652_1_0_0/* this_arg */
	, &PersistentCallGroup_t1652_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t1652)/* instance_size */
	, sizeof (PersistentCallGroup_t1652)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// Metadata Definition UnityEngine.Events.InvokableCallList
extern TypeInfo InvokableCallList_t1654_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
MethodInfo InvokableCallList__ctor_m7445_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCallList__ctor_m7445/* method */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
static ParameterInfo InvokableCallList_t1654_InvokableCallList_AddPersistentInvokableCall_m7446_ParameterInfos[] = 
{
	{"call", 0, 134219508, 0, &BaseInvokableCall_t1647_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
MethodInfo InvokableCallList_AddPersistentInvokableCall_m7446_MethodInfo = 
{
	"AddPersistentInvokableCall"/* name */
	, (methodPointerType)&InvokableCallList_AddPersistentInvokableCall_m7446/* method */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InvokableCallList_t1654_InvokableCallList_AddPersistentInvokableCall_m7446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
static ParameterInfo InvokableCallList_t1654_InvokableCallList_AddListener_m7447_ParameterInfos[] = 
{
	{"call", 0, 134219509, 0, &BaseInvokableCall_t1647_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
MethodInfo InvokableCallList_AddListener_m7447_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&InvokableCallList_AddListener_m7447/* method */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InvokableCallList_t1654_InvokableCallList_AddListener_m7447_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo InvokableCallList_t1654_InvokableCallList_RemoveListener_m7448_ParameterInfos[] = 
{
	{"targetObj", 0, 134219510, 0, &Object_t_0_0_0},
	{"method", 1, 134219511, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCallList_RemoveListener_m7448_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&InvokableCallList_RemoveListener_m7448/* method */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, InvokableCallList_t1654_InvokableCallList_RemoveListener_m7448_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
MethodInfo InvokableCallList_ClearPersistent_m7449_MethodInfo = 
{
	"ClearPersistent"/* name */
	, (methodPointerType)&InvokableCallList_ClearPersistent_m7449/* method */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo InvokableCallList_t1654_InvokableCallList_Invoke_m7450_ParameterInfos[] = 
{
	{"parameters", 0, 134219512, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
MethodInfo InvokableCallList_Invoke_m7450_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCallList_Invoke_m7450/* method */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InvokableCallList_t1654_InvokableCallList_Invoke_m7450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCallList_t1654_MethodInfos[] =
{
	&InvokableCallList__ctor_m7445_MethodInfo,
	&InvokableCallList_AddPersistentInvokableCall_m7446_MethodInfo,
	&InvokableCallList_AddListener_m7447_MethodInfo,
	&InvokableCallList_RemoveListener_m7448_MethodInfo,
	&InvokableCallList_ClearPersistent_m7449_MethodInfo,
	&InvokableCallList_Invoke_m7450_MethodInfo,
	NULL
};
extern Il2CppType List_1_t1653_0_0_33;
FieldInfo InvokableCallList_t1654____m_PersistentCalls_0_FieldInfo = 
{
	"m_PersistentCalls"/* name */
	, &List_1_t1653_0_0_33/* type */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1654, ___m_PersistentCalls_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t1653_0_0_33;
FieldInfo InvokableCallList_t1654____m_RuntimeCalls_1_FieldInfo = 
{
	"m_RuntimeCalls"/* name */
	, &List_1_t1653_0_0_33/* type */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1654, ___m_RuntimeCalls_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t1653_0_0_33;
FieldInfo InvokableCallList_t1654____m_ExecutingCalls_2_FieldInfo = 
{
	"m_ExecutingCalls"/* name */
	, &List_1_t1653_0_0_33/* type */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1654, ___m_ExecutingCalls_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* InvokableCallList_t1654_FieldInfos[] =
{
	&InvokableCallList_t1654____m_PersistentCalls_0_FieldInfo,
	&InvokableCallList_t1654____m_RuntimeCalls_1_FieldInfo,
	&InvokableCallList_t1654____m_ExecutingCalls_2_FieldInfo,
	NULL
};
static Il2CppMethodReference InvokableCallList_t1654_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool InvokableCallList_t1654_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCallList_t1654_1_0_0;
struct InvokableCallList_t1654;
const Il2CppTypeDefinitionMetadata InvokableCallList_t1654_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InvokableCallList_t1654_VTable/* vtableMethods */
	, InvokableCallList_t1654_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo InvokableCallList_t1654_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCallList_t1654_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCallList_t1654_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCallList_t1654_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCallList_t1654_0_0_0/* byval_arg */
	, &InvokableCallList_t1654_1_0_0/* this_arg */
	, &InvokableCallList_t1654_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t1654)/* instance_size */
	, sizeof (InvokableCallList_t1654)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// Metadata Definition UnityEngine.Events.UnityEventBase
extern TypeInfo UnityEventBase_t1655_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
MethodInfo UnityEventBase__ctor_m7451_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEventBase__ctor_m7451/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_FindMethod_Impl_m7610_ParameterInfos[] = 
{
	{"name", 0, 134219513, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219514, 0, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEventBase_FindMethod_Impl_m7610_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_FindMethod_Impl_m7610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_GetDelegate_m7611_ParameterInfos[] = 
{
	{"target", 0, 134219515, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219516, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEventBase_GetDelegate_m7611_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_GetDelegate_m7611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1475/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PersistentCall_t1650_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_FindMethod_m7452_ParameterInfos[] = 
{
	{"call", 0, 134219517, 0, &PersistentCall_t1650_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
MethodInfo UnityEventBase_FindMethod_m7452_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m7452/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_FindMethod_m7452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType PersistentListenerMode_t1645_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_FindMethod_m7453_ParameterInfos[] = 
{
	{"name", 0, 134219518, 0, &String_t_0_0_0},
	{"listener", 1, 134219519, 0, &Object_t_0_0_0},
	{"mode", 2, 134219520, 0, &PersistentListenerMode_t1645_0_0_0},
	{"argumentType", 3, 134219521, 0, &Type_t_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
MethodInfo UnityEventBase_FindMethod_m7453_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m7453/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_FindMethod_m7453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
MethodInfo UnityEventBase_DirtyPersistentCalls_m7454_MethodInfo = 
{
	"DirtyPersistentCalls"/* name */
	, (methodPointerType)&UnityEventBase_DirtyPersistentCalls_m7454/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m7455_MethodInfo = 
{
	"RebuildPersistentCallsIfNeeded"/* name */
	, (methodPointerType)&UnityEventBase_RebuildPersistentCallsIfNeeded_m7455/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_AddCall_m7456_ParameterInfos[] = 
{
	{"call", 0, 134219522, 0, &BaseInvokableCall_t1647_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
MethodInfo UnityEventBase_AddCall_m7456_MethodInfo = 
{
	"AddCall"/* name */
	, (methodPointerType)&UnityEventBase_AddCall_m7456/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_AddCall_m7456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_RemoveListener_m7457_ParameterInfos[] = 
{
	{"targetObj", 0, 134219523, 0, &Object_t_0_0_0},
	{"method", 1, 134219524, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEventBase_RemoveListener_m7457_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEventBase_RemoveListener_m7457/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_RemoveListener_m7457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_Invoke_m7458_ParameterInfos[] = 
{
	{"parameters", 0, 134219525, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
MethodInfo UnityEventBase_Invoke_m7458_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEventBase_Invoke_m7458/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_Invoke_m7458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.UnityEventBase::ToString()
MethodInfo UnityEventBase_ToString_m6293_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UnityEventBase_ToString_m6293/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType TypeU5BU5D_t1671_0_0_0;
extern Il2CppType TypeU5BU5D_t1671_0_0_0;
static ParameterInfo UnityEventBase_t1655_UnityEventBase_GetValidMethodInfo_m7459_ParameterInfos[] = 
{
	{"obj", 0, 134219526, 0, &Object_t_0_0_0},
	{"functionName", 1, 134219527, 0, &String_t_0_0_0},
	{"argumentTypes", 2, 134219528, 0, &TypeU5BU5D_t1671_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
MethodInfo UnityEventBase_GetValidMethodInfo_m7459_MethodInfo = 
{
	"GetValidMethodInfo"/* name */
	, (methodPointerType)&UnityEventBase_GetValidMethodInfo_m7459/* method */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1655_UnityEventBase_GetValidMethodInfo_m7459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEventBase_t1655_MethodInfos[] =
{
	&UnityEventBase__ctor_m7451_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEventBase_FindMethod_Impl_m7610_MethodInfo,
	&UnityEventBase_GetDelegate_m7611_MethodInfo,
	&UnityEventBase_FindMethod_m7452_MethodInfo,
	&UnityEventBase_FindMethod_m7453_MethodInfo,
	&UnityEventBase_DirtyPersistentCalls_m7454_MethodInfo,
	&UnityEventBase_RebuildPersistentCallsIfNeeded_m7455_MethodInfo,
	&UnityEventBase_AddCall_m7456_MethodInfo,
	&UnityEventBase_RemoveListener_m7457_MethodInfo,
	&UnityEventBase_Invoke_m7458_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_GetValidMethodInfo_m7459_MethodInfo,
	NULL
};
extern Il2CppType InvokableCallList_t1654_0_0_1;
FieldInfo UnityEventBase_t1655____m_Calls_0_FieldInfo = 
{
	"m_Calls"/* name */
	, &InvokableCallList_t1654_0_0_1/* type */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1655, ___m_Calls_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PersistentCallGroup_t1652_0_0_1;
FieldInfo UnityEventBase_t1655____m_PersistentCalls_1_FieldInfo = 
{
	"m_PersistentCalls"/* name */
	, &PersistentCallGroup_t1652_0_0_1/* type */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1655, ___m_PersistentCalls_1)/* offset */
	, 658/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo UnityEventBase_t1655____m_TypeName_2_FieldInfo = 
{
	"m_TypeName"/* name */
	, &String_t_0_0_1/* type */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1655, ___m_TypeName_2)/* offset */
	, 659/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo UnityEventBase_t1655____m_CallsDirty_3_FieldInfo = 
{
	"m_CallsDirty"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1655, ___m_CallsDirty_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityEventBase_t1655_FieldInfos[] =
{
	&UnityEventBase_t1655____m_Calls_0_FieldInfo,
	&UnityEventBase_t1655____m_PersistentCalls_1_FieldInfo,
	&UnityEventBase_t1655____m_TypeName_2_FieldInfo,
	&UnityEventBase_t1655____m_CallsDirty_3_FieldInfo,
	NULL
};
extern MethodInfo UnityEventBase_ToString_m6293_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo;
static Il2CppMethodReference UnityEventBase_t1655_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	NULL,
	NULL,
};
static bool UnityEventBase_t1655_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ISerializationCallbackReceiver_t1446_0_0_0;
static const Il2CppType* UnityEventBase_t1655_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t1446_0_0_0,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t1655_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEventBase_t1655_1_0_0;
struct UnityEventBase_t1655;
const Il2CppTypeDefinitionMetadata UnityEventBase_t1655_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UnityEventBase_t1655_InterfacesTypeInfos/* implementedInterfaces */
	, UnityEventBase_t1655_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityEventBase_t1655_VTable/* vtableMethods */
	, UnityEventBase_t1655_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityEventBase_t1655_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventBase_t1655_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEventBase_t1655_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1655_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventBase_t1655_0_0_0/* byval_arg */
	, &UnityEventBase_t1655_1_0_0/* this_arg */
	, &UnityEventBase_t1655_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t1655)/* instance_size */
	, sizeof (UnityEventBase_t1655)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// Metadata Definition UnityEngine.Events.UnityEvent
extern TypeInfo UnityEvent_t1212_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
MethodInfo UnityEvent__ctor_m5845_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent__ctor_m5845/* method */
	, &UnityEvent_t1212_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_t1212_UnityEvent_FindMethod_Impl_m6308_ParameterInfos[] = 
{
	{"name", 0, 134219529, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219530, 0, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_FindMethod_Impl_m6308_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_FindMethod_Impl_m6308/* method */
	, &UnityEvent_t1212_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t1212_UnityEvent_FindMethod_Impl_m6308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo UnityEvent_t1212_UnityEvent_GetDelegate_m6309_ParameterInfos[] = 
{
	{"target", 0, 134219531, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219532, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_GetDelegate_m6309_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m6309/* method */
	, &UnityEvent_t1212_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t1212_UnityEvent_GetDelegate_m6309_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
MethodInfo UnityEvent_Invoke_m5847_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_Invoke_m5847/* method */
	, &UnityEvent_t1212_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_t1212_MethodInfos[] =
{
	&UnityEvent__ctor_m5845_MethodInfo,
	&UnityEvent_FindMethod_Impl_m6308_MethodInfo,
	&UnityEvent_GetDelegate_m6309_MethodInfo,
	&UnityEvent_Invoke_m5847_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_33;
FieldInfo UnityEvent_t1212____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t208_0_0_33/* type */
	, &UnityEvent_t1212_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_t1212, ___m_InvokeArray_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_t1212_FieldInfos[] =
{
	&UnityEvent_t1212____m_InvokeArray_4_FieldInfo,
	NULL
};
extern MethodInfo UnityEvent_FindMethod_Impl_m6308_MethodInfo;
extern MethodInfo UnityEvent_GetDelegate_m6309_MethodInfo;
static Il2CppMethodReference UnityEvent_t1212_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_FindMethod_Impl_m6308_MethodInfo,
	&UnityEvent_GetDelegate_m6309_MethodInfo,
};
static bool UnityEvent_t1212_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_t1212_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_t1212_0_0_0;
extern Il2CppType UnityEvent_t1212_1_0_0;
struct UnityEvent_t1212;
const Il2CppTypeDefinitionMetadata UnityEvent_t1212_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_t1212_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1655_0_0_0/* parent */
	, UnityEvent_t1212_VTable/* vtableMethods */
	, UnityEvent_t1212_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityEvent_t1212_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_t1212_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_t1212_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEvent_t1212_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_t1212_0_0_0/* byval_arg */
	, &UnityEvent_t1212_1_0_0/* this_arg */
	, &UnityEvent_t1212_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t1212)/* instance_size */
	, sizeof (UnityEvent_t1212)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`1
extern TypeInfo UnityEvent_1_t1712_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityEvent_1_t1712_Il2CppGenericContainer;
extern TypeInfo UnityEvent_1_t1712_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_1_t1712_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_1_t1712_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_1_t1712_Il2CppGenericParametersArray[1] = 
{
	&UnityEvent_1_t1712_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityEvent_1_t1712_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_1_t1712_il2cpp_TypeInfo, 1, 0, UnityEvent_1_t1712_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::.ctor()
MethodInfo UnityEvent_1__ctor_m7612_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1775_0_0_0;
extern Il2CppType UnityAction_1_t1775_0_0_0;
static ParameterInfo UnityEvent_1_t1712_UnityEvent_1_AddListener_m7613_ParameterInfos[] = 
{
	{"call", 0, 134219533, 0, &UnityAction_1_t1775_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_AddListener_m7613_MethodInfo = 
{
	"AddListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1712_UnityEvent_1_AddListener_m7613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1775_0_0_0;
static ParameterInfo UnityEvent_1_t1712_UnityEvent_1_RemoveListener_m7614_ParameterInfos[] = 
{
	{"call", 0, 134219534, 0, &UnityAction_1_t1775_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_RemoveListener_m7614_MethodInfo = 
{
	"RemoveListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1712_UnityEvent_1_RemoveListener_m7614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_1_t1712_UnityEvent_1_FindMethod_Impl_m7615_ParameterInfos[] = 
{
	{"name", 0, 134219535, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219536, 0, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_1_FindMethod_Impl_m7615_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1712_UnityEvent_1_FindMethod_Impl_m7615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo UnityEvent_1_t1712_UnityEvent_1_GetDelegate_m7616_ParameterInfos[] = 
{
	{"target", 0, 134219537, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219538, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_1_GetDelegate_m7616_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1712_UnityEvent_1_GetDelegate_m7616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1775_0_0_0;
static ParameterInfo UnityEvent_1_t1712_UnityEvent_1_GetDelegate_m7617_ParameterInfos[] = 
{
	{"action", 0, 134219539, 0, &UnityAction_1_t1775_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_GetDelegate_m7617_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1712_UnityEvent_1_GetDelegate_m7617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityEvent_1_t1712_gp_0_0_0_0;
extern Il2CppType UnityEvent_1_t1712_gp_0_0_0_0;
static ParameterInfo UnityEvent_1_t1712_UnityEvent_1_Invoke_m7618_ParameterInfos[] = 
{
	{"arg0", 0, 134219540, 0, &UnityEvent_1_t1712_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
MethodInfo UnityEvent_1_Invoke_m7618_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1712_UnityEvent_1_Invoke_m7618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_1_t1712_MethodInfos[] =
{
	&UnityEvent_1__ctor_m7612_MethodInfo,
	&UnityEvent_1_AddListener_m7613_MethodInfo,
	&UnityEvent_1_RemoveListener_m7614_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m7615_MethodInfo,
	&UnityEvent_1_GetDelegate_m7616_MethodInfo,
	&UnityEvent_1_GetDelegate_m7617_MethodInfo,
	&UnityEvent_1_Invoke_m7618_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_33;
FieldInfo UnityEvent_1_t1712____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t208_0_0_33/* type */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_1_t1712_FieldInfos[] =
{
	&UnityEvent_1_t1712____m_InvokeArray_4_FieldInfo,
	NULL
};
extern MethodInfo UnityEvent_1_FindMethod_Impl_m7615_MethodInfo;
extern MethodInfo UnityEvent_1_GetDelegate_m7616_MethodInfo;
static Il2CppMethodReference UnityEvent_1_t1712_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m7615_MethodInfo,
	&UnityEvent_1_GetDelegate_m7616_MethodInfo,
};
static bool UnityEvent_1_t1712_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_1_t1712_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppGenericMethod UnityEvent_1_GetDelegate_m7664_GenericMethod;
extern Il2CppType InvokableCall_1_t1776_0_0_0;
extern Il2CppGenericMethod InvokableCall_1__ctor_m7665_GenericMethod;
extern Il2CppGenericMethod InvokableCall_1__ctor_m7666_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_1_t1712_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityEvent_1_GetDelegate_m7664_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_1_t1712_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_1_t1776_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7665_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7666_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &UnityEvent_1_t1712_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_1_t1712_0_0_0;
extern Il2CppType UnityEvent_1_t1712_1_0_0;
struct UnityEvent_1_t1712;
const Il2CppTypeDefinitionMetadata UnityEvent_1_t1712_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_1_t1712_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1655_0_0_0/* parent */
	, UnityEvent_1_t1712_VTable/* vtableMethods */
	, UnityEvent_1_t1712_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_1_t1712_RGCTXData/* rgctxDefinition */

};
TypeInfo UnityEvent_1_t1712_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t1712_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_1_t1712_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEvent_1_t1712_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_1_t1712_0_0_0/* byval_arg */
	, &UnityEvent_1_t1712_1_0_0/* this_arg */
	, &UnityEvent_1_t1712_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_1_t1712_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`2
extern TypeInfo UnityEvent_2_t1713_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityEvent_2_t1713_Il2CppGenericContainer;
extern TypeInfo UnityEvent_2_t1713_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_2_t1713_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_2_t1713_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_2_t1713_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_2_t1713_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_2_t1713_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_2_t1713_Il2CppGenericParametersArray[2] = 
{
	&UnityEvent_2_t1713_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_2_t1713_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityEvent_2_t1713_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_2_t1713_il2cpp_TypeInfo, 2, 0, UnityEvent_2_t1713_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`2::.ctor()
MethodInfo UnityEvent_2__ctor_m7619_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1713_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_2_t1713_UnityEvent_2_FindMethod_Impl_m7620_ParameterInfos[] = 
{
	{"name", 0, 134219541, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219542, 0, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_2_FindMethod_Impl_m7620_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1713_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1713_UnityEvent_2_FindMethod_Impl_m7620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo UnityEvent_2_t1713_UnityEvent_2_GetDelegate_m7621_ParameterInfos[] = 
{
	{"target", 0, 134219543, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219544, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_2_GetDelegate_m7621_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1713_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1713_UnityEvent_2_GetDelegate_m7621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_2_t1713_MethodInfos[] =
{
	&UnityEvent_2__ctor_m7619_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m7620_MethodInfo,
	&UnityEvent_2_GetDelegate_m7621_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_33;
FieldInfo UnityEvent_2_t1713____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t208_0_0_33/* type */
	, &UnityEvent_2_t1713_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_2_t1713_FieldInfos[] =
{
	&UnityEvent_2_t1713____m_InvokeArray_4_FieldInfo,
	NULL
};
extern MethodInfo UnityEvent_2_FindMethod_Impl_m7620_MethodInfo;
extern MethodInfo UnityEvent_2_GetDelegate_m7621_MethodInfo;
static Il2CppMethodReference UnityEvent_2_t1713_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m7620_MethodInfo,
	&UnityEvent_2_GetDelegate_m7621_MethodInfo,
};
static bool UnityEvent_2_t1713_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_2_t1713_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppType UnityEvent_2_t1713_gp_0_0_0_0;
extern Il2CppType UnityEvent_2_t1713_gp_1_0_0_0;
extern Il2CppType InvokableCall_2_t1779_0_0_0;
extern Il2CppGenericMethod InvokableCall_2__ctor_m7667_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_2_t1713_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_2_t1713_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_2_t1713_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_2_t1779_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_2__ctor_m7667_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_2_t1713_0_0_0;
extern Il2CppType UnityEvent_2_t1713_1_0_0;
struct UnityEvent_2_t1713;
const Il2CppTypeDefinitionMetadata UnityEvent_2_t1713_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_2_t1713_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1655_0_0_0/* parent */
	, UnityEvent_2_t1713_VTable/* vtableMethods */
	, UnityEvent_2_t1713_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_2_t1713_RGCTXData/* rgctxDefinition */

};
TypeInfo UnityEvent_2_t1713_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t1713_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_2_t1713_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEvent_2_t1713_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_2_t1713_0_0_0/* byval_arg */
	, &UnityEvent_2_t1713_1_0_0/* this_arg */
	, &UnityEvent_2_t1713_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_2_t1713_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`3
extern TypeInfo UnityEvent_3_t1714_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityEvent_3_t1714_Il2CppGenericContainer;
extern TypeInfo UnityEvent_3_t1714_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1714_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1714_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_3_t1714_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1714_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1714_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityEvent_3_t1714_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1714_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1714_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_3_t1714_Il2CppGenericParametersArray[3] = 
{
	&UnityEvent_3_t1714_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1714_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1714_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityEvent_3_t1714_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_3_t1714_il2cpp_TypeInfo, 3, 0, UnityEvent_3_t1714_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`3::.ctor()
MethodInfo UnityEvent_3__ctor_m7622_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1714_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_3_t1714_UnityEvent_3_FindMethod_Impl_m7623_ParameterInfos[] = 
{
	{"name", 0, 134219545, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219546, 0, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_3_FindMethod_Impl_m7623_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1714_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1714_UnityEvent_3_FindMethod_Impl_m7623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo UnityEvent_3_t1714_UnityEvent_3_GetDelegate_m7624_ParameterInfos[] = 
{
	{"target", 0, 134219547, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219548, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_3_GetDelegate_m7624_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1714_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1714_UnityEvent_3_GetDelegate_m7624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_3_t1714_MethodInfos[] =
{
	&UnityEvent_3__ctor_m7622_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m7623_MethodInfo,
	&UnityEvent_3_GetDelegate_m7624_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_33;
FieldInfo UnityEvent_3_t1714____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t208_0_0_33/* type */
	, &UnityEvent_3_t1714_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_3_t1714_FieldInfos[] =
{
	&UnityEvent_3_t1714____m_InvokeArray_4_FieldInfo,
	NULL
};
extern MethodInfo UnityEvent_3_FindMethod_Impl_m7623_MethodInfo;
extern MethodInfo UnityEvent_3_GetDelegate_m7624_MethodInfo;
static Il2CppMethodReference UnityEvent_3_t1714_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m7623_MethodInfo,
	&UnityEvent_3_GetDelegate_m7624_MethodInfo,
};
static bool UnityEvent_3_t1714_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t1714_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppType UnityEvent_3_t1714_gp_0_0_0_0;
extern Il2CppType UnityEvent_3_t1714_gp_1_0_0_0;
extern Il2CppType UnityEvent_3_t1714_gp_2_0_0_0;
extern Il2CppType InvokableCall_3_t1783_0_0_0;
extern Il2CppGenericMethod InvokableCall_3__ctor_m7668_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_3_t1714_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_3_t1714_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_3_t1714_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_3_t1714_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_3_t1783_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_3__ctor_m7668_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_3_t1714_0_0_0;
extern Il2CppType UnityEvent_3_t1714_1_0_0;
struct UnityEvent_3_t1714;
const Il2CppTypeDefinitionMetadata UnityEvent_3_t1714_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_3_t1714_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1655_0_0_0/* parent */
	, UnityEvent_3_t1714_VTable/* vtableMethods */
	, UnityEvent_3_t1714_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_3_t1714_RGCTXData/* rgctxDefinition */

};
TypeInfo UnityEvent_3_t1714_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t1714_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_3_t1714_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEvent_3_t1714_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_3_t1714_0_0_0/* byval_arg */
	, &UnityEvent_3_t1714_1_0_0/* this_arg */
	, &UnityEvent_3_t1714_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_3_t1714_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`4
extern TypeInfo UnityEvent_4_t1715_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityEvent_4_t1715_Il2CppGenericContainer;
extern TypeInfo UnityEvent_4_t1715_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1715_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1715_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1715_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1715_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1715_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1715_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1715_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1715_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1715_gp_T3_3_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1715_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1715_Il2CppGenericContainer, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_4_t1715_Il2CppGenericParametersArray[4] = 
{
	&UnityEvent_4_t1715_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1715_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1715_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1715_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityEvent_4_t1715_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_4_t1715_il2cpp_TypeInfo, 4, 0, UnityEvent_4_t1715_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`4::.ctor()
MethodInfo UnityEvent_4__ctor_m7625_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1715_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_4_t1715_UnityEvent_4_FindMethod_Impl_m7626_ParameterInfos[] = 
{
	{"name", 0, 134219549, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219550, 0, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_4_FindMethod_Impl_m7626_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1715_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1715_UnityEvent_4_FindMethod_Impl_m7626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t_0_0_0;
static ParameterInfo UnityEvent_4_t1715_UnityEvent_4_GetDelegate_m7627_ParameterInfos[] = 
{
	{"target", 0, 134219551, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219552, 0, &MethodInfo_t_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1647_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_4_GetDelegate_m7627_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1715_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1647_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1715_UnityEvent_4_GetDelegate_m7627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_4_t1715_MethodInfos[] =
{
	&UnityEvent_4__ctor_m7625_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m7626_MethodInfo,
	&UnityEvent_4_GetDelegate_m7627_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_33;
FieldInfo UnityEvent_4_t1715____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t208_0_0_33/* type */
	, &UnityEvent_4_t1715_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_4_t1715_FieldInfos[] =
{
	&UnityEvent_4_t1715____m_InvokeArray_4_FieldInfo,
	NULL
};
extern MethodInfo UnityEvent_4_FindMethod_Impl_m7626_MethodInfo;
extern MethodInfo UnityEvent_4_GetDelegate_m7627_MethodInfo;
static Il2CppMethodReference UnityEvent_4_t1715_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m7626_MethodInfo,
	&UnityEvent_4_GetDelegate_m7627_MethodInfo,
};
static bool UnityEvent_4_t1715_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t1715_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppType UnityEvent_4_t1715_gp_0_0_0_0;
extern Il2CppType UnityEvent_4_t1715_gp_1_0_0_0;
extern Il2CppType UnityEvent_4_t1715_gp_2_0_0_0;
extern Il2CppType UnityEvent_4_t1715_gp_3_0_0_0;
extern Il2CppType InvokableCall_4_t1788_0_0_0;
extern Il2CppGenericMethod InvokableCall_4__ctor_m7669_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_4_t1715_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_4_t1715_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_4_t1715_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_4_t1715_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, &UnityEvent_4_t1715_gp_3_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, &InvokableCall_4_t1788_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_4__ctor_m7669_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_4_t1715_0_0_0;
extern Il2CppType UnityEvent_4_t1715_1_0_0;
struct UnityEvent_4_t1715;
const Il2CppTypeDefinitionMetadata UnityEvent_4_t1715_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_4_t1715_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1655_0_0_0/* parent */
	, UnityEvent_4_t1715_VTable/* vtableMethods */
	, UnityEvent_4_t1715_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_4_t1715_RGCTXData/* rgctxDefinition */

};
TypeInfo UnityEvent_4_t1715_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t1715_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_4_t1715_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEvent_4_t1715_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_4_t1715_0_0_0/* byval_arg */
	, &UnityEvent_4_t1715_1_0_0/* this_arg */
	, &UnityEvent_4_t1715_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_4_t1715_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern TypeInfo UserAuthorizationDialog_t1656_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
MethodInfo UserAuthorizationDialog__ctor_m7460_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserAuthorizationDialog__ctor_m7460/* method */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::Start()
MethodInfo UserAuthorizationDialog_Start_m7461_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserAuthorizationDialog_Start_m7461/* method */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
MethodInfo UserAuthorizationDialog_OnGUI_m7462_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&UserAuthorizationDialog_OnGUI_m7462/* method */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo UserAuthorizationDialog_t1656_UserAuthorizationDialog_DoUserAuthorizationDialog_m7463_ParameterInfos[] = 
{
	{"windowID", 0, 134219553, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m7463_MethodInfo = 
{
	"DoUserAuthorizationDialog"/* name */
	, (methodPointerType)&UserAuthorizationDialog_DoUserAuthorizationDialog_m7463/* method */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, UserAuthorizationDialog_t1656_UserAuthorizationDialog_DoUserAuthorizationDialog_m7463_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserAuthorizationDialog_t1656_MethodInfos[] =
{
	&UserAuthorizationDialog__ctor_m7460_MethodInfo,
	&UserAuthorizationDialog_Start_m7461_MethodInfo,
	&UserAuthorizationDialog_OnGUI_m7462_MethodInfo,
	&UserAuthorizationDialog_DoUserAuthorizationDialog_m7463_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_32849;
FieldInfo UserAuthorizationDialog_t1656____width_2_FieldInfo = 
{
	"width"/* name */
	, &Int32_t189_0_0_32849/* type */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_32849;
FieldInfo UserAuthorizationDialog_t1656____height_3_FieldInfo = 
{
	"height"/* name */
	, &Int32_t189_0_0_32849/* type */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo UserAuthorizationDialog_t1656____windowRect_4_FieldInfo = 
{
	"windowRect"/* name */
	, &Rect_t738_0_0_1/* type */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* parent */
	, offsetof(UserAuthorizationDialog_t1656, ___windowRect_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Texture_t736_0_0_1;
FieldInfo UserAuthorizationDialog_t1656____warningIcon_5_FieldInfo = 
{
	"warningIcon"/* name */
	, &Texture_t736_0_0_1/* type */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* parent */
	, offsetof(UserAuthorizationDialog_t1656, ___warningIcon_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UserAuthorizationDialog_t1656_FieldInfos[] =
{
	&UserAuthorizationDialog_t1656____width_2_FieldInfo,
	&UserAuthorizationDialog_t1656____height_3_FieldInfo,
	&UserAuthorizationDialog_t1656____windowRect_4_FieldInfo,
	&UserAuthorizationDialog_t1656____warningIcon_5_FieldInfo,
	NULL
};
static const int32_t UserAuthorizationDialog_t1656____width_2_DefaultValueData = 385;
static Il2CppFieldDefaultValueEntry UserAuthorizationDialog_t1656____width_2_DefaultValue = 
{
	&UserAuthorizationDialog_t1656____width_2_FieldInfo/* field */
	, { (char*)&UserAuthorizationDialog_t1656____width_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t UserAuthorizationDialog_t1656____height_3_DefaultValueData = 155;
static Il2CppFieldDefaultValueEntry UserAuthorizationDialog_t1656____height_3_DefaultValue = 
{
	&UserAuthorizationDialog_t1656____height_3_FieldInfo/* field */
	, { (char*)&UserAuthorizationDialog_t1656____height_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserAuthorizationDialog_t1656_FieldDefaultValues[] = 
{
	&UserAuthorizationDialog_t1656____width_2_DefaultValue,
	&UserAuthorizationDialog_t1656____height_3_DefaultValue,
	NULL
};
static Il2CppMethodReference UserAuthorizationDialog_t1656_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool UserAuthorizationDialog_t1656_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserAuthorizationDialog_t1656_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1656_1_0_0;
extern Il2CppType MonoBehaviour_t26_0_0_0;
struct UserAuthorizationDialog_t1656;
const Il2CppTypeDefinitionMetadata UserAuthorizationDialog_t1656_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, UserAuthorizationDialog_t1656_VTable/* vtableMethods */
	, UserAuthorizationDialog_t1656_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UserAuthorizationDialog_t1656_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, UserAuthorizationDialog_t1656_MethodInfos/* methods */
	, NULL/* properties */
	, UserAuthorizationDialog_t1656_FieldInfos/* fields */
	, NULL/* events */
	, &UserAuthorizationDialog_t1656_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 660/* custom_attributes_cache */
	, &UserAuthorizationDialog_t1656_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t1656_1_0_0/* this_arg */
	, &UserAuthorizationDialog_t1656_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserAuthorizationDialog_t1656_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t1656)/* instance_size */
	, sizeof (UserAuthorizationDialog_t1656)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t1657_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo DefaultValueAttribute_t1657_DefaultValueAttribute__ctor_m7464_ParameterInfos[] = 
{
	{"value", 0, 134219554, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
MethodInfo DefaultValueAttribute__ctor_m7464_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultValueAttribute__ctor_m7464/* method */
	, &DefaultValueAttribute_t1657_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, DefaultValueAttribute_t1657_DefaultValueAttribute__ctor_m7464_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
MethodInfo DefaultValueAttribute_get_Value_m7465_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DefaultValueAttribute_get_Value_m7465/* method */
	, &DefaultValueAttribute_t1657_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo DefaultValueAttribute_t1657_DefaultValueAttribute_Equals_m7466_ParameterInfos[] = 
{
	{"obj", 0, 134219555, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
MethodInfo DefaultValueAttribute_Equals_m7466_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultValueAttribute_Equals_m7466/* method */
	, &DefaultValueAttribute_t1657_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, DefaultValueAttribute_t1657_DefaultValueAttribute_Equals_m7466_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
MethodInfo DefaultValueAttribute_GetHashCode_m7467_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultValueAttribute_GetHashCode_m7467/* method */
	, &DefaultValueAttribute_t1657_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DefaultValueAttribute_t1657_MethodInfos[] =
{
	&DefaultValueAttribute__ctor_m7464_MethodInfo,
	&DefaultValueAttribute_get_Value_m7465_MethodInfo,
	&DefaultValueAttribute_Equals_m7466_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m7467_MethodInfo,
	NULL
};
extern Il2CppType Object_t_0_0_1;
FieldInfo DefaultValueAttribute_t1657____DefaultValue_0_FieldInfo = 
{
	"DefaultValue"/* name */
	, &Object_t_0_0_1/* type */
	, &DefaultValueAttribute_t1657_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultValueAttribute_t1657, ___DefaultValue_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* DefaultValueAttribute_t1657_FieldInfos[] =
{
	&DefaultValueAttribute_t1657____DefaultValue_0_FieldInfo,
	NULL
};
extern MethodInfo DefaultValueAttribute_get_Value_m7465_MethodInfo;
static PropertyInfo DefaultValueAttribute_t1657____Value_PropertyInfo = 
{
	&DefaultValueAttribute_t1657_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DefaultValueAttribute_get_Value_m7465_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* DefaultValueAttribute_t1657_PropertyInfos[] =
{
	&DefaultValueAttribute_t1657____Value_PropertyInfo,
	NULL
};
extern MethodInfo DefaultValueAttribute_Equals_m7466_MethodInfo;
extern MethodInfo DefaultValueAttribute_GetHashCode_m7467_MethodInfo;
static Il2CppMethodReference DefaultValueAttribute_t1657_VTable[] =
{
	&DefaultValueAttribute_Equals_m7466_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m7467_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool DefaultValueAttribute_t1657_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1657_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType DefaultValueAttribute_t1657_0_0_0;
extern Il2CppType DefaultValueAttribute_t1657_1_0_0;
struct DefaultValueAttribute_t1657;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t1657_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t1657_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, DefaultValueAttribute_t1657_VTable/* vtableMethods */
	, DefaultValueAttribute_t1657_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo DefaultValueAttribute_t1657_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, DefaultValueAttribute_t1657_MethodInfos/* methods */
	, DefaultValueAttribute_t1657_PropertyInfos/* properties */
	, DefaultValueAttribute_t1657_FieldInfos/* fields */
	, NULL/* events */
	, &DefaultValueAttribute_t1657_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 661/* custom_attributes_cache */
	, &DefaultValueAttribute_t1657_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1657_1_0_0/* this_arg */
	, &DefaultValueAttribute_t1657_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1657)/* instance_size */
	, sizeof (DefaultValueAttribute_t1657)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern TypeInfo ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
MethodInfo ExcludeFromDocsAttribute__ctor_m7468_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExcludeFromDocsAttribute__ctor_m7468/* method */
	, &ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ExcludeFromDocsAttribute_t1658_MethodInfos[] =
{
	&ExcludeFromDocsAttribute__ctor_m7468_MethodInfo,
	NULL
};
static Il2CppMethodReference ExcludeFromDocsAttribute_t1658_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ExcludeFromDocsAttribute_t1658_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t1658_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ExcludeFromDocsAttribute_t1658_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1658_1_0_0;
struct ExcludeFromDocsAttribute_t1658;
const Il2CppTypeDefinitionMetadata ExcludeFromDocsAttribute_t1658_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExcludeFromDocsAttribute_t1658_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, ExcludeFromDocsAttribute_t1658_VTable/* vtableMethods */
	, ExcludeFromDocsAttribute_t1658_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, ExcludeFromDocsAttribute_t1658_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 662/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t1658_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t1658_1_0_0/* this_arg */
	, &ExcludeFromDocsAttribute_t1658_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t1658)/* instance_size */
	, sizeof (ExcludeFromDocsAttribute_t1658)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern TypeInfo FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo FormerlySerializedAsAttribute_t1430_FormerlySerializedAsAttribute__ctor_m6202_ParameterInfos[] = 
{
	{"oldName", 0, 134219556, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
MethodInfo FormerlySerializedAsAttribute__ctor_m6202_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormerlySerializedAsAttribute__ctor_m6202/* method */
	, &FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, FormerlySerializedAsAttribute_t1430_FormerlySerializedAsAttribute__ctor_m6202_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FormerlySerializedAsAttribute_t1430_MethodInfos[] =
{
	&FormerlySerializedAsAttribute__ctor_m6202_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo FormerlySerializedAsAttribute_t1430____m_oldName_0_FieldInfo = 
{
	"m_oldName"/* name */
	, &String_t_0_0_1/* type */
	, &FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo/* parent */
	, offsetof(FormerlySerializedAsAttribute_t1430, ___m_oldName_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* FormerlySerializedAsAttribute_t1430_FieldInfos[] =
{
	&FormerlySerializedAsAttribute_t1430____m_oldName_0_FieldInfo,
	NULL
};
static Il2CppMethodReference FormerlySerializedAsAttribute_t1430_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool FormerlySerializedAsAttribute_t1430_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t1430_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType FormerlySerializedAsAttribute_t1430_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t1430_1_0_0;
struct FormerlySerializedAsAttribute_t1430;
const Il2CppTypeDefinitionMetadata FormerlySerializedAsAttribute_t1430_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormerlySerializedAsAttribute_t1430_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, FormerlySerializedAsAttribute_t1430_VTable/* vtableMethods */
	, FormerlySerializedAsAttribute_t1430_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, FormerlySerializedAsAttribute_t1430_MethodInfos/* methods */
	, NULL/* properties */
	, FormerlySerializedAsAttribute_t1430_FieldInfos/* fields */
	, NULL/* events */
	, &FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 663/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t1430_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t1430_1_0_0/* this_arg */
	, &FormerlySerializedAsAttribute_t1430_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t1430)/* instance_size */
	, sizeof (FormerlySerializedAsAttribute_t1430)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern TypeInfo TypeInferenceRules_t1659_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
static MethodInfo* TypeInferenceRules_t1659_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo TypeInferenceRules_t1659____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &TypeInferenceRules_t1659_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInferenceRules_t1659, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1659_0_0_32854;
FieldInfo TypeInferenceRules_t1659____TypeReferencedByFirstArgument_2_FieldInfo = 
{
	"TypeReferencedByFirstArgument"/* name */
	, &TypeInferenceRules_t1659_0_0_32854/* type */
	, &TypeInferenceRules_t1659_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1659_0_0_32854;
FieldInfo TypeInferenceRules_t1659____TypeReferencedBySecondArgument_3_FieldInfo = 
{
	"TypeReferencedBySecondArgument"/* name */
	, &TypeInferenceRules_t1659_0_0_32854/* type */
	, &TypeInferenceRules_t1659_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1659_0_0_32854;
FieldInfo TypeInferenceRules_t1659____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo = 
{
	"ArrayOfTypeReferencedByFirstArgument"/* name */
	, &TypeInferenceRules_t1659_0_0_32854/* type */
	, &TypeInferenceRules_t1659_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1659_0_0_32854;
FieldInfo TypeInferenceRules_t1659____TypeOfFirstArgument_5_FieldInfo = 
{
	"TypeOfFirstArgument"/* name */
	, &TypeInferenceRules_t1659_0_0_32854/* type */
	, &TypeInferenceRules_t1659_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TypeInferenceRules_t1659_FieldInfos[] =
{
	&TypeInferenceRules_t1659____value___1_FieldInfo,
	&TypeInferenceRules_t1659____TypeReferencedByFirstArgument_2_FieldInfo,
	&TypeInferenceRules_t1659____TypeReferencedBySecondArgument_3_FieldInfo,
	&TypeInferenceRules_t1659____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo,
	&TypeInferenceRules_t1659____TypeOfFirstArgument_5_FieldInfo,
	NULL
};
static const int32_t TypeInferenceRules_t1659____TypeReferencedByFirstArgument_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1659____TypeReferencedByFirstArgument_2_DefaultValue = 
{
	&TypeInferenceRules_t1659____TypeReferencedByFirstArgument_2_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1659____TypeReferencedByFirstArgument_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1659____TypeReferencedBySecondArgument_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1659____TypeReferencedBySecondArgument_3_DefaultValue = 
{
	&TypeInferenceRules_t1659____TypeReferencedBySecondArgument_3_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1659____TypeReferencedBySecondArgument_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1659____ArrayOfTypeReferencedByFirstArgument_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1659____ArrayOfTypeReferencedByFirstArgument_4_DefaultValue = 
{
	&TypeInferenceRules_t1659____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1659____ArrayOfTypeReferencedByFirstArgument_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1659____TypeOfFirstArgument_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1659____TypeOfFirstArgument_5_DefaultValue = 
{
	&TypeInferenceRules_t1659____TypeOfFirstArgument_5_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1659____TypeOfFirstArgument_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TypeInferenceRules_t1659_FieldDefaultValues[] = 
{
	&TypeInferenceRules_t1659____TypeReferencedByFirstArgument_2_DefaultValue,
	&TypeInferenceRules_t1659____TypeReferencedBySecondArgument_3_DefaultValue,
	&TypeInferenceRules_t1659____ArrayOfTypeReferencedByFirstArgument_4_DefaultValue,
	&TypeInferenceRules_t1659____TypeOfFirstArgument_5_DefaultValue,
	NULL
};
static Il2CppMethodReference TypeInferenceRules_t1659_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool TypeInferenceRules_t1659_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t1659_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TypeInferenceRules_t1659_0_0_0;
extern Il2CppType TypeInferenceRules_t1659_1_0_0;
const Il2CppTypeDefinitionMetadata TypeInferenceRules_t1659_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRules_t1659_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, TypeInferenceRules_t1659_VTable/* vtableMethods */
	, TypeInferenceRules_t1659_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TypeInferenceRules_t1659_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRules_t1659_MethodInfos/* methods */
	, NULL/* properties */
	, TypeInferenceRules_t1659_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInferenceRules_t1659_0_0_0/* byval_arg */
	, &TypeInferenceRules_t1659_1_0_0/* this_arg */
	, &TypeInferenceRules_t1659_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TypeInferenceRules_t1659_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t1659)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeInferenceRules_t1659)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern TypeInfo TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern Il2CppType TypeInferenceRules_t1659_0_0_0;
static ParameterInfo TypeInferenceRuleAttribute_t1660_TypeInferenceRuleAttribute__ctor_m7469_ParameterInfos[] = 
{
	{"rule", 0, 134219557, 0, &TypeInferenceRules_t1659_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
MethodInfo TypeInferenceRuleAttribute__ctor_m7469_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m7469/* method */
	, &TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, TypeInferenceRuleAttribute_t1660_TypeInferenceRuleAttribute__ctor_m7469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TypeInferenceRuleAttribute_t1660_TypeInferenceRuleAttribute__ctor_m7470_ParameterInfos[] = 
{
	{"rule", 0, 134219558, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
MethodInfo TypeInferenceRuleAttribute__ctor_m7470_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m7470/* method */
	, &TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, TypeInferenceRuleAttribute_t1660_TypeInferenceRuleAttribute__ctor_m7470_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
MethodInfo TypeInferenceRuleAttribute_ToString_m7471_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute_ToString_m7471/* method */
	, &TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TypeInferenceRuleAttribute_t1660_MethodInfos[] =
{
	&TypeInferenceRuleAttribute__ctor_m7469_MethodInfo,
	&TypeInferenceRuleAttribute__ctor_m7470_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m7471_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_33;
FieldInfo TypeInferenceRuleAttribute_t1660_____rule_0_FieldInfo = 
{
	"_rule"/* name */
	, &String_t_0_0_33/* type */
	, &TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInferenceRuleAttribute_t1660, ____rule_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TypeInferenceRuleAttribute_t1660_FieldInfos[] =
{
	&TypeInferenceRuleAttribute_t1660_____rule_0_FieldInfo,
	NULL
};
extern MethodInfo TypeInferenceRuleAttribute_ToString_m7471_MethodInfo;
static Il2CppMethodReference TypeInferenceRuleAttribute_t1660_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m7471_MethodInfo,
};
static bool TypeInferenceRuleAttribute_t1660_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t1660_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TypeInferenceRuleAttribute_t1660_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1660_1_0_0;
struct TypeInferenceRuleAttribute_t1660;
const Il2CppTypeDefinitionMetadata TypeInferenceRuleAttribute_t1660_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRuleAttribute_t1660_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, TypeInferenceRuleAttribute_t1660_VTable/* vtableMethods */
	, TypeInferenceRuleAttribute_t1660_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRuleAttribute_t1660_MethodInfos/* methods */
	, NULL/* properties */
	, TypeInferenceRuleAttribute_t1660_FieldInfos/* fields */
	, NULL/* events */
	, &TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 664/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t1660_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t1660_1_0_0/* this_arg */
	, &TypeInferenceRuleAttribute_t1660_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t1660)/* instance_size */
	, sizeof (TypeInferenceRuleAttribute_t1660)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// Metadata Definition UnityEngineInternal.GenericStack
extern TypeInfo GenericStack_t1517_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.GenericStack::.ctor()
MethodInfo GenericStack__ctor_m7472_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericStack__ctor_m7472/* method */
	, &GenericStack_t1517_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GenericStack_t1517_MethodInfos[] =
{
	&GenericStack__ctor_m7472_MethodInfo,
	NULL
};
extern MethodInfo Stack_GetEnumerator_m7670_MethodInfo;
extern MethodInfo Stack_get_Count_m7671_MethodInfo;
extern MethodInfo Stack_get_IsSynchronized_m7672_MethodInfo;
extern MethodInfo Stack_get_SyncRoot_m7673_MethodInfo;
extern MethodInfo Stack_CopyTo_m7674_MethodInfo;
extern MethodInfo Stack_Clear_m7675_MethodInfo;
extern MethodInfo Stack_Peek_m7676_MethodInfo;
extern MethodInfo Stack_Pop_m7677_MethodInfo;
extern MethodInfo Stack_Push_m7678_MethodInfo;
static Il2CppMethodReference GenericStack_t1517_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Stack_GetEnumerator_m7670_MethodInfo,
	&Stack_get_Count_m7671_MethodInfo,
	&Stack_get_IsSynchronized_m7672_MethodInfo,
	&Stack_get_SyncRoot_m7673_MethodInfo,
	&Stack_CopyTo_m7674_MethodInfo,
	&Stack_get_Count_m7671_MethodInfo,
	&Stack_get_IsSynchronized_m7672_MethodInfo,
	&Stack_get_SyncRoot_m7673_MethodInfo,
	&Stack_Clear_m7675_MethodInfo,
	&Stack_CopyTo_m7674_MethodInfo,
	&Stack_GetEnumerator_m7670_MethodInfo,
	&Stack_Peek_m7676_MethodInfo,
	&Stack_Pop_m7677_MethodInfo,
	&Stack_Push_m7678_MethodInfo,
};
static bool GenericStack_t1517_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerable_t38_0_0_0;
extern Il2CppType ICloneable_t309_0_0_0;
extern Il2CppType ICollection_t1789_0_0_0;
static Il2CppInterfaceOffsetPair GenericStack_t1517_InterfacesOffsets[] = 
{
	{ &IEnumerable_t38_0_0_0, 4},
	{ &ICloneable_t309_0_0_0, 5},
	{ &ICollection_t1789_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GenericStack_t1517_0_0_0;
extern Il2CppType GenericStack_t1517_1_0_0;
extern Il2CppType Stack_t1661_0_0_0;
struct GenericStack_t1517;
const Il2CppTypeDefinitionMetadata GenericStack_t1517_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericStack_t1517_InterfacesOffsets/* interfaceOffsets */
	, &Stack_t1661_0_0_0/* parent */
	, GenericStack_t1517_VTable/* vtableMethods */
	, GenericStack_t1517_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo GenericStack_t1517_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, GenericStack_t1517_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &GenericStack_t1517_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericStack_t1517_0_0_0/* byval_arg */
	, &GenericStack_t1517_1_0_0/* this_arg */
	, &GenericStack_t1517_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t1517)/* instance_size */
	, sizeof (GenericStack_t1517)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// Metadata Definition UnityEngine.Events.UnityAction
extern TypeInfo UnityAction_t1230_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo UnityAction_t1230_UnityAction__ctor_m5978_ParameterInfos[] = 
{
	{"object", 0, 134219559, 0, &Object_t_0_0_0},
	{"method", 1, 134219560, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction__ctor_m5978_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction__ctor_m5978/* method */
	, &UnityAction_t1230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, UnityAction_t1230_UnityAction__ctor_m5978_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::Invoke()
MethodInfo UnityAction_Invoke_m7473_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_Invoke_m7473/* method */
	, &UnityAction_t1230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_t1230_UnityAction_BeginInvoke_m7474_ParameterInfos[] = 
{
	{"callback", 0, 134219561, 0, &AsyncCallback_t20_0_0_0},
	{"object", 1, 134219562, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
MethodInfo UnityAction_BeginInvoke_m7474_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_BeginInvoke_m7474/* method */
	, &UnityAction_t1230_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_t1230_UnityAction_BeginInvoke_m7474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo UnityAction_t1230_UnityAction_EndInvoke_m7475_ParameterInfos[] = 
{
	{"result", 0, 134219563, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_EndInvoke_m7475_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_EndInvoke_m7475/* method */
	, &UnityAction_t1230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UnityAction_t1230_UnityAction_EndInvoke_m7475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_t1230_MethodInfos[] =
{
	&UnityAction__ctor_m5978_MethodInfo,
	&UnityAction_Invoke_m7473_MethodInfo,
	&UnityAction_BeginInvoke_m7474_MethodInfo,
	&UnityAction_EndInvoke_m7475_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m1272_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m1273_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m1274_MethodInfo;
extern MethodInfo Delegate_Clone_m1275_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m1276_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m1277_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m1278_MethodInfo;
extern MethodInfo UnityAction_Invoke_m7473_MethodInfo;
extern MethodInfo UnityAction_BeginInvoke_m7474_MethodInfo;
extern MethodInfo UnityAction_EndInvoke_m7475_MethodInfo;
static Il2CppMethodReference UnityAction_t1230_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&UnityAction_Invoke_m7473_MethodInfo,
	&UnityAction_BeginInvoke_m7474_MethodInfo,
	&UnityAction_EndInvoke_m7475_MethodInfo,
};
static bool UnityAction_t1230_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_t1230_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_t1230_0_0_0;
extern Il2CppType UnityAction_t1230_1_0_0;
extern Il2CppType MulticastDelegate_t22_0_0_0;
struct UnityAction_t1230;
const Il2CppTypeDefinitionMetadata UnityAction_t1230_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_t1230_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, UnityAction_t1230_VTable/* vtableMethods */
	, UnityAction_t1230_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAction_t1230_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_t1230_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAction_t1230_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_t1230_0_0_0/* byval_arg */
	, &UnityAction_t1230_1_0_0/* this_arg */
	, &UnityAction_t1230_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t1230/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t1230)/* instance_size */
	, sizeof (UnityAction_t1230)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`1
extern TypeInfo UnityAction_1_t1716_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityAction_1_t1716_Il2CppGenericContainer;
extern TypeInfo UnityAction_1_t1716_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_1_t1716_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_1_t1716_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_1_t1716_Il2CppGenericParametersArray[1] = 
{
	&UnityAction_1_t1716_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityAction_1_t1716_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_1_t1716_il2cpp_TypeInfo, 1, 0, UnityAction_1_t1716_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo UnityAction_1_t1716_UnityAction_1__ctor_m7628_ParameterInfos[] = 
{
	{"object", 0, 134219564, 0, &Object_t_0_0_0},
	{"method", 1, 134219565, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m7628_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_1_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1716_UnityAction_1__ctor_m7628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1716_gp_0_0_0_0;
extern Il2CppType UnityAction_1_t1716_gp_0_0_0_0;
static ParameterInfo UnityAction_1_t1716_UnityAction_1_Invoke_m7629_ParameterInfos[] = 
{
	{"arg0", 0, 134219566, 0, &UnityAction_1_t1716_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m7629_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1716_UnityAction_1_Invoke_m7629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1716_gp_0_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t1716_UnityAction_1_BeginInvoke_m7630_ParameterInfos[] = 
{
	{"arg0", 0, 134219567, 0, &UnityAction_1_t1716_gp_0_0_0_0},
	{"callback", 1, 134219568, 0, &AsyncCallback_t20_0_0_0},
	{"object", 2, 134219569, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m7630_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1716_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1716_UnityAction_1_BeginInvoke_m7630_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo UnityAction_1_t1716_UnityAction_1_EndInvoke_m7631_ParameterInfos[] = 
{
	{"result", 0, 134219570, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m7631_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1716_UnityAction_1_EndInvoke_m7631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_1_t1716_MethodInfos[] =
{
	&UnityAction_1__ctor_m7628_MethodInfo,
	&UnityAction_1_Invoke_m7629_MethodInfo,
	&UnityAction_1_BeginInvoke_m7630_MethodInfo,
	&UnityAction_1_EndInvoke_m7631_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_Invoke_m7629_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m7630_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m7631_MethodInfo;
static Il2CppMethodReference UnityAction_1_t1716_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&UnityAction_1_Invoke_m7629_MethodInfo,
	&UnityAction_1_BeginInvoke_m7630_MethodInfo,
	&UnityAction_1_EndInvoke_m7631_MethodInfo,
};
static bool UnityAction_1_t1716_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t1716_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t1716_0_0_0;
extern Il2CppType UnityAction_1_t1716_1_0_0;
struct UnityAction_1_t1716;
const Il2CppTypeDefinitionMetadata UnityAction_1_t1716_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_1_t1716_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, UnityAction_1_t1716_VTable/* vtableMethods */
	, UnityAction_1_t1716_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAction_1_t1716_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t1716_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAction_1_t1716_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_1_t1716_0_0_0/* byval_arg */
	, &UnityAction_1_t1716_1_0_0/* this_arg */
	, &UnityAction_1_t1716_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_1_t1716_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`2
extern TypeInfo UnityAction_2_t1717_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityAction_2_t1717_Il2CppGenericContainer;
extern TypeInfo UnityAction_2_t1717_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_2_t1717_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_2_t1717_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_2_t1717_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_2_t1717_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_2_t1717_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_2_t1717_Il2CppGenericParametersArray[2] = 
{
	&UnityAction_2_t1717_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_2_t1717_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityAction_2_t1717_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_2_t1717_il2cpp_TypeInfo, 2, 0, UnityAction_2_t1717_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo UnityAction_2_t1717_UnityAction_2__ctor_m7632_ParameterInfos[] = 
{
	{"object", 0, 134219571, 0, &Object_t_0_0_0},
	{"method", 1, 134219572, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_2__ctor_m7632_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_2_t1717_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1717_UnityAction_2__ctor_m7632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_2_t1717_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1717_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1717_gp_1_0_0_0;
extern Il2CppType UnityAction_2_t1717_gp_1_0_0_0;
static ParameterInfo UnityAction_2_t1717_UnityAction_2_Invoke_m7633_ParameterInfos[] = 
{
	{"arg0", 0, 134219573, 0, &UnityAction_2_t1717_gp_0_0_0_0},
	{"arg1", 1, 134219574, 0, &UnityAction_2_t1717_gp_1_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
MethodInfo UnityAction_2_Invoke_m7633_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1717_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1717_UnityAction_2_Invoke_m7633_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_2_t1717_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1717_gp_1_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_2_t1717_UnityAction_2_BeginInvoke_m7634_ParameterInfos[] = 
{
	{"arg0", 0, 134219575, 0, &UnityAction_2_t1717_gp_0_0_0_0},
	{"arg1", 1, 134219576, 0, &UnityAction_2_t1717_gp_1_0_0_0},
	{"callback", 2, 134219577, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134219578, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
MethodInfo UnityAction_2_BeginInvoke_m7634_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1717_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1717_UnityAction_2_BeginInvoke_m7634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo UnityAction_2_t1717_UnityAction_2_EndInvoke_m7635_ParameterInfos[] = 
{
	{"result", 0, 134219579, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_2_EndInvoke_m7635_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1717_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1717_UnityAction_2_EndInvoke_m7635_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_2_t1717_MethodInfos[] =
{
	&UnityAction_2__ctor_m7632_MethodInfo,
	&UnityAction_2_Invoke_m7633_MethodInfo,
	&UnityAction_2_BeginInvoke_m7634_MethodInfo,
	&UnityAction_2_EndInvoke_m7635_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_2_Invoke_m7633_MethodInfo;
extern MethodInfo UnityAction_2_BeginInvoke_m7634_MethodInfo;
extern MethodInfo UnityAction_2_EndInvoke_m7635_MethodInfo;
static Il2CppMethodReference UnityAction_2_t1717_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&UnityAction_2_Invoke_m7633_MethodInfo,
	&UnityAction_2_BeginInvoke_m7634_MethodInfo,
	&UnityAction_2_EndInvoke_m7635_MethodInfo,
};
static bool UnityAction_2_t1717_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_2_t1717_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_2_t1717_0_0_0;
extern Il2CppType UnityAction_2_t1717_1_0_0;
struct UnityAction_2_t1717;
const Il2CppTypeDefinitionMetadata UnityAction_2_t1717_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_2_t1717_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, UnityAction_2_t1717_VTable/* vtableMethods */
	, UnityAction_2_t1717_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAction_2_t1717_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_2_t1717_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAction_2_t1717_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_2_t1717_0_0_0/* byval_arg */
	, &UnityAction_2_t1717_1_0_0/* this_arg */
	, &UnityAction_2_t1717_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_2_t1717_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`3
extern TypeInfo UnityAction_3_t1718_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityAction_3_t1718_Il2CppGenericContainer;
extern TypeInfo UnityAction_3_t1718_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1718_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1718_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_3_t1718_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1718_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1718_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityAction_3_t1718_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1718_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1718_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_3_t1718_Il2CppGenericParametersArray[3] = 
{
	&UnityAction_3_t1718_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1718_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1718_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityAction_3_t1718_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_3_t1718_il2cpp_TypeInfo, 3, 0, UnityAction_3_t1718_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo UnityAction_3_t1718_UnityAction_3__ctor_m7636_ParameterInfos[] = 
{
	{"object", 0, 134219580, 0, &Object_t_0_0_0},
	{"method", 1, 134219581, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_3__ctor_m7636_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_3_t1718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1718_UnityAction_3__ctor_m7636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_3_t1718_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1718_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1718_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1718_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1718_gp_2_0_0_0;
extern Il2CppType UnityAction_3_t1718_gp_2_0_0_0;
static ParameterInfo UnityAction_3_t1718_UnityAction_3_Invoke_m7637_ParameterInfos[] = 
{
	{"arg0", 0, 134219582, 0, &UnityAction_3_t1718_gp_0_0_0_0},
	{"arg1", 1, 134219583, 0, &UnityAction_3_t1718_gp_1_0_0_0},
	{"arg2", 2, 134219584, 0, &UnityAction_3_t1718_gp_2_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
MethodInfo UnityAction_3_Invoke_m7637_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1718_UnityAction_3_Invoke_m7637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_3_t1718_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1718_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1718_gp_2_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_3_t1718_UnityAction_3_BeginInvoke_m7638_ParameterInfos[] = 
{
	{"arg0", 0, 134219585, 0, &UnityAction_3_t1718_gp_0_0_0_0},
	{"arg1", 1, 134219586, 0, &UnityAction_3_t1718_gp_1_0_0_0},
	{"arg2", 2, 134219587, 0, &UnityAction_3_t1718_gp_2_0_0_0},
	{"callback", 3, 134219588, 0, &AsyncCallback_t20_0_0_0},
	{"object", 4, 134219589, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
MethodInfo UnityAction_3_BeginInvoke_m7638_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1718_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1718_UnityAction_3_BeginInvoke_m7638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo UnityAction_3_t1718_UnityAction_3_EndInvoke_m7639_ParameterInfos[] = 
{
	{"result", 0, 134219590, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_3_EndInvoke_m7639_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1718_UnityAction_3_EndInvoke_m7639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_3_t1718_MethodInfos[] =
{
	&UnityAction_3__ctor_m7636_MethodInfo,
	&UnityAction_3_Invoke_m7637_MethodInfo,
	&UnityAction_3_BeginInvoke_m7638_MethodInfo,
	&UnityAction_3_EndInvoke_m7639_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_3_Invoke_m7637_MethodInfo;
extern MethodInfo UnityAction_3_BeginInvoke_m7638_MethodInfo;
extern MethodInfo UnityAction_3_EndInvoke_m7639_MethodInfo;
static Il2CppMethodReference UnityAction_3_t1718_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&UnityAction_3_Invoke_m7637_MethodInfo,
	&UnityAction_3_BeginInvoke_m7638_MethodInfo,
	&UnityAction_3_EndInvoke_m7639_MethodInfo,
};
static bool UnityAction_3_t1718_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_3_t1718_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_3_t1718_0_0_0;
extern Il2CppType UnityAction_3_t1718_1_0_0;
struct UnityAction_3_t1718;
const Il2CppTypeDefinitionMetadata UnityAction_3_t1718_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_3_t1718_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, UnityAction_3_t1718_VTable/* vtableMethods */
	, UnityAction_3_t1718_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAction_3_t1718_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_3_t1718_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAction_3_t1718_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_3_t1718_0_0_0/* byval_arg */
	, &UnityAction_3_t1718_1_0_0/* this_arg */
	, &UnityAction_3_t1718_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_3_t1718_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`4
extern TypeInfo UnityAction_4_t1719_il2cpp_TypeInfo;
extern Il2CppGenericContainer UnityAction_4_t1719_Il2CppGenericContainer;
extern TypeInfo UnityAction_4_t1719_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1719_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1719_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1719_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1719_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1719_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1719_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1719_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1719_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1719_gp_T3_3_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1719_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1719_Il2CppGenericContainer, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_4_t1719_Il2CppGenericParametersArray[4] = 
{
	&UnityAction_4_t1719_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1719_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1719_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1719_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer UnityAction_4_t1719_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_4_t1719_il2cpp_TypeInfo, 4, 0, UnityAction_4_t1719_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo UnityAction_4_t1719_UnityAction_4__ctor_m7640_ParameterInfos[] = 
{
	{"object", 0, 134219591, 0, &Object_t_0_0_0},
	{"method", 1, 134219592, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_4__ctor_m7640_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_4_t1719_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1719_UnityAction_4__ctor_m7640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_4_t1719_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_3_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_3_0_0_0;
static ParameterInfo UnityAction_4_t1719_UnityAction_4_Invoke_m7641_ParameterInfos[] = 
{
	{"arg0", 0, 134219593, 0, &UnityAction_4_t1719_gp_0_0_0_0},
	{"arg1", 1, 134219594, 0, &UnityAction_4_t1719_gp_1_0_0_0},
	{"arg2", 2, 134219595, 0, &UnityAction_4_t1719_gp_2_0_0_0},
	{"arg3", 3, 134219596, 0, &UnityAction_4_t1719_gp_3_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
MethodInfo UnityAction_4_Invoke_m7641_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1719_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1719_UnityAction_4_Invoke_m7641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_4_t1719_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1719_gp_3_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_4_t1719_UnityAction_4_BeginInvoke_m7642_ParameterInfos[] = 
{
	{"arg0", 0, 134219597, 0, &UnityAction_4_t1719_gp_0_0_0_0},
	{"arg1", 1, 134219598, 0, &UnityAction_4_t1719_gp_1_0_0_0},
	{"arg2", 2, 134219599, 0, &UnityAction_4_t1719_gp_2_0_0_0},
	{"arg3", 3, 134219600, 0, &UnityAction_4_t1719_gp_3_0_0_0},
	{"callback", 4, 134219601, 0, &AsyncCallback_t20_0_0_0},
	{"object", 5, 134219602, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
MethodInfo UnityAction_4_BeginInvoke_m7642_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1719_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1719_UnityAction_4_BeginInvoke_m7642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo UnityAction_4_t1719_UnityAction_4_EndInvoke_m7643_ParameterInfos[] = 
{
	{"result", 0, 134219603, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_4_EndInvoke_m7643_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1719_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1719_UnityAction_4_EndInvoke_m7643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_4_t1719_MethodInfos[] =
{
	&UnityAction_4__ctor_m7640_MethodInfo,
	&UnityAction_4_Invoke_m7641_MethodInfo,
	&UnityAction_4_BeginInvoke_m7642_MethodInfo,
	&UnityAction_4_EndInvoke_m7643_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_4_Invoke_m7641_MethodInfo;
extern MethodInfo UnityAction_4_BeginInvoke_m7642_MethodInfo;
extern MethodInfo UnityAction_4_EndInvoke_m7643_MethodInfo;
static Il2CppMethodReference UnityAction_4_t1719_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&UnityAction_4_Invoke_m7641_MethodInfo,
	&UnityAction_4_BeginInvoke_m7642_MethodInfo,
	&UnityAction_4_EndInvoke_m7643_MethodInfo,
};
static bool UnityAction_4_t1719_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_4_t1719_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_4_t1719_0_0_0;
extern Il2CppType UnityAction_4_t1719_1_0_0;
struct UnityAction_4_t1719;
const Il2CppTypeDefinitionMetadata UnityAction_4_t1719_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_4_t1719_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, UnityAction_4_t1719_VTable/* vtableMethods */
	, UnityAction_4_t1719_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAction_4_t1719_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_4_t1719_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAction_4_t1719_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_4_t1719_0_0_0/* byval_arg */
	, &UnityAction_4_t1719_1_0_0/* this_arg */
	, &UnityAction_4_t1719_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_4_t1719_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
