﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpaceAttribute
struct SpaceAttribute_t1440;

// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C" void SpaceAttribute__ctor_m6244 (SpaceAttribute_t1440 * __this, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
