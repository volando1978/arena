﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct ConnectionResponse_t358;
struct ConnectionResponse_t358_marshaled;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_0.h"
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_1.h"

// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.ctor(System.Int64,System.String,GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status,System.Byte[])
extern "C" void ConnectionResponse__ctor_m1427 (ConnectionResponse_t358 * __this, int64_t ___localClientId, String_t* ___remoteEndpointId, int32_t ___code, ByteU5BU5D_t350* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.cctor()
extern "C" void ConnectionResponse__cctor_m1428 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_LocalClientId()
extern "C" int64_t ConnectionResponse_get_LocalClientId_m1429 (ConnectionResponse_t358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_RemoteEndpointId()
extern "C" String_t* ConnectionResponse_get_RemoteEndpointId_m1430 (ConnectionResponse_t358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_ResponseStatus()
extern "C" int32_t ConnectionResponse_get_ResponseStatus_m1431 (ConnectionResponse_t358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_Payload()
extern "C" ByteU5BU5D_t350* ConnectionResponse_get_Payload_m1432 (ConnectionResponse_t358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Rejected(System.Int64,System.String)
extern "C" ConnectionResponse_t358  ConnectionResponse_Rejected_m1433 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::NetworkNotConnected(System.Int64,System.String)
extern "C" ConnectionResponse_t358  ConnectionResponse_NetworkNotConnected_m1434 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::InternalError(System.Int64,System.String)
extern "C" ConnectionResponse_t358  ConnectionResponse_InternalError_m1435 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::EndpointNotConnected(System.Int64,System.String)
extern "C" ConnectionResponse_t358  ConnectionResponse_EndpointNotConnected_m1436 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Accepted(System.Int64,System.String,System.Byte[])
extern "C" ConnectionResponse_t358  ConnectionResponse_Accepted_m1437 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, ByteU5BU5D_t350* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::AlreadyConnected(System.Int64,System.String)
extern "C" ConnectionResponse_t358  ConnectionResponse_AlreadyConnected_m1438 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method) IL2CPP_METHOD_ATTR;
void ConnectionResponse_t358_marshal(const ConnectionResponse_t358& unmarshaled, ConnectionResponse_t358_marshaled& marshaled);
void ConnectionResponse_t358_marshal_back(const ConnectionResponse_t358_marshaled& marshaled, ConnectionResponse_t358& unmarshaled);
void ConnectionResponse_t358_marshal_cleanup(ConnectionResponse_t358_marshaled& marshaled);
