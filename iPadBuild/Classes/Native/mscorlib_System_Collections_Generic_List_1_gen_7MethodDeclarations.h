﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Store.VirtualItem>
struct List_1_t179;
// System.Object
struct Object_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.Collections.Generic.IEnumerable`1<Soomla.Store.VirtualItem>
struct IEnumerable_1_t4311;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.VirtualItem>
struct IEnumerator_1_t4312;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Store.VirtualItem>
struct ICollection_1_t4313;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.VirtualItem>
struct ReadOnlyCollection_1_t3566;
// Soomla.Store.VirtualItem[]
struct VirtualItemU5BU5D_t3564;
// System.Predicate`1<Soomla.Store.VirtualItem>
struct Predicate_1_t3567;
// System.Action`1<Soomla.Store.VirtualItem>
struct Action_1_t3568;
// System.Comparison`1<Soomla.Store.VirtualItem>
struct Comparison_1_t3569;
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m972(__this, method) (( void (*) (List_1_t179 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m17410(__this, ___collection, method) (( void (*) (List_1_t179 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::.ctor(System.Int32)
#define List_1__ctor_m17411(__this, ___capacity, method) (( void (*) (List_1_t179 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::.cctor()
#define List_1__cctor_m17412(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17413(__this, method) (( Object_t* (*) (List_1_t179 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m17414(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t179 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17415(__this, method) (( Object_t * (*) (List_1_t179 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m17416(__this, ___item, method) (( int32_t (*) (List_1_t179 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m17417(__this, ___item, method) (( bool (*) (List_1_t179 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m17418(__this, ___item, method) (( int32_t (*) (List_1_t179 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m17419(__this, ___index, ___item, method) (( void (*) (List_1_t179 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m17420(__this, ___item, method) (( void (*) (List_1_t179 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17421(__this, method) (( bool (*) (List_1_t179 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17422(__this, method) (( bool (*) (List_1_t179 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m17423(__this, method) (( Object_t * (*) (List_1_t179 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m17424(__this, method) (( bool (*) (List_1_t179 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m17425(__this, method) (( bool (*) (List_1_t179 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m17426(__this, ___index, method) (( Object_t * (*) (List_1_t179 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m17427(__this, ___index, ___value, method) (( void (*) (List_1_t179 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Add(T)
#define List_1_Add_m17428(__this, ___item, method) (( void (*) (List_1_t179 *, VirtualItem_t125 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m17429(__this, ___newCount, method) (( void (*) (List_1_t179 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m17430(__this, ___collection, method) (( void (*) (List_1_t179 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m17431(__this, ___enumerable, method) (( void (*) (List_1_t179 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m17432(__this, ___collection, method) (( void (*) (List_1_t179 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::AsReadOnly()
#define List_1_AsReadOnly_m17433(__this, method) (( ReadOnlyCollection_1_t3566 * (*) (List_1_t179 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Clear()
#define List_1_Clear_m17434(__this, method) (( void (*) (List_1_t179 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Contains(T)
#define List_1_Contains_m17435(__this, ___item, method) (( bool (*) (List_1_t179 *, VirtualItem_t125 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17436(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t179 *, VirtualItemU5BU5D_t3564*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m17437(__this, ___match, method) (( VirtualItem_t125 * (*) (List_1_t179 *, Predicate_1_t3567 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17438(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3567 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m17439(__this, ___match, method) (( int32_t (*) (List_1_t179 *, Predicate_1_t3567 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17440(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t179 *, int32_t, int32_t, Predicate_1_t3567 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m17441(__this, ___action, method) (( void (*) (List_1_t179 *, Action_1_t3568 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::GetEnumerator()
#define List_1_GetEnumerator_m1014(__this, method) (( Enumerator_t226  (*) (List_1_t179 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::IndexOf(T)
#define List_1_IndexOf_m17442(__this, ___item, method) (( int32_t (*) (List_1_t179 *, VirtualItem_t125 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17443(__this, ___start, ___delta, method) (( void (*) (List_1_t179 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17444(__this, ___index, method) (( void (*) (List_1_t179 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Insert(System.Int32,T)
#define List_1_Insert_m17445(__this, ___index, ___item, method) (( void (*) (List_1_t179 *, int32_t, VirtualItem_t125 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17446(__this, ___collection, method) (( void (*) (List_1_t179 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Remove(T)
#define List_1_Remove_m17447(__this, ___item, method) (( bool (*) (List_1_t179 *, VirtualItem_t125 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m17448(__this, ___match, method) (( int32_t (*) (List_1_t179 *, Predicate_1_t3567 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17449(__this, ___index, method) (( void (*) (List_1_t179 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Reverse()
#define List_1_Reverse_m17450(__this, method) (( void (*) (List_1_t179 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Sort()
#define List_1_Sort_m17451(__this, method) (( void (*) (List_1_t179 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17452(__this, ___comparison, method) (( void (*) (List_1_t179 *, Comparison_1_t3569 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::ToArray()
#define List_1_ToArray_m17453(__this, method) (( VirtualItemU5BU5D_t3564* (*) (List_1_t179 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::TrimExcess()
#define List_1_TrimExcess_m17454(__this, method) (( void (*) (List_1_t179 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::get_Capacity()
#define List_1_get_Capacity_m17455(__this, method) (( int32_t (*) (List_1_t179 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17456(__this, ___value, method) (( void (*) (List_1_t179 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::get_Count()
#define List_1_get_Count_m17457(__this, method) (( int32_t (*) (List_1_t179 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::get_Item(System.Int32)
#define List_1_get_Item_m17458(__this, ___index, method) (( VirtualItem_t125 * (*) (List_1_t179 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m17459(__this, ___index, ___value, method) (( void (*) (List_1_t179 *, int32_t, VirtualItem_t125 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
