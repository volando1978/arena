﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuest
struct NativeQuest_t677;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Quests.IQuestMilestone
struct IQuestMilestone_t863;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Nullable`1<System.DateTime>
#include "mscorlib_System_Nullable_1_gen_0.h"
// GooglePlayGames.BasicApi.Quests.QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestState.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.NativeQuest::.ctor(System.IntPtr)
extern "C" void NativeQuest__ctor_m2760 (NativeQuest_t677 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuest::get_Id()
extern "C" String_t* NativeQuest_get_Id_m2761 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuest::get_Name()
extern "C" String_t* NativeQuest_get_Name_m2762 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuest::get_Description()
extern "C" String_t* NativeQuest_get_Description_m2763 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuest::get_BannerUrl()
extern "C" String_t* NativeQuest_get_BannerUrl_m2764 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuest::get_IconUrl()
extern "C" String_t* NativeQuest_get_IconUrl_m2765 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.Native.NativeQuest::get_StartTime()
extern "C" DateTime_t48  NativeQuest_get_StartTime_m2766 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.Native.NativeQuest::get_ExpirationTime()
extern "C" DateTime_t48  NativeQuest_get_ExpirationTime_m2767 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> GooglePlayGames.Native.NativeQuest::get_AcceptedTime()
extern "C" Nullable_1_t873  NativeQuest_get_AcceptedTime_m2768 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.IQuestMilestone GooglePlayGames.Native.NativeQuest::get_Milestone()
extern "C" Object_t * NativeQuest_get_Milestone_m2769 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.QuestState GooglePlayGames.Native.NativeQuest::get_State()
extern "C" int32_t NativeQuest_get_State_m2770 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeQuest::Valid()
extern "C" bool NativeQuest_Valid_m2771 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuest::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeQuest_CallDispose_m2772 (NativeQuest_t677 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuest::ToString()
extern "C" String_t* NativeQuest_ToString_m2773 (NativeQuest_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.NativeQuest::FromPointer(System.IntPtr)
extern "C" NativeQuest_t677 * NativeQuest_FromPointer_m2774 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_Id>m__85(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_IdU3Em__85_m2775 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_Name>m__86(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_NameU3Em__86_m2776 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_Description>m__87(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_DescriptionU3Em__87_m2777 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_BannerUrl>m__88(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_BannerUrlU3Em__88_m2778 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuest::<get_IconUrl>m__89(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuest_U3Cget_IconUrlU3Em__89_m2779 (NativeQuest_t677 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
