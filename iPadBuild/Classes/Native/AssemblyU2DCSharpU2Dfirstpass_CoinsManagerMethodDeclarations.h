﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CoinsManager
struct CoinsManager_t72;

// System.Void CoinsManager::.ctor()
extern "C" void CoinsManager__ctor_m259 (CoinsManager_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoinsManager::.cctor()
extern "C" void CoinsManager__cctor_m260 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoinsManager::Start()
extern "C" void CoinsManager_Start_m261 (CoinsManager_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoinsManager::substractCoins(System.Int32)
extern "C" void CoinsManager_substractCoins_m262 (Object_t * __this /* static, unused */, int32_t ___coinsToSubstract, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoinsManager::addCoins(System.Int32)
extern "C" void CoinsManager_addCoins_m263 (Object_t * __this /* static, unused */, int32_t ___coinsToAdd, MethodInfo* method) IL2CPP_METHOD_ATTR;
