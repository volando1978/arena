﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>
struct InternalEnumerator_1_t4060;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24204_gshared (InternalEnumerator_1_t4060 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m24204(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4060 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m24204_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24205_gshared (InternalEnumerator_1_t4060 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24205(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4060 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24205_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24206_gshared (InternalEnumerator_1_t4060 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24206(__this, method) (( void (*) (InternalEnumerator_1_t4060 *, MethodInfo*))InternalEnumerator_1_Dispose_m24206_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24207_gshared (InternalEnumerator_1_t4060 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24207(__this, method) (( bool (*) (InternalEnumerator_1_t4060 *, MethodInfo*))InternalEnumerator_1_MoveNext_m24207_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C" GcAchievementData_t1617  InternalEnumerator_1_get_Current_m24208_gshared (InternalEnumerator_1_t4060 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24208(__this, method) (( GcAchievementData_t1617  (*) (InternalEnumerator_1_t4060 *, MethodInfo*))InternalEnumerator_1_get_Current_m24208_gshared)(__this, method)
