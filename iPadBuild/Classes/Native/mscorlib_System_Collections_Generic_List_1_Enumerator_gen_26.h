﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.IntPtr>
struct List_1_t3807;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.List`1/Enumerator<System.IntPtr>
struct  Enumerator_t3808 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.IntPtr>::l
	List_1_t3807 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.IntPtr>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.IntPtr>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.IntPtr>::current
	IntPtr_t ___current_3;
};
