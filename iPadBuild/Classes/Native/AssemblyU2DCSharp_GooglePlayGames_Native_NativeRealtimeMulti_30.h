﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37
struct U3CAcceptInvitationU3Ec__AnonStorey37_t605;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36
struct U3CAcceptInvitationU3Ec__AnonStorey36_t606;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38
struct  U3CAcceptInvitationU3Ec__AnonStorey38_t607  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38::invitation
	MultiplayerInvitation_t601 * ___invitation_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38::<>f__ref$55
	U3CAcceptInvitationU3Ec__AnonStorey37_t605 * ___U3CU3Ef__refU2455_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38::<>f__ref$54
	U3CAcceptInvitationU3Ec__AnonStorey36_t606 * ___U3CU3Ef__refU2454_2;
};
