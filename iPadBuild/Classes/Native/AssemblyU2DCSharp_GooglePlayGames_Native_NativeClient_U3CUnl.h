﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D
struct  U3CUnlockAchievementU3Ec__AnonStorey1D_t526  : public Object_t
{
	// System.String GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D::achId
	String_t* ___achId_0;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D::<>f__this
	NativeClient_t524 * ___U3CU3Ef__this_1;
};
