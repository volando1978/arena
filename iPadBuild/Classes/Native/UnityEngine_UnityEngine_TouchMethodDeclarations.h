﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Touch
struct Touch_t901;
struct Touch_t901_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"

// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m5784 (Touch_t901 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t739  Touch_get_position_m4100 (Touch_t901 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C" Vector2_t739  Touch_get_deltaPosition_m4099 (Touch_t901 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m4098 (Touch_t901 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Touch_t901_marshal(const Touch_t901& unmarshaled, Touch_t901_marshaled& marshaled);
void Touch_t901_marshal_back(const Touch_t901_marshaled& marshaled, Touch_t901& unmarshaled);
void Touch_t901_marshal_cleanup(Touch_t901_marshaled& marshaled);
