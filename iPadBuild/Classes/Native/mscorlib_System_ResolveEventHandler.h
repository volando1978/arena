﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t2144;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2834;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct  ResolveEventHandler_t2778  : public MulticastDelegate_t22
{
};
