﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.UnexceptionalStreamWriter
struct UnexceptionalStreamWriter_t2504;
// System.IO.Stream
struct Stream_t1685;
// System.Text.Encoding
struct Encoding_t1666;
// System.Char[]
struct CharU5BU5D_t41;
// System.String
struct String_t;

// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.IO.Stream,System.Text.Encoding)
extern "C" void UnexceptionalStreamWriter__ctor_m11984 (UnexceptionalStreamWriter_t2504 * __this, Stream_t1685 * ___stream, Encoding_t1666 * ___encoding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Flush()
extern "C" void UnexceptionalStreamWriter_Flush_m11985 (UnexceptionalStreamWriter_t2504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char[],System.Int32,System.Int32)
extern "C" void UnexceptionalStreamWriter_Write_m11986 (UnexceptionalStreamWriter_t2504 * __this, CharU5BU5D_t41* ___buffer, int32_t ___index, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char)
extern "C" void UnexceptionalStreamWriter_Write_m11987 (UnexceptionalStreamWriter_t2504 * __this, uint16_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char[])
extern "C" void UnexceptionalStreamWriter_Write_m11988 (UnexceptionalStreamWriter_t2504 * __this, CharU5BU5D_t41* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.String)
extern "C" void UnexceptionalStreamWriter_Write_m11989 (UnexceptionalStreamWriter_t2504 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
