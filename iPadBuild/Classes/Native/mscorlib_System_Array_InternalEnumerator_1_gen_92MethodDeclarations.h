﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.DateTime>
struct InternalEnumerator_1_t4235;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25978_gshared (InternalEnumerator_1_t4235 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25978(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4235 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25978_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25979_gshared (InternalEnumerator_1_t4235 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25979(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4235 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25979_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25980_gshared (InternalEnumerator_1_t4235 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25980(__this, method) (( void (*) (InternalEnumerator_1_t4235 *, MethodInfo*))InternalEnumerator_1_Dispose_m25980_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25981_gshared (InternalEnumerator_1_t4235 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25981(__this, method) (( bool (*) (InternalEnumerator_1_t4235 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25981_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C" DateTime_t48  InternalEnumerator_1_get_Current_m25982_gshared (InternalEnumerator_1_t4235 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25982(__this, method) (( DateTime_t48  (*) (InternalEnumerator_1_t4235 *, MethodInfo*))InternalEnumerator_1_get_Current_m25982_gshared)(__this, method)
