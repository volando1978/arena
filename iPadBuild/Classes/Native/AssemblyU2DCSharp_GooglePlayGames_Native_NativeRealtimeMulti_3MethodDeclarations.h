﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D
struct U3CPeersConnectedU3Ec__AnonStorey3D_t569;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D::.ctor()
extern "C" void U3CPeersConnectedU3Ec__AnonStorey3D__ctor_m2356 (U3CPeersConnectedU3Ec__AnonStorey3D_t569 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D::<>m__33()
extern "C" void U3CPeersConnectedU3Ec__AnonStorey3D_U3CU3Em__33_m2357 (U3CPeersConnectedU3Ec__AnonStorey3D_t569 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
