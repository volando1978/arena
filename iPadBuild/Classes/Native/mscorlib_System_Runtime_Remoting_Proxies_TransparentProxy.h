﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t2624;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Proxies.TransparentProxy
struct  TransparentProxy_t2625  : public Object_t
{
	// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.TransparentProxy::_rp
	RealProxy_t2624 * ____rp_0;
};
