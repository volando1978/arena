﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t242;

// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern "C" void ExtensionAttribute__ctor_m1071 (ExtensionAttribute_t242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
