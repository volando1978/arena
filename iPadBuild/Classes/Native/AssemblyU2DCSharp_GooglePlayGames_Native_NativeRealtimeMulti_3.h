﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t169;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t563;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D
struct  U3CPeersConnectedU3Ec__AnonStorey3D_t569  : public Object_t
{
	// System.String[] GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D::participantIds
	StringU5BU5D_t169* ___participantIds_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D::<>f__this
	OnGameThreadForwardingListener_t563 * ___U3CU3Ef__this_1;
};
