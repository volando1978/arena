﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>
struct Enumerator_t3832;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Matrix4x4>
struct List_1_t806;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m21086_gshared (Enumerator_t3832 * __this, List_1_t806 * ___l, MethodInfo* method);
#define Enumerator__ctor_m21086(__this, ___l, method) (( void (*) (Enumerator_t3832 *, List_1_t806 *, MethodInfo*))Enumerator__ctor_m21086_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21087_gshared (Enumerator_t3832 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21087(__this, method) (( Object_t * (*) (Enumerator_t3832 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21087_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::Dispose()
extern "C" void Enumerator_Dispose_m21088_gshared (Enumerator_t3832 * __this, MethodInfo* method);
#define Enumerator_Dispose_m21088(__this, method) (( void (*) (Enumerator_t3832 *, MethodInfo*))Enumerator_Dispose_m21088_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::VerifyState()
extern "C" void Enumerator_VerifyState_m21089_gshared (Enumerator_t3832 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m21089(__this, method) (( void (*) (Enumerator_t3832 *, MethodInfo*))Enumerator_VerifyState_m21089_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21090_gshared (Enumerator_t3832 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m21090(__this, method) (( bool (*) (Enumerator_t3832 *, MethodInfo*))Enumerator_MoveNext_m21090_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::get_Current()
extern "C" Matrix4x4_t997  Enumerator_get_Current_m21091_gshared (Enumerator_t3832 * __this, MethodInfo* method);
#define Enumerator_get_Current_m21091(__this, method) (( Matrix4x4_t997  (*) (Enumerator_t3832 *, MethodInfo*))Enumerator_get_Current_m21091_gshared)(__this, method)
