﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WinTextScript
struct WinTextScript_t760;

// System.Void WinTextScript::.ctor()
extern "C" void WinTextScript__ctor_m3298 (WinTextScript_t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinTextScript::Start()
extern "C" void WinTextScript_Start_m3299 (WinTextScript_t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinTextScript::Update()
extern "C" void WinTextScript_Update_m3300 (WinTextScript_t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinTextScript::OnGUI()
extern "C" void WinTextScript_OnGUI_m3301 (WinTextScript_t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
