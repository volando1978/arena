﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t2841;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct  TimeZone_t2841  : public Object_t
{
};
struct TimeZone_t2841_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t2841 * ___currentTimeZone_0;
};
