﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24
struct U3CFetchAllEventsU3Ec__AnonStorey24_t545;
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
struct FetchAllResponse_t664;

// System.Void GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24::.ctor()
extern "C" void U3CFetchAllEventsU3Ec__AnonStorey24__ctor_m2291 (U3CFetchAllEventsU3Ec__AnonStorey24_t545 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24::<>m__1C(GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse)
extern "C" void U3CFetchAllEventsU3Ec__AnonStorey24_U3CU3Em__1C_m2292 (U3CFetchAllEventsU3Ec__AnonStorey24_t545 * __this, FetchAllResponse_t664 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
