﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3281;
// System.String[]
struct StringU5BU5D_t169;
// Soomla.Store.VirtualItem[]
struct VirtualItemU5BU5D_t3564;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,Soomla.Store.VirtualItem,System.Collections.DictionaryEntry>
struct Transform_1_t3600;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>
struct  Dictionary_2_t110  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::table
	Int32U5BU5D_t107* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::linkSlots
	LinkU5BU5D_t3281* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::keySlots
	StringU5BU5D_t169* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::valueSlots
	VirtualItemU5BU5D_t3564* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::serialization_info
	SerializationInfo_t1673 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t110_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>::<>f__am$cacheB
	Transform_1_t3600 * ___U3CU3Ef__amU24cacheB_15;
};
