﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder
struct TurnBasedMatchConfigBuilder_t712;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t686;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig
struct TurnBasedMatchConfig_t710;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::.ctor(System.IntPtr)
extern "C" void TurnBasedMatchConfigBuilder__ctor_m3111 (TurnBasedMatchConfigBuilder_t712 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::PopulateFromUIResponse(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C" TurnBasedMatchConfigBuilder_t712 * TurnBasedMatchConfigBuilder_PopulateFromUIResponse_m3112 (TurnBasedMatchConfigBuilder_t712 * __this, PlayerSelectUIResponse_t686 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetVariant(System.UInt32)
extern "C" TurnBasedMatchConfigBuilder_t712 * TurnBasedMatchConfigBuilder_SetVariant_m3113 (TurnBasedMatchConfigBuilder_t712 * __this, uint32_t ___variant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::AddInvitedPlayer(System.String)
extern "C" TurnBasedMatchConfigBuilder_t712 * TurnBasedMatchConfigBuilder_AddInvitedPlayer_m3114 (TurnBasedMatchConfigBuilder_t712 * __this, String_t* ___playerId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetExclusiveBitMask(System.UInt64)
extern "C" TurnBasedMatchConfigBuilder_t712 * TurnBasedMatchConfigBuilder_SetExclusiveBitMask_m3115 (TurnBasedMatchConfigBuilder_t712 * __this, uint64_t ___bitmask, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetMinimumAutomatchingPlayers(System.UInt32)
extern "C" TurnBasedMatchConfigBuilder_t712 * TurnBasedMatchConfigBuilder_SetMinimumAutomatchingPlayers_m3116 (TurnBasedMatchConfigBuilder_t712 * __this, uint32_t ___minimum, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetMaximumAutomatchingPlayers(System.UInt32)
extern "C" TurnBasedMatchConfigBuilder_t712 * TurnBasedMatchConfigBuilder_SetMaximumAutomatchingPlayers_m3117 (TurnBasedMatchConfigBuilder_t712 * __this, uint32_t ___maximum, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::Build()
extern "C" TurnBasedMatchConfig_t710 * TurnBasedMatchConfigBuilder_Build_m3118 (TurnBasedMatchConfigBuilder_t712 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMatchConfigBuilder_CallDispose_m3119 (TurnBasedMatchConfigBuilder_t712 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::Create()
extern "C" TurnBasedMatchConfigBuilder_t712 * TurnBasedMatchConfigBuilder_Create_m3120 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
