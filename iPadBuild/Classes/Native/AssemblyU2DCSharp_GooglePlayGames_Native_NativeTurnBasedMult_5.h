﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53
struct  U3CTakeTurnU3Ec__AnonStorey53_t640  : public Object_t
{
	// System.Byte[] GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::data
	ByteU5BU5D_t350* ___data_0;
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::callback
	Action_1_t98 * ___callback_1;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::<>f__this
	NativeTurnBasedMultiplayerClient_t535 * ___U3CU3Ef__this_2;
};
