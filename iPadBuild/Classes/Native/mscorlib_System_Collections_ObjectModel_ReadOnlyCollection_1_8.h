﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Soomla.Store.MarketItem>
struct IList_1_t3558;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>
struct  ReadOnlyCollection_1_t3559  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>::list
	Object_t* ___list_0;
};
