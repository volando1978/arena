﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
struct FetchResponse_t688;
// GooglePlayGames.Native.NativeQuest
struct NativeQuest_t677;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::.ctor(System.IntPtr)
extern "C" void FetchResponse__ctor_m2889 (FetchResponse_t688 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::ResponseStatus()
extern "C" int32_t FetchResponse_ResponseStatus_m2890 (FetchResponse_t688 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::Data()
extern "C" NativeQuest_t677 * FetchResponse_Data_m2891 (FetchResponse_t688 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::RequestSucceeded()
extern "C" bool FetchResponse_RequestSucceeded_m2892 (FetchResponse_t688 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchResponse_CallDispose_m2893 (FetchResponse_t688 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::FromPointer(System.IntPtr)
extern "C" FetchResponse_t688 * FetchResponse_FromPointer_m2894 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
