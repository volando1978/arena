﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.ClientRecordProtocol
struct ClientRecordProtocol_t2247;
// System.IO.Stream
struct Stream_t1685;
// Mono.Security.Protocol.Tls.ClientContext
struct ClientContext_t2246;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct HandshakeMessage_t2264;
// Mono.Security.Protocol.Tls.TlsStream
struct TlsStream_t2255;
// System.Byte[]
struct ByteU5BU5D_t350;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"

// System.Void Mono.Security.Protocol.Tls.ClientRecordProtocol::.ctor(System.IO.Stream,Mono.Security.Protocol.Tls.ClientContext)
extern "C" void ClientRecordProtocol__ctor_m9393 (ClientRecordProtocol_t2247 * __this, Stream_t1685 * ___innerStream, ClientContext_t2246 * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.ClientRecordProtocol::GetMessage(Mono.Security.Protocol.Tls.Handshake.HandshakeType)
extern "C" HandshakeMessage_t2264 * ClientRecordProtocol_GetMessage_m9394 (ClientRecordProtocol_t2247 * __this, uint8_t ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.ClientRecordProtocol::ProcessHandshakeMessage(Mono.Security.Protocol.Tls.TlsStream)
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m9395 (ClientRecordProtocol_t2247 * __this, TlsStream_t2255 * ___handMsg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.ClientRecordProtocol::createClientHandshakeMessage(Mono.Security.Protocol.Tls.Handshake.HandshakeType)
extern "C" HandshakeMessage_t2264 * ClientRecordProtocol_createClientHandshakeMessage_m9396 (ClientRecordProtocol_t2247 * __this, uint8_t ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.ClientRecordProtocol::createServerHandshakeMessage(Mono.Security.Protocol.Tls.Handshake.HandshakeType,System.Byte[])
extern "C" HandshakeMessage_t2264 * ClientRecordProtocol_createServerHandshakeMessage_m9397 (ClientRecordProtocol_t2247 * __this, uint8_t ___type, ByteU5BU5D_t350* ___buffer, MethodInfo* method) IL2CPP_METHOD_ATTR;
