﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// cabezaScr/Frame
struct Frame_t770;

// System.Void cabezaScr/Frame::.ctor()
extern "C" void Frame__ctor_m3329 (Frame_t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
