﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// System.String[]
struct StringU5BU5D_t169;
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// WinTextScript
struct  WinTextScript_t760  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle WinTextScript::WinSt
	GUIStyle_t724 * ___WinSt_2;
	// System.Single WinTextScript::verticalPos
	float ___verticalPos_3;
	// System.Int32 WinTextScript::minColor
	int32_t ___minColor_4;
	// System.String[] WinTextScript::winTexts
	StringU5BU5D_t169* ___winTexts_5;
	// System.String WinTextScript::winText
	String_t* ___winText_6;
};
