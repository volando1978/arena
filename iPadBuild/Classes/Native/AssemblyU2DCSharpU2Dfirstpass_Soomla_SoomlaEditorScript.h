﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.SoomlaEditorScript
struct SoomlaEditorScript_t14;
// ObjectDictionary
struct ObjectDictionary_t12;
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObject.h"
// Soomla.SoomlaEditorScript
struct  SoomlaEditorScript_t14  : public ScriptableObject_t15
{
	// ObjectDictionary Soomla.SoomlaEditorScript::SoomlaSettings
	ObjectDictionary_t12 * ___SoomlaSettings_8;
};
struct SoomlaEditorScript_t14_StaticFields{
	// System.String Soomla.SoomlaEditorScript::AND_PUB_KEY_DEFAULT
	String_t* ___AND_PUB_KEY_DEFAULT_5;
	// System.String Soomla.SoomlaEditorScript::ONLY_ONCE_DEFAULT
	String_t* ___ONLY_ONCE_DEFAULT_6;
	// Soomla.SoomlaEditorScript Soomla.SoomlaEditorScript::instance
	SoomlaEditorScript_t14 * ___instance_7;
};
