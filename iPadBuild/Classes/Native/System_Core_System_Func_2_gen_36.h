﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t720;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct  Func_2_t1304  : public MulticastDelegate_t22
{
};
