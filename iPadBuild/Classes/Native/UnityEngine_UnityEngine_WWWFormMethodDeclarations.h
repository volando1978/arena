﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WWWForm
struct WWWForm_t171;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1666;

// System.Void UnityEngine.WWWForm::.ctor()
extern "C" void WWWForm__ctor_m913 (WWWForm_t171 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern "C" void WWWForm_AddField_m915 (WWWForm_t171 * __this, String_t* ___fieldName, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String,System.Text.Encoding)
extern "C" void WWWForm_AddField_m6945 (WWWForm_t171 * __this, String_t* ___fieldName, String_t* ___value, Encoding_t1666 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWWForm::get_headers()
extern "C" Dictionary_2_t165 * WWWForm_get_headers_m6946 (WWWForm_t171 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWForm::get_data()
extern "C" ByteU5BU5D_t350* WWWForm_get_data_m6947 (WWWForm_t171 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
