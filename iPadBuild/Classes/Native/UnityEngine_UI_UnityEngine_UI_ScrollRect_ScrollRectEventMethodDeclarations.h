﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t1282;

// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
extern "C" void ScrollRectEvent__ctor_m5256 (ScrollRectEvent_t1282 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
