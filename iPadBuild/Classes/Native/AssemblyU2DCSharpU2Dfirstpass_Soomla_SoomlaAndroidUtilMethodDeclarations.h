﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaAndroidUtil
struct SoomlaAndroidUtil_t17;

// System.Void Soomla.SoomlaAndroidUtil::.ctor()
extern "C" void SoomlaAndroidUtil__ctor_m27 (SoomlaAndroidUtil_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
