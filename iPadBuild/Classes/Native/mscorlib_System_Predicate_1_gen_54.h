﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2727;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Security.Policy.StrongName>
struct  Predicate_1_t4242  : public MulticastDelegate_t22
{
};
