﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t2218;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct  KeyGeneratedEventHandler_t2400  : public MulticastDelegate_t22
{
};
