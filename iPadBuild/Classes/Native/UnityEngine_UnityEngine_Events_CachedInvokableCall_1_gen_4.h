﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t208;
// UnityEngine.Events.InvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Byte>
struct  CachedInvokableCall_1_t4151  : public InvokableCall_1_t3821
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<System.Byte>::m_Arg1
	ObjectU5BU5D_t208* ___m_Arg1_1;
};
