﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t2129;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.DSASignatureFormatter
struct  DSASignatureFormatter_t2688  : public AsymmetricSignatureFormatter_t2269
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::dsa
	DSA_t2129 * ___dsa_0;
};
