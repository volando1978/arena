﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Byte>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3774;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Byte>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20360_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3774 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20360(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3774 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20360_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Byte>::<>m__77()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20361_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3774 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20361(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3774 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20361_gshared)(__this, method)
