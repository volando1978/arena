﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// System.String
struct String_t;

// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.String>::.ctor()
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_UnityKeyValuePair__0MethodDeclarations.h"
#define UnityKeyValuePair_2__ctor_m15790(__this, method) (( void (*) (UnityKeyValuePair_2_t164 *, MethodInfo*))UnityKeyValuePair_2__ctor_m15545_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.String>::.ctor(K,V)
#define UnityKeyValuePair_2__ctor_m15791(__this, ___key, ___value, method) (( void (*) (UnityKeyValuePair_2_t164 *, String_t*, String_t*, MethodInfo*))UnityKeyValuePair_2__ctor_m15546_gshared)(__this, ___key, ___value, method)
// K UnityEngine.UnityKeyValuePair`2<System.String,System.String>::get_Key()
#define UnityKeyValuePair_2_get_Key_m15792(__this, method) (( String_t* (*) (UnityKeyValuePair_2_t164 *, MethodInfo*))UnityKeyValuePair_2_get_Key_m15547_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.String>::set_Key(K)
#define UnityKeyValuePair_2_set_Key_m15793(__this, ___value, method) (( void (*) (UnityKeyValuePair_2_t164 *, String_t*, MethodInfo*))UnityKeyValuePair_2_set_Key_m15548_gshared)(__this, ___value, method)
// V UnityEngine.UnityKeyValuePair`2<System.String,System.String>::get_Value()
#define UnityKeyValuePair_2_get_Value_m15794(__this, method) (( String_t* (*) (UnityKeyValuePair_2_t164 *, MethodInfo*))UnityKeyValuePair_2_get_Value_m15549_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.String,System.String>::set_Value(V)
#define UnityKeyValuePair_2_set_Value_m15795(__this, ___value, method) (( void (*) (UnityKeyValuePair_2_t164 *, String_t*, MethodInfo*))UnityKeyValuePair_2_set_Value_m15550_gshared)(__this, ___value, method)
