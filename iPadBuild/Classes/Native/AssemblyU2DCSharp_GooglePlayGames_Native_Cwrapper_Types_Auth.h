﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
struct  AuthOperation_t504 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/AuthOperation::value__
	int32_t ___value___1;
};
