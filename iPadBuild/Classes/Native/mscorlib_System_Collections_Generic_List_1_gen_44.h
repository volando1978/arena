﻿#pragma once
#include <stdint.h>
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t4090;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct  List_1_t1577  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::_items
	Rigidbody2DU5BU5D_t4090* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::_version
	int32_t ____version_3;
};
struct List_1_t1577_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::EmptyArray
	Rigidbody2DU5BU5D_t4090* ___EmptyArray_4;
};
