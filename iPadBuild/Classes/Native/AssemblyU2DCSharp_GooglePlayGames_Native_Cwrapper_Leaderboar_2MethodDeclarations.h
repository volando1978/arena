﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback
struct FetchScorePageCallback_t429;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchScorePageCallback__ctor_m1744 (FetchScorePageCallback_t429 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchScorePageCallback_Invoke_m1745 (FetchScorePageCallback_t429 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchScorePageCallback_t429(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchScorePageCallback_BeginInvoke_m1746 (FetchScorePageCallback_t429 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchScorePageCallback_EndInvoke_m1747 (FetchScorePageCallback_t429 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
