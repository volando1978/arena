﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.InsufficientFundsException
struct InsufficientFundsException_t134;
// System.String
struct String_t;

// System.Void Soomla.Store.InsufficientFundsException::.ctor()
extern "C" void InsufficientFundsException__ctor_m665 (InsufficientFundsException_t134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.InsufficientFundsException::.ctor(System.String)
extern "C" void InsufficientFundsException__ctor_m666 (InsufficientFundsException_t134 * __this, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
