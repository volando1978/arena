﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t736;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UserAuthorizationDialog
struct  UserAuthorizationDialog_t1656  : public MonoBehaviour_t26
{
	// UnityEngine.Rect UnityEngine.UserAuthorizationDialog::windowRect
	Rect_t738  ___windowRect_4;
	// UnityEngine.Texture UnityEngine.UserAuthorizationDialog::warningIcon
	Texture_t736 * ___warningIcon_5;
};
