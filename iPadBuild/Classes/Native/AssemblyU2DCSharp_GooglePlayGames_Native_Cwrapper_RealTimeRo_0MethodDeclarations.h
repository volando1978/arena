﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig
struct RealTimeRoomConfig_t467;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_Length_m2020 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_GetElement_m2021 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t RealTimeRoomConfig_RealTimeRoomConfig_Variant_m2022 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C" int64_t RealTimeRoomConfig_RealTimeRoomConfig_ExclusiveBitMask_m2023 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool RealTimeRoomConfig_RealTimeRoomConfig_Valid_m2024 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MaximumAutomatchingPlayers_m2025 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MinimumAutomatchingPlayers_m2026 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RealTimeRoomConfig_RealTimeRoomConfig_Dispose_m2027 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
