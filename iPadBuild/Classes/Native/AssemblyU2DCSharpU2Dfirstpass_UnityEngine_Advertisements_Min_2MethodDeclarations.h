﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.MiniJSON.Json
struct Json_t150;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Object UnityEngine.Advertisements.MiniJSON.Json::Deserialize(System.String)
extern "C" Object_t * Json_Deserialize_m729 (Object_t * __this /* static, unused */, String_t* ___json, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.MiniJSON.Json::Serialize(System.Object)
extern "C" String_t* Json_Serialize_m730 (Object_t * __this /* static, unused */, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
