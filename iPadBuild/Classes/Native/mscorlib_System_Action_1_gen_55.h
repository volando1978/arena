﻿#pragma once
#include <stdint.h>
// Soomla.Schedule/DateTimeRange
struct DateTimeRange_t47;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Soomla.Schedule/DateTimeRange>
struct  Action_1_t3513  : public MulticastDelegate_t22
{
};
