﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t861;
// GooglePlayGames.BasicApi.Quests.IQuestMilestone
struct IQuestMilestone_t863;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// GooglePlayGames.BasicApi.Quests.QuestUiResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestUiRes.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
struct  Action_3_t554  : public MulticastDelegate_t22
{
};
