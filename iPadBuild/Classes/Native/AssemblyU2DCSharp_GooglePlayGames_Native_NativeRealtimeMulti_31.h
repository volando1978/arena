﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t594;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36
struct U3CAcceptInvitationU3Ec__AnonStorey36_t606;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38
struct U3CAcceptInvitationU3Ec__AnonStorey38_t607;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39
struct  U3CAcceptInvitationU3Ec__AnonStorey39_t608  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39::helper
	RealTimeEventListenerHelper_t594 * ___helper_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39::<>f__ref$54
	U3CAcceptInvitationU3Ec__AnonStorey36_t606 * ___U3CU3Ef__refU2454_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey39::<>f__ref$56
	U3CAcceptInvitationU3Ec__AnonStorey38_t607 * ___U3CU3Ef__refU2456_2;
};
