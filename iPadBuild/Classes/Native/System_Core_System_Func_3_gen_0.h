﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`3<System.Object,System.Object,System.Object>
struct  Func_3_t3634  : public MulticastDelegate_t22
{
};
