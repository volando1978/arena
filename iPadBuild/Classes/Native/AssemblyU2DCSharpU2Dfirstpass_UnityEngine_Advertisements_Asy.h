﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t26;
// UnityEngine.Advertisements.AsyncExec
struct AsyncExec_t145;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Advertisements.AsyncExec
struct  AsyncExec_t145  : public Object_t
{
};
struct AsyncExec_t145_StaticFields{
	// UnityEngine.GameObject UnityEngine.Advertisements.AsyncExec::asyncExecGameObject
	GameObject_t144 * ___asyncExecGameObject_0;
	// UnityEngine.MonoBehaviour UnityEngine.Advertisements.AsyncExec::coroutineHost
	MonoBehaviour_t26 * ___coroutineHost_1;
	// UnityEngine.Advertisements.AsyncExec UnityEngine.Advertisements.AsyncExec::asyncImpl
	AsyncExec_t145 * ___asyncImpl_2;
	// System.Boolean UnityEngine.Advertisements.AsyncExec::init
	bool ___init_3;
};
