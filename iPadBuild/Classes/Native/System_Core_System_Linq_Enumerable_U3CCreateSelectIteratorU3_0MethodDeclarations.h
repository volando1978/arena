﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>::.ctor()
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20436_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20436(__this, method) (( void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20436_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C" Object_t * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20437_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20437(__this, method) (( Object_t * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20437_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20438_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20438(__this, method) (( Object_t * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20438_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20439_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20439(__this, method) (( Object_t * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20439_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C" Object_t* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20440_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20440(__this, method) (( Object_t* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20440_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>::MoveNext()
extern "C" bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20441_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20441(__this, method) (( bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20441_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.IntPtr,System.Object>::Dispose()
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20442_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 * __this, MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20442(__this, method) (( void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3785 *, MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20442_gshared)(__this, method)
