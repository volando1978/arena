﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RandomAnimFrequency
struct RandomAnimFrequency_t750;

// System.Void RandomAnimFrequency::.ctor()
extern "C" void RandomAnimFrequency__ctor_m3243 (RandomAnimFrequency_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomAnimFrequency::Start()
extern "C" void RandomAnimFrequency_Start_m3244 (RandomAnimFrequency_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomAnimFrequency::Update()
extern "C" void RandomAnimFrequency_Update_m3245 (RandomAnimFrequency_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
