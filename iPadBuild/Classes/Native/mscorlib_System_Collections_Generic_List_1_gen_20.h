﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata[]
struct ISavedGameMetadataU5BU5D_t3689;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct  List_1_t931  : public Object_t
{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::_items
	ISavedGameMetadataU5BU5D_t3689* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::_version
	int32_t ____version_3;
};
struct List_1_t931_StaticFields{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::EmptyArray
	ISavedGameMetadataU5BU5D_t3689* ___EmptyArray_4;
};
