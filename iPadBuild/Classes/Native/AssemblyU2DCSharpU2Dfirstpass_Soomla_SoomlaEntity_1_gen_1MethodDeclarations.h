﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaEntity`1<System.Object>
struct SoomlaEntity_1_t3493;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.Object
struct Object_t;

// System.Void Soomla.SoomlaEntity`1<System.Object>::.ctor(System.String)
extern "C" void SoomlaEntity_1__ctor_m16331_gshared (SoomlaEntity_1_t3493 * __this, String_t* ___id, MethodInfo* method);
#define SoomlaEntity_1__ctor_m16331(__this, ___id, method) (( void (*) (SoomlaEntity_1_t3493 *, String_t*, MethodInfo*))SoomlaEntity_1__ctor_m16331_gshared)(__this, ___id, method)
// System.Void Soomla.SoomlaEntity`1<System.Object>::.ctor(System.String,System.String,System.String)
extern "C" void SoomlaEntity_1__ctor_m16332_gshared (SoomlaEntity_1_t3493 * __this, String_t* ___id, String_t* ___name, String_t* ___description, MethodInfo* method);
#define SoomlaEntity_1__ctor_m16332(__this, ___id, ___name, ___description, method) (( void (*) (SoomlaEntity_1_t3493 *, String_t*, String_t*, String_t*, MethodInfo*))SoomlaEntity_1__ctor_m16332_gshared)(__this, ___id, ___name, ___description, method)
// System.Void Soomla.SoomlaEntity`1<System.Object>::.ctor(JSONObject)
extern "C" void SoomlaEntity_1__ctor_m16333_gshared (SoomlaEntity_1_t3493 * __this, JSONObject_t30 * ___jsonEntity, MethodInfo* method);
#define SoomlaEntity_1__ctor_m16333(__this, ___jsonEntity, method) (( void (*) (SoomlaEntity_1_t3493 *, JSONObject_t30 *, MethodInfo*))SoomlaEntity_1__ctor_m16333_gshared)(__this, ___jsonEntity, method)
// System.String Soomla.SoomlaEntity`1<System.Object>::get_ID()
extern "C" String_t* SoomlaEntity_1_get_ID_m16334_gshared (SoomlaEntity_1_t3493 * __this, MethodInfo* method);
#define SoomlaEntity_1_get_ID_m16334(__this, method) (( String_t* (*) (SoomlaEntity_1_t3493 *, MethodInfo*))SoomlaEntity_1_get_ID_m16334_gshared)(__this, method)
// JSONObject Soomla.SoomlaEntity`1<System.Object>::toJSONObject()
extern "C" JSONObject_t30 * SoomlaEntity_1_toJSONObject_m16335_gshared (SoomlaEntity_1_t3493 * __this, MethodInfo* method);
#define SoomlaEntity_1_toJSONObject_m16335(__this, method) (( JSONObject_t30 * (*) (SoomlaEntity_1_t3493 *, MethodInfo*))SoomlaEntity_1_toJSONObject_m16335_gshared)(__this, method)
// System.Boolean Soomla.SoomlaEntity`1<System.Object>::Equals(System.Object)
extern "C" bool SoomlaEntity_1_Equals_m16336_gshared (SoomlaEntity_1_t3493 * __this, Object_t * ___obj, MethodInfo* method);
#define SoomlaEntity_1_Equals_m16336(__this, ___obj, method) (( bool (*) (SoomlaEntity_1_t3493 *, Object_t *, MethodInfo*))SoomlaEntity_1_Equals_m16336_gshared)(__this, ___obj, method)
// System.Boolean Soomla.SoomlaEntity`1<System.Object>::Equals(Soomla.SoomlaEntity`1<T>)
extern "C" bool SoomlaEntity_1_Equals_m16338_gshared (SoomlaEntity_1_t3493 * __this, SoomlaEntity_1_t3493 * ___g, MethodInfo* method);
#define SoomlaEntity_1_Equals_m16338(__this, ___g, method) (( bool (*) (SoomlaEntity_1_t3493 *, SoomlaEntity_1_t3493 *, MethodInfo*))SoomlaEntity_1_Equals_m16338_gshared)(__this, ___g, method)
// System.Int32 Soomla.SoomlaEntity`1<System.Object>::GetHashCode()
extern "C" int32_t SoomlaEntity_1_GetHashCode_m16339_gshared (SoomlaEntity_1_t3493 * __this, MethodInfo* method);
#define SoomlaEntity_1_GetHashCode_m16339(__this, method) (( int32_t (*) (SoomlaEntity_1_t3493 *, MethodInfo*))SoomlaEntity_1_GetHashCode_m16339_gshared)(__this, method)
// T Soomla.SoomlaEntity`1<System.Object>::Clone(System.String)
extern "C" Object_t * SoomlaEntity_1_Clone_m16340_gshared (SoomlaEntity_1_t3493 * __this, String_t* ___newId, MethodInfo* method);
#define SoomlaEntity_1_Clone_m16340(__this, ___newId, method) (( Object_t * (*) (SoomlaEntity_1_t3493 *, String_t*, MethodInfo*))SoomlaEntity_1_Clone_m16340_gshared)(__this, ___newId, method)
// System.Boolean Soomla.SoomlaEntity`1<System.Object>::op_Equality(Soomla.SoomlaEntity`1<T>,Soomla.SoomlaEntity`1<T>)
extern "C" bool SoomlaEntity_1_op_Equality_m16341_gshared (Object_t * __this /* static, unused */, SoomlaEntity_1_t3493 * ___a, SoomlaEntity_1_t3493 * ___b, MethodInfo* method);
#define SoomlaEntity_1_op_Equality_m16341(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, SoomlaEntity_1_t3493 *, SoomlaEntity_1_t3493 *, MethodInfo*))SoomlaEntity_1_op_Equality_m16341_gshared)(__this /* static, unused */, ___a, ___b, method)
// System.Boolean Soomla.SoomlaEntity`1<System.Object>::op_Inequality(Soomla.SoomlaEntity`1<T>,Soomla.SoomlaEntity`1<T>)
extern "C" bool SoomlaEntity_1_op_Inequality_m16343_gshared (Object_t * __this /* static, unused */, SoomlaEntity_1_t3493 * ___a, SoomlaEntity_1_t3493 * ___b, MethodInfo* method);
#define SoomlaEntity_1_op_Inequality_m16343(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, SoomlaEntity_1_t3493 *, SoomlaEntity_1_t3493 *, MethodInfo*))SoomlaEntity_1_op_Inequality_m16343_gshared)(__this /* static, unused */, ___a, ___b, method)
