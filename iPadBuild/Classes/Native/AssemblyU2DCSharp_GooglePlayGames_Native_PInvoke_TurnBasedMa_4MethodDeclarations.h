﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStorey64
struct U3CPlayerIdAtIndexU3Ec__AnonStorey64_t711;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStorey64::.ctor()
extern "C" void U3CPlayerIdAtIndexU3Ec__AnonStorey64__ctor_m3101 (U3CPlayerIdAtIndexU3Ec__AnonStorey64_t711 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStorey64::<>m__A1(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  U3CPlayerIdAtIndexU3Ec__AnonStorey64_U3CU3Em__A1_m3102 (U3CPlayerIdAtIndexU3Ec__AnonStorey64_t711 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
