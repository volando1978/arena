﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// circCont
struct circCont_t777;

// System.Void circCont::.ctor()
extern "C" void circCont__ctor_m3350 (circCont_t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circCont::Start()
extern "C" void circCont_Start_m3351 (circCont_t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circCont::Update()
extern "C" void circCont_Update_m3352 (circCont_t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circCont::destroyCirculares()
extern "C" void circCont_destroyCirculares_m3353 (circCont_t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
