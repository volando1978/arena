﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct  EndpointDetails_t356 
{
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mEndpointId
	String_t* ___mEndpointId_0;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mDeviceId
	String_t* ___mDeviceId_1;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mName
	String_t* ___mName_2;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mServiceId
	String_t* ___mServiceId_3;
};
// Native definition for marshalling of: GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_t356_marshaled
{
	char* ___mEndpointId_0;
	char* ___mDeviceId_1;
	char* ___mName_2;
	char* ___mServiceId_3;
};
