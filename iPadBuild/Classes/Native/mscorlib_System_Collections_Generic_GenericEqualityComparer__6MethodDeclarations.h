﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Byte>
struct GenericEqualityComparer_1_t4183;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m25705_gshared (GenericEqualityComparer_1_t4183 * __this, MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m25705(__this, method) (( void (*) (GenericEqualityComparer_1_t4183 *, MethodInfo*))GenericEqualityComparer_1__ctor_m25705_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m25706_gshared (GenericEqualityComparer_1_t4183 * __this, uint8_t ___obj, MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m25706(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t4183 *, uint8_t, MethodInfo*))GenericEqualityComparer_1_GetHashCode_m25706_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m25707_gshared (GenericEqualityComparer_1_t4183 * __this, uint8_t ___x, uint8_t ___y, MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m25707(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t4183 *, uint8_t, uint8_t, MethodInfo*))GenericEqualityComparer_1_Equals_m25707_gshared)(__this, ___x, ___y, method)
