﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Collection_1_t3443;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3438;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t4259;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IList_1_t3441;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void Collection_1__ctor_m15705_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1__ctor_m15705(__this, method) (( void (*) (Collection_1_t3443 *, MethodInfo*))Collection_1__ctor_m15705_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15706_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15706(__this, method) (( bool (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15706_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15707_gshared (Collection_1_t3443 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15707(__this, ___array, ___index, method) (( void (*) (Collection_1_t3443 *, Array_t *, int32_t, MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15707_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15708_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15708(__this, method) (( Object_t * (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15708_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15709_gshared (Collection_1_t3443 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15709(__this, ___value, method) (( int32_t (*) (Collection_1_t3443 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Add_m15709_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15710_gshared (Collection_1_t3443 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15710(__this, ___value, method) (( bool (*) (Collection_1_t3443 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Contains_m15710_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15711_gshared (Collection_1_t3443 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15711(__this, ___value, method) (( int32_t (*) (Collection_1_t3443 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15711_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15712_gshared (Collection_1_t3443 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15712(__this, ___index, ___value, method) (( void (*) (Collection_1_t3443 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Insert_m15712_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15713_gshared (Collection_1_t3443 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15713(__this, ___value, method) (( void (*) (Collection_1_t3443 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Remove_m15713_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15714_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15714(__this, method) (( bool (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15714_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15715_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15715(__this, method) (( Object_t * (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15715_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15716_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15716(__this, method) (( bool (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15716_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15717_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15717(__this, method) (( bool (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15717_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15718_gshared (Collection_1_t3443 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15718(__this, ___index, method) (( Object_t * (*) (Collection_1_t3443 *, int32_t, MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15718_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15719_gshared (Collection_1_t3443 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15719(__this, ___index, ___value, method) (( void (*) (Collection_1_t3443 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15719_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
extern "C" void Collection_1_Add_m15720_gshared (Collection_1_t3443 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define Collection_1_Add_m15720(__this, ___item, method) (( void (*) (Collection_1_t3443 *, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_Add_m15720_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void Collection_1_Clear_m15721_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_Clear_m15721(__this, method) (( void (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_Clear_m15721_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ClearItems()
extern "C" void Collection_1_ClearItems_m15722_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_ClearItems_m15722(__this, method) (( void (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_ClearItems_m15722_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T)
extern "C" bool Collection_1_Contains_m15723_gshared (Collection_1_t3443 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define Collection_1_Contains_m15723(__this, ___item, method) (( bool (*) (Collection_1_t3443 *, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_Contains_m15723_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15724_gshared (Collection_1_t3443 * __this, KeyValuePair_2U5BU5D_t3438* ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_CopyTo_m15724(__this, ___array, ___index, method) (( void (*) (Collection_1_t3443 *, KeyValuePair_2U5BU5D_t3438*, int32_t, MethodInfo*))Collection_1_CopyTo_m15724_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15725_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_GetEnumerator_m15725(__this, method) (( Object_t* (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_GetEnumerator_m15725_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15726_gshared (Collection_1_t3443 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define Collection_1_IndexOf_m15726(__this, ___item, method) (( int32_t (*) (Collection_1_t3443 *, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_IndexOf_m15726_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15727_gshared (Collection_1_t3443 * __this, int32_t ___index, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define Collection_1_Insert_m15727(__this, ___index, ___item, method) (( void (*) (Collection_1_t3443 *, int32_t, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_Insert_m15727_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15728_gshared (Collection_1_t3443 * __this, int32_t ___index, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define Collection_1_InsertItem_m15728(__this, ___index, ___item, method) (( void (*) (Collection_1_t3443 *, int32_t, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_InsertItem_m15728_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(T)
extern "C" bool Collection_1_Remove_m15729_gshared (Collection_1_t3443 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define Collection_1_Remove_m15729(__this, ___item, method) (( bool (*) (Collection_1_t3443 *, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_Remove_m15729_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15730_gshared (Collection_1_t3443 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveAt_m15730(__this, ___index, method) (( void (*) (Collection_1_t3443 *, int32_t, MethodInfo*))Collection_1_RemoveAt_m15730_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15731_gshared (Collection_1_t3443 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveItem_m15731(__this, ___index, method) (( void (*) (Collection_1_t3443 *, int32_t, MethodInfo*))Collection_1_RemoveItem_m15731_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15732_gshared (Collection_1_t3443 * __this, MethodInfo* method);
#define Collection_1_get_Count_m15732(__this, method) (( int32_t (*) (Collection_1_t3443 *, MethodInfo*))Collection_1_get_Count_m15732_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C" KeyValuePair_2_t3407  Collection_1_get_Item_m15733_gshared (Collection_1_t3443 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_get_Item_m15733(__this, ___index, method) (( KeyValuePair_2_t3407  (*) (Collection_1_t3443 *, int32_t, MethodInfo*))Collection_1_get_Item_m15733_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15734_gshared (Collection_1_t3443 * __this, int32_t ___index, KeyValuePair_2_t3407  ___value, MethodInfo* method);
#define Collection_1_set_Item_m15734(__this, ___index, ___value, method) (( void (*) (Collection_1_t3443 *, int32_t, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_set_Item_m15734_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15735_gshared (Collection_1_t3443 * __this, int32_t ___index, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define Collection_1_SetItem_m15735(__this, ___index, ___item, method) (( void (*) (Collection_1_t3443 *, int32_t, KeyValuePair_2_t3407 , MethodInfo*))Collection_1_SetItem_m15735_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15736_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_IsValidItem_m15736(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_IsValidItem_m15736_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ConvertItem(System.Object)
extern "C" KeyValuePair_2_t3407  Collection_1_ConvertItem_m15737_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_ConvertItem_m15737(__this /* static, unused */, ___item, method) (( KeyValuePair_2_t3407  (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_ConvertItem_m15737_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15738_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_CheckWritable_m15738(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_CheckWritable_m15738_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15739_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsSynchronized_m15739(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsSynchronized_m15739_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15740_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsFixedSize_m15740(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsFixedSize_m15740_gshared)(__this /* static, unused */, ___list, method)
