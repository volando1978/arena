﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5D`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct U3CToIntPtrU3Ec__AnonStorey5D_2_t3779;
// System.Object
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5D`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::.ctor()
extern "C" void U3CToIntPtrU3Ec__AnonStorey5D_2__ctor_m20414_gshared (U3CToIntPtrU3Ec__AnonStorey5D_2_t3779 * __this, MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey5D_2__ctor_m20414(__this, method) (( void (*) (U3CToIntPtrU3Ec__AnonStorey5D_2_t3779 *, MethodInfo*))U3CToIntPtrU3Ec__AnonStorey5D_2__ctor_m20414_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5D`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::<>m__74(T,System.IntPtr)
extern "C" void U3CToIntPtrU3Ec__AnonStorey5D_2_U3CU3Em__74_m20415_gshared (U3CToIntPtrU3Ec__AnonStorey5D_2_t3779 * __this, Object_t * ___param1, IntPtr_t ___param2, MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey5D_2_U3CU3Em__74_m20415(__this, ___param1, ___param2, method) (( void (*) (U3CToIntPtrU3Ec__AnonStorey5D_2_t3779 *, Object_t *, IntPtr_t, MethodInfo*))U3CToIntPtrU3Ec__AnonStorey5D_2_U3CU3Em__74_m20415_gshared)(__this, ___param1, ___param2, method)
