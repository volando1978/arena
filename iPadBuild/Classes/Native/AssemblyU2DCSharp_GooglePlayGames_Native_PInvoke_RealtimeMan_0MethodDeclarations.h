﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse
struct RoomInboxUIResponse_t696;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::.ctor(System.IntPtr)
extern "C" void RoomInboxUIResponse__ctor_m2959 (RoomInboxUIResponse_t696 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::ResponseStatus()
extern "C" int32_t RoomInboxUIResponse_ResponseStatus_m2960 (RoomInboxUIResponse_t696 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::Invitation()
extern "C" MultiplayerInvitation_t601 * RoomInboxUIResponse_Invitation_m2961 (RoomInboxUIResponse_t696 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RoomInboxUIResponse_CallDispose_m2962 (RoomInboxUIResponse_t696 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::FromPointer(System.IntPtr)
extern "C" RoomInboxUIResponse_t696 * RoomInboxUIResponse_FromPointer_m2963 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
