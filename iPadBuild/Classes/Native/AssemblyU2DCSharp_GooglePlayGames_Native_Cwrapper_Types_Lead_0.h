﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_0.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
struct  LeaderboardStart_t510 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart::value__
	int32_t ___value___1;
};
