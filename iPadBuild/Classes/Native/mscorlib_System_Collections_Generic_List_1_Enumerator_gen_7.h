﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>
struct List_1_t115;
// Soomla.Store.VirtualCurrencyPack
struct VirtualCurrencyPack_t78;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrencyPack>
struct  Enumerator_t227 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrencyPack>::l
	List_1_t115 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrencyPack>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrencyPack>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrencyPack>::current
	VirtualCurrencyPack_t78 * ___current_3;
};
