﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t1383;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t1602;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct  Font_t1222  : public Object_t187
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t1602 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t1222_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t1383 * ___textureRebuilt_2;
};
