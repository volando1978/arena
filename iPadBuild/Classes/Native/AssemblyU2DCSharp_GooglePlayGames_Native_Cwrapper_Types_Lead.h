﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder
struct  LeaderboardOrder_t509 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder::value__
	int32_t ___value___1;
};
