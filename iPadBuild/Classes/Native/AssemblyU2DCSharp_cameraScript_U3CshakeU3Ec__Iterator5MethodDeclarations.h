﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// cameraScript/<shake>c__Iterator5
struct U3CshakeU3Ec__Iterator5_t775;
// System.Object
struct Object_t;

// System.Void cameraScript/<shake>c__Iterator5::.ctor()
extern "C" void U3CshakeU3Ec__Iterator5__ctor_m3334 (U3CshakeU3Ec__Iterator5_t775 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraScript/<shake>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335 (U3CshakeU3Ec__Iterator5_t775 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraScript/<shake>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336 (U3CshakeU3Ec__Iterator5_t775 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean cameraScript/<shake>c__Iterator5::MoveNext()
extern "C" bool U3CshakeU3Ec__Iterator5_MoveNext_m3337 (U3CshakeU3Ec__Iterator5_t775 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraScript/<shake>c__Iterator5::Dispose()
extern "C" void U3CshakeU3Ec__Iterator5_Dispose_m3338 (U3CshakeU3Ec__Iterator5_t775 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraScript/<shake>c__Iterator5::Reset()
extern "C" void U3CshakeU3Ec__Iterator5_Reset_m3339 (U3CshakeU3Ec__Iterator5_t775 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
