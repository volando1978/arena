﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.QuestManager
struct QuestManager_t560;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse>
struct Action_1_t877;
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>
struct Action_1_t878;
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse>
struct Action_1_t862;
// GooglePlayGames.Native.NativeQuest
struct NativeQuest_t677;
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse>
struct Action_1_t879;
// GooglePlayGames.Native.NativeQuestMilestone
struct NativeQuestMilestone_t676;
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>
struct Action_1_t880;
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void QuestManager__ctor_m2922 (QuestManager_t560 * __this, GameServices_t534 * ___services, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::Fetch(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse>)
extern "C" void QuestManager_Fetch_m2923 (QuestManager_t560 * __this, int32_t ___source, String_t* ___questId, Action_1_t877 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::InternalFetchCallback(System.IntPtr,System.IntPtr)
extern "C" void QuestManager_InternalFetchCallback_m2924 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::FetchList(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.Int32,System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>)
extern "C" void QuestManager_FetchList_m2925 (QuestManager_t560 * __this, int32_t ___source, int32_t ___fetchFlags, Action_1_t878 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::InternalFetchListCallback(System.IntPtr,System.IntPtr)
extern "C" void QuestManager_InternalFetchListCallback_m2926 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::ShowAllQuestUI(System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse>)
extern "C" void QuestManager_ShowAllQuestUI_m2927 (QuestManager_t560 * __this, Action_1_t862 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::ShowQuestUI(GooglePlayGames.Native.NativeQuest,System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse>)
extern "C" void QuestManager_ShowQuestUI_m2928 (QuestManager_t560 * __this, NativeQuest_t677 * ___quest, Action_1_t862 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::InternalQuestUICallback(System.IntPtr,System.IntPtr)
extern "C" void QuestManager_InternalQuestUICallback_m2929 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::Accept(GooglePlayGames.Native.NativeQuest,System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse>)
extern "C" void QuestManager_Accept_m2930 (QuestManager_t560 * __this, NativeQuest_t677 * ___quest, Action_1_t879 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::InternalAcceptCallback(System.IntPtr,System.IntPtr)
extern "C" void QuestManager_InternalAcceptCallback_m2931 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::ClaimMilestone(GooglePlayGames.Native.NativeQuestMilestone,System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>)
extern "C" void QuestManager_ClaimMilestone_m2932 (QuestManager_t560 * __this, NativeQuestMilestone_t676 * ___milestone, Action_1_t880 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager::InternalClaimMilestoneCallback(System.IntPtr,System.IntPtr)
extern "C" void QuestManager_InternalClaimMilestoneCallback_m2933 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
