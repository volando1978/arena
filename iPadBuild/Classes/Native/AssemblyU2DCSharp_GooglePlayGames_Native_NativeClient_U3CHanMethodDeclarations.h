﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C
struct U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
struct FetchAllResponse_t655;
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse
struct FetchSelfResponse_t684;

// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::.ctor()
extern "C" void U3CHandleAuthTransitionU3Ec__AnonStorey1C__ctor_m2238 (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::<>m__11(GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse)
extern "C" void U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__11_m2239 (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * __this, FetchAllResponse_t655 * ___results, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::<>m__12(GooglePlayGames.Native.PlayerManager/FetchSelfResponse)
extern "C" void U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__12_m2240 (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * __this, FetchSelfResponse_t684 * ___results, MethodInfo* method) IL2CPP_METHOD_ATTR;
