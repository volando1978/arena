﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback
struct OnLogCallback_t402;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/LogLevel
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_LogL.h"

// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnLogCallback__ctor_m1653 (OnLogCallback_t402 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/LogLevel,System.String,System.IntPtr)
extern "C" void OnLogCallback_Invoke_m1654 (OnLogCallback_t402 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String
#include "mscorlib_System_String.h"
extern "C" void pinvoke_delegate_wrapper_OnLogCallback_t402(Il2CppObject* delegate, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/LogLevel,System.String,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnLogCallback_BeginInvoke_m1655 (OnLogCallback_t402 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnLogCallback_EndInvoke_m1656 (OnLogCallback_t402 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
