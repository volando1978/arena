﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>
struct Dictionary_2_t542;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>
struct KeyCollection_t3715;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Achievement>
struct ValueCollection_t3716;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>[]
struct KeyValuePair_2U5BU5D_t4374;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>>
struct IEnumerator_1_t4375;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__15.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_23MethodDeclarations.h"
#define Dictionary_2__ctor_m3751(__this, method) (( void (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2__ctor_m16211_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m19633(__this, ___comparer, method) (( void (*) (Dictionary_2_t542 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m16213_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(System.Int32)
#define Dictionary_2__ctor_m19634(__this, ___capacity, method) (( void (*) (Dictionary_2_t542 *, int32_t, MethodInfo*))Dictionary_2__ctor_m16215_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m19635(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t542 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m16217_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m19636(__this, method) (( Object_t * (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m19637(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t542 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m19638(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t542 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m19639(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t542 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m19640(__this, ___key, method) (( void (*) (Dictionary_2_t542 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19641(__this, method) (( bool (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19642(__this, method) (( Object_t * (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19643(__this, method) (( bool (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19644(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t542 *, KeyValuePair_2_t3714 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19645(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t542 *, KeyValuePair_2_t3714 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19646(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t542 *, KeyValuePair_2U5BU5D_t4374*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19647(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t542 *, KeyValuePair_2_t3714 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m19648(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t542 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19649(__this, method) (( Object_t * (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19650(__this, method) (( Object_t* (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19651(__this, method) (( Object_t * (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Count()
#define Dictionary_2_get_Count_m19652(__this, method) (( int32_t (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_get_Count_m16251_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Item(TKey)
#define Dictionary_2_get_Item_m19653(__this, ___key, method) (( Achievement_t333 * (*) (Dictionary_2_t542 *, String_t*, MethodInfo*))Dictionary_2_get_Item_m16253_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m19654(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t542 *, String_t*, Achievement_t333 *, MethodInfo*))Dictionary_2_set_Item_m16255_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m19655(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t542 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m16257_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m19656(__this, ___size, method) (( void (*) (Dictionary_2_t542 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m16259_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m19657(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t542 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m16261_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m19658(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3714  (*) (Object_t * /* static, unused */, String_t*, Achievement_t333 *, MethodInfo*))Dictionary_2_make_pair_m16263_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m19659(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, Achievement_t333 *, MethodInfo*))Dictionary_2_pick_key_m16265_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m19660(__this /* static, unused */, ___key, ___value, method) (( Achievement_t333 * (*) (Object_t * /* static, unused */, String_t*, Achievement_t333 *, MethodInfo*))Dictionary_2_pick_value_m16267_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m19661(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t542 *, KeyValuePair_2U5BU5D_t4374*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m16269_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Resize()
#define Dictionary_2_Resize_m19662(__this, method) (( void (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_Resize_m16271_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Add(TKey,TValue)
#define Dictionary_2_Add_m19663(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t542 *, String_t*, Achievement_t333 *, MethodInfo*))Dictionary_2_Add_m16273_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Clear()
#define Dictionary_2_Clear_m19664(__this, method) (( void (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_Clear_m16275_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m19665(__this, ___key, method) (( bool (*) (Dictionary_2_t542 *, String_t*, MethodInfo*))Dictionary_2_ContainsKey_m16277_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m19666(__this, ___value, method) (( bool (*) (Dictionary_2_t542 *, Achievement_t333 *, MethodInfo*))Dictionary_2_ContainsValue_m16279_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m19667(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t542 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m16281_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m19668(__this, ___sender, method) (( void (*) (Dictionary_2_t542 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m16283_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Remove(TKey)
#define Dictionary_2_Remove_m19669(__this, ___key, method) (( bool (*) (Dictionary_2_t542 *, String_t*, MethodInfo*))Dictionary_2_Remove_m16285_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m19670(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t542 *, String_t*, Achievement_t333 **, MethodInfo*))Dictionary_2_TryGetValue_m16287_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Keys()
#define Dictionary_2_get_Keys_m19671(__this, method) (( KeyCollection_t3715 * (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_get_Keys_m16289_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Values()
#define Dictionary_2_get_Values_m19672(__this, method) (( ValueCollection_t3716 * (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_get_Values_m16291_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m19673(__this, ___key, method) (( String_t* (*) (Dictionary_2_t542 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m16293_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m19674(__this, ___value, method) (( Achievement_t333 * (*) (Dictionary_2_t542 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m16295_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m19675(__this, ___pair, method) (( bool (*) (Dictionary_2_t542 *, KeyValuePair_2_t3714 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16297_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m19676(__this, method) (( Enumerator_t3717  (*) (Dictionary_2_t542 *, MethodInfo*))Dictionary_2_GetEnumerator_m16298_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m19677(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, String_t*, Achievement_t333 *, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16300_gshared)(__this /* static, unused */, ___key, ___value, method)
