﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.CLSCompliantAttribute
struct CLSCompliantAttribute_t1813;

// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
extern "C" void CLSCompliantAttribute__ctor_m7695 (CLSCompliantAttribute_t1813 * __this, bool ___isCompliant, MethodInfo* method) IL2CPP_METHOD_ATTR;
