﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// gameControl/<waitTime>c__IteratorA
struct  U3CwaitTimeU3Ec__IteratorA_t798  : public Object_t
{
	// System.Int32 gameControl/<waitTime>c__IteratorA::<t>__0
	int32_t ___U3CtU3E__0_0;
	// System.Single gameControl/<waitTime>c__IteratorA::seconds
	float ___seconds_1;
	// System.Int32 gameControl/<waitTime>c__IteratorA::$PC
	int32_t ___U24PC_2;
	// System.Object gameControl/<waitTime>c__IteratorA::$current
	Object_t * ___U24current_3;
	// System.Single gameControl/<waitTime>c__IteratorA::<$>seconds
	float ___U3CU24U3Eseconds_4;
};
