﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<System.Action>
struct IList_1_t3701;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Action>
struct  ReadOnlyCollection_1_t3702  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Action>::list
	Object_t* ___list_0;
};
