﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t3492;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t920;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16324_gshared (ShimEnumerator_t3492 * __this, Dictionary_2_t920 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m16324(__this, ___host, method) (( void (*) (ShimEnumerator_t3492 *, Dictionary_2_t920 *, MethodInfo*))ShimEnumerator__ctor_m16324_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16325_gshared (ShimEnumerator_t3492 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m16325(__this, method) (( bool (*) (ShimEnumerator_t3492 *, MethodInfo*))ShimEnumerator_MoveNext_m16325_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t2128  ShimEnumerator_get_Entry_m16326_gshared (ShimEnumerator_t3492 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m16326(__this, method) (( DictionaryEntry_t2128  (*) (ShimEnumerator_t3492 *, MethodInfo*))ShimEnumerator_get_Entry_m16326_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16327_gshared (ShimEnumerator_t3492 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m16327(__this, method) (( Object_t * (*) (ShimEnumerator_t3492 *, MethodInfo*))ShimEnumerator_get_Key_m16327_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16328_gshared (ShimEnumerator_t3492 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m16328(__this, method) (( Object_t * (*) (ShimEnumerator_t3492 *, MethodInfo*))ShimEnumerator_get_Value_m16328_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16329_gshared (ShimEnumerator_t3492 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m16329(__this, method) (( Object_t * (*) (ShimEnumerator_t3492 *, MethodInfo*))ShimEnumerator_get_Current_m16329_gshared)(__this, method)
