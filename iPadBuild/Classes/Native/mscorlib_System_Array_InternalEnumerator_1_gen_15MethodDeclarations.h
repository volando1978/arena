﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>
struct InternalEnumerator_1_t3581;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17645_gshared (InternalEnumerator_1_t3581 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m17645(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3581 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m17645_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17646_gshared (InternalEnumerator_1_t3581 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17646(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3581 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17646_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17647_gshared (InternalEnumerator_1_t3581 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17647(__this, method) (( void (*) (InternalEnumerator_1_t3581 *, MethodInfo*))InternalEnumerator_1_Dispose_m17647_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17648_gshared (InternalEnumerator_1_t3581 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17648(__this, method) (( bool (*) (InternalEnumerator_1_t3581 *, MethodInfo*))InternalEnumerator_1_MoveNext_m17648_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern "C" Link_t3579  InternalEnumerator_1_get_Current_m17649_gshared (InternalEnumerator_1_t3581 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17649(__this, method) (( Link_t3579  (*) (InternalEnumerator_1_t3581 *, MethodInfo*))InternalEnumerator_1_get_Current_m17649_gshared)(__this, method)
