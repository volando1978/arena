﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t4009;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.String>
struct  InvokableCall_1_t4149  : public BaseInvokableCall_t1647
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.String>::Delegate
	UnityAction_1_t4009 * ___Delegate_0;
};
