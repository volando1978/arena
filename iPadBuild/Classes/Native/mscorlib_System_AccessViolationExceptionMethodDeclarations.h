﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AccessViolationException
struct AccessViolationException_t2771;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.AccessViolationException::.ctor()
extern "C" void AccessViolationException__ctor_m13576 (AccessViolationException_t2771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AccessViolationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void AccessViolationException__ctor_m13577 (AccessViolationException_t2771 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
