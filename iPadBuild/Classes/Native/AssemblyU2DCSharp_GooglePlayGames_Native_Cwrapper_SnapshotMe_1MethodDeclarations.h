﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder
struct SnapshotMetadataChangeBuilder_t481;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetDescription_m2132 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___description, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Construct()
extern "C" IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Construct_m2133 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetPlayedTime(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetPlayedTime_m2134 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint64_t ___played_time, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetCoverImageFromPngData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetCoverImageFromPngData_m2135 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___png_data, UIntPtr_t  ___png_data_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Create_m2136 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Dispose_m2137 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
