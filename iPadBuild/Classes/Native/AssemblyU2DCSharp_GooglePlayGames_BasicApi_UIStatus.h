﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus.h"
// GooglePlayGames.BasicApi.UIStatus
struct  UIStatus_t336 
{
	// System.Int32 GooglePlayGames.BasicApi.UIStatus::value__
	int32_t ___value___1;
};
