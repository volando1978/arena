﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RayoScr
struct RayoScr_t752;

// System.Void RayoScr::.ctor()
extern "C" void RayoScr__ctor_m3250 (RayoScr_t752 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RayoScr::Start()
extern "C" void RayoScr_Start_m3251 (RayoScr_t752 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RayoScr::Update()
extern "C" void RayoScr_Update_m3252 (RayoScr_t752 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
