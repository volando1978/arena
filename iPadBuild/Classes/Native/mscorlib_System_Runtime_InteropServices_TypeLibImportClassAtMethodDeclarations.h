﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.TypeLibImportClassAttribute
struct TypeLibImportClassAttribute_t2579;
// System.Type
struct Type_t;

// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
extern "C" void TypeLibImportClassAttribute__ctor_m12498 (TypeLibImportClassAttribute_t2579 * __this, Type_t * ___importClass, MethodInfo* method) IL2CPP_METHOD_ATTR;
