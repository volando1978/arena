﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t1211;
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// UnityEngine.UI.Button
struct  Button_t1213  : public Selectable_t1215
{
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t1211 * ___m_OnClick_16;
};
