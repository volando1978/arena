﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33
struct U3CAcceptFromInboxU3Ec__AnonStorey33_t602;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34
struct  U3CAcceptFromInboxU3Ec__AnonStorey34_t603  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34::invitation
	MultiplayerInvitation_t601 * ___invitation_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34::<>f__ref$51
	U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * ___U3CU3Ef__refU2451_1;
};
