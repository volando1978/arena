﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35
struct U3CAcceptFromInboxU3Ec__AnonStorey35_t604;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t695;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey35__ctor_m2464 (U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::<>m__2D()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2D_m2465 (U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::<>m__2E(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey35_U3CU3Em__2E_m2466 (U3CAcceptFromInboxU3Ec__AnonStorey35_t604 * __this, RealTimeRoomResponse_t695 * ___acceptResponse, MethodInfo* method) IL2CPP_METHOD_ATTR;
