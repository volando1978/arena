﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>
struct DefaultComparer_t3508;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::.ctor()
extern "C" void DefaultComparer__ctor_m16628_gshared (DefaultComparer_t3508 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m16628(__this, method) (( void (*) (DefaultComparer_t3508 *, MethodInfo*))DefaultComparer__ctor_m16628_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m16629_gshared (DefaultComparer_t3508 * __this, uint16_t ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m16629(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3508 *, uint16_t, MethodInfo*))DefaultComparer_GetHashCode_m16629_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m16630_gshared (DefaultComparer_t3508 * __this, uint16_t ___x, uint16_t ___y, MethodInfo* method);
#define DefaultComparer_Equals_m16630(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3508 *, uint16_t, uint16_t, MethodInfo*))DefaultComparer_Equals_m16630_gshared)(__this, ___x, ___y, method)
