﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t4010;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t1405;
// System.Object[]
struct ObjectU5BU5D_t208;

// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m23541_gshared (InvokableCall_1_t4010 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, MethodInfo* method);
#define InvokableCall_1__ctor_m23541(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t4010 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_1__ctor_m23541_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m23542_gshared (InvokableCall_1_t4010 * __this, UnityAction_1_t1405 * ___callback, MethodInfo* method);
#define InvokableCall_1__ctor_m23542(__this, ___callback, method) (( void (*) (InvokableCall_1_t4010 *, UnityAction_1_t1405 *, MethodInfo*))InvokableCall_1__ctor_m23542_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m23543_gshared (InvokableCall_1_t4010 * __this, ObjectU5BU5D_t208* ___args, MethodInfo* method);
#define InvokableCall_1_Invoke_m23543(__this, ___args, method) (( void (*) (InvokableCall_1_t4010 *, ObjectU5BU5D_t208*, MethodInfo*))InvokableCall_1_Invoke_m23543_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m23544_gshared (InvokableCall_1_t4010 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, MethodInfo* method);
#define InvokableCall_1_Find_m23544(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t4010 *, Object_t *, MethodInfo_t *, MethodInfo*))InvokableCall_1_Find_m23544_gshared)(__this, ___targetObj, ___method, method)
