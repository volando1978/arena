﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t686;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::.ctor(System.IntPtr)
extern "C" void PlayerSelectUIResponse__ctor_m2880 (PlayerSelectUIResponse_t686 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * PlayerSelectUIResponse_System_Collections_IEnumerable_GetEnumerator_m2881 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::Status()
extern "C" int32_t PlayerSelectUIResponse_Status_m2882 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::PlayerIdAtIndex(System.UIntPtr)
extern "C" String_t* PlayerSelectUIResponse_PlayerIdAtIndex_m2883 (PlayerSelectUIResponse_t686 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::GetEnumerator()
extern "C" Object_t* PlayerSelectUIResponse_GetEnumerator_m2884 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::MinimumAutomatchingPlayers()
extern "C" uint32_t PlayerSelectUIResponse_MinimumAutomatchingPlayers_m2885 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::MaximumAutomatchingPlayers()
extern "C" uint32_t PlayerSelectUIResponse_MaximumAutomatchingPlayers_m2886 (PlayerSelectUIResponse_t686 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void PlayerSelectUIResponse_CallDispose_m2887 (PlayerSelectUIResponse_t686 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::FromPointer(System.IntPtr)
extern "C" PlayerSelectUIResponse_t686 * PlayerSelectUIResponse_FromPointer_m2888 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
