﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.EquippableVG/EquippingModel
struct EquippingModel_t128;
// System.String
struct String_t;

// System.Void Soomla.Store.EquippableVG/EquippingModel::.ctor(System.Int32,System.String)
extern "C" void EquippingModel__ctor_m619 (EquippingModel_t128 * __this, int32_t ___value, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EquippableVG/EquippingModel::.cctor()
extern "C" void EquippingModel__cctor_m620 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.Store.EquippableVG/EquippingModel::ToString()
extern "C" String_t* EquippingModel_ToString_m621 (EquippingModel_t128 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.EquippableVG/EquippingModel::toInt()
extern "C" int32_t EquippingModel_toInt_m622 (EquippingModel_t128 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
