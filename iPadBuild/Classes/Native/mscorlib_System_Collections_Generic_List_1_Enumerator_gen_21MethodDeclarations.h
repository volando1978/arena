﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>
struct Enumerator_t3687;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t861;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct List_1_t918;

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m19282(__this, ___l, method) (( void (*) (Enumerator_t3687 *, List_1_t918 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19283(__this, method) (( Object_t * (*) (Enumerator_t3687 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::Dispose()
#define Enumerator_Dispose_m19284(__this, method) (( void (*) (Enumerator_t3687 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::VerifyState()
#define Enumerator_VerifyState_m19285(__this, method) (( void (*) (Enumerator_t3687 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::MoveNext()
#define Enumerator_MoveNext_m19286(__this, method) (( bool (*) (Enumerator_t3687 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::get_Current()
#define Enumerator_get_Current_m19287(__this, method) (( Object_t * (*) (Enumerator_t3687 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
