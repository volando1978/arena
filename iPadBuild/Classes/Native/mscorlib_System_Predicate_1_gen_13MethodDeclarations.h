﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Soomla.Store.MarketItem>
struct Predicate_1_t3560;
// System.Object
struct Object_t;
// Soomla.Store.MarketItem
struct MarketItem_t122;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Soomla.Store.MarketItem>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_5MethodDeclarations.h"
#define Predicate_1__ctor_m17341(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3560 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m15507_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Soomla.Store.MarketItem>::Invoke(T)
#define Predicate_1_Invoke_m17342(__this, ___obj, method) (( bool (*) (Predicate_1_t3560 *, MarketItem_t122 *, MethodInfo*))Predicate_1_Invoke_m15508_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Soomla.Store.MarketItem>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m17343(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3560 *, MarketItem_t122 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m15509_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Soomla.Store.MarketItem>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m17344(__this, ___result, method) (( bool (*) (Predicate_1_t3560 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m15510_gshared)(__this, ___result, method)
