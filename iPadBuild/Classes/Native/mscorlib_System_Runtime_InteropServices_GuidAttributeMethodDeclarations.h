﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_t1425;
// System.String
struct String_t;

// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
extern "C" void GuidAttribute__ctor_m6181 (GuidAttribute_t1425 * __this, String_t* ___guid, MethodInfo* method) IL2CPP_METHOD_ATTR;
