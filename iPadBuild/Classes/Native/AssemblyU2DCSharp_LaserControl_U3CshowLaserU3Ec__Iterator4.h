﻿#pragma once
#include <stdint.h>
// UnityEngine.LineRenderer
struct LineRenderer_t743;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t744;
// System.Object
struct Object_t;
// LaserControl
struct LaserControl_t745;
// System.Object
#include "mscorlib_System_Object.h"
// LaserControl/<showLaser>c__Iterator4
struct  U3CshowLaserU3Ec__Iterator4_t746  : public Object_t
{
	// UnityEngine.LineRenderer LaserControl/<showLaser>c__Iterator4::<line>__0
	LineRenderer_t743 * ___U3ClineU3E__0_0;
	// UnityEngine.SpriteRenderer LaserControl/<showLaser>c__Iterator4::<spriteRenderer>__1
	SpriteRenderer_t744 * ___U3CspriteRendererU3E__1_1;
	// System.Int32 LaserControl/<showLaser>c__Iterator4::$PC
	int32_t ___U24PC_2;
	// System.Object LaserControl/<showLaser>c__Iterator4::$current
	Object_t * ___U24current_3;
	// LaserControl LaserControl/<showLaser>c__Iterator4::<>f__this
	LaserControl_t745 * ___U3CU3Ef__this_4;
};
