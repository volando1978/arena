﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.ContractionComparer
struct ContractionComparer_t2365;
// System.Object
struct Object_t;

// System.Void Mono.Globalization.Unicode.ContractionComparer::.ctor()
extern "C" void ContractionComparer__ctor_m10746 (ContractionComparer_t2365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.ContractionComparer::.cctor()
extern "C" void ContractionComparer__cctor_m10747 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.ContractionComparer::Compare(System.Object,System.Object)
extern "C" int32_t ContractionComparer_Compare_m10748 (ContractionComparer_t2365 * __this, Object_t * ___o1, Object_t * ___o2, MethodInfo* method) IL2CPP_METHOD_ATTR;
