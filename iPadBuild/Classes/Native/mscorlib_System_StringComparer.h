﻿#pragma once
#include <stdint.h>
// System.StringComparer
struct StringComparer_t1680;
// System.Object
#include "mscorlib_System_Object.h"
// System.StringComparer
struct  StringComparer_t1680  : public Object_t
{
};
struct StringComparer_t1680_StaticFields{
	// System.StringComparer System.StringComparer::invariantCultureIgnoreCase
	StringComparer_t1680 * ___invariantCultureIgnoreCase_0;
	// System.StringComparer System.StringComparer::invariantCulture
	StringComparer_t1680 * ___invariantCulture_1;
	// System.StringComparer System.StringComparer::ordinalIgnoreCase
	StringComparer_t1680 * ___ordinalIgnoreCase_2;
	// System.StringComparer System.StringComparer::ordinal
	StringComparer_t1680 * ___ordinal_3;
};
