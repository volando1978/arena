﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>
struct InternalEnumerator_1_t4225;
// System.Object
struct Object_t;
// System.Reflection.Emit.GenericTypeParameterBuilder
struct GenericTypeParameterBuilder_t2514;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m25930(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4225 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25931(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4225 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m25932(__this, method) (( void (*) (InternalEnumerator_1_t4225 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m25933(__this, method) (( bool (*) (InternalEnumerator_1_t4225 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m25934(__this, method) (( GenericTypeParameterBuilder_t2514 * (*) (InternalEnumerator_1_t4225 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
