﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>
struct InternalEnumerator_1_t4224;
// System.Object
struct Object_t;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t2518;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m25925(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4224 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25926(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4224 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m25927(__this, method) (( void (*) (InternalEnumerator_1_t4224 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m25928(__this, method) (( bool (*) (InternalEnumerator_1_t4224 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m25929(__this, method) (( ParameterBuilder_t2518 * (*) (InternalEnumerator_1_t4224 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
