﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager
struct SnapshotManager_t610;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>
struct Action_1_t893;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>
struct Action_1_t894;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>
struct Action_1_t895;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// GooglePlayGames.Native.NativeSnapshotMetadataChange
struct NativeSnapshotMetadataChange_t679;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>
struct Action_1_t896;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>
struct Action_1_t897;
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Snap.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void SnapshotManager__ctor_m3046 (SnapshotManager_t610 * __this, GameServices_t534 * ___services, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::FetchAll(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>)
extern "C" void SnapshotManager_FetchAll_m3047 (SnapshotManager_t610 * __this, int32_t ___source, Action_1_t893 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalFetchAllCallback(System.IntPtr,System.IntPtr)
extern "C" void SnapshotManager_InternalFetchAllCallback_m3048 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::SnapshotSelectUI(System.Boolean,System.Boolean,System.UInt32,System.String,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>)
extern "C" void SnapshotManager_SnapshotSelectUI_m3049 (SnapshotManager_t610 * __this, bool ___allowCreate, bool ___allowDelete, uint32_t ___maxSnapshots, String_t* ___uiTitle, Action_1_t894 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalSnapshotSelectUICallback(System.IntPtr,System.IntPtr)
extern "C" void SnapshotManager_InternalSnapshotSelectUICallback_m3050 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Open(System.String,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>)
extern "C" void SnapshotManager_Open_m3051 (SnapshotManager_t610 * __this, String_t* ___fileName, int32_t ___source, int32_t ___conflictPolicy, Action_1_t895 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalOpenCallback(System.IntPtr,System.IntPtr)
extern "C" void SnapshotManager_InternalOpenCallback_m3052 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Commit(GooglePlayGames.Native.NativeSnapshotMetadata,GooglePlayGames.Native.NativeSnapshotMetadataChange,System.Byte[],System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>)
extern "C" void SnapshotManager_Commit_m3053 (SnapshotManager_t610 * __this, NativeSnapshotMetadata_t611 * ___metadata, NativeSnapshotMetadataChange_t679 * ___metadataChange, ByteU5BU5D_t350* ___updatedData, Action_1_t896 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Resolve(GooglePlayGames.Native.NativeSnapshotMetadata,GooglePlayGames.Native.NativeSnapshotMetadataChange,System.String,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>)
extern "C" void SnapshotManager_Resolve_m3054 (SnapshotManager_t610 * __this, NativeSnapshotMetadata_t611 * ___metadata, NativeSnapshotMetadataChange_t679 * ___metadataChange, String_t* ___conflictId, Action_1_t896 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalCommitCallback(System.IntPtr,System.IntPtr)
extern "C" void SnapshotManager_InternalCommitCallback_m3055 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Delete(GooglePlayGames.Native.NativeSnapshotMetadata)
extern "C" void SnapshotManager_Delete_m3056 (SnapshotManager_t610 * __this, NativeSnapshotMetadata_t611 * ___metadata, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Read(GooglePlayGames.Native.NativeSnapshotMetadata,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>)
extern "C" void SnapshotManager_Read_m3057 (SnapshotManager_t610 * __this, NativeSnapshotMetadata_t611 * ___metadata, Action_1_t897 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalReadCallback(System.IntPtr,System.IntPtr)
extern "C" void SnapshotManager_InternalReadCallback_m3058 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
