﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEvent.h"
// Metadata Definition UnityEngine.UI.ScrollRect/ScrollRectEvent
extern TypeInfo ScrollRectEvent_t1282_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEventMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
MethodInfo ScrollRectEvent__ctor_m5256_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRectEvent__ctor_m5256/* method */
	, &ScrollRectEvent_t1282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ScrollRectEvent_t1282_MethodInfos[] =
{
	&ScrollRectEvent__ctor_m5256_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo UnityEventBase_ToString_m6293_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo;
extern Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m6314_GenericMethod;
extern Il2CppGenericMethod UnityEvent_1_GetDelegate_m6315_GenericMethod;
static Il2CppMethodReference ScrollRectEvent_t1282_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m6314_GenericMethod,
	&UnityEvent_1_GetDelegate_m6315_GenericMethod,
};
static bool ScrollRectEvent_t1282_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
extern Il2CppType ISerializationCallbackReceiver_t1446_0_0_0;
static Il2CppInterfaceOffsetPair ScrollRectEvent_t1282_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ScrollRectEvent_t1282_0_0_0;
extern Il2CppType ScrollRectEvent_t1282_1_0_0;
extern Il2CppType UnityEvent_1_t1283_0_0_0;
extern TypeInfo ScrollRect_t1285_il2cpp_TypeInfo;
extern Il2CppType ScrollRect_t1285_0_0_0;
struct ScrollRectEvent_t1282;
const Il2CppTypeDefinitionMetadata ScrollRectEvent_t1282_DefinitionMetadata = 
{
	&ScrollRect_t1285_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrollRectEvent_t1282_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t1283_0_0_0/* parent */
	, ScrollRectEvent_t1282_VTable/* vtableMethods */
	, ScrollRectEvent_t1282_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ScrollRectEvent_t1282_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRectEvent"/* name */
	, ""/* namespaze */
	, ScrollRectEvent_t1282_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ScrollRectEvent_t1282_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollRectEvent_t1282_0_0_0/* byval_arg */
	, &ScrollRectEvent_t1282_1_0_0/* this_arg */
	, &ScrollRectEvent_t1282_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRectEvent_t1282)/* instance_size */
	, sizeof (ScrollRectEvent_t1282)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
// Metadata Definition UnityEngine.UI.ScrollRect
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRectMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::.ctor()
MethodInfo ScrollRect__ctor_m5257_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRect__ctor_m5257/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_content()
MethodInfo ScrollRect_get_content_m5258_MethodInfo = 
{
	"get_content"/* name */
	, (methodPointerType)&ScrollRect_get_content_m5258/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_content_m5259_ParameterInfos[] = 
{
	{"value", 0, 134218189, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_content(UnityEngine.RectTransform)
MethodInfo ScrollRect_set_content_m5259_MethodInfo = 
{
	"set_content"/* name */
	, (methodPointerType)&ScrollRect_set_content_m5259/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_content_m5259_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_horizontal()
MethodInfo ScrollRect_get_horizontal_m5260_MethodInfo = 
{
	"get_horizontal"/* name */
	, (methodPointerType)&ScrollRect_get_horizontal_m5260/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_horizontal_m5261_ParameterInfos[] = 
{
	{"value", 0, 134218190, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontal(System.Boolean)
MethodInfo ScrollRect_set_horizontal_m5261_MethodInfo = 
{
	"set_horizontal"/* name */
	, (methodPointerType)&ScrollRect_set_horizontal_m5261/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_horizontal_m5261_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_vertical()
MethodInfo ScrollRect_get_vertical_m5262_MethodInfo = 
{
	"get_vertical"/* name */
	, (methodPointerType)&ScrollRect_get_vertical_m5262/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_vertical_m5263_ParameterInfos[] = 
{
	{"value", 0, 134218191, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_vertical(System.Boolean)
MethodInfo ScrollRect_set_vertical_m5263_MethodInfo = 
{
	"set_vertical"/* name */
	, (methodPointerType)&ScrollRect_set_vertical_m5263/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_vertical_m5263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MovementType_t1281_0_0_0;
extern void* RuntimeInvoker_MovementType_t1281 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::get_movementType()
MethodInfo ScrollRect_get_movementType_m5264_MethodInfo = 
{
	"get_movementType"/* name */
	, (methodPointerType)&ScrollRect_get_movementType_m5264/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &MovementType_t1281_0_0_0/* return_type */
	, RuntimeInvoker_MovementType_t1281/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MovementType_t1281_0_0_0;
extern Il2CppType MovementType_t1281_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_movementType_m5265_ParameterInfos[] = 
{
	{"value", 0, 134218192, 0, &MovementType_t1281_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_movementType(UnityEngine.UI.ScrollRect/MovementType)
MethodInfo ScrollRect_set_movementType_m5265_MethodInfo = 
{
	"set_movementType"/* name */
	, (methodPointerType)&ScrollRect_set_movementType_m5265/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_movementType_m5265_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_elasticity()
MethodInfo ScrollRect_get_elasticity_m5266_MethodInfo = 
{
	"get_elasticity"/* name */
	, (methodPointerType)&ScrollRect_get_elasticity_m5266/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_elasticity_m5267_ParameterInfos[] = 
{
	{"value", 0, 134218193, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_elasticity(System.Single)
MethodInfo ScrollRect_set_elasticity_m5267_MethodInfo = 
{
	"set_elasticity"/* name */
	, (methodPointerType)&ScrollRect_set_elasticity_m5267/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_elasticity_m5267_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_inertia()
MethodInfo ScrollRect_get_inertia_m5268_MethodInfo = 
{
	"get_inertia"/* name */
	, (methodPointerType)&ScrollRect_get_inertia_m5268/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_inertia_m5269_ParameterInfos[] = 
{
	{"value", 0, 134218194, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_inertia(System.Boolean)
MethodInfo ScrollRect_set_inertia_m5269_MethodInfo = 
{
	"set_inertia"/* name */
	, (methodPointerType)&ScrollRect_set_inertia_m5269/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_inertia_m5269_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_decelerationRate()
MethodInfo ScrollRect_get_decelerationRate_m5270_MethodInfo = 
{
	"get_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_get_decelerationRate_m5270/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_decelerationRate_m5271_ParameterInfos[] = 
{
	{"value", 0, 134218195, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_decelerationRate(System.Single)
MethodInfo ScrollRect_set_decelerationRate_m5271_MethodInfo = 
{
	"set_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_set_decelerationRate_m5271/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_decelerationRate_m5271_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_scrollSensitivity()
MethodInfo ScrollRect_get_scrollSensitivity_m5272_MethodInfo = 
{
	"get_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_get_scrollSensitivity_m5272/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_scrollSensitivity_m5273_ParameterInfos[] = 
{
	{"value", 0, 134218196, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_scrollSensitivity(System.Single)
MethodInfo ScrollRect_set_scrollSensitivity_m5273_MethodInfo = 
{
	"set_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_set_scrollSensitivity_m5273/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_scrollSensitivity_m5273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Scrollbar_t1278_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_horizontalScrollbar()
MethodInfo ScrollRect_get_horizontalScrollbar_m5274_MethodInfo = 
{
	"get_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalScrollbar_m5274/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t1278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Scrollbar_t1278_0_0_0;
extern Il2CppType Scrollbar_t1278_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_horizontalScrollbar_m5275_ParameterInfos[] = 
{
	{"value", 0, 134218197, 0, &Scrollbar_t1278_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalScrollbar(UnityEngine.UI.Scrollbar)
MethodInfo ScrollRect_set_horizontalScrollbar_m5275_MethodInfo = 
{
	"set_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalScrollbar_m5275/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_horizontalScrollbar_m5275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Scrollbar_t1278_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_verticalScrollbar()
MethodInfo ScrollRect_get_verticalScrollbar_m5276_MethodInfo = 
{
	"get_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_verticalScrollbar_m5276/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t1278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Scrollbar_t1278_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_verticalScrollbar_m5277_ParameterInfos[] = 
{
	{"value", 0, 134218198, 0, &Scrollbar_t1278_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalScrollbar(UnityEngine.UI.Scrollbar)
MethodInfo ScrollRect_set_verticalScrollbar_m5277_MethodInfo = 
{
	"set_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_verticalScrollbar_m5277/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_verticalScrollbar_m5277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScrollRectEvent_t1282_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::get_onValueChanged()
MethodInfo ScrollRect_get_onValueChanged_m5278_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_get_onValueChanged_m5278/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &ScrollRectEvent_t1282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScrollRectEvent_t1282_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_onValueChanged_m5279_ParameterInfos[] = 
{
	{"value", 0, 134218199, 0, &ScrollRectEvent_t1282_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_onValueChanged(UnityEngine.UI.ScrollRect/ScrollRectEvent)
MethodInfo ScrollRect_set_onValueChanged_m5279_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_set_onValueChanged_m5279/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_onValueChanged_m5279_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_viewRect()
MethodInfo ScrollRect_get_viewRect_m5280_MethodInfo = 
{
	"get_viewRect"/* name */
	, (methodPointerType)&ScrollRect_get_viewRect_m5280/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_velocity()
MethodInfo ScrollRect_get_velocity_m5281_MethodInfo = 
{
	"get_velocity"/* name */
	, (methodPointerType)&ScrollRect_get_velocity_m5281/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_velocity_m5282_ParameterInfos[] = 
{
	{"value", 0, 134218200, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_velocity(UnityEngine.Vector2)
MethodInfo ScrollRect_set_velocity_m5282_MethodInfo = 
{
	"set_velocity"/* name */
	, (methodPointerType)&ScrollRect_set_velocity_m5282/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_velocity_m5282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CanvasUpdate_t1216_0_0_0;
extern Il2CppType CanvasUpdate_t1216_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_Rebuild_m5283_ParameterInfos[] = 
{
	{"executing", 0, 134218201, 0, &CanvasUpdate_t1216_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::Rebuild(UnityEngine.UI.CanvasUpdate)
MethodInfo ScrollRect_Rebuild_m5283_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&ScrollRect_Rebuild_m5283/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ScrollRect_t1285_ScrollRect_Rebuild_m5283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEnable()
MethodInfo ScrollRect_OnEnable_m5284_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ScrollRect_OnEnable_m5284/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDisable()
MethodInfo ScrollRect_OnDisable_m5285_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ScrollRect_OnDisable_m5285/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::IsActive()
MethodInfo ScrollRect_IsActive_m5286_MethodInfo = 
{
	"IsActive"/* name */
	, (methodPointerType)&ScrollRect_IsActive_m5286/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::EnsureLayoutHasRebuilt()
MethodInfo ScrollRect_EnsureLayoutHasRebuilt_m5287_MethodInfo = 
{
	"EnsureLayoutHasRebuilt"/* name */
	, (methodPointerType)&ScrollRect_EnsureLayoutHasRebuilt_m5287/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::StopMovement()
MethodInfo ScrollRect_StopMovement_m5288_MethodInfo = 
{
	"StopMovement"/* name */
	, (methodPointerType)&ScrollRect_StopMovement_m5288/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_OnScroll_m5289_ParameterInfos[] = 
{
	{"data", 0, 134218202, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnScroll(UnityEngine.EventSystems.PointerEventData)
MethodInfo ScrollRect_OnScroll_m5289_MethodInfo = 
{
	"OnScroll"/* name */
	, (methodPointerType)&ScrollRect_OnScroll_m5289/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_OnScroll_m5289_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_OnInitializePotentialDrag_m5290_ParameterInfos[] = 
{
	{"eventData", 0, 134218203, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
MethodInfo ScrollRect_OnInitializePotentialDrag_m5290_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&ScrollRect_OnInitializePotentialDrag_m5290/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_OnInitializePotentialDrag_m5290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_OnBeginDrag_m5291_ParameterInfos[] = 
{
	{"eventData", 0, 134218204, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
MethodInfo ScrollRect_OnBeginDrag_m5291_MethodInfo = 
{
	"OnBeginDrag"/* name */
	, (methodPointerType)&ScrollRect_OnBeginDrag_m5291/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_OnBeginDrag_m5291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_OnEndDrag_m5292_ParameterInfos[] = 
{
	{"eventData", 0, 134218205, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
MethodInfo ScrollRect_OnEndDrag_m5292_MethodInfo = 
{
	"OnEndDrag"/* name */
	, (methodPointerType)&ScrollRect_OnEndDrag_m5292/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_OnEndDrag_m5292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_OnDrag_m5293_ParameterInfos[] = 
{
	{"eventData", 0, 134218206, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDrag(UnityEngine.EventSystems.PointerEventData)
MethodInfo ScrollRect_OnDrag_m5293_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&ScrollRect_OnDrag_m5293/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ScrollRect_t1285_ScrollRect_OnDrag_m5293_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_SetContentAnchoredPosition_m5294_ParameterInfos[] = 
{
	{"position", 0, 134218207, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetContentAnchoredPosition(UnityEngine.Vector2)
MethodInfo ScrollRect_SetContentAnchoredPosition_m5294_MethodInfo = 
{
	"SetContentAnchoredPosition"/* name */
	, (methodPointerType)&ScrollRect_SetContentAnchoredPosition_m5294/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, ScrollRect_t1285_ScrollRect_SetContentAnchoredPosition_m5294_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::LateUpdate()
MethodInfo ScrollRect_LateUpdate_m5295_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&ScrollRect_LateUpdate_m5295/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdatePrevData()
MethodInfo ScrollRect_UpdatePrevData_m5296_MethodInfo = 
{
	"UpdatePrevData"/* name */
	, (methodPointerType)&ScrollRect_UpdatePrevData_m5296/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_UpdateScrollbars_m5297_ParameterInfos[] = 
{
	{"offset", 0, 134218208, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateScrollbars(UnityEngine.Vector2)
MethodInfo ScrollRect_UpdateScrollbars_m5297_MethodInfo = 
{
	"UpdateScrollbars"/* name */
	, (methodPointerType)&ScrollRect_UpdateScrollbars_m5297/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, ScrollRect_t1285_ScrollRect_UpdateScrollbars_m5297_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_normalizedPosition()
MethodInfo ScrollRect_get_normalizedPosition_m5298_MethodInfo = 
{
	"get_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_normalizedPosition_m5298/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_normalizedPosition_m5299_ParameterInfos[] = 
{
	{"value", 0, 134218209, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_normalizedPosition(UnityEngine.Vector2)
MethodInfo ScrollRect_set_normalizedPosition_m5299_MethodInfo = 
{
	"set_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_normalizedPosition_m5299/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_normalizedPosition_m5299_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_horizontalNormalizedPosition()
MethodInfo ScrollRect_get_horizontalNormalizedPosition_m5300_MethodInfo = 
{
	"get_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalNormalizedPosition_m5300/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_horizontalNormalizedPosition_m5301_ParameterInfos[] = 
{
	{"value", 0, 134218210, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalNormalizedPosition(System.Single)
MethodInfo ScrollRect_set_horizontalNormalizedPosition_m5301_MethodInfo = 
{
	"set_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalNormalizedPosition_m5301/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_horizontalNormalizedPosition_m5301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_verticalNormalizedPosition()
MethodInfo ScrollRect_get_verticalNormalizedPosition_m5302_MethodInfo = 
{
	"get_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_verticalNormalizedPosition_m5302/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_set_verticalNormalizedPosition_m5303_ParameterInfos[] = 
{
	{"value", 0, 134218211, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalNormalizedPosition(System.Single)
MethodInfo ScrollRect_set_verticalNormalizedPosition_m5303_MethodInfo = 
{
	"set_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_verticalNormalizedPosition_m5303/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_set_verticalNormalizedPosition_m5303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_SetHorizontalNormalizedPosition_m5304_ParameterInfos[] = 
{
	{"value", 0, 134218212, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetHorizontalNormalizedPosition(System.Single)
MethodInfo ScrollRect_SetHorizontalNormalizedPosition_m5304_MethodInfo = 
{
	"SetHorizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetHorizontalNormalizedPosition_m5304/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_SetHorizontalNormalizedPosition_m5304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_SetVerticalNormalizedPosition_m5305_ParameterInfos[] = 
{
	{"value", 0, 134218213, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetVerticalNormalizedPosition(System.Single)
MethodInfo ScrollRect_SetVerticalNormalizedPosition_m5305_MethodInfo = 
{
	"SetVerticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetVerticalNormalizedPosition_m5305/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_SetVerticalNormalizedPosition_m5305_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_SetNormalizedPosition_m5306_ParameterInfos[] = 
{
	{"value", 0, 134218214, 0, &Single_t202_0_0_0},
	{"axis", 1, 134218215, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetNormalizedPosition(System.Single,System.Int32)
MethodInfo ScrollRect_SetNormalizedPosition_m5306_MethodInfo = 
{
	"SetNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetNormalizedPosition_m5306/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202_Int32_t189/* invoker_method */
	, ScrollRect_t1285_ScrollRect_SetNormalizedPosition_m5306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_RubberDelta_m5307_ParameterInfos[] = 
{
	{"overStretching", 0, 134218216, 0, &Single_t202_0_0_0},
	{"viewSize", 1, 134218217, 0, &Single_t202_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::RubberDelta(System.Single,System.Single)
MethodInfo ScrollRect_RubberDelta_m5307_MethodInfo = 
{
	"RubberDelta"/* name */
	, (methodPointerType)&ScrollRect_RubberDelta_m5307/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Single_t202_Single_t202/* invoker_method */
	, ScrollRect_t1285_ScrollRect_RubberDelta_m5307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateBounds()
MethodInfo ScrollRect_UpdateBounds_m5308_MethodInfo = 
{
	"UpdateBounds"/* name */
	, (methodPointerType)&ScrollRect_UpdateBounds_m5308/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Bounds_t979_0_0_0;
extern void* RuntimeInvoker_Bounds_t979 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Bounds UnityEngine.UI.ScrollRect::GetBounds()
MethodInfo ScrollRect_GetBounds_m5309_MethodInfo = 
{
	"GetBounds"/* name */
	, (methodPointerType)&ScrollRect_GetBounds_m5309/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Bounds_t979_0_0_0/* return_type */
	, RuntimeInvoker_Bounds_t979/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo ScrollRect_t1285_ScrollRect_CalculateOffset_m5310_ParameterInfos[] = 
{
	{"delta", 0, 134218218, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::CalculateOffset(UnityEngine.Vector2)
MethodInfo ScrollRect_CalculateOffset_m5310_MethodInfo = 
{
	"CalculateOffset"/* name */
	, (methodPointerType)&ScrollRect_CalculateOffset_m5310/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739_Vector2_t739/* invoker_method */
	, ScrollRect_t1285_ScrollRect_CalculateOffset_m5310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.IsDestroyed()
MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m5311_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m5311/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t809_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.get_transform()
MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m5312_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m5312/* method */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t809_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ScrollRect_t1285_MethodInfos[] =
{
	&ScrollRect__ctor_m5257_MethodInfo,
	&ScrollRect_get_content_m5258_MethodInfo,
	&ScrollRect_set_content_m5259_MethodInfo,
	&ScrollRect_get_horizontal_m5260_MethodInfo,
	&ScrollRect_set_horizontal_m5261_MethodInfo,
	&ScrollRect_get_vertical_m5262_MethodInfo,
	&ScrollRect_set_vertical_m5263_MethodInfo,
	&ScrollRect_get_movementType_m5264_MethodInfo,
	&ScrollRect_set_movementType_m5265_MethodInfo,
	&ScrollRect_get_elasticity_m5266_MethodInfo,
	&ScrollRect_set_elasticity_m5267_MethodInfo,
	&ScrollRect_get_inertia_m5268_MethodInfo,
	&ScrollRect_set_inertia_m5269_MethodInfo,
	&ScrollRect_get_decelerationRate_m5270_MethodInfo,
	&ScrollRect_set_decelerationRate_m5271_MethodInfo,
	&ScrollRect_get_scrollSensitivity_m5272_MethodInfo,
	&ScrollRect_set_scrollSensitivity_m5273_MethodInfo,
	&ScrollRect_get_horizontalScrollbar_m5274_MethodInfo,
	&ScrollRect_set_horizontalScrollbar_m5275_MethodInfo,
	&ScrollRect_get_verticalScrollbar_m5276_MethodInfo,
	&ScrollRect_set_verticalScrollbar_m5277_MethodInfo,
	&ScrollRect_get_onValueChanged_m5278_MethodInfo,
	&ScrollRect_set_onValueChanged_m5279_MethodInfo,
	&ScrollRect_get_viewRect_m5280_MethodInfo,
	&ScrollRect_get_velocity_m5281_MethodInfo,
	&ScrollRect_set_velocity_m5282_MethodInfo,
	&ScrollRect_Rebuild_m5283_MethodInfo,
	&ScrollRect_OnEnable_m5284_MethodInfo,
	&ScrollRect_OnDisable_m5285_MethodInfo,
	&ScrollRect_IsActive_m5286_MethodInfo,
	&ScrollRect_EnsureLayoutHasRebuilt_m5287_MethodInfo,
	&ScrollRect_StopMovement_m5288_MethodInfo,
	&ScrollRect_OnScroll_m5289_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m5290_MethodInfo,
	&ScrollRect_OnBeginDrag_m5291_MethodInfo,
	&ScrollRect_OnEndDrag_m5292_MethodInfo,
	&ScrollRect_OnDrag_m5293_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m5294_MethodInfo,
	&ScrollRect_LateUpdate_m5295_MethodInfo,
	&ScrollRect_UpdatePrevData_m5296_MethodInfo,
	&ScrollRect_UpdateScrollbars_m5297_MethodInfo,
	&ScrollRect_get_normalizedPosition_m5298_MethodInfo,
	&ScrollRect_set_normalizedPosition_m5299_MethodInfo,
	&ScrollRect_get_horizontalNormalizedPosition_m5300_MethodInfo,
	&ScrollRect_set_horizontalNormalizedPosition_m5301_MethodInfo,
	&ScrollRect_get_verticalNormalizedPosition_m5302_MethodInfo,
	&ScrollRect_set_verticalNormalizedPosition_m5303_MethodInfo,
	&ScrollRect_SetHorizontalNormalizedPosition_m5304_MethodInfo,
	&ScrollRect_SetVerticalNormalizedPosition_m5305_MethodInfo,
	&ScrollRect_SetNormalizedPosition_m5306_MethodInfo,
	&ScrollRect_RubberDelta_m5307_MethodInfo,
	&ScrollRect_UpdateBounds_m5308_MethodInfo,
	&ScrollRect_GetBounds_m5309_MethodInfo,
	&ScrollRect_CalculateOffset_m5310_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m5311_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m5312_MethodInfo,
	NULL
};
extern Il2CppType RectTransform_t1227_0_0_1;
FieldInfo ScrollRect_t1285____m_Content_2_FieldInfo = 
{
	"m_Content"/* name */
	, &RectTransform_t1227_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Content_2)/* offset */
	, 208/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ScrollRect_t1285____m_Horizontal_3_FieldInfo = 
{
	"m_Horizontal"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Horizontal_3)/* offset */
	, 209/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ScrollRect_t1285____m_Vertical_4_FieldInfo = 
{
	"m_Vertical"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Vertical_4)/* offset */
	, 210/* custom_attributes_cache */

};
extern Il2CppType MovementType_t1281_0_0_1;
FieldInfo ScrollRect_t1285____m_MovementType_5_FieldInfo = 
{
	"m_MovementType"/* name */
	, &MovementType_t1281_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_MovementType_5)/* offset */
	, 211/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo ScrollRect_t1285____m_Elasticity_6_FieldInfo = 
{
	"m_Elasticity"/* name */
	, &Single_t202_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Elasticity_6)/* offset */
	, 212/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ScrollRect_t1285____m_Inertia_7_FieldInfo = 
{
	"m_Inertia"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Inertia_7)/* offset */
	, 213/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo ScrollRect_t1285____m_DecelerationRate_8_FieldInfo = 
{
	"m_DecelerationRate"/* name */
	, &Single_t202_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_DecelerationRate_8)/* offset */
	, 214/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo ScrollRect_t1285____m_ScrollSensitivity_9_FieldInfo = 
{
	"m_ScrollSensitivity"/* name */
	, &Single_t202_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_ScrollSensitivity_9)/* offset */
	, 215/* custom_attributes_cache */

};
extern Il2CppType Scrollbar_t1278_0_0_1;
FieldInfo ScrollRect_t1285____m_HorizontalScrollbar_10_FieldInfo = 
{
	"m_HorizontalScrollbar"/* name */
	, &Scrollbar_t1278_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_HorizontalScrollbar_10)/* offset */
	, 216/* custom_attributes_cache */

};
extern Il2CppType Scrollbar_t1278_0_0_1;
FieldInfo ScrollRect_t1285____m_VerticalScrollbar_11_FieldInfo = 
{
	"m_VerticalScrollbar"/* name */
	, &Scrollbar_t1278_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_VerticalScrollbar_11)/* offset */
	, 217/* custom_attributes_cache */

};
extern Il2CppType ScrollRectEvent_t1282_0_0_1;
FieldInfo ScrollRect_t1285____m_OnValueChanged_12_FieldInfo = 
{
	"m_OnValueChanged"/* name */
	, &ScrollRectEvent_t1282_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_OnValueChanged_12)/* offset */
	, 218/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo ScrollRect_t1285____m_PointerStartLocalCursor_13_FieldInfo = 
{
	"m_PointerStartLocalCursor"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_PointerStartLocalCursor_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo ScrollRect_t1285____m_ContentStartPosition_14_FieldInfo = 
{
	"m_ContentStartPosition"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_ContentStartPosition_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_1;
FieldInfo ScrollRect_t1285____m_ViewRect_15_FieldInfo = 
{
	"m_ViewRect"/* name */
	, &RectTransform_t1227_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_ViewRect_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Bounds_t979_0_0_1;
FieldInfo ScrollRect_t1285____m_ContentBounds_16_FieldInfo = 
{
	"m_ContentBounds"/* name */
	, &Bounds_t979_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_ContentBounds_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Bounds_t979_0_0_1;
FieldInfo ScrollRect_t1285____m_ViewBounds_17_FieldInfo = 
{
	"m_ViewBounds"/* name */
	, &Bounds_t979_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_ViewBounds_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo ScrollRect_t1285____m_Velocity_18_FieldInfo = 
{
	"m_Velocity"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Velocity_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ScrollRect_t1285____m_Dragging_19_FieldInfo = 
{
	"m_Dragging"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Dragging_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo ScrollRect_t1285____m_PrevPosition_20_FieldInfo = 
{
	"m_PrevPosition"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_PrevPosition_20)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Bounds_t979_0_0_1;
FieldInfo ScrollRect_t1285____m_PrevContentBounds_21_FieldInfo = 
{
	"m_PrevContentBounds"/* name */
	, &Bounds_t979_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_PrevContentBounds_21)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Bounds_t979_0_0_1;
FieldInfo ScrollRect_t1285____m_PrevViewBounds_22_FieldInfo = 
{
	"m_PrevViewBounds"/* name */
	, &Bounds_t979_0_0_1/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_PrevViewBounds_22)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_129;
FieldInfo ScrollRect_t1285____m_HasRebuiltLayout_23_FieldInfo = 
{
	"m_HasRebuiltLayout"/* name */
	, &Boolean_t203_0_0_129/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_HasRebuiltLayout_23)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3U5BU5D_t1284_0_0_33;
FieldInfo ScrollRect_t1285____m_Corners_24_FieldInfo = 
{
	"m_Corners"/* name */
	, &Vector3U5BU5D_t1284_0_0_33/* type */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, offsetof(ScrollRect_t1285, ___m_Corners_24)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ScrollRect_t1285_FieldInfos[] =
{
	&ScrollRect_t1285____m_Content_2_FieldInfo,
	&ScrollRect_t1285____m_Horizontal_3_FieldInfo,
	&ScrollRect_t1285____m_Vertical_4_FieldInfo,
	&ScrollRect_t1285____m_MovementType_5_FieldInfo,
	&ScrollRect_t1285____m_Elasticity_6_FieldInfo,
	&ScrollRect_t1285____m_Inertia_7_FieldInfo,
	&ScrollRect_t1285____m_DecelerationRate_8_FieldInfo,
	&ScrollRect_t1285____m_ScrollSensitivity_9_FieldInfo,
	&ScrollRect_t1285____m_HorizontalScrollbar_10_FieldInfo,
	&ScrollRect_t1285____m_VerticalScrollbar_11_FieldInfo,
	&ScrollRect_t1285____m_OnValueChanged_12_FieldInfo,
	&ScrollRect_t1285____m_PointerStartLocalCursor_13_FieldInfo,
	&ScrollRect_t1285____m_ContentStartPosition_14_FieldInfo,
	&ScrollRect_t1285____m_ViewRect_15_FieldInfo,
	&ScrollRect_t1285____m_ContentBounds_16_FieldInfo,
	&ScrollRect_t1285____m_ViewBounds_17_FieldInfo,
	&ScrollRect_t1285____m_Velocity_18_FieldInfo,
	&ScrollRect_t1285____m_Dragging_19_FieldInfo,
	&ScrollRect_t1285____m_PrevPosition_20_FieldInfo,
	&ScrollRect_t1285____m_PrevContentBounds_21_FieldInfo,
	&ScrollRect_t1285____m_PrevViewBounds_22_FieldInfo,
	&ScrollRect_t1285____m_HasRebuiltLayout_23_FieldInfo,
	&ScrollRect_t1285____m_Corners_24_FieldInfo,
	NULL
};
extern MethodInfo ScrollRect_get_content_m5258_MethodInfo;
extern MethodInfo ScrollRect_set_content_m5259_MethodInfo;
static PropertyInfo ScrollRect_t1285____content_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "content"/* name */
	, &ScrollRect_get_content_m5258_MethodInfo/* get */
	, &ScrollRect_set_content_m5259_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_horizontal_m5260_MethodInfo;
extern MethodInfo ScrollRect_set_horizontal_m5261_MethodInfo;
static PropertyInfo ScrollRect_t1285____horizontal_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "horizontal"/* name */
	, &ScrollRect_get_horizontal_m5260_MethodInfo/* get */
	, &ScrollRect_set_horizontal_m5261_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_vertical_m5262_MethodInfo;
extern MethodInfo ScrollRect_set_vertical_m5263_MethodInfo;
static PropertyInfo ScrollRect_t1285____vertical_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "vertical"/* name */
	, &ScrollRect_get_vertical_m5262_MethodInfo/* get */
	, &ScrollRect_set_vertical_m5263_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_movementType_m5264_MethodInfo;
extern MethodInfo ScrollRect_set_movementType_m5265_MethodInfo;
static PropertyInfo ScrollRect_t1285____movementType_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "movementType"/* name */
	, &ScrollRect_get_movementType_m5264_MethodInfo/* get */
	, &ScrollRect_set_movementType_m5265_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_elasticity_m5266_MethodInfo;
extern MethodInfo ScrollRect_set_elasticity_m5267_MethodInfo;
static PropertyInfo ScrollRect_t1285____elasticity_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "elasticity"/* name */
	, &ScrollRect_get_elasticity_m5266_MethodInfo/* get */
	, &ScrollRect_set_elasticity_m5267_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_inertia_m5268_MethodInfo;
extern MethodInfo ScrollRect_set_inertia_m5269_MethodInfo;
static PropertyInfo ScrollRect_t1285____inertia_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "inertia"/* name */
	, &ScrollRect_get_inertia_m5268_MethodInfo/* get */
	, &ScrollRect_set_inertia_m5269_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_decelerationRate_m5270_MethodInfo;
extern MethodInfo ScrollRect_set_decelerationRate_m5271_MethodInfo;
static PropertyInfo ScrollRect_t1285____decelerationRate_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "decelerationRate"/* name */
	, &ScrollRect_get_decelerationRate_m5270_MethodInfo/* get */
	, &ScrollRect_set_decelerationRate_m5271_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_scrollSensitivity_m5272_MethodInfo;
extern MethodInfo ScrollRect_set_scrollSensitivity_m5273_MethodInfo;
static PropertyInfo ScrollRect_t1285____scrollSensitivity_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "scrollSensitivity"/* name */
	, &ScrollRect_get_scrollSensitivity_m5272_MethodInfo/* get */
	, &ScrollRect_set_scrollSensitivity_m5273_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_horizontalScrollbar_m5274_MethodInfo;
extern MethodInfo ScrollRect_set_horizontalScrollbar_m5275_MethodInfo;
static PropertyInfo ScrollRect_t1285____horizontalScrollbar_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "horizontalScrollbar"/* name */
	, &ScrollRect_get_horizontalScrollbar_m5274_MethodInfo/* get */
	, &ScrollRect_set_horizontalScrollbar_m5275_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_verticalScrollbar_m5276_MethodInfo;
extern MethodInfo ScrollRect_set_verticalScrollbar_m5277_MethodInfo;
static PropertyInfo ScrollRect_t1285____verticalScrollbar_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "verticalScrollbar"/* name */
	, &ScrollRect_get_verticalScrollbar_m5276_MethodInfo/* get */
	, &ScrollRect_set_verticalScrollbar_m5277_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_onValueChanged_m5278_MethodInfo;
extern MethodInfo ScrollRect_set_onValueChanged_m5279_MethodInfo;
static PropertyInfo ScrollRect_t1285____onValueChanged_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &ScrollRect_get_onValueChanged_m5278_MethodInfo/* get */
	, &ScrollRect_set_onValueChanged_m5279_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_viewRect_m5280_MethodInfo;
static PropertyInfo ScrollRect_t1285____viewRect_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "viewRect"/* name */
	, &ScrollRect_get_viewRect_m5280_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_velocity_m5281_MethodInfo;
extern MethodInfo ScrollRect_set_velocity_m5282_MethodInfo;
static PropertyInfo ScrollRect_t1285____velocity_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "velocity"/* name */
	, &ScrollRect_get_velocity_m5281_MethodInfo/* get */
	, &ScrollRect_set_velocity_m5282_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_normalizedPosition_m5298_MethodInfo;
extern MethodInfo ScrollRect_set_normalizedPosition_m5299_MethodInfo;
static PropertyInfo ScrollRect_t1285____normalizedPosition_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "normalizedPosition"/* name */
	, &ScrollRect_get_normalizedPosition_m5298_MethodInfo/* get */
	, &ScrollRect_set_normalizedPosition_m5299_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_horizontalNormalizedPosition_m5300_MethodInfo;
extern MethodInfo ScrollRect_set_horizontalNormalizedPosition_m5301_MethodInfo;
static PropertyInfo ScrollRect_t1285____horizontalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "horizontalNormalizedPosition"/* name */
	, &ScrollRect_get_horizontalNormalizedPosition_m5300_MethodInfo/* get */
	, &ScrollRect_set_horizontalNormalizedPosition_m5301_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ScrollRect_get_verticalNormalizedPosition_m5302_MethodInfo;
extern MethodInfo ScrollRect_set_verticalNormalizedPosition_m5303_MethodInfo;
static PropertyInfo ScrollRect_t1285____verticalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t1285_il2cpp_TypeInfo/* parent */
	, "verticalNormalizedPosition"/* name */
	, &ScrollRect_get_verticalNormalizedPosition_m5302_MethodInfo/* get */
	, &ScrollRect_set_verticalNormalizedPosition_m5303_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ScrollRect_t1285_PropertyInfos[] =
{
	&ScrollRect_t1285____content_PropertyInfo,
	&ScrollRect_t1285____horizontal_PropertyInfo,
	&ScrollRect_t1285____vertical_PropertyInfo,
	&ScrollRect_t1285____movementType_PropertyInfo,
	&ScrollRect_t1285____elasticity_PropertyInfo,
	&ScrollRect_t1285____inertia_PropertyInfo,
	&ScrollRect_t1285____decelerationRate_PropertyInfo,
	&ScrollRect_t1285____scrollSensitivity_PropertyInfo,
	&ScrollRect_t1285____horizontalScrollbar_PropertyInfo,
	&ScrollRect_t1285____verticalScrollbar_PropertyInfo,
	&ScrollRect_t1285____onValueChanged_PropertyInfo,
	&ScrollRect_t1285____viewRect_PropertyInfo,
	&ScrollRect_t1285____velocity_PropertyInfo,
	&ScrollRect_t1285____normalizedPosition_PropertyInfo,
	&ScrollRect_t1285____horizontalNormalizedPosition_PropertyInfo,
	&ScrollRect_t1285____verticalNormalizedPosition_PropertyInfo,
	NULL
};
static const Il2CppType* ScrollRect_t1285_il2cpp_TypeInfo__nestedTypes[2] =
{
	&MovementType_t1281_0_0_0,
	&ScrollRectEvent_t1282_0_0_0,
};
extern MethodInfo Object_Equals_m1199_MethodInfo;
extern MethodInfo Object_GetHashCode_m1200_MethodInfo;
extern MethodInfo Object_ToString_m1201_MethodInfo;
extern MethodInfo UIBehaviour_Awake_m4650_MethodInfo;
extern MethodInfo ScrollRect_OnEnable_m5284_MethodInfo;
extern MethodInfo UIBehaviour_Start_m4652_MethodInfo;
extern MethodInfo ScrollRect_OnDisable_m5285_MethodInfo;
extern MethodInfo UIBehaviour_OnDestroy_m4654_MethodInfo;
extern MethodInfo ScrollRect_IsActive_m5286_MethodInfo;
extern MethodInfo UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo;
extern MethodInfo UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo;
extern MethodInfo UIBehaviour_OnTransformParentChanged_m4658_MethodInfo;
extern MethodInfo UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo;
extern MethodInfo UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo;
extern MethodInfo ScrollRect_OnBeginDrag_m5291_MethodInfo;
extern MethodInfo ScrollRect_OnInitializePotentialDrag_m5290_MethodInfo;
extern MethodInfo ScrollRect_OnDrag_m5293_MethodInfo;
extern MethodInfo ScrollRect_OnEndDrag_m5292_MethodInfo;
extern MethodInfo ScrollRect_OnScroll_m5289_MethodInfo;
extern MethodInfo ScrollRect_Rebuild_m5283_MethodInfo;
extern MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m5312_MethodInfo;
extern MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m5311_MethodInfo;
extern MethodInfo ScrollRect_StopMovement_m5288_MethodInfo;
extern MethodInfo ScrollRect_SetContentAnchoredPosition_m5294_MethodInfo;
extern MethodInfo ScrollRect_LateUpdate_m5295_MethodInfo;
static Il2CppMethodReference ScrollRect_t1285_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&ScrollRect_OnEnable_m5284_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&ScrollRect_OnDisable_m5285_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&ScrollRect_IsActive_m5286_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&ScrollRect_OnBeginDrag_m5291_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m5290_MethodInfo,
	&ScrollRect_OnDrag_m5293_MethodInfo,
	&ScrollRect_OnEndDrag_m5292_MethodInfo,
	&ScrollRect_OnScroll_m5289_MethodInfo,
	&ScrollRect_Rebuild_m5283_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m5312_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m5311_MethodInfo,
	&ScrollRect_Rebuild_m5283_MethodInfo,
	&ScrollRect_StopMovement_m5288_MethodInfo,
	&ScrollRect_OnScroll_m5289_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m5290_MethodInfo,
	&ScrollRect_OnBeginDrag_m5291_MethodInfo,
	&ScrollRect_OnEndDrag_m5292_MethodInfo,
	&ScrollRect_OnDrag_m5293_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m5294_MethodInfo,
	&ScrollRect_LateUpdate_m5295_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m5311_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m5312_MethodInfo,
};
static bool ScrollRect_t1285_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEventSystemHandler_t1428_0_0_0;
extern Il2CppType IBeginDragHandler_t1346_0_0_0;
extern Il2CppType IInitializePotentialDragHandler_t1345_0_0_0;
extern Il2CppType IDragHandler_t1347_0_0_0;
extern Il2CppType IEndDragHandler_t1348_0_0_0;
extern Il2CppType IScrollHandler_t1350_0_0_0;
extern Il2CppType ICanvasElement_t1360_0_0_0;
static const Il2CppType* ScrollRect_t1285_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t1428_0_0_0,
	&IBeginDragHandler_t1346_0_0_0,
	&IInitializePotentialDragHandler_t1345_0_0_0,
	&IDragHandler_t1347_0_0_0,
	&IEndDragHandler_t1348_0_0_0,
	&IScrollHandler_t1350_0_0_0,
	&ICanvasElement_t1360_0_0_0,
};
static Il2CppInterfaceOffsetPair ScrollRect_t1285_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t1428_0_0_0, 15},
	{ &IBeginDragHandler_t1346_0_0_0, 15},
	{ &IInitializePotentialDragHandler_t1345_0_0_0, 16},
	{ &IDragHandler_t1347_0_0_0, 17},
	{ &IEndDragHandler_t1348_0_0_0, 18},
	{ &IScrollHandler_t1350_0_0_0, 19},
	{ &ICanvasElement_t1360_0_0_0, 20},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ScrollRect_t1285_1_0_0;
extern Il2CppType UIBehaviour_t1156_0_0_0;
struct ScrollRect_t1285;
const Il2CppTypeDefinitionMetadata ScrollRect_t1285_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScrollRect_t1285_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ScrollRect_t1285_InterfacesTypeInfos/* implementedInterfaces */
	, ScrollRect_t1285_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, ScrollRect_t1285_VTable/* vtableMethods */
	, ScrollRect_t1285_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ScrollRect_t1285_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ScrollRect_t1285_MethodInfos/* methods */
	, ScrollRect_t1285_PropertyInfos/* properties */
	, ScrollRect_t1285_FieldInfos/* fields */
	, NULL/* events */
	, &ScrollRect_t1285_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 207/* custom_attributes_cache */
	, &ScrollRect_t1285_0_0_0/* byval_arg */
	, &ScrollRect_t1285_1_0_0/* this_arg */
	, &ScrollRect_t1285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRect_t1285)/* instance_size */
	, sizeof (ScrollRect_t1285)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 56/* method_count */
	, 16/* property_count */
	, 23/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 34/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
// Metadata Definition UnityEngine.UI.Selectable/Transition
extern TypeInfo Transition_t1286_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_TransitionMethodDeclarations.h"
static MethodInfo* Transition_t1286_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Transition_t1286____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Transition_t1286_il2cpp_TypeInfo/* parent */
	, offsetof(Transition_t1286, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Transition_t1286_0_0_32854;
FieldInfo Transition_t1286____None_2_FieldInfo = 
{
	"None"/* name */
	, &Transition_t1286_0_0_32854/* type */
	, &Transition_t1286_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Transition_t1286_0_0_32854;
FieldInfo Transition_t1286____ColorTint_3_FieldInfo = 
{
	"ColorTint"/* name */
	, &Transition_t1286_0_0_32854/* type */
	, &Transition_t1286_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Transition_t1286_0_0_32854;
FieldInfo Transition_t1286____SpriteSwap_4_FieldInfo = 
{
	"SpriteSwap"/* name */
	, &Transition_t1286_0_0_32854/* type */
	, &Transition_t1286_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Transition_t1286_0_0_32854;
FieldInfo Transition_t1286____Animation_5_FieldInfo = 
{
	"Animation"/* name */
	, &Transition_t1286_0_0_32854/* type */
	, &Transition_t1286_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Transition_t1286_FieldInfos[] =
{
	&Transition_t1286____value___1_FieldInfo,
	&Transition_t1286____None_2_FieldInfo,
	&Transition_t1286____ColorTint_3_FieldInfo,
	&Transition_t1286____SpriteSwap_4_FieldInfo,
	&Transition_t1286____Animation_5_FieldInfo,
	NULL
};
static const int32_t Transition_t1286____None_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Transition_t1286____None_2_DefaultValue = 
{
	&Transition_t1286____None_2_FieldInfo/* field */
	, { (char*)&Transition_t1286____None_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Transition_t1286____ColorTint_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Transition_t1286____ColorTint_3_DefaultValue = 
{
	&Transition_t1286____ColorTint_3_FieldInfo/* field */
	, { (char*)&Transition_t1286____ColorTint_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Transition_t1286____SpriteSwap_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry Transition_t1286____SpriteSwap_4_DefaultValue = 
{
	&Transition_t1286____SpriteSwap_4_FieldInfo/* field */
	, { (char*)&Transition_t1286____SpriteSwap_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Transition_t1286____Animation_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry Transition_t1286____Animation_5_DefaultValue = 
{
	&Transition_t1286____Animation_5_FieldInfo/* field */
	, { (char*)&Transition_t1286____Animation_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Transition_t1286_FieldDefaultValues[] = 
{
	&Transition_t1286____None_2_DefaultValue,
	&Transition_t1286____ColorTint_3_DefaultValue,
	&Transition_t1286____SpriteSwap_4_DefaultValue,
	&Transition_t1286____Animation_5_DefaultValue,
	NULL
};
extern MethodInfo Enum_Equals_m1279_MethodInfo;
extern MethodInfo Enum_GetHashCode_m1280_MethodInfo;
extern MethodInfo Enum_ToString_m1281_MethodInfo;
extern MethodInfo Enum_ToString_m1282_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m1283_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m1284_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m1285_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m1286_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m1287_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m1288_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m1289_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m1290_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m1291_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m1292_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m1293_MethodInfo;
extern MethodInfo Enum_ToString_m1294_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m1295_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m1296_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m1297_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m1298_MethodInfo;
extern MethodInfo Enum_CompareTo_m1299_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m1300_MethodInfo;
static Il2CppMethodReference Transition_t1286_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Transition_t1286_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormattable_t312_0_0_0;
extern Il2CppType IConvertible_t313_0_0_0;
extern Il2CppType IComparable_t314_0_0_0;
static Il2CppInterfaceOffsetPair Transition_t1286_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Transition_t1286_0_0_0;
extern Il2CppType Transition_t1286_1_0_0;
extern Il2CppType Enum_t218_0_0_0;
extern TypeInfo Selectable_t1215_il2cpp_TypeInfo;
extern Il2CppType Selectable_t1215_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t189_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Transition_t1286_DefinitionMetadata = 
{
	&Selectable_t1215_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transition_t1286_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Transition_t1286_VTable/* vtableMethods */
	, Transition_t1286_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Transition_t1286_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transition"/* name */
	, ""/* namespaze */
	, Transition_t1286_MethodInfos/* methods */
	, NULL/* properties */
	, Transition_t1286_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transition_t1286_0_0_0/* byval_arg */
	, &Transition_t1286_1_0_0/* this_arg */
	, &Transition_t1286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Transition_t1286_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transition_t1286)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Transition_t1286)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
// Metadata Definition UnityEngine.UI.Selectable/SelectionState
extern TypeInfo SelectionState_t1287_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionStateMethodDeclarations.h"
static MethodInfo* SelectionState_t1287_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo SelectionState_t1287____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &SelectionState_t1287_il2cpp_TypeInfo/* parent */
	, offsetof(SelectionState_t1287, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SelectionState_t1287_0_0_32854;
FieldInfo SelectionState_t1287____Normal_2_FieldInfo = 
{
	"Normal"/* name */
	, &SelectionState_t1287_0_0_32854/* type */
	, &SelectionState_t1287_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SelectionState_t1287_0_0_32854;
FieldInfo SelectionState_t1287____Highlighted_3_FieldInfo = 
{
	"Highlighted"/* name */
	, &SelectionState_t1287_0_0_32854/* type */
	, &SelectionState_t1287_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SelectionState_t1287_0_0_32854;
FieldInfo SelectionState_t1287____Pressed_4_FieldInfo = 
{
	"Pressed"/* name */
	, &SelectionState_t1287_0_0_32854/* type */
	, &SelectionState_t1287_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SelectionState_t1287_0_0_32854;
FieldInfo SelectionState_t1287____Disabled_5_FieldInfo = 
{
	"Disabled"/* name */
	, &SelectionState_t1287_0_0_32854/* type */
	, &SelectionState_t1287_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* SelectionState_t1287_FieldInfos[] =
{
	&SelectionState_t1287____value___1_FieldInfo,
	&SelectionState_t1287____Normal_2_FieldInfo,
	&SelectionState_t1287____Highlighted_3_FieldInfo,
	&SelectionState_t1287____Pressed_4_FieldInfo,
	&SelectionState_t1287____Disabled_5_FieldInfo,
	NULL
};
static const int32_t SelectionState_t1287____Normal_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry SelectionState_t1287____Normal_2_DefaultValue = 
{
	&SelectionState_t1287____Normal_2_FieldInfo/* field */
	, { (char*)&SelectionState_t1287____Normal_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t SelectionState_t1287____Highlighted_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry SelectionState_t1287____Highlighted_3_DefaultValue = 
{
	&SelectionState_t1287____Highlighted_3_FieldInfo/* field */
	, { (char*)&SelectionState_t1287____Highlighted_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t SelectionState_t1287____Pressed_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry SelectionState_t1287____Pressed_4_DefaultValue = 
{
	&SelectionState_t1287____Pressed_4_FieldInfo/* field */
	, { (char*)&SelectionState_t1287____Pressed_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t SelectionState_t1287____Disabled_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry SelectionState_t1287____Disabled_5_DefaultValue = 
{
	&SelectionState_t1287____Disabled_5_FieldInfo/* field */
	, { (char*)&SelectionState_t1287____Disabled_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* SelectionState_t1287_FieldDefaultValues[] = 
{
	&SelectionState_t1287____Normal_2_DefaultValue,
	&SelectionState_t1287____Highlighted_3_DefaultValue,
	&SelectionState_t1287____Pressed_4_DefaultValue,
	&SelectionState_t1287____Disabled_5_DefaultValue,
	NULL
};
static Il2CppMethodReference SelectionState_t1287_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool SelectionState_t1287_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionState_t1287_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType SelectionState_t1287_0_0_0;
extern Il2CppType SelectionState_t1287_1_0_0;
const Il2CppTypeDefinitionMetadata SelectionState_t1287_DefinitionMetadata = 
{
	&Selectable_t1215_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionState_t1287_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, SelectionState_t1287_VTable/* vtableMethods */
	, SelectionState_t1287_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SelectionState_t1287_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionState"/* name */
	, ""/* namespaze */
	, SelectionState_t1287_MethodInfos/* methods */
	, NULL/* properties */
	, SelectionState_t1287_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SelectionState_t1287_0_0_0/* byval_arg */
	, &SelectionState_t1287_1_0_0/* this_arg */
	, &SelectionState_t1287_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, SelectionState_t1287_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionState_t1287)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SelectionState_t1287)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 260/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// Metadata Definition UnityEngine.UI.Selectable
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.ctor()
MethodInfo Selectable__ctor_m5313_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Selectable__ctor_m5313/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.cctor()
MethodInfo Selectable__cctor_m5314_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Selectable__cctor_m5314/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1288_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::get_allSelectables()
MethodInfo Selectable_get_allSelectables_m5315_MethodInfo = 
{
	"get_allSelectables"/* name */
	, (methodPointerType)&Selectable_get_allSelectables_m5315/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1288_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Navigation_t1272_0_0_0;
extern void* RuntimeInvoker_Navigation_t1272 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::get_navigation()
MethodInfo Selectable_get_navigation_m5316_MethodInfo = 
{
	"get_navigation"/* name */
	, (methodPointerType)&Selectable_get_navigation_m5316/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Navigation_t1272_0_0_0/* return_type */
	, RuntimeInvoker_Navigation_t1272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Navigation_t1272_0_0_0;
extern Il2CppType Navigation_t1272_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_navigation_m5317_ParameterInfos[] = 
{
	{"value", 0, 134218219, 0, &Navigation_t1272_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Navigation_t1272 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_navigation(UnityEngine.UI.Navigation)
MethodInfo Selectable_set_navigation_m5317_MethodInfo = 
{
	"set_navigation"/* name */
	, (methodPointerType)&Selectable_set_navigation_m5317/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Navigation_t1272/* invoker_method */
	, Selectable_t1215_Selectable_set_navigation_m5317_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transition_t1286_0_0_0;
extern void* RuntimeInvoker_Transition_t1286 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::get_transition()
MethodInfo Selectable_get_transition_m5318_MethodInfo = 
{
	"get_transition"/* name */
	, (methodPointerType)&Selectable_get_transition_m5318/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Transition_t1286_0_0_0/* return_type */
	, RuntimeInvoker_Transition_t1286/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transition_t1286_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_transition_m5319_ParameterInfos[] = 
{
	{"value", 0, 134218220, 0, &Transition_t1286_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_transition(UnityEngine.UI.Selectable/Transition)
MethodInfo Selectable_set_transition_m5319_MethodInfo = 
{
	"set_transition"/* name */
	, (methodPointerType)&Selectable_set_transition_m5319/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Selectable_t1215_Selectable_set_transition_m5319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ColorBlock_t1221_0_0_0;
extern void* RuntimeInvoker_ColorBlock_t1221 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
MethodInfo Selectable_get_colors_m5320_MethodInfo = 
{
	"get_colors"/* name */
	, (methodPointerType)&Selectable_get_colors_m5320/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &ColorBlock_t1221_0_0_0/* return_type */
	, RuntimeInvoker_ColorBlock_t1221/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ColorBlock_t1221_0_0_0;
extern Il2CppType ColorBlock_t1221_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_colors_m5321_ParameterInfos[] = 
{
	{"value", 0, 134218221, 0, &ColorBlock_t1221_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_ColorBlock_t1221 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
MethodInfo Selectable_set_colors_m5321_MethodInfo = 
{
	"set_colors"/* name */
	, (methodPointerType)&Selectable_set_colors_m5321/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_ColorBlock_t1221/* invoker_method */
	, Selectable_t1215_Selectable_set_colors_m5321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SpriteState_t1290_0_0_0;
extern void* RuntimeInvoker_SpriteState_t1290 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::get_spriteState()
MethodInfo Selectable_get_spriteState_m5322_MethodInfo = 
{
	"get_spriteState"/* name */
	, (methodPointerType)&Selectable_get_spriteState_m5322/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &SpriteState_t1290_0_0_0/* return_type */
	, RuntimeInvoker_SpriteState_t1290/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SpriteState_t1290_0_0_0;
extern Il2CppType SpriteState_t1290_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_spriteState_m5323_ParameterInfos[] = 
{
	{"value", 0, 134218222, 0, &SpriteState_t1290_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SpriteState_t1290 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_spriteState(UnityEngine.UI.SpriteState)
MethodInfo Selectable_set_spriteState_m5323_MethodInfo = 
{
	"set_spriteState"/* name */
	, (methodPointerType)&Selectable_set_spriteState_m5323/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SpriteState_t1290/* invoker_method */
	, Selectable_t1215_Selectable_set_spriteState_m5323_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AnimationTriggers_t1210_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::get_animationTriggers()
MethodInfo Selectable_get_animationTriggers_m5324_MethodInfo = 
{
	"get_animationTriggers"/* name */
	, (methodPointerType)&Selectable_get_animationTriggers_m5324/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &AnimationTriggers_t1210_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AnimationTriggers_t1210_0_0_0;
extern Il2CppType AnimationTriggers_t1210_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_animationTriggers_m5325_ParameterInfos[] = 
{
	{"value", 0, 134218223, 0, &AnimationTriggers_t1210_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_animationTriggers(UnityEngine.UI.AnimationTriggers)
MethodInfo Selectable_set_animationTriggers_m5325_MethodInfo = 
{
	"set_animationTriggers"/* name */
	, (methodPointerType)&Selectable_set_animationTriggers_m5325/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_set_animationTriggers_m5325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Graphic_t1233_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::get_targetGraphic()
MethodInfo Selectable_get_targetGraphic_m5326_MethodInfo = 
{
	"get_targetGraphic"/* name */
	, (methodPointerType)&Selectable_get_targetGraphic_m5326/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t1233_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Graphic_t1233_0_0_0;
extern Il2CppType Graphic_t1233_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_targetGraphic_m5327_ParameterInfos[] = 
{
	{"value", 0, 134218224, 0, &Graphic_t1233_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_targetGraphic(UnityEngine.UI.Graphic)
MethodInfo Selectable_set_targetGraphic_m5327_MethodInfo = 
{
	"set_targetGraphic"/* name */
	, (methodPointerType)&Selectable_set_targetGraphic_m5327/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_set_targetGraphic_m5327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_interactable()
MethodInfo Selectable_get_interactable_m5328_MethodInfo = 
{
	"get_interactable"/* name */
	, (methodPointerType)&Selectable_get_interactable_m5328/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_interactable_m5329_ParameterInfos[] = 
{
	{"value", 0, 134218225, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
MethodInfo Selectable_set_interactable_m5329_MethodInfo = 
{
	"set_interactable"/* name */
	, (methodPointerType)&Selectable_set_interactable_m5329/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Selectable_t1215_Selectable_set_interactable_m5329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerInside()
MethodInfo Selectable_get_isPointerInside_m5330_MethodInfo = 
{
	"get_isPointerInside"/* name */
	, (methodPointerType)&Selectable_get_isPointerInside_m5330/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 230/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_isPointerInside_m5331_ParameterInfos[] = 
{
	{"value", 0, 134218226, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerInside(System.Boolean)
MethodInfo Selectable_set_isPointerInside_m5331_MethodInfo = 
{
	"set_isPointerInside"/* name */
	, (methodPointerType)&Selectable_set_isPointerInside_m5331/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Selectable_t1215_Selectable_set_isPointerInside_m5331_ParameterInfos/* parameters */
	, 231/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerDown()
MethodInfo Selectable_get_isPointerDown_m5332_MethodInfo = 
{
	"get_isPointerDown"/* name */
	, (methodPointerType)&Selectable_get_isPointerDown_m5332/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 232/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_isPointerDown_m5333_ParameterInfos[] = 
{
	{"value", 0, 134218227, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerDown(System.Boolean)
MethodInfo Selectable_set_isPointerDown_m5333_MethodInfo = 
{
	"set_isPointerDown"/* name */
	, (methodPointerType)&Selectable_set_isPointerDown_m5333/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Selectable_t1215_Selectable_set_isPointerDown_m5333_ParameterInfos/* parameters */
	, 233/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_hasSelection()
MethodInfo Selectable_get_hasSelection_m5334_MethodInfo = 
{
	"get_hasSelection"/* name */
	, (methodPointerType)&Selectable_get_hasSelection_m5334/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 234/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_hasSelection_m5335_ParameterInfos[] = 
{
	{"value", 0, 134218228, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_hasSelection(System.Boolean)
MethodInfo Selectable_set_hasSelection_m5335_MethodInfo = 
{
	"set_hasSelection"/* name */
	, (methodPointerType)&Selectable_set_hasSelection_m5335/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Selectable_t1215_Selectable_set_hasSelection_m5335_ParameterInfos/* parameters */
	, 235/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Image_t1248_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Image UnityEngine.UI.Selectable::get_image()
MethodInfo Selectable_get_image_m5336_MethodInfo = 
{
	"get_image"/* name */
	, (methodPointerType)&Selectable_get_image_m5336/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Image_t1248_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Image_t1248_0_0_0;
extern Il2CppType Image_t1248_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_set_image_m5337_ParameterInfos[] = 
{
	{"value", 0, 134218229, 0, &Image_t1248_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_image(UnityEngine.UI.Image)
MethodInfo Selectable_set_image_m5337_MethodInfo = 
{
	"set_image"/* name */
	, (methodPointerType)&Selectable_set_image_m5337/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_set_image_m5337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t749_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Animator UnityEngine.UI.Selectable::get_animator()
MethodInfo Selectable_get_animator_m5338_MethodInfo = 
{
	"get_animator"/* name */
	, (methodPointerType)&Selectable_get_animator_m5338/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t749_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Awake()
MethodInfo Selectable_Awake_m5339_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Selectable_Awake_m5339/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnCanvasGroupChanged()
MethodInfo Selectable_OnCanvasGroupChanged_m5340_MethodInfo = 
{
	"OnCanvasGroupChanged"/* name */
	, (methodPointerType)&Selectable_OnCanvasGroupChanged_m5340/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsInteractable()
MethodInfo Selectable_IsInteractable_m5341_MethodInfo = 
{
	"IsInteractable"/* name */
	, (methodPointerType)&Selectable_IsInteractable_m5341/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDidApplyAnimationProperties()
MethodInfo Selectable_OnDidApplyAnimationProperties_m5342_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&Selectable_OnDidApplyAnimationProperties_m5342/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnEnable()
MethodInfo Selectable_OnEnable_m5343_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Selectable_OnEnable_m5343/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSetProperty()
MethodInfo Selectable_OnSetProperty_m5344_MethodInfo = 
{
	"OnSetProperty"/* name */
	, (methodPointerType)&Selectable_OnSetProperty_m5344/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDisable()
MethodInfo Selectable_OnDisable_m5345_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Selectable_OnDisable_m5345/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SelectionState_t1287_0_0_0;
extern void* RuntimeInvoker_SelectionState_t1287 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::get_currentSelectionState()
MethodInfo Selectable_get_currentSelectionState_m5346_MethodInfo = 
{
	"get_currentSelectionState"/* name */
	, (methodPointerType)&Selectable_get_currentSelectionState_m5346/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &SelectionState_t1287_0_0_0/* return_type */
	, RuntimeInvoker_SelectionState_t1287/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InstantClearState()
MethodInfo Selectable_InstantClearState_m5347_MethodInfo = 
{
	"InstantClearState"/* name */
	, (methodPointerType)&Selectable_InstantClearState_m5347/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SelectionState_t1287_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_DoStateTransition_m5348_ParameterInfos[] = 
{
	{"state", 0, 134218230, 0, &SelectionState_t1287_0_0_0},
	{"instant", 1, 134218231, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoStateTransition(UnityEngine.UI.Selectable/SelectionState,System.Boolean)
MethodInfo Selectable_DoStateTransition_m5348_MethodInfo = 
{
	"DoStateTransition"/* name */
	, (methodPointerType)&Selectable_DoStateTransition_m5348/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_SByte_t236/* invoker_method */
	, Selectable_t1215_Selectable_DoStateTransition_m5348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector3_t758_0_0_0;
extern Il2CppType Vector3_t758_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_FindSelectable_m5349_ParameterInfos[] = 
{
	{"dir", 0, 134218232, 0, &Vector3_t758_0_0_0},
};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t_Vector3_t758 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectable(UnityEngine.Vector3)
MethodInfo Selectable_FindSelectable_m5349_MethodInfo = 
{
	"FindSelectable"/* name */
	, (methodPointerType)&Selectable_FindSelectable_m5349/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t758/* invoker_method */
	, Selectable_t1215_Selectable_FindSelectable_m5349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_GetPointOnRectEdge_m5350_ParameterInfos[] = 
{
	{"rect", 0, 134218233, 0, &RectTransform_t1227_0_0_0},
	{"dir", 1, 134218234, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Vector3_t758_0_0_0;
extern void* RuntimeInvoker_Vector3_t758_Object_t_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityEngine.UI.Selectable::GetPointOnRectEdge(UnityEngine.RectTransform,UnityEngine.Vector2)
MethodInfo Selectable_GetPointOnRectEdge_m5350_MethodInfo = 
{
	"GetPointOnRectEdge"/* name */
	, (methodPointerType)&Selectable_GetPointOnRectEdge_m5350/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t758_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t758_Object_t_Vector2_t739/* invoker_method */
	, Selectable_t1215_Selectable_GetPointOnRectEdge_m5350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AxisEventData_t1188_0_0_0;
extern Il2CppType AxisEventData_t1188_0_0_0;
extern Il2CppType Selectable_t1215_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_Navigate_m5351_ParameterInfos[] = 
{
	{"eventData", 0, 134218235, 0, &AxisEventData_t1188_0_0_0},
	{"sel", 1, 134218236, 0, &Selectable_t1215_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Navigate(UnityEngine.EventSystems.AxisEventData,UnityEngine.UI.Selectable)
MethodInfo Selectable_Navigate_m5351_MethodInfo = 
{
	"Navigate"/* name */
	, (methodPointerType)&Selectable_Navigate_m5351/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_Navigate_m5351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnLeft()
MethodInfo Selectable_FindSelectableOnLeft_m5352_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnLeft_m5352/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnRight()
MethodInfo Selectable_FindSelectableOnRight_m5353_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnRight_m5353/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnUp()
MethodInfo Selectable_FindSelectableOnUp_m5354_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnUp_m5354/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnDown()
MethodInfo Selectable_FindSelectableOnDown_m5355_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnDown_m5355/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AxisEventData_t1188_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_OnMove_m5356_ParameterInfos[] = 
{
	{"eventData", 0, 134218237, 0, &AxisEventData_t1188_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnMove(UnityEngine.EventSystems.AxisEventData)
MethodInfo Selectable_OnMove_m5356_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Selectable_OnMove_m5356/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_OnMove_m5356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Color_t747_0_0_0;
extern Il2CppType Color_t747_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_StartColorTween_m5357_ParameterInfos[] = 
{
	{"targetColor", 0, 134218238, 0, &Color_t747_0_0_0},
	{"instant", 1, 134218239, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Color_t747_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::StartColorTween(UnityEngine.Color,System.Boolean)
MethodInfo Selectable_StartColorTween_m5357_MethodInfo = 
{
	"StartColorTween"/* name */
	, (methodPointerType)&Selectable_StartColorTween_m5357/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Color_t747_SByte_t236/* invoker_method */
	, Selectable_t1215_Selectable_StartColorTween_m5357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sprite_t766_0_0_0;
extern Il2CppType Sprite_t766_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_DoSpriteSwap_m5358_ParameterInfos[] = 
{
	{"newSprite", 0, 134218240, 0, &Sprite_t766_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoSpriteSwap(UnityEngine.Sprite)
MethodInfo Selectable_DoSpriteSwap_m5358_MethodInfo = 
{
	"DoSpriteSwap"/* name */
	, (methodPointerType)&Selectable_DoSpriteSwap_m5358/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_DoSpriteSwap_m5358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_TriggerAnimation_m5359_ParameterInfos[] = 
{
	{"triggername", 0, 134218241, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::TriggerAnimation(System.String)
MethodInfo Selectable_TriggerAnimation_m5359_MethodInfo = 
{
	"TriggerAnimation"/* name */
	, (methodPointerType)&Selectable_TriggerAnimation_m5359/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_TriggerAnimation_m5359_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseEventData_t1153_0_0_0;
extern Il2CppType BaseEventData_t1153_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_IsHighlighted_m5360_ParameterInfos[] = 
{
	{"eventData", 0, 134218242, 0, &BaseEventData_t1153_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsHighlighted(UnityEngine.EventSystems.BaseEventData)
MethodInfo Selectable_IsHighlighted_m5360_MethodInfo = 
{
	"IsHighlighted"/* name */
	, (methodPointerType)&Selectable_IsHighlighted_m5360/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_IsHighlighted_m5360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseEventData_t1153_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_IsPressed_m5361_ParameterInfos[] = 
{
	{"eventData", 0, 134218243, 0, &BaseEventData_t1153_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed(UnityEngine.EventSystems.BaseEventData)
MethodInfo Selectable_IsPressed_m5361_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m5361/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_IsPressed_m5361_ParameterInfos/* parameters */
	, 236/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed()
MethodInfo Selectable_IsPressed_m5362_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m5362/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseEventData_t1153_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_UpdateSelectionState_m5363_ParameterInfos[] = 
{
	{"eventData", 0, 134218244, 0, &BaseEventData_t1153_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::UpdateSelectionState(UnityEngine.EventSystems.BaseEventData)
MethodInfo Selectable_UpdateSelectionState_m5363_MethodInfo = 
{
	"UpdateSelectionState"/* name */
	, (methodPointerType)&Selectable_UpdateSelectionState_m5363/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_UpdateSelectionState_m5363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseEventData_t1153_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_EvaluateAndTransitionToSelectionState_m5364_ParameterInfos[] = 
{
	{"eventData", 0, 134218245, 0, &BaseEventData_t1153_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::EvaluateAndTransitionToSelectionState(UnityEngine.EventSystems.BaseEventData)
MethodInfo Selectable_EvaluateAndTransitionToSelectionState_m5364_MethodInfo = 
{
	"EvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_EvaluateAndTransitionToSelectionState_m5364/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_EvaluateAndTransitionToSelectionState_m5364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_InternalEvaluateAndTransitionToSelectionState_m5365_ParameterInfos[] = 
{
	{"instant", 0, 134218246, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InternalEvaluateAndTransitionToSelectionState(System.Boolean)
MethodInfo Selectable_InternalEvaluateAndTransitionToSelectionState_m5365_MethodInfo = 
{
	"InternalEvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_InternalEvaluateAndTransitionToSelectionState_m5365/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Selectable_t1215_Selectable_InternalEvaluateAndTransitionToSelectionState_m5365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_OnPointerDown_m5366_ParameterInfos[] = 
{
	{"eventData", 0, 134218247, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
MethodInfo Selectable_OnPointerDown_m5366_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Selectable_OnPointerDown_m5366/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_OnPointerDown_m5366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_OnPointerUp_m5367_ParameterInfos[] = 
{
	{"eventData", 0, 134218248, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
MethodInfo Selectable_OnPointerUp_m5367_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&Selectable_OnPointerUp_m5367/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_OnPointerUp_m5367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_OnPointerEnter_m5368_ParameterInfos[] = 
{
	{"eventData", 0, 134218249, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
MethodInfo Selectable_OnPointerEnter_m5368_MethodInfo = 
{
	"OnPointerEnter"/* name */
	, (methodPointerType)&Selectable_OnPointerEnter_m5368/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_OnPointerEnter_m5368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_OnPointerExit_m5369_ParameterInfos[] = 
{
	{"eventData", 0, 134218250, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
MethodInfo Selectable_OnPointerExit_m5369_MethodInfo = 
{
	"OnPointerExit"/* name */
	, (methodPointerType)&Selectable_OnPointerExit_m5369/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_OnPointerExit_m5369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseEventData_t1153_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_OnSelect_m5370_ParameterInfos[] = 
{
	{"eventData", 0, 134218251, 0, &BaseEventData_t1153_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSelect(UnityEngine.EventSystems.BaseEventData)
MethodInfo Selectable_OnSelect_m5370_MethodInfo = 
{
	"OnSelect"/* name */
	, (methodPointerType)&Selectable_OnSelect_m5370/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_OnSelect_m5370_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseEventData_t1153_0_0_0;
static ParameterInfo Selectable_t1215_Selectable_OnDeselect_m5371_ParameterInfos[] = 
{
	{"eventData", 0, 134218252, 0, &BaseEventData_t1153_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDeselect(UnityEngine.EventSystems.BaseEventData)
MethodInfo Selectable_OnDeselect_m5371_MethodInfo = 
{
	"OnDeselect"/* name */
	, (methodPointerType)&Selectable_OnDeselect_m5371/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Selectable_t1215_Selectable_OnDeselect_m5371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Select()
MethodInfo Selectable_Select_m5372_MethodInfo = 
{
	"Select"/* name */
	, (methodPointerType)&Selectable_Select_m5372/* method */
	, &Selectable_t1215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Selectable_t1215_MethodInfos[] =
{
	&Selectable__ctor_m5313_MethodInfo,
	&Selectable__cctor_m5314_MethodInfo,
	&Selectable_get_allSelectables_m5315_MethodInfo,
	&Selectable_get_navigation_m5316_MethodInfo,
	&Selectable_set_navigation_m5317_MethodInfo,
	&Selectable_get_transition_m5318_MethodInfo,
	&Selectable_set_transition_m5319_MethodInfo,
	&Selectable_get_colors_m5320_MethodInfo,
	&Selectable_set_colors_m5321_MethodInfo,
	&Selectable_get_spriteState_m5322_MethodInfo,
	&Selectable_set_spriteState_m5323_MethodInfo,
	&Selectable_get_animationTriggers_m5324_MethodInfo,
	&Selectable_set_animationTriggers_m5325_MethodInfo,
	&Selectable_get_targetGraphic_m5326_MethodInfo,
	&Selectable_set_targetGraphic_m5327_MethodInfo,
	&Selectable_get_interactable_m5328_MethodInfo,
	&Selectable_set_interactable_m5329_MethodInfo,
	&Selectable_get_isPointerInside_m5330_MethodInfo,
	&Selectable_set_isPointerInside_m5331_MethodInfo,
	&Selectable_get_isPointerDown_m5332_MethodInfo,
	&Selectable_set_isPointerDown_m5333_MethodInfo,
	&Selectable_get_hasSelection_m5334_MethodInfo,
	&Selectable_set_hasSelection_m5335_MethodInfo,
	&Selectable_get_image_m5336_MethodInfo,
	&Selectable_set_image_m5337_MethodInfo,
	&Selectable_get_animator_m5338_MethodInfo,
	&Selectable_Awake_m5339_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m5340_MethodInfo,
	&Selectable_IsInteractable_m5341_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m5342_MethodInfo,
	&Selectable_OnEnable_m5343_MethodInfo,
	&Selectable_OnSetProperty_m5344_MethodInfo,
	&Selectable_OnDisable_m5345_MethodInfo,
	&Selectable_get_currentSelectionState_m5346_MethodInfo,
	&Selectable_InstantClearState_m5347_MethodInfo,
	&Selectable_DoStateTransition_m5348_MethodInfo,
	&Selectable_FindSelectable_m5349_MethodInfo,
	&Selectable_GetPointOnRectEdge_m5350_MethodInfo,
	&Selectable_Navigate_m5351_MethodInfo,
	&Selectable_FindSelectableOnLeft_m5352_MethodInfo,
	&Selectable_FindSelectableOnRight_m5353_MethodInfo,
	&Selectable_FindSelectableOnUp_m5354_MethodInfo,
	&Selectable_FindSelectableOnDown_m5355_MethodInfo,
	&Selectable_OnMove_m5356_MethodInfo,
	&Selectable_StartColorTween_m5357_MethodInfo,
	&Selectable_DoSpriteSwap_m5358_MethodInfo,
	&Selectable_TriggerAnimation_m5359_MethodInfo,
	&Selectable_IsHighlighted_m5360_MethodInfo,
	&Selectable_IsPressed_m5361_MethodInfo,
	&Selectable_IsPressed_m5362_MethodInfo,
	&Selectable_UpdateSelectionState_m5363_MethodInfo,
	&Selectable_EvaluateAndTransitionToSelectionState_m5364_MethodInfo,
	&Selectable_InternalEvaluateAndTransitionToSelectionState_m5365_MethodInfo,
	&Selectable_OnPointerDown_m5366_MethodInfo,
	&Selectable_OnPointerUp_m5367_MethodInfo,
	&Selectable_OnPointerEnter_m5368_MethodInfo,
	&Selectable_OnPointerExit_m5369_MethodInfo,
	&Selectable_OnSelect_m5370_MethodInfo,
	&Selectable_OnDeselect_m5371_MethodInfo,
	&Selectable_Select_m5372_MethodInfo,
	NULL
};
extern Il2CppType List_1_t1288_0_0_17;
FieldInfo Selectable_t1215____s_List_2_FieldInfo = 
{
	"s_List"/* name */
	, &List_1_t1288_0_0_17/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215_StaticFields, ___s_List_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Navigation_t1272_0_0_1;
FieldInfo Selectable_t1215____m_Navigation_3_FieldInfo = 
{
	"m_Navigation"/* name */
	, &Navigation_t1272_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_Navigation_3)/* offset */
	, 220/* custom_attributes_cache */

};
extern Il2CppType Transition_t1286_0_0_1;
FieldInfo Selectable_t1215____m_Transition_4_FieldInfo = 
{
	"m_Transition"/* name */
	, &Transition_t1286_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_Transition_4)/* offset */
	, 221/* custom_attributes_cache */

};
extern Il2CppType ColorBlock_t1221_0_0_1;
FieldInfo Selectable_t1215____m_Colors_5_FieldInfo = 
{
	"m_Colors"/* name */
	, &ColorBlock_t1221_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_Colors_5)/* offset */
	, 222/* custom_attributes_cache */

};
extern Il2CppType SpriteState_t1290_0_0_1;
FieldInfo Selectable_t1215____m_SpriteState_6_FieldInfo = 
{
	"m_SpriteState"/* name */
	, &SpriteState_t1290_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_SpriteState_6)/* offset */
	, 223/* custom_attributes_cache */

};
extern Il2CppType AnimationTriggers_t1210_0_0_1;
FieldInfo Selectable_t1215____m_AnimationTriggers_7_FieldInfo = 
{
	"m_AnimationTriggers"/* name */
	, &AnimationTriggers_t1210_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_AnimationTriggers_7)/* offset */
	, 224/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Selectable_t1215____m_Interactable_8_FieldInfo = 
{
	"m_Interactable"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_Interactable_8)/* offset */
	, 225/* custom_attributes_cache */

};
extern Il2CppType Graphic_t1233_0_0_1;
FieldInfo Selectable_t1215____m_TargetGraphic_9_FieldInfo = 
{
	"m_TargetGraphic"/* name */
	, &Graphic_t1233_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_TargetGraphic_9)/* offset */
	, 226/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Selectable_t1215____m_GroupsAllowInteraction_10_FieldInfo = 
{
	"m_GroupsAllowInteraction"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_GroupsAllowInteraction_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SelectionState_t1287_0_0_1;
FieldInfo Selectable_t1215____m_CurrentSelectionState_11_FieldInfo = 
{
	"m_CurrentSelectionState"/* name */
	, &SelectionState_t1287_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_CurrentSelectionState_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t1289_0_0_33;
FieldInfo Selectable_t1215____m_CanvasGroupCache_12_FieldInfo = 
{
	"m_CanvasGroupCache"/* name */
	, &List_1_t1289_0_0_33/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___m_CanvasGroupCache_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Selectable_t1215____U3CisPointerInsideU3Ek__BackingField_13_FieldInfo = 
{
	"<isPointerInside>k__BackingField"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___U3CisPointerInsideU3Ek__BackingField_13)/* offset */
	, 227/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Selectable_t1215____U3CisPointerDownU3Ek__BackingField_14_FieldInfo = 
{
	"<isPointerDown>k__BackingField"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___U3CisPointerDownU3Ek__BackingField_14)/* offset */
	, 228/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Selectable_t1215____U3ChasSelectionU3Ek__BackingField_15_FieldInfo = 
{
	"<hasSelection>k__BackingField"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Selectable_t1215_il2cpp_TypeInfo/* parent */
	, offsetof(Selectable_t1215, ___U3ChasSelectionU3Ek__BackingField_15)/* offset */
	, 229/* custom_attributes_cache */

};
static FieldInfo* Selectable_t1215_FieldInfos[] =
{
	&Selectable_t1215____s_List_2_FieldInfo,
	&Selectable_t1215____m_Navigation_3_FieldInfo,
	&Selectable_t1215____m_Transition_4_FieldInfo,
	&Selectable_t1215____m_Colors_5_FieldInfo,
	&Selectable_t1215____m_SpriteState_6_FieldInfo,
	&Selectable_t1215____m_AnimationTriggers_7_FieldInfo,
	&Selectable_t1215____m_Interactable_8_FieldInfo,
	&Selectable_t1215____m_TargetGraphic_9_FieldInfo,
	&Selectable_t1215____m_GroupsAllowInteraction_10_FieldInfo,
	&Selectable_t1215____m_CurrentSelectionState_11_FieldInfo,
	&Selectable_t1215____m_CanvasGroupCache_12_FieldInfo,
	&Selectable_t1215____U3CisPointerInsideU3Ek__BackingField_13_FieldInfo,
	&Selectable_t1215____U3CisPointerDownU3Ek__BackingField_14_FieldInfo,
	&Selectable_t1215____U3ChasSelectionU3Ek__BackingField_15_FieldInfo,
	NULL
};
extern MethodInfo Selectable_get_allSelectables_m5315_MethodInfo;
static PropertyInfo Selectable_t1215____allSelectables_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "allSelectables"/* name */
	, &Selectable_get_allSelectables_m5315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_navigation_m5316_MethodInfo;
extern MethodInfo Selectable_set_navigation_m5317_MethodInfo;
static PropertyInfo Selectable_t1215____navigation_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "navigation"/* name */
	, &Selectable_get_navigation_m5316_MethodInfo/* get */
	, &Selectable_set_navigation_m5317_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_transition_m5318_MethodInfo;
extern MethodInfo Selectable_set_transition_m5319_MethodInfo;
static PropertyInfo Selectable_t1215____transition_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "transition"/* name */
	, &Selectable_get_transition_m5318_MethodInfo/* get */
	, &Selectable_set_transition_m5319_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_colors_m5320_MethodInfo;
extern MethodInfo Selectable_set_colors_m5321_MethodInfo;
static PropertyInfo Selectable_t1215____colors_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "colors"/* name */
	, &Selectable_get_colors_m5320_MethodInfo/* get */
	, &Selectable_set_colors_m5321_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_spriteState_m5322_MethodInfo;
extern MethodInfo Selectable_set_spriteState_m5323_MethodInfo;
static PropertyInfo Selectable_t1215____spriteState_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "spriteState"/* name */
	, &Selectable_get_spriteState_m5322_MethodInfo/* get */
	, &Selectable_set_spriteState_m5323_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_animationTriggers_m5324_MethodInfo;
extern MethodInfo Selectable_set_animationTriggers_m5325_MethodInfo;
static PropertyInfo Selectable_t1215____animationTriggers_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "animationTriggers"/* name */
	, &Selectable_get_animationTriggers_m5324_MethodInfo/* get */
	, &Selectable_set_animationTriggers_m5325_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_targetGraphic_m5326_MethodInfo;
extern MethodInfo Selectable_set_targetGraphic_m5327_MethodInfo;
static PropertyInfo Selectable_t1215____targetGraphic_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "targetGraphic"/* name */
	, &Selectable_get_targetGraphic_m5326_MethodInfo/* get */
	, &Selectable_set_targetGraphic_m5327_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_interactable_m5328_MethodInfo;
extern MethodInfo Selectable_set_interactable_m5329_MethodInfo;
static PropertyInfo Selectable_t1215____interactable_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "interactable"/* name */
	, &Selectable_get_interactable_m5328_MethodInfo/* get */
	, &Selectable_set_interactable_m5329_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_isPointerInside_m5330_MethodInfo;
extern MethodInfo Selectable_set_isPointerInside_m5331_MethodInfo;
static PropertyInfo Selectable_t1215____isPointerInside_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "isPointerInside"/* name */
	, &Selectable_get_isPointerInside_m5330_MethodInfo/* get */
	, &Selectable_set_isPointerInside_m5331_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_isPointerDown_m5332_MethodInfo;
extern MethodInfo Selectable_set_isPointerDown_m5333_MethodInfo;
static PropertyInfo Selectable_t1215____isPointerDown_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "isPointerDown"/* name */
	, &Selectable_get_isPointerDown_m5332_MethodInfo/* get */
	, &Selectable_set_isPointerDown_m5333_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_hasSelection_m5334_MethodInfo;
extern MethodInfo Selectable_set_hasSelection_m5335_MethodInfo;
static PropertyInfo Selectable_t1215____hasSelection_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "hasSelection"/* name */
	, &Selectable_get_hasSelection_m5334_MethodInfo/* get */
	, &Selectable_set_hasSelection_m5335_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_image_m5336_MethodInfo;
extern MethodInfo Selectable_set_image_m5337_MethodInfo;
static PropertyInfo Selectable_t1215____image_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "image"/* name */
	, &Selectable_get_image_m5336_MethodInfo/* get */
	, &Selectable_set_image_m5337_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_animator_m5338_MethodInfo;
static PropertyInfo Selectable_t1215____animator_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "animator"/* name */
	, &Selectable_get_animator_m5338_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Selectable_get_currentSelectionState_m5346_MethodInfo;
static PropertyInfo Selectable_t1215____currentSelectionState_PropertyInfo = 
{
	&Selectable_t1215_il2cpp_TypeInfo/* parent */
	, "currentSelectionState"/* name */
	, &Selectable_get_currentSelectionState_m5346_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Selectable_t1215_PropertyInfos[] =
{
	&Selectable_t1215____allSelectables_PropertyInfo,
	&Selectable_t1215____navigation_PropertyInfo,
	&Selectable_t1215____transition_PropertyInfo,
	&Selectable_t1215____colors_PropertyInfo,
	&Selectable_t1215____spriteState_PropertyInfo,
	&Selectable_t1215____animationTriggers_PropertyInfo,
	&Selectable_t1215____targetGraphic_PropertyInfo,
	&Selectable_t1215____interactable_PropertyInfo,
	&Selectable_t1215____isPointerInside_PropertyInfo,
	&Selectable_t1215____isPointerDown_PropertyInfo,
	&Selectable_t1215____hasSelection_PropertyInfo,
	&Selectable_t1215____image_PropertyInfo,
	&Selectable_t1215____animator_PropertyInfo,
	&Selectable_t1215____currentSelectionState_PropertyInfo,
	NULL
};
static const Il2CppType* Selectable_t1215_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Transition_t1286_0_0_0,
	&SelectionState_t1287_0_0_0,
};
extern MethodInfo Selectable_Awake_m5339_MethodInfo;
extern MethodInfo Selectable_OnEnable_m5343_MethodInfo;
extern MethodInfo Selectable_OnDisable_m5345_MethodInfo;
extern MethodInfo UIBehaviour_IsActive_m4655_MethodInfo;
extern MethodInfo Selectable_OnDidApplyAnimationProperties_m5342_MethodInfo;
extern MethodInfo Selectable_OnCanvasGroupChanged_m5340_MethodInfo;
extern MethodInfo Selectable_OnPointerEnter_m5368_MethodInfo;
extern MethodInfo Selectable_OnPointerExit_m5369_MethodInfo;
extern MethodInfo Selectable_OnPointerDown_m5366_MethodInfo;
extern MethodInfo Selectable_OnPointerUp_m5367_MethodInfo;
extern MethodInfo Selectable_OnSelect_m5370_MethodInfo;
extern MethodInfo Selectable_OnDeselect_m5371_MethodInfo;
extern MethodInfo Selectable_OnMove_m5356_MethodInfo;
extern MethodInfo Selectable_IsInteractable_m5341_MethodInfo;
extern MethodInfo Selectable_InstantClearState_m5347_MethodInfo;
extern MethodInfo Selectable_DoStateTransition_m5348_MethodInfo;
extern MethodInfo Selectable_FindSelectableOnLeft_m5352_MethodInfo;
extern MethodInfo Selectable_FindSelectableOnRight_m5353_MethodInfo;
extern MethodInfo Selectable_FindSelectableOnUp_m5354_MethodInfo;
extern MethodInfo Selectable_FindSelectableOnDown_m5355_MethodInfo;
extern MethodInfo Selectable_Select_m5372_MethodInfo;
static Il2CppMethodReference Selectable_t1215_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&Selectable_Awake_m5339_MethodInfo,
	&Selectable_OnEnable_m5343_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&Selectable_OnDisable_m5345_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m5342_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m5340_MethodInfo,
	&Selectable_OnPointerEnter_m5368_MethodInfo,
	&Selectable_OnPointerExit_m5369_MethodInfo,
	&Selectable_OnPointerDown_m5366_MethodInfo,
	&Selectable_OnPointerUp_m5367_MethodInfo,
	&Selectable_OnSelect_m5370_MethodInfo,
	&Selectable_OnDeselect_m5371_MethodInfo,
	&Selectable_OnMove_m5356_MethodInfo,
	&Selectable_IsInteractable_m5341_MethodInfo,
	&Selectable_InstantClearState_m5347_MethodInfo,
	&Selectable_DoStateTransition_m5348_MethodInfo,
	&Selectable_FindSelectableOnLeft_m5352_MethodInfo,
	&Selectable_FindSelectableOnRight_m5353_MethodInfo,
	&Selectable_FindSelectableOnUp_m5354_MethodInfo,
	&Selectable_FindSelectableOnDown_m5355_MethodInfo,
	&Selectable_OnMove_m5356_MethodInfo,
	&Selectable_OnPointerDown_m5366_MethodInfo,
	&Selectable_OnPointerUp_m5367_MethodInfo,
	&Selectable_OnPointerEnter_m5368_MethodInfo,
	&Selectable_OnPointerExit_m5369_MethodInfo,
	&Selectable_OnSelect_m5370_MethodInfo,
	&Selectable_OnDeselect_m5371_MethodInfo,
	&Selectable_Select_m5372_MethodInfo,
};
static bool Selectable_t1215_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IPointerEnterHandler_t1340_0_0_0;
extern Il2CppType IPointerExitHandler_t1341_0_0_0;
extern Il2CppType IPointerDownHandler_t1342_0_0_0;
extern Il2CppType IPointerUpHandler_t1343_0_0_0;
extern Il2CppType ISelectHandler_t1352_0_0_0;
extern Il2CppType IDeselectHandler_t1353_0_0_0;
extern Il2CppType IMoveHandler_t1354_0_0_0;
static const Il2CppType* Selectable_t1215_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t1428_0_0_0,
	&IPointerEnterHandler_t1340_0_0_0,
	&IPointerExitHandler_t1341_0_0_0,
	&IPointerDownHandler_t1342_0_0_0,
	&IPointerUpHandler_t1343_0_0_0,
	&ISelectHandler_t1352_0_0_0,
	&IDeselectHandler_t1353_0_0_0,
	&IMoveHandler_t1354_0_0_0,
};
static Il2CppInterfaceOffsetPair Selectable_t1215_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t1428_0_0_0, 15},
	{ &IPointerEnterHandler_t1340_0_0_0, 15},
	{ &IPointerExitHandler_t1341_0_0_0, 16},
	{ &IPointerDownHandler_t1342_0_0_0, 17},
	{ &IPointerUpHandler_t1343_0_0_0, 18},
	{ &ISelectHandler_t1352_0_0_0, 19},
	{ &IDeselectHandler_t1353_0_0_0, 20},
	{ &IMoveHandler_t1354_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Selectable_t1215_1_0_0;
struct Selectable_t1215;
const Il2CppTypeDefinitionMetadata Selectable_t1215_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Selectable_t1215_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Selectable_t1215_InterfacesTypeInfos/* implementedInterfaces */
	, Selectable_t1215_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, Selectable_t1215_VTable/* vtableMethods */
	, Selectable_t1215_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Selectable_t1215_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Selectable"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Selectable_t1215_MethodInfos/* methods */
	, Selectable_t1215_PropertyInfos/* properties */
	, Selectable_t1215_FieldInfos/* fields */
	, NULL/* events */
	, &Selectable_t1215_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 219/* custom_attributes_cache */
	, &Selectable_t1215_0_0_0/* byval_arg */
	, &Selectable_t1215_1_0_0/* this_arg */
	, &Selectable_t1215_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Selectable_t1215)/* instance_size */
	, sizeof (Selectable_t1215)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Selectable_t1215_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 60/* method_count */
	, 14/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 37/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// Metadata Definition UnityEngine.UI.SetPropertyUtility
extern TypeInfo SetPropertyUtility_t1291_il2cpp_TypeInfo;
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
extern Il2CppType Color_t747_1_0_0;
extern Il2CppType Color_t747_1_0_0;
extern Il2CppType Color_t747_0_0_0;
static ParameterInfo SetPropertyUtility_t1291_SetPropertyUtility_SetColor_m5373_ParameterInfos[] = 
{
	{"currentValue", 0, 134218253, 0, &Color_t747_1_0_0},
	{"newValue", 1, 134218254, 0, &Color_t747_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_ColorU26_t1465_Color_t747 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetColor(UnityEngine.Color&,UnityEngine.Color)
MethodInfo SetPropertyUtility_SetColor_m5373_MethodInfo = 
{
	"SetColor"/* name */
	, (methodPointerType)&SetPropertyUtility_SetColor_m5373/* method */
	, &SetPropertyUtility_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_ColorU26_t1465_Color_t747/* invoker_method */
	, SetPropertyUtility_t1291_SetPropertyUtility_SetColor_m5373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SetPropertyUtility_SetStruct_m6247_gp_0_1_0_0;
extern Il2CppType SetPropertyUtility_SetStruct_m6247_gp_0_1_0_0;
extern Il2CppType SetPropertyUtility_SetStruct_m6247_gp_0_0_0_0;
extern Il2CppType SetPropertyUtility_SetStruct_m6247_gp_0_0_0_0;
static ParameterInfo SetPropertyUtility_t1291_SetPropertyUtility_SetStruct_m6247_ParameterInfos[] = 
{
	{"currentValue", 0, 134218255, 0, &SetPropertyUtility_SetStruct_m6247_gp_0_1_0_0},
	{"newValue", 1, 134218256, 0, &SetPropertyUtility_SetStruct_m6247_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppGenericContainer SetPropertyUtility_SetStruct_m6247_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetStruct_m6247_gp_T_0_il2cpp_TypeInfo;
extern Il2CppType ValueType_t329_0_0_0;
static const Il2CppType* SetPropertyUtility_SetStruct_m6247_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t329_0_0_0 /* System.ValueType */, 
 NULL };
Il2CppGenericParamFull SetPropertyUtility_SetStruct_m6247_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &SetPropertyUtility_SetStruct_m6247_Il2CppGenericContainer, 0}, {NULL, "T", 24, 0, SetPropertyUtility_SetStruct_m6247_gp_T_0_il2cpp_TypeInfo_constraints} };
static Il2CppGenericParamFull* SetPropertyUtility_SetStruct_m6247_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetStruct_m6247_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo SetPropertyUtility_SetStruct_m6247_MethodInfo;
Il2CppGenericContainer SetPropertyUtility_SetStruct_m6247_Il2CppGenericContainer = { { NULL, NULL }, NULL, &SetPropertyUtility_SetStruct_m6247_MethodInfo, 1, 1, SetPropertyUtility_SetStruct_m6247_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetStruct_m6247_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &SetPropertyUtility_SetStruct_m6247_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct(T&,T)
MethodInfo SetPropertyUtility_SetStruct_m6247_MethodInfo = 
{
	"SetStruct"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t1291_SetPropertyUtility_SetStruct_m6247_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, SetPropertyUtility_SetStruct_m6247_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetStruct_m6247_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType SetPropertyUtility_SetClass_m6248_gp_0_1_0_0;
extern Il2CppType SetPropertyUtility_SetClass_m6248_gp_0_1_0_0;
extern Il2CppType SetPropertyUtility_SetClass_m6248_gp_0_0_0_0;
extern Il2CppType SetPropertyUtility_SetClass_m6248_gp_0_0_0_0;
static ParameterInfo SetPropertyUtility_t1291_SetPropertyUtility_SetClass_m6248_ParameterInfos[] = 
{
	{"currentValue", 0, 134218257, 0, &SetPropertyUtility_SetClass_m6248_gp_0_1_0_0},
	{"newValue", 1, 134218258, 0, &SetPropertyUtility_SetClass_m6248_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppGenericContainer SetPropertyUtility_SetClass_m6248_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetClass_m6248_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull SetPropertyUtility_SetClass_m6248_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &SetPropertyUtility_SetClass_m6248_Il2CppGenericContainer, 0}, {NULL, "T", 4, 0, NULL} };
static Il2CppGenericParamFull* SetPropertyUtility_SetClass_m6248_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetClass_m6248_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo SetPropertyUtility_SetClass_m6248_MethodInfo;
Il2CppGenericContainer SetPropertyUtility_SetClass_m6248_Il2CppGenericContainer = { { NULL, NULL }, NULL, &SetPropertyUtility_SetClass_m6248_MethodInfo, 1, 1, SetPropertyUtility_SetClass_m6248_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetClass_m6248_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &SetPropertyUtility_SetClass_m6248_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetClass(T&,T)
MethodInfo SetPropertyUtility_SetClass_m6248_MethodInfo = 
{
	"SetClass"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t1291_SetPropertyUtility_SetClass_m6248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, SetPropertyUtility_SetClass_m6248_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetClass_m6248_Il2CppGenericContainer/* genericContainer */

};
static MethodInfo* SetPropertyUtility_t1291_MethodInfos[] =
{
	&SetPropertyUtility_SetColor_m5373_MethodInfo,
	&SetPropertyUtility_SetStruct_m6247_MethodInfo,
	&SetPropertyUtility_SetClass_m6248_MethodInfo,
	NULL
};
extern MethodInfo Object_ToString_m1179_MethodInfo;
static Il2CppMethodReference SetPropertyUtility_t1291_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool SetPropertyUtility_t1291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType SetPropertyUtility_t1291_0_0_0;
extern Il2CppType SetPropertyUtility_t1291_1_0_0;
extern Il2CppType Object_t_0_0_0;
struct SetPropertyUtility_t1291;
const Il2CppTypeDefinitionMetadata SetPropertyUtility_t1291_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetPropertyUtility_t1291_VTable/* vtableMethods */
	, SetPropertyUtility_t1291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SetPropertyUtility_t1291_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetPropertyUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SetPropertyUtility_t1291_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SetPropertyUtility_t1291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetPropertyUtility_t1291_0_0_0/* byval_arg */
	, &SetPropertyUtility_t1291_1_0_0/* this_arg */
	, &SetPropertyUtility_t1291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetPropertyUtility_t1291)/* instance_size */
	, sizeof (SetPropertyUtility_t1291)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
// Metadata Definition UnityEngine.UI.Slider/Direction
extern TypeInfo Direction_t1292_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_DirectionMethodDeclarations.h"
static MethodInfo* Direction_t1292_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Direction_t1292____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Direction_t1292_il2cpp_TypeInfo/* parent */
	, offsetof(Direction_t1292, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Direction_t1292_0_0_32854;
FieldInfo Direction_t1292____LeftToRight_2_FieldInfo = 
{
	"LeftToRight"/* name */
	, &Direction_t1292_0_0_32854/* type */
	, &Direction_t1292_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Direction_t1292_0_0_32854;
FieldInfo Direction_t1292____RightToLeft_3_FieldInfo = 
{
	"RightToLeft"/* name */
	, &Direction_t1292_0_0_32854/* type */
	, &Direction_t1292_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Direction_t1292_0_0_32854;
FieldInfo Direction_t1292____BottomToTop_4_FieldInfo = 
{
	"BottomToTop"/* name */
	, &Direction_t1292_0_0_32854/* type */
	, &Direction_t1292_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Direction_t1292_0_0_32854;
FieldInfo Direction_t1292____TopToBottom_5_FieldInfo = 
{
	"TopToBottom"/* name */
	, &Direction_t1292_0_0_32854/* type */
	, &Direction_t1292_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Direction_t1292_FieldInfos[] =
{
	&Direction_t1292____value___1_FieldInfo,
	&Direction_t1292____LeftToRight_2_FieldInfo,
	&Direction_t1292____RightToLeft_3_FieldInfo,
	&Direction_t1292____BottomToTop_4_FieldInfo,
	&Direction_t1292____TopToBottom_5_FieldInfo,
	NULL
};
static const int32_t Direction_t1292____LeftToRight_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Direction_t1292____LeftToRight_2_DefaultValue = 
{
	&Direction_t1292____LeftToRight_2_FieldInfo/* field */
	, { (char*)&Direction_t1292____LeftToRight_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Direction_t1292____RightToLeft_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Direction_t1292____RightToLeft_3_DefaultValue = 
{
	&Direction_t1292____RightToLeft_3_FieldInfo/* field */
	, { (char*)&Direction_t1292____RightToLeft_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Direction_t1292____BottomToTop_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry Direction_t1292____BottomToTop_4_DefaultValue = 
{
	&Direction_t1292____BottomToTop_4_FieldInfo/* field */
	, { (char*)&Direction_t1292____BottomToTop_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Direction_t1292____TopToBottom_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry Direction_t1292____TopToBottom_5_DefaultValue = 
{
	&Direction_t1292____TopToBottom_5_FieldInfo/* field */
	, { (char*)&Direction_t1292____TopToBottom_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Direction_t1292_FieldDefaultValues[] = 
{
	&Direction_t1292____LeftToRight_2_DefaultValue,
	&Direction_t1292____RightToLeft_3_DefaultValue,
	&Direction_t1292____BottomToTop_4_DefaultValue,
	&Direction_t1292____TopToBottom_5_DefaultValue,
	NULL
};
static Il2CppMethodReference Direction_t1292_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Direction_t1292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Direction_t1292_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Direction_t1292_0_0_0;
extern Il2CppType Direction_t1292_1_0_0;
extern TypeInfo Slider_t1295_il2cpp_TypeInfo;
extern Il2CppType Slider_t1295_0_0_0;
const Il2CppTypeDefinitionMetadata Direction_t1292_DefinitionMetadata = 
{
	&Slider_t1295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t1292_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Direction_t1292_VTable/* vtableMethods */
	, Direction_t1292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Direction_t1292_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, Direction_t1292_MethodInfos/* methods */
	, NULL/* properties */
	, Direction_t1292_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t1292_0_0_0/* byval_arg */
	, &Direction_t1292_1_0_0/* this_arg */
	, &Direction_t1292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Direction_t1292_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t1292)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t1292)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent.h"
// Metadata Definition UnityEngine.UI.Slider/SliderEvent
extern TypeInfo SliderEvent_t1293_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEventMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
MethodInfo SliderEvent__ctor_m5374_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderEvent__ctor_m5374/* method */
	, &SliderEvent_t1293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SliderEvent_t1293_MethodInfos[] =
{
	&SliderEvent__ctor_m5374_MethodInfo,
	NULL
};
extern Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m6312_GenericMethod;
extern Il2CppGenericMethod UnityEvent_1_GetDelegate_m6313_GenericMethod;
static Il2CppMethodReference SliderEvent_t1293_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m6312_GenericMethod,
	&UnityEvent_1_GetDelegate_m6313_GenericMethod,
};
static bool SliderEvent_t1293_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair SliderEvent_t1293_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType SliderEvent_t1293_0_0_0;
extern Il2CppType SliderEvent_t1293_1_0_0;
extern Il2CppType UnityEvent_1_t1276_0_0_0;
struct SliderEvent_t1293;
const Il2CppTypeDefinitionMetadata SliderEvent_t1293_DefinitionMetadata = 
{
	&Slider_t1295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SliderEvent_t1293_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t1276_0_0_0/* parent */
	, SliderEvent_t1293_VTable/* vtableMethods */
	, SliderEvent_t1293_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SliderEvent_t1293_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderEvent"/* name */
	, ""/* namespaze */
	, SliderEvent_t1293_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SliderEvent_t1293_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderEvent_t1293_0_0_0/* byval_arg */
	, &SliderEvent_t1293_1_0_0/* this_arg */
	, &SliderEvent_t1293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderEvent_t1293)/* instance_size */
	, sizeof (SliderEvent_t1293)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
// Metadata Definition UnityEngine.UI.Slider/Axis
extern TypeInfo Axis_t1294_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_AxisMethodDeclarations.h"
static MethodInfo* Axis_t1294_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Axis_t1294____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Axis_t1294_il2cpp_TypeInfo/* parent */
	, offsetof(Axis_t1294, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Axis_t1294_0_0_32854;
FieldInfo Axis_t1294____Horizontal_2_FieldInfo = 
{
	"Horizontal"/* name */
	, &Axis_t1294_0_0_32854/* type */
	, &Axis_t1294_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Axis_t1294_0_0_32854;
FieldInfo Axis_t1294____Vertical_3_FieldInfo = 
{
	"Vertical"/* name */
	, &Axis_t1294_0_0_32854/* type */
	, &Axis_t1294_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Axis_t1294_FieldInfos[] =
{
	&Axis_t1294____value___1_FieldInfo,
	&Axis_t1294____Horizontal_2_FieldInfo,
	&Axis_t1294____Vertical_3_FieldInfo,
	NULL
};
static const int32_t Axis_t1294____Horizontal_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Axis_t1294____Horizontal_2_DefaultValue = 
{
	&Axis_t1294____Horizontal_2_FieldInfo/* field */
	, { (char*)&Axis_t1294____Horizontal_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Axis_t1294____Vertical_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Axis_t1294____Vertical_3_DefaultValue = 
{
	&Axis_t1294____Vertical_3_FieldInfo/* field */
	, { (char*)&Axis_t1294____Vertical_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Axis_t1294_FieldDefaultValues[] = 
{
	&Axis_t1294____Horizontal_2_DefaultValue,
	&Axis_t1294____Vertical_3_DefaultValue,
	NULL
};
static Il2CppMethodReference Axis_t1294_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Axis_t1294_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t1294_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Axis_t1294_0_0_0;
extern Il2CppType Axis_t1294_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t1294_DefinitionMetadata = 
{
	&Slider_t1295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t1294_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Axis_t1294_VTable/* vtableMethods */
	, Axis_t1294_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Axis_t1294_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t1294_MethodInfos/* methods */
	, NULL/* properties */
	, Axis_t1294_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t1294_0_0_0/* byval_arg */
	, &Axis_t1294_1_0_0/* this_arg */
	, &Axis_t1294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Axis_t1294_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t1294)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t1294)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// Metadata Definition UnityEngine.UI.Slider
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::.ctor()
MethodInfo Slider__ctor_m5375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Slider__ctor_m5375/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_fillRect()
MethodInfo Slider_get_fillRect_m5376_MethodInfo = 
{
	"get_fillRect"/* name */
	, (methodPointerType)&Slider_get_fillRect_m5376/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_fillRect_m5377_ParameterInfos[] = 
{
	{"value", 0, 134218259, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_fillRect(UnityEngine.RectTransform)
MethodInfo Slider_set_fillRect_m5377_MethodInfo = 
{
	"set_fillRect"/* name */
	, (methodPointerType)&Slider_set_fillRect_m5377/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Slider_t1295_Slider_set_fillRect_m5377_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_handleRect()
MethodInfo Slider_get_handleRect_m5378_MethodInfo = 
{
	"get_handleRect"/* name */
	, (methodPointerType)&Slider_get_handleRect_m5378/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_handleRect_m5379_ParameterInfos[] = 
{
	{"value", 0, 134218260, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_handleRect(UnityEngine.RectTransform)
MethodInfo Slider_set_handleRect_m5379_MethodInfo = 
{
	"set_handleRect"/* name */
	, (methodPointerType)&Slider_set_handleRect_m5379/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Slider_t1295_Slider_set_handleRect_m5379_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Direction_t1292_0_0_0;
extern void* RuntimeInvoker_Direction_t1292 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::get_direction()
MethodInfo Slider_get_direction_m5380_MethodInfo = 
{
	"get_direction"/* name */
	, (methodPointerType)&Slider_get_direction_m5380/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t1292_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t1292/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Direction_t1292_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_direction_m5381_ParameterInfos[] = 
{
	{"value", 0, 134218261, 0, &Direction_t1292_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_direction(UnityEngine.UI.Slider/Direction)
MethodInfo Slider_set_direction_m5381_MethodInfo = 
{
	"set_direction"/* name */
	, (methodPointerType)&Slider_set_direction_m5381/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Slider_t1295_Slider_set_direction_m5381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_minValue()
MethodInfo Slider_get_minValue_m5382_MethodInfo = 
{
	"get_minValue"/* name */
	, (methodPointerType)&Slider_get_minValue_m5382/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_minValue_m5383_ParameterInfos[] = 
{
	{"value", 0, 134218262, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_minValue(System.Single)
MethodInfo Slider_set_minValue_m5383_MethodInfo = 
{
	"set_minValue"/* name */
	, (methodPointerType)&Slider_set_minValue_m5383/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, Slider_t1295_Slider_set_minValue_m5383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_maxValue()
MethodInfo Slider_get_maxValue_m5384_MethodInfo = 
{
	"get_maxValue"/* name */
	, (methodPointerType)&Slider_get_maxValue_m5384/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_maxValue_m5385_ParameterInfos[] = 
{
	{"value", 0, 134218263, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
MethodInfo Slider_set_maxValue_m5385_MethodInfo = 
{
	"set_maxValue"/* name */
	, (methodPointerType)&Slider_set_maxValue_m5385/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, Slider_t1295_Slider_set_maxValue_m5385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_wholeNumbers()
MethodInfo Slider_get_wholeNumbers_m5386_MethodInfo = 
{
	"get_wholeNumbers"/* name */
	, (methodPointerType)&Slider_get_wholeNumbers_m5386/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_wholeNumbers_m5387_ParameterInfos[] = 
{
	{"value", 0, 134218264, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_wholeNumbers(System.Boolean)
MethodInfo Slider_set_wholeNumbers_m5387_MethodInfo = 
{
	"set_wholeNumbers"/* name */
	, (methodPointerType)&Slider_set_wholeNumbers_m5387/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Slider_t1295_Slider_set_wholeNumbers_m5387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_value()
MethodInfo Slider_get_value_m5388_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Slider_get_value_m5388/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_value_m5389_ParameterInfos[] = 
{
	{"value", 0, 134218265, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_value(System.Single)
MethodInfo Slider_set_value_m5389_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Slider_set_value_m5389/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, Slider_t1295_Slider_set_value_m5389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_normalizedValue()
MethodInfo Slider_get_normalizedValue_m5390_MethodInfo = 
{
	"get_normalizedValue"/* name */
	, (methodPointerType)&Slider_get_normalizedValue_m5390/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_normalizedValue_m5391_ParameterInfos[] = 
{
	{"value", 0, 134218266, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_normalizedValue(System.Single)
MethodInfo Slider_set_normalizedValue_m5391_MethodInfo = 
{
	"set_normalizedValue"/* name */
	, (methodPointerType)&Slider_set_normalizedValue_m5391/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, Slider_t1295_Slider_set_normalizedValue_m5391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SliderEvent_t1293_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::get_onValueChanged()
MethodInfo Slider_get_onValueChanged_m5392_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&Slider_get_onValueChanged_m5392/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &SliderEvent_t1293_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SliderEvent_t1293_0_0_0;
static ParameterInfo Slider_t1295_Slider_set_onValueChanged_m5393_ParameterInfos[] = 
{
	{"value", 0, 134218267, 0, &SliderEvent_t1293_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_onValueChanged(UnityEngine.UI.Slider/SliderEvent)
MethodInfo Slider_set_onValueChanged_m5393_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&Slider_set_onValueChanged_m5393/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Slider_t1295_Slider_set_onValueChanged_m5393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_stepSize()
MethodInfo Slider_get_stepSize_m5394_MethodInfo = 
{
	"get_stepSize"/* name */
	, (methodPointerType)&Slider_get_stepSize_m5394/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CanvasUpdate_t1216_0_0_0;
static ParameterInfo Slider_t1295_Slider_Rebuild_m5395_ParameterInfos[] = 
{
	{"executing", 0, 134218268, 0, &CanvasUpdate_t1216_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Rebuild(UnityEngine.UI.CanvasUpdate)
MethodInfo Slider_Rebuild_m5395_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Slider_Rebuild_m5395/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Slider_t1295_Slider_Rebuild_m5395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 42/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnEnable()
MethodInfo Slider_OnEnable_m5396_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Slider_OnEnable_m5396/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDisable()
MethodInfo Slider_OnDisable_m5397_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Slider_OnDisable_m5397/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateCachedReferences()
MethodInfo Slider_UpdateCachedReferences_m5398_MethodInfo = 
{
	"UpdateCachedReferences"/* name */
	, (methodPointerType)&Slider_UpdateCachedReferences_m5398/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo Slider_t1295_Slider_Set_m5399_ParameterInfos[] = 
{
	{"input", 0, 134218269, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single)
MethodInfo Slider_Set_m5399_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m5399/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, Slider_t1295_Slider_Set_m5399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Slider_t1295_Slider_Set_m5400_ParameterInfos[] = 
{
	{"input", 0, 134218270, 0, &Single_t202_0_0_0},
	{"sendCallback", 1, 134218271, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single,System.Boolean)
MethodInfo Slider_Set_m5400_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m5400/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202_SByte_t236/* invoker_method */
	, Slider_t1295_Slider_Set_m5400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnRectTransformDimensionsChange()
MethodInfo Slider_OnRectTransformDimensionsChange_m5401_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&Slider_OnRectTransformDimensionsChange_m5401/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Axis_t1294_0_0_0;
extern void* RuntimeInvoker_Axis_t1294 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Axis UnityEngine.UI.Slider::get_axis()
MethodInfo Slider_get_axis_m5402_MethodInfo = 
{
	"get_axis"/* name */
	, (methodPointerType)&Slider_get_axis_m5402/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t1294_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t1294/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_reverseValue()
MethodInfo Slider_get_reverseValue_m5403_MethodInfo = 
{
	"get_reverseValue"/* name */
	, (methodPointerType)&Slider_get_reverseValue_m5403/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateVisuals()
MethodInfo Slider_UpdateVisuals_m5404_MethodInfo = 
{
	"UpdateVisuals"/* name */
	, (methodPointerType)&Slider_UpdateVisuals_m5404/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
extern Il2CppType Camera_t978_0_0_0;
extern Il2CppType Camera_t978_0_0_0;
static ParameterInfo Slider_t1295_Slider_UpdateDrag_m5405_ParameterInfos[] = 
{
	{"eventData", 0, 134218272, 0, &PointerEventData_t1191_0_0_0},
	{"cam", 1, 134218273, 0, &Camera_t978_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
MethodInfo Slider_UpdateDrag_m5405_MethodInfo = 
{
	"UpdateDrag"/* name */
	, (methodPointerType)&Slider_UpdateDrag_m5405/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, Slider_t1295_Slider_UpdateDrag_m5405_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Slider_t1295_Slider_MayDrag_m5406_ParameterInfos[] = 
{
	{"eventData", 0, 134218274, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::MayDrag(UnityEngine.EventSystems.PointerEventData)
MethodInfo Slider_MayDrag_m5406_MethodInfo = 
{
	"MayDrag"/* name */
	, (methodPointerType)&Slider_MayDrag_m5406/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Slider_t1295_Slider_MayDrag_m5406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Slider_t1295_Slider_OnPointerDown_m5407_ParameterInfos[] = 
{
	{"eventData", 0, 134218275, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
MethodInfo Slider_OnPointerDown_m5407_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Slider_OnPointerDown_m5407/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Slider_t1295_Slider_OnPointerDown_m5407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Slider_t1295_Slider_OnDrag_m5408_ParameterInfos[] = 
{
	{"eventData", 0, 134218276, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDrag(UnityEngine.EventSystems.PointerEventData)
MethodInfo Slider_OnDrag_m5408_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&Slider_OnDrag_m5408/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Slider_t1295_Slider_OnDrag_m5408_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AxisEventData_t1188_0_0_0;
static ParameterInfo Slider_t1295_Slider_OnMove_m5409_ParameterInfos[] = 
{
	{"eventData", 0, 134218277, 0, &AxisEventData_t1188_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnMove(UnityEngine.EventSystems.AxisEventData)
MethodInfo Slider_OnMove_m5409_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Slider_OnMove_m5409/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Slider_t1295_Slider_OnMove_m5409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnLeft()
MethodInfo Slider_FindSelectableOnLeft_m5410_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Slider_FindSelectableOnLeft_m5410/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnRight()
MethodInfo Slider_FindSelectableOnRight_m5411_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Slider_FindSelectableOnRight_m5411/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnUp()
MethodInfo Slider_FindSelectableOnUp_m5412_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Slider_FindSelectableOnUp_m5412/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Selectable_t1215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnDown()
MethodInfo Slider_FindSelectableOnDown_m5413_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Slider_FindSelectableOnDown_m5413/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t1215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Slider_t1295_Slider_OnInitializePotentialDrag_m5414_ParameterInfos[] = 
{
	{"eventData", 0, 134218278, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
MethodInfo Slider_OnInitializePotentialDrag_m5414_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&Slider_OnInitializePotentialDrag_m5414/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Slider_t1295_Slider_OnInitializePotentialDrag_m5414_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Direction_t1292_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Slider_t1295_Slider_SetDirection_m5415_ParameterInfos[] = 
{
	{"direction", 0, 134218279, 0, &Direction_t1292_0_0_0},
	{"includeRectLayouts", 1, 134218280, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::SetDirection(UnityEngine.UI.Slider/Direction,System.Boolean)
MethodInfo Slider_SetDirection_m5415_MethodInfo = 
{
	"SetDirection"/* name */
	, (methodPointerType)&Slider_SetDirection_m5415/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_SByte_t236/* invoker_method */
	, Slider_t1295_Slider_SetDirection_m5415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.IsDestroyed()
MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m5416_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m5416/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t809_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.get_transform()
MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m5417_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_get_transform_m5417/* method */
	, &Slider_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t809_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Slider_t1295_MethodInfos[] =
{
	&Slider__ctor_m5375_MethodInfo,
	&Slider_get_fillRect_m5376_MethodInfo,
	&Slider_set_fillRect_m5377_MethodInfo,
	&Slider_get_handleRect_m5378_MethodInfo,
	&Slider_set_handleRect_m5379_MethodInfo,
	&Slider_get_direction_m5380_MethodInfo,
	&Slider_set_direction_m5381_MethodInfo,
	&Slider_get_minValue_m5382_MethodInfo,
	&Slider_set_minValue_m5383_MethodInfo,
	&Slider_get_maxValue_m5384_MethodInfo,
	&Slider_set_maxValue_m5385_MethodInfo,
	&Slider_get_wholeNumbers_m5386_MethodInfo,
	&Slider_set_wholeNumbers_m5387_MethodInfo,
	&Slider_get_value_m5388_MethodInfo,
	&Slider_set_value_m5389_MethodInfo,
	&Slider_get_normalizedValue_m5390_MethodInfo,
	&Slider_set_normalizedValue_m5391_MethodInfo,
	&Slider_get_onValueChanged_m5392_MethodInfo,
	&Slider_set_onValueChanged_m5393_MethodInfo,
	&Slider_get_stepSize_m5394_MethodInfo,
	&Slider_Rebuild_m5395_MethodInfo,
	&Slider_OnEnable_m5396_MethodInfo,
	&Slider_OnDisable_m5397_MethodInfo,
	&Slider_UpdateCachedReferences_m5398_MethodInfo,
	&Slider_Set_m5399_MethodInfo,
	&Slider_Set_m5400_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m5401_MethodInfo,
	&Slider_get_axis_m5402_MethodInfo,
	&Slider_get_reverseValue_m5403_MethodInfo,
	&Slider_UpdateVisuals_m5404_MethodInfo,
	&Slider_UpdateDrag_m5405_MethodInfo,
	&Slider_MayDrag_m5406_MethodInfo,
	&Slider_OnPointerDown_m5407_MethodInfo,
	&Slider_OnDrag_m5408_MethodInfo,
	&Slider_OnMove_m5409_MethodInfo,
	&Slider_FindSelectableOnLeft_m5410_MethodInfo,
	&Slider_FindSelectableOnRight_m5411_MethodInfo,
	&Slider_FindSelectableOnUp_m5412_MethodInfo,
	&Slider_FindSelectableOnDown_m5413_MethodInfo,
	&Slider_OnInitializePotentialDrag_m5414_MethodInfo,
	&Slider_SetDirection_m5415_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m5416_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m5417_MethodInfo,
	NULL
};
extern Il2CppType RectTransform_t1227_0_0_1;
FieldInfo Slider_t1295____m_FillRect_16_FieldInfo = 
{
	"m_FillRect"/* name */
	, &RectTransform_t1227_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_FillRect_16)/* offset */
	, 238/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_1;
FieldInfo Slider_t1295____m_HandleRect_17_FieldInfo = 
{
	"m_HandleRect"/* name */
	, &RectTransform_t1227_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_HandleRect_17)/* offset */
	, 239/* custom_attributes_cache */

};
extern Il2CppType Direction_t1292_0_0_1;
FieldInfo Slider_t1295____m_Direction_18_FieldInfo = 
{
	"m_Direction"/* name */
	, &Direction_t1292_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_Direction_18)/* offset */
	, 240/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo Slider_t1295____m_MinValue_19_FieldInfo = 
{
	"m_MinValue"/* name */
	, &Single_t202_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_MinValue_19)/* offset */
	, 241/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo Slider_t1295____m_MaxValue_20_FieldInfo = 
{
	"m_MaxValue"/* name */
	, &Single_t202_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_MaxValue_20)/* offset */
	, 242/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Slider_t1295____m_WholeNumbers_21_FieldInfo = 
{
	"m_WholeNumbers"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_WholeNumbers_21)/* offset */
	, 243/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo Slider_t1295____m_Value_22_FieldInfo = 
{
	"m_Value"/* name */
	, &Single_t202_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_Value_22)/* offset */
	, 244/* custom_attributes_cache */

};
extern Il2CppType SliderEvent_t1293_0_0_1;
FieldInfo Slider_t1295____m_OnValueChanged_23_FieldInfo = 
{
	"m_OnValueChanged"/* name */
	, &SliderEvent_t1293_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_OnValueChanged_23)/* offset */
	, 245/* custom_attributes_cache */

};
extern Il2CppType Image_t1248_0_0_1;
FieldInfo Slider_t1295____m_FillImage_24_FieldInfo = 
{
	"m_FillImage"/* name */
	, &Image_t1248_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_FillImage_24)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Transform_t809_0_0_1;
FieldInfo Slider_t1295____m_FillTransform_25_FieldInfo = 
{
	"m_FillTransform"/* name */
	, &Transform_t809_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_FillTransform_25)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_1;
FieldInfo Slider_t1295____m_FillContainerRect_26_FieldInfo = 
{
	"m_FillContainerRect"/* name */
	, &RectTransform_t1227_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_FillContainerRect_26)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Transform_t809_0_0_1;
FieldInfo Slider_t1295____m_HandleTransform_27_FieldInfo = 
{
	"m_HandleTransform"/* name */
	, &Transform_t809_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_HandleTransform_27)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_1;
FieldInfo Slider_t1295____m_HandleContainerRect_28_FieldInfo = 
{
	"m_HandleContainerRect"/* name */
	, &RectTransform_t1227_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_HandleContainerRect_28)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo Slider_t1295____m_Offset_29_FieldInfo = 
{
	"m_Offset"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_Offset_29)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DrivenRectTransformTracker_t1280_0_0_1;
FieldInfo Slider_t1295____m_Tracker_30_FieldInfo = 
{
	"m_Tracker"/* name */
	, &DrivenRectTransformTracker_t1280_0_0_1/* type */
	, &Slider_t1295_il2cpp_TypeInfo/* parent */
	, offsetof(Slider_t1295, ___m_Tracker_30)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Slider_t1295_FieldInfos[] =
{
	&Slider_t1295____m_FillRect_16_FieldInfo,
	&Slider_t1295____m_HandleRect_17_FieldInfo,
	&Slider_t1295____m_Direction_18_FieldInfo,
	&Slider_t1295____m_MinValue_19_FieldInfo,
	&Slider_t1295____m_MaxValue_20_FieldInfo,
	&Slider_t1295____m_WholeNumbers_21_FieldInfo,
	&Slider_t1295____m_Value_22_FieldInfo,
	&Slider_t1295____m_OnValueChanged_23_FieldInfo,
	&Slider_t1295____m_FillImage_24_FieldInfo,
	&Slider_t1295____m_FillTransform_25_FieldInfo,
	&Slider_t1295____m_FillContainerRect_26_FieldInfo,
	&Slider_t1295____m_HandleTransform_27_FieldInfo,
	&Slider_t1295____m_HandleContainerRect_28_FieldInfo,
	&Slider_t1295____m_Offset_29_FieldInfo,
	&Slider_t1295____m_Tracker_30_FieldInfo,
	NULL
};
extern MethodInfo Slider_get_fillRect_m5376_MethodInfo;
extern MethodInfo Slider_set_fillRect_m5377_MethodInfo;
static PropertyInfo Slider_t1295____fillRect_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "fillRect"/* name */
	, &Slider_get_fillRect_m5376_MethodInfo/* get */
	, &Slider_set_fillRect_m5377_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_handleRect_m5378_MethodInfo;
extern MethodInfo Slider_set_handleRect_m5379_MethodInfo;
static PropertyInfo Slider_t1295____handleRect_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "handleRect"/* name */
	, &Slider_get_handleRect_m5378_MethodInfo/* get */
	, &Slider_set_handleRect_m5379_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_direction_m5380_MethodInfo;
extern MethodInfo Slider_set_direction_m5381_MethodInfo;
static PropertyInfo Slider_t1295____direction_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "direction"/* name */
	, &Slider_get_direction_m5380_MethodInfo/* get */
	, &Slider_set_direction_m5381_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_minValue_m5382_MethodInfo;
extern MethodInfo Slider_set_minValue_m5383_MethodInfo;
static PropertyInfo Slider_t1295____minValue_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "minValue"/* name */
	, &Slider_get_minValue_m5382_MethodInfo/* get */
	, &Slider_set_minValue_m5383_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_maxValue_m5384_MethodInfo;
extern MethodInfo Slider_set_maxValue_m5385_MethodInfo;
static PropertyInfo Slider_t1295____maxValue_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "maxValue"/* name */
	, &Slider_get_maxValue_m5384_MethodInfo/* get */
	, &Slider_set_maxValue_m5385_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_wholeNumbers_m5386_MethodInfo;
extern MethodInfo Slider_set_wholeNumbers_m5387_MethodInfo;
static PropertyInfo Slider_t1295____wholeNumbers_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "wholeNumbers"/* name */
	, &Slider_get_wholeNumbers_m5386_MethodInfo/* get */
	, &Slider_set_wholeNumbers_m5387_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_value_m5388_MethodInfo;
extern MethodInfo Slider_set_value_m5389_MethodInfo;
static PropertyInfo Slider_t1295____value_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Slider_get_value_m5388_MethodInfo/* get */
	, &Slider_set_value_m5389_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_normalizedValue_m5390_MethodInfo;
extern MethodInfo Slider_set_normalizedValue_m5391_MethodInfo;
static PropertyInfo Slider_t1295____normalizedValue_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "normalizedValue"/* name */
	, &Slider_get_normalizedValue_m5390_MethodInfo/* get */
	, &Slider_set_normalizedValue_m5391_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_onValueChanged_m5392_MethodInfo;
extern MethodInfo Slider_set_onValueChanged_m5393_MethodInfo;
static PropertyInfo Slider_t1295____onValueChanged_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &Slider_get_onValueChanged_m5392_MethodInfo/* get */
	, &Slider_set_onValueChanged_m5393_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_stepSize_m5394_MethodInfo;
static PropertyInfo Slider_t1295____stepSize_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "stepSize"/* name */
	, &Slider_get_stepSize_m5394_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_axis_m5402_MethodInfo;
static PropertyInfo Slider_t1295____axis_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "axis"/* name */
	, &Slider_get_axis_m5402_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Slider_get_reverseValue_m5403_MethodInfo;
static PropertyInfo Slider_t1295____reverseValue_PropertyInfo = 
{
	&Slider_t1295_il2cpp_TypeInfo/* parent */
	, "reverseValue"/* name */
	, &Slider_get_reverseValue_m5403_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Slider_t1295_PropertyInfos[] =
{
	&Slider_t1295____fillRect_PropertyInfo,
	&Slider_t1295____handleRect_PropertyInfo,
	&Slider_t1295____direction_PropertyInfo,
	&Slider_t1295____minValue_PropertyInfo,
	&Slider_t1295____maxValue_PropertyInfo,
	&Slider_t1295____wholeNumbers_PropertyInfo,
	&Slider_t1295____value_PropertyInfo,
	&Slider_t1295____normalizedValue_PropertyInfo,
	&Slider_t1295____onValueChanged_PropertyInfo,
	&Slider_t1295____stepSize_PropertyInfo,
	&Slider_t1295____axis_PropertyInfo,
	&Slider_t1295____reverseValue_PropertyInfo,
	NULL
};
static const Il2CppType* Slider_t1295_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Direction_t1292_0_0_0,
	&SliderEvent_t1293_0_0_0,
	&Axis_t1294_0_0_0,
};
extern MethodInfo Slider_OnEnable_m5396_MethodInfo;
extern MethodInfo Slider_OnDisable_m5397_MethodInfo;
extern MethodInfo Slider_OnRectTransformDimensionsChange_m5401_MethodInfo;
extern MethodInfo Slider_OnPointerDown_m5407_MethodInfo;
extern MethodInfo Slider_OnMove_m5409_MethodInfo;
extern MethodInfo Slider_FindSelectableOnLeft_m5410_MethodInfo;
extern MethodInfo Slider_FindSelectableOnRight_m5411_MethodInfo;
extern MethodInfo Slider_FindSelectableOnUp_m5412_MethodInfo;
extern MethodInfo Slider_FindSelectableOnDown_m5413_MethodInfo;
extern MethodInfo Slider_OnInitializePotentialDrag_m5414_MethodInfo;
extern MethodInfo Slider_OnDrag_m5408_MethodInfo;
extern MethodInfo Slider_Rebuild_m5395_MethodInfo;
extern MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m5417_MethodInfo;
extern MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m5416_MethodInfo;
static Il2CppMethodReference Slider_t1295_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&Selectable_Awake_m5339_MethodInfo,
	&Slider_OnEnable_m5396_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&Slider_OnDisable_m5397_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m5401_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m5342_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m5340_MethodInfo,
	&Selectable_OnPointerEnter_m5368_MethodInfo,
	&Selectable_OnPointerExit_m5369_MethodInfo,
	&Slider_OnPointerDown_m5407_MethodInfo,
	&Selectable_OnPointerUp_m5367_MethodInfo,
	&Selectable_OnSelect_m5370_MethodInfo,
	&Selectable_OnDeselect_m5371_MethodInfo,
	&Slider_OnMove_m5409_MethodInfo,
	&Selectable_IsInteractable_m5341_MethodInfo,
	&Selectable_InstantClearState_m5347_MethodInfo,
	&Selectable_DoStateTransition_m5348_MethodInfo,
	&Slider_FindSelectableOnLeft_m5410_MethodInfo,
	&Slider_FindSelectableOnRight_m5411_MethodInfo,
	&Slider_FindSelectableOnUp_m5412_MethodInfo,
	&Slider_FindSelectableOnDown_m5413_MethodInfo,
	&Slider_OnMove_m5409_MethodInfo,
	&Slider_OnPointerDown_m5407_MethodInfo,
	&Selectable_OnPointerUp_m5367_MethodInfo,
	&Selectable_OnPointerEnter_m5368_MethodInfo,
	&Selectable_OnPointerExit_m5369_MethodInfo,
	&Selectable_OnSelect_m5370_MethodInfo,
	&Selectable_OnDeselect_m5371_MethodInfo,
	&Selectable_Select_m5372_MethodInfo,
	&Slider_OnInitializePotentialDrag_m5414_MethodInfo,
	&Slider_OnDrag_m5408_MethodInfo,
	&Slider_Rebuild_m5395_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m5417_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m5416_MethodInfo,
	&Slider_Rebuild_m5395_MethodInfo,
	&Slider_OnDrag_m5408_MethodInfo,
	&Slider_OnInitializePotentialDrag_m5414_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m5416_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m5417_MethodInfo,
};
static bool Slider_t1295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Slider_t1295_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t1428_0_0_0,
	&IInitializePotentialDragHandler_t1345_0_0_0,
	&IDragHandler_t1347_0_0_0,
	&ICanvasElement_t1360_0_0_0,
};
static Il2CppInterfaceOffsetPair Slider_t1295_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t1428_0_0_0, 15},
	{ &IPointerEnterHandler_t1340_0_0_0, 15},
	{ &IPointerExitHandler_t1341_0_0_0, 16},
	{ &IPointerDownHandler_t1342_0_0_0, 17},
	{ &IPointerUpHandler_t1343_0_0_0, 18},
	{ &ISelectHandler_t1352_0_0_0, 19},
	{ &IDeselectHandler_t1353_0_0_0, 20},
	{ &IMoveHandler_t1354_0_0_0, 21},
	{ &IInitializePotentialDragHandler_t1345_0_0_0, 37},
	{ &IDragHandler_t1347_0_0_0, 38},
	{ &ICanvasElement_t1360_0_0_0, 39},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Slider_t1295_1_0_0;
struct Slider_t1295;
const Il2CppTypeDefinitionMetadata Slider_t1295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Slider_t1295_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Slider_t1295_InterfacesTypeInfos/* implementedInterfaces */
	, Slider_t1295_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t1215_0_0_0/* parent */
	, Slider_t1295_VTable/* vtableMethods */
	, Slider_t1295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Slider_t1295_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slider"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Slider_t1295_MethodInfos/* methods */
	, Slider_t1295_PropertyInfos/* properties */
	, Slider_t1295_FieldInfos/* fields */
	, NULL/* events */
	, &Slider_t1295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 237/* custom_attributes_cache */
	, &Slider_t1295_0_0_0/* byval_arg */
	, &Slider_t1295_1_0_0/* this_arg */
	, &Slider_t1295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slider_t1295)/* instance_size */
	, sizeof (Slider_t1295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 43/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 47/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// Metadata Definition UnityEngine.UI.SpriteState
extern TypeInfo SpriteState_t1290_il2cpp_TypeInfo;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
extern Il2CppType Sprite_t766_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
MethodInfo SpriteState_get_highlightedSprite_m5418_MethodInfo = 
{
	"get_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_get_highlightedSprite_m5418/* method */
	, &SpriteState_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t766_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sprite_t766_0_0_0;
static ParameterInfo SpriteState_t1290_SpriteState_set_highlightedSprite_m5419_ParameterInfos[] = 
{
	{"value", 0, 134218281, 0, &Sprite_t766_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
MethodInfo SpriteState_set_highlightedSprite_m5419_MethodInfo = 
{
	"set_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_set_highlightedSprite_m5419/* method */
	, &SpriteState_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, SpriteState_t1290_SpriteState_set_highlightedSprite_m5419_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sprite_t766_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
MethodInfo SpriteState_get_pressedSprite_m5420_MethodInfo = 
{
	"get_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_get_pressedSprite_m5420/* method */
	, &SpriteState_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t766_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sprite_t766_0_0_0;
static ParameterInfo SpriteState_t1290_SpriteState_set_pressedSprite_m5421_ParameterInfos[] = 
{
	{"value", 0, 134218282, 0, &Sprite_t766_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
MethodInfo SpriteState_set_pressedSprite_m5421_MethodInfo = 
{
	"set_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_set_pressedSprite_m5421/* method */
	, &SpriteState_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, SpriteState_t1290_SpriteState_set_pressedSprite_m5421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sprite_t766_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
MethodInfo SpriteState_get_disabledSprite_m5422_MethodInfo = 
{
	"get_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_get_disabledSprite_m5422/* method */
	, &SpriteState_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t766_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sprite_t766_0_0_0;
static ParameterInfo SpriteState_t1290_SpriteState_set_disabledSprite_m5423_ParameterInfos[] = 
{
	{"value", 0, 134218283, 0, &Sprite_t766_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
MethodInfo SpriteState_set_disabledSprite_m5423_MethodInfo = 
{
	"set_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_set_disabledSprite_m5423/* method */
	, &SpriteState_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, SpriteState_t1290_SpriteState_set_disabledSprite_m5423_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SpriteState_t1290_MethodInfos[] =
{
	&SpriteState_get_highlightedSprite_m5418_MethodInfo,
	&SpriteState_set_highlightedSprite_m5419_MethodInfo,
	&SpriteState_get_pressedSprite_m5420_MethodInfo,
	&SpriteState_set_pressedSprite_m5421_MethodInfo,
	&SpriteState_get_disabledSprite_m5422_MethodInfo,
	&SpriteState_set_disabledSprite_m5423_MethodInfo,
	NULL
};
extern Il2CppType Sprite_t766_0_0_1;
FieldInfo SpriteState_t1290____m_HighlightedSprite_0_FieldInfo = 
{
	"m_HighlightedSprite"/* name */
	, &Sprite_t766_0_0_1/* type */
	, &SpriteState_t1290_il2cpp_TypeInfo/* parent */
	, offsetof(SpriteState_t1290, ___m_HighlightedSprite_0) + sizeof(Object_t)/* offset */
	, 246/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_1;
FieldInfo SpriteState_t1290____m_PressedSprite_1_FieldInfo = 
{
	"m_PressedSprite"/* name */
	, &Sprite_t766_0_0_1/* type */
	, &SpriteState_t1290_il2cpp_TypeInfo/* parent */
	, offsetof(SpriteState_t1290, ___m_PressedSprite_1) + sizeof(Object_t)/* offset */
	, 247/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_1;
FieldInfo SpriteState_t1290____m_DisabledSprite_2_FieldInfo = 
{
	"m_DisabledSprite"/* name */
	, &Sprite_t766_0_0_1/* type */
	, &SpriteState_t1290_il2cpp_TypeInfo/* parent */
	, offsetof(SpriteState_t1290, ___m_DisabledSprite_2) + sizeof(Object_t)/* offset */
	, 248/* custom_attributes_cache */

};
static FieldInfo* SpriteState_t1290_FieldInfos[] =
{
	&SpriteState_t1290____m_HighlightedSprite_0_FieldInfo,
	&SpriteState_t1290____m_PressedSprite_1_FieldInfo,
	&SpriteState_t1290____m_DisabledSprite_2_FieldInfo,
	NULL
};
extern MethodInfo SpriteState_get_highlightedSprite_m5418_MethodInfo;
extern MethodInfo SpriteState_set_highlightedSprite_m5419_MethodInfo;
static PropertyInfo SpriteState_t1290____highlightedSprite_PropertyInfo = 
{
	&SpriteState_t1290_il2cpp_TypeInfo/* parent */
	, "highlightedSprite"/* name */
	, &SpriteState_get_highlightedSprite_m5418_MethodInfo/* get */
	, &SpriteState_set_highlightedSprite_m5419_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo SpriteState_get_pressedSprite_m5420_MethodInfo;
extern MethodInfo SpriteState_set_pressedSprite_m5421_MethodInfo;
static PropertyInfo SpriteState_t1290____pressedSprite_PropertyInfo = 
{
	&SpriteState_t1290_il2cpp_TypeInfo/* parent */
	, "pressedSprite"/* name */
	, &SpriteState_get_pressedSprite_m5420_MethodInfo/* get */
	, &SpriteState_set_pressedSprite_m5421_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo SpriteState_get_disabledSprite_m5422_MethodInfo;
extern MethodInfo SpriteState_set_disabledSprite_m5423_MethodInfo;
static PropertyInfo SpriteState_t1290____disabledSprite_PropertyInfo = 
{
	&SpriteState_t1290_il2cpp_TypeInfo/* parent */
	, "disabledSprite"/* name */
	, &SpriteState_get_disabledSprite_m5422_MethodInfo/* get */
	, &SpriteState_set_disabledSprite_m5423_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* SpriteState_t1290_PropertyInfos[] =
{
	&SpriteState_t1290____highlightedSprite_PropertyInfo,
	&SpriteState_t1290____pressedSprite_PropertyInfo,
	&SpriteState_t1290____disabledSprite_PropertyInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1319_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1320_MethodInfo;
extern MethodInfo ValueType_ToString_m1321_MethodInfo;
static Il2CppMethodReference SpriteState_t1290_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool SpriteState_t1290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType SpriteState_t1290_1_0_0;
const Il2CppTypeDefinitionMetadata SpriteState_t1290_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, SpriteState_t1290_VTable/* vtableMethods */
	, SpriteState_t1290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SpriteState_t1290_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpriteState"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SpriteState_t1290_MethodInfos/* methods */
	, SpriteState_t1290_PropertyInfos/* properties */
	, SpriteState_t1290_FieldInfos/* fields */
	, NULL/* events */
	, &SpriteState_t1290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpriteState_t1290_0_0_0/* byval_arg */
	, &SpriteState_t1290_1_0_0/* this_arg */
	, &SpriteState_t1290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpriteState_t1290)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpriteState_t1290)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry
extern TypeInfo MatEntry_t1296_il2cpp_TypeInfo;
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
MethodInfo MatEntry__ctor_m5424_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatEntry__ctor_m5424/* method */
	, &MatEntry_t1296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MatEntry_t1296_MethodInfos[] =
{
	&MatEntry__ctor_m5424_MethodInfo,
	NULL
};
extern Il2CppType Material_t989_0_0_6;
FieldInfo MatEntry_t1296____baseMat_0_FieldInfo = 
{
	"baseMat"/* name */
	, &Material_t989_0_0_6/* type */
	, &MatEntry_t1296_il2cpp_TypeInfo/* parent */
	, offsetof(MatEntry_t1296, ___baseMat_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Material_t989_0_0_6;
FieldInfo MatEntry_t1296____customMat_1_FieldInfo = 
{
	"customMat"/* name */
	, &Material_t989_0_0_6/* type */
	, &MatEntry_t1296_il2cpp_TypeInfo/* parent */
	, offsetof(MatEntry_t1296, ___customMat_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo MatEntry_t1296____count_2_FieldInfo = 
{
	"count"/* name */
	, &Int32_t189_0_0_6/* type */
	, &MatEntry_t1296_il2cpp_TypeInfo/* parent */
	, offsetof(MatEntry_t1296, ___count_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo MatEntry_t1296____stencilID_3_FieldInfo = 
{
	"stencilID"/* name */
	, &Int32_t189_0_0_6/* type */
	, &MatEntry_t1296_il2cpp_TypeInfo/* parent */
	, offsetof(MatEntry_t1296, ___stencilID_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* MatEntry_t1296_FieldInfos[] =
{
	&MatEntry_t1296____baseMat_0_FieldInfo,
	&MatEntry_t1296____customMat_1_FieldInfo,
	&MatEntry_t1296____count_2_FieldInfo,
	&MatEntry_t1296____stencilID_3_FieldInfo,
	NULL
};
static Il2CppMethodReference MatEntry_t1296_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool MatEntry_t1296_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType MatEntry_t1296_0_0_0;
extern Il2CppType MatEntry_t1296_1_0_0;
extern TypeInfo StencilMaterial_t1298_il2cpp_TypeInfo;
extern Il2CppType StencilMaterial_t1298_0_0_0;
struct MatEntry_t1296;
const Il2CppTypeDefinitionMetadata MatEntry_t1296_DefinitionMetadata = 
{
	&StencilMaterial_t1298_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatEntry_t1296_VTable/* vtableMethods */
	, MatEntry_t1296_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MatEntry_t1296_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatEntry"/* name */
	, ""/* namespaze */
	, MatEntry_t1296_MethodInfos/* methods */
	, NULL/* properties */
	, MatEntry_t1296_FieldInfos/* fields */
	, NULL/* events */
	, &MatEntry_t1296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatEntry_t1296_0_0_0/* byval_arg */
	, &MatEntry_t1296_1_0_0/* this_arg */
	, &MatEntry_t1296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatEntry_t1296)/* instance_size */
	, sizeof (MatEntry_t1296)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// Metadata Definition UnityEngine.UI.StencilMaterial
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::.cctor()
MethodInfo StencilMaterial__cctor_m5425_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StencilMaterial__cctor_m5425/* method */
	, &StencilMaterial_t1298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Material_t989_0_0_0;
extern Il2CppType Material_t989_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo StencilMaterial_t1298_StencilMaterial_Add_m5426_ParameterInfos[] = 
{
	{"baseMat", 0, 134218284, 0, &Material_t989_0_0_0},
	{"stencilID", 1, 134218285, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Material_t989_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32)
MethodInfo StencilMaterial_Add_m5426_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&StencilMaterial_Add_m5426/* method */
	, &StencilMaterial_t1298_il2cpp_TypeInfo/* declaring_type */
	, &Material_t989_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t189/* invoker_method */
	, StencilMaterial_t1298_StencilMaterial_Add_m5426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Material_t989_0_0_0;
static ParameterInfo StencilMaterial_t1298_StencilMaterial_Remove_m5427_ParameterInfos[] = 
{
	{"customMat", 0, 134218286, 0, &Material_t989_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
MethodInfo StencilMaterial_Remove_m5427_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&StencilMaterial_Remove_m5427/* method */
	, &StencilMaterial_t1298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, StencilMaterial_t1298_StencilMaterial_Remove_m5427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* StencilMaterial_t1298_MethodInfos[] =
{
	&StencilMaterial__cctor_m5425_MethodInfo,
	&StencilMaterial_Add_m5426_MethodInfo,
	&StencilMaterial_Remove_m5427_MethodInfo,
	NULL
};
extern Il2CppType List_1_t1297_0_0_17;
FieldInfo StencilMaterial_t1298____m_List_0_FieldInfo = 
{
	"m_List"/* name */
	, &List_1_t1297_0_0_17/* type */
	, &StencilMaterial_t1298_il2cpp_TypeInfo/* parent */
	, offsetof(StencilMaterial_t1298_StaticFields, ___m_List_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* StencilMaterial_t1298_FieldInfos[] =
{
	&StencilMaterial_t1298____m_List_0_FieldInfo,
	NULL
};
static const Il2CppType* StencilMaterial_t1298_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatEntry_t1296_0_0_0,
};
static Il2CppMethodReference StencilMaterial_t1298_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool StencilMaterial_t1298_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType StencilMaterial_t1298_1_0_0;
struct StencilMaterial_t1298;
const Il2CppTypeDefinitionMetadata StencilMaterial_t1298_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StencilMaterial_t1298_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StencilMaterial_t1298_VTable/* vtableMethods */
	, StencilMaterial_t1298_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo StencilMaterial_t1298_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "StencilMaterial"/* name */
	, "UnityEngine.UI"/* namespaze */
	, StencilMaterial_t1298_MethodInfos/* methods */
	, NULL/* properties */
	, StencilMaterial_t1298_FieldInfos/* fields */
	, NULL/* events */
	, &StencilMaterial_t1298_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StencilMaterial_t1298_0_0_0/* byval_arg */
	, &StencilMaterial_t1298_1_0_0/* this_arg */
	, &StencilMaterial_t1298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StencilMaterial_t1298)/* instance_size */
	, sizeof (StencilMaterial_t1298)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StencilMaterial_t1298_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// Metadata Definition UnityEngine.UI.Text
extern TypeInfo Text_t1263_il2cpp_TypeInfo;
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.ctor()
MethodInfo Text__ctor_m5428_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Text__ctor_m5428/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.cctor()
MethodInfo Text__cctor_m5429_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Text__cctor_m5429/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextGenerator_t1266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGenerator()
MethodInfo Text_get_cachedTextGenerator_m5430_MethodInfo = 
{
	"get_cachedTextGenerator"/* name */
	, (methodPointerType)&Text_get_cachedTextGenerator_m5430/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t1266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextGenerator_t1266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGeneratorForLayout()
MethodInfo Text_get_cachedTextGeneratorForLayout_m5431_MethodInfo = 
{
	"get_cachedTextGeneratorForLayout"/* name */
	, (methodPointerType)&Text_get_cachedTextGeneratorForLayout_m5431/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t1266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Material_t989_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Text::get_defaultMaterial()
MethodInfo Text_get_defaultMaterial_m5432_MethodInfo = 
{
	"get_defaultMaterial"/* name */
	, (methodPointerType)&Text_get_defaultMaterial_m5432/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Material_t989_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Texture_t736_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Texture UnityEngine.UI.Text::get_mainTexture()
MethodInfo Text_get_mainTexture_m5433_MethodInfo = 
{
	"get_mainTexture"/* name */
	, (methodPointerType)&Text_get_mainTexture_m5433/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Texture_t736_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::FontTextureChanged()
MethodInfo Text_FontTextureChanged_m5434_MethodInfo = 
{
	"FontTextureChanged"/* name */
	, (methodPointerType)&Text_FontTextureChanged_m5434/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Font_t1222_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Font UnityEngine.UI.Text::get_font()
MethodInfo Text_get_font_m5435_MethodInfo = 
{
	"get_font"/* name */
	, (methodPointerType)&Text_get_font_m5435/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Font_t1222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Font_t1222_0_0_0;
extern Il2CppType Font_t1222_0_0_0;
static ParameterInfo Text_t1263_Text_set_font_m5436_ParameterInfos[] = 
{
	{"value", 0, 134218287, 0, &Font_t1222_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
MethodInfo Text_set_font_m5436_MethodInfo = 
{
	"set_font"/* name */
	, (methodPointerType)&Text_set_font_m5436/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Text_t1263_Text_set_font_m5436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.Text::get_text()
MethodInfo Text_get_text_m5437_MethodInfo = 
{
	"get_text"/* name */
	, (methodPointerType)&Text_get_text_m5437/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Text_t1263_Text_set_text_m5438_ParameterInfos[] = 
{
	{"value", 0, 134218288, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_text(System.String)
MethodInfo Text_set_text_m5438_MethodInfo = 
{
	"set_text"/* name */
	, (methodPointerType)&Text_set_text_m5438/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Text_t1263_Text_set_text_m5438_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_supportRichText()
MethodInfo Text_get_supportRichText_m5439_MethodInfo = 
{
	"get_supportRichText"/* name */
	, (methodPointerType)&Text_get_supportRichText_m5439/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Text_t1263_Text_set_supportRichText_m5440_ParameterInfos[] = 
{
	{"value", 0, 134218289, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_supportRichText(System.Boolean)
MethodInfo Text_set_supportRichText_m5440_MethodInfo = 
{
	"set_supportRichText"/* name */
	, (methodPointerType)&Text_set_supportRichText_m5440/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Text_t1263_Text_set_supportRichText_m5440_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_resizeTextForBestFit()
MethodInfo Text_get_resizeTextForBestFit_m5441_MethodInfo = 
{
	"get_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_get_resizeTextForBestFit_m5441/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Text_t1263_Text_set_resizeTextForBestFit_m5442_ParameterInfos[] = 
{
	{"value", 0, 134218290, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextForBestFit(System.Boolean)
MethodInfo Text_set_resizeTextForBestFit_m5442_MethodInfo = 
{
	"set_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_set_resizeTextForBestFit_m5442/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Text_t1263_Text_set_resizeTextForBestFit_m5442_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMinSize()
MethodInfo Text_get_resizeTextMinSize_m5443_MethodInfo = 
{
	"get_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMinSize_m5443/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Text_t1263_Text_set_resizeTextMinSize_m5444_ParameterInfos[] = 
{
	{"value", 0, 134218291, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMinSize(System.Int32)
MethodInfo Text_set_resizeTextMinSize_m5444_MethodInfo = 
{
	"set_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMinSize_m5444/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Text_t1263_Text_set_resizeTextMinSize_m5444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMaxSize()
MethodInfo Text_get_resizeTextMaxSize_m5445_MethodInfo = 
{
	"get_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMaxSize_m5445/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Text_t1263_Text_set_resizeTextMaxSize_m5446_ParameterInfos[] = 
{
	{"value", 0, 134218292, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMaxSize(System.Int32)
MethodInfo Text_set_resizeTextMaxSize_m5446_MethodInfo = 
{
	"set_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMaxSize_m5446/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Text_t1263_Text_set_resizeTextMaxSize_m5446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextAnchor_t1410_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t1410 (MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.Text::get_alignment()
MethodInfo Text_get_alignment_m5447_MethodInfo = 
{
	"get_alignment"/* name */
	, (methodPointerType)&Text_get_alignment_m5447/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t1410_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t1410/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextAnchor_t1410_0_0_0;
extern Il2CppType TextAnchor_t1410_0_0_0;
static ParameterInfo Text_t1263_Text_set_alignment_m5448_ParameterInfos[] = 
{
	{"value", 0, 134218293, 0, &TextAnchor_t1410_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
MethodInfo Text_set_alignment_m5448_MethodInfo = 
{
	"set_alignment"/* name */
	, (methodPointerType)&Text_set_alignment_m5448/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Text_t1263_Text_set_alignment_m5448_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_fontSize()
MethodInfo Text_get_fontSize_m5449_MethodInfo = 
{
	"get_fontSize"/* name */
	, (methodPointerType)&Text_get_fontSize_m5449/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo Text_t1263_Text_set_fontSize_m5450_ParameterInfos[] = 
{
	{"value", 0, 134218294, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
MethodInfo Text_set_fontSize_m5450_MethodInfo = 
{
	"set_fontSize"/* name */
	, (methodPointerType)&Text_set_fontSize_m5450/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Text_t1263_Text_set_fontSize_m5450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HorizontalWrapMode_t1463_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t1463 (MethodInfo* method, void* obj, void** args);
// UnityEngine.HorizontalWrapMode UnityEngine.UI.Text::get_horizontalOverflow()
MethodInfo Text_get_horizontalOverflow_m5451_MethodInfo = 
{
	"get_horizontalOverflow"/* name */
	, (methodPointerType)&Text_get_horizontalOverflow_m5451/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t1463_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t1463/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HorizontalWrapMode_t1463_0_0_0;
extern Il2CppType HorizontalWrapMode_t1463_0_0_0;
static ParameterInfo Text_t1263_Text_set_horizontalOverflow_m5452_ParameterInfos[] = 
{
	{"value", 0, 134218295, 0, &HorizontalWrapMode_t1463_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_horizontalOverflow(UnityEngine.HorizontalWrapMode)
MethodInfo Text_set_horizontalOverflow_m5452_MethodInfo = 
{
	"set_horizontalOverflow"/* name */
	, (methodPointerType)&Text_set_horizontalOverflow_m5452/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Text_t1263_Text_set_horizontalOverflow_m5452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VerticalWrapMode_t1464_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t1464 (MethodInfo* method, void* obj, void** args);
// UnityEngine.VerticalWrapMode UnityEngine.UI.Text::get_verticalOverflow()
MethodInfo Text_get_verticalOverflow_m5453_MethodInfo = 
{
	"get_verticalOverflow"/* name */
	, (methodPointerType)&Text_get_verticalOverflow_m5453/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t1464_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t1464/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VerticalWrapMode_t1464_0_0_0;
extern Il2CppType VerticalWrapMode_t1464_0_0_0;
static ParameterInfo Text_t1263_Text_set_verticalOverflow_m5454_ParameterInfos[] = 
{
	{"value", 0, 134218296, 0, &VerticalWrapMode_t1464_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_verticalOverflow(UnityEngine.VerticalWrapMode)
MethodInfo Text_set_verticalOverflow_m5454_MethodInfo = 
{
	"set_verticalOverflow"/* name */
	, (methodPointerType)&Text_set_verticalOverflow_m5454/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Text_t1263_Text_set_verticalOverflow_m5454_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_lineSpacing()
MethodInfo Text_get_lineSpacing_m5455_MethodInfo = 
{
	"get_lineSpacing"/* name */
	, (methodPointerType)&Text_get_lineSpacing_m5455/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo Text_t1263_Text_set_lineSpacing_m5456_ParameterInfos[] = 
{
	{"value", 0, 134218297, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_lineSpacing(System.Single)
MethodInfo Text_set_lineSpacing_m5456_MethodInfo = 
{
	"set_lineSpacing"/* name */
	, (methodPointerType)&Text_set_lineSpacing_m5456/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, Text_t1263_Text_set_lineSpacing_m5456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FontStyle_t1462_0_0_0;
extern void* RuntimeInvoker_FontStyle_t1462 (MethodInfo* method, void* obj, void** args);
// UnityEngine.FontStyle UnityEngine.UI.Text::get_fontStyle()
MethodInfo Text_get_fontStyle_m5457_MethodInfo = 
{
	"get_fontStyle"/* name */
	, (methodPointerType)&Text_get_fontStyle_m5457/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &FontStyle_t1462_0_0_0/* return_type */
	, RuntimeInvoker_FontStyle_t1462/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FontStyle_t1462_0_0_0;
extern Il2CppType FontStyle_t1462_0_0_0;
static ParameterInfo Text_t1263_Text_set_fontStyle_m5458_ParameterInfos[] = 
{
	{"value", 0, 134218298, 0, &FontStyle_t1462_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontStyle(UnityEngine.FontStyle)
MethodInfo Text_set_fontStyle_m5458_MethodInfo = 
{
	"set_fontStyle"/* name */
	, (methodPointerType)&Text_set_fontStyle_m5458/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Text_t1263_Text_set_fontStyle_m5458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_pixelsPerUnit()
MethodInfo Text_get_pixelsPerUnit_m5459_MethodInfo = 
{
	"get_pixelsPerUnit"/* name */
	, (methodPointerType)&Text_get_pixelsPerUnit_m5459/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnEnable()
MethodInfo Text_OnEnable_m5460_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Text_OnEnable_m5460/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnDisable()
MethodInfo Text_OnDisable_m5461_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Text_OnDisable_m5461/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::UpdateGeometry()
MethodInfo Text_UpdateGeometry_m5462_MethodInfo = 
{
	"UpdateGeometry"/* name */
	, (methodPointerType)&Text_UpdateGeometry_m5462/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo Text_t1263_Text_GetGenerationSettings_m5463_ParameterInfos[] = 
{
	{"extents", 0, 134218299, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType TextGenerationSettings_t1364_0_0_0;
extern void* RuntimeInvoker_TextGenerationSettings_t1364_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerationSettings UnityEngine.UI.Text::GetGenerationSettings(UnityEngine.Vector2)
MethodInfo Text_GetGenerationSettings_m5463_MethodInfo = 
{
	"GetGenerationSettings"/* name */
	, (methodPointerType)&Text_GetGenerationSettings_m5463/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerationSettings_t1364_0_0_0/* return_type */
	, RuntimeInvoker_TextGenerationSettings_t1364_Vector2_t739/* invoker_method */
	, Text_t1263_Text_GetGenerationSettings_m5463_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextAnchor_t1410_0_0_0;
static ParameterInfo Text_t1263_Text_GetTextAnchorPivot_m5464_ParameterInfos[] = 
{
	{"anchor", 0, 134218300, 0, &TextAnchor_t1410_0_0_0},
};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739_Int32_t189 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Text::GetTextAnchorPivot(UnityEngine.TextAnchor)
MethodInfo Text_GetTextAnchorPivot_m5464_MethodInfo = 
{
	"GetTextAnchorPivot"/* name */
	, (methodPointerType)&Text_GetTextAnchorPivot_m5464/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739_Int32_t189/* invoker_method */
	, Text_t1263_Text_GetTextAnchorPivot_m5464_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1267_0_0_0;
extern Il2CppType List_1_t1267_0_0_0;
static ParameterInfo Text_t1263_Text_OnFillVBO_m5465_ParameterInfos[] = 
{
	{"vbo", 0, 134218301, 0, &List_1_t1267_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
MethodInfo Text_OnFillVBO_m5465_MethodInfo = 
{
	"OnFillVBO"/* name */
	, (methodPointerType)&Text_OnFillVBO_m5465/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Text_t1263_Text_OnFillVBO_m5465_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputHorizontal()
MethodInfo Text_CalculateLayoutInputHorizontal_m5466_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputHorizontal_m5466/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 48/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputVertical()
MethodInfo Text_CalculateLayoutInputVertical_m5467_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputVertical_m5467/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 49/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minWidth()
MethodInfo Text_get_minWidth_m5468_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&Text_get_minWidth_m5468/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredWidth()
MethodInfo Text_get_preferredWidth_m5469_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&Text_get_preferredWidth_m5469/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleWidth()
MethodInfo Text_get_flexibleWidth_m5470_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&Text_get_flexibleWidth_m5470/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minHeight()
MethodInfo Text_get_minHeight_m5471_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&Text_get_minHeight_m5471/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 53/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredHeight()
MethodInfo Text_get_preferredHeight_m5472_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&Text_get_preferredHeight_m5472/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 54/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleHeight()
MethodInfo Text_get_flexibleHeight_m5473_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&Text_get_flexibleHeight_m5473/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 55/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_layoutPriority()
MethodInfo Text_get_layoutPriority_m5474_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&Text_get_layoutPriority_m5474/* method */
	, &Text_t1263_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 56/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Text_t1263_MethodInfos[] =
{
	&Text__ctor_m5428_MethodInfo,
	&Text__cctor_m5429_MethodInfo,
	&Text_get_cachedTextGenerator_m5430_MethodInfo,
	&Text_get_cachedTextGeneratorForLayout_m5431_MethodInfo,
	&Text_get_defaultMaterial_m5432_MethodInfo,
	&Text_get_mainTexture_m5433_MethodInfo,
	&Text_FontTextureChanged_m5434_MethodInfo,
	&Text_get_font_m5435_MethodInfo,
	&Text_set_font_m5436_MethodInfo,
	&Text_get_text_m5437_MethodInfo,
	&Text_set_text_m5438_MethodInfo,
	&Text_get_supportRichText_m5439_MethodInfo,
	&Text_set_supportRichText_m5440_MethodInfo,
	&Text_get_resizeTextForBestFit_m5441_MethodInfo,
	&Text_set_resizeTextForBestFit_m5442_MethodInfo,
	&Text_get_resizeTextMinSize_m5443_MethodInfo,
	&Text_set_resizeTextMinSize_m5444_MethodInfo,
	&Text_get_resizeTextMaxSize_m5445_MethodInfo,
	&Text_set_resizeTextMaxSize_m5446_MethodInfo,
	&Text_get_alignment_m5447_MethodInfo,
	&Text_set_alignment_m5448_MethodInfo,
	&Text_get_fontSize_m5449_MethodInfo,
	&Text_set_fontSize_m5450_MethodInfo,
	&Text_get_horizontalOverflow_m5451_MethodInfo,
	&Text_set_horizontalOverflow_m5452_MethodInfo,
	&Text_get_verticalOverflow_m5453_MethodInfo,
	&Text_set_verticalOverflow_m5454_MethodInfo,
	&Text_get_lineSpacing_m5455_MethodInfo,
	&Text_set_lineSpacing_m5456_MethodInfo,
	&Text_get_fontStyle_m5457_MethodInfo,
	&Text_set_fontStyle_m5458_MethodInfo,
	&Text_get_pixelsPerUnit_m5459_MethodInfo,
	&Text_OnEnable_m5460_MethodInfo,
	&Text_OnDisable_m5461_MethodInfo,
	&Text_UpdateGeometry_m5462_MethodInfo,
	&Text_GetGenerationSettings_m5463_MethodInfo,
	&Text_GetTextAnchorPivot_m5464_MethodInfo,
	&Text_OnFillVBO_m5465_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m5466_MethodInfo,
	&Text_CalculateLayoutInputVertical_m5467_MethodInfo,
	&Text_get_minWidth_m5468_MethodInfo,
	&Text_get_preferredWidth_m5469_MethodInfo,
	&Text_get_flexibleWidth_m5470_MethodInfo,
	&Text_get_minHeight_m5471_MethodInfo,
	&Text_get_preferredHeight_m5472_MethodInfo,
	&Text_get_flexibleHeight_m5473_MethodInfo,
	&Text_get_layoutPriority_m5474_MethodInfo,
	NULL
};
extern Il2CppType FontData_t1223_0_0_1;
FieldInfo Text_t1263____m_FontData_23_FieldInfo = 
{
	"m_FontData"/* name */
	, &FontData_t1223_0_0_1/* type */
	, &Text_t1263_il2cpp_TypeInfo/* parent */
	, offsetof(Text_t1263, ___m_FontData_23)/* offset */
	, 250/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_4;
FieldInfo Text_t1263____m_Text_24_FieldInfo = 
{
	"m_Text"/* name */
	, &String_t_0_0_4/* type */
	, &Text_t1263_il2cpp_TypeInfo/* parent */
	, offsetof(Text_t1263, ___m_Text_24)/* offset */
	, 251/* custom_attributes_cache */

};
extern Il2CppType TextGenerator_t1266_0_0_1;
FieldInfo Text_t1263____m_TextCache_25_FieldInfo = 
{
	"m_TextCache"/* name */
	, &TextGenerator_t1266_0_0_1/* type */
	, &Text_t1263_il2cpp_TypeInfo/* parent */
	, offsetof(Text_t1263, ___m_TextCache_25)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TextGenerator_t1266_0_0_1;
FieldInfo Text_t1263____m_TextCacheForLayout_26_FieldInfo = 
{
	"m_TextCacheForLayout"/* name */
	, &TextGenerator_t1266_0_0_1/* type */
	, &Text_t1263_il2cpp_TypeInfo/* parent */
	, offsetof(Text_t1263, ___m_TextCacheForLayout_26)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_17;
FieldInfo Text_t1263____kEpsilon_27_FieldInfo = 
{
	"kEpsilon"/* name */
	, &Single_t202_0_0_17/* type */
	, &Text_t1263_il2cpp_TypeInfo/* parent */
	, offsetof(Text_t1263_StaticFields, ___kEpsilon_27)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Material_t989_0_0_20;
FieldInfo Text_t1263____s_DefaultText_28_FieldInfo = 
{
	"s_DefaultText"/* name */
	, &Material_t989_0_0_20/* type */
	, &Text_t1263_il2cpp_TypeInfo/* parent */
	, offsetof(Text_t1263_StaticFields, ___s_DefaultText_28)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_129;
FieldInfo Text_t1263____m_DisableFontTextureRebuiltCallback_29_FieldInfo = 
{
	"m_DisableFontTextureRebuiltCallback"/* name */
	, &Boolean_t203_0_0_129/* type */
	, &Text_t1263_il2cpp_TypeInfo/* parent */
	, offsetof(Text_t1263, ___m_DisableFontTextureRebuiltCallback_29)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Text_t1263_FieldInfos[] =
{
	&Text_t1263____m_FontData_23_FieldInfo,
	&Text_t1263____m_Text_24_FieldInfo,
	&Text_t1263____m_TextCache_25_FieldInfo,
	&Text_t1263____m_TextCacheForLayout_26_FieldInfo,
	&Text_t1263____kEpsilon_27_FieldInfo,
	&Text_t1263____s_DefaultText_28_FieldInfo,
	&Text_t1263____m_DisableFontTextureRebuiltCallback_29_FieldInfo,
	NULL
};
extern MethodInfo Text_get_cachedTextGenerator_m5430_MethodInfo;
static PropertyInfo Text_t1263____cachedTextGenerator_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "cachedTextGenerator"/* name */
	, &Text_get_cachedTextGenerator_m5430_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_cachedTextGeneratorForLayout_m5431_MethodInfo;
static PropertyInfo Text_t1263____cachedTextGeneratorForLayout_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "cachedTextGeneratorForLayout"/* name */
	, &Text_get_cachedTextGeneratorForLayout_m5431_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_defaultMaterial_m5432_MethodInfo;
static PropertyInfo Text_t1263____defaultMaterial_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "defaultMaterial"/* name */
	, &Text_get_defaultMaterial_m5432_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_mainTexture_m5433_MethodInfo;
static PropertyInfo Text_t1263____mainTexture_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "mainTexture"/* name */
	, &Text_get_mainTexture_m5433_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_font_m5435_MethodInfo;
extern MethodInfo Text_set_font_m5436_MethodInfo;
static PropertyInfo Text_t1263____font_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "font"/* name */
	, &Text_get_font_m5435_MethodInfo/* get */
	, &Text_set_font_m5436_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_text_m5437_MethodInfo;
extern MethodInfo Text_set_text_m5438_MethodInfo;
static PropertyInfo Text_t1263____text_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "text"/* name */
	, &Text_get_text_m5437_MethodInfo/* get */
	, &Text_set_text_m5438_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_supportRichText_m5439_MethodInfo;
extern MethodInfo Text_set_supportRichText_m5440_MethodInfo;
static PropertyInfo Text_t1263____supportRichText_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "supportRichText"/* name */
	, &Text_get_supportRichText_m5439_MethodInfo/* get */
	, &Text_set_supportRichText_m5440_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_resizeTextForBestFit_m5441_MethodInfo;
extern MethodInfo Text_set_resizeTextForBestFit_m5442_MethodInfo;
static PropertyInfo Text_t1263____resizeTextForBestFit_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "resizeTextForBestFit"/* name */
	, &Text_get_resizeTextForBestFit_m5441_MethodInfo/* get */
	, &Text_set_resizeTextForBestFit_m5442_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_resizeTextMinSize_m5443_MethodInfo;
extern MethodInfo Text_set_resizeTextMinSize_m5444_MethodInfo;
static PropertyInfo Text_t1263____resizeTextMinSize_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "resizeTextMinSize"/* name */
	, &Text_get_resizeTextMinSize_m5443_MethodInfo/* get */
	, &Text_set_resizeTextMinSize_m5444_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_resizeTextMaxSize_m5445_MethodInfo;
extern MethodInfo Text_set_resizeTextMaxSize_m5446_MethodInfo;
static PropertyInfo Text_t1263____resizeTextMaxSize_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "resizeTextMaxSize"/* name */
	, &Text_get_resizeTextMaxSize_m5445_MethodInfo/* get */
	, &Text_set_resizeTextMaxSize_m5446_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_alignment_m5447_MethodInfo;
extern MethodInfo Text_set_alignment_m5448_MethodInfo;
static PropertyInfo Text_t1263____alignment_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "alignment"/* name */
	, &Text_get_alignment_m5447_MethodInfo/* get */
	, &Text_set_alignment_m5448_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_fontSize_m5449_MethodInfo;
extern MethodInfo Text_set_fontSize_m5450_MethodInfo;
static PropertyInfo Text_t1263____fontSize_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "fontSize"/* name */
	, &Text_get_fontSize_m5449_MethodInfo/* get */
	, &Text_set_fontSize_m5450_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_horizontalOverflow_m5451_MethodInfo;
extern MethodInfo Text_set_horizontalOverflow_m5452_MethodInfo;
static PropertyInfo Text_t1263____horizontalOverflow_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "horizontalOverflow"/* name */
	, &Text_get_horizontalOverflow_m5451_MethodInfo/* get */
	, &Text_set_horizontalOverflow_m5452_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_verticalOverflow_m5453_MethodInfo;
extern MethodInfo Text_set_verticalOverflow_m5454_MethodInfo;
static PropertyInfo Text_t1263____verticalOverflow_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "verticalOverflow"/* name */
	, &Text_get_verticalOverflow_m5453_MethodInfo/* get */
	, &Text_set_verticalOverflow_m5454_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_lineSpacing_m5455_MethodInfo;
extern MethodInfo Text_set_lineSpacing_m5456_MethodInfo;
static PropertyInfo Text_t1263____lineSpacing_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "lineSpacing"/* name */
	, &Text_get_lineSpacing_m5455_MethodInfo/* get */
	, &Text_set_lineSpacing_m5456_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_fontStyle_m5457_MethodInfo;
extern MethodInfo Text_set_fontStyle_m5458_MethodInfo;
static PropertyInfo Text_t1263____fontStyle_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "fontStyle"/* name */
	, &Text_get_fontStyle_m5457_MethodInfo/* get */
	, &Text_set_fontStyle_m5458_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_pixelsPerUnit_m5459_MethodInfo;
static PropertyInfo Text_t1263____pixelsPerUnit_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "pixelsPerUnit"/* name */
	, &Text_get_pixelsPerUnit_m5459_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_minWidth_m5468_MethodInfo;
static PropertyInfo Text_t1263____minWidth_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &Text_get_minWidth_m5468_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_preferredWidth_m5469_MethodInfo;
static PropertyInfo Text_t1263____preferredWidth_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &Text_get_preferredWidth_m5469_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_flexibleWidth_m5470_MethodInfo;
static PropertyInfo Text_t1263____flexibleWidth_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &Text_get_flexibleWidth_m5470_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_minHeight_m5471_MethodInfo;
static PropertyInfo Text_t1263____minHeight_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &Text_get_minHeight_m5471_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_preferredHeight_m5472_MethodInfo;
static PropertyInfo Text_t1263____preferredHeight_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &Text_get_preferredHeight_m5472_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_flexibleHeight_m5473_MethodInfo;
static PropertyInfo Text_t1263____flexibleHeight_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &Text_get_flexibleHeight_m5473_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Text_get_layoutPriority_m5474_MethodInfo;
static PropertyInfo Text_t1263____layoutPriority_PropertyInfo = 
{
	&Text_t1263_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &Text_get_layoutPriority_m5474_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Text_t1263_PropertyInfos[] =
{
	&Text_t1263____cachedTextGenerator_PropertyInfo,
	&Text_t1263____cachedTextGeneratorForLayout_PropertyInfo,
	&Text_t1263____defaultMaterial_PropertyInfo,
	&Text_t1263____mainTexture_PropertyInfo,
	&Text_t1263____font_PropertyInfo,
	&Text_t1263____text_PropertyInfo,
	&Text_t1263____supportRichText_PropertyInfo,
	&Text_t1263____resizeTextForBestFit_PropertyInfo,
	&Text_t1263____resizeTextMinSize_PropertyInfo,
	&Text_t1263____resizeTextMaxSize_PropertyInfo,
	&Text_t1263____alignment_PropertyInfo,
	&Text_t1263____fontSize_PropertyInfo,
	&Text_t1263____horizontalOverflow_PropertyInfo,
	&Text_t1263____verticalOverflow_PropertyInfo,
	&Text_t1263____lineSpacing_PropertyInfo,
	&Text_t1263____fontStyle_PropertyInfo,
	&Text_t1263____pixelsPerUnit_PropertyInfo,
	&Text_t1263____minWidth_PropertyInfo,
	&Text_t1263____preferredWidth_PropertyInfo,
	&Text_t1263____flexibleWidth_PropertyInfo,
	&Text_t1263____minHeight_PropertyInfo,
	&Text_t1263____preferredHeight_PropertyInfo,
	&Text_t1263____flexibleHeight_PropertyInfo,
	&Text_t1263____layoutPriority_PropertyInfo,
	NULL
};
extern MethodInfo Text_OnEnable_m5460_MethodInfo;
extern MethodInfo Text_OnDisable_m5461_MethodInfo;
extern MethodInfo Graphic_OnRectTransformDimensionsChange_m4931_MethodInfo;
extern MethodInfo Graphic_OnBeforeTransformParentChanged_m4932_MethodInfo;
extern MethodInfo MaskableGraphic_OnTransformParentChanged_m5183_MethodInfo;
extern MethodInfo Graphic_OnDidApplyAnimationProperties_m4951_MethodInfo;
extern MethodInfo Graphic_Rebuild_m4947_MethodInfo;
extern MethodInfo Graphic_UnityEngine_UI_ICanvasElement_get_transform_m4969_MethodInfo;
extern MethodInfo Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m4968_MethodInfo;
extern MethodInfo Graphic_SetAllDirty_m4927_MethodInfo;
extern MethodInfo Graphic_SetLayoutDirty_m4928_MethodInfo;
extern MethodInfo Graphic_SetVerticesDirty_m4929_MethodInfo;
extern MethodInfo MaskableGraphic_SetMaterialDirty_m5186_MethodInfo;
extern MethodInfo MaskableGraphic_get_material_m5178_MethodInfo;
extern MethodInfo MaskableGraphic_set_material_m5179_MethodInfo;
extern MethodInfo Graphic_get_materialForRendering_m4942_MethodInfo;
extern MethodInfo Text_UpdateGeometry_m5462_MethodInfo;
extern MethodInfo Graphic_UpdateMaterial_m4949_MethodInfo;
extern MethodInfo Text_OnFillVBO_m5465_MethodInfo;
extern MethodInfo Graphic_SetNativeSize_m4952_MethodInfo;
extern MethodInfo Graphic_Raycast_m4953_MethodInfo;
extern MethodInfo MaskableGraphic_ParentMaskStateChanged_m5184_MethodInfo;
extern MethodInfo Text_CalculateLayoutInputHorizontal_m5466_MethodInfo;
extern MethodInfo Text_CalculateLayoutInputVertical_m5467_MethodInfo;
static Il2CppMethodReference Text_t1263_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&Text_OnEnable_m5460_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&Text_OnDisable_m5461_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&Graphic_OnRectTransformDimensionsChange_m4931_MethodInfo,
	&Graphic_OnBeforeTransformParentChanged_m4932_MethodInfo,
	&MaskableGraphic_OnTransformParentChanged_m5183_MethodInfo,
	&Graphic_OnDidApplyAnimationProperties_m4951_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&Graphic_Rebuild_m4947_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m4969_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m4968_MethodInfo,
	&Graphic_SetAllDirty_m4927_MethodInfo,
	&Graphic_SetLayoutDirty_m4928_MethodInfo,
	&Graphic_SetVerticesDirty_m4929_MethodInfo,
	&MaskableGraphic_SetMaterialDirty_m5186_MethodInfo,
	&Text_get_defaultMaterial_m5432_MethodInfo,
	&MaskableGraphic_get_material_m5178_MethodInfo,
	&MaskableGraphic_set_material_m5179_MethodInfo,
	&Graphic_get_materialForRendering_m4942_MethodInfo,
	&Text_get_mainTexture_m5433_MethodInfo,
	&Graphic_Rebuild_m4947_MethodInfo,
	&Text_UpdateGeometry_m5462_MethodInfo,
	&Graphic_UpdateMaterial_m4949_MethodInfo,
	&Text_OnFillVBO_m5465_MethodInfo,
	&Graphic_SetNativeSize_m4952_MethodInfo,
	&Graphic_Raycast_m4953_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m4968_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m4969_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m5184_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m5184_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m5466_MethodInfo,
	&Text_CalculateLayoutInputVertical_m5467_MethodInfo,
	&Text_get_minWidth_m5468_MethodInfo,
	&Text_get_preferredWidth_m5469_MethodInfo,
	&Text_get_flexibleWidth_m5470_MethodInfo,
	&Text_get_minHeight_m5471_MethodInfo,
	&Text_get_preferredHeight_m5472_MethodInfo,
	&Text_get_flexibleHeight_m5473_MethodInfo,
	&Text_get_layoutPriority_m5474_MethodInfo,
	&Text_get_text_m5437_MethodInfo,
	&Text_set_text_m5438_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m5466_MethodInfo,
	&Text_CalculateLayoutInputVertical_m5467_MethodInfo,
	&Text_get_minWidth_m5468_MethodInfo,
	&Text_get_preferredWidth_m5469_MethodInfo,
	&Text_get_flexibleWidth_m5470_MethodInfo,
	&Text_get_minHeight_m5471_MethodInfo,
	&Text_get_preferredHeight_m5472_MethodInfo,
	&Text_get_flexibleHeight_m5473_MethodInfo,
	&Text_get_layoutPriority_m5474_MethodInfo,
};
static bool Text_t1263_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static const Il2CppType* Text_t1263_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t1367_0_0_0,
};
extern Il2CppType IMaskable_t1417_0_0_0;
static Il2CppInterfaceOffsetPair Text_t1263_InterfacesOffsets[] = 
{
	{ &IMaskable_t1417_0_0_0, 35},
	{ &ICanvasElement_t1360_0_0_0, 15},
	{ &ILayoutElement_t1367_0_0_0, 37},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Text_t1263_0_0_0;
extern Il2CppType Text_t1263_1_0_0;
extern Il2CppType MaskableGraphic_t1249_0_0_0;
struct Text_t1263;
const Il2CppTypeDefinitionMetadata Text_t1263_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Text_t1263_InterfacesTypeInfos/* implementedInterfaces */
	, Text_t1263_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t1249_0_0_0/* parent */
	, Text_t1263_VTable/* vtableMethods */
	, Text_t1263_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Text_t1263_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Text"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Text_t1263_MethodInfos/* methods */
	, Text_t1263_PropertyInfos/* properties */
	, Text_t1263_FieldInfos/* fields */
	, NULL/* events */
	, &Text_t1263_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 249/* custom_attributes_cache */
	, &Text_t1263_0_0_0/* byval_arg */
	, &Text_t1263_1_0_0/* this_arg */
	, &Text_t1263_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Text_t1263)/* instance_size */
	, sizeof (Text_t1263)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Text_t1263_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 24/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 57/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition
extern TypeInfo ToggleTransition_t1299_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
static MethodInfo* ToggleTransition_t1299_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo ToggleTransition_t1299____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &ToggleTransition_t1299_il2cpp_TypeInfo/* parent */
	, offsetof(ToggleTransition_t1299, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ToggleTransition_t1299_0_0_32854;
FieldInfo ToggleTransition_t1299____None_2_FieldInfo = 
{
	"None"/* name */
	, &ToggleTransition_t1299_0_0_32854/* type */
	, &ToggleTransition_t1299_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ToggleTransition_t1299_0_0_32854;
FieldInfo ToggleTransition_t1299____Fade_3_FieldInfo = 
{
	"Fade"/* name */
	, &ToggleTransition_t1299_0_0_32854/* type */
	, &ToggleTransition_t1299_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ToggleTransition_t1299_FieldInfos[] =
{
	&ToggleTransition_t1299____value___1_FieldInfo,
	&ToggleTransition_t1299____None_2_FieldInfo,
	&ToggleTransition_t1299____Fade_3_FieldInfo,
	NULL
};
static const int32_t ToggleTransition_t1299____None_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry ToggleTransition_t1299____None_2_DefaultValue = 
{
	&ToggleTransition_t1299____None_2_FieldInfo/* field */
	, { (char*)&ToggleTransition_t1299____None_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ToggleTransition_t1299____Fade_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ToggleTransition_t1299____Fade_3_DefaultValue = 
{
	&ToggleTransition_t1299____Fade_3_FieldInfo/* field */
	, { (char*)&ToggleTransition_t1299____Fade_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ToggleTransition_t1299_FieldDefaultValues[] = 
{
	&ToggleTransition_t1299____None_2_DefaultValue,
	&ToggleTransition_t1299____Fade_3_DefaultValue,
	NULL
};
static Il2CppMethodReference ToggleTransition_t1299_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool ToggleTransition_t1299_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ToggleTransition_t1299_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ToggleTransition_t1299_0_0_0;
extern Il2CppType ToggleTransition_t1299_1_0_0;
extern TypeInfo Toggle_t720_il2cpp_TypeInfo;
extern Il2CppType Toggle_t720_0_0_0;
const Il2CppTypeDefinitionMetadata ToggleTransition_t1299_DefinitionMetadata = 
{
	&Toggle_t720_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleTransition_t1299_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, ToggleTransition_t1299_VTable/* vtableMethods */
	, ToggleTransition_t1299_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ToggleTransition_t1299_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleTransition"/* name */
	, ""/* namespaze */
	, ToggleTransition_t1299_MethodInfos/* methods */
	, NULL/* properties */
	, ToggleTransition_t1299_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleTransition_t1299_0_0_0/* byval_arg */
	, &ToggleTransition_t1299_1_0_0/* this_arg */
	, &ToggleTransition_t1299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ToggleTransition_t1299_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleTransition_t1299)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ToggleTransition_t1299)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleEvent
extern TypeInfo ToggleEvent_t975_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
MethodInfo ToggleEvent__ctor_m5475_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleEvent__ctor_m5475/* method */
	, &ToggleEvent_t975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ToggleEvent_t975_MethodInfos[] =
{
	&ToggleEvent__ctor_m5475_MethodInfo,
	NULL
};
extern Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m6316_GenericMethod;
extern Il2CppGenericMethod UnityEvent_1_GetDelegate_m6317_GenericMethod;
static Il2CppMethodReference ToggleEvent_t975_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&UnityEventBase_ToString_m6293_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6294_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6295_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m6316_GenericMethod,
	&UnityEvent_1_GetDelegate_m6317_GenericMethod,
};
static bool ToggleEvent_t975_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair ToggleEvent_t975_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t1446_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ToggleEvent_t975_0_0_0;
extern Il2CppType ToggleEvent_t975_1_0_0;
extern Il2CppType UnityEvent_1_t1300_0_0_0;
struct ToggleEvent_t975;
const Il2CppTypeDefinitionMetadata ToggleEvent_t975_DefinitionMetadata = 
{
	&Toggle_t720_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleEvent_t975_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t1300_0_0_0/* parent */
	, ToggleEvent_t975_VTable/* vtableMethods */
	, ToggleEvent_t975_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ToggleEvent_t975_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleEvent"/* name */
	, ""/* namespaze */
	, ToggleEvent_t975_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ToggleEvent_t975_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleEvent_t975_0_0_0/* byval_arg */
	, &ToggleEvent_t975_1_0_0/* this_arg */
	, &ToggleEvent_t975_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleEvent_t975)/* instance_size */
	, sizeof (ToggleEvent_t975)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// Metadata Definition UnityEngine.UI.Toggle
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::.ctor()
MethodInfo Toggle__ctor_m5476_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Toggle__ctor_m5476/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ToggleGroup_t1301_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
MethodInfo Toggle_get_group_m5477_MethodInfo = 
{
	"get_group"/* name */
	, (methodPointerType)&Toggle_get_group_m5477/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &ToggleGroup_t1301_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ToggleGroup_t1301_0_0_0;
extern Il2CppType ToggleGroup_t1301_0_0_0;
static ParameterInfo Toggle_t720_Toggle_set_group_m5478_ParameterInfos[] = 
{
	{"value", 0, 134218302, 0, &ToggleGroup_t1301_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
MethodInfo Toggle_set_group_m5478_MethodInfo = 
{
	"set_group"/* name */
	, (methodPointerType)&Toggle_set_group_m5478/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Toggle_t720_Toggle_set_group_m5478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CanvasUpdate_t1216_0_0_0;
static ParameterInfo Toggle_t720_Toggle_Rebuild_m5479_ParameterInfos[] = 
{
	{"executing", 0, 134218303, 0, &CanvasUpdate_t1216_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
MethodInfo Toggle_Rebuild_m5479_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Toggle_Rebuild_m5479/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Toggle_t720_Toggle_Rebuild_m5479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 42/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnEnable()
MethodInfo Toggle_OnEnable_m5480_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Toggle_OnEnable_m5480/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnDisable()
MethodInfo Toggle_OnDisable_m5481_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Toggle_OnDisable_m5481/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ToggleGroup_t1301_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Toggle_t720_Toggle_SetToggleGroup_m5482_ParameterInfos[] = 
{
	{"newGroup", 0, 134218304, 0, &ToggleGroup_t1301_0_0_0},
	{"setMemberValue", 1, 134218305, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
MethodInfo Toggle_SetToggleGroup_m5482_MethodInfo = 
{
	"SetToggleGroup"/* name */
	, (methodPointerType)&Toggle_SetToggleGroup_m5482/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Toggle_t720_Toggle_SetToggleGroup_m5482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
MethodInfo Toggle_get_isOn_m5483_MethodInfo = 
{
	"get_isOn"/* name */
	, (methodPointerType)&Toggle_get_isOn_m5483/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Toggle_t720_Toggle_set_isOn_m4013_ParameterInfos[] = 
{
	{"value", 0, 134218306, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
MethodInfo Toggle_set_isOn_m4013_MethodInfo = 
{
	"set_isOn"/* name */
	, (methodPointerType)&Toggle_set_isOn_m4013/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Toggle_t720_Toggle_set_isOn_m4013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Toggle_t720_Toggle_Set_m5484_ParameterInfos[] = 
{
	{"value", 0, 134218307, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
MethodInfo Toggle_Set_m5484_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m5484/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Toggle_t720_Toggle_Set_m5484_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Toggle_t720_Toggle_Set_m5485_ParameterInfos[] = 
{
	{"value", 0, 134218308, 0, &Boolean_t203_0_0_0},
	{"sendCallback", 1, 134218309, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
MethodInfo Toggle_Set_m5485_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m5485/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236_SByte_t236/* invoker_method */
	, Toggle_t720_Toggle_Set_m5485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Toggle_t720_Toggle_PlayEffect_m5486_ParameterInfos[] = 
{
	{"instant", 0, 134218310, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
MethodInfo Toggle_PlayEffect_m5486_MethodInfo = 
{
	"PlayEffect"/* name */
	, (methodPointerType)&Toggle_PlayEffect_m5486/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Toggle_t720_Toggle_PlayEffect_m5486_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Start()
MethodInfo Toggle_Start_m5487_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Toggle_Start_m5487/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::InternalToggle()
MethodInfo Toggle_InternalToggle_m5488_MethodInfo = 
{
	"InternalToggle"/* name */
	, (methodPointerType)&Toggle_InternalToggle_m5488/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PointerEventData_t1191_0_0_0;
static ParameterInfo Toggle_t720_Toggle_OnPointerClick_m5489_ParameterInfos[] = 
{
	{"eventData", 0, 134218311, 0, &PointerEventData_t1191_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
MethodInfo Toggle_OnPointerClick_m5489_MethodInfo = 
{
	"OnPointerClick"/* name */
	, (methodPointerType)&Toggle_OnPointerClick_m5489/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Toggle_t720_Toggle_OnPointerClick_m5489_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseEventData_t1153_0_0_0;
static ParameterInfo Toggle_t720_Toggle_OnSubmit_m5490_ParameterInfos[] = 
{
	{"eventData", 0, 134218312, 0, &BaseEventData_t1153_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
MethodInfo Toggle_OnSubmit_m5490_MethodInfo = 
{
	"OnSubmit"/* name */
	, (methodPointerType)&Toggle_OnSubmit_m5490/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Toggle_t720_Toggle_OnSubmit_m5490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m5491_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m5491/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t809_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m5492_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m5492/* method */
	, &Toggle_t720_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t809_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Toggle_t720_MethodInfos[] =
{
	&Toggle__ctor_m5476_MethodInfo,
	&Toggle_get_group_m5477_MethodInfo,
	&Toggle_set_group_m5478_MethodInfo,
	&Toggle_Rebuild_m5479_MethodInfo,
	&Toggle_OnEnable_m5480_MethodInfo,
	&Toggle_OnDisable_m5481_MethodInfo,
	&Toggle_SetToggleGroup_m5482_MethodInfo,
	&Toggle_get_isOn_m5483_MethodInfo,
	&Toggle_set_isOn_m4013_MethodInfo,
	&Toggle_Set_m5484_MethodInfo,
	&Toggle_Set_m5485_MethodInfo,
	&Toggle_PlayEffect_m5486_MethodInfo,
	&Toggle_Start_m5487_MethodInfo,
	&Toggle_InternalToggle_m5488_MethodInfo,
	&Toggle_OnPointerClick_m5489_MethodInfo,
	&Toggle_OnSubmit_m5490_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m5491_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m5492_MethodInfo,
	NULL
};
extern Il2CppType ToggleTransition_t1299_0_0_6;
FieldInfo Toggle_t720____toggleTransition_16_FieldInfo = 
{
	"toggleTransition"/* name */
	, &ToggleTransition_t1299_0_0_6/* type */
	, &Toggle_t720_il2cpp_TypeInfo/* parent */
	, offsetof(Toggle_t720, ___toggleTransition_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Graphic_t1233_0_0_6;
FieldInfo Toggle_t720____graphic_17_FieldInfo = 
{
	"graphic"/* name */
	, &Graphic_t1233_0_0_6/* type */
	, &Toggle_t720_il2cpp_TypeInfo/* parent */
	, offsetof(Toggle_t720, ___graphic_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ToggleGroup_t1301_0_0_1;
FieldInfo Toggle_t720____m_Group_18_FieldInfo = 
{
	"m_Group"/* name */
	, &ToggleGroup_t1301_0_0_1/* type */
	, &Toggle_t720_il2cpp_TypeInfo/* parent */
	, offsetof(Toggle_t720, ___m_Group_18)/* offset */
	, 253/* custom_attributes_cache */

};
extern Il2CppType ToggleEvent_t975_0_0_6;
FieldInfo Toggle_t720____onValueChanged_19_FieldInfo = 
{
	"onValueChanged"/* name */
	, &ToggleEvent_t975_0_0_6/* type */
	, &Toggle_t720_il2cpp_TypeInfo/* parent */
	, offsetof(Toggle_t720, ___onValueChanged_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Toggle_t720____m_IsOn_20_FieldInfo = 
{
	"m_IsOn"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Toggle_t720_il2cpp_TypeInfo/* parent */
	, offsetof(Toggle_t720, ___m_IsOn_20)/* offset */
	, 254/* custom_attributes_cache */

};
static FieldInfo* Toggle_t720_FieldInfos[] =
{
	&Toggle_t720____toggleTransition_16_FieldInfo,
	&Toggle_t720____graphic_17_FieldInfo,
	&Toggle_t720____m_Group_18_FieldInfo,
	&Toggle_t720____onValueChanged_19_FieldInfo,
	&Toggle_t720____m_IsOn_20_FieldInfo,
	NULL
};
extern MethodInfo Toggle_get_group_m5477_MethodInfo;
extern MethodInfo Toggle_set_group_m5478_MethodInfo;
static PropertyInfo Toggle_t720____group_PropertyInfo = 
{
	&Toggle_t720_il2cpp_TypeInfo/* parent */
	, "group"/* name */
	, &Toggle_get_group_m5477_MethodInfo/* get */
	, &Toggle_set_group_m5478_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Toggle_get_isOn_m5483_MethodInfo;
extern MethodInfo Toggle_set_isOn_m4013_MethodInfo;
static PropertyInfo Toggle_t720____isOn_PropertyInfo = 
{
	&Toggle_t720_il2cpp_TypeInfo/* parent */
	, "isOn"/* name */
	, &Toggle_get_isOn_m5483_MethodInfo/* get */
	, &Toggle_set_isOn_m4013_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Toggle_t720_PropertyInfos[] =
{
	&Toggle_t720____group_PropertyInfo,
	&Toggle_t720____isOn_PropertyInfo,
	NULL
};
static const Il2CppType* Toggle_t720_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ToggleTransition_t1299_0_0_0,
	&ToggleEvent_t975_0_0_0,
};
extern MethodInfo Toggle_OnEnable_m5480_MethodInfo;
extern MethodInfo Toggle_Start_m5487_MethodInfo;
extern MethodInfo Toggle_OnDisable_m5481_MethodInfo;
extern MethodInfo Toggle_OnPointerClick_m5489_MethodInfo;
extern MethodInfo Toggle_OnSubmit_m5490_MethodInfo;
extern MethodInfo Toggle_Rebuild_m5479_MethodInfo;
extern MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m5492_MethodInfo;
extern MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m5491_MethodInfo;
static Il2CppMethodReference Toggle_t720_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&Selectable_Awake_m5339_MethodInfo,
	&Toggle_OnEnable_m5480_MethodInfo,
	&Toggle_Start_m5487_MethodInfo,
	&Toggle_OnDisable_m5481_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m5342_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m5340_MethodInfo,
	&Selectable_OnPointerEnter_m5368_MethodInfo,
	&Selectable_OnPointerExit_m5369_MethodInfo,
	&Selectable_OnPointerDown_m5366_MethodInfo,
	&Selectable_OnPointerUp_m5367_MethodInfo,
	&Selectable_OnSelect_m5370_MethodInfo,
	&Selectable_OnDeselect_m5371_MethodInfo,
	&Selectable_OnMove_m5356_MethodInfo,
	&Selectable_IsInteractable_m5341_MethodInfo,
	&Selectable_InstantClearState_m5347_MethodInfo,
	&Selectable_DoStateTransition_m5348_MethodInfo,
	&Selectable_FindSelectableOnLeft_m5352_MethodInfo,
	&Selectable_FindSelectableOnRight_m5353_MethodInfo,
	&Selectable_FindSelectableOnUp_m5354_MethodInfo,
	&Selectable_FindSelectableOnDown_m5355_MethodInfo,
	&Selectable_OnMove_m5356_MethodInfo,
	&Selectable_OnPointerDown_m5366_MethodInfo,
	&Selectable_OnPointerUp_m5367_MethodInfo,
	&Selectable_OnPointerEnter_m5368_MethodInfo,
	&Selectable_OnPointerExit_m5369_MethodInfo,
	&Selectable_OnSelect_m5370_MethodInfo,
	&Selectable_OnDeselect_m5371_MethodInfo,
	&Selectable_Select_m5372_MethodInfo,
	&Toggle_OnPointerClick_m5489_MethodInfo,
	&Toggle_OnSubmit_m5490_MethodInfo,
	&Toggle_Rebuild_m5479_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m5492_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m5491_MethodInfo,
	&Toggle_Rebuild_m5479_MethodInfo,
	&Toggle_OnPointerClick_m5489_MethodInfo,
	&Toggle_OnSubmit_m5490_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m5491_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m5492_MethodInfo,
};
static bool Toggle_t720_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IPointerClickHandler_t1344_0_0_0;
extern Il2CppType ISubmitHandler_t1355_0_0_0;
static const Il2CppType* Toggle_t720_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t1428_0_0_0,
	&IPointerClickHandler_t1344_0_0_0,
	&ISubmitHandler_t1355_0_0_0,
	&ICanvasElement_t1360_0_0_0,
};
static Il2CppInterfaceOffsetPair Toggle_t720_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t1428_0_0_0, 15},
	{ &IPointerEnterHandler_t1340_0_0_0, 15},
	{ &IPointerExitHandler_t1341_0_0_0, 16},
	{ &IPointerDownHandler_t1342_0_0_0, 17},
	{ &IPointerUpHandler_t1343_0_0_0, 18},
	{ &ISelectHandler_t1352_0_0_0, 19},
	{ &IDeselectHandler_t1353_0_0_0, 20},
	{ &IMoveHandler_t1354_0_0_0, 21},
	{ &IPointerClickHandler_t1344_0_0_0, 37},
	{ &ISubmitHandler_t1355_0_0_0, 38},
	{ &ICanvasElement_t1360_0_0_0, 39},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Toggle_t720_1_0_0;
struct Toggle_t720;
const Il2CppTypeDefinitionMetadata Toggle_t720_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Toggle_t720_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Toggle_t720_InterfacesTypeInfos/* implementedInterfaces */
	, Toggle_t720_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t1215_0_0_0/* parent */
	, Toggle_t720_VTable/* vtableMethods */
	, Toggle_t720_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Toggle_t720_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Toggle"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Toggle_t720_MethodInfos/* methods */
	, Toggle_t720_PropertyInfos/* properties */
	, Toggle_t720_FieldInfos/* fields */
	, NULL/* events */
	, &Toggle_t720_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 252/* custom_attributes_cache */
	, &Toggle_t720_0_0_0/* byval_arg */
	, &Toggle_t720_1_0_0/* this_arg */
	, &Toggle_t720_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Toggle_t720)/* instance_size */
	, sizeof (Toggle_t720)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 47/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// Metadata Definition UnityEngine.UI.ToggleGroup
extern TypeInfo ToggleGroup_t1301_il2cpp_TypeInfo;
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::.ctor()
MethodInfo ToggleGroup__ctor_m5493_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleGroup__ctor_m5493/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
MethodInfo ToggleGroup_get_allowSwitchOff_m5494_MethodInfo = 
{
	"get_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_get_allowSwitchOff_m5494/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ToggleGroup_t1301_ToggleGroup_set_allowSwitchOff_m5495_ParameterInfos[] = 
{
	{"value", 0, 134218313, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
MethodInfo ToggleGroup_set_allowSwitchOff_m5495_MethodInfo = 
{
	"set_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_set_allowSwitchOff_m5495/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ToggleGroup_t1301_ToggleGroup_set_allowSwitchOff_m5495_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Toggle_t720_0_0_0;
static ParameterInfo ToggleGroup_t1301_ToggleGroup_ValidateToggleIsInGroup_m5496_ParameterInfos[] = 
{
	{"toggle", 0, 134218314, 0, &Toggle_t720_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
MethodInfo ToggleGroup_ValidateToggleIsInGroup_m5496_MethodInfo = 
{
	"ValidateToggleIsInGroup"/* name */
	, (methodPointerType)&ToggleGroup_ValidateToggleIsInGroup_m5496/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ToggleGroup_t1301_ToggleGroup_ValidateToggleIsInGroup_m5496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Toggle_t720_0_0_0;
static ParameterInfo ToggleGroup_t1301_ToggleGroup_NotifyToggleOn_m5497_ParameterInfos[] = 
{
	{"toggle", 0, 134218315, 0, &Toggle_t720_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
MethodInfo ToggleGroup_NotifyToggleOn_m5497_MethodInfo = 
{
	"NotifyToggleOn"/* name */
	, (methodPointerType)&ToggleGroup_NotifyToggleOn_m5497/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ToggleGroup_t1301_ToggleGroup_NotifyToggleOn_m5497_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Toggle_t720_0_0_0;
static ParameterInfo ToggleGroup_t1301_ToggleGroup_UnregisterToggle_m5498_ParameterInfos[] = 
{
	{"toggle", 0, 134218316, 0, &Toggle_t720_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
MethodInfo ToggleGroup_UnregisterToggle_m5498_MethodInfo = 
{
	"UnregisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_UnregisterToggle_m5498/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ToggleGroup_t1301_ToggleGroup_UnregisterToggle_m5498_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Toggle_t720_0_0_0;
static ParameterInfo ToggleGroup_t1301_ToggleGroup_RegisterToggle_m5499_ParameterInfos[] = 
{
	{"toggle", 0, 134218317, 0, &Toggle_t720_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
MethodInfo ToggleGroup_RegisterToggle_m5499_MethodInfo = 
{
	"RegisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_RegisterToggle_m5499/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ToggleGroup_t1301_ToggleGroup_RegisterToggle_m5499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
MethodInfo ToggleGroup_AnyTogglesOn_m5500_MethodInfo = 
{
	"AnyTogglesOn"/* name */
	, (methodPointerType)&ToggleGroup_AnyTogglesOn_m5500/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerable_1_t1365_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
MethodInfo ToggleGroup_ActiveToggles_m5501_MethodInfo = 
{
	"ActiveToggles"/* name */
	, (methodPointerType)&ToggleGroup_ActiveToggles_m5501/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1365_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
MethodInfo ToggleGroup_SetAllTogglesOff_m5502_MethodInfo = 
{
	"SetAllTogglesOff"/* name */
	, (methodPointerType)&ToggleGroup_SetAllTogglesOff_m5502/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Toggle_t720_0_0_0;
static ParameterInfo ToggleGroup_t1301_ToggleGroup_U3CAnyTogglesOnU3Em__7_m5503_ParameterInfos[] = 
{
	{"x", 0, 134218318, 0, &Toggle_t720_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__7(UnityEngine.UI.Toggle)
MethodInfo ToggleGroup_U3CAnyTogglesOnU3Em__7_m5503_MethodInfo = 
{
	"<AnyTogglesOn>m__7"/* name */
	, (methodPointerType)&ToggleGroup_U3CAnyTogglesOnU3Em__7_m5503/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, ToggleGroup_t1301_ToggleGroup_U3CAnyTogglesOnU3Em__7_m5503_ParameterInfos/* parameters */
	, 259/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Toggle_t720_0_0_0;
static ParameterInfo ToggleGroup_t1301_ToggleGroup_U3CActiveTogglesU3Em__8_m5504_ParameterInfos[] = 
{
	{"x", 0, 134218319, 0, &Toggle_t720_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__8(UnityEngine.UI.Toggle)
MethodInfo ToggleGroup_U3CActiveTogglesU3Em__8_m5504_MethodInfo = 
{
	"<ActiveToggles>m__8"/* name */
	, (methodPointerType)&ToggleGroup_U3CActiveTogglesU3Em__8_m5504/* method */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, ToggleGroup_t1301_ToggleGroup_U3CActiveTogglesU3Em__8_m5504_ParameterInfos/* parameters */
	, 260/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ToggleGroup_t1301_MethodInfos[] =
{
	&ToggleGroup__ctor_m5493_MethodInfo,
	&ToggleGroup_get_allowSwitchOff_m5494_MethodInfo,
	&ToggleGroup_set_allowSwitchOff_m5495_MethodInfo,
	&ToggleGroup_ValidateToggleIsInGroup_m5496_MethodInfo,
	&ToggleGroup_NotifyToggleOn_m5497_MethodInfo,
	&ToggleGroup_UnregisterToggle_m5498_MethodInfo,
	&ToggleGroup_RegisterToggle_m5499_MethodInfo,
	&ToggleGroup_AnyTogglesOn_m5500_MethodInfo,
	&ToggleGroup_ActiveToggles_m5501_MethodInfo,
	&ToggleGroup_SetAllTogglesOff_m5502_MethodInfo,
	&ToggleGroup_U3CAnyTogglesOnU3Em__7_m5503_MethodInfo,
	&ToggleGroup_U3CActiveTogglesU3Em__8_m5504_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ToggleGroup_t1301____m_AllowSwitchOff_2_FieldInfo = 
{
	"m_AllowSwitchOff"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* parent */
	, offsetof(ToggleGroup_t1301, ___m_AllowSwitchOff_2)/* offset */
	, 256/* custom_attributes_cache */

};
extern Il2CppType List_1_t1302_0_0_1;
FieldInfo ToggleGroup_t1301____m_Toggles_3_FieldInfo = 
{
	"m_Toggles"/* name */
	, &List_1_t1302_0_0_1/* type */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* parent */
	, offsetof(ToggleGroup_t1301, ___m_Toggles_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Predicate_1_t1303_0_0_17;
FieldInfo ToggleGroup_t1301____U3CU3Ef__amU24cache2_4_FieldInfo = 
{
	"<>f__am$cache2"/* name */
	, &Predicate_1_t1303_0_0_17/* type */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* parent */
	, offsetof(ToggleGroup_t1301_StaticFields, ___U3CU3Ef__amU24cache2_4)/* offset */
	, 257/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1304_0_0_17;
FieldInfo ToggleGroup_t1301____U3CU3Ef__amU24cache3_5_FieldInfo = 
{
	"<>f__am$cache3"/* name */
	, &Func_2_t1304_0_0_17/* type */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* parent */
	, offsetof(ToggleGroup_t1301_StaticFields, ___U3CU3Ef__amU24cache3_5)/* offset */
	, 258/* custom_attributes_cache */

};
static FieldInfo* ToggleGroup_t1301_FieldInfos[] =
{
	&ToggleGroup_t1301____m_AllowSwitchOff_2_FieldInfo,
	&ToggleGroup_t1301____m_Toggles_3_FieldInfo,
	&ToggleGroup_t1301____U3CU3Ef__amU24cache2_4_FieldInfo,
	&ToggleGroup_t1301____U3CU3Ef__amU24cache3_5_FieldInfo,
	NULL
};
extern MethodInfo ToggleGroup_get_allowSwitchOff_m5494_MethodInfo;
extern MethodInfo ToggleGroup_set_allowSwitchOff_m5495_MethodInfo;
static PropertyInfo ToggleGroup_t1301____allowSwitchOff_PropertyInfo = 
{
	&ToggleGroup_t1301_il2cpp_TypeInfo/* parent */
	, "allowSwitchOff"/* name */
	, &ToggleGroup_get_allowSwitchOff_m5494_MethodInfo/* get */
	, &ToggleGroup_set_allowSwitchOff_m5495_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ToggleGroup_t1301_PropertyInfos[] =
{
	&ToggleGroup_t1301____allowSwitchOff_PropertyInfo,
	NULL
};
extern MethodInfo UIBehaviour_OnEnable_m4651_MethodInfo;
extern MethodInfo UIBehaviour_OnDisable_m4653_MethodInfo;
static Il2CppMethodReference ToggleGroup_t1301_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&UIBehaviour_OnEnable_m4651_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&UIBehaviour_OnDisable_m4653_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
};
static bool ToggleGroup_t1301_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ToggleGroup_t1301_1_0_0;
struct ToggleGroup_t1301;
const Il2CppTypeDefinitionMetadata ToggleGroup_t1301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, ToggleGroup_t1301_VTable/* vtableMethods */
	, ToggleGroup_t1301_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ToggleGroup_t1301_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ToggleGroup_t1301_MethodInfos/* methods */
	, ToggleGroup_t1301_PropertyInfos/* properties */
	, ToggleGroup_t1301_FieldInfos/* fields */
	, NULL/* events */
	, &ToggleGroup_t1301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 255/* custom_attributes_cache */
	, &ToggleGroup_t1301_0_0_0/* byval_arg */
	, &ToggleGroup_t1301_1_0_0/* this_arg */
	, &ToggleGroup_t1301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleGroup_t1301)/* instance_size */
	, sizeof (ToggleGroup_t1301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ToggleGroup_t1301_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode
extern TypeInfo AspectMode_t1305_il2cpp_TypeInfo;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
static MethodInfo* AspectMode_t1305_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo AspectMode_t1305____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &AspectMode_t1305_il2cpp_TypeInfo/* parent */
	, offsetof(AspectMode_t1305, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AspectMode_t1305_0_0_32854;
FieldInfo AspectMode_t1305____None_2_FieldInfo = 
{
	"None"/* name */
	, &AspectMode_t1305_0_0_32854/* type */
	, &AspectMode_t1305_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AspectMode_t1305_0_0_32854;
FieldInfo AspectMode_t1305____WidthControlsHeight_3_FieldInfo = 
{
	"WidthControlsHeight"/* name */
	, &AspectMode_t1305_0_0_32854/* type */
	, &AspectMode_t1305_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AspectMode_t1305_0_0_32854;
FieldInfo AspectMode_t1305____HeightControlsWidth_4_FieldInfo = 
{
	"HeightControlsWidth"/* name */
	, &AspectMode_t1305_0_0_32854/* type */
	, &AspectMode_t1305_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AspectMode_t1305_0_0_32854;
FieldInfo AspectMode_t1305____FitInParent_5_FieldInfo = 
{
	"FitInParent"/* name */
	, &AspectMode_t1305_0_0_32854/* type */
	, &AspectMode_t1305_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AspectMode_t1305_0_0_32854;
FieldInfo AspectMode_t1305____EnvelopeParent_6_FieldInfo = 
{
	"EnvelopeParent"/* name */
	, &AspectMode_t1305_0_0_32854/* type */
	, &AspectMode_t1305_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* AspectMode_t1305_FieldInfos[] =
{
	&AspectMode_t1305____value___1_FieldInfo,
	&AspectMode_t1305____None_2_FieldInfo,
	&AspectMode_t1305____WidthControlsHeight_3_FieldInfo,
	&AspectMode_t1305____HeightControlsWidth_4_FieldInfo,
	&AspectMode_t1305____FitInParent_5_FieldInfo,
	&AspectMode_t1305____EnvelopeParent_6_FieldInfo,
	NULL
};
static const int32_t AspectMode_t1305____None_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry AspectMode_t1305____None_2_DefaultValue = 
{
	&AspectMode_t1305____None_2_FieldInfo/* field */
	, { (char*)&AspectMode_t1305____None_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t AspectMode_t1305____WidthControlsHeight_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry AspectMode_t1305____WidthControlsHeight_3_DefaultValue = 
{
	&AspectMode_t1305____WidthControlsHeight_3_FieldInfo/* field */
	, { (char*)&AspectMode_t1305____WidthControlsHeight_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t AspectMode_t1305____HeightControlsWidth_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry AspectMode_t1305____HeightControlsWidth_4_DefaultValue = 
{
	&AspectMode_t1305____HeightControlsWidth_4_FieldInfo/* field */
	, { (char*)&AspectMode_t1305____HeightControlsWidth_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t AspectMode_t1305____FitInParent_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry AspectMode_t1305____FitInParent_5_DefaultValue = 
{
	&AspectMode_t1305____FitInParent_5_FieldInfo/* field */
	, { (char*)&AspectMode_t1305____FitInParent_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t AspectMode_t1305____EnvelopeParent_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry AspectMode_t1305____EnvelopeParent_6_DefaultValue = 
{
	&AspectMode_t1305____EnvelopeParent_6_FieldInfo/* field */
	, { (char*)&AspectMode_t1305____EnvelopeParent_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* AspectMode_t1305_FieldDefaultValues[] = 
{
	&AspectMode_t1305____None_2_DefaultValue,
	&AspectMode_t1305____WidthControlsHeight_3_DefaultValue,
	&AspectMode_t1305____HeightControlsWidth_4_DefaultValue,
	&AspectMode_t1305____FitInParent_5_DefaultValue,
	&AspectMode_t1305____EnvelopeParent_6_DefaultValue,
	NULL
};
static Il2CppMethodReference AspectMode_t1305_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool AspectMode_t1305_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AspectMode_t1305_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType AspectMode_t1305_0_0_0;
extern Il2CppType AspectMode_t1305_1_0_0;
extern TypeInfo AspectRatioFitter_t1306_il2cpp_TypeInfo;
extern Il2CppType AspectRatioFitter_t1306_0_0_0;
const Il2CppTypeDefinitionMetadata AspectMode_t1305_DefinitionMetadata = 
{
	&AspectRatioFitter_t1306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AspectMode_t1305_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, AspectMode_t1305_VTable/* vtableMethods */
	, AspectMode_t1305_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo AspectMode_t1305_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectMode"/* name */
	, ""/* namespaze */
	, AspectMode_t1305_MethodInfos/* methods */
	, NULL/* properties */
	, AspectMode_t1305_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AspectMode_t1305_0_0_0/* byval_arg */
	, &AspectMode_t1305_1_0_0/* this_arg */
	, &AspectMode_t1305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, AspectMode_t1305_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectMode_t1305)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AspectMode_t1305)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
MethodInfo AspectRatioFitter__ctor_m5505_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AspectRatioFitter__ctor_m5505/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AspectMode_t1305_0_0_0;
extern void* RuntimeInvoker_AspectMode_t1305 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
MethodInfo AspectRatioFitter_get_aspectMode_m5506_MethodInfo = 
{
	"get_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectMode_m5506/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &AspectMode_t1305_0_0_0/* return_type */
	, RuntimeInvoker_AspectMode_t1305/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AspectMode_t1305_0_0_0;
static ParameterInfo AspectRatioFitter_t1306_AspectRatioFitter_set_aspectMode_m5507_ParameterInfos[] = 
{
	{"value", 0, 134218320, 0, &AspectMode_t1305_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
MethodInfo AspectRatioFitter_set_aspectMode_m5507_MethodInfo = 
{
	"set_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectMode_m5507/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, AspectRatioFitter_t1306_AspectRatioFitter_set_aspectMode_m5507_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
MethodInfo AspectRatioFitter_get_aspectRatio_m5508_MethodInfo = 
{
	"get_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectRatio_m5508/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo AspectRatioFitter_t1306_AspectRatioFitter_set_aspectRatio_m5509_ParameterInfos[] = 
{
	{"value", 0, 134218321, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
MethodInfo AspectRatioFitter_set_aspectRatio_m5509_MethodInfo = 
{
	"set_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectRatio_m5509/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, AspectRatioFitter_t1306_AspectRatioFitter_set_aspectRatio_m5509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
MethodInfo AspectRatioFitter_get_rectTransform_m5510_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&AspectRatioFitter_get_rectTransform_m5510/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
MethodInfo AspectRatioFitter_OnEnable_m5511_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnEnable_m5511/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
MethodInfo AspectRatioFitter_OnDisable_m5512_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnDisable_m5512/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m5513_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&AspectRatioFitter_OnRectTransformDimensionsChange_m5513/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
MethodInfo AspectRatioFitter_UpdateRect_m5514_MethodInfo = 
{
	"UpdateRect"/* name */
	, (methodPointerType)&AspectRatioFitter_UpdateRect_m5514/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo AspectRatioFitter_t1306_AspectRatioFitter_GetSizeDeltaToProduceSize_m5515_ParameterInfos[] = 
{
	{"size", 0, 134218322, 0, &Single_t202_0_0_0},
	{"axis", 1, 134218323, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
MethodInfo AspectRatioFitter_GetSizeDeltaToProduceSize_m5515_MethodInfo = 
{
	"GetSizeDeltaToProduceSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetSizeDeltaToProduceSize_m5515/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Single_t202_Int32_t189/* invoker_method */
	, AspectRatioFitter_t1306_AspectRatioFitter_GetSizeDeltaToProduceSize_m5515_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
MethodInfo AspectRatioFitter_GetParentSize_m5516_MethodInfo = 
{
	"GetParentSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetParentSize_m5516/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
MethodInfo AspectRatioFitter_SetLayoutHorizontal_m5517_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutHorizontal_m5517/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
MethodInfo AspectRatioFitter_SetLayoutVertical_m5518_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutVertical_m5518/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
MethodInfo AspectRatioFitter_SetDirty_m5519_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&AspectRatioFitter_SetDirty_m5519/* method */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AspectRatioFitter_t1306_MethodInfos[] =
{
	&AspectRatioFitter__ctor_m5505_MethodInfo,
	&AspectRatioFitter_get_aspectMode_m5506_MethodInfo,
	&AspectRatioFitter_set_aspectMode_m5507_MethodInfo,
	&AspectRatioFitter_get_aspectRatio_m5508_MethodInfo,
	&AspectRatioFitter_set_aspectRatio_m5509_MethodInfo,
	&AspectRatioFitter_get_rectTransform_m5510_MethodInfo,
	&AspectRatioFitter_OnEnable_m5511_MethodInfo,
	&AspectRatioFitter_OnDisable_m5512_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m5513_MethodInfo,
	&AspectRatioFitter_UpdateRect_m5514_MethodInfo,
	&AspectRatioFitter_GetSizeDeltaToProduceSize_m5515_MethodInfo,
	&AspectRatioFitter_GetParentSize_m5516_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m5517_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m5518_MethodInfo,
	&AspectRatioFitter_SetDirty_m5519_MethodInfo,
	NULL
};
extern Il2CppType AspectMode_t1305_0_0_1;
FieldInfo AspectRatioFitter_t1306____m_AspectMode_2_FieldInfo = 
{
	"m_AspectMode"/* name */
	, &AspectMode_t1305_0_0_1/* type */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* parent */
	, offsetof(AspectRatioFitter_t1306, ___m_AspectMode_2)/* offset */
	, 262/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo AspectRatioFitter_t1306____m_AspectRatio_3_FieldInfo = 
{
	"m_AspectRatio"/* name */
	, &Single_t202_0_0_1/* type */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* parent */
	, offsetof(AspectRatioFitter_t1306, ___m_AspectRatio_3)/* offset */
	, 263/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_129;
FieldInfo AspectRatioFitter_t1306____m_Rect_4_FieldInfo = 
{
	"m_Rect"/* name */
	, &RectTransform_t1227_0_0_129/* type */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* parent */
	, offsetof(AspectRatioFitter_t1306, ___m_Rect_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DrivenRectTransformTracker_t1280_0_0_1;
FieldInfo AspectRatioFitter_t1306____m_Tracker_5_FieldInfo = 
{
	"m_Tracker"/* name */
	, &DrivenRectTransformTracker_t1280_0_0_1/* type */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* parent */
	, offsetof(AspectRatioFitter_t1306, ___m_Tracker_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* AspectRatioFitter_t1306_FieldInfos[] =
{
	&AspectRatioFitter_t1306____m_AspectMode_2_FieldInfo,
	&AspectRatioFitter_t1306____m_AspectRatio_3_FieldInfo,
	&AspectRatioFitter_t1306____m_Rect_4_FieldInfo,
	&AspectRatioFitter_t1306____m_Tracker_5_FieldInfo,
	NULL
};
extern MethodInfo AspectRatioFitter_get_aspectMode_m5506_MethodInfo;
extern MethodInfo AspectRatioFitter_set_aspectMode_m5507_MethodInfo;
static PropertyInfo AspectRatioFitter_t1306____aspectMode_PropertyInfo = 
{
	&AspectRatioFitter_t1306_il2cpp_TypeInfo/* parent */
	, "aspectMode"/* name */
	, &AspectRatioFitter_get_aspectMode_m5506_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectMode_m5507_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AspectRatioFitter_get_aspectRatio_m5508_MethodInfo;
extern MethodInfo AspectRatioFitter_set_aspectRatio_m5509_MethodInfo;
static PropertyInfo AspectRatioFitter_t1306____aspectRatio_PropertyInfo = 
{
	&AspectRatioFitter_t1306_il2cpp_TypeInfo/* parent */
	, "aspectRatio"/* name */
	, &AspectRatioFitter_get_aspectRatio_m5508_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectRatio_m5509_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AspectRatioFitter_get_rectTransform_m5510_MethodInfo;
static PropertyInfo AspectRatioFitter_t1306____rectTransform_PropertyInfo = 
{
	&AspectRatioFitter_t1306_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &AspectRatioFitter_get_rectTransform_m5510_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* AspectRatioFitter_t1306_PropertyInfos[] =
{
	&AspectRatioFitter_t1306____aspectMode_PropertyInfo,
	&AspectRatioFitter_t1306____aspectRatio_PropertyInfo,
	&AspectRatioFitter_t1306____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* AspectRatioFitter_t1306_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AspectMode_t1305_0_0_0,
};
extern MethodInfo AspectRatioFitter_OnEnable_m5511_MethodInfo;
extern MethodInfo AspectRatioFitter_OnDisable_m5512_MethodInfo;
extern MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m5513_MethodInfo;
extern MethodInfo AspectRatioFitter_SetLayoutHorizontal_m5517_MethodInfo;
extern MethodInfo AspectRatioFitter_SetLayoutVertical_m5518_MethodInfo;
static Il2CppMethodReference AspectRatioFitter_t1306_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&AspectRatioFitter_OnEnable_m5511_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&AspectRatioFitter_OnDisable_m5512_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m5513_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m5517_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m5518_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m5517_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m5518_MethodInfo,
};
static bool AspectRatioFitter_t1306_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ILayoutController_t1414_0_0_0;
extern Il2CppType ILayoutSelfController_t1415_0_0_0;
static const Il2CppType* AspectRatioFitter_t1306_InterfacesTypeInfos[] = 
{
	&ILayoutController_t1414_0_0_0,
	&ILayoutSelfController_t1415_0_0_0,
};
static Il2CppInterfaceOffsetPair AspectRatioFitter_t1306_InterfacesOffsets[] = 
{
	{ &ILayoutController_t1414_0_0_0, 15},
	{ &ILayoutSelfController_t1415_0_0_0, 17},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType AspectRatioFitter_t1306_1_0_0;
struct AspectRatioFitter_t1306;
const Il2CppTypeDefinitionMetadata AspectRatioFitter_t1306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AspectRatioFitter_t1306_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, AspectRatioFitter_t1306_InterfacesTypeInfos/* implementedInterfaces */
	, AspectRatioFitter_t1306_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, AspectRatioFitter_t1306_VTable/* vtableMethods */
	, AspectRatioFitter_t1306_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo AspectRatioFitter_t1306_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectRatioFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, AspectRatioFitter_t1306_MethodInfos/* methods */
	, AspectRatioFitter_t1306_PropertyInfos/* properties */
	, AspectRatioFitter_t1306_FieldInfos/* fields */
	, NULL/* events */
	, &AspectRatioFitter_t1306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 261/* custom_attributes_cache */
	, &AspectRatioFitter_t1306_0_0_0/* byval_arg */
	, &AspectRatioFitter_t1306_1_0_0/* this_arg */
	, &AspectRatioFitter_t1306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectRatioFitter_t1306)/* instance_size */
	, sizeof (AspectRatioFitter_t1306)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 19/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode
extern TypeInfo ScaleMode_t1307_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
static MethodInfo* ScaleMode_t1307_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo ScaleMode_t1307____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &ScaleMode_t1307_il2cpp_TypeInfo/* parent */
	, offsetof(ScaleMode_t1307, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScaleMode_t1307_0_0_32854;
FieldInfo ScaleMode_t1307____ConstantPixelSize_2_FieldInfo = 
{
	"ConstantPixelSize"/* name */
	, &ScaleMode_t1307_0_0_32854/* type */
	, &ScaleMode_t1307_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScaleMode_t1307_0_0_32854;
FieldInfo ScaleMode_t1307____ScaleWithScreenSize_3_FieldInfo = 
{
	"ScaleWithScreenSize"/* name */
	, &ScaleMode_t1307_0_0_32854/* type */
	, &ScaleMode_t1307_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScaleMode_t1307_0_0_32854;
FieldInfo ScaleMode_t1307____ConstantPhysicalSize_4_FieldInfo = 
{
	"ConstantPhysicalSize"/* name */
	, &ScaleMode_t1307_0_0_32854/* type */
	, &ScaleMode_t1307_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ScaleMode_t1307_FieldInfos[] =
{
	&ScaleMode_t1307____value___1_FieldInfo,
	&ScaleMode_t1307____ConstantPixelSize_2_FieldInfo,
	&ScaleMode_t1307____ScaleWithScreenSize_3_FieldInfo,
	&ScaleMode_t1307____ConstantPhysicalSize_4_FieldInfo,
	NULL
};
static const int32_t ScaleMode_t1307____ConstantPixelSize_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry ScaleMode_t1307____ConstantPixelSize_2_DefaultValue = 
{
	&ScaleMode_t1307____ConstantPixelSize_2_FieldInfo/* field */
	, { (char*)&ScaleMode_t1307____ConstantPixelSize_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScaleMode_t1307____ScaleWithScreenSize_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ScaleMode_t1307____ScaleWithScreenSize_3_DefaultValue = 
{
	&ScaleMode_t1307____ScaleWithScreenSize_3_FieldInfo/* field */
	, { (char*)&ScaleMode_t1307____ScaleWithScreenSize_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScaleMode_t1307____ConstantPhysicalSize_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ScaleMode_t1307____ConstantPhysicalSize_4_DefaultValue = 
{
	&ScaleMode_t1307____ConstantPhysicalSize_4_FieldInfo/* field */
	, { (char*)&ScaleMode_t1307____ConstantPhysicalSize_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ScaleMode_t1307_FieldDefaultValues[] = 
{
	&ScaleMode_t1307____ConstantPixelSize_2_DefaultValue,
	&ScaleMode_t1307____ScaleWithScreenSize_3_DefaultValue,
	&ScaleMode_t1307____ConstantPhysicalSize_4_DefaultValue,
	NULL
};
static Il2CppMethodReference ScaleMode_t1307_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool ScaleMode_t1307_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScaleMode_t1307_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ScaleMode_t1307_0_0_0;
extern Il2CppType ScaleMode_t1307_1_0_0;
extern TypeInfo CanvasScaler_t1310_il2cpp_TypeInfo;
extern Il2CppType CanvasScaler_t1310_0_0_0;
const Il2CppTypeDefinitionMetadata ScaleMode_t1307_DefinitionMetadata = 
{
	&CanvasScaler_t1310_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScaleMode_t1307_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, ScaleMode_t1307_VTable/* vtableMethods */
	, ScaleMode_t1307_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ScaleMode_t1307_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScaleMode"/* name */
	, ""/* namespaze */
	, ScaleMode_t1307_MethodInfos/* methods */
	, NULL/* properties */
	, ScaleMode_t1307_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScaleMode_t1307_0_0_0/* byval_arg */
	, &ScaleMode_t1307_1_0_0/* this_arg */
	, &ScaleMode_t1307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ScaleMode_t1307_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScaleMode_t1307)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScaleMode_t1307)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode
extern TypeInfo ScreenMatchMode_t1308_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
static MethodInfo* ScreenMatchMode_t1308_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo ScreenMatchMode_t1308____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &ScreenMatchMode_t1308_il2cpp_TypeInfo/* parent */
	, offsetof(ScreenMatchMode_t1308, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenMatchMode_t1308_0_0_32854;
FieldInfo ScreenMatchMode_t1308____MatchWidthOrHeight_2_FieldInfo = 
{
	"MatchWidthOrHeight"/* name */
	, &ScreenMatchMode_t1308_0_0_32854/* type */
	, &ScreenMatchMode_t1308_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenMatchMode_t1308_0_0_32854;
FieldInfo ScreenMatchMode_t1308____Expand_3_FieldInfo = 
{
	"Expand"/* name */
	, &ScreenMatchMode_t1308_0_0_32854/* type */
	, &ScreenMatchMode_t1308_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScreenMatchMode_t1308_0_0_32854;
FieldInfo ScreenMatchMode_t1308____Shrink_4_FieldInfo = 
{
	"Shrink"/* name */
	, &ScreenMatchMode_t1308_0_0_32854/* type */
	, &ScreenMatchMode_t1308_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ScreenMatchMode_t1308_FieldInfos[] =
{
	&ScreenMatchMode_t1308____value___1_FieldInfo,
	&ScreenMatchMode_t1308____MatchWidthOrHeight_2_FieldInfo,
	&ScreenMatchMode_t1308____Expand_3_FieldInfo,
	&ScreenMatchMode_t1308____Shrink_4_FieldInfo,
	NULL
};
static const int32_t ScreenMatchMode_t1308____MatchWidthOrHeight_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry ScreenMatchMode_t1308____MatchWidthOrHeight_2_DefaultValue = 
{
	&ScreenMatchMode_t1308____MatchWidthOrHeight_2_FieldInfo/* field */
	, { (char*)&ScreenMatchMode_t1308____MatchWidthOrHeight_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenMatchMode_t1308____Expand_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ScreenMatchMode_t1308____Expand_3_DefaultValue = 
{
	&ScreenMatchMode_t1308____Expand_3_FieldInfo/* field */
	, { (char*)&ScreenMatchMode_t1308____Expand_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ScreenMatchMode_t1308____Shrink_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ScreenMatchMode_t1308____Shrink_4_DefaultValue = 
{
	&ScreenMatchMode_t1308____Shrink_4_FieldInfo/* field */
	, { (char*)&ScreenMatchMode_t1308____Shrink_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ScreenMatchMode_t1308_FieldDefaultValues[] = 
{
	&ScreenMatchMode_t1308____MatchWidthOrHeight_2_DefaultValue,
	&ScreenMatchMode_t1308____Expand_3_DefaultValue,
	&ScreenMatchMode_t1308____Shrink_4_DefaultValue,
	NULL
};
static Il2CppMethodReference ScreenMatchMode_t1308_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool ScreenMatchMode_t1308_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenMatchMode_t1308_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ScreenMatchMode_t1308_0_0_0;
extern Il2CppType ScreenMatchMode_t1308_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenMatchMode_t1308_DefinitionMetadata = 
{
	&CanvasScaler_t1310_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenMatchMode_t1308_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, ScreenMatchMode_t1308_VTable/* vtableMethods */
	, ScreenMatchMode_t1308_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ScreenMatchMode_t1308_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenMatchMode"/* name */
	, ""/* namespaze */
	, ScreenMatchMode_t1308_MethodInfos/* methods */
	, NULL/* properties */
	, ScreenMatchMode_t1308_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenMatchMode_t1308_0_0_0/* byval_arg */
	, &ScreenMatchMode_t1308_1_0_0/* this_arg */
	, &ScreenMatchMode_t1308_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ScreenMatchMode_t1308_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenMatchMode_t1308)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenMatchMode_t1308)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/Unit
extern TypeInfo Unit_t1309_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
static MethodInfo* Unit_t1309_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Unit_t1309____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Unit_t1309_il2cpp_TypeInfo/* parent */
	, offsetof(Unit_t1309, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Unit_t1309_0_0_32854;
FieldInfo Unit_t1309____Centimeters_2_FieldInfo = 
{
	"Centimeters"/* name */
	, &Unit_t1309_0_0_32854/* type */
	, &Unit_t1309_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Unit_t1309_0_0_32854;
FieldInfo Unit_t1309____Millimeters_3_FieldInfo = 
{
	"Millimeters"/* name */
	, &Unit_t1309_0_0_32854/* type */
	, &Unit_t1309_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Unit_t1309_0_0_32854;
FieldInfo Unit_t1309____Inches_4_FieldInfo = 
{
	"Inches"/* name */
	, &Unit_t1309_0_0_32854/* type */
	, &Unit_t1309_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Unit_t1309_0_0_32854;
FieldInfo Unit_t1309____Points_5_FieldInfo = 
{
	"Points"/* name */
	, &Unit_t1309_0_0_32854/* type */
	, &Unit_t1309_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Unit_t1309_0_0_32854;
FieldInfo Unit_t1309____Picas_6_FieldInfo = 
{
	"Picas"/* name */
	, &Unit_t1309_0_0_32854/* type */
	, &Unit_t1309_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Unit_t1309_FieldInfos[] =
{
	&Unit_t1309____value___1_FieldInfo,
	&Unit_t1309____Centimeters_2_FieldInfo,
	&Unit_t1309____Millimeters_3_FieldInfo,
	&Unit_t1309____Inches_4_FieldInfo,
	&Unit_t1309____Points_5_FieldInfo,
	&Unit_t1309____Picas_6_FieldInfo,
	NULL
};
static const int32_t Unit_t1309____Centimeters_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Unit_t1309____Centimeters_2_DefaultValue = 
{
	&Unit_t1309____Centimeters_2_FieldInfo/* field */
	, { (char*)&Unit_t1309____Centimeters_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Unit_t1309____Millimeters_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Unit_t1309____Millimeters_3_DefaultValue = 
{
	&Unit_t1309____Millimeters_3_FieldInfo/* field */
	, { (char*)&Unit_t1309____Millimeters_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Unit_t1309____Inches_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry Unit_t1309____Inches_4_DefaultValue = 
{
	&Unit_t1309____Inches_4_FieldInfo/* field */
	, { (char*)&Unit_t1309____Inches_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Unit_t1309____Points_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry Unit_t1309____Points_5_DefaultValue = 
{
	&Unit_t1309____Points_5_FieldInfo/* field */
	, { (char*)&Unit_t1309____Points_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Unit_t1309____Picas_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry Unit_t1309____Picas_6_DefaultValue = 
{
	&Unit_t1309____Picas_6_FieldInfo/* field */
	, { (char*)&Unit_t1309____Picas_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Unit_t1309_FieldDefaultValues[] = 
{
	&Unit_t1309____Centimeters_2_DefaultValue,
	&Unit_t1309____Millimeters_3_DefaultValue,
	&Unit_t1309____Inches_4_DefaultValue,
	&Unit_t1309____Points_5_DefaultValue,
	&Unit_t1309____Picas_6_DefaultValue,
	NULL
};
static Il2CppMethodReference Unit_t1309_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Unit_t1309_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Unit_t1309_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Unit_t1309_0_0_0;
extern Il2CppType Unit_t1309_1_0_0;
const Il2CppTypeDefinitionMetadata Unit_t1309_DefinitionMetadata = 
{
	&CanvasScaler_t1310_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Unit_t1309_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Unit_t1309_VTable/* vtableMethods */
	, Unit_t1309_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Unit_t1309_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Unit"/* name */
	, ""/* namespaze */
	, Unit_t1309_MethodInfos/* methods */
	, NULL/* properties */
	, Unit_t1309_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Unit_t1309_0_0_0/* byval_arg */
	, &Unit_t1309_1_0_0/* this_arg */
	, &Unit_t1309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Unit_t1309_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Unit_t1309)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Unit_t1309)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// Metadata Definition UnityEngine.UI.CanvasScaler
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::.ctor()
MethodInfo CanvasScaler__ctor_m5520_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CanvasScaler__ctor_m5520/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScaleMode_t1307_0_0_0;
extern void* RuntimeInvoker_ScaleMode_t1307 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::get_uiScaleMode()
MethodInfo CanvasScaler_get_uiScaleMode_m5521_MethodInfo = 
{
	"get_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_get_uiScaleMode_m5521/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &ScaleMode_t1307_0_0_0/* return_type */
	, RuntimeInvoker_ScaleMode_t1307/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScaleMode_t1307_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_uiScaleMode_m5522_ParameterInfos[] = 
{
	{"value", 0, 134218324, 0, &ScaleMode_t1307_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_uiScaleMode(UnityEngine.UI.CanvasScaler/ScaleMode)
MethodInfo CanvasScaler_set_uiScaleMode_m5522_MethodInfo = 
{
	"set_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_set_uiScaleMode_m5522/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_uiScaleMode_m5522_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_referencePixelsPerUnit()
MethodInfo CanvasScaler_get_referencePixelsPerUnit_m5523_MethodInfo = 
{
	"get_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_referencePixelsPerUnit_m5523/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_referencePixelsPerUnit_m5524_ParameterInfos[] = 
{
	{"value", 0, 134218325, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referencePixelsPerUnit(System.Single)
MethodInfo CanvasScaler_set_referencePixelsPerUnit_m5524_MethodInfo = 
{
	"set_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_referencePixelsPerUnit_m5524/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_referencePixelsPerUnit_m5524_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_scaleFactor()
MethodInfo CanvasScaler_get_scaleFactor_m5525_MethodInfo = 
{
	"get_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_get_scaleFactor_m5525/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_scaleFactor_m5526_ParameterInfos[] = 
{
	{"value", 0, 134218326, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_scaleFactor(System.Single)
MethodInfo CanvasScaler_set_scaleFactor_m5526_MethodInfo = 
{
	"set_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_set_scaleFactor_m5526/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_scaleFactor_m5526_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::get_referenceResolution()
MethodInfo CanvasScaler_get_referenceResolution_m5527_MethodInfo = 
{
	"get_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_get_referenceResolution_m5527/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_referenceResolution_m5528_ParameterInfos[] = 
{
	{"value", 0, 134218327, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
MethodInfo CanvasScaler_set_referenceResolution_m5528_MethodInfo = 
{
	"set_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_set_referenceResolution_m5528/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_referenceResolution_m5528_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenMatchMode_t1308_0_0_0;
extern void* RuntimeInvoker_ScreenMatchMode_t1308 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::get_screenMatchMode()
MethodInfo CanvasScaler_get_screenMatchMode_m5529_MethodInfo = 
{
	"get_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_get_screenMatchMode_m5529/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &ScreenMatchMode_t1308_0_0_0/* return_type */
	, RuntimeInvoker_ScreenMatchMode_t1308/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenMatchMode_t1308_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_screenMatchMode_m5530_ParameterInfos[] = 
{
	{"value", 0, 134218328, 0, &ScreenMatchMode_t1308_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_screenMatchMode(UnityEngine.UI.CanvasScaler/ScreenMatchMode)
MethodInfo CanvasScaler_set_screenMatchMode_m5530_MethodInfo = 
{
	"set_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_set_screenMatchMode_m5530/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_screenMatchMode_m5530_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_matchWidthOrHeight()
MethodInfo CanvasScaler_get_matchWidthOrHeight_m5531_MethodInfo = 
{
	"get_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_get_matchWidthOrHeight_m5531/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_matchWidthOrHeight_m5532_ParameterInfos[] = 
{
	{"value", 0, 134218329, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_matchWidthOrHeight(System.Single)
MethodInfo CanvasScaler_set_matchWidthOrHeight_m5532_MethodInfo = 
{
	"set_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_set_matchWidthOrHeight_m5532/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_matchWidthOrHeight_m5532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Unit_t1309_0_0_0;
extern void* RuntimeInvoker_Unit_t1309 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::get_physicalUnit()
MethodInfo CanvasScaler_get_physicalUnit_m5533_MethodInfo = 
{
	"get_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_physicalUnit_m5533/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Unit_t1309_0_0_0/* return_type */
	, RuntimeInvoker_Unit_t1309/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Unit_t1309_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_physicalUnit_m5534_ParameterInfos[] = 
{
	{"value", 0, 134218330, 0, &Unit_t1309_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_physicalUnit(UnityEngine.UI.CanvasScaler/Unit)
MethodInfo CanvasScaler_set_physicalUnit_m5534_MethodInfo = 
{
	"set_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_physicalUnit_m5534/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_physicalUnit_m5534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_fallbackScreenDPI()
MethodInfo CanvasScaler_get_fallbackScreenDPI_m5535_MethodInfo = 
{
	"get_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_fallbackScreenDPI_m5535/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_fallbackScreenDPI_m5536_ParameterInfos[] = 
{
	{"value", 0, 134218331, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_fallbackScreenDPI(System.Single)
MethodInfo CanvasScaler_set_fallbackScreenDPI_m5536_MethodInfo = 
{
	"set_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_fallbackScreenDPI_m5536/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_fallbackScreenDPI_m5536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_defaultSpriteDPI()
MethodInfo CanvasScaler_get_defaultSpriteDPI_m5537_MethodInfo = 
{
	"get_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_defaultSpriteDPI_m5537/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_defaultSpriteDPI_m5538_ParameterInfos[] = 
{
	{"value", 0, 134218332, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_defaultSpriteDPI(System.Single)
MethodInfo CanvasScaler_set_defaultSpriteDPI_m5538_MethodInfo = 
{
	"set_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_defaultSpriteDPI_m5538/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_defaultSpriteDPI_m5538_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_dynamicPixelsPerUnit()
MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m5539_MethodInfo = 
{
	"get_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_dynamicPixelsPerUnit_m5539/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_set_dynamicPixelsPerUnit_m5540_ParameterInfos[] = 
{
	{"value", 0, 134218333, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_dynamicPixelsPerUnit(System.Single)
MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m5540_MethodInfo = 
{
	"set_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_dynamicPixelsPerUnit_m5540/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_set_dynamicPixelsPerUnit_m5540_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnEnable()
MethodInfo CanvasScaler_OnEnable_m5541_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&CanvasScaler_OnEnable_m5541/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnDisable()
MethodInfo CanvasScaler_OnDisable_m5542_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&CanvasScaler_OnDisable_m5542/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Update()
MethodInfo CanvasScaler_Update_m5543_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&CanvasScaler_Update_m5543/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Handle()
MethodInfo CanvasScaler_Handle_m5544_MethodInfo = 
{
	"Handle"/* name */
	, (methodPointerType)&CanvasScaler_Handle_m5544/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas()
MethodInfo CanvasScaler_HandleWorldCanvas_m5545_MethodInfo = 
{
	"HandleWorldCanvas"/* name */
	, (methodPointerType)&CanvasScaler_HandleWorldCanvas_m5545/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize()
MethodInfo CanvasScaler_HandleConstantPixelSize_m5546_MethodInfo = 
{
	"HandleConstantPixelSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPixelSize_m5546/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize()
MethodInfo CanvasScaler_HandleScaleWithScreenSize_m5547_MethodInfo = 
{
	"HandleScaleWithScreenSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleScaleWithScreenSize_m5547/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize()
MethodInfo CanvasScaler_HandleConstantPhysicalSize_m5548_MethodInfo = 
{
	"HandleConstantPhysicalSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPhysicalSize_m5548/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_SetScaleFactor_m5549_ParameterInfos[] = 
{
	{"scaleFactor", 0, 134218334, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetScaleFactor(System.Single)
MethodInfo CanvasScaler_SetScaleFactor_m5549_MethodInfo = 
{
	"SetScaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_SetScaleFactor_m5549/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_SetScaleFactor_m5549_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo CanvasScaler_t1310_CanvasScaler_SetReferencePixelsPerUnit_m5550_ParameterInfos[] = 
{
	{"referencePixelsPerUnit", 0, 134218335, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetReferencePixelsPerUnit(System.Single)
MethodInfo CanvasScaler_SetReferencePixelsPerUnit_m5550_MethodInfo = 
{
	"SetReferencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_SetReferencePixelsPerUnit_m5550/* method */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, CanvasScaler_t1310_CanvasScaler_SetReferencePixelsPerUnit_m5550_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CanvasScaler_t1310_MethodInfos[] =
{
	&CanvasScaler__ctor_m5520_MethodInfo,
	&CanvasScaler_get_uiScaleMode_m5521_MethodInfo,
	&CanvasScaler_set_uiScaleMode_m5522_MethodInfo,
	&CanvasScaler_get_referencePixelsPerUnit_m5523_MethodInfo,
	&CanvasScaler_set_referencePixelsPerUnit_m5524_MethodInfo,
	&CanvasScaler_get_scaleFactor_m5525_MethodInfo,
	&CanvasScaler_set_scaleFactor_m5526_MethodInfo,
	&CanvasScaler_get_referenceResolution_m5527_MethodInfo,
	&CanvasScaler_set_referenceResolution_m5528_MethodInfo,
	&CanvasScaler_get_screenMatchMode_m5529_MethodInfo,
	&CanvasScaler_set_screenMatchMode_m5530_MethodInfo,
	&CanvasScaler_get_matchWidthOrHeight_m5531_MethodInfo,
	&CanvasScaler_set_matchWidthOrHeight_m5532_MethodInfo,
	&CanvasScaler_get_physicalUnit_m5533_MethodInfo,
	&CanvasScaler_set_physicalUnit_m5534_MethodInfo,
	&CanvasScaler_get_fallbackScreenDPI_m5535_MethodInfo,
	&CanvasScaler_set_fallbackScreenDPI_m5536_MethodInfo,
	&CanvasScaler_get_defaultSpriteDPI_m5537_MethodInfo,
	&CanvasScaler_set_defaultSpriteDPI_m5538_MethodInfo,
	&CanvasScaler_get_dynamicPixelsPerUnit_m5539_MethodInfo,
	&CanvasScaler_set_dynamicPixelsPerUnit_m5540_MethodInfo,
	&CanvasScaler_OnEnable_m5541_MethodInfo,
	&CanvasScaler_OnDisable_m5542_MethodInfo,
	&CanvasScaler_Update_m5543_MethodInfo,
	&CanvasScaler_Handle_m5544_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m5545_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m5546_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m5547_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m5548_MethodInfo,
	&CanvasScaler_SetScaleFactor_m5549_MethodInfo,
	&CanvasScaler_SetReferencePixelsPerUnit_m5550_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_32849;
FieldInfo CanvasScaler_t1310____kLogBase_2_FieldInfo = 
{
	"kLogBase"/* name */
	, &Single_t202_0_0_32849/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ScaleMode_t1307_0_0_1;
FieldInfo CanvasScaler_t1310____m_UiScaleMode_3_FieldInfo = 
{
	"m_UiScaleMode"/* name */
	, &ScaleMode_t1307_0_0_1/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_UiScaleMode_3)/* offset */
	, 265/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_4;
FieldInfo CanvasScaler_t1310____m_ReferencePixelsPerUnit_4_FieldInfo = 
{
	"m_ReferencePixelsPerUnit"/* name */
	, &Single_t202_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_ReferencePixelsPerUnit_4)/* offset */
	, 266/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_4;
FieldInfo CanvasScaler_t1310____m_ScaleFactor_5_FieldInfo = 
{
	"m_ScaleFactor"/* name */
	, &Single_t202_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_ScaleFactor_5)/* offset */
	, 267/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_4;
FieldInfo CanvasScaler_t1310____m_ReferenceResolution_6_FieldInfo = 
{
	"m_ReferenceResolution"/* name */
	, &Vector2_t739_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_ReferenceResolution_6)/* offset */
	, 268/* custom_attributes_cache */

};
extern Il2CppType ScreenMatchMode_t1308_0_0_4;
FieldInfo CanvasScaler_t1310____m_ScreenMatchMode_7_FieldInfo = 
{
	"m_ScreenMatchMode"/* name */
	, &ScreenMatchMode_t1308_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_ScreenMatchMode_7)/* offset */
	, 269/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_4;
FieldInfo CanvasScaler_t1310____m_MatchWidthOrHeight_8_FieldInfo = 
{
	"m_MatchWidthOrHeight"/* name */
	, &Single_t202_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_MatchWidthOrHeight_8)/* offset */
	, 270/* custom_attributes_cache */

};
extern Il2CppType Unit_t1309_0_0_4;
FieldInfo CanvasScaler_t1310____m_PhysicalUnit_9_FieldInfo = 
{
	"m_PhysicalUnit"/* name */
	, &Unit_t1309_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_PhysicalUnit_9)/* offset */
	, 271/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_4;
FieldInfo CanvasScaler_t1310____m_FallbackScreenDPI_10_FieldInfo = 
{
	"m_FallbackScreenDPI"/* name */
	, &Single_t202_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_FallbackScreenDPI_10)/* offset */
	, 272/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_4;
FieldInfo CanvasScaler_t1310____m_DefaultSpriteDPI_11_FieldInfo = 
{
	"m_DefaultSpriteDPI"/* name */
	, &Single_t202_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_DefaultSpriteDPI_11)/* offset */
	, 273/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_4;
FieldInfo CanvasScaler_t1310____m_DynamicPixelsPerUnit_12_FieldInfo = 
{
	"m_DynamicPixelsPerUnit"/* name */
	, &Single_t202_0_0_4/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_DynamicPixelsPerUnit_12)/* offset */
	, 274/* custom_attributes_cache */

};
extern Il2CppType Canvas_t1229_0_0_1;
FieldInfo CanvasScaler_t1310____m_Canvas_13_FieldInfo = 
{
	"m_Canvas"/* name */
	, &Canvas_t1229_0_0_1/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_Canvas_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_129;
FieldInfo CanvasScaler_t1310____m_PrevScaleFactor_14_FieldInfo = 
{
	"m_PrevScaleFactor"/* name */
	, &Single_t202_0_0_129/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_PrevScaleFactor_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_129;
FieldInfo CanvasScaler_t1310____m_PrevReferencePixelsPerUnit_15_FieldInfo = 
{
	"m_PrevReferencePixelsPerUnit"/* name */
	, &Single_t202_0_0_129/* type */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasScaler_t1310, ___m_PrevReferencePixelsPerUnit_15)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CanvasScaler_t1310_FieldInfos[] =
{
	&CanvasScaler_t1310____kLogBase_2_FieldInfo,
	&CanvasScaler_t1310____m_UiScaleMode_3_FieldInfo,
	&CanvasScaler_t1310____m_ReferencePixelsPerUnit_4_FieldInfo,
	&CanvasScaler_t1310____m_ScaleFactor_5_FieldInfo,
	&CanvasScaler_t1310____m_ReferenceResolution_6_FieldInfo,
	&CanvasScaler_t1310____m_ScreenMatchMode_7_FieldInfo,
	&CanvasScaler_t1310____m_MatchWidthOrHeight_8_FieldInfo,
	&CanvasScaler_t1310____m_PhysicalUnit_9_FieldInfo,
	&CanvasScaler_t1310____m_FallbackScreenDPI_10_FieldInfo,
	&CanvasScaler_t1310____m_DefaultSpriteDPI_11_FieldInfo,
	&CanvasScaler_t1310____m_DynamicPixelsPerUnit_12_FieldInfo,
	&CanvasScaler_t1310____m_Canvas_13_FieldInfo,
	&CanvasScaler_t1310____m_PrevScaleFactor_14_FieldInfo,
	&CanvasScaler_t1310____m_PrevReferencePixelsPerUnit_15_FieldInfo,
	NULL
};
static const float CanvasScaler_t1310____kLogBase_2_DefaultValueData = 2.0f;
static Il2CppFieldDefaultValueEntry CanvasScaler_t1310____kLogBase_2_DefaultValue = 
{
	&CanvasScaler_t1310____kLogBase_2_FieldInfo/* field */
	, { (char*)&CanvasScaler_t1310____kLogBase_2_DefaultValueData, &Single_t202_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* CanvasScaler_t1310_FieldDefaultValues[] = 
{
	&CanvasScaler_t1310____kLogBase_2_DefaultValue,
	NULL
};
extern MethodInfo CanvasScaler_get_uiScaleMode_m5521_MethodInfo;
extern MethodInfo CanvasScaler_set_uiScaleMode_m5522_MethodInfo;
static PropertyInfo CanvasScaler_t1310____uiScaleMode_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "uiScaleMode"/* name */
	, &CanvasScaler_get_uiScaleMode_m5521_MethodInfo/* get */
	, &CanvasScaler_set_uiScaleMode_m5522_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_referencePixelsPerUnit_m5523_MethodInfo;
extern MethodInfo CanvasScaler_set_referencePixelsPerUnit_m5524_MethodInfo;
static PropertyInfo CanvasScaler_t1310____referencePixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "referencePixelsPerUnit"/* name */
	, &CanvasScaler_get_referencePixelsPerUnit_m5523_MethodInfo/* get */
	, &CanvasScaler_set_referencePixelsPerUnit_m5524_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_scaleFactor_m5525_MethodInfo;
extern MethodInfo CanvasScaler_set_scaleFactor_m5526_MethodInfo;
static PropertyInfo CanvasScaler_t1310____scaleFactor_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "scaleFactor"/* name */
	, &CanvasScaler_get_scaleFactor_m5525_MethodInfo/* get */
	, &CanvasScaler_set_scaleFactor_m5526_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_referenceResolution_m5527_MethodInfo;
extern MethodInfo CanvasScaler_set_referenceResolution_m5528_MethodInfo;
static PropertyInfo CanvasScaler_t1310____referenceResolution_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "referenceResolution"/* name */
	, &CanvasScaler_get_referenceResolution_m5527_MethodInfo/* get */
	, &CanvasScaler_set_referenceResolution_m5528_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_screenMatchMode_m5529_MethodInfo;
extern MethodInfo CanvasScaler_set_screenMatchMode_m5530_MethodInfo;
static PropertyInfo CanvasScaler_t1310____screenMatchMode_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "screenMatchMode"/* name */
	, &CanvasScaler_get_screenMatchMode_m5529_MethodInfo/* get */
	, &CanvasScaler_set_screenMatchMode_m5530_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_matchWidthOrHeight_m5531_MethodInfo;
extern MethodInfo CanvasScaler_set_matchWidthOrHeight_m5532_MethodInfo;
static PropertyInfo CanvasScaler_t1310____matchWidthOrHeight_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "matchWidthOrHeight"/* name */
	, &CanvasScaler_get_matchWidthOrHeight_m5531_MethodInfo/* get */
	, &CanvasScaler_set_matchWidthOrHeight_m5532_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_physicalUnit_m5533_MethodInfo;
extern MethodInfo CanvasScaler_set_physicalUnit_m5534_MethodInfo;
static PropertyInfo CanvasScaler_t1310____physicalUnit_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "physicalUnit"/* name */
	, &CanvasScaler_get_physicalUnit_m5533_MethodInfo/* get */
	, &CanvasScaler_set_physicalUnit_m5534_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_fallbackScreenDPI_m5535_MethodInfo;
extern MethodInfo CanvasScaler_set_fallbackScreenDPI_m5536_MethodInfo;
static PropertyInfo CanvasScaler_t1310____fallbackScreenDPI_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "fallbackScreenDPI"/* name */
	, &CanvasScaler_get_fallbackScreenDPI_m5535_MethodInfo/* get */
	, &CanvasScaler_set_fallbackScreenDPI_m5536_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_defaultSpriteDPI_m5537_MethodInfo;
extern MethodInfo CanvasScaler_set_defaultSpriteDPI_m5538_MethodInfo;
static PropertyInfo CanvasScaler_t1310____defaultSpriteDPI_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "defaultSpriteDPI"/* name */
	, &CanvasScaler_get_defaultSpriteDPI_m5537_MethodInfo/* get */
	, &CanvasScaler_set_defaultSpriteDPI_m5538_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m5539_MethodInfo;
extern MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m5540_MethodInfo;
static PropertyInfo CanvasScaler_t1310____dynamicPixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t1310_il2cpp_TypeInfo/* parent */
	, "dynamicPixelsPerUnit"/* name */
	, &CanvasScaler_get_dynamicPixelsPerUnit_m5539_MethodInfo/* get */
	, &CanvasScaler_set_dynamicPixelsPerUnit_m5540_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* CanvasScaler_t1310_PropertyInfos[] =
{
	&CanvasScaler_t1310____uiScaleMode_PropertyInfo,
	&CanvasScaler_t1310____referencePixelsPerUnit_PropertyInfo,
	&CanvasScaler_t1310____scaleFactor_PropertyInfo,
	&CanvasScaler_t1310____referenceResolution_PropertyInfo,
	&CanvasScaler_t1310____screenMatchMode_PropertyInfo,
	&CanvasScaler_t1310____matchWidthOrHeight_PropertyInfo,
	&CanvasScaler_t1310____physicalUnit_PropertyInfo,
	&CanvasScaler_t1310____fallbackScreenDPI_PropertyInfo,
	&CanvasScaler_t1310____defaultSpriteDPI_PropertyInfo,
	&CanvasScaler_t1310____dynamicPixelsPerUnit_PropertyInfo,
	NULL
};
static const Il2CppType* CanvasScaler_t1310_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ScaleMode_t1307_0_0_0,
	&ScreenMatchMode_t1308_0_0_0,
	&Unit_t1309_0_0_0,
};
extern MethodInfo CanvasScaler_OnEnable_m5541_MethodInfo;
extern MethodInfo CanvasScaler_OnDisable_m5542_MethodInfo;
extern MethodInfo CanvasScaler_Update_m5543_MethodInfo;
extern MethodInfo CanvasScaler_Handle_m5544_MethodInfo;
extern MethodInfo CanvasScaler_HandleWorldCanvas_m5545_MethodInfo;
extern MethodInfo CanvasScaler_HandleConstantPixelSize_m5546_MethodInfo;
extern MethodInfo CanvasScaler_HandleScaleWithScreenSize_m5547_MethodInfo;
extern MethodInfo CanvasScaler_HandleConstantPhysicalSize_m5548_MethodInfo;
static Il2CppMethodReference CanvasScaler_t1310_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&CanvasScaler_OnEnable_m5541_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&CanvasScaler_OnDisable_m5542_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&CanvasScaler_Update_m5543_MethodInfo,
	&CanvasScaler_Handle_m5544_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m5545_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m5546_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m5547_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m5548_MethodInfo,
};
static bool CanvasScaler_t1310_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType CanvasScaler_t1310_1_0_0;
struct CanvasScaler_t1310;
const Il2CppTypeDefinitionMetadata CanvasScaler_t1310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CanvasScaler_t1310_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, CanvasScaler_t1310_VTable/* vtableMethods */
	, CanvasScaler_t1310_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CanvasScaler_t1310_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasScaler"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasScaler_t1310_MethodInfos/* methods */
	, CanvasScaler_t1310_PropertyInfos/* properties */
	, CanvasScaler_t1310_FieldInfos/* fields */
	, NULL/* events */
	, &CanvasScaler_t1310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 264/* custom_attributes_cache */
	, &CanvasScaler_t1310_0_0_0/* byval_arg */
	, &CanvasScaler_t1310_1_0_0/* this_arg */
	, &CanvasScaler_t1310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, CanvasScaler_t1310_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasScaler_t1310)/* instance_size */
	, sizeof (CanvasScaler_t1310)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 10/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 21/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode
extern TypeInfo FitMode_t1311_il2cpp_TypeInfo;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
static MethodInfo* FitMode_t1311_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo FitMode_t1311____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &FitMode_t1311_il2cpp_TypeInfo/* parent */
	, offsetof(FitMode_t1311, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FitMode_t1311_0_0_32854;
FieldInfo FitMode_t1311____Unconstrained_2_FieldInfo = 
{
	"Unconstrained"/* name */
	, &FitMode_t1311_0_0_32854/* type */
	, &FitMode_t1311_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FitMode_t1311_0_0_32854;
FieldInfo FitMode_t1311____MinSize_3_FieldInfo = 
{
	"MinSize"/* name */
	, &FitMode_t1311_0_0_32854/* type */
	, &FitMode_t1311_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FitMode_t1311_0_0_32854;
FieldInfo FitMode_t1311____PreferredSize_4_FieldInfo = 
{
	"PreferredSize"/* name */
	, &FitMode_t1311_0_0_32854/* type */
	, &FitMode_t1311_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* FitMode_t1311_FieldInfos[] =
{
	&FitMode_t1311____value___1_FieldInfo,
	&FitMode_t1311____Unconstrained_2_FieldInfo,
	&FitMode_t1311____MinSize_3_FieldInfo,
	&FitMode_t1311____PreferredSize_4_FieldInfo,
	NULL
};
static const int32_t FitMode_t1311____Unconstrained_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry FitMode_t1311____Unconstrained_2_DefaultValue = 
{
	&FitMode_t1311____Unconstrained_2_FieldInfo/* field */
	, { (char*)&FitMode_t1311____Unconstrained_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t FitMode_t1311____MinSize_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry FitMode_t1311____MinSize_3_DefaultValue = 
{
	&FitMode_t1311____MinSize_3_FieldInfo/* field */
	, { (char*)&FitMode_t1311____MinSize_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t FitMode_t1311____PreferredSize_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry FitMode_t1311____PreferredSize_4_DefaultValue = 
{
	&FitMode_t1311____PreferredSize_4_FieldInfo/* field */
	, { (char*)&FitMode_t1311____PreferredSize_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* FitMode_t1311_FieldDefaultValues[] = 
{
	&FitMode_t1311____Unconstrained_2_DefaultValue,
	&FitMode_t1311____MinSize_3_DefaultValue,
	&FitMode_t1311____PreferredSize_4_DefaultValue,
	NULL
};
static Il2CppMethodReference FitMode_t1311_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool FitMode_t1311_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FitMode_t1311_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType FitMode_t1311_0_0_0;
extern Il2CppType FitMode_t1311_1_0_0;
extern TypeInfo ContentSizeFitter_t1312_il2cpp_TypeInfo;
extern Il2CppType ContentSizeFitter_t1312_0_0_0;
const Il2CppTypeDefinitionMetadata FitMode_t1311_DefinitionMetadata = 
{
	&ContentSizeFitter_t1312_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FitMode_t1311_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, FitMode_t1311_VTable/* vtableMethods */
	, FitMode_t1311_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo FitMode_t1311_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FitMode"/* name */
	, ""/* namespaze */
	, FitMode_t1311_MethodInfos/* methods */
	, NULL/* properties */
	, FitMode_t1311_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FitMode_t1311_0_0_0/* byval_arg */
	, &FitMode_t1311_1_0_0/* this_arg */
	, &FitMode_t1311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, FitMode_t1311_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FitMode_t1311)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FitMode_t1311)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
MethodInfo ContentSizeFitter__ctor_m5551_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContentSizeFitter__ctor_m5551/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FitMode_t1311_0_0_0;
extern void* RuntimeInvoker_FitMode_t1311 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
MethodInfo ContentSizeFitter_get_horizontalFit_m5552_MethodInfo = 
{
	"get_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_horizontalFit_m5552/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t1311_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t1311/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FitMode_t1311_0_0_0;
static ParameterInfo ContentSizeFitter_t1312_ContentSizeFitter_set_horizontalFit_m5553_ParameterInfos[] = 
{
	{"value", 0, 134218336, 0, &FitMode_t1311_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
MethodInfo ContentSizeFitter_set_horizontalFit_m5553_MethodInfo = 
{
	"set_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_horizontalFit_m5553/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ContentSizeFitter_t1312_ContentSizeFitter_set_horizontalFit_m5553_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FitMode_t1311_0_0_0;
extern void* RuntimeInvoker_FitMode_t1311 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
MethodInfo ContentSizeFitter_get_verticalFit_m5554_MethodInfo = 
{
	"get_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_verticalFit_m5554/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t1311_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t1311/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FitMode_t1311_0_0_0;
static ParameterInfo ContentSizeFitter_t1312_ContentSizeFitter_set_verticalFit_m5555_ParameterInfos[] = 
{
	{"value", 0, 134218337, 0, &FitMode_t1311_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
MethodInfo ContentSizeFitter_set_verticalFit_m5555_MethodInfo = 
{
	"set_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_verticalFit_m5555/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ContentSizeFitter_t1312_ContentSizeFitter_set_verticalFit_m5555_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
MethodInfo ContentSizeFitter_get_rectTransform_m5556_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&ContentSizeFitter_get_rectTransform_m5556/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
MethodInfo ContentSizeFitter_OnEnable_m5557_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnEnable_m5557/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
MethodInfo ContentSizeFitter_OnDisable_m5558_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnDisable_m5558/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m5559_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&ContentSizeFitter_OnRectTransformDimensionsChange_m5559/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ContentSizeFitter_t1312_ContentSizeFitter_HandleSelfFittingAlongAxis_m5560_ParameterInfos[] = 
{
	{"axis", 0, 134218338, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
MethodInfo ContentSizeFitter_HandleSelfFittingAlongAxis_m5560_MethodInfo = 
{
	"HandleSelfFittingAlongAxis"/* name */
	, (methodPointerType)&ContentSizeFitter_HandleSelfFittingAlongAxis_m5560/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ContentSizeFitter_t1312_ContentSizeFitter_HandleSelfFittingAlongAxis_m5560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
MethodInfo ContentSizeFitter_SetLayoutHorizontal_m5561_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutHorizontal_m5561/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
MethodInfo ContentSizeFitter_SetLayoutVertical_m5562_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutVertical_m5562/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
MethodInfo ContentSizeFitter_SetDirty_m5563_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&ContentSizeFitter_SetDirty_m5563/* method */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ContentSizeFitter_t1312_MethodInfos[] =
{
	&ContentSizeFitter__ctor_m5551_MethodInfo,
	&ContentSizeFitter_get_horizontalFit_m5552_MethodInfo,
	&ContentSizeFitter_set_horizontalFit_m5553_MethodInfo,
	&ContentSizeFitter_get_verticalFit_m5554_MethodInfo,
	&ContentSizeFitter_set_verticalFit_m5555_MethodInfo,
	&ContentSizeFitter_get_rectTransform_m5556_MethodInfo,
	&ContentSizeFitter_OnEnable_m5557_MethodInfo,
	&ContentSizeFitter_OnDisable_m5558_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m5559_MethodInfo,
	&ContentSizeFitter_HandleSelfFittingAlongAxis_m5560_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m5561_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m5562_MethodInfo,
	&ContentSizeFitter_SetDirty_m5563_MethodInfo,
	NULL
};
extern Il2CppType FitMode_t1311_0_0_4;
FieldInfo ContentSizeFitter_t1312____m_HorizontalFit_2_FieldInfo = 
{
	"m_HorizontalFit"/* name */
	, &FitMode_t1311_0_0_4/* type */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* parent */
	, offsetof(ContentSizeFitter_t1312, ___m_HorizontalFit_2)/* offset */
	, 276/* custom_attributes_cache */

};
extern Il2CppType FitMode_t1311_0_0_4;
FieldInfo ContentSizeFitter_t1312____m_VerticalFit_3_FieldInfo = 
{
	"m_VerticalFit"/* name */
	, &FitMode_t1311_0_0_4/* type */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* parent */
	, offsetof(ContentSizeFitter_t1312, ___m_VerticalFit_3)/* offset */
	, 277/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_129;
FieldInfo ContentSizeFitter_t1312____m_Rect_4_FieldInfo = 
{
	"m_Rect"/* name */
	, &RectTransform_t1227_0_0_129/* type */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* parent */
	, offsetof(ContentSizeFitter_t1312, ___m_Rect_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DrivenRectTransformTracker_t1280_0_0_1;
FieldInfo ContentSizeFitter_t1312____m_Tracker_5_FieldInfo = 
{
	"m_Tracker"/* name */
	, &DrivenRectTransformTracker_t1280_0_0_1/* type */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* parent */
	, offsetof(ContentSizeFitter_t1312, ___m_Tracker_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ContentSizeFitter_t1312_FieldInfos[] =
{
	&ContentSizeFitter_t1312____m_HorizontalFit_2_FieldInfo,
	&ContentSizeFitter_t1312____m_VerticalFit_3_FieldInfo,
	&ContentSizeFitter_t1312____m_Rect_4_FieldInfo,
	&ContentSizeFitter_t1312____m_Tracker_5_FieldInfo,
	NULL
};
extern MethodInfo ContentSizeFitter_get_horizontalFit_m5552_MethodInfo;
extern MethodInfo ContentSizeFitter_set_horizontalFit_m5553_MethodInfo;
static PropertyInfo ContentSizeFitter_t1312____horizontalFit_PropertyInfo = 
{
	&ContentSizeFitter_t1312_il2cpp_TypeInfo/* parent */
	, "horizontalFit"/* name */
	, &ContentSizeFitter_get_horizontalFit_m5552_MethodInfo/* get */
	, &ContentSizeFitter_set_horizontalFit_m5553_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ContentSizeFitter_get_verticalFit_m5554_MethodInfo;
extern MethodInfo ContentSizeFitter_set_verticalFit_m5555_MethodInfo;
static PropertyInfo ContentSizeFitter_t1312____verticalFit_PropertyInfo = 
{
	&ContentSizeFitter_t1312_il2cpp_TypeInfo/* parent */
	, "verticalFit"/* name */
	, &ContentSizeFitter_get_verticalFit_m5554_MethodInfo/* get */
	, &ContentSizeFitter_set_verticalFit_m5555_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ContentSizeFitter_get_rectTransform_m5556_MethodInfo;
static PropertyInfo ContentSizeFitter_t1312____rectTransform_PropertyInfo = 
{
	&ContentSizeFitter_t1312_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &ContentSizeFitter_get_rectTransform_m5556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ContentSizeFitter_t1312_PropertyInfos[] =
{
	&ContentSizeFitter_t1312____horizontalFit_PropertyInfo,
	&ContentSizeFitter_t1312____verticalFit_PropertyInfo,
	&ContentSizeFitter_t1312____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* ContentSizeFitter_t1312_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FitMode_t1311_0_0_0,
};
extern MethodInfo ContentSizeFitter_OnEnable_m5557_MethodInfo;
extern MethodInfo ContentSizeFitter_OnDisable_m5558_MethodInfo;
extern MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m5559_MethodInfo;
extern MethodInfo ContentSizeFitter_SetLayoutHorizontal_m5561_MethodInfo;
extern MethodInfo ContentSizeFitter_SetLayoutVertical_m5562_MethodInfo;
static Il2CppMethodReference ContentSizeFitter_t1312_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&ContentSizeFitter_OnEnable_m5557_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&ContentSizeFitter_OnDisable_m5558_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m5559_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m5561_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m5562_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m5561_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m5562_MethodInfo,
};
static bool ContentSizeFitter_t1312_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContentSizeFitter_t1312_InterfacesTypeInfos[] = 
{
	&ILayoutController_t1414_0_0_0,
	&ILayoutSelfController_t1415_0_0_0,
};
static Il2CppInterfaceOffsetPair ContentSizeFitter_t1312_InterfacesOffsets[] = 
{
	{ &ILayoutController_t1414_0_0_0, 15},
	{ &ILayoutSelfController_t1415_0_0_0, 17},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ContentSizeFitter_t1312_1_0_0;
struct ContentSizeFitter_t1312;
const Il2CppTypeDefinitionMetadata ContentSizeFitter_t1312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ContentSizeFitter_t1312_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ContentSizeFitter_t1312_InterfacesTypeInfos/* implementedInterfaces */
	, ContentSizeFitter_t1312_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, ContentSizeFitter_t1312_VTable/* vtableMethods */
	, ContentSizeFitter_t1312_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ContentSizeFitter_t1312_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentSizeFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ContentSizeFitter_t1312_MethodInfos/* methods */
	, ContentSizeFitter_t1312_PropertyInfos/* properties */
	, ContentSizeFitter_t1312_FieldInfos/* fields */
	, NULL/* events */
	, &ContentSizeFitter_t1312_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 275/* custom_attributes_cache */
	, &ContentSizeFitter_t1312_0_0_0/* byval_arg */
	, &ContentSizeFitter_t1312_1_0_0/* this_arg */
	, &ContentSizeFitter_t1312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentSizeFitter_t1312)/* instance_size */
	, sizeof (ContentSizeFitter_t1312)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 19/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner
extern TypeInfo Corner_t1313_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
static MethodInfo* Corner_t1313_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Corner_t1313____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Corner_t1313_il2cpp_TypeInfo/* parent */
	, offsetof(Corner_t1313, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Corner_t1313_0_0_32854;
FieldInfo Corner_t1313____UpperLeft_2_FieldInfo = 
{
	"UpperLeft"/* name */
	, &Corner_t1313_0_0_32854/* type */
	, &Corner_t1313_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Corner_t1313_0_0_32854;
FieldInfo Corner_t1313____UpperRight_3_FieldInfo = 
{
	"UpperRight"/* name */
	, &Corner_t1313_0_0_32854/* type */
	, &Corner_t1313_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Corner_t1313_0_0_32854;
FieldInfo Corner_t1313____LowerLeft_4_FieldInfo = 
{
	"LowerLeft"/* name */
	, &Corner_t1313_0_0_32854/* type */
	, &Corner_t1313_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Corner_t1313_0_0_32854;
FieldInfo Corner_t1313____LowerRight_5_FieldInfo = 
{
	"LowerRight"/* name */
	, &Corner_t1313_0_0_32854/* type */
	, &Corner_t1313_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Corner_t1313_FieldInfos[] =
{
	&Corner_t1313____value___1_FieldInfo,
	&Corner_t1313____UpperLeft_2_FieldInfo,
	&Corner_t1313____UpperRight_3_FieldInfo,
	&Corner_t1313____LowerLeft_4_FieldInfo,
	&Corner_t1313____LowerRight_5_FieldInfo,
	NULL
};
static const int32_t Corner_t1313____UpperLeft_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Corner_t1313____UpperLeft_2_DefaultValue = 
{
	&Corner_t1313____UpperLeft_2_FieldInfo/* field */
	, { (char*)&Corner_t1313____UpperLeft_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Corner_t1313____UpperRight_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Corner_t1313____UpperRight_3_DefaultValue = 
{
	&Corner_t1313____UpperRight_3_FieldInfo/* field */
	, { (char*)&Corner_t1313____UpperRight_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Corner_t1313____LowerLeft_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry Corner_t1313____LowerLeft_4_DefaultValue = 
{
	&Corner_t1313____LowerLeft_4_FieldInfo/* field */
	, { (char*)&Corner_t1313____LowerLeft_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Corner_t1313____LowerRight_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry Corner_t1313____LowerRight_5_DefaultValue = 
{
	&Corner_t1313____LowerRight_5_FieldInfo/* field */
	, { (char*)&Corner_t1313____LowerRight_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Corner_t1313_FieldDefaultValues[] = 
{
	&Corner_t1313____UpperLeft_2_DefaultValue,
	&Corner_t1313____UpperRight_3_DefaultValue,
	&Corner_t1313____LowerLeft_4_DefaultValue,
	&Corner_t1313____LowerRight_5_DefaultValue,
	NULL
};
static Il2CppMethodReference Corner_t1313_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Corner_t1313_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Corner_t1313_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Corner_t1313_0_0_0;
extern Il2CppType Corner_t1313_1_0_0;
extern TypeInfo GridLayoutGroup_t1316_il2cpp_TypeInfo;
extern Il2CppType GridLayoutGroup_t1316_0_0_0;
const Il2CppTypeDefinitionMetadata Corner_t1313_DefinitionMetadata = 
{
	&GridLayoutGroup_t1316_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Corner_t1313_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Corner_t1313_VTable/* vtableMethods */
	, Corner_t1313_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Corner_t1313_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Corner"/* name */
	, ""/* namespaze */
	, Corner_t1313_MethodInfos/* methods */
	, NULL/* properties */
	, Corner_t1313_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Corner_t1313_0_0_0/* byval_arg */
	, &Corner_t1313_1_0_0/* this_arg */
	, &Corner_t1313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Corner_t1313_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Corner_t1313)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Corner_t1313)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis
extern TypeInfo Axis_t1314_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
static MethodInfo* Axis_t1314_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Axis_t1314____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Axis_t1314_il2cpp_TypeInfo/* parent */
	, offsetof(Axis_t1314, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Axis_t1314_0_0_32854;
FieldInfo Axis_t1314____Horizontal_2_FieldInfo = 
{
	"Horizontal"/* name */
	, &Axis_t1314_0_0_32854/* type */
	, &Axis_t1314_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Axis_t1314_0_0_32854;
FieldInfo Axis_t1314____Vertical_3_FieldInfo = 
{
	"Vertical"/* name */
	, &Axis_t1314_0_0_32854/* type */
	, &Axis_t1314_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Axis_t1314_FieldInfos[] =
{
	&Axis_t1314____value___1_FieldInfo,
	&Axis_t1314____Horizontal_2_FieldInfo,
	&Axis_t1314____Vertical_3_FieldInfo,
	NULL
};
static const int32_t Axis_t1314____Horizontal_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Axis_t1314____Horizontal_2_DefaultValue = 
{
	&Axis_t1314____Horizontal_2_FieldInfo/* field */
	, { (char*)&Axis_t1314____Horizontal_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Axis_t1314____Vertical_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Axis_t1314____Vertical_3_DefaultValue = 
{
	&Axis_t1314____Vertical_3_FieldInfo/* field */
	, { (char*)&Axis_t1314____Vertical_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Axis_t1314_FieldDefaultValues[] = 
{
	&Axis_t1314____Horizontal_2_DefaultValue,
	&Axis_t1314____Vertical_3_DefaultValue,
	NULL
};
static Il2CppMethodReference Axis_t1314_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Axis_t1314_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t1314_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Axis_t1314_0_0_0;
extern Il2CppType Axis_t1314_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t1314_DefinitionMetadata = 
{
	&GridLayoutGroup_t1316_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t1314_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Axis_t1314_VTable/* vtableMethods */
	, Axis_t1314_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Axis_t1314_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t1314_MethodInfos/* methods */
	, NULL/* properties */
	, Axis_t1314_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t1314_0_0_0/* byval_arg */
	, &Axis_t1314_1_0_0/* this_arg */
	, &Axis_t1314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Axis_t1314_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t1314)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t1314)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint
extern TypeInfo Constraint_t1315_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
static MethodInfo* Constraint_t1315_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Constraint_t1315____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Constraint_t1315_il2cpp_TypeInfo/* parent */
	, offsetof(Constraint_t1315, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Constraint_t1315_0_0_32854;
FieldInfo Constraint_t1315____Flexible_2_FieldInfo = 
{
	"Flexible"/* name */
	, &Constraint_t1315_0_0_32854/* type */
	, &Constraint_t1315_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Constraint_t1315_0_0_32854;
FieldInfo Constraint_t1315____FixedColumnCount_3_FieldInfo = 
{
	"FixedColumnCount"/* name */
	, &Constraint_t1315_0_0_32854/* type */
	, &Constraint_t1315_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Constraint_t1315_0_0_32854;
FieldInfo Constraint_t1315____FixedRowCount_4_FieldInfo = 
{
	"FixedRowCount"/* name */
	, &Constraint_t1315_0_0_32854/* type */
	, &Constraint_t1315_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Constraint_t1315_FieldInfos[] =
{
	&Constraint_t1315____value___1_FieldInfo,
	&Constraint_t1315____Flexible_2_FieldInfo,
	&Constraint_t1315____FixedColumnCount_3_FieldInfo,
	&Constraint_t1315____FixedRowCount_4_FieldInfo,
	NULL
};
static const int32_t Constraint_t1315____Flexible_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Constraint_t1315____Flexible_2_DefaultValue = 
{
	&Constraint_t1315____Flexible_2_FieldInfo/* field */
	, { (char*)&Constraint_t1315____Flexible_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Constraint_t1315____FixedColumnCount_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Constraint_t1315____FixedColumnCount_3_DefaultValue = 
{
	&Constraint_t1315____FixedColumnCount_3_FieldInfo/* field */
	, { (char*)&Constraint_t1315____FixedColumnCount_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Constraint_t1315____FixedRowCount_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry Constraint_t1315____FixedRowCount_4_DefaultValue = 
{
	&Constraint_t1315____FixedRowCount_4_FieldInfo/* field */
	, { (char*)&Constraint_t1315____FixedRowCount_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Constraint_t1315_FieldDefaultValues[] = 
{
	&Constraint_t1315____Flexible_2_DefaultValue,
	&Constraint_t1315____FixedColumnCount_3_DefaultValue,
	&Constraint_t1315____FixedRowCount_4_DefaultValue,
	NULL
};
static Il2CppMethodReference Constraint_t1315_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Constraint_t1315_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Constraint_t1315_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Constraint_t1315_0_0_0;
extern Il2CppType Constraint_t1315_1_0_0;
const Il2CppTypeDefinitionMetadata Constraint_t1315_DefinitionMetadata = 
{
	&GridLayoutGroup_t1316_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Constraint_t1315_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Constraint_t1315_VTable/* vtableMethods */
	, Constraint_t1315_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Constraint_t1315_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Constraint"/* name */
	, ""/* namespaze */
	, Constraint_t1315_MethodInfos/* methods */
	, NULL/* properties */
	, Constraint_t1315_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Constraint_t1315_0_0_0/* byval_arg */
	, &Constraint_t1315_1_0_0/* this_arg */
	, &Constraint_t1315_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Constraint_t1315_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Constraint_t1315)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Constraint_t1315)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
MethodInfo GridLayoutGroup__ctor_m5564_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GridLayoutGroup__ctor_m5564/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Corner_t1313_0_0_0;
extern void* RuntimeInvoker_Corner_t1313 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
MethodInfo GridLayoutGroup_get_startCorner_m5565_MethodInfo = 
{
	"get_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startCorner_m5565/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Corner_t1313_0_0_0/* return_type */
	, RuntimeInvoker_Corner_t1313/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Corner_t1313_0_0_0;
static ParameterInfo GridLayoutGroup_t1316_GridLayoutGroup_set_startCorner_m5566_ParameterInfos[] = 
{
	{"value", 0, 134218339, 0, &Corner_t1313_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
MethodInfo GridLayoutGroup_set_startCorner_m5566_MethodInfo = 
{
	"set_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startCorner_m5566/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, GridLayoutGroup_t1316_GridLayoutGroup_set_startCorner_m5566_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Axis_t1314_0_0_0;
extern void* RuntimeInvoker_Axis_t1314 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
MethodInfo GridLayoutGroup_get_startAxis_m5567_MethodInfo = 
{
	"get_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startAxis_m5567/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t1314_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t1314/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Axis_t1314_0_0_0;
static ParameterInfo GridLayoutGroup_t1316_GridLayoutGroup_set_startAxis_m5568_ParameterInfos[] = 
{
	{"value", 0, 134218340, 0, &Axis_t1314_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
MethodInfo GridLayoutGroup_set_startAxis_m5568_MethodInfo = 
{
	"set_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startAxis_m5568/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, GridLayoutGroup_t1316_GridLayoutGroup_set_startAxis_m5568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
MethodInfo GridLayoutGroup_get_cellSize_m5569_MethodInfo = 
{
	"get_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_get_cellSize_m5569/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo GridLayoutGroup_t1316_GridLayoutGroup_set_cellSize_m5570_ParameterInfos[] = 
{
	{"value", 0, 134218341, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
MethodInfo GridLayoutGroup_set_cellSize_m5570_MethodInfo = 
{
	"set_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_set_cellSize_m5570/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, GridLayoutGroup_t1316_GridLayoutGroup_set_cellSize_m5570_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
MethodInfo GridLayoutGroup_get_spacing_m5571_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_get_spacing_m5571/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo GridLayoutGroup_t1316_GridLayoutGroup_set_spacing_m5572_ParameterInfos[] = 
{
	{"value", 0, 134218342, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
MethodInfo GridLayoutGroup_set_spacing_m5572_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_set_spacing_m5572/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, GridLayoutGroup_t1316_GridLayoutGroup_set_spacing_m5572_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Constraint_t1315_0_0_0;
extern void* RuntimeInvoker_Constraint_t1315 (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
MethodInfo GridLayoutGroup_get_constraint_m5573_MethodInfo = 
{
	"get_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraint_m5573/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Constraint_t1315_0_0_0/* return_type */
	, RuntimeInvoker_Constraint_t1315/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Constraint_t1315_0_0_0;
static ParameterInfo GridLayoutGroup_t1316_GridLayoutGroup_set_constraint_m5574_ParameterInfos[] = 
{
	{"value", 0, 134218343, 0, &Constraint_t1315_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
MethodInfo GridLayoutGroup_set_constraint_m5574_MethodInfo = 
{
	"set_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraint_m5574/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, GridLayoutGroup_t1316_GridLayoutGroup_set_constraint_m5574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
MethodInfo GridLayoutGroup_get_constraintCount_m5575_MethodInfo = 
{
	"get_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraintCount_m5575/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo GridLayoutGroup_t1316_GridLayoutGroup_set_constraintCount_m5576_ParameterInfos[] = 
{
	{"value", 0, 134218344, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
MethodInfo GridLayoutGroup_set_constraintCount_m5576_MethodInfo = 
{
	"set_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraintCount_m5576/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, GridLayoutGroup_t1316_GridLayoutGroup_set_constraintCount_m5576_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m5577_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputHorizontal_m5577/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m5578_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputVertical_m5578/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
MethodInfo GridLayoutGroup_SetLayoutHorizontal_m5579_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutHorizontal_m5579/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
MethodInfo GridLayoutGroup_SetLayoutVertical_m5580_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutVertical_m5580/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo GridLayoutGroup_t1316_GridLayoutGroup_SetCellsAlongAxis_m5581_ParameterInfos[] = 
{
	{"axis", 0, 134218345, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
MethodInfo GridLayoutGroup_SetCellsAlongAxis_m5581_MethodInfo = 
{
	"SetCellsAlongAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_SetCellsAlongAxis_m5581/* method */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, GridLayoutGroup_t1316_GridLayoutGroup_SetCellsAlongAxis_m5581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GridLayoutGroup_t1316_MethodInfos[] =
{
	&GridLayoutGroup__ctor_m5564_MethodInfo,
	&GridLayoutGroup_get_startCorner_m5565_MethodInfo,
	&GridLayoutGroup_set_startCorner_m5566_MethodInfo,
	&GridLayoutGroup_get_startAxis_m5567_MethodInfo,
	&GridLayoutGroup_set_startAxis_m5568_MethodInfo,
	&GridLayoutGroup_get_cellSize_m5569_MethodInfo,
	&GridLayoutGroup_set_cellSize_m5570_MethodInfo,
	&GridLayoutGroup_get_spacing_m5571_MethodInfo,
	&GridLayoutGroup_set_spacing_m5572_MethodInfo,
	&GridLayoutGroup_get_constraint_m5573_MethodInfo,
	&GridLayoutGroup_set_constraint_m5574_MethodInfo,
	&GridLayoutGroup_get_constraintCount_m5575_MethodInfo,
	&GridLayoutGroup_set_constraintCount_m5576_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m5577_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m5578_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m5579_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m5580_MethodInfo,
	&GridLayoutGroup_SetCellsAlongAxis_m5581_MethodInfo,
	NULL
};
extern Il2CppType Corner_t1313_0_0_4;
FieldInfo GridLayoutGroup_t1316____m_StartCorner_10_FieldInfo = 
{
	"m_StartCorner"/* name */
	, &Corner_t1313_0_0_4/* type */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, offsetof(GridLayoutGroup_t1316, ___m_StartCorner_10)/* offset */
	, 279/* custom_attributes_cache */

};
extern Il2CppType Axis_t1314_0_0_4;
FieldInfo GridLayoutGroup_t1316____m_StartAxis_11_FieldInfo = 
{
	"m_StartAxis"/* name */
	, &Axis_t1314_0_0_4/* type */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, offsetof(GridLayoutGroup_t1316, ___m_StartAxis_11)/* offset */
	, 280/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_4;
FieldInfo GridLayoutGroup_t1316____m_CellSize_12_FieldInfo = 
{
	"m_CellSize"/* name */
	, &Vector2_t739_0_0_4/* type */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, offsetof(GridLayoutGroup_t1316, ___m_CellSize_12)/* offset */
	, 281/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_4;
FieldInfo GridLayoutGroup_t1316____m_Spacing_13_FieldInfo = 
{
	"m_Spacing"/* name */
	, &Vector2_t739_0_0_4/* type */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, offsetof(GridLayoutGroup_t1316, ___m_Spacing_13)/* offset */
	, 282/* custom_attributes_cache */

};
extern Il2CppType Constraint_t1315_0_0_4;
FieldInfo GridLayoutGroup_t1316____m_Constraint_14_FieldInfo = 
{
	"m_Constraint"/* name */
	, &Constraint_t1315_0_0_4/* type */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, offsetof(GridLayoutGroup_t1316, ___m_Constraint_14)/* offset */
	, 283/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_4;
FieldInfo GridLayoutGroup_t1316____m_ConstraintCount_15_FieldInfo = 
{
	"m_ConstraintCount"/* name */
	, &Int32_t189_0_0_4/* type */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, offsetof(GridLayoutGroup_t1316, ___m_ConstraintCount_15)/* offset */
	, 284/* custom_attributes_cache */

};
static FieldInfo* GridLayoutGroup_t1316_FieldInfos[] =
{
	&GridLayoutGroup_t1316____m_StartCorner_10_FieldInfo,
	&GridLayoutGroup_t1316____m_StartAxis_11_FieldInfo,
	&GridLayoutGroup_t1316____m_CellSize_12_FieldInfo,
	&GridLayoutGroup_t1316____m_Spacing_13_FieldInfo,
	&GridLayoutGroup_t1316____m_Constraint_14_FieldInfo,
	&GridLayoutGroup_t1316____m_ConstraintCount_15_FieldInfo,
	NULL
};
extern MethodInfo GridLayoutGroup_get_startCorner_m5565_MethodInfo;
extern MethodInfo GridLayoutGroup_set_startCorner_m5566_MethodInfo;
static PropertyInfo GridLayoutGroup_t1316____startCorner_PropertyInfo = 
{
	&GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, "startCorner"/* name */
	, &GridLayoutGroup_get_startCorner_m5565_MethodInfo/* get */
	, &GridLayoutGroup_set_startCorner_m5566_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo GridLayoutGroup_get_startAxis_m5567_MethodInfo;
extern MethodInfo GridLayoutGroup_set_startAxis_m5568_MethodInfo;
static PropertyInfo GridLayoutGroup_t1316____startAxis_PropertyInfo = 
{
	&GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, "startAxis"/* name */
	, &GridLayoutGroup_get_startAxis_m5567_MethodInfo/* get */
	, &GridLayoutGroup_set_startAxis_m5568_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo GridLayoutGroup_get_cellSize_m5569_MethodInfo;
extern MethodInfo GridLayoutGroup_set_cellSize_m5570_MethodInfo;
static PropertyInfo GridLayoutGroup_t1316____cellSize_PropertyInfo = 
{
	&GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, "cellSize"/* name */
	, &GridLayoutGroup_get_cellSize_m5569_MethodInfo/* get */
	, &GridLayoutGroup_set_cellSize_m5570_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo GridLayoutGroup_get_spacing_m5571_MethodInfo;
extern MethodInfo GridLayoutGroup_set_spacing_m5572_MethodInfo;
static PropertyInfo GridLayoutGroup_t1316____spacing_PropertyInfo = 
{
	&GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &GridLayoutGroup_get_spacing_m5571_MethodInfo/* get */
	, &GridLayoutGroup_set_spacing_m5572_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo GridLayoutGroup_get_constraint_m5573_MethodInfo;
extern MethodInfo GridLayoutGroup_set_constraint_m5574_MethodInfo;
static PropertyInfo GridLayoutGroup_t1316____constraint_PropertyInfo = 
{
	&GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, "constraint"/* name */
	, &GridLayoutGroup_get_constraint_m5573_MethodInfo/* get */
	, &GridLayoutGroup_set_constraint_m5574_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo GridLayoutGroup_get_constraintCount_m5575_MethodInfo;
extern MethodInfo GridLayoutGroup_set_constraintCount_m5576_MethodInfo;
static PropertyInfo GridLayoutGroup_t1316____constraintCount_PropertyInfo = 
{
	&GridLayoutGroup_t1316_il2cpp_TypeInfo/* parent */
	, "constraintCount"/* name */
	, &GridLayoutGroup_get_constraintCount_m5575_MethodInfo/* get */
	, &GridLayoutGroup_set_constraintCount_m5576_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* GridLayoutGroup_t1316_PropertyInfos[] =
{
	&GridLayoutGroup_t1316____startCorner_PropertyInfo,
	&GridLayoutGroup_t1316____startAxis_PropertyInfo,
	&GridLayoutGroup_t1316____cellSize_PropertyInfo,
	&GridLayoutGroup_t1316____spacing_PropertyInfo,
	&GridLayoutGroup_t1316____constraint_PropertyInfo,
	&GridLayoutGroup_t1316____constraintCount_PropertyInfo,
	NULL
};
static const Il2CppType* GridLayoutGroup_t1316_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Corner_t1313_0_0_0,
	&Axis_t1314_0_0_0,
	&Constraint_t1315_0_0_0,
};
extern MethodInfo LayoutGroup_OnEnable_m5635_MethodInfo;
extern MethodInfo LayoutGroup_OnDisable_m5636_MethodInfo;
extern MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo;
extern MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo;
extern MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m5577_MethodInfo;
extern MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m5578_MethodInfo;
extern MethodInfo LayoutGroup_get_minWidth_m5628_MethodInfo;
extern MethodInfo LayoutGroup_get_preferredWidth_m5629_MethodInfo;
extern MethodInfo LayoutGroup_get_flexibleWidth_m5630_MethodInfo;
extern MethodInfo LayoutGroup_get_minHeight_m5631_MethodInfo;
extern MethodInfo LayoutGroup_get_preferredHeight_m5632_MethodInfo;
extern MethodInfo LayoutGroup_get_flexibleHeight_m5633_MethodInfo;
extern MethodInfo LayoutGroup_get_layoutPriority_m5634_MethodInfo;
extern MethodInfo GridLayoutGroup_SetLayoutHorizontal_m5579_MethodInfo;
extern MethodInfo GridLayoutGroup_SetLayoutVertical_m5580_MethodInfo;
extern MethodInfo LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo;
static Il2CppMethodReference GridLayoutGroup_t1316_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&LayoutGroup_OnEnable_m5635_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&LayoutGroup_OnDisable_m5636_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m5577_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m5578_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m5579_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m5580_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m5577_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m5578_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m5579_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m5580_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo,
};
static bool GridLayoutGroup_t1316_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ILayoutGroup_t1412_0_0_0;
static Il2CppInterfaceOffsetPair GridLayoutGroup_t1316_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t1367_0_0_0, 15},
	{ &ILayoutController_t1414_0_0_0, 24},
	{ &ILayoutGroup_t1412_0_0_0, 26},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType GridLayoutGroup_t1316_1_0_0;
extern Il2CppType LayoutGroup_t1317_0_0_0;
struct GridLayoutGroup_t1316;
const Il2CppTypeDefinitionMetadata GridLayoutGroup_t1316_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GridLayoutGroup_t1316_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GridLayoutGroup_t1316_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t1317_0_0_0/* parent */
	, GridLayoutGroup_t1316_VTable/* vtableMethods */
	, GridLayoutGroup_t1316_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo GridLayoutGroup_t1316_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GridLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, GridLayoutGroup_t1316_MethodInfos/* methods */
	, GridLayoutGroup_t1316_PropertyInfos/* properties */
	, GridLayoutGroup_t1316_FieldInfos/* fields */
	, NULL/* events */
	, &GridLayoutGroup_t1316_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 278/* custom_attributes_cache */
	, &GridLayoutGroup_t1316_0_0_0/* byval_arg */
	, &GridLayoutGroup_t1316_1_0_0/* this_arg */
	, &GridLayoutGroup_t1316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GridLayoutGroup_t1316)/* instance_size */
	, sizeof (GridLayoutGroup_t1316)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 38/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup
extern TypeInfo HorizontalLayoutGroup_t1318_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
MethodInfo HorizontalLayoutGroup__ctor_m5582_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalLayoutGroup__ctor_m5582/* method */
	, &HorizontalLayoutGroup_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m5583_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m5583/* method */
	, &HorizontalLayoutGroup_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m5584_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputVertical_m5584/* method */
	, &HorizontalLayoutGroup_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m5585_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutHorizontal_m5585/* method */
	, &HorizontalLayoutGroup_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m5586_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutVertical_m5586/* method */
	, &HorizontalLayoutGroup_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* HorizontalLayoutGroup_t1318_MethodInfos[] =
{
	&HorizontalLayoutGroup__ctor_m5582_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m5583_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m5584_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m5585_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m5586_MethodInfo,
	NULL
};
extern MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m5583_MethodInfo;
extern MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m5584_MethodInfo;
extern MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m5585_MethodInfo;
extern MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m5586_MethodInfo;
static Il2CppMethodReference HorizontalLayoutGroup_t1318_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&LayoutGroup_OnEnable_m5635_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&LayoutGroup_OnDisable_m5636_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m5583_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m5584_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m5585_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m5586_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m5583_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m5584_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m5585_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m5586_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo,
};
static bool HorizontalLayoutGroup_t1318_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalLayoutGroup_t1318_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t1367_0_0_0, 15},
	{ &ILayoutController_t1414_0_0_0, 24},
	{ &ILayoutGroup_t1412_0_0_0, 26},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType HorizontalLayoutGroup_t1318_0_0_0;
extern Il2CppType HorizontalLayoutGroup_t1318_1_0_0;
extern Il2CppType HorizontalOrVerticalLayoutGroup_t1319_0_0_0;
struct HorizontalLayoutGroup_t1318;
const Il2CppTypeDefinitionMetadata HorizontalLayoutGroup_t1318_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalLayoutGroup_t1318_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t1319_0_0_0/* parent */
	, HorizontalLayoutGroup_t1318_VTable/* vtableMethods */
	, HorizontalLayoutGroup_t1318_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo HorizontalLayoutGroup_t1318_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalLayoutGroup_t1318_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &HorizontalLayoutGroup_t1318_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 285/* custom_attributes_cache */
	, &HorizontalLayoutGroup_t1318_0_0_0/* byval_arg */
	, &HorizontalLayoutGroup_t1318_1_0_0/* this_arg */
	, &HorizontalLayoutGroup_t1318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalLayoutGroup_t1318)/* instance_size */
	, sizeof (HorizontalLayoutGroup_t1318)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 38/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup
extern TypeInfo HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
MethodInfo HorizontalOrVerticalLayoutGroup__ctor_m5587_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup__ctor_m5587/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m5588_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_spacing_m5588/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_set_spacing_m5589_ParameterInfos[] = 
{
	{"value", 0, 134218346, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m5589_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_spacing_m5589/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_set_spacing_m5589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m5590_MethodInfo = 
{
	"get_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m5590/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591_ParameterInfos[] = 
{
	{"value", 0, 134218347, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591_MethodInfo = 
{
	"set_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m5592_MethodInfo = 
{
	"get_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m5592/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593_ParameterInfos[] = 
{
	{"value", 0, 134218348, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593_MethodInfo = 
{
	"set_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m5594_ParameterInfos[] = 
{
	{"axis", 0, 134218349, 0, &Int32_t189_0_0_0},
	{"isVertical", 1, 134218350, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
MethodInfo HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m5594_MethodInfo = 
{
	"CalcAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m5594/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_SByte_t236/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m5594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m5595_ParameterInfos[] = 
{
	{"axis", 0, 134218351, 0, &Int32_t189_0_0_0},
	{"isVertical", 1, 134218352, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
MethodInfo HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m5595_MethodInfo = 
{
	"SetChildrenAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m5595/* method */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_SByte_t236/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t1319_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m5595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* HorizontalOrVerticalLayoutGroup_t1319_MethodInfos[] =
{
	&HorizontalOrVerticalLayoutGroup__ctor_m5587_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_spacing_m5588_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_spacing_m5589_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m5590_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m5592_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m5594_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m5595_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_4;
FieldInfo HorizontalOrVerticalLayoutGroup_t1319____m_Spacing_10_FieldInfo = 
{
	"m_Spacing"/* name */
	, &Single_t202_0_0_4/* type */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* parent */
	, offsetof(HorizontalOrVerticalLayoutGroup_t1319, ___m_Spacing_10)/* offset */
	, 286/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_4;
FieldInfo HorizontalOrVerticalLayoutGroup_t1319____m_ChildForceExpandWidth_11_FieldInfo = 
{
	"m_ChildForceExpandWidth"/* name */
	, &Boolean_t203_0_0_4/* type */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* parent */
	, offsetof(HorizontalOrVerticalLayoutGroup_t1319, ___m_ChildForceExpandWidth_11)/* offset */
	, 287/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_4;
FieldInfo HorizontalOrVerticalLayoutGroup_t1319____m_ChildForceExpandHeight_12_FieldInfo = 
{
	"m_ChildForceExpandHeight"/* name */
	, &Boolean_t203_0_0_4/* type */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* parent */
	, offsetof(HorizontalOrVerticalLayoutGroup_t1319, ___m_ChildForceExpandHeight_12)/* offset */
	, 288/* custom_attributes_cache */

};
static FieldInfo* HorizontalOrVerticalLayoutGroup_t1319_FieldInfos[] =
{
	&HorizontalOrVerticalLayoutGroup_t1319____m_Spacing_10_FieldInfo,
	&HorizontalOrVerticalLayoutGroup_t1319____m_ChildForceExpandWidth_11_FieldInfo,
	&HorizontalOrVerticalLayoutGroup_t1319____m_ChildForceExpandHeight_12_FieldInfo,
	NULL
};
extern MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m5588_MethodInfo;
extern MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m5589_MethodInfo;
static PropertyInfo HorizontalOrVerticalLayoutGroup_t1319____spacing_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_spacing_m5588_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_spacing_m5589_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m5590_MethodInfo;
extern MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591_MethodInfo;
static PropertyInfo HorizontalOrVerticalLayoutGroup_t1319____childForceExpandWidth_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* parent */
	, "childForceExpandWidth"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m5590_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m5591_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m5592_MethodInfo;
extern MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593_MethodInfo;
static PropertyInfo HorizontalOrVerticalLayoutGroup_t1319____childForceExpandHeight_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* parent */
	, "childForceExpandHeight"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m5592_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m5593_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* HorizontalOrVerticalLayoutGroup_t1319_PropertyInfos[] =
{
	&HorizontalOrVerticalLayoutGroup_t1319____spacing_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t1319____childForceExpandWidth_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t1319____childForceExpandHeight_PropertyInfo,
	NULL
};
extern MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m5627_MethodInfo;
extern MethodInfo LayoutGroup_CalculateLayoutInputVertical_m6262_MethodInfo;
extern MethodInfo LayoutGroup_SetLayoutHorizontal_m6263_MethodInfo;
extern MethodInfo LayoutGroup_SetLayoutVertical_m6264_MethodInfo;
static Il2CppMethodReference HorizontalOrVerticalLayoutGroup_t1319_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&LayoutGroup_OnEnable_m5635_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&LayoutGroup_OnDisable_m5636_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m5627_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m6262_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m6263_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m6264_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m5627_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo,
};
static bool HorizontalOrVerticalLayoutGroup_t1319_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalOrVerticalLayoutGroup_t1319_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t1367_0_0_0, 15},
	{ &ILayoutController_t1414_0_0_0, 24},
	{ &ILayoutGroup_t1412_0_0_0, 26},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType HorizontalOrVerticalLayoutGroup_t1319_1_0_0;
struct HorizontalOrVerticalLayoutGroup_t1319;
const Il2CppTypeDefinitionMetadata HorizontalOrVerticalLayoutGroup_t1319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalOrVerticalLayoutGroup_t1319_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t1317_0_0_0/* parent */
	, HorizontalOrVerticalLayoutGroup_t1319_VTable/* vtableMethods */
	, HorizontalOrVerticalLayoutGroup_t1319_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalOrVerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalOrVerticalLayoutGroup_t1319_MethodInfos/* methods */
	, HorizontalOrVerticalLayoutGroup_t1319_PropertyInfos/* properties */
	, HorizontalOrVerticalLayoutGroup_t1319_FieldInfos/* fields */
	, NULL/* events */
	, &HorizontalOrVerticalLayoutGroup_t1319_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalOrVerticalLayoutGroup_t1319_0_0_0/* byval_arg */
	, &HorizontalOrVerticalLayoutGroup_t1319_1_0_0/* this_arg */
	, &HorizontalOrVerticalLayoutGroup_t1319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalOrVerticalLayoutGroup_t1319)/* instance_size */
	, sizeof (HorizontalOrVerticalLayoutGroup_t1319)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 38/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutElement
extern TypeInfo ILayoutElement_t1367_il2cpp_TypeInfo;
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal()
MethodInfo ILayoutElement_CalculateLayoutInputHorizontal_m6250_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical()
MethodInfo ILayoutElement_CalculateLayoutInputVertical_m6251_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minWidth()
MethodInfo ILayoutElement_get_minWidth_m6252_MethodInfo = 
{
	"get_minWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth()
MethodInfo ILayoutElement_get_preferredWidth_m6253_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth()
MethodInfo ILayoutElement_get_flexibleWidth_m6254_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minHeight()
MethodInfo ILayoutElement_get_minHeight_m6255_MethodInfo = 
{
	"get_minHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight()
MethodInfo ILayoutElement_get_preferredHeight_m6256_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight()
MethodInfo ILayoutElement_get_flexibleHeight_m6257_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority()
MethodInfo ILayoutElement_get_layoutPriority_m6258_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, NULL/* method */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILayoutElement_t1367_MethodInfos[] =
{
	&ILayoutElement_CalculateLayoutInputHorizontal_m6250_MethodInfo,
	&ILayoutElement_CalculateLayoutInputVertical_m6251_MethodInfo,
	&ILayoutElement_get_minWidth_m6252_MethodInfo,
	&ILayoutElement_get_preferredWidth_m6253_MethodInfo,
	&ILayoutElement_get_flexibleWidth_m6254_MethodInfo,
	&ILayoutElement_get_minHeight_m6255_MethodInfo,
	&ILayoutElement_get_preferredHeight_m6256_MethodInfo,
	&ILayoutElement_get_flexibleHeight_m6257_MethodInfo,
	&ILayoutElement_get_layoutPriority_m6258_MethodInfo,
	NULL
};
extern MethodInfo ILayoutElement_get_minWidth_m6252_MethodInfo;
static PropertyInfo ILayoutElement_t1367____minWidth_PropertyInfo = 
{
	&ILayoutElement_t1367_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &ILayoutElement_get_minWidth_m6252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILayoutElement_get_preferredWidth_m6253_MethodInfo;
static PropertyInfo ILayoutElement_t1367____preferredWidth_PropertyInfo = 
{
	&ILayoutElement_t1367_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &ILayoutElement_get_preferredWidth_m6253_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILayoutElement_get_flexibleWidth_m6254_MethodInfo;
static PropertyInfo ILayoutElement_t1367____flexibleWidth_PropertyInfo = 
{
	&ILayoutElement_t1367_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &ILayoutElement_get_flexibleWidth_m6254_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILayoutElement_get_minHeight_m6255_MethodInfo;
static PropertyInfo ILayoutElement_t1367____minHeight_PropertyInfo = 
{
	&ILayoutElement_t1367_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &ILayoutElement_get_minHeight_m6255_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILayoutElement_get_preferredHeight_m6256_MethodInfo;
static PropertyInfo ILayoutElement_t1367____preferredHeight_PropertyInfo = 
{
	&ILayoutElement_t1367_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &ILayoutElement_get_preferredHeight_m6256_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILayoutElement_get_flexibleHeight_m6257_MethodInfo;
static PropertyInfo ILayoutElement_t1367____flexibleHeight_PropertyInfo = 
{
	&ILayoutElement_t1367_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &ILayoutElement_get_flexibleHeight_m6257_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ILayoutElement_get_layoutPriority_m6258_MethodInfo;
static PropertyInfo ILayoutElement_t1367____layoutPriority_PropertyInfo = 
{
	&ILayoutElement_t1367_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &ILayoutElement_get_layoutPriority_m6258_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ILayoutElement_t1367_PropertyInfos[] =
{
	&ILayoutElement_t1367____minWidth_PropertyInfo,
	&ILayoutElement_t1367____preferredWidth_PropertyInfo,
	&ILayoutElement_t1367____flexibleWidth_PropertyInfo,
	&ILayoutElement_t1367____minHeight_PropertyInfo,
	&ILayoutElement_t1367____preferredHeight_PropertyInfo,
	&ILayoutElement_t1367____flexibleHeight_PropertyInfo,
	&ILayoutElement_t1367____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ILayoutElement_t1367_1_0_0;
struct ILayoutElement_t1367;
const Il2CppTypeDefinitionMetadata ILayoutElement_t1367_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ILayoutElement_t1367_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutElement_t1367_MethodInfos/* methods */
	, ILayoutElement_t1367_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ILayoutElement_t1367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutElement_t1367_0_0_0/* byval_arg */
	, &ILayoutElement_t1367_1_0_0/* this_arg */
	, &ILayoutElement_t1367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutController
extern TypeInfo ILayoutController_t1414_il2cpp_TypeInfo;
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal()
MethodInfo ILayoutController_SetLayoutHorizontal_m6259_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &ILayoutController_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical()
MethodInfo ILayoutController_SetLayoutVertical_m6260_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &ILayoutController_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILayoutController_t1414_MethodInfos[] =
{
	&ILayoutController_SetLayoutHorizontal_m6259_MethodInfo,
	&ILayoutController_SetLayoutVertical_m6260_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ILayoutController_t1414_1_0_0;
struct ILayoutController_t1414;
const Il2CppTypeDefinitionMetadata ILayoutController_t1414_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ILayoutController_t1414_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutController_t1414_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ILayoutController_t1414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutController_t1414_0_0_0/* byval_arg */
	, &ILayoutController_t1414_1_0_0/* this_arg */
	, &ILayoutController_t1414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutGroup
extern TypeInfo ILayoutGroup_t1412_il2cpp_TypeInfo;
static MethodInfo* ILayoutGroup_t1412_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutGroup_t1412_InterfacesTypeInfos[] = 
{
	&ILayoutController_t1414_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ILayoutGroup_t1412_1_0_0;
struct ILayoutGroup_t1412;
const Il2CppTypeDefinitionMetadata ILayoutGroup_t1412_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutGroup_t1412_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ILayoutGroup_t1412_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutGroup_t1412_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ILayoutGroup_t1412_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutGroup_t1412_0_0_0/* byval_arg */
	, &ILayoutGroup_t1412_1_0_0/* this_arg */
	, &ILayoutGroup_t1412_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutSelfController
extern TypeInfo ILayoutSelfController_t1415_il2cpp_TypeInfo;
static MethodInfo* ILayoutSelfController_t1415_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutSelfController_t1415_InterfacesTypeInfos[] = 
{
	&ILayoutController_t1414_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ILayoutSelfController_t1415_1_0_0;
struct ILayoutSelfController_t1415;
const Il2CppTypeDefinitionMetadata ILayoutSelfController_t1415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutSelfController_t1415_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ILayoutSelfController_t1415_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutSelfController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutSelfController_t1415_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ILayoutSelfController_t1415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutSelfController_t1415_0_0_0/* byval_arg */
	, &ILayoutSelfController_t1415_1_0_0/* this_arg */
	, &ILayoutSelfController_t1415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutIgnorer
extern TypeInfo ILayoutIgnorer_t1411_il2cpp_TypeInfo;
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout()
MethodInfo ILayoutIgnorer_get_ignoreLayout_m6261_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, NULL/* method */
	, &ILayoutIgnorer_t1411_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILayoutIgnorer_t1411_MethodInfos[] =
{
	&ILayoutIgnorer_get_ignoreLayout_m6261_MethodInfo,
	NULL
};
extern MethodInfo ILayoutIgnorer_get_ignoreLayout_m6261_MethodInfo;
static PropertyInfo ILayoutIgnorer_t1411____ignoreLayout_PropertyInfo = 
{
	&ILayoutIgnorer_t1411_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &ILayoutIgnorer_get_ignoreLayout_m6261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ILayoutIgnorer_t1411_PropertyInfos[] =
{
	&ILayoutIgnorer_t1411____ignoreLayout_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ILayoutIgnorer_t1411_0_0_0;
extern Il2CppType ILayoutIgnorer_t1411_1_0_0;
struct ILayoutIgnorer_t1411;
const Il2CppTypeDefinitionMetadata ILayoutIgnorer_t1411_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ILayoutIgnorer_t1411_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutIgnorer"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutIgnorer_t1411_MethodInfos/* methods */
	, ILayoutIgnorer_t1411_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ILayoutIgnorer_t1411_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutIgnorer_t1411_0_0_0/* byval_arg */
	, &ILayoutIgnorer_t1411_1_0_0/* this_arg */
	, &ILayoutIgnorer_t1411_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// Metadata Definition UnityEngine.UI.LayoutElement
extern TypeInfo LayoutElement_t1320_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::.ctor()
MethodInfo LayoutElement__ctor_m5596_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutElement__ctor_m5596/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
MethodInfo LayoutElement_get_ignoreLayout_m5597_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_get_ignoreLayout_m5597/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo LayoutElement_t1320_LayoutElement_set_ignoreLayout_m5598_ParameterInfos[] = 
{
	{"value", 0, 134218353, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
MethodInfo LayoutElement_set_ignoreLayout_m5598_MethodInfo = 
{
	"set_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_set_ignoreLayout_m5598/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, LayoutElement_t1320_LayoutElement_set_ignoreLayout_m5598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m5599_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputHorizontal_m5599/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
MethodInfo LayoutElement_CalculateLayoutInputVertical_m5600_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputVertical_m5600/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
MethodInfo LayoutElement_get_minWidth_m5601_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutElement_get_minWidth_m5601/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutElement_t1320_LayoutElement_set_minWidth_m5602_ParameterInfos[] = 
{
	{"value", 0, 134218354, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
MethodInfo LayoutElement_set_minWidth_m5602_MethodInfo = 
{
	"set_minWidth"/* name */
	, (methodPointerType)&LayoutElement_set_minWidth_m5602/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, LayoutElement_t1320_LayoutElement_set_minWidth_m5602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
MethodInfo LayoutElement_get_minHeight_m5603_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutElement_get_minHeight_m5603/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutElement_t1320_LayoutElement_set_minHeight_m5604_ParameterInfos[] = 
{
	{"value", 0, 134218355, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
MethodInfo LayoutElement_set_minHeight_m5604_MethodInfo = 
{
	"set_minHeight"/* name */
	, (methodPointerType)&LayoutElement_set_minHeight_m5604/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, LayoutElement_t1320_LayoutElement_set_minHeight_m5604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
MethodInfo LayoutElement_get_preferredWidth_m5605_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_get_preferredWidth_m5605/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutElement_t1320_LayoutElement_set_preferredWidth_m5606_ParameterInfos[] = 
{
	{"value", 0, 134218356, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
MethodInfo LayoutElement_set_preferredWidth_m5606_MethodInfo = 
{
	"set_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_set_preferredWidth_m5606/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, LayoutElement_t1320_LayoutElement_set_preferredWidth_m5606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
MethodInfo LayoutElement_get_preferredHeight_m5607_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_get_preferredHeight_m5607/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutElement_t1320_LayoutElement_set_preferredHeight_m5608_ParameterInfos[] = 
{
	{"value", 0, 134218357, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
MethodInfo LayoutElement_set_preferredHeight_m5608_MethodInfo = 
{
	"set_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_set_preferredHeight_m5608/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, LayoutElement_t1320_LayoutElement_set_preferredHeight_m5608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
MethodInfo LayoutElement_get_flexibleWidth_m5609_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleWidth_m5609/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutElement_t1320_LayoutElement_set_flexibleWidth_m5610_ParameterInfos[] = 
{
	{"value", 0, 134218358, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
MethodInfo LayoutElement_set_flexibleWidth_m5610_MethodInfo = 
{
	"set_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleWidth_m5610/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, LayoutElement_t1320_LayoutElement_set_flexibleWidth_m5610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
MethodInfo LayoutElement_get_flexibleHeight_m5611_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleHeight_m5611/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 39/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutElement_t1320_LayoutElement_set_flexibleHeight_m5612_ParameterInfos[] = 
{
	{"value", 0, 134218359, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
MethodInfo LayoutElement_set_flexibleHeight_m5612_MethodInfo = 
{
	"set_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleHeight_m5612/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202/* invoker_method */
	, LayoutElement_t1320_LayoutElement_set_flexibleHeight_m5612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 40/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
MethodInfo LayoutElement_get_layoutPriority_m5613_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutElement_get_layoutPriority_m5613/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 41/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
MethodInfo LayoutElement_OnEnable_m5614_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutElement_OnEnable_m5614/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
MethodInfo LayoutElement_OnTransformParentChanged_m5615_MethodInfo = 
{
	"OnTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnTransformParentChanged_m5615/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
MethodInfo LayoutElement_OnDisable_m5616_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutElement_OnDisable_m5616/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
MethodInfo LayoutElement_OnDidApplyAnimationProperties_m5617_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutElement_OnDidApplyAnimationProperties_m5617/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
MethodInfo LayoutElement_OnBeforeTransformParentChanged_m5618_MethodInfo = 
{
	"OnBeforeTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnBeforeTransformParentChanged_m5618/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
MethodInfo LayoutElement_SetDirty_m5619_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutElement_SetDirty_m5619/* method */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LayoutElement_t1320_MethodInfos[] =
{
	&LayoutElement__ctor_m5596_MethodInfo,
	&LayoutElement_get_ignoreLayout_m5597_MethodInfo,
	&LayoutElement_set_ignoreLayout_m5598_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m5599_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m5600_MethodInfo,
	&LayoutElement_get_minWidth_m5601_MethodInfo,
	&LayoutElement_set_minWidth_m5602_MethodInfo,
	&LayoutElement_get_minHeight_m5603_MethodInfo,
	&LayoutElement_set_minHeight_m5604_MethodInfo,
	&LayoutElement_get_preferredWidth_m5605_MethodInfo,
	&LayoutElement_set_preferredWidth_m5606_MethodInfo,
	&LayoutElement_get_preferredHeight_m5607_MethodInfo,
	&LayoutElement_set_preferredHeight_m5608_MethodInfo,
	&LayoutElement_get_flexibleWidth_m5609_MethodInfo,
	&LayoutElement_set_flexibleWidth_m5610_MethodInfo,
	&LayoutElement_get_flexibleHeight_m5611_MethodInfo,
	&LayoutElement_set_flexibleHeight_m5612_MethodInfo,
	&LayoutElement_get_layoutPriority_m5613_MethodInfo,
	&LayoutElement_OnEnable_m5614_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m5615_MethodInfo,
	&LayoutElement_OnDisable_m5616_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m5617_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m5618_MethodInfo,
	&LayoutElement_SetDirty_m5619_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo LayoutElement_t1320____m_IgnoreLayout_2_FieldInfo = 
{
	"m_IgnoreLayout"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutElement_t1320, ___m_IgnoreLayout_2)/* offset */
	, 290/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo LayoutElement_t1320____m_MinWidth_3_FieldInfo = 
{
	"m_MinWidth"/* name */
	, &Single_t202_0_0_1/* type */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutElement_t1320, ___m_MinWidth_3)/* offset */
	, 291/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo LayoutElement_t1320____m_MinHeight_4_FieldInfo = 
{
	"m_MinHeight"/* name */
	, &Single_t202_0_0_1/* type */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutElement_t1320, ___m_MinHeight_4)/* offset */
	, 292/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo LayoutElement_t1320____m_PreferredWidth_5_FieldInfo = 
{
	"m_PreferredWidth"/* name */
	, &Single_t202_0_0_1/* type */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutElement_t1320, ___m_PreferredWidth_5)/* offset */
	, 293/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo LayoutElement_t1320____m_PreferredHeight_6_FieldInfo = 
{
	"m_PreferredHeight"/* name */
	, &Single_t202_0_0_1/* type */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutElement_t1320, ___m_PreferredHeight_6)/* offset */
	, 294/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo LayoutElement_t1320____m_FlexibleWidth_7_FieldInfo = 
{
	"m_FlexibleWidth"/* name */
	, &Single_t202_0_0_1/* type */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutElement_t1320, ___m_FlexibleWidth_7)/* offset */
	, 295/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo LayoutElement_t1320____m_FlexibleHeight_8_FieldInfo = 
{
	"m_FlexibleHeight"/* name */
	, &Single_t202_0_0_1/* type */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutElement_t1320, ___m_FlexibleHeight_8)/* offset */
	, 296/* custom_attributes_cache */

};
static FieldInfo* LayoutElement_t1320_FieldInfos[] =
{
	&LayoutElement_t1320____m_IgnoreLayout_2_FieldInfo,
	&LayoutElement_t1320____m_MinWidth_3_FieldInfo,
	&LayoutElement_t1320____m_MinHeight_4_FieldInfo,
	&LayoutElement_t1320____m_PreferredWidth_5_FieldInfo,
	&LayoutElement_t1320____m_PreferredHeight_6_FieldInfo,
	&LayoutElement_t1320____m_FlexibleWidth_7_FieldInfo,
	&LayoutElement_t1320____m_FlexibleHeight_8_FieldInfo,
	NULL
};
extern MethodInfo LayoutElement_get_ignoreLayout_m5597_MethodInfo;
extern MethodInfo LayoutElement_set_ignoreLayout_m5598_MethodInfo;
static PropertyInfo LayoutElement_t1320____ignoreLayout_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &LayoutElement_get_ignoreLayout_m5597_MethodInfo/* get */
	, &LayoutElement_set_ignoreLayout_m5598_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutElement_get_minWidth_m5601_MethodInfo;
extern MethodInfo LayoutElement_set_minWidth_m5602_MethodInfo;
static PropertyInfo LayoutElement_t1320____minWidth_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutElement_get_minWidth_m5601_MethodInfo/* get */
	, &LayoutElement_set_minWidth_m5602_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutElement_get_minHeight_m5603_MethodInfo;
extern MethodInfo LayoutElement_set_minHeight_m5604_MethodInfo;
static PropertyInfo LayoutElement_t1320____minHeight_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutElement_get_minHeight_m5603_MethodInfo/* get */
	, &LayoutElement_set_minHeight_m5604_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutElement_get_preferredWidth_m5605_MethodInfo;
extern MethodInfo LayoutElement_set_preferredWidth_m5606_MethodInfo;
static PropertyInfo LayoutElement_t1320____preferredWidth_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutElement_get_preferredWidth_m5605_MethodInfo/* get */
	, &LayoutElement_set_preferredWidth_m5606_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutElement_get_preferredHeight_m5607_MethodInfo;
extern MethodInfo LayoutElement_set_preferredHeight_m5608_MethodInfo;
static PropertyInfo LayoutElement_t1320____preferredHeight_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutElement_get_preferredHeight_m5607_MethodInfo/* get */
	, &LayoutElement_set_preferredHeight_m5608_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutElement_get_flexibleWidth_m5609_MethodInfo;
extern MethodInfo LayoutElement_set_flexibleWidth_m5610_MethodInfo;
static PropertyInfo LayoutElement_t1320____flexibleWidth_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutElement_get_flexibleWidth_m5609_MethodInfo/* get */
	, &LayoutElement_set_flexibleWidth_m5610_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutElement_get_flexibleHeight_m5611_MethodInfo;
extern MethodInfo LayoutElement_set_flexibleHeight_m5612_MethodInfo;
static PropertyInfo LayoutElement_t1320____flexibleHeight_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutElement_get_flexibleHeight_m5611_MethodInfo/* get */
	, &LayoutElement_set_flexibleHeight_m5612_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutElement_get_layoutPriority_m5613_MethodInfo;
static PropertyInfo LayoutElement_t1320____layoutPriority_PropertyInfo = 
{
	&LayoutElement_t1320_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutElement_get_layoutPriority_m5613_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* LayoutElement_t1320_PropertyInfos[] =
{
	&LayoutElement_t1320____ignoreLayout_PropertyInfo,
	&LayoutElement_t1320____minWidth_PropertyInfo,
	&LayoutElement_t1320____minHeight_PropertyInfo,
	&LayoutElement_t1320____preferredWidth_PropertyInfo,
	&LayoutElement_t1320____preferredHeight_PropertyInfo,
	&LayoutElement_t1320____flexibleWidth_PropertyInfo,
	&LayoutElement_t1320____flexibleHeight_PropertyInfo,
	&LayoutElement_t1320____layoutPriority_PropertyInfo,
	NULL
};
extern MethodInfo LayoutElement_OnEnable_m5614_MethodInfo;
extern MethodInfo LayoutElement_OnDisable_m5616_MethodInfo;
extern MethodInfo LayoutElement_OnBeforeTransformParentChanged_m5618_MethodInfo;
extern MethodInfo LayoutElement_OnTransformParentChanged_m5615_MethodInfo;
extern MethodInfo LayoutElement_OnDidApplyAnimationProperties_m5617_MethodInfo;
extern MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m5599_MethodInfo;
extern MethodInfo LayoutElement_CalculateLayoutInputVertical_m5600_MethodInfo;
static Il2CppMethodReference LayoutElement_t1320_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&LayoutElement_OnEnable_m5614_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&LayoutElement_OnDisable_m5616_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m5618_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m5615_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m5617_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m5599_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m5600_MethodInfo,
	&LayoutElement_get_minWidth_m5601_MethodInfo,
	&LayoutElement_get_preferredWidth_m5605_MethodInfo,
	&LayoutElement_get_flexibleWidth_m5609_MethodInfo,
	&LayoutElement_get_minHeight_m5603_MethodInfo,
	&LayoutElement_get_preferredHeight_m5607_MethodInfo,
	&LayoutElement_get_flexibleHeight_m5611_MethodInfo,
	&LayoutElement_get_layoutPriority_m5613_MethodInfo,
	&LayoutElement_get_ignoreLayout_m5597_MethodInfo,
	&LayoutElement_get_ignoreLayout_m5597_MethodInfo,
	&LayoutElement_set_ignoreLayout_m5598_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m5599_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m5600_MethodInfo,
	&LayoutElement_get_minWidth_m5601_MethodInfo,
	&LayoutElement_set_minWidth_m5602_MethodInfo,
	&LayoutElement_get_minHeight_m5603_MethodInfo,
	&LayoutElement_set_minHeight_m5604_MethodInfo,
	&LayoutElement_get_preferredWidth_m5605_MethodInfo,
	&LayoutElement_set_preferredWidth_m5606_MethodInfo,
	&LayoutElement_get_preferredHeight_m5607_MethodInfo,
	&LayoutElement_set_preferredHeight_m5608_MethodInfo,
	&LayoutElement_get_flexibleWidth_m5609_MethodInfo,
	&LayoutElement_set_flexibleWidth_m5610_MethodInfo,
	&LayoutElement_get_flexibleHeight_m5611_MethodInfo,
	&LayoutElement_set_flexibleHeight_m5612_MethodInfo,
	&LayoutElement_get_layoutPriority_m5613_MethodInfo,
};
static bool LayoutElement_t1320_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutElement_t1320_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t1367_0_0_0,
	&ILayoutIgnorer_t1411_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutElement_t1320_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t1367_0_0_0, 15},
	{ &ILayoutIgnorer_t1411_0_0_0, 24},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType LayoutElement_t1320_0_0_0;
extern Il2CppType LayoutElement_t1320_1_0_0;
struct LayoutElement_t1320;
const Il2CppTypeDefinitionMetadata LayoutElement_t1320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutElement_t1320_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutElement_t1320_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, LayoutElement_t1320_VTable/* vtableMethods */
	, LayoutElement_t1320_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LayoutElement_t1320_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutElement_t1320_MethodInfos/* methods */
	, LayoutElement_t1320_PropertyInfos/* properties */
	, LayoutElement_t1320_FieldInfos/* fields */
	, NULL/* events */
	, &LayoutElement_t1320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 289/* custom_attributes_cache */
	, &LayoutElement_t1320_0_0_0/* byval_arg */
	, &LayoutElement_t1320_1_0_0/* this_arg */
	, &LayoutElement_t1320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutElement_t1320)/* instance_size */
	, sizeof (LayoutElement_t1320)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 8/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 42/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// Metadata Definition UnityEngine.UI.LayoutGroup
extern TypeInfo LayoutGroup_t1317_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::.ctor()
MethodInfo LayoutGroup__ctor_m5620_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutGroup__ctor_m5620/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectOffset_t1321_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
MethodInfo LayoutGroup_get_padding_m5621_MethodInfo = 
{
	"get_padding"/* name */
	, (methodPointerType)&LayoutGroup_get_padding_m5621/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &RectOffset_t1321_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectOffset_t1321_0_0_0;
extern Il2CppType RectOffset_t1321_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_set_padding_m5622_ParameterInfos[] = 
{
	{"value", 0, 134218360, 0, &RectOffset_t1321_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
MethodInfo LayoutGroup_set_padding_m5622_MethodInfo = 
{
	"set_padding"/* name */
	, (methodPointerType)&LayoutGroup_set_padding_m5622/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_set_padding_m5622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextAnchor_t1410_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t1410 (MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
MethodInfo LayoutGroup_get_childAlignment_m5623_MethodInfo = 
{
	"get_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_get_childAlignment_m5623/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t1410_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t1410/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextAnchor_t1410_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_set_childAlignment_m5624_ParameterInfos[] = 
{
	{"value", 0, 134218361, 0, &TextAnchor_t1410_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
MethodInfo LayoutGroup_set_childAlignment_m5624_MethodInfo = 
{
	"set_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_set_childAlignment_m5624/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_set_childAlignment_m5624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
MethodInfo LayoutGroup_get_rectTransform_m5625_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&LayoutGroup_get_rectTransform_m5625/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1322_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
MethodInfo LayoutGroup_get_rectChildren_m5626_MethodInfo = 
{
	"get_rectChildren"/* name */
	, (methodPointerType)&LayoutGroup_get_rectChildren_m5626/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1322_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m5627_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutGroup_CalculateLayoutInputHorizontal_m5627/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputVertical()
MethodInfo LayoutGroup_CalculateLayoutInputVertical_m6262_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
MethodInfo LayoutGroup_get_minWidth_m5628_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_minWidth_m5628/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
MethodInfo LayoutGroup_get_preferredWidth_m5629_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredWidth_m5629/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
MethodInfo LayoutGroup_get_flexibleWidth_m5630_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleWidth_m5630/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
MethodInfo LayoutGroup_get_minHeight_m5631_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_minHeight_m5631/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
MethodInfo LayoutGroup_get_preferredHeight_m5632_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredHeight_m5632/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
MethodInfo LayoutGroup_get_flexibleHeight_m5633_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleHeight_m5633/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
MethodInfo LayoutGroup_get_layoutPriority_m5634_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutGroup_get_layoutPriority_m5634/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutHorizontal()
MethodInfo LayoutGroup_SetLayoutHorizontal_m6263_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutVertical()
MethodInfo LayoutGroup_SetLayoutVertical_m6264_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
MethodInfo LayoutGroup_OnEnable_m5635_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutGroup_OnEnable_m5635/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
MethodInfo LayoutGroup_OnDisable_m5636_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutGroup_OnDisable_m5636/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutGroup_OnDidApplyAnimationProperties_m5637/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_GetTotalMinSize_m5638_ParameterInfos[] = 
{
	{"axis", 0, 134218362, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
MethodInfo LayoutGroup_GetTotalMinSize_m5638_MethodInfo = 
{
	"GetTotalMinSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalMinSize_m5638/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Int32_t189/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_GetTotalMinSize_m5638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_GetTotalPreferredSize_m5639_ParameterInfos[] = 
{
	{"axis", 0, 134218363, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
MethodInfo LayoutGroup_GetTotalPreferredSize_m5639_MethodInfo = 
{
	"GetTotalPreferredSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalPreferredSize_m5639/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Int32_t189/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_GetTotalPreferredSize_m5639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_GetTotalFlexibleSize_m5640_ParameterInfos[] = 
{
	{"axis", 0, 134218364, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
MethodInfo LayoutGroup_GetTotalFlexibleSize_m5640_MethodInfo = 
{
	"GetTotalFlexibleSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalFlexibleSize_m5640/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Int32_t189/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_GetTotalFlexibleSize_m5640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_GetStartOffset_m5641_ParameterInfos[] = 
{
	{"axis", 0, 134218365, 0, &Int32_t189_0_0_0},
	{"requiredSpaceWithoutPadding", 1, 134218366, 0, &Single_t202_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
MethodInfo LayoutGroup_GetStartOffset_m5641_MethodInfo = 
{
	"GetStartOffset"/* name */
	, (methodPointerType)&LayoutGroup_GetStartOffset_m5641/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Int32_t189_Single_t202/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_GetStartOffset_m5641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_SetLayoutInputForAxis_m5642_ParameterInfos[] = 
{
	{"totalMin", 0, 134218367, 0, &Single_t202_0_0_0},
	{"totalPreferred", 1, 134218368, 0, &Single_t202_0_0_0},
	{"totalFlexible", 2, 134218369, 0, &Single_t202_0_0_0},
	{"axis", 3, 134218370, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Single_t202_Single_t202_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
MethodInfo LayoutGroup_SetLayoutInputForAxis_m5642_MethodInfo = 
{
	"SetLayoutInputForAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetLayoutInputForAxis_m5642/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Single_t202_Single_t202_Single_t202_Int32_t189/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_SetLayoutInputForAxis_m5642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_SetChildAlongAxis_m5643_ParameterInfos[] = 
{
	{"rect", 0, 134218371, 0, &RectTransform_t1227_0_0_0},
	{"axis", 1, 134218372, 0, &Int32_t189_0_0_0},
	{"pos", 2, 134218373, 0, &Single_t202_0_0_0},
	{"size", 3, 134218374, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
MethodInfo LayoutGroup_SetChildAlongAxis_m5643_MethodInfo = 
{
	"SetChildAlongAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetChildAlongAxis_m5643/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Single_t202_Single_t202/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_SetChildAlongAxis_m5643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
MethodInfo LayoutGroup_get_isRootLayoutGroup_m5644_MethodInfo = 
{
	"get_isRootLayoutGroup"/* name */
	, (methodPointerType)&LayoutGroup_get_isRootLayoutGroup_m5644/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&LayoutGroup_OnRectTransformDimensionsChange_m5645/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
MethodInfo LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo = 
{
	"OnTransformChildrenChanged"/* name */
	, (methodPointerType)&LayoutGroup_OnTransformChildrenChanged_m5646/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LayoutGroup_SetProperty_m6265_gp_0_1_0_0;
extern Il2CppType LayoutGroup_SetProperty_m6265_gp_0_1_0_0;
extern Il2CppType LayoutGroup_SetProperty_m6265_gp_0_0_0_0;
extern Il2CppType LayoutGroup_SetProperty_m6265_gp_0_0_0_0;
static ParameterInfo LayoutGroup_t1317_LayoutGroup_SetProperty_m6265_ParameterInfos[] = 
{
	{"currentValue", 0, 134218375, 0, &LayoutGroup_SetProperty_m6265_gp_0_1_0_0},
	{"newValue", 1, 134218376, 0, &LayoutGroup_SetProperty_m6265_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern Il2CppGenericContainer LayoutGroup_SetProperty_m6265_Il2CppGenericContainer;
extern TypeInfo LayoutGroup_SetProperty_m6265_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull LayoutGroup_SetProperty_m6265_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &LayoutGroup_SetProperty_m6265_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* LayoutGroup_SetProperty_m6265_Il2CppGenericParametersArray[1] = 
{
	&LayoutGroup_SetProperty_m6265_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo LayoutGroup_SetProperty_m6265_MethodInfo;
Il2CppGenericContainer LayoutGroup_SetProperty_m6265_Il2CppGenericContainer = { { NULL, NULL }, NULL, &LayoutGroup_SetProperty_m6265_MethodInfo, 1, 1, LayoutGroup_SetProperty_m6265_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition LayoutGroup_SetProperty_m6265_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &LayoutGroup_SetProperty_m6265_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.UI.LayoutGroup::SetProperty(T&,T)
MethodInfo LayoutGroup_SetProperty_m6265_MethodInfo = 
{
	"SetProperty"/* name */
	, NULL/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, LayoutGroup_t1317_LayoutGroup_SetProperty_m6265_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1163/* token */
	, LayoutGroup_SetProperty_m6265_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LayoutGroup_SetProperty_m6265_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
MethodInfo LayoutGroup_SetDirty_m5647_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutGroup_SetDirty_m5647/* method */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LayoutGroup_t1317_MethodInfos[] =
{
	&LayoutGroup__ctor_m5620_MethodInfo,
	&LayoutGroup_get_padding_m5621_MethodInfo,
	&LayoutGroup_set_padding_m5622_MethodInfo,
	&LayoutGroup_get_childAlignment_m5623_MethodInfo,
	&LayoutGroup_set_childAlignment_m5624_MethodInfo,
	&LayoutGroup_get_rectTransform_m5625_MethodInfo,
	&LayoutGroup_get_rectChildren_m5626_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m5627_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m6262_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m6263_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m6264_MethodInfo,
	&LayoutGroup_OnEnable_m5635_MethodInfo,
	&LayoutGroup_OnDisable_m5636_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo,
	&LayoutGroup_GetTotalMinSize_m5638_MethodInfo,
	&LayoutGroup_GetTotalPreferredSize_m5639_MethodInfo,
	&LayoutGroup_GetTotalFlexibleSize_m5640_MethodInfo,
	&LayoutGroup_GetStartOffset_m5641_MethodInfo,
	&LayoutGroup_SetLayoutInputForAxis_m5642_MethodInfo,
	&LayoutGroup_SetChildAlongAxis_m5643_MethodInfo,
	&LayoutGroup_get_isRootLayoutGroup_m5644_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo,
	&LayoutGroup_SetProperty_m6265_MethodInfo,
	&LayoutGroup_SetDirty_m5647_MethodInfo,
	NULL
};
extern Il2CppType RectOffset_t1321_0_0_4;
FieldInfo LayoutGroup_t1317____m_Padding_2_FieldInfo = 
{
	"m_Padding"/* name */
	, &RectOffset_t1321_0_0_4/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_Padding_2)/* offset */
	, 298/* custom_attributes_cache */

};
extern Il2CppType TextAnchor_t1410_0_0_4;
FieldInfo LayoutGroup_t1317____m_ChildAlignment_3_FieldInfo = 
{
	"m_ChildAlignment"/* name */
	, &TextAnchor_t1410_0_0_4/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_ChildAlignment_3)/* offset */
	, 299/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_129;
FieldInfo LayoutGroup_t1317____m_Rect_4_FieldInfo = 
{
	"m_Rect"/* name */
	, &RectTransform_t1227_0_0_129/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_Rect_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DrivenRectTransformTracker_t1280_0_0_4;
FieldInfo LayoutGroup_t1317____m_Tracker_5_FieldInfo = 
{
	"m_Tracker"/* name */
	, &DrivenRectTransformTracker_t1280_0_0_4/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_Tracker_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo LayoutGroup_t1317____m_TotalMinSize_6_FieldInfo = 
{
	"m_TotalMinSize"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_TotalMinSize_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo LayoutGroup_t1317____m_TotalPreferredSize_7_FieldInfo = 
{
	"m_TotalPreferredSize"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_TotalPreferredSize_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo LayoutGroup_t1317____m_TotalFlexibleSize_8_FieldInfo = 
{
	"m_TotalFlexibleSize"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_TotalFlexibleSize_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t1322_0_0_129;
FieldInfo LayoutGroup_t1317____m_RectChildren_9_FieldInfo = 
{
	"m_RectChildren"/* name */
	, &List_1_t1322_0_0_129/* type */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutGroup_t1317, ___m_RectChildren_9)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* LayoutGroup_t1317_FieldInfos[] =
{
	&LayoutGroup_t1317____m_Padding_2_FieldInfo,
	&LayoutGroup_t1317____m_ChildAlignment_3_FieldInfo,
	&LayoutGroup_t1317____m_Rect_4_FieldInfo,
	&LayoutGroup_t1317____m_Tracker_5_FieldInfo,
	&LayoutGroup_t1317____m_TotalMinSize_6_FieldInfo,
	&LayoutGroup_t1317____m_TotalPreferredSize_7_FieldInfo,
	&LayoutGroup_t1317____m_TotalFlexibleSize_8_FieldInfo,
	&LayoutGroup_t1317____m_RectChildren_9_FieldInfo,
	NULL
};
extern MethodInfo LayoutGroup_get_padding_m5621_MethodInfo;
extern MethodInfo LayoutGroup_set_padding_m5622_MethodInfo;
static PropertyInfo LayoutGroup_t1317____padding_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "padding"/* name */
	, &LayoutGroup_get_padding_m5621_MethodInfo/* get */
	, &LayoutGroup_set_padding_m5622_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutGroup_get_childAlignment_m5623_MethodInfo;
extern MethodInfo LayoutGroup_set_childAlignment_m5624_MethodInfo;
static PropertyInfo LayoutGroup_t1317____childAlignment_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "childAlignment"/* name */
	, &LayoutGroup_get_childAlignment_m5623_MethodInfo/* get */
	, &LayoutGroup_set_childAlignment_m5624_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutGroup_get_rectTransform_m5625_MethodInfo;
static PropertyInfo LayoutGroup_t1317____rectTransform_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &LayoutGroup_get_rectTransform_m5625_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutGroup_get_rectChildren_m5626_MethodInfo;
static PropertyInfo LayoutGroup_t1317____rectChildren_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "rectChildren"/* name */
	, &LayoutGroup_get_rectChildren_m5626_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo LayoutGroup_t1317____minWidth_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutGroup_get_minWidth_m5628_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo LayoutGroup_t1317____preferredWidth_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutGroup_get_preferredWidth_m5629_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo LayoutGroup_t1317____flexibleWidth_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutGroup_get_flexibleWidth_m5630_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo LayoutGroup_t1317____minHeight_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutGroup_get_minHeight_m5631_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo LayoutGroup_t1317____preferredHeight_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutGroup_get_preferredHeight_m5632_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo LayoutGroup_t1317____flexibleHeight_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutGroup_get_flexibleHeight_m5633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo LayoutGroup_t1317____layoutPriority_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutGroup_get_layoutPriority_m5634_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo LayoutGroup_get_isRootLayoutGroup_m5644_MethodInfo;
static PropertyInfo LayoutGroup_t1317____isRootLayoutGroup_PropertyInfo = 
{
	&LayoutGroup_t1317_il2cpp_TypeInfo/* parent */
	, "isRootLayoutGroup"/* name */
	, &LayoutGroup_get_isRootLayoutGroup_m5644_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* LayoutGroup_t1317_PropertyInfos[] =
{
	&LayoutGroup_t1317____padding_PropertyInfo,
	&LayoutGroup_t1317____childAlignment_PropertyInfo,
	&LayoutGroup_t1317____rectTransform_PropertyInfo,
	&LayoutGroup_t1317____rectChildren_PropertyInfo,
	&LayoutGroup_t1317____minWidth_PropertyInfo,
	&LayoutGroup_t1317____preferredWidth_PropertyInfo,
	&LayoutGroup_t1317____flexibleWidth_PropertyInfo,
	&LayoutGroup_t1317____minHeight_PropertyInfo,
	&LayoutGroup_t1317____preferredHeight_PropertyInfo,
	&LayoutGroup_t1317____flexibleHeight_PropertyInfo,
	&LayoutGroup_t1317____layoutPriority_PropertyInfo,
	&LayoutGroup_t1317____isRootLayoutGroup_PropertyInfo,
	NULL
};
static Il2CppMethodReference LayoutGroup_t1317_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&LayoutGroup_OnEnable_m5635_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&LayoutGroup_OnDisable_m5636_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m5627_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m6262_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m6263_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m6264_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m5627_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo,
};
static bool LayoutGroup_t1317_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutGroup_t1317_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t1367_0_0_0,
	&ILayoutController_t1414_0_0_0,
	&ILayoutGroup_t1412_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutGroup_t1317_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t1367_0_0_0, 15},
	{ &ILayoutController_t1414_0_0_0, 24},
	{ &ILayoutGroup_t1412_0_0_0, 26},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType LayoutGroup_t1317_1_0_0;
struct LayoutGroup_t1317;
const Il2CppTypeDefinitionMetadata LayoutGroup_t1317_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutGroup_t1317_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutGroup_t1317_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, LayoutGroup_t1317_VTable/* vtableMethods */
	, LayoutGroup_t1317_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LayoutGroup_t1317_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutGroup_t1317_MethodInfos/* methods */
	, LayoutGroup_t1317_PropertyInfos/* properties */
	, LayoutGroup_t1317_FieldInfos/* fields */
	, NULL/* events */
	, &LayoutGroup_t1317_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 297/* custom_attributes_cache */
	, &LayoutGroup_t1317_0_0_0/* byval_arg */
	, &LayoutGroup_t1317_1_0_0/* this_arg */
	, &LayoutGroup_t1317_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutGroup_t1317)/* instance_size */
	, sizeof (LayoutGroup_t1317)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 32/* method_count */
	, 12/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 38/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// Metadata Definition UnityEngine.UI.LayoutRebuilder
extern TypeInfo LayoutRebuilder_t1325_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder__ctor_m5648_ParameterInfos[] = 
{
	{"controller", 0, 134218377, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.ctor(UnityEngine.RectTransform)
MethodInfo LayoutRebuilder__ctor_m5648_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutRebuilder__ctor_m5648/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder__ctor_m5648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
MethodInfo LayoutRebuilder__cctor_m5649_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&LayoutRebuilder__cctor_m5649/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType CanvasUpdate_t1216_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m5650_ParameterInfos[] = 
{
	{"executing", 0, 134218378, 0, &CanvasUpdate_t1216_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::UnityEngine.UI.ICanvasElement.Rebuild(UnityEngine.UI.CanvasUpdate)
MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m5650_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.Rebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m5650/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m5650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_ReapplyDrivenProperties_m5651_ParameterInfos[] = 
{
	{"driven", 0, 134218379, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
MethodInfo LayoutRebuilder_ReapplyDrivenProperties_m5651_MethodInfo = 
{
	"ReapplyDrivenProperties"/* name */
	, (methodPointerType)&LayoutRebuilder_ReapplyDrivenProperties_m5651/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_ReapplyDrivenProperties_m5651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t809_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
MethodInfo LayoutRebuilder_get_transform_m5652_MethodInfo = 
{
	"get_transform"/* name */
	, (methodPointerType)&LayoutRebuilder_get_transform_m5652/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t809_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
MethodInfo LayoutRebuilder_IsDestroyed_m5653_MethodInfo = 
{
	"IsDestroyed"/* name */
	, (methodPointerType)&LayoutRebuilder_IsDestroyed_m5653/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1366_0_0_0;
extern Il2CppType List_1_t1366_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_StripDisabledBehavioursFromList_m5654_ParameterInfos[] = 
{
	{"components", 0, 134218380, 0, &List_1_t1366_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
MethodInfo LayoutRebuilder_StripDisabledBehavioursFromList_m5654_MethodInfo = 
{
	"StripDisabledBehavioursFromList"/* name */
	, (methodPointerType)&LayoutRebuilder_StripDisabledBehavioursFromList_m5654/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_StripDisabledBehavioursFromList_m5654_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType UnityAction_1_t1323_0_0_0;
extern Il2CppType UnityAction_1_t1323_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_PerformLayoutControl_m5655_ParameterInfos[] = 
{
	{"rect", 0, 134218381, 0, &RectTransform_t1227_0_0_0},
	{"action", 1, 134218382, 0, &UnityAction_1_t1323_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
MethodInfo LayoutRebuilder_PerformLayoutControl_m5655_MethodInfo = 
{
	"PerformLayoutControl"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutControl_m5655/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_PerformLayoutControl_m5655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType UnityAction_1_t1323_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_PerformLayoutCalculation_m5656_ParameterInfos[] = 
{
	{"rect", 0, 134218383, 0, &RectTransform_t1227_0_0_0},
	{"action", 1, 134218384, 0, &UnityAction_1_t1323_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
MethodInfo LayoutRebuilder_PerformLayoutCalculation_m5656_MethodInfo = 
{
	"PerformLayoutCalculation"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutCalculation_m5656/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_PerformLayoutCalculation_m5656_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_MarkLayoutForRebuild_m5657_ParameterInfos[] = 
{
	{"rect", 0, 134218385, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
MethodInfo LayoutRebuilder_MarkLayoutForRebuild_m5657_MethodInfo = 
{
	"MarkLayoutForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutForRebuild_m5657/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_MarkLayoutForRebuild_m5657_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_ValidLayoutGroup_m5658_ParameterInfos[] = 
{
	{"parent", 0, 134218386, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
MethodInfo LayoutRebuilder_ValidLayoutGroup_m5658_MethodInfo = 
{
	"ValidLayoutGroup"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidLayoutGroup_m5658/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_ValidLayoutGroup_m5658_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_ValidController_m5659_ParameterInfos[] = 
{
	{"layoutRoot", 0, 134218387, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
MethodInfo LayoutRebuilder_ValidController_m5659_MethodInfo = 
{
	"ValidController"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidController_m5659/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_ValidController_m5659_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_MarkLayoutRootForRebuild_m5660_ParameterInfos[] = 
{
	{"controller", 0, 134218388, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
MethodInfo LayoutRebuilder_MarkLayoutRootForRebuild_m5660_MethodInfo = 
{
	"MarkLayoutRootForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutRootForRebuild_m5660/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_MarkLayoutRootForRebuild_m5660_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LayoutRebuilder_t1325_0_0_0;
extern Il2CppType LayoutRebuilder_t1325_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_Equals_m5661_ParameterInfos[] = 
{
	{"other", 0, 134218389, 0, &LayoutRebuilder_t1325_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_LayoutRebuilder_t1325 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(UnityEngine.UI.LayoutRebuilder)
MethodInfo LayoutRebuilder_Equals_m5661_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&LayoutRebuilder_Equals_m5661/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_LayoutRebuilder_t1325/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_Equals_m5661_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
MethodInfo LayoutRebuilder_GetHashCode_m5662_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&LayoutRebuilder_GetHashCode_m5662/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
MethodInfo LayoutRebuilder_ToString_m5663_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&LayoutRebuilder_ToString_m5663/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Component_t230_0_0_0;
extern Il2CppType Component_t230_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__9_m5664_ParameterInfos[] = 
{
	{"e", 0, 134218390, 0, &Component_t230_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
MethodInfo LayoutRebuilder_U3CRebuildU3Em__9_m5664_MethodInfo = 
{
	"<Rebuild>m__9"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__9_m5664/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__9_m5664_ParameterInfos/* parameters */
	, 305/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Component_t230_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__A_m5665_ParameterInfos[] = 
{
	{"e", 0, 134218391, 0, &Component_t230_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
MethodInfo LayoutRebuilder_U3CRebuildU3Em__A_m5665_MethodInfo = 
{
	"<Rebuild>m__A"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__A_m5665/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__A_m5665_ParameterInfos/* parameters */
	, 306/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Component_t230_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__B_m5666_ParameterInfos[] = 
{
	{"e", 0, 134218392, 0, &Component_t230_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
MethodInfo LayoutRebuilder_U3CRebuildU3Em__B_m5666_MethodInfo = 
{
	"<Rebuild>m__B"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__B_m5666/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__B_m5666_ParameterInfos/* parameters */
	, 307/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Component_t230_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__C_m5667_ParameterInfos[] = 
{
	{"e", 0, 134218393, 0, &Component_t230_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__C(UnityEngine.Component)
MethodInfo LayoutRebuilder_U3CRebuildU3Em__C_m5667_MethodInfo = 
{
	"<Rebuild>m__C"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__C_m5667/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_U3CRebuildU3Em__C_m5667_ParameterInfos/* parameters */
	, 308/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Component_t230_0_0_0;
static ParameterInfo LayoutRebuilder_t1325_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m5668_ParameterInfos[] = 
{
	{"e", 0, 134218394, 0, &Component_t230_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__D(UnityEngine.Component)
MethodInfo LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m5668_MethodInfo = 
{
	"<StripDisabledBehavioursFromList>m__D"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m5668/* method */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, LayoutRebuilder_t1325_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m5668_ParameterInfos/* parameters */
	, 309/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LayoutRebuilder_t1325_MethodInfos[] =
{
	&LayoutRebuilder__ctor_m5648_MethodInfo,
	&LayoutRebuilder__cctor_m5649_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m5650_MethodInfo,
	&LayoutRebuilder_ReapplyDrivenProperties_m5651_MethodInfo,
	&LayoutRebuilder_get_transform_m5652_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m5653_MethodInfo,
	&LayoutRebuilder_StripDisabledBehavioursFromList_m5654_MethodInfo,
	&LayoutRebuilder_PerformLayoutControl_m5655_MethodInfo,
	&LayoutRebuilder_PerformLayoutCalculation_m5656_MethodInfo,
	&LayoutRebuilder_MarkLayoutForRebuild_m5657_MethodInfo,
	&LayoutRebuilder_ValidLayoutGroup_m5658_MethodInfo,
	&LayoutRebuilder_ValidController_m5659_MethodInfo,
	&LayoutRebuilder_MarkLayoutRootForRebuild_m5660_MethodInfo,
	&LayoutRebuilder_Equals_m5661_MethodInfo,
	&LayoutRebuilder_GetHashCode_m5662_MethodInfo,
	&LayoutRebuilder_ToString_m5663_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__9_m5664_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__A_m5665_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__B_m5666_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__C_m5667_MethodInfo,
	&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m5668_MethodInfo,
	NULL
};
extern Il2CppType RectTransform_t1227_0_0_33;
FieldInfo LayoutRebuilder_t1325____m_ToRebuild_0_FieldInfo = 
{
	"m_ToRebuild"/* name */
	, &RectTransform_t1227_0_0_33/* type */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutRebuilder_t1325, ___m_ToRebuild_0) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_33;
FieldInfo LayoutRebuilder_t1325____m_CachedHasFromTrasnform_1_FieldInfo = 
{
	"m_CachedHasFromTrasnform"/* name */
	, &Int32_t189_0_0_33/* type */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutRebuilder_t1325, ___m_CachedHasFromTrasnform_1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1323_0_0_17;
FieldInfo LayoutRebuilder_t1325____U3CU3Ef__amU24cache2_2_FieldInfo = 
{
	"<>f__am$cache2"/* name */
	, &UnityAction_1_t1323_0_0_17/* type */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutRebuilder_t1325_StaticFields, ___U3CU3Ef__amU24cache2_2)/* offset */
	, 300/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1323_0_0_17;
FieldInfo LayoutRebuilder_t1325____U3CU3Ef__amU24cache3_3_FieldInfo = 
{
	"<>f__am$cache3"/* name */
	, &UnityAction_1_t1323_0_0_17/* type */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutRebuilder_t1325_StaticFields, ___U3CU3Ef__amU24cache3_3)/* offset */
	, 301/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1323_0_0_17;
FieldInfo LayoutRebuilder_t1325____U3CU3Ef__amU24cache4_4_FieldInfo = 
{
	"<>f__am$cache4"/* name */
	, &UnityAction_1_t1323_0_0_17/* type */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutRebuilder_t1325_StaticFields, ___U3CU3Ef__amU24cache4_4)/* offset */
	, 302/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1323_0_0_17;
FieldInfo LayoutRebuilder_t1325____U3CU3Ef__amU24cache5_5_FieldInfo = 
{
	"<>f__am$cache5"/* name */
	, &UnityAction_1_t1323_0_0_17/* type */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutRebuilder_t1325_StaticFields, ___U3CU3Ef__amU24cache5_5)/* offset */
	, 303/* custom_attributes_cache */

};
extern Il2CppType Predicate_1_t1324_0_0_17;
FieldInfo LayoutRebuilder_t1325____U3CU3Ef__amU24cache6_6_FieldInfo = 
{
	"<>f__am$cache6"/* name */
	, &Predicate_1_t1324_0_0_17/* type */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutRebuilder_t1325_StaticFields, ___U3CU3Ef__amU24cache6_6)/* offset */
	, 304/* custom_attributes_cache */

};
static FieldInfo* LayoutRebuilder_t1325_FieldInfos[] =
{
	&LayoutRebuilder_t1325____m_ToRebuild_0_FieldInfo,
	&LayoutRebuilder_t1325____m_CachedHasFromTrasnform_1_FieldInfo,
	&LayoutRebuilder_t1325____U3CU3Ef__amU24cache2_2_FieldInfo,
	&LayoutRebuilder_t1325____U3CU3Ef__amU24cache3_3_FieldInfo,
	&LayoutRebuilder_t1325____U3CU3Ef__amU24cache4_4_FieldInfo,
	&LayoutRebuilder_t1325____U3CU3Ef__amU24cache5_5_FieldInfo,
	&LayoutRebuilder_t1325____U3CU3Ef__amU24cache6_6_FieldInfo,
	NULL
};
extern MethodInfo LayoutRebuilder_get_transform_m5652_MethodInfo;
static PropertyInfo LayoutRebuilder_t1325____transform_PropertyInfo = 
{
	&LayoutRebuilder_t1325_il2cpp_TypeInfo/* parent */
	, "transform"/* name */
	, &LayoutRebuilder_get_transform_m5652_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* LayoutRebuilder_t1325_PropertyInfos[] =
{
	&LayoutRebuilder_t1325____transform_PropertyInfo,
	NULL
};
extern MethodInfo LayoutRebuilder_GetHashCode_m5662_MethodInfo;
extern MethodInfo LayoutRebuilder_ToString_m5663_MethodInfo;
extern MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m5650_MethodInfo;
extern MethodInfo LayoutRebuilder_IsDestroyed_m5653_MethodInfo;
extern MethodInfo LayoutRebuilder_Equals_m5661_MethodInfo;
static Il2CppMethodReference LayoutRebuilder_t1325_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&LayoutRebuilder_GetHashCode_m5662_MethodInfo,
	&LayoutRebuilder_ToString_m5663_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m5650_MethodInfo,
	&LayoutRebuilder_get_transform_m5652_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m5653_MethodInfo,
	&LayoutRebuilder_Equals_m5661_MethodInfo,
};
static bool LayoutRebuilder_t1325_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEquatable_1_t1469_0_0_0;
static const Il2CppType* LayoutRebuilder_t1325_InterfacesTypeInfos[] = 
{
	&ICanvasElement_t1360_0_0_0,
	&IEquatable_1_t1469_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutRebuilder_t1325_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t1360_0_0_0, 4},
	{ &IEquatable_1_t1469_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType LayoutRebuilder_t1325_1_0_0;
const Il2CppTypeDefinitionMetadata LayoutRebuilder_t1325_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutRebuilder_t1325_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutRebuilder_t1325_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, LayoutRebuilder_t1325_VTable/* vtableMethods */
	, LayoutRebuilder_t1325_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LayoutRebuilder_t1325_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutRebuilder"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutRebuilder_t1325_MethodInfos/* methods */
	, LayoutRebuilder_t1325_PropertyInfos/* properties */
	, LayoutRebuilder_t1325_FieldInfos/* fields */
	, NULL/* events */
	, &LayoutRebuilder_t1325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutRebuilder_t1325_0_0_0/* byval_arg */
	, &LayoutRebuilder_t1325_1_0_0/* this_arg */
	, &LayoutRebuilder_t1325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutRebuilder_t1325)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayoutRebuilder_t1325)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutRebuilder_t1325_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 265/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// Metadata Definition UnityEngine.UI.LayoutUtility
extern TypeInfo LayoutUtility_t1327_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetMinSize_m5669_ParameterInfos[] = 
{
	{"rect", 0, 134218395, 0, &RectTransform_t1227_0_0_0},
	{"axis", 1, 134218396, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
MethodInfo LayoutUtility_GetMinSize_m5669_MethodInfo = 
{
	"GetMinSize"/* name */
	, (methodPointerType)&LayoutUtility_GetMinSize_m5669/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t_Int32_t189/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetMinSize_m5669_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetPreferredSize_m5670_ParameterInfos[] = 
{
	{"rect", 0, 134218397, 0, &RectTransform_t1227_0_0_0},
	{"axis", 1, 134218398, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
MethodInfo LayoutUtility_GetPreferredSize_m5670_MethodInfo = 
{
	"GetPreferredSize"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredSize_m5670/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t_Int32_t189/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetPreferredSize_m5670_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetFlexibleSize_m5671_ParameterInfos[] = 
{
	{"rect", 0, 134218399, 0, &RectTransform_t1227_0_0_0},
	{"axis", 1, 134218400, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
MethodInfo LayoutUtility_GetFlexibleSize_m5671_MethodInfo = 
{
	"GetFlexibleSize"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleSize_m5671/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t_Int32_t189/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetFlexibleSize_m5671_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetMinWidth_m5672_ParameterInfos[] = 
{
	{"rect", 0, 134218401, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
MethodInfo LayoutUtility_GetMinWidth_m5672_MethodInfo = 
{
	"GetMinWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetMinWidth_m5672/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetMinWidth_m5672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetPreferredWidth_m5673_ParameterInfos[] = 
{
	{"rect", 0, 134218402, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
MethodInfo LayoutUtility_GetPreferredWidth_m5673_MethodInfo = 
{
	"GetPreferredWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredWidth_m5673/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetPreferredWidth_m5673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetFlexibleWidth_m5674_ParameterInfos[] = 
{
	{"rect", 0, 134218403, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
MethodInfo LayoutUtility_GetFlexibleWidth_m5674_MethodInfo = 
{
	"GetFlexibleWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleWidth_m5674/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetFlexibleWidth_m5674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetMinHeight_m5675_ParameterInfos[] = 
{
	{"rect", 0, 134218404, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
MethodInfo LayoutUtility_GetMinHeight_m5675_MethodInfo = 
{
	"GetMinHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetMinHeight_m5675/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetMinHeight_m5675_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetPreferredHeight_m5676_ParameterInfos[] = 
{
	{"rect", 0, 134218405, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
MethodInfo LayoutUtility_GetPreferredHeight_m5676_MethodInfo = 
{
	"GetPreferredHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredHeight_m5676/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetPreferredHeight_m5676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetFlexibleHeight_m5677_ParameterInfos[] = 
{
	{"rect", 0, 134218406, 0, &RectTransform_t1227_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
MethodInfo LayoutUtility_GetFlexibleHeight_m5677_MethodInfo = 
{
	"GetFlexibleHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleHeight_m5677/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetFlexibleHeight_m5677_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType Func_2_t1326_0_0_0;
extern Il2CppType Func_2_t1326_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetLayoutProperty_m5678_ParameterInfos[] = 
{
	{"rect", 0, 134218407, 0, &RectTransform_t1227_0_0_0},
	{"property", 1, 134218408, 0, &Func_2_t1326_0_0_0},
	{"defaultValue", 2, 134218409, 0, &Single_t202_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
MethodInfo LayoutUtility_GetLayoutProperty_m5678_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m5678/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t_Object_t_Single_t202/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetLayoutProperty_m5678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern Il2CppType Func_2_t1326_0_0_0;
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType ILayoutElement_t1367_1_0_2;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_GetLayoutProperty_m5679_ParameterInfos[] = 
{
	{"rect", 0, 134218410, 0, &RectTransform_t1227_0_0_0},
	{"property", 1, 134218411, 0, &Func_2_t1326_0_0_0},
	{"defaultValue", 2, 134218412, 0, &Single_t202_0_0_0},
	{"source", 3, 134218413, 0, &ILayoutElement_t1367_1_0_2},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t_Object_t_Single_t202_ILayoutElementU26_t1470 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
MethodInfo LayoutUtility_GetLayoutProperty_m5679_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m5679/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t_Object_t_Single_t202_ILayoutElementU26_t1470/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_GetLayoutProperty_m5679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetMinWidthU3Em__E_m5680_ParameterInfos[] = 
{
	{"e", 0, 134218414, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__E(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetMinWidthU3Em__E_m5680_MethodInfo = 
{
	"<GetMinWidth>m__E"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinWidthU3Em__E_m5680/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetMinWidthU3Em__E_m5680_ParameterInfos/* parameters */
	, 318/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetPreferredWidthU3Em__F_m5681_ParameterInfos[] = 
{
	{"e", 0, 134218415, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__F(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__F_m5681_MethodInfo = 
{
	"<GetPreferredWidth>m__F"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__F_m5681/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetPreferredWidthU3Em__F_m5681_ParameterInfos/* parameters */
	, 319/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetPreferredWidthU3Em__10_m5682_ParameterInfos[] = 
{
	{"e", 0, 134218416, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__10(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__10_m5682_MethodInfo = 
{
	"<GetPreferredWidth>m__10"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__10_m5682/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetPreferredWidthU3Em__10_m5682_ParameterInfos/* parameters */
	, 320/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m5683_ParameterInfos[] = 
{
	{"e", 0, 134218417, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__11(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetFlexibleWidthU3Em__11_m5683_MethodInfo = 
{
	"<GetFlexibleWidth>m__11"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m5683/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m5683_ParameterInfos/* parameters */
	, 321/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetMinHeightU3Em__12_m5684_ParameterInfos[] = 
{
	{"e", 0, 134218418, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__12(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetMinHeightU3Em__12_m5684_MethodInfo = 
{
	"<GetMinHeight>m__12"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinHeightU3Em__12_m5684/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetMinHeightU3Em__12_m5684_ParameterInfos/* parameters */
	, 322/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetPreferredHeightU3Em__13_m5685_ParameterInfos[] = 
{
	{"e", 0, 134218419, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__13(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__13_m5685_MethodInfo = 
{
	"<GetPreferredHeight>m__13"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__13_m5685/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetPreferredHeightU3Em__13_m5685_ParameterInfos/* parameters */
	, 323/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetPreferredHeightU3Em__14_m5686_ParameterInfos[] = 
{
	{"e", 0, 134218420, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__14(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__14_m5686_MethodInfo = 
{
	"<GetPreferredHeight>m__14"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__14_m5686/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetPreferredHeightU3Em__14_m5686_ParameterInfos/* parameters */
	, 324/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILayoutElement_t1367_0_0_0;
static ParameterInfo LayoutUtility_t1327_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m5687_ParameterInfos[] = 
{
	{"e", 0, 134218421, 0, &ILayoutElement_t1367_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__15(UnityEngine.UI.ILayoutElement)
MethodInfo LayoutUtility_U3CGetFlexibleHeightU3Em__15_m5687_MethodInfo = 
{
	"<GetFlexibleHeight>m__15"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m5687/* method */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Object_t/* invoker_method */
	, LayoutUtility_t1327_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m5687_ParameterInfos/* parameters */
	, 325/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LayoutUtility_t1327_MethodInfos[] =
{
	&LayoutUtility_GetMinSize_m5669_MethodInfo,
	&LayoutUtility_GetPreferredSize_m5670_MethodInfo,
	&LayoutUtility_GetFlexibleSize_m5671_MethodInfo,
	&LayoutUtility_GetMinWidth_m5672_MethodInfo,
	&LayoutUtility_GetPreferredWidth_m5673_MethodInfo,
	&LayoutUtility_GetFlexibleWidth_m5674_MethodInfo,
	&LayoutUtility_GetMinHeight_m5675_MethodInfo,
	&LayoutUtility_GetPreferredHeight_m5676_MethodInfo,
	&LayoutUtility_GetFlexibleHeight_m5677_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m5678_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m5679_MethodInfo,
	&LayoutUtility_U3CGetMinWidthU3Em__E_m5680_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__F_m5681_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__10_m5682_MethodInfo,
	&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m5683_MethodInfo,
	&LayoutUtility_U3CGetMinHeightU3Em__12_m5684_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__13_m5685_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__14_m5686_MethodInfo,
	&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m5687_MethodInfo,
	NULL
};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache0_0_FieldInfo = 
{
	"<>f__am$cache0"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache0_0)/* offset */
	, 310/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache1_1_FieldInfo = 
{
	"<>f__am$cache1"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache1_1)/* offset */
	, 311/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache2_2_FieldInfo = 
{
	"<>f__am$cache2"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache2_2)/* offset */
	, 312/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache3_3_FieldInfo = 
{
	"<>f__am$cache3"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache3_3)/* offset */
	, 313/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache4_4_FieldInfo = 
{
	"<>f__am$cache4"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache4_4)/* offset */
	, 314/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache5_5_FieldInfo = 
{
	"<>f__am$cache5"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache5_5)/* offset */
	, 315/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache6_6_FieldInfo = 
{
	"<>f__am$cache6"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache6_6)/* offset */
	, 316/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1326_0_0_17;
FieldInfo LayoutUtility_t1327____U3CU3Ef__amU24cache7_7_FieldInfo = 
{
	"<>f__am$cache7"/* name */
	, &Func_2_t1326_0_0_17/* type */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* parent */
	, offsetof(LayoutUtility_t1327_StaticFields, ___U3CU3Ef__amU24cache7_7)/* offset */
	, 317/* custom_attributes_cache */

};
static FieldInfo* LayoutUtility_t1327_FieldInfos[] =
{
	&LayoutUtility_t1327____U3CU3Ef__amU24cache0_0_FieldInfo,
	&LayoutUtility_t1327____U3CU3Ef__amU24cache1_1_FieldInfo,
	&LayoutUtility_t1327____U3CU3Ef__amU24cache2_2_FieldInfo,
	&LayoutUtility_t1327____U3CU3Ef__amU24cache3_3_FieldInfo,
	&LayoutUtility_t1327____U3CU3Ef__amU24cache4_4_FieldInfo,
	&LayoutUtility_t1327____U3CU3Ef__amU24cache5_5_FieldInfo,
	&LayoutUtility_t1327____U3CU3Ef__amU24cache6_6_FieldInfo,
	&LayoutUtility_t1327____U3CU3Ef__amU24cache7_7_FieldInfo,
	NULL
};
static Il2CppMethodReference LayoutUtility_t1327_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool LayoutUtility_t1327_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType LayoutUtility_t1327_0_0_0;
extern Il2CppType LayoutUtility_t1327_1_0_0;
struct LayoutUtility_t1327;
const Il2CppTypeDefinitionMetadata LayoutUtility_t1327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutUtility_t1327_VTable/* vtableMethods */
	, LayoutUtility_t1327_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LayoutUtility_t1327_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutUtility_t1327_MethodInfos/* methods */
	, NULL/* properties */
	, LayoutUtility_t1327_FieldInfos/* fields */
	, NULL/* events */
	, &LayoutUtility_t1327_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutUtility_t1327_0_0_0/* byval_arg */
	, &LayoutUtility_t1327_1_0_0/* this_arg */
	, &LayoutUtility_t1327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutUtility_t1327)/* instance_size */
	, sizeof (LayoutUtility_t1327)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutUtility_t1327_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.VerticalLayoutGroup
extern TypeInfo VerticalLayoutGroup_t1328_il2cpp_TypeInfo;
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
MethodInfo VerticalLayoutGroup__ctor_m5688_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VerticalLayoutGroup__ctor_m5688/* method */
	, &VerticalLayoutGroup_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m5689_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m5689/* method */
	, &VerticalLayoutGroup_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m5690_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputVertical_m5690/* method */
	, &VerticalLayoutGroup_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m5691_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutHorizontal_m5691/* method */
	, &VerticalLayoutGroup_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
MethodInfo VerticalLayoutGroup_SetLayoutVertical_m5692_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutVertical_m5692/* method */
	, &VerticalLayoutGroup_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VerticalLayoutGroup_t1328_MethodInfos[] =
{
	&VerticalLayoutGroup__ctor_m5688_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m5689_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m5690_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m5691_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m5692_MethodInfo,
	NULL
};
extern MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m5689_MethodInfo;
extern MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m5690_MethodInfo;
extern MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m5691_MethodInfo;
extern MethodInfo VerticalLayoutGroup_SetLayoutVertical_m5692_MethodInfo;
static Il2CppMethodReference VerticalLayoutGroup_t1328_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&LayoutGroup_OnEnable_m5635_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&LayoutGroup_OnDisable_m5636_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m5645_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m5637_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m5689_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m5690_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m5691_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m5692_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m5689_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m5690_MethodInfo,
	&LayoutGroup_get_minWidth_m5628_MethodInfo,
	&LayoutGroup_get_preferredWidth_m5629_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m5630_MethodInfo,
	&LayoutGroup_get_minHeight_m5631_MethodInfo,
	&LayoutGroup_get_preferredHeight_m5632_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m5633_MethodInfo,
	&LayoutGroup_get_layoutPriority_m5634_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m5691_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m5692_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m5646_MethodInfo,
};
static bool VerticalLayoutGroup_t1328_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair VerticalLayoutGroup_t1328_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t1367_0_0_0, 15},
	{ &ILayoutController_t1414_0_0_0, 24},
	{ &ILayoutGroup_t1412_0_0_0, 26},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType VerticalLayoutGroup_t1328_0_0_0;
extern Il2CppType VerticalLayoutGroup_t1328_1_0_0;
struct VerticalLayoutGroup_t1328;
const Il2CppTypeDefinitionMetadata VerticalLayoutGroup_t1328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalLayoutGroup_t1328_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t1319_0_0_0/* parent */
	, VerticalLayoutGroup_t1328_VTable/* vtableMethods */
	, VerticalLayoutGroup_t1328_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo VerticalLayoutGroup_t1328_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, VerticalLayoutGroup_t1328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &VerticalLayoutGroup_t1328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 326/* custom_attributes_cache */
	, &VerticalLayoutGroup_t1328_0_0_0/* byval_arg */
	, &VerticalLayoutGroup_t1328_1_0_0/* this_arg */
	, &VerticalLayoutGroup_t1328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalLayoutGroup_t1328)/* instance_size */
	, sizeof (VerticalLayoutGroup_t1328)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 38/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMaterialModifier
extern TypeInfo IMaterialModifier_t1384_il2cpp_TypeInfo;
extern Il2CppType Material_t989_0_0_0;
static ParameterInfo IMaterialModifier_t1384_IMaterialModifier_GetModifiedMaterial_m6266_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218422, 0, &Material_t989_0_0_0},
};
extern Il2CppType Material_t989_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.IMaterialModifier::GetModifiedMaterial(UnityEngine.Material)
MethodInfo IMaterialModifier_GetModifiedMaterial_m6266_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, NULL/* method */
	, &IMaterialModifier_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Material_t989_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IMaterialModifier_t1384_IMaterialModifier_GetModifiedMaterial_m6266_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IMaterialModifier_t1384_MethodInfos[] =
{
	&IMaterialModifier_GetModifiedMaterial_m6266_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType IMaterialModifier_t1384_0_0_0;
extern Il2CppType IMaterialModifier_t1384_1_0_0;
struct IMaterialModifier_t1384;
const Il2CppTypeDefinitionMetadata IMaterialModifier_t1384_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IMaterialModifier_t1384_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMaterialModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IMaterialModifier_t1384_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IMaterialModifier_t1384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMaterialModifier_t1384_0_0_0/* byval_arg */
	, &IMaterialModifier_t1384_1_0_0/* this_arg */
	, &IMaterialModifier_t1384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
// Metadata Definition UnityEngine.UI.Mask
extern TypeInfo Mask_t1329_il2cpp_TypeInfo;
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_MaskMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::.ctor()
MethodInfo Mask__ctor_m5693_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mask__ctor_m5693/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Graphic_t1233_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
MethodInfo Mask_get_graphic_m5694_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&Mask_get_graphic_m5694/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t1233_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::get_showMaskGraphic()
MethodInfo Mask_get_showMaskGraphic_m5695_MethodInfo = 
{
	"get_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_get_showMaskGraphic_m5695/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Mask_t1329_Mask_set_showMaskGraphic_m5696_ParameterInfos[] = 
{
	{"value", 0, 134218423, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::set_showMaskGraphic(System.Boolean)
MethodInfo Mask_set_showMaskGraphic_m5696_MethodInfo = 
{
	"set_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_set_showMaskGraphic_m5696/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Mask_t1329_Mask_set_showMaskGraphic_m5696_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RectTransform_t1227_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
MethodInfo Mask_get_rectTransform_m5697_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&Mask_get_rectTransform_m5697/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t1227_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::MaskEnabled()
MethodInfo Mask_MaskEnabled_m5698_MethodInfo = 
{
	"MaskEnabled"/* name */
	, (methodPointerType)&Mask_MaskEnabled_m5698/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnSiblingGraphicEnabledDisabled()
MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m5699_MethodInfo = 
{
	"OnSiblingGraphicEnabledDisabled"/* name */
	, (methodPointerType)&Mask_OnSiblingGraphicEnabledDisabled_m5699/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::NotifyMaskStateChanged()
MethodInfo Mask_NotifyMaskStateChanged_m5700_MethodInfo = 
{
	"NotifyMaskStateChanged"/* name */
	, (methodPointerType)&Mask_NotifyMaskStateChanged_m5700/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::ClearCachedMaterial()
MethodInfo Mask_ClearCachedMaterial_m5701_MethodInfo = 
{
	"ClearCachedMaterial"/* name */
	, (methodPointerType)&Mask_ClearCachedMaterial_m5701/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnEnable()
MethodInfo Mask_OnEnable_m5702_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Mask_OnEnable_m5702/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnDisable()
MethodInfo Mask_OnDisable_m5703_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Mask_OnDisable_m5703/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern Il2CppType Camera_t978_0_0_0;
static ParameterInfo Mask_t1329_Mask_IsRaycastLocationValid_m5704_ParameterInfos[] = 
{
	{"sp", 0, 134218424, 0, &Vector2_t739_0_0_0},
	{"eventCamera", 1, 134218425, 0, &Camera_t978_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Vector2_t739_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
MethodInfo Mask_IsRaycastLocationValid_m5704_MethodInfo = 
{
	"IsRaycastLocationValid"/* name */
	, (methodPointerType)&Mask_IsRaycastLocationValid_m5704/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Vector2_t739_Object_t/* invoker_method */
	, Mask_t1329_Mask_IsRaycastLocationValid_m5704_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Material_t989_0_0_0;
static ParameterInfo Mask_t1329_Mask_GetModifiedMaterial_m5705_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218426, 0, &Material_t989_0_0_0},
};
extern Il2CppType Material_t989_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
MethodInfo Mask_GetModifiedMaterial_m5705_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, (methodPointerType)&Mask_GetModifiedMaterial_m5705/* method */
	, &Mask_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Material_t989_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mask_t1329_Mask_GetModifiedMaterial_m5705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Mask_t1329_MethodInfos[] =
{
	&Mask__ctor_m5693_MethodInfo,
	&Mask_get_graphic_m5694_MethodInfo,
	&Mask_get_showMaskGraphic_m5695_MethodInfo,
	&Mask_set_showMaskGraphic_m5696_MethodInfo,
	&Mask_get_rectTransform_m5697_MethodInfo,
	&Mask_MaskEnabled_m5698_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m5699_MethodInfo,
	&Mask_NotifyMaskStateChanged_m5700_MethodInfo,
	&Mask_ClearCachedMaterial_m5701_MethodInfo,
	&Mask_OnEnable_m5702_MethodInfo,
	&Mask_OnDisable_m5703_MethodInfo,
	&Mask_IsRaycastLocationValid_m5704_MethodInfo,
	&Mask_GetModifiedMaterial_m5705_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Mask_t1329____m_ShowMaskGraphic_2_FieldInfo = 
{
	"m_ShowMaskGraphic"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Mask_t1329_il2cpp_TypeInfo/* parent */
	, offsetof(Mask_t1329, ___m_ShowMaskGraphic_2)/* offset */
	, 328/* custom_attributes_cache */

};
extern Il2CppType Material_t989_0_0_1;
FieldInfo Mask_t1329____m_RenderMaterial_3_FieldInfo = 
{
	"m_RenderMaterial"/* name */
	, &Material_t989_0_0_1/* type */
	, &Mask_t1329_il2cpp_TypeInfo/* parent */
	, offsetof(Mask_t1329, ___m_RenderMaterial_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Graphic_t1233_0_0_1;
FieldInfo Mask_t1329____m_Graphic_4_FieldInfo = 
{
	"m_Graphic"/* name */
	, &Graphic_t1233_0_0_1/* type */
	, &Mask_t1329_il2cpp_TypeInfo/* parent */
	, offsetof(Mask_t1329, ___m_Graphic_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RectTransform_t1227_0_0_1;
FieldInfo Mask_t1329____m_RectTransform_5_FieldInfo = 
{
	"m_RectTransform"/* name */
	, &RectTransform_t1227_0_0_1/* type */
	, &Mask_t1329_il2cpp_TypeInfo/* parent */
	, offsetof(Mask_t1329, ___m_RectTransform_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Mask_t1329_FieldInfos[] =
{
	&Mask_t1329____m_ShowMaskGraphic_2_FieldInfo,
	&Mask_t1329____m_RenderMaterial_3_FieldInfo,
	&Mask_t1329____m_Graphic_4_FieldInfo,
	&Mask_t1329____m_RectTransform_5_FieldInfo,
	NULL
};
extern MethodInfo Mask_get_graphic_m5694_MethodInfo;
static PropertyInfo Mask_t1329____graphic_PropertyInfo = 
{
	&Mask_t1329_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &Mask_get_graphic_m5694_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Mask_get_showMaskGraphic_m5695_MethodInfo;
extern MethodInfo Mask_set_showMaskGraphic_m5696_MethodInfo;
static PropertyInfo Mask_t1329____showMaskGraphic_PropertyInfo = 
{
	&Mask_t1329_il2cpp_TypeInfo/* parent */
	, "showMaskGraphic"/* name */
	, &Mask_get_showMaskGraphic_m5695_MethodInfo/* get */
	, &Mask_set_showMaskGraphic_m5696_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Mask_get_rectTransform_m5697_MethodInfo;
static PropertyInfo Mask_t1329____rectTransform_PropertyInfo = 
{
	&Mask_t1329_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &Mask_get_rectTransform_m5697_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Mask_t1329_PropertyInfos[] =
{
	&Mask_t1329____graphic_PropertyInfo,
	&Mask_t1329____showMaskGraphic_PropertyInfo,
	&Mask_t1329____rectTransform_PropertyInfo,
	NULL
};
extern MethodInfo Mask_OnEnable_m5702_MethodInfo;
extern MethodInfo Mask_OnDisable_m5703_MethodInfo;
extern MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m5699_MethodInfo;
extern MethodInfo Mask_MaskEnabled_m5698_MethodInfo;
extern MethodInfo Mask_IsRaycastLocationValid_m5704_MethodInfo;
extern MethodInfo Mask_GetModifiedMaterial_m5705_MethodInfo;
static Il2CppMethodReference Mask_t1329_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&Mask_OnEnable_m5702_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&Mask_OnDisable_m5703_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m5699_MethodInfo,
	&Mask_MaskEnabled_m5698_MethodInfo,
	&Mask_IsRaycastLocationValid_m5704_MethodInfo,
	&Mask_GetModifiedMaterial_m5705_MethodInfo,
	&Mask_MaskEnabled_m5698_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m5699_MethodInfo,
	&Mask_IsRaycastLocationValid_m5704_MethodInfo,
	&Mask_GetModifiedMaterial_m5705_MethodInfo,
};
static bool Mask_t1329_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IGraphicEnabledDisabled_t1385_0_0_0;
extern Il2CppType IMask_t1403_0_0_0;
extern Il2CppType ICanvasRaycastFilter_t1388_0_0_0;
static const Il2CppType* Mask_t1329_InterfacesTypeInfos[] = 
{
	&IGraphicEnabledDisabled_t1385_0_0_0,
	&IMask_t1403_0_0_0,
	&ICanvasRaycastFilter_t1388_0_0_0,
	&IMaterialModifier_t1384_0_0_0,
};
static Il2CppInterfaceOffsetPair Mask_t1329_InterfacesOffsets[] = 
{
	{ &IGraphicEnabledDisabled_t1385_0_0_0, 15},
	{ &IMask_t1403_0_0_0, 16},
	{ &ICanvasRaycastFilter_t1388_0_0_0, 17},
	{ &IMaterialModifier_t1384_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Mask_t1329_0_0_0;
extern Il2CppType Mask_t1329_1_0_0;
struct Mask_t1329;
const Il2CppTypeDefinitionMetadata Mask_t1329_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Mask_t1329_InterfacesTypeInfos/* implementedInterfaces */
	, Mask_t1329_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, Mask_t1329_VTable/* vtableMethods */
	, Mask_t1329_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Mask_t1329_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mask"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Mask_t1329_MethodInfos/* methods */
	, Mask_t1329_PropertyInfos/* properties */
	, Mask_t1329_FieldInfos/* fields */
	, NULL/* events */
	, &Mask_t1329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 327/* custom_attributes_cache */
	, &Mask_t1329_0_0_0/* byval_arg */
	, &Mask_t1329_1_0_0/* this_arg */
	, &Mask_t1329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mask_t1329)/* instance_size */
	, sizeof (Mask_t1329)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1
extern TypeInfo IndexedSet_1_t1444_il2cpp_TypeInfo;
extern Il2CppGenericContainer IndexedSet_1_t1444_Il2CppGenericContainer;
extern TypeInfo IndexedSet_1_t1444_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull IndexedSet_1_t1444_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &IndexedSet_1_t1444_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* IndexedSet_1_t1444_Il2CppGenericParametersArray[1] = 
{
	&IndexedSet_1_t1444_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer IndexedSet_1_t1444_Il2CppGenericContainer = { { NULL, NULL }, NULL, &IndexedSet_1_t1444_il2cpp_TypeInfo, 1, 0, IndexedSet_1_t1444_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::.ctor()
MethodInfo IndexedSet_1__ctor_m6267_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m6268_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_Add_m6269_ParameterInfos[] = 
{
	{"item", 0, 134218427, 0, &IndexedSet_1_t1444_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Add(T)
MethodInfo IndexedSet_1_Add_m6269_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_Add_m6269_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_Remove_m6270_ParameterInfos[] = 
{
	{"item", 0, 134218428, 0, &IndexedSet_1_t1444_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Remove(T)
MethodInfo IndexedSet_1_Remove_m6270_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_Remove_m6270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1472_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1::GetEnumerator()
MethodInfo IndexedSet_1_GetEnumerator_m6271_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1472_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Clear()
MethodInfo IndexedSet_1_Clear_m6272_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_Contains_m6273_ParameterInfos[] = 
{
	{"item", 0, 134218429, 0, &IndexedSet_1_t1444_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Contains(T)
MethodInfo IndexedSet_1_Contains_m6273_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_Contains_m6273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TU5BU5D_t1473_0_0_0;
extern Il2CppType TU5BU5D_t1473_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_CopyTo_m6274_ParameterInfos[] = 
{
	{"array", 0, 134218430, 0, &TU5BU5D_t1473_0_0_0},
	{"arrayIndex", 1, 134218431, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::CopyTo(T[],System.Int32)
MethodInfo IndexedSet_1_CopyTo_m6274_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_CopyTo_m6274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::get_Count()
MethodInfo IndexedSet_1_get_Count_m6275_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::get_IsReadOnly()
MethodInfo IndexedSet_1_get_IsReadOnly_m6276_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_IndexOf_m6277_ParameterInfos[] = 
{
	{"item", 0, 134218432, 0, &IndexedSet_1_t1444_gp_0_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::IndexOf(T)
MethodInfo IndexedSet_1_IndexOf_m6277_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_IndexOf_m6277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_Insert_m6278_ParameterInfos[] = 
{
	{"index", 0, 134218433, 0, &Int32_t189_0_0_0},
	{"item", 1, 134218434, 0, &IndexedSet_1_t1444_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Insert(System.Int32,T)
MethodInfo IndexedSet_1_Insert_m6278_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_Insert_m6278_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_RemoveAt_m6279_ParameterInfos[] = 
{
	{"index", 0, 134218435, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAt(System.Int32)
MethodInfo IndexedSet_1_RemoveAt_m6279_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_RemoveAt_m6279_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_get_Item_m6280_ParameterInfos[] = 
{
	{"index", 0, 134218436, 0, &Int32_t189_0_0_0},
};
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
// T UnityEngine.UI.Collections.IndexedSet`1::get_Item(System.Int32)
MethodInfo IndexedSet_1_get_Item_m6280_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &IndexedSet_1_t1444_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_get_Item_m6280_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType IndexedSet_1_t1444_gp_0_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_set_Item_m6281_ParameterInfos[] = 
{
	{"index", 0, 134218437, 0, &Int32_t189_0_0_0},
	{"value", 1, 134218438, 0, &IndexedSet_1_t1444_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::set_Item(System.Int32,T)
MethodInfo IndexedSet_1_set_Item_m6281_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_set_Item_m6281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Predicate_1_t1474_0_0_0;
extern Il2CppType Predicate_1_t1474_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_RemoveAll_m6282_ParameterInfos[] = 
{
	{"match", 0, 134218439, 0, &Predicate_1_t1474_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAll(System.Predicate`1<T>)
MethodInfo IndexedSet_1_RemoveAll_m6282_MethodInfo = 
{
	"RemoveAll"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_RemoveAll_m6282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Comparison_1_t1475_0_0_0;
extern Il2CppType Comparison_1_t1475_0_0_0;
static ParameterInfo IndexedSet_1_t1444_IndexedSet_1_Sort_m6283_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134218440, 0, &Comparison_1_t1475_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Sort(System.Comparison`1<T>)
MethodInfo IndexedSet_1_Sort_m6283_MethodInfo = 
{
	"Sort"/* name */
	, NULL/* method */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t1444_IndexedSet_1_Sort_m6283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IndexedSet_1_t1444_MethodInfos[] =
{
	&IndexedSet_1__ctor_m6267_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m6268_MethodInfo,
	&IndexedSet_1_Add_m6269_MethodInfo,
	&IndexedSet_1_Remove_m6270_MethodInfo,
	&IndexedSet_1_GetEnumerator_m6271_MethodInfo,
	&IndexedSet_1_Clear_m6272_MethodInfo,
	&IndexedSet_1_Contains_m6273_MethodInfo,
	&IndexedSet_1_CopyTo_m6274_MethodInfo,
	&IndexedSet_1_get_Count_m6275_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m6276_MethodInfo,
	&IndexedSet_1_IndexOf_m6277_MethodInfo,
	&IndexedSet_1_Insert_m6278_MethodInfo,
	&IndexedSet_1_RemoveAt_m6279_MethodInfo,
	&IndexedSet_1_get_Item_m6280_MethodInfo,
	&IndexedSet_1_set_Item_m6281_MethodInfo,
	&IndexedSet_1_RemoveAll_m6282_MethodInfo,
	&IndexedSet_1_Sort_m6283_MethodInfo,
	NULL
};
extern Il2CppType List_1_t1476_0_0_33;
FieldInfo IndexedSet_1_t1444____m_List_0_FieldInfo = 
{
	"m_List"/* name */
	, &List_1_t1476_0_0_33/* type */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t1477_0_0_1;
FieldInfo IndexedSet_1_t1444____m_Dictionary_1_FieldInfo = 
{
	"m_Dictionary"/* name */
	, &Dictionary_2_t1477_0_0_1/* type */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* IndexedSet_1_t1444_FieldInfos[] =
{
	&IndexedSet_1_t1444____m_List_0_FieldInfo,
	&IndexedSet_1_t1444____m_Dictionary_1_FieldInfo,
	NULL
};
extern MethodInfo IndexedSet_1_get_Count_m6275_MethodInfo;
static PropertyInfo IndexedSet_1_t1444____Count_PropertyInfo = 
{
	&IndexedSet_1_t1444_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IndexedSet_1_get_Count_m6275_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IndexedSet_1_get_IsReadOnly_m6276_MethodInfo;
static PropertyInfo IndexedSet_1_t1444____IsReadOnly_PropertyInfo = 
{
	&IndexedSet_1_t1444_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &IndexedSet_1_get_IsReadOnly_m6276_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IndexedSet_1_get_Item_m6280_MethodInfo;
extern MethodInfo IndexedSet_1_set_Item_m6281_MethodInfo;
static PropertyInfo IndexedSet_1_t1444____Item_PropertyInfo = 
{
	&IndexedSet_1_t1444_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IndexedSet_1_get_Item_m6280_MethodInfo/* get */
	, &IndexedSet_1_set_Item_m6281_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IndexedSet_1_t1444_PropertyInfos[] =
{
	&IndexedSet_1_t1444____Count_PropertyInfo,
	&IndexedSet_1_t1444____IsReadOnly_PropertyInfo,
	&IndexedSet_1_t1444____Item_PropertyInfo,
	NULL
};
extern MethodInfo IndexedSet_1_IndexOf_m6277_MethodInfo;
extern MethodInfo IndexedSet_1_Insert_m6278_MethodInfo;
extern MethodInfo IndexedSet_1_RemoveAt_m6279_MethodInfo;
extern MethodInfo IndexedSet_1_Add_m6269_MethodInfo;
extern MethodInfo IndexedSet_1_Clear_m6272_MethodInfo;
extern MethodInfo IndexedSet_1_Contains_m6273_MethodInfo;
extern MethodInfo IndexedSet_1_CopyTo_m6274_MethodInfo;
extern MethodInfo IndexedSet_1_Remove_m6270_MethodInfo;
extern MethodInfo IndexedSet_1_GetEnumerator_m6271_MethodInfo;
extern MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m6268_MethodInfo;
static Il2CppMethodReference IndexedSet_1_t1444_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&IndexedSet_1_IndexOf_m6277_MethodInfo,
	&IndexedSet_1_Insert_m6278_MethodInfo,
	&IndexedSet_1_RemoveAt_m6279_MethodInfo,
	&IndexedSet_1_get_Item_m6280_MethodInfo,
	&IndexedSet_1_set_Item_m6281_MethodInfo,
	&IndexedSet_1_get_Count_m6275_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m6276_MethodInfo,
	&IndexedSet_1_Add_m6269_MethodInfo,
	&IndexedSet_1_Clear_m6272_MethodInfo,
	&IndexedSet_1_Contains_m6273_MethodInfo,
	&IndexedSet_1_CopyTo_m6274_MethodInfo,
	&IndexedSet_1_Remove_m6270_MethodInfo,
	&IndexedSet_1_GetEnumerator_m6271_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m6268_MethodInfo,
};
static bool IndexedSet_1_t1444_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IList_1_t1478_0_0_0;
extern Il2CppType ICollection_1_t1479_0_0_0;
extern Il2CppType IEnumerable_1_t1480_0_0_0;
extern Il2CppType IEnumerable_t38_0_0_0;
static const Il2CppType* IndexedSet_1_t1444_InterfacesTypeInfos[] = 
{
	&IList_1_t1478_0_0_0,
	&ICollection_1_t1479_0_0_0,
	&IEnumerable_1_t1480_0_0_0,
	&IEnumerable_t38_0_0_0,
};
static Il2CppInterfaceOffsetPair IndexedSet_1_t1444_InterfacesOffsets[] = 
{
	{ &IList_1_t1478_0_0_0, 4},
	{ &ICollection_1_t1479_0_0_0, 9},
	{ &IEnumerable_1_t1480_0_0_0, 16},
	{ &IEnumerable_t38_0_0_0, 17},
};
extern Il2CppType List_1_t1476_0_0_0;
extern Il2CppGenericMethod List_1__ctor_m6318_GenericMethod;
extern Il2CppType Dictionary_2_t1477_0_0_0;
extern Il2CppGenericMethod Dictionary_2__ctor_m6319_GenericMethod;
extern Il2CppGenericMethod IndexedSet_1_GetEnumerator_m6320_GenericMethod;
extern Il2CppGenericMethod Dictionary_2_ContainsKey_m6321_GenericMethod;
extern Il2CppGenericMethod List_1_Add_m6322_GenericMethod;
extern Il2CppGenericMethod List_1_get_Count_m6323_GenericMethod;
extern Il2CppGenericMethod Dictionary_2_Add_m6324_GenericMethod;
extern Il2CppGenericMethod Dictionary_2_TryGetValue_m6325_GenericMethod;
extern Il2CppGenericMethod IndexedSet_1_RemoveAt_m6326_GenericMethod;
extern Il2CppGenericMethod List_1_Clear_m6327_GenericMethod;
extern Il2CppGenericMethod Dictionary_2_Clear_m6328_GenericMethod;
extern Il2CppGenericMethod List_1_CopyTo_m6329_GenericMethod;
extern Il2CppGenericMethod List_1_get_Item_m6330_GenericMethod;
extern Il2CppGenericMethod Dictionary_2_Remove_m6331_GenericMethod;
extern Il2CppGenericMethod List_1_RemoveAt_m6332_GenericMethod;
extern Il2CppGenericMethod List_1_set_Item_m6333_GenericMethod;
extern Il2CppGenericMethod Dictionary_2_set_Item_m6334_GenericMethod;
extern Il2CppGenericMethod Predicate_1_Invoke_m6335_GenericMethod;
extern Il2CppGenericMethod IndexedSet_1_Remove_m6336_GenericMethod;
extern Il2CppGenericMethod List_1_Sort_m6337_GenericMethod;
static Il2CppRGCTXDefinition IndexedSet_1_t1444_RGCTXData[23] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &List_1_t1476_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m6318_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &Dictionary_2_t1477_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m6319_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_GetEnumerator_m6320_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_ContainsKey_m6321_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Add_m6322_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Count_m6323_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Add_m6324_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m6325_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_RemoveAt_m6326_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Clear_m6327_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Clear_m6328_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_CopyTo_m6329_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Item_m6330_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Remove_m6331_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_RemoveAt_m6332_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_set_Item_m6333_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m6334_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Predicate_1_Invoke_m6335_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_Remove_m6336_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Sort_m6337_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType IndexedSet_1_t1444_0_0_0;
extern Il2CppType IndexedSet_1_t1444_1_0_0;
struct IndexedSet_1_t1444;
const Il2CppTypeDefinitionMetadata IndexedSet_1_t1444_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IndexedSet_1_t1444_InterfacesTypeInfos/* implementedInterfaces */
	, IndexedSet_1_t1444_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IndexedSet_1_t1444_VTable/* vtableMethods */
	, IndexedSet_1_t1444_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, IndexedSet_1_t1444_RGCTXData/* rgctxDefinition */

};
TypeInfo IndexedSet_1_t1444_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexedSet`1"/* name */
	, "UnityEngine.UI.Collections"/* namespaze */
	, IndexedSet_1_t1444_MethodInfos/* methods */
	, IndexedSet_1_t1444_PropertyInfos/* properties */
	, IndexedSet_1_t1444_FieldInfos/* fields */
	, NULL/* events */
	, &IndexedSet_1_t1444_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 329/* custom_attributes_cache */
	, &IndexedSet_1_t1444_0_0_0/* byval_arg */
	, &IndexedSet_1_t1444_1_0_0/* this_arg */
	, &IndexedSet_1_t1444_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &IndexedSet_1_t1444_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPool.h"
// Metadata Definition UnityEngine.UI.CanvasListPool
extern TypeInfo CanvasListPool_t1332_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPoolMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::.cctor()
MethodInfo CanvasListPool__cctor_m5706_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CanvasListPool__cctor_m5706/* method */
	, &CanvasListPool_t1332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1368_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Canvas> UnityEngine.UI.CanvasListPool::Get()
MethodInfo CanvasListPool_Get_m5707_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&CanvasListPool_Get_m5707/* method */
	, &CanvasListPool_t1332_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1368_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1368_0_0_0;
extern Il2CppType List_1_t1368_0_0_0;
static ParameterInfo CanvasListPool_t1332_CanvasListPool_Release_m5708_ParameterInfos[] = 
{
	{"toRelease", 0, 134218441, 0, &List_1_t1368_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::Release(System.Collections.Generic.List`1<UnityEngine.Canvas>)
MethodInfo CanvasListPool_Release_m5708_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&CanvasListPool_Release_m5708/* method */
	, &CanvasListPool_t1332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, CanvasListPool_t1332_CanvasListPool_Release_m5708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1368_0_0_0;
static ParameterInfo CanvasListPool_t1332_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m5709_ParameterInfos[] = 
{
	{"l", 0, 134218442, 0, &List_1_t1368_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::<s_CanvasListPool>m__16(System.Collections.Generic.List`1<UnityEngine.Canvas>)
MethodInfo CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m5709_MethodInfo = 
{
	"<s_CanvasListPool>m__16"/* name */
	, (methodPointerType)&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m5709/* method */
	, &CanvasListPool_t1332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, CanvasListPool_t1332_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m5709_ParameterInfos/* parameters */
	, 331/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CanvasListPool_t1332_MethodInfos[] =
{
	&CanvasListPool__cctor_m5706_MethodInfo,
	&CanvasListPool_Get_m5707_MethodInfo,
	&CanvasListPool_Release_m5708_MethodInfo,
	&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m5709_MethodInfo,
	NULL
};
extern Il2CppType ObjectPool_1_t1330_0_0_49;
FieldInfo CanvasListPool_t1332____s_CanvasListPool_0_FieldInfo = 
{
	"s_CanvasListPool"/* name */
	, &ObjectPool_1_t1330_0_0_49/* type */
	, &CanvasListPool_t1332_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasListPool_t1332_StaticFields, ___s_CanvasListPool_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1331_0_0_17;
FieldInfo CanvasListPool_t1332____U3CU3Ef__amU24cache1_1_FieldInfo = 
{
	"<>f__am$cache1"/* name */
	, &UnityAction_1_t1331_0_0_17/* type */
	, &CanvasListPool_t1332_il2cpp_TypeInfo/* parent */
	, offsetof(CanvasListPool_t1332_StaticFields, ___U3CU3Ef__amU24cache1_1)/* offset */
	, 330/* custom_attributes_cache */

};
static FieldInfo* CanvasListPool_t1332_FieldInfos[] =
{
	&CanvasListPool_t1332____s_CanvasListPool_0_FieldInfo,
	&CanvasListPool_t1332____U3CU3Ef__amU24cache1_1_FieldInfo,
	NULL
};
static Il2CppMethodReference CanvasListPool_t1332_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool CanvasListPool_t1332_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType CanvasListPool_t1332_0_0_0;
extern Il2CppType CanvasListPool_t1332_1_0_0;
struct CanvasListPool_t1332;
const Il2CppTypeDefinitionMetadata CanvasListPool_t1332_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CanvasListPool_t1332_VTable/* vtableMethods */
	, CanvasListPool_t1332_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CanvasListPool_t1332_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasListPool_t1332_MethodInfos/* methods */
	, NULL/* properties */
	, CanvasListPool_t1332_FieldInfos/* fields */
	, NULL/* events */
	, &CanvasListPool_t1332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasListPool_t1332_0_0_0/* byval_arg */
	, &CanvasListPool_t1332_1_0_0/* this_arg */
	, &CanvasListPool_t1332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasListPool_t1332)/* instance_size */
	, sizeof (CanvasListPool_t1332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CanvasListPool_t1332_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPool.h"
// Metadata Definition UnityEngine.UI.ComponentListPool
extern TypeInfo ComponentListPool_t1335_il2cpp_TypeInfo;
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPoolMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::.cctor()
MethodInfo ComponentListPool__cctor_m5710_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ComponentListPool__cctor_m5710/* method */
	, &ComponentListPool_t1335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1366_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Component> UnityEngine.UI.ComponentListPool::Get()
MethodInfo ComponentListPool_Get_m5711_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&ComponentListPool_Get_m5711/* method */
	, &ComponentListPool_t1335_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1366_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1366_0_0_0;
static ParameterInfo ComponentListPool_t1335_ComponentListPool_Release_m5712_ParameterInfos[] = 
{
	{"toRelease", 0, 134218443, 0, &List_1_t1366_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::Release(System.Collections.Generic.List`1<UnityEngine.Component>)
MethodInfo ComponentListPool_Release_m5712_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&ComponentListPool_Release_m5712/* method */
	, &ComponentListPool_t1335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ComponentListPool_t1335_ComponentListPool_Release_m5712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1366_0_0_0;
static ParameterInfo ComponentListPool_t1335_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m5713_ParameterInfos[] = 
{
	{"l", 0, 134218444, 0, &List_1_t1366_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::<s_ComponentListPool>m__17(System.Collections.Generic.List`1<UnityEngine.Component>)
MethodInfo ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m5713_MethodInfo = 
{
	"<s_ComponentListPool>m__17"/* name */
	, (methodPointerType)&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m5713/* method */
	, &ComponentListPool_t1335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ComponentListPool_t1335_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m5713_ParameterInfos/* parameters */
	, 333/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ComponentListPool_t1335_MethodInfos[] =
{
	&ComponentListPool__cctor_m5710_MethodInfo,
	&ComponentListPool_Get_m5711_MethodInfo,
	&ComponentListPool_Release_m5712_MethodInfo,
	&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m5713_MethodInfo,
	NULL
};
extern Il2CppType ObjectPool_1_t1333_0_0_49;
FieldInfo ComponentListPool_t1335____s_ComponentListPool_0_FieldInfo = 
{
	"s_ComponentListPool"/* name */
	, &ObjectPool_1_t1333_0_0_49/* type */
	, &ComponentListPool_t1335_il2cpp_TypeInfo/* parent */
	, offsetof(ComponentListPool_t1335_StaticFields, ___s_ComponentListPool_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1334_0_0_17;
FieldInfo ComponentListPool_t1335____U3CU3Ef__amU24cache1_1_FieldInfo = 
{
	"<>f__am$cache1"/* name */
	, &UnityAction_1_t1334_0_0_17/* type */
	, &ComponentListPool_t1335_il2cpp_TypeInfo/* parent */
	, offsetof(ComponentListPool_t1335_StaticFields, ___U3CU3Ef__amU24cache1_1)/* offset */
	, 332/* custom_attributes_cache */

};
static FieldInfo* ComponentListPool_t1335_FieldInfos[] =
{
	&ComponentListPool_t1335____s_ComponentListPool_0_FieldInfo,
	&ComponentListPool_t1335____U3CU3Ef__amU24cache1_1_FieldInfo,
	NULL
};
static Il2CppMethodReference ComponentListPool_t1335_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ComponentListPool_t1335_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ComponentListPool_t1335_0_0_0;
extern Il2CppType ComponentListPool_t1335_1_0_0;
struct ComponentListPool_t1335;
const Il2CppTypeDefinitionMetadata ComponentListPool_t1335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ComponentListPool_t1335_VTable/* vtableMethods */
	, ComponentListPool_t1335_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ComponentListPool_t1335_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ComponentListPool_t1335_MethodInfos/* methods */
	, NULL/* properties */
	, ComponentListPool_t1335_FieldInfos/* fields */
	, NULL/* events */
	, &ComponentListPool_t1335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentListPool_t1335_0_0_0/* byval_arg */
	, &ComponentListPool_t1335_1_0_0/* this_arg */
	, &ComponentListPool_t1335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentListPool_t1335)/* instance_size */
	, sizeof (ComponentListPool_t1335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ComponentListPool_t1335_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ObjectPool`1
extern TypeInfo ObjectPool_1_t1445_il2cpp_TypeInfo;
extern Il2CppGenericContainer ObjectPool_1_t1445_Il2CppGenericContainer;
extern TypeInfo ObjectPool_1_t1445_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull ObjectPool_1_t1445_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &ObjectPool_1_t1445_Il2CppGenericContainer, 0}, {NULL, "T", 16, 0, NULL} };
static Il2CppGenericParamFull* ObjectPool_1_t1445_Il2CppGenericParametersArray[1] = 
{
	&ObjectPool_1_t1445_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer ObjectPool_1_t1445_Il2CppGenericContainer = { { NULL, NULL }, NULL, &ObjectPool_1_t1445_il2cpp_TypeInfo, 1, 0, ObjectPool_1_t1445_Il2CppGenericParametersArray };
extern Il2CppType UnityAction_1_t1482_0_0_0;
extern Il2CppType UnityAction_1_t1482_0_0_0;
extern Il2CppType UnityAction_1_t1482_0_0_0;
static ParameterInfo ObjectPool_1_t1445_ObjectPool_1__ctor_m6284_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134218445, 0, &UnityAction_1_t1482_0_0_0},
	{"actionOnRelease", 1, 134218446, 0, &UnityAction_1_t1482_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.ObjectPool`1::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
MethodInfo ObjectPool_1__ctor_m6284_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t1445_ObjectPool_1__ctor_m6284_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countAll()
MethodInfo ObjectPool_1_get_countAll_m6285_MethodInfo = 
{
	"get_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 335/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ObjectPool_1_t1445_ObjectPool_1_set_countAll_m6286_ParameterInfos[] = 
{
	{"value", 0, 134218447, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.ObjectPool`1::set_countAll(System.Int32)
MethodInfo ObjectPool_1_set_countAll_m6286_MethodInfo = 
{
	"set_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t1445_ObjectPool_1_set_countAll_m6286_ParameterInfos/* parameters */
	, 336/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countActive()
MethodInfo ObjectPool_1_get_countActive_m6287_MethodInfo = 
{
	"get_countActive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countInactive()
MethodInfo ObjectPool_1_get_countInactive_m6288_MethodInfo = 
{
	"get_countInactive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectPool_1_t1445_gp_0_0_0_0;
// T UnityEngine.UI.ObjectPool`1::Get()
MethodInfo ObjectPool_1_Get_m6289_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &ObjectPool_1_t1445_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectPool_1_t1445_gp_0_0_0_0;
extern Il2CppType ObjectPool_1_t1445_gp_0_0_0_0;
static ParameterInfo ObjectPool_1_t1445_ObjectPool_1_Release_m6290_ParameterInfos[] = 
{
	{"element", 0, 134218448, 0, &ObjectPool_1_t1445_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void UnityEngine.UI.ObjectPool`1::Release(T)
MethodInfo ObjectPool_1_Release_m6290_MethodInfo = 
{
	"Release"/* name */
	, NULL/* method */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t1445_ObjectPool_1_Release_m6290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ObjectPool_1_t1445_MethodInfos[] =
{
	&ObjectPool_1__ctor_m6284_MethodInfo,
	&ObjectPool_1_get_countAll_m6285_MethodInfo,
	&ObjectPool_1_set_countAll_m6286_MethodInfo,
	&ObjectPool_1_get_countActive_m6287_MethodInfo,
	&ObjectPool_1_get_countInactive_m6288_MethodInfo,
	&ObjectPool_1_Get_m6289_MethodInfo,
	&ObjectPool_1_Release_m6290_MethodInfo,
	NULL
};
extern Il2CppType Stack_1_t1483_0_0_33;
FieldInfo ObjectPool_1_t1445____m_Stack_0_FieldInfo = 
{
	"m_Stack"/* name */
	, &Stack_1_t1483_0_0_33/* type */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1482_0_0_33;
FieldInfo ObjectPool_1_t1445____m_ActionOnGet_1_FieldInfo = 
{
	"m_ActionOnGet"/* name */
	, &UnityAction_1_t1482_0_0_33/* type */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t1482_0_0_33;
FieldInfo ObjectPool_1_t1445____m_ActionOnRelease_2_FieldInfo = 
{
	"m_ActionOnRelease"/* name */
	, &UnityAction_1_t1482_0_0_33/* type */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo ObjectPool_1_t1445____U3CcountAllU3Ek__BackingField_3_FieldInfo = 
{
	"<countAll>k__BackingField"/* name */
	, &Int32_t189_0_0_1/* type */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 334/* custom_attributes_cache */

};
static FieldInfo* ObjectPool_1_t1445_FieldInfos[] =
{
	&ObjectPool_1_t1445____m_Stack_0_FieldInfo,
	&ObjectPool_1_t1445____m_ActionOnGet_1_FieldInfo,
	&ObjectPool_1_t1445____m_ActionOnRelease_2_FieldInfo,
	&ObjectPool_1_t1445____U3CcountAllU3Ek__BackingField_3_FieldInfo,
	NULL
};
extern MethodInfo ObjectPool_1_get_countAll_m6285_MethodInfo;
extern MethodInfo ObjectPool_1_set_countAll_m6286_MethodInfo;
static PropertyInfo ObjectPool_1_t1445____countAll_PropertyInfo = 
{
	&ObjectPool_1_t1445_il2cpp_TypeInfo/* parent */
	, "countAll"/* name */
	, &ObjectPool_1_get_countAll_m6285_MethodInfo/* get */
	, &ObjectPool_1_set_countAll_m6286_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ObjectPool_1_get_countActive_m6287_MethodInfo;
static PropertyInfo ObjectPool_1_t1445____countActive_PropertyInfo = 
{
	&ObjectPool_1_t1445_il2cpp_TypeInfo/* parent */
	, "countActive"/* name */
	, &ObjectPool_1_get_countActive_m6287_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ObjectPool_1_get_countInactive_m6288_MethodInfo;
static PropertyInfo ObjectPool_1_t1445____countInactive_PropertyInfo = 
{
	&ObjectPool_1_t1445_il2cpp_TypeInfo/* parent */
	, "countInactive"/* name */
	, &ObjectPool_1_get_countInactive_m6288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ObjectPool_1_t1445_PropertyInfos[] =
{
	&ObjectPool_1_t1445____countAll_PropertyInfo,
	&ObjectPool_1_t1445____countActive_PropertyInfo,
	&ObjectPool_1_t1445____countInactive_PropertyInfo,
	NULL
};
static Il2CppMethodReference ObjectPool_1_t1445_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ObjectPool_1_t1445_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppType Stack_1_t1483_0_0_0;
extern Il2CppGenericMethod Stack_1__ctor_m6338_GenericMethod;
extern Il2CppGenericMethod ObjectPool_1_get_countAll_m6339_GenericMethod;
extern Il2CppGenericMethod ObjectPool_1_get_countInactive_m6340_GenericMethod;
extern Il2CppGenericMethod Stack_1_get_Count_m6341_GenericMethod;
extern Il2CppGenericMethod Activator_CreateInstance_TisT_t1481_m6342_GenericMethod;
extern Il2CppGenericMethod ObjectPool_1_set_countAll_m6343_GenericMethod;
extern Il2CppGenericMethod Stack_1_Pop_m6344_GenericMethod;
extern Il2CppGenericMethod UnityAction_1_Invoke_m6345_GenericMethod;
extern Il2CppGenericMethod Stack_1_Peek_m6346_GenericMethod;
extern Il2CppGenericMethod Stack_1_Push_m6347_GenericMethod;
static Il2CppRGCTXDefinition ObjectPool_1_t1445_RGCTXData[13] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &Stack_1_t1483_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1__ctor_m6338_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countAll_m6339_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countInactive_m6340_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_get_Count_m6341_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &ObjectPool_1_t1445_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisT_t1481_m6342_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_set_countAll_m6343_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Pop_m6344_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m6345_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Peek_m6346_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Push_m6347_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ObjectPool_1_t1445_0_0_0;
extern Il2CppType ObjectPool_1_t1445_1_0_0;
struct ObjectPool_1_t1445;
const Il2CppTypeDefinitionMetadata ObjectPool_1_t1445_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectPool_1_t1445_VTable/* vtableMethods */
	, ObjectPool_1_t1445_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ObjectPool_1_t1445_RGCTXData/* rgctxDefinition */

};
TypeInfo ObjectPool_1_t1445_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ObjectPool_1_t1445_MethodInfos/* methods */
	, ObjectPool_1_t1445_PropertyInfos/* properties */
	, ObjectPool_1_t1445_FieldInfos/* fields */
	, NULL/* events */
	, &ObjectPool_1_t1445_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectPool_1_t1445_0_0_0/* byval_arg */
	, &ObjectPool_1_t1445_1_0_0/* this_arg */
	, &ObjectPool_1_t1445_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ObjectPool_1_t1445_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// Metadata Definition UnityEngine.UI.BaseVertexEffect
extern TypeInfo BaseVertexEffect_t1336_il2cpp_TypeInfo;
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
MethodInfo BaseVertexEffect__ctor_m5714_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseVertexEffect__ctor_m5714/* method */
	, &BaseVertexEffect_t1336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Graphic_t1233_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
MethodInfo BaseVertexEffect_get_graphic_m5715_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&BaseVertexEffect_get_graphic_m5715/* method */
	, &BaseVertexEffect_t1336_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t1233_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
MethodInfo BaseVertexEffect_OnEnable_m5716_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnEnable_m5716/* method */
	, &BaseVertexEffect_t1336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
MethodInfo BaseVertexEffect_OnDisable_m5717_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnDisable_m5717/* method */
	, &BaseVertexEffect_t1336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1267_0_0_0;
static ParameterInfo BaseVertexEffect_t1336_BaseVertexEffect_ModifyVertices_m6291_ParameterInfos[] = 
{
	{"verts", 0, 134218449, 0, &List_1_t1267_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
MethodInfo BaseVertexEffect_ModifyVertices_m6291_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &BaseVertexEffect_t1336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, BaseVertexEffect_t1336_BaseVertexEffect_ModifyVertices_m6291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BaseVertexEffect_t1336_MethodInfos[] =
{
	&BaseVertexEffect__ctor_m5714_MethodInfo,
	&BaseVertexEffect_get_graphic_m5715_MethodInfo,
	&BaseVertexEffect_OnEnable_m5716_MethodInfo,
	&BaseVertexEffect_OnDisable_m5717_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m6291_MethodInfo,
	NULL
};
extern Il2CppType Graphic_t1233_0_0_129;
FieldInfo BaseVertexEffect_t1336____m_Graphic_2_FieldInfo = 
{
	"m_Graphic"/* name */
	, &Graphic_t1233_0_0_129/* type */
	, &BaseVertexEffect_t1336_il2cpp_TypeInfo/* parent */
	, offsetof(BaseVertexEffect_t1336, ___m_Graphic_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* BaseVertexEffect_t1336_FieldInfos[] =
{
	&BaseVertexEffect_t1336____m_Graphic_2_FieldInfo,
	NULL
};
extern MethodInfo BaseVertexEffect_get_graphic_m5715_MethodInfo;
static PropertyInfo BaseVertexEffect_t1336____graphic_PropertyInfo = 
{
	&BaseVertexEffect_t1336_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &BaseVertexEffect_get_graphic_m5715_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* BaseVertexEffect_t1336_PropertyInfos[] =
{
	&BaseVertexEffect_t1336____graphic_PropertyInfo,
	NULL
};
extern MethodInfo BaseVertexEffect_OnEnable_m5716_MethodInfo;
extern MethodInfo BaseVertexEffect_OnDisable_m5717_MethodInfo;
extern MethodInfo BaseVertexEffect_ModifyVertices_m6291_MethodInfo;
static Il2CppMethodReference BaseVertexEffect_t1336_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&BaseVertexEffect_OnEnable_m5716_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&BaseVertexEffect_OnDisable_m5717_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m6291_MethodInfo,
	NULL,
};
static bool BaseVertexEffect_t1336_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IVertexModifier_t1386_0_0_0;
static const Il2CppType* BaseVertexEffect_t1336_InterfacesTypeInfos[] = 
{
	&IVertexModifier_t1386_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseVertexEffect_t1336_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t1386_0_0_0, 15},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType BaseVertexEffect_t1336_0_0_0;
extern Il2CppType BaseVertexEffect_t1336_1_0_0;
struct BaseVertexEffect_t1336;
const Il2CppTypeDefinitionMetadata BaseVertexEffect_t1336_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BaseVertexEffect_t1336_InterfacesTypeInfos/* implementedInterfaces */
	, BaseVertexEffect_t1336_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t1156_0_0_0/* parent */
	, BaseVertexEffect_t1336_VTable/* vtableMethods */
	, BaseVertexEffect_t1336_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo BaseVertexEffect_t1336_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseVertexEffect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, BaseVertexEffect_t1336_MethodInfos/* methods */
	, BaseVertexEffect_t1336_PropertyInfos/* properties */
	, BaseVertexEffect_t1336_FieldInfos/* fields */
	, NULL/* events */
	, &BaseVertexEffect_t1336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 337/* custom_attributes_cache */
	, &BaseVertexEffect_t1336_0_0_0/* byval_arg */
	, &BaseVertexEffect_t1336_1_0_0/* this_arg */
	, &BaseVertexEffect_t1336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseVertexEffect_t1336)/* instance_size */
	, sizeof (BaseVertexEffect_t1336)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IVertexModifier
extern TypeInfo IVertexModifier_t1386_il2cpp_TypeInfo;
extern Il2CppType List_1_t1267_0_0_0;
static ParameterInfo IVertexModifier_t1386_IVertexModifier_ModifyVertices_m6292_ParameterInfos[] = 
{
	{"verts", 0, 134218450, 0, &List_1_t1267_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.IVertexModifier::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
MethodInfo IVertexModifier_ModifyVertices_m6292_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &IVertexModifier_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, IVertexModifier_t1386_IVertexModifier_ModifyVertices_m6292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IVertexModifier_t1386_MethodInfos[] =
{
	&IVertexModifier_ModifyVertices_m6292_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType IVertexModifier_t1386_1_0_0;
struct IVertexModifier_t1386;
const Il2CppTypeDefinitionMetadata IVertexModifier_t1386_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IVertexModifier_t1386_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IVertexModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IVertexModifier_t1386_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IVertexModifier_t1386_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IVertexModifier_t1386_0_0_0/* byval_arg */
	, &IVertexModifier_t1386_1_0_0/* this_arg */
	, &IVertexModifier_t1386_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// Metadata Definition UnityEngine.UI.Outline
extern TypeInfo Outline_t1337_il2cpp_TypeInfo;
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::.ctor()
MethodInfo Outline__ctor_m5718_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Outline__ctor_m5718/* method */
	, &Outline_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1267_0_0_0;
static ParameterInfo Outline_t1337_Outline_ModifyVertices_m5719_ParameterInfos[] = 
{
	{"verts", 0, 134218451, 0, &List_1_t1267_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
MethodInfo Outline_ModifyVertices_m5719_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Outline_ModifyVertices_m5719/* method */
	, &Outline_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Outline_t1337_Outline_ModifyVertices_m5719_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Outline_t1337_MethodInfos[] =
{
	&Outline__ctor_m5718_MethodInfo,
	&Outline_ModifyVertices_m5719_MethodInfo,
	NULL
};
extern MethodInfo Outline_ModifyVertices_m5719_MethodInfo;
static Il2CppMethodReference Outline_t1337_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&BaseVertexEffect_OnEnable_m5716_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&BaseVertexEffect_OnDisable_m5717_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&Outline_ModifyVertices_m5719_MethodInfo,
	&Outline_ModifyVertices_m5719_MethodInfo,
};
static bool Outline_t1337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Outline_t1337_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t1386_0_0_0, 15},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Outline_t1337_0_0_0;
extern Il2CppType Outline_t1337_1_0_0;
extern Il2CppType Shadow_t1338_0_0_0;
struct Outline_t1337;
const Il2CppTypeDefinitionMetadata Outline_t1337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Outline_t1337_InterfacesOffsets/* interfaceOffsets */
	, &Shadow_t1338_0_0_0/* parent */
	, Outline_t1337_VTable/* vtableMethods */
	, Outline_t1337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Outline_t1337_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Outline"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Outline_t1337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Outline_t1337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 338/* custom_attributes_cache */
	, &Outline_t1337_0_0_0/* byval_arg */
	, &Outline_t1337_1_0_0/* this_arg */
	, &Outline_t1337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Outline_t1337)/* instance_size */
	, sizeof (Outline_t1337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// Metadata Definition UnityEngine.UI.PositionAsUV1
extern TypeInfo PositionAsUV1_t1339_il2cpp_TypeInfo;
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
MethodInfo PositionAsUV1__ctor_m5720_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAsUV1__ctor_m5720/* method */
	, &PositionAsUV1_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1267_0_0_0;
static ParameterInfo PositionAsUV1_t1339_PositionAsUV1_ModifyVertices_m5721_ParameterInfos[] = 
{
	{"verts", 0, 134218452, 0, &List_1_t1267_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
MethodInfo PositionAsUV1_ModifyVertices_m5721_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&PositionAsUV1_ModifyVertices_m5721/* method */
	, &PositionAsUV1_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PositionAsUV1_t1339_PositionAsUV1_ModifyVertices_m5721_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PositionAsUV1_t1339_MethodInfos[] =
{
	&PositionAsUV1__ctor_m5720_MethodInfo,
	&PositionAsUV1_ModifyVertices_m5721_MethodInfo,
	NULL
};
extern MethodInfo PositionAsUV1_ModifyVertices_m5721_MethodInfo;
static Il2CppMethodReference PositionAsUV1_t1339_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&BaseVertexEffect_OnEnable_m5716_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&BaseVertexEffect_OnDisable_m5717_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&PositionAsUV1_ModifyVertices_m5721_MethodInfo,
	&PositionAsUV1_ModifyVertices_m5721_MethodInfo,
};
static bool PositionAsUV1_t1339_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PositionAsUV1_t1339_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t1386_0_0_0, 15},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType PositionAsUV1_t1339_0_0_0;
extern Il2CppType PositionAsUV1_t1339_1_0_0;
struct PositionAsUV1_t1339;
const Il2CppTypeDefinitionMetadata PositionAsUV1_t1339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PositionAsUV1_t1339_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t1336_0_0_0/* parent */
	, PositionAsUV1_t1339_VTable/* vtableMethods */
	, PositionAsUV1_t1339_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PositionAsUV1_t1339_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAsUV1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, PositionAsUV1_t1339_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &PositionAsUV1_t1339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 339/* custom_attributes_cache */
	, &PositionAsUV1_t1339_0_0_0/* byval_arg */
	, &PositionAsUV1_t1339_1_0_0/* this_arg */
	, &PositionAsUV1_t1339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAsUV1_t1339)/* instance_size */
	, sizeof (PositionAsUV1_t1339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// Metadata Definition UnityEngine.UI.Shadow
extern TypeInfo Shadow_t1338_il2cpp_TypeInfo;
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::.ctor()
MethodInfo Shadow__ctor_m5722_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Shadow__ctor_m5722/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Color_t747_0_0_0;
extern void* RuntimeInvoker_Color_t747 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
MethodInfo Shadow_get_effectColor_m5723_MethodInfo = 
{
	"get_effectColor"/* name */
	, (methodPointerType)&Shadow_get_effectColor_m5723/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Color_t747_0_0_0/* return_type */
	, RuntimeInvoker_Color_t747/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Color_t747_0_0_0;
static ParameterInfo Shadow_t1338_Shadow_set_effectColor_m5724_ParameterInfos[] = 
{
	{"value", 0, 134218453, 0, &Color_t747_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Color_t747 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
MethodInfo Shadow_set_effectColor_m5724_MethodInfo = 
{
	"set_effectColor"/* name */
	, (methodPointerType)&Shadow_set_effectColor_m5724/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Color_t747/* invoker_method */
	, Shadow_t1338_Shadow_set_effectColor_m5724_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
MethodInfo Shadow_get_effectDistance_m5725_MethodInfo = 
{
	"get_effectDistance"/* name */
	, (methodPointerType)&Shadow_get_effectDistance_m5725/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo Shadow_t1338_Shadow_set_effectDistance_m5726_ParameterInfos[] = 
{
	{"value", 0, 134218454, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
MethodInfo Shadow_set_effectDistance_m5726_MethodInfo = 
{
	"set_effectDistance"/* name */
	, (methodPointerType)&Shadow_set_effectDistance_m5726/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector2_t739/* invoker_method */
	, Shadow_t1338_Shadow_set_effectDistance_m5726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
MethodInfo Shadow_get_useGraphicAlpha_m5727_MethodInfo = 
{
	"get_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_get_useGraphicAlpha_m5727/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Shadow_t1338_Shadow_set_useGraphicAlpha_m5728_ParameterInfos[] = 
{
	{"value", 0, 134218455, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
MethodInfo Shadow_set_useGraphicAlpha_m5728_MethodInfo = 
{
	"set_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_set_useGraphicAlpha_m5728/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Shadow_t1338_Shadow_set_useGraphicAlpha_m5728_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1267_0_0_0;
extern Il2CppType Color32_t992_0_0_0;
extern Il2CppType Color32_t992_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo Shadow_t1338_Shadow_ApplyShadow_m5729_ParameterInfos[] = 
{
	{"verts", 0, 134218456, 0, &List_1_t1267_0_0_0},
	{"color", 1, 134218457, 0, &Color32_t992_0_0_0},
	{"start", 2, 134218458, 0, &Int32_t189_0_0_0},
	{"end", 3, 134218459, 0, &Int32_t189_0_0_0},
	{"x", 4, 134218460, 0, &Single_t202_0_0_0},
	{"y", 5, 134218461, 0, &Single_t202_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Color32_t992_Int32_t189_Int32_t189_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
MethodInfo Shadow_ApplyShadow_m5729_MethodInfo = 
{
	"ApplyShadow"/* name */
	, (methodPointerType)&Shadow_ApplyShadow_m5729/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Color32_t992_Int32_t189_Int32_t189_Single_t202_Single_t202/* invoker_method */
	, Shadow_t1338_Shadow_ApplyShadow_m5729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t1267_0_0_0;
static ParameterInfo Shadow_t1338_Shadow_ModifyVertices_m5730_ParameterInfos[] = 
{
	{"verts", 0, 134218462, 0, &List_1_t1267_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
MethodInfo Shadow_ModifyVertices_m5730_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Shadow_ModifyVertices_m5730/* method */
	, &Shadow_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Shadow_t1338_Shadow_ModifyVertices_m5730_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Shadow_t1338_MethodInfos[] =
{
	&Shadow__ctor_m5722_MethodInfo,
	&Shadow_get_effectColor_m5723_MethodInfo,
	&Shadow_set_effectColor_m5724_MethodInfo,
	&Shadow_get_effectDistance_m5725_MethodInfo,
	&Shadow_set_effectDistance_m5726_MethodInfo,
	&Shadow_get_useGraphicAlpha_m5727_MethodInfo,
	&Shadow_set_useGraphicAlpha_m5728_MethodInfo,
	&Shadow_ApplyShadow_m5729_MethodInfo,
	&Shadow_ModifyVertices_m5730_MethodInfo,
	NULL
};
extern Il2CppType Color_t747_0_0_1;
FieldInfo Shadow_t1338____m_EffectColor_3_FieldInfo = 
{
	"m_EffectColor"/* name */
	, &Color_t747_0_0_1/* type */
	, &Shadow_t1338_il2cpp_TypeInfo/* parent */
	, offsetof(Shadow_t1338, ___m_EffectColor_3)/* offset */
	, 341/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo Shadow_t1338____m_EffectDistance_4_FieldInfo = 
{
	"m_EffectDistance"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &Shadow_t1338_il2cpp_TypeInfo/* parent */
	, offsetof(Shadow_t1338, ___m_EffectDistance_4)/* offset */
	, 342/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Shadow_t1338____m_UseGraphicAlpha_5_FieldInfo = 
{
	"m_UseGraphicAlpha"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Shadow_t1338_il2cpp_TypeInfo/* parent */
	, offsetof(Shadow_t1338, ___m_UseGraphicAlpha_5)/* offset */
	, 343/* custom_attributes_cache */

};
static FieldInfo* Shadow_t1338_FieldInfos[] =
{
	&Shadow_t1338____m_EffectColor_3_FieldInfo,
	&Shadow_t1338____m_EffectDistance_4_FieldInfo,
	&Shadow_t1338____m_UseGraphicAlpha_5_FieldInfo,
	NULL
};
extern MethodInfo Shadow_get_effectColor_m5723_MethodInfo;
extern MethodInfo Shadow_set_effectColor_m5724_MethodInfo;
static PropertyInfo Shadow_t1338____effectColor_PropertyInfo = 
{
	&Shadow_t1338_il2cpp_TypeInfo/* parent */
	, "effectColor"/* name */
	, &Shadow_get_effectColor_m5723_MethodInfo/* get */
	, &Shadow_set_effectColor_m5724_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Shadow_get_effectDistance_m5725_MethodInfo;
extern MethodInfo Shadow_set_effectDistance_m5726_MethodInfo;
static PropertyInfo Shadow_t1338____effectDistance_PropertyInfo = 
{
	&Shadow_t1338_il2cpp_TypeInfo/* parent */
	, "effectDistance"/* name */
	, &Shadow_get_effectDistance_m5725_MethodInfo/* get */
	, &Shadow_set_effectDistance_m5726_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Shadow_get_useGraphicAlpha_m5727_MethodInfo;
extern MethodInfo Shadow_set_useGraphicAlpha_m5728_MethodInfo;
static PropertyInfo Shadow_t1338____useGraphicAlpha_PropertyInfo = 
{
	&Shadow_t1338_il2cpp_TypeInfo/* parent */
	, "useGraphicAlpha"/* name */
	, &Shadow_get_useGraphicAlpha_m5727_MethodInfo/* get */
	, &Shadow_set_useGraphicAlpha_m5728_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Shadow_t1338_PropertyInfos[] =
{
	&Shadow_t1338____effectColor_PropertyInfo,
	&Shadow_t1338____effectDistance_PropertyInfo,
	&Shadow_t1338____useGraphicAlpha_PropertyInfo,
	NULL
};
extern MethodInfo Shadow_ModifyVertices_m5730_MethodInfo;
static Il2CppMethodReference Shadow_t1338_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
	&UIBehaviour_Awake_m4650_MethodInfo,
	&BaseVertexEffect_OnEnable_m5716_MethodInfo,
	&UIBehaviour_Start_m4652_MethodInfo,
	&BaseVertexEffect_OnDisable_m5717_MethodInfo,
	&UIBehaviour_OnDestroy_m4654_MethodInfo,
	&UIBehaviour_IsActive_m4655_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m4656_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m4657_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m4658_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m4659_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m4660_MethodInfo,
	&Shadow_ModifyVertices_m5730_MethodInfo,
	&Shadow_ModifyVertices_m5730_MethodInfo,
};
static bool Shadow_t1338_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Shadow_t1338_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t1386_0_0_0, 15},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType Shadow_t1338_1_0_0;
struct Shadow_t1338;
const Il2CppTypeDefinitionMetadata Shadow_t1338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Shadow_t1338_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t1336_0_0_0/* parent */
	, Shadow_t1338_VTable/* vtableMethods */
	, Shadow_t1338_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Shadow_t1338_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Shadow"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Shadow_t1338_MethodInfos/* methods */
	, Shadow_t1338_PropertyInfos/* properties */
	, Shadow_t1338_FieldInfos/* fields */
	, NULL/* events */
	, &Shadow_t1338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 340/* custom_attributes_cache */
	, &Shadow_t1338_0_0_0/* byval_arg */
	, &Shadow_t1338_1_0_0/* this_arg */
	, &Shadow_t1338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Shadow_t1338)/* instance_size */
	, sizeof (Shadow_t1338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
