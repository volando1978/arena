﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t536;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A
struct  U3CDeclineInvitationU3Ec__AnonStorey3A_t609  : public Object_t
{
	// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A::invitationId
	String_t* ___invitationId_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A::<>f__this
	NativeRealtimeMultiplayerClient_t536 * ___U3CU3Ef__this_1;
};
