﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>
struct  KeyValuePair_2_t3607 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::value
	PurchasableVirtualItem_t124 * ___value_1;
};
