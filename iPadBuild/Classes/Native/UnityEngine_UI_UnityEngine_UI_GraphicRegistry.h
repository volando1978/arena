﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.GraphicRegistry
struct GraphicRegistry_t1238;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Dictionary_2_t1239;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t1235;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.GraphicRegistry
struct  GraphicRegistry_t1238  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>> UnityEngine.UI.GraphicRegistry::m_Graphics
	Dictionary_2_t1239 * ___m_Graphics_1;
};
struct GraphicRegistry_t1238_StaticFields{
	// UnityEngine.UI.GraphicRegistry UnityEngine.UI.GraphicRegistry::s_Instance
	GraphicRegistry_t1238 * ___s_Instance_0;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRegistry::s_EmptyList
	List_1_t1235 * ___s_EmptyList_2;
};
