﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IDropHandler
struct IDropHandler_t1349;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t1153;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct  EventFunction_1_t1172  : public MulticastDelegate_t22
{
};
