﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct Converter_2_t3462;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"

// System.Void System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::.ctor(System.Object,System.IntPtr)
// System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Converter_2_gen_4MethodDeclarations.h"
#define Converter_2__ctor_m15902(__this, ___object, ___method, method) (( void (*) (Converter_2_t3462 *, Object_t *, IntPtr_t, MethodInfo*))Converter_2__ctor_m15610_gshared)(__this, ___object, ___method, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::Invoke(TInput)
#define Converter_2_Invoke_m15903(__this, ___input, method) (( KeyValuePair_2_t3463  (*) (Converter_2_t3462 *, UnityKeyValuePair_2_t3457 *, MethodInfo*))Converter_2_Invoke_m15612_gshared)(__this, ___input, method)
// System.IAsyncResult System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
#define Converter_2_BeginInvoke_m15904(__this, ___input, ___callback, ___object, method) (( Object_t * (*) (Converter_2_t3462 *, UnityKeyValuePair_2_t3457 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Converter_2_BeginInvoke_m15614_gshared)(__this, ___input, ___callback, ___object, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::EndInvoke(System.IAsyncResult)
#define Converter_2_EndInvoke_m15905(__this, ___result, method) (( KeyValuePair_2_t3463  (*) (Converter_2_t3462 *, Object_t *, MethodInfo*))Converter_2_EndInvoke_m15616_gshared)(__this, ___result, method)
