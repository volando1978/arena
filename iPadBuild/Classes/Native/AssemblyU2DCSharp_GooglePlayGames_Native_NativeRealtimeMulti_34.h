﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// GooglePlayGames.Native.PInvoke.RealtimeManager
struct RealtimeManager_t564;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession modreq(System.Runtime.CompilerServices.IsVolatile)
struct RoomSession_t566;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct  NativeRealtimeMultiplayerClient_t536  : public Object_t
{
	// System.Object GooglePlayGames.Native.NativeRealtimeMultiplayerClient::mSessionLock
	Object_t * ___mSessionLock_0;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient::mNativeClient
	NativeClient_t524 * ___mNativeClient_1;
	// GooglePlayGames.Native.PInvoke.RealtimeManager GooglePlayGames.Native.NativeRealtimeMultiplayerClient::mRealtimeManager
	RealtimeManager_t564 * ___mRealtimeManager_2;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeRealtimeMultiplayerClient::mCurrentSession
	RoomSession_t566 * ___mCurrentSession_3;
};
