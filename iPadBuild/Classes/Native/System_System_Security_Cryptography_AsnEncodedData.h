﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.Oid
struct Oid_t2015;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t75;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t2014  : public Object_t
{
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_t2015 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t350* ____raw_1;
};
struct AsnEncodedData_t2014_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.AsnEncodedData::<>f__switch$mapA
	Dictionary_2_t75 * ___U3CU3Ef__switchU24mapA_2;
};
