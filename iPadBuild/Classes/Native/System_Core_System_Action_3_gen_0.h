﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`3<Soomla.Store.VirtualGood,System.Int32,System.Int32>
struct  Action_3_t92  : public MulticastDelegate_t22
{
};
