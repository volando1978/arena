﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.Texture
struct Texture_t736;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// menu
struct  menu_t814  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject menu::controller
	GameObject_t144 * ___controller_2;
	// UnityEngine.GUIStyle menu::marqueeSt
	GUIStyle_t724 * ___marqueeSt_3;
	// UnityEngine.GUIStyle menu::startSt
	GUIStyle_t724 * ___startSt_4;
	// System.Single menu::offset
	float ___offset_5;
	// UnityEngine.Rect menu::messageRect
	Rect_t738  ___messageRect_6;
	// System.Int32 menu::t
	int32_t ___t_7;
	// UnityEngine.Vector2 menu::vChild
	Vector2_t739  ___vChild_8;
	// System.Single menu::v
	float ___v_9;
	// System.Single menu::yU
	float ___yU_10;
	// UnityEngine.Texture menu::splash
	Texture_t736 * ___splash_11;
};
