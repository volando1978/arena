﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t4207;
// System.Object
struct Object_t;

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m25855_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4207 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m25855(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4207 *, MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m25855_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m25856_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4207 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m25856(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t4207 *, MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m25856_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m25857_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4207 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m25857(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t4207 *, MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m25857_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m25858_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4207 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m25858(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t4207 *, MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m25858_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m25859_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4207 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m25859(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4207 *, MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m25859_gshared)(__this, method)
