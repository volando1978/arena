﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>
struct ValueCollection_t3614;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualCategory>
struct Dictionary_2_t112;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.VirtualCategory>
struct IEnumerator_1_t4337;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// Soomla.Store.VirtualCategory[]
struct VirtualCategoryU5BU5D_t175;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Store.VirtualCategory>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_48.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10MethodDeclarations.h"
#define ValueCollection__ctor_m18207(__this, ___dictionary, method) (( void (*) (ValueCollection_t3614 *, Dictionary_2_t112 *, MethodInfo*))ValueCollection__ctor_m16162_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18208(__this, ___item, method) (( void (*) (ValueCollection_t3614 *, VirtualCategory_t126 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18209(__this, method) (( void (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18210(__this, ___item, method) (( bool (*) (ValueCollection_t3614 *, VirtualCategory_t126 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18211(__this, ___item, method) (( bool (*) (ValueCollection_t3614 *, VirtualCategory_t126 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18212(__this, method) (( Object_t* (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m18213(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3614 *, Array_t *, int32_t, MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16174_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18214(__this, method) (( Object_t * (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18215(__this, method) (( bool (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18216(__this, method) (( bool (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m18217(__this, method) (( Object_t * (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m18218(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3614 *, VirtualCategoryU5BU5D_t175*, int32_t, MethodInfo*))ValueCollection_CopyTo_m16184_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::GetEnumerator()
#define ValueCollection_GetEnumerator_m18219(__this, method) (( Enumerator_t4338  (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_GetEnumerator_m16186_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualCategory>::get_Count()
#define ValueCollection_get_Count_m18220(__this, method) (( int32_t (*) (ValueCollection_t3614 *, MethodInfo*))ValueCollection_get_Count_m16188_gshared)(__this, method)
