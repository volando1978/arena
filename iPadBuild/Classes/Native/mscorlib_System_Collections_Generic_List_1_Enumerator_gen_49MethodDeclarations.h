﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
struct Enumerator_t4099;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1603;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m24792_gshared (Enumerator_t4099 * __this, List_1_t1603 * ___l, MethodInfo* method);
#define Enumerator__ctor_m24792(__this, ___l, method) (( void (*) (Enumerator_t4099 *, List_1_t1603 *, MethodInfo*))Enumerator__ctor_m24792_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24793_gshared (Enumerator_t4099 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24793(__this, method) (( Object_t * (*) (Enumerator_t4099 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24793_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m24794_gshared (Enumerator_t4099 * __this, MethodInfo* method);
#define Enumerator_Dispose_m24794(__this, method) (( void (*) (Enumerator_t4099 *, MethodInfo*))Enumerator_Dispose_m24794_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m24795_gshared (Enumerator_t4099 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m24795(__this, method) (( void (*) (Enumerator_t4099 *, MethodInfo*))Enumerator_VerifyState_m24795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24796_gshared (Enumerator_t4099 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m24796(__this, method) (( bool (*) (Enumerator_t4099 *, MethodInfo*))Enumerator_MoveNext_m24796_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t1400  Enumerator_get_Current_m24797_gshared (Enumerator_t4099 * __this, MethodInfo* method);
#define Enumerator_get_Current_m24797(__this, method) (( UICharInfo_t1400  (*) (Enumerator_t4099 *, MethodInfo*))Enumerator_get_Current_m24797_gshared)(__this, method)
