﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct InternalEnumerator_1_t3532;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17094_gshared (InternalEnumerator_1_t3532 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m17094(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3532 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m17094_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17095_gshared (InternalEnumerator_1_t3532 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17095(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3532 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17095_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17096_gshared (InternalEnumerator_1_t3532 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17096(__this, method) (( void (*) (InternalEnumerator_1_t3532 *, MethodInfo*))InternalEnumerator_1_Dispose_m17096_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17097_gshared (InternalEnumerator_1_t3532 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17097(__this, method) (( bool (*) (InternalEnumerator_1_t3532 *, MethodInfo*))InternalEnumerator_1_MoveNext_m17097_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C" KeyValuePair_2_t3531  InternalEnumerator_1_get_Current_m17098_gshared (InternalEnumerator_1_t3532 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17098(__this, method) (( KeyValuePair_2_t3531  (*) (InternalEnumerator_1_t3532 *, MethodInfo*))InternalEnumerator_1_get_Current_m17098_gshared)(__this, method)
