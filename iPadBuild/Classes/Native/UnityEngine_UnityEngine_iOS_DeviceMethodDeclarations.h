﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.iOS.Device
struct Device_t1573;
// UnityEngine.iOS.DeviceGeneration
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration.h"

// UnityEngine.iOS.DeviceGeneration UnityEngine.iOS.Device::get_generation()
extern "C" int32_t Device_get_generation_m4205 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
