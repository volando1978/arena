﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>
struct  Func_2_t973  : public MulticastDelegate_t22
{
};
