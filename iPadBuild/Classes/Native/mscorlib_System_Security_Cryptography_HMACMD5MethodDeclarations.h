﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACMD5
struct HMACMD5_t2689;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void System.Security.Cryptography.HMACMD5::.ctor()
extern "C" void HMACMD5__ctor_m12977 (HMACMD5_t2689 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACMD5::.ctor(System.Byte[])
extern "C" void HMACMD5__ctor_m12978 (HMACMD5_t2689 * __this, ByteU5BU5D_t350* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
