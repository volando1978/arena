﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder
struct TurnBasedMatchConfigBuilder_t495;
// System.String
struct String_t;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse_m2170 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetVariant(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetVariant_m2171 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___variant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_AddPlayerToInvite(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_AddPlayerToInvite_m2172 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___player_id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Construct()
extern "C" IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Construct_m2173 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetExclusiveBitMask(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetExclusiveBitMask_m2174 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint64_t ___exclusive_bit_mask, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers_m2175 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___maximum_automatching_players, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Create_m2176 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers_m2177 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___minimum_automatching_players, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Dispose_m2178 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
