﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameControl/<wait>c__Iterator8
struct U3CwaitU3Ec__Iterator8_t796;
// System.Object
struct Object_t;

// System.Void gameControl/<wait>c__Iterator8::.ctor()
extern "C" void U3CwaitU3Ec__Iterator8__ctor_m3419 (U3CwaitU3Ec__Iterator8_t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<wait>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420 (U3CwaitU3Ec__Iterator8_t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<wait>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421 (U3CwaitU3Ec__Iterator8_t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameControl/<wait>c__Iterator8::MoveNext()
extern "C" bool U3CwaitU3Ec__Iterator8_MoveNext_m3422 (U3CwaitU3Ec__Iterator8_t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<wait>c__Iterator8::Dispose()
extern "C" void U3CwaitU3Ec__Iterator8_Dispose_m3423 (U3CwaitU3Ec__Iterator8_t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<wait>c__Iterator8::Reset()
extern "C" void U3CwaitU3Ec__Iterator8_Reset_m3424 (U3CwaitU3Ec__Iterator8_t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
