﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeEvent[]
struct NativeEventU5BU5D_t3728;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>
struct  List_1_t868  : public Object_t
{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::_items
	NativeEventU5BU5D_t3728* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::_version
	int32_t ____version_3;
};
struct List_1_t868_StaticFields{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>::EmptyArray
	NativeEventU5BU5D_t3728* ___EmptyArray_4;
};
