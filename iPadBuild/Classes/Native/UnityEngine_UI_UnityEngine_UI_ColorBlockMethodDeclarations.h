﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ColorBlock
struct ColorBlock_t1221;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"

// UnityEngine.Color UnityEngine.UI.ColorBlock::get_normalColor()
extern "C" Color_t747  ColorBlock_get_normalColor_m4879 (ColorBlock_t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
extern "C" void ColorBlock_set_normalColor_m4880 (ColorBlock_t1221 * __this, Color_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_highlightedColor()
extern "C" Color_t747  ColorBlock_get_highlightedColor_m4881 (ColorBlock_t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_highlightedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_highlightedColor_m4882 (ColorBlock_t1221 * __this, Color_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_pressedColor()
extern "C" Color_t747  ColorBlock_get_pressedColor_m4883 (ColorBlock_t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_pressedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_pressedColor_m4884 (ColorBlock_t1221 * __this, Color_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_disabledColor()
extern "C" Color_t747  ColorBlock_get_disabledColor_m4885 (ColorBlock_t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_disabledColor(UnityEngine.Color)
extern "C" void ColorBlock_set_disabledColor_m4886 (ColorBlock_t1221 * __this, Color_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_colorMultiplier()
extern "C" float ColorBlock_get_colorMultiplier_m4887 (ColorBlock_t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_colorMultiplier(System.Single)
extern "C" void ColorBlock_set_colorMultiplier_m4888 (ColorBlock_t1221 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_fadeDuration()
extern "C" float ColorBlock_get_fadeDuration_m4889 (ColorBlock_t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_fadeDuration(System.Single)
extern "C" void ColorBlock_set_fadeDuration_m4890 (ColorBlock_t1221 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::get_defaultColorBlock()
extern "C" ColorBlock_t1221  ColorBlock_get_defaultColorBlock_m4891 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
