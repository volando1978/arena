﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// naveProxyController
struct naveProxyController_t820;

// System.Void naveProxyController::.ctor()
extern "C" void naveProxyController__ctor_m3587 (naveProxyController_t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void naveProxyController::.cctor()
extern "C" void naveProxyController__cctor_m3588 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void naveProxyController::Start()
extern "C" void naveProxyController_Start_m3589 (naveProxyController_t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void naveProxyController::Update()
extern "C" void naveProxyController_Update_m3590 (naveProxyController_t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean naveProxyController::isWorking()
extern "C" bool naveProxyController_isWorking_m3591 (naveProxyController_t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void naveProxyController::setWorking()
extern "C" void naveProxyController_setWorking_m3592 (naveProxyController_t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
