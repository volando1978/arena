﻿#pragma once
#include <stdint.h>
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.PurchaseType
struct  PurchaseType_t123  : public Object_t
{
	// Soomla.Store.PurchasableVirtualItem Soomla.Store.PurchaseType::AssociatedItem
	PurchasableVirtualItem_t124 * ___AssociatedItem_0;
};
