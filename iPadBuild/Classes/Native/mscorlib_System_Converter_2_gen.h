﻿#pragma once
#include <stdint.h>
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// ObjectKvp
struct ObjectKvp_t6;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Converter`2<ObjectKvp,UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct  Converter_2_t10  : public MulticastDelegate_t22
{
};
