﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// logoScene
struct  logoScene_t812  : public MonoBehaviour_t26
{
	// System.Single logoScene::fadeTime
	float ___fadeTime_2;
	// System.Boolean logoScene::fadeOut
	bool ___fadeOut_3;
	// System.Boolean logoScene::fadeIn
	bool ___fadeIn_4;
	// System.String logoScene::levelName
	String_t* ___levelName_5;
};
