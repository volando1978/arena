﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct List_1_t163;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct IEnumerable_1_t4264;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct IEnumerator_1_t4265;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct ICollection_1_t4266;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct ReadOnlyCollection_1_t3467;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>[]
struct UnityKeyValuePair_2U5BU5D_t3455;
// System.Predicate`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct Predicate_1_t3468;
// System.Action`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct Action_1_t3469;
// System.Comparison`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct Comparison_1_t3471;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m15804(__this, method) (( void (*) (List_1_t163 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15805(__this, ___collection, method) (( void (*) (List_1_t163 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::.ctor(System.Int32)
#define List_1__ctor_m15806(__this, ___capacity, method) (( void (*) (List_1_t163 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::.cctor()
#define List_1__cctor_m15807(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15808(__this, method) (( Object_t* (*) (List_1_t163 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15809(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t163 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15810(__this, method) (( Object_t * (*) (List_1_t163 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15811(__this, ___item, method) (( int32_t (*) (List_1_t163 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15812(__this, ___item, method) (( bool (*) (List_1_t163 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15813(__this, ___item, method) (( int32_t (*) (List_1_t163 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15814(__this, ___index, ___item, method) (( void (*) (List_1_t163 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15815(__this, ___item, method) (( void (*) (List_1_t163 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15816(__this, method) (( bool (*) (List_1_t163 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15817(__this, method) (( bool (*) (List_1_t163 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15818(__this, method) (( Object_t * (*) (List_1_t163 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15819(__this, method) (( bool (*) (List_1_t163 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15820(__this, method) (( bool (*) (List_1_t163 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15821(__this, ___index, method) (( Object_t * (*) (List_1_t163 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15822(__this, ___index, ___value, method) (( void (*) (List_1_t163 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Add(T)
#define List_1_Add_m15823(__this, ___item, method) (( void (*) (List_1_t163 *, UnityKeyValuePair_2_t164 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15824(__this, ___newCount, method) (( void (*) (List_1_t163 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15825(__this, ___collection, method) (( void (*) (List_1_t163 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15826(__this, ___enumerable, method) (( void (*) (List_1_t163 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15827(__this, ___collection, method) (( void (*) (List_1_t163 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::AsReadOnly()
#define List_1_AsReadOnly_m15828(__this, method) (( ReadOnlyCollection_1_t3467 * (*) (List_1_t163 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Clear()
#define List_1_Clear_m15829(__this, method) (( void (*) (List_1_t163 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Contains(T)
#define List_1_Contains_m15830(__this, ___item, method) (( bool (*) (List_1_t163 *, UnityKeyValuePair_2_t164 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15831(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t163 *, UnityKeyValuePair_2U5BU5D_t3455*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Find(System.Predicate`1<T>)
#define List_1_Find_m15832(__this, ___match, method) (( UnityKeyValuePair_2_t164 * (*) (List_1_t163 *, Predicate_1_t3468 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15833(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3468 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m15834(__this, ___match, method) (( int32_t (*) (List_1_t163 *, Predicate_1_t3468 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15835(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t163 *, int32_t, int32_t, Predicate_1_t3468 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m15836(__this, ___action, method) (( void (*) (List_1_t163 *, Action_1_t3469 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::GetEnumerator()
#define List_1_GetEnumerator_m15837(__this, method) (( Enumerator_t3470  (*) (List_1_t163 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::IndexOf(T)
#define List_1_IndexOf_m15838(__this, ___item, method) (( int32_t (*) (List_1_t163 *, UnityKeyValuePair_2_t164 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15839(__this, ___start, ___delta, method) (( void (*) (List_1_t163 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15840(__this, ___index, method) (( void (*) (List_1_t163 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Insert(System.Int32,T)
#define List_1_Insert_m15841(__this, ___index, ___item, method) (( void (*) (List_1_t163 *, int32_t, UnityKeyValuePair_2_t164 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15842(__this, ___collection, method) (( void (*) (List_1_t163 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Remove(T)
#define List_1_Remove_m15843(__this, ___item, method) (( bool (*) (List_1_t163 *, UnityKeyValuePair_2_t164 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15844(__this, ___match, method) (( int32_t (*) (List_1_t163 *, Predicate_1_t3468 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15845(__this, ___index, method) (( void (*) (List_1_t163 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Reverse()
#define List_1_Reverse_m15846(__this, method) (( void (*) (List_1_t163 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Sort()
#define List_1_Sort_m15847(__this, method) (( void (*) (List_1_t163 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15848(__this, ___comparison, method) (( void (*) (List_1_t163 *, Comparison_1_t3471 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::ToArray()
#define List_1_ToArray_m15849(__this, method) (( UnityKeyValuePair_2U5BU5D_t3455* (*) (List_1_t163 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::TrimExcess()
#define List_1_TrimExcess_m15850(__this, method) (( void (*) (List_1_t163 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::get_Capacity()
#define List_1_get_Capacity_m15851(__this, method) (( int32_t (*) (List_1_t163 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15852(__this, ___value, method) (( void (*) (List_1_t163 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::get_Count()
#define List_1_get_Count_m15853(__this, method) (( int32_t (*) (List_1_t163 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::get_Item(System.Int32)
#define List_1_get_Item_m15854(__this, ___index, method) (( UnityKeyValuePair_2_t164 * (*) (List_1_t163 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::set_Item(System.Int32,T)
#define List_1_set_Item_m15855(__this, ___index, ___value, method) (( void (*) (List_1_t163 *, int32_t, UnityKeyValuePair_2_t164 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
