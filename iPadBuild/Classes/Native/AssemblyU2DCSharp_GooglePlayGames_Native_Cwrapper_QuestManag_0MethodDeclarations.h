﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback
struct FetchListCallback_t445;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchListCallback__ctor_m1873 (FetchListCallback_t445 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchListCallback_Invoke_m1874 (FetchListCallback_t445 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchListCallback_t445(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchListCallback_BeginInvoke_m1875 (FetchListCallback_t445 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchListCallback_EndInvoke_m1876 (FetchListCallback_t445 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
