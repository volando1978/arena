﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Status/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_UIS.h"
// GooglePlayGames.Native.Cwrapper.Status/UIStatus
struct  UIStatus_t485 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Status/UIStatus::value__
	int32_t ___value___1;
};
