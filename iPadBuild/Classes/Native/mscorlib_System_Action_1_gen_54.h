﻿#pragma once
#include <stdint.h>
// JSONObject
struct JSONObject_t30;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<JSONObject>
struct  Action_1_t3497  : public MulticastDelegate_t22
{
};
