﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`2/<>c__AnonStorey4<System.Object,System.Object>
struct U3CU3Ec__AnonStorey4_t3436;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;

// System.Void UnityEngine.UnityDictionary`2/<>c__AnonStorey4<System.Object,System.Object>::.ctor()
extern "C" void U3CU3Ec__AnonStorey4__ctor_m15599_gshared (U3CU3Ec__AnonStorey4_t3436 * __this, MethodInfo* method);
#define U3CU3Ec__AnonStorey4__ctor_m15599(__this, method) (( void (*) (U3CU3Ec__AnonStorey4_t3436 *, MethodInfo*))U3CU3Ec__AnonStorey4__ctor_m15599_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2/<>c__AnonStorey4<System.Object,System.Object>::<>m__3(UnityEngine.UnityKeyValuePair`2<K,V>)
extern "C" bool U3CU3Ec__AnonStorey4_U3CU3Em__3_m15600_gshared (U3CU3Ec__AnonStorey4_t3436 * __this, UnityKeyValuePair_2_t3412 * ___x, MethodInfo* method);
#define U3CU3Ec__AnonStorey4_U3CU3Em__3_m15600(__this, ___x, method) (( bool (*) (U3CU3Ec__AnonStorey4_t3436 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))U3CU3Ec__AnonStorey4_U3CU3Em__3_m15600_gshared)(__this, ___x, method)
