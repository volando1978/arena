﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t1186;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.EventSystems.BaseRaycaster>
struct  Action_1_t3904  : public MulticastDelegate_t22
{
};
