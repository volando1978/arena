﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
struct  MultiplayerInvitationType_t519 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType::value__
	int32_t ___value___1;
};
