﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3580;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t221;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3482;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" void HashSet_1__ctor_m17601_gshared (HashSet_1_t3580 * __this, MethodInfo* method);
#define HashSet_1__ctor_m17601(__this, method) (( void (*) (HashSet_1_t3580 *, MethodInfo*))HashSet_1__ctor_m17601_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1__ctor_m17603_gshared (HashSet_1_t3580 * __this, Object_t* ___collection, Object_t* ___comparer, MethodInfo* method);
#define HashSet_1__ctor_m17603(__this, ___collection, ___comparer, method) (( void (*) (HashSet_1_t3580 *, Object_t*, Object_t*, MethodInfo*))HashSet_1__ctor_m17603_gshared)(__this, ___collection, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m17605_gshared (HashSet_1_t3580 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define HashSet_1__ctor_m17605(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3580 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1__ctor_m17605_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17607_gshared (HashSet_1_t3580 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17607(__this, method) (( Object_t* (*) (HashSet_1_t3580 *, MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17607_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17609_gshared (HashSet_1_t3580 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17609(__this, method) (( bool (*) (HashSet_1_t3580 *, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17609_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m17611_gshared (HashSet_1_t3580 * __this, ObjectU5BU5D_t208* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m17611(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3580 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m17611_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17613_gshared (HashSet_1_t3580 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17613(__this, ___item, method) (( void (*) (HashSet_1_t3580 *, Object_t *, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17613_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m17615_gshared (HashSet_1_t3580 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m17615(__this, method) (( Object_t * (*) (HashSet_1_t3580 *, MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m17615_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m17617_gshared (HashSet_1_t3580 * __this, MethodInfo* method);
#define HashSet_1_get_Count_m17617(__this, method) (( int32_t (*) (HashSet_1_t3580 *, MethodInfo*))HashSet_1_get_Count_m17617_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m17619_gshared (HashSet_1_t3580 * __this, int32_t ___capacity, Object_t* ___comparer, MethodInfo* method);
#define HashSet_1_Init_m17619(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t3580 *, int32_t, Object_t*, MethodInfo*))HashSet_1_Init_m17619_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m17621_gshared (HashSet_1_t3580 * __this, int32_t ___size, MethodInfo* method);
#define HashSet_1_InitArrays_m17621(__this, ___size, method) (( void (*) (HashSet_1_t3580 *, int32_t, MethodInfo*))HashSet_1_InitArrays_m17621_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m17623_gshared (HashSet_1_t3580 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m17623(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t3580 *, int32_t, int32_t, Object_t *, MethodInfo*))HashSet_1_SlotsContainsAt_m17623_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m17625_gshared (HashSet_1_t3580 * __this, ObjectU5BU5D_t208* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_CopyTo_m17625(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3580 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))HashSet_1_CopyTo_m17625_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m17627_gshared (HashSet_1_t3580 * __this, ObjectU5BU5D_t208* ___array, int32_t ___index, int32_t ___count, MethodInfo* method);
#define HashSet_1_CopyTo_m17627(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t3580 *, ObjectU5BU5D_t208*, int32_t, int32_t, MethodInfo*))HashSet_1_CopyTo_m17627_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern "C" void HashSet_1_Resize_m17629_gshared (HashSet_1_t3580 * __this, MethodInfo* method);
#define HashSet_1_Resize_m17629(__this, method) (( void (*) (HashSet_1_t3580 *, MethodInfo*))HashSet_1_Resize_m17629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m17631_gshared (HashSet_1_t3580 * __this, int32_t ___index, MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m17631(__this, ___index, method) (( int32_t (*) (HashSet_1_t3580 *, int32_t, MethodInfo*))HashSet_1_GetLinkHashCode_m17631_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m17633_gshared (HashSet_1_t3580 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_GetItemHashCode_m17633(__this, ___item, method) (( int32_t (*) (HashSet_1_t3580 *, Object_t *, MethodInfo*))HashSet_1_GetItemHashCode_m17633_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C" bool HashSet_1_Add_m17634_gshared (HashSet_1_t3580 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Add_m17634(__this, ___item, method) (( bool (*) (HashSet_1_t3580 *, Object_t *, MethodInfo*))HashSet_1_Add_m17634_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" void HashSet_1_Clear_m17636_gshared (HashSet_1_t3580 * __this, MethodInfo* method);
#define HashSet_1_Clear_m17636(__this, method) (( void (*) (HashSet_1_t3580 *, MethodInfo*))HashSet_1_Clear_m17636_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C" bool HashSet_1_Contains_m17638_gshared (HashSet_1_t3580 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Contains_m17638(__this, ___item, method) (( bool (*) (HashSet_1_t3580 *, Object_t *, MethodInfo*))HashSet_1_Contains_m17638_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern "C" bool HashSet_1_Remove_m17640_gshared (HashSet_1_t3580 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Remove_m17640(__this, ___item, method) (( bool (*) (HashSet_1_t3580 *, Object_t *, MethodInfo*))HashSet_1_Remove_m17640_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m17642_gshared (HashSet_1_t3580 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define HashSet_1_GetObjectData_m17642(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3580 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1_GetObjectData_m17642_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m17644_gshared (HashSet_1_t3580 * __this, Object_t * ___sender, MethodInfo* method);
#define HashSet_1_OnDeserialization_m17644(__this, ___sender, method) (( void (*) (HashSet_1_t3580 *, Object_t *, MethodInfo*))HashSet_1_OnDeserialization_m17644_gshared)(__this, ___sender, method)
