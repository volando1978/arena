﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.ReportProgress
struct ReportProgress_t380;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.ReportProgress::.ctor(System.Object,System.IntPtr)
extern "C" void ReportProgress__ctor_m3684 (ReportProgress_t380 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.ReportProgress::Invoke(System.String,System.Double,System.Action`1<System.Boolean>)
extern "C" void ReportProgress_Invoke_m3685 (ReportProgress_t380 * __this, String_t* ___id, double ___progress, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GooglePlayGames.ReportProgress::BeginInvoke(System.String,System.Double,System.Action`1<System.Boolean>,System.AsyncCallback,System.Object)
extern "C" Object_t * ReportProgress_BeginInvoke_m3686 (ReportProgress_t380 * __this, String_t* ___id, double ___progress, Action_1_t98 * ___callback, AsyncCallback_t20 * ____callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.ReportProgress::EndInvoke(System.IAsyncResult)
extern "C" void ReportProgress_EndInvoke_m3687 (ReportProgress_t380 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
