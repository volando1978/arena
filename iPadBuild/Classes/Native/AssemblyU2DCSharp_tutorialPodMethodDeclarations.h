﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// tutorialPod
struct tutorialPod_t838;
// UnityEngine.Collider
struct Collider_t900;

// System.Void tutorialPod::.ctor()
extern "C" void tutorialPod__ctor_m3660 (tutorialPod_t838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tutorialPod::Start()
extern "C" void tutorialPod_Start_m3661 (tutorialPod_t838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tutorialPod::OnTriggerEnter(UnityEngine.Collider)
extern "C" void tutorialPod_OnTriggerEnter_m3662 (tutorialPod_t838 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
