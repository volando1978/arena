﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualGoodsStorageIOS
struct VirtualGoodsStorageIOS_t86;
// System.String
struct String_t;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void Soomla.Store.VirtualGoodsStorageIOS::.ctor()
extern "C" void VirtualGoodsStorageIOS__ctor_m353 (VirtualGoodsStorageIOS_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_RemoveUpgrades(System.String,System.Boolean)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_RemoveUpgrades_m354 (Object_t * __this /* static, unused */, String_t* ___itemId, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_AssignCurrentUpgrade(System.String,System.String,System.Boolean)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_AssignCurrentUpgrade_m355 (Object_t * __this /* static, unused */, String_t* ___itemId, String_t* ___upgradeItemId, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_GetCurrentUpgrade(System.String,System.String&)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_GetCurrentUpgrade_m356 (Object_t * __this /* static, unused */, String_t* ___itemId, String_t** ___outItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_IsEquipped(System.String,System.Boolean&)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_IsEquipped_m357 (Object_t * __this /* static, unused */, String_t* ___itemId, bool* ___outResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_Equip(System.String,System.Boolean)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_Equip_m358 (Object_t * __this /* static, unused */, String_t* ___itemId, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_UnEquip(System.String,System.Boolean)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_UnEquip_m359 (Object_t * __this /* static, unused */, String_t* ___itemId, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_GetBalance(System.String,System.Int32&)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_GetBalance_m360 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_SetBalance(System.String,System.Int32,System.Boolean,System.Int32&)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_SetBalance_m361 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___balance, bool ___notify, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_Add(System.String,System.Int32,System.Boolean,System.Int32&)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_Add_m362 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___amount, bool ___notify, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::vgStorage_Remove(System.String,System.Int32,System.Boolean,System.Int32&)
extern "C" int32_t VirtualGoodsStorageIOS_vgStorage_Remove_m363 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___amount, bool ___notify, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorageIOS::_removeUpgrades(Soomla.Store.VirtualGood,System.Boolean)
extern "C" void VirtualGoodsStorageIOS__removeUpgrades_m364 (VirtualGoodsStorageIOS_t86 * __this, VirtualGood_t79 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorageIOS::_assignCurrentUpgrade(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG,System.Boolean)
extern "C" void VirtualGoodsStorageIOS__assignCurrentUpgrade_m365 (VirtualGoodsStorageIOS_t86 * __this, VirtualGood_t79 * ___good, UpgradeVG_t133 * ___upgradeVG, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.UpgradeVG Soomla.Store.VirtualGoodsStorageIOS::_getCurrentUpgrade(Soomla.Store.VirtualGood)
extern "C" UpgradeVG_t133 * VirtualGoodsStorageIOS__getCurrentUpgrade_m366 (VirtualGoodsStorageIOS_t86 * __this, VirtualGood_t79 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.VirtualGoodsStorageIOS::_isEquipped(Soomla.Store.EquippableVG)
extern "C" bool VirtualGoodsStorageIOS__isEquipped_m367 (VirtualGoodsStorageIOS_t86 * __this, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorageIOS::_equip(Soomla.Store.EquippableVG,System.Boolean)
extern "C" void VirtualGoodsStorageIOS__equip_m368 (VirtualGoodsStorageIOS_t86 * __this, EquippableVG_t129 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorageIOS::_unequip(Soomla.Store.EquippableVG,System.Boolean)
extern "C" void VirtualGoodsStorageIOS__unequip_m369 (VirtualGoodsStorageIOS_t86 * __this, EquippableVG_t129 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::_getBalance(Soomla.Store.VirtualItem)
extern "C" int32_t VirtualGoodsStorageIOS__getBalance_m370 (VirtualGoodsStorageIOS_t86 * __this, VirtualItem_t125 * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::_setBalance(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualGoodsStorageIOS__setBalance_m371 (VirtualGoodsStorageIOS_t86 * __this, VirtualItem_t125 * ___item, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::_add(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualGoodsStorageIOS__add_m372 (VirtualGoodsStorageIOS_t86 * __this, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorageIOS::_remove(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualGoodsStorageIOS__remove_m373 (VirtualGoodsStorageIOS_t86 * __this, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
