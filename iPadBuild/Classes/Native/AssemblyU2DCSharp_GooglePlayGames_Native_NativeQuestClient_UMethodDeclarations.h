﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26
struct U3CFetchU3Ec__AnonStorey26_t551;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
struct FetchResponse_t688;

// System.Void GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26::.ctor()
extern "C" void U3CFetchU3Ec__AnonStorey26__ctor_m2299 (U3CFetchU3Ec__AnonStorey26_t551 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26::<>m__1E(GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse)
extern "C" void U3CFetchU3Ec__AnonStorey26_U3CU3Em__1E_m2300 (U3CFetchU3Ec__AnonStorey26_t551 * __this, FetchResponse_t688 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
