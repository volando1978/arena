﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t1694;
// UnityEngine.Object
struct Object_t187;
struct Object_t187_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t208;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C" void CachedInvokableCall_1__ctor_m7535_gshared (CachedInvokableCall_1_t1694 * __this, Object_t187 * ___target, MethodInfo_t * ___theFunction, float ___argument, MethodInfo* method);
#define CachedInvokableCall_1__ctor_m7535(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1694 *, Object_t187 *, MethodInfo_t *, float, MethodInfo*))CachedInvokableCall_1__ctor_m7535_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C" void CachedInvokableCall_1_Invoke_m25299_gshared (CachedInvokableCall_1_t1694 * __this, ObjectU5BU5D_t208* ___args, MethodInfo* method);
#define CachedInvokableCall_1_Invoke_m25299(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1694 *, ObjectU5BU5D_t208*, MethodInfo*))CachedInvokableCall_1_Invoke_m25299_gshared)(__this, ___args, method)
