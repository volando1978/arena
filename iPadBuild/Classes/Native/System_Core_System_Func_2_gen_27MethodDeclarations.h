﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeSnapshotMetadata>
struct Func_2_t965;
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeSnapshotMetadata>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.UIntPtr,System.Object>
#include "System_Core_System_Func_2_gen_43MethodDeclarations.h"
#define Func_2__ctor_m3986(__this, ___object, ___method, method) (( void (*) (Func_2_t965 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20391_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeSnapshotMetadata>::Invoke(T)
#define Func_2_Invoke_m20894(__this, ___arg1, method) (( NativeSnapshotMetadata_t611 * (*) (Func_2_t965 *, UIntPtr_t , MethodInfo*))Func_2_Invoke_m20393_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeSnapshotMetadata>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20895(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t965 *, UIntPtr_t , AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20395_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeSnapshotMetadata>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20896(__this, ___result, method) (( NativeSnapshotMetadata_t611 * (*) (Func_2_t965 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20397_gshared)(__this, ___result, method)
