﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3985;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t1233;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.UI.Graphic>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_5MethodDeclarations.h"
#define Predicate_1__ctor_m23236(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3985 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m15507_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Graphic>::Invoke(T)
#define Predicate_1_Invoke_m23237(__this, ___obj, method) (( bool (*) (Predicate_1_t3985 *, Graphic_t1233 *, MethodInfo*))Predicate_1_Invoke_m15508_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UI.Graphic>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m23238(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3985 *, Graphic_t1233 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m15509_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Graphic>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m23239(__this, ___result, method) (( bool (*) (Predicate_1_t3985 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m15510_gshared)(__this, ___result, method)
