﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t686;
// System.Object
#include "mscorlib_System_Object.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62
struct  U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687  : public Object_t
{
	// System.UIntPtr GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62::index
	UIntPtr_t  ___index_0;
	// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62::<>f__this
	PlayerSelectUIResponse_t686 * ___U3CU3Ef__this_1;
};
