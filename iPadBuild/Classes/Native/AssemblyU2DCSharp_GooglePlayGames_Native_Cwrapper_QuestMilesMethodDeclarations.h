﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.QuestMilestone
struct QuestMilestone_t450;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques_0.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_EventId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_EventId_m1913 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CurrentCount(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t QuestMilestone_QuestMilestone_CurrentCount_m1914 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Copy(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t QuestMilestone_QuestMilestone_Copy_m1915 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void QuestMilestone_QuestMilestone_Dispose_m1916 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_TargetCount(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t QuestMilestone_QuestMilestone_TargetCount_m1917 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_QuestId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_QuestId_m1918 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CompletionRewardData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_CompletionRewardData_m1919 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_State(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t QuestMilestone_QuestMilestone_State_m1920 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool QuestMilestone_QuestMilestone_Valid_m1921 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_Id_m1922 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
