﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.TimeSpan>
struct Nullable_1_t377;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m3713_gshared (Nullable_1_t377 * __this, TimeSpan_t190  ___value, MethodInfo* method);
#define Nullable_1__ctor_m3713(__this, ___value, method) (( void (*) (Nullable_1_t377 *, TimeSpan_t190 , MethodInfo*))Nullable_1__ctor_m3713_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m3714_gshared (Nullable_1_t377 * __this, MethodInfo* method);
#define Nullable_1_get_HasValue_m3714(__this, method) (( bool (*) (Nullable_1_t377 *, MethodInfo*))Nullable_1_get_HasValue_m3714_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t190  Nullable_1_get_Value_m3870_gshared (Nullable_1_t377 * __this, MethodInfo* method);
#define Nullable_1_get_Value_m3870(__this, method) (( TimeSpan_t190  (*) (Nullable_1_t377 *, MethodInfo*))Nullable_1_get_Value_m3870_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m19210_gshared (Nullable_1_t377 * __this, Object_t * ___other, MethodInfo* method);
#define Nullable_1_Equals_m19210(__this, ___other, method) (( bool (*) (Nullable_1_t377 *, Object_t *, MethodInfo*))Nullable_1_Equals_m19210_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m19211_gshared (Nullable_1_t377 * __this, Nullable_1_t377  ___other, MethodInfo* method);
#define Nullable_1_Equals_m19211(__this, ___other, method) (( bool (*) (Nullable_1_t377 *, Nullable_1_t377 , MethodInfo*))Nullable_1_Equals_m19211_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m19212_gshared (Nullable_1_t377 * __this, MethodInfo* method);
#define Nullable_1_GetHashCode_m19212(__this, method) (( int32_t (*) (Nullable_1_t377 *, MethodInfo*))Nullable_1_GetHashCode_m19212_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m19213_gshared (Nullable_1_t377 * __this, MethodInfo* method);
#define Nullable_1_ToString_m19213(__this, method) (( String_t* (*) (Nullable_1_t377 *, MethodInfo*))Nullable_1_ToString_m19213_gshared)(__this, method)
