﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Coin
struct  Coin_t727  : public MonoBehaviour_t26
{
	// System.Int32 Coin::lifeTime
	int32_t ___lifeTime_2;
	// System.Int32 Coin::coinTypeVar
	int32_t ___coinTypeVar_3;
	// System.Single Coin::aumento
	float ___aumento_4;
	// System.Boolean Coin::lift
	bool ___lift_5;
	// UnityEngine.GUIStyle Coin::coinSt
	GUIStyle_t724 * ___coinSt_6;
	// System.Single Coin::t
	float ___t_7;
};
