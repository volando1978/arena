﻿#pragma once
#include <stdint.h>
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>[]
struct UnityKeyValuePair_2U5BU5D_t3455;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct  List_1_t163  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::_items
	UnityKeyValuePair_2U5BU5D_t3455* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::_version
	int32_t ____version_3;
};
struct List_1_t163_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::EmptyArray
	UnityKeyValuePair_2U5BU5D_t3455* ___EmptyArray_4;
};
