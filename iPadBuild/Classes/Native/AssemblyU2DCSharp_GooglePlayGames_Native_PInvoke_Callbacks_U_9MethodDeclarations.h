﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3782;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Object>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20420_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3782 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20420(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3782 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20420_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Object>::<>m__76(T1,T2)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20421_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3782 * __this, Object_t * ___result1, Object_t * ___result2, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20421(__this, ___result1, ___result2, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3782 *, Object_t *, Object_t *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20421_gshared)(__this, ___result1, ___result2, method)
