﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t1330;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t1331;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t1368;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m6167(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t1330 *, UnityAction_1_t1331 *, UnityAction_1_t1331 *, MethodInfo*))ObjectPool_1__ctor_m21655_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
#define ObjectPool_1_get_countAll_m24062(__this, method) (( int32_t (*) (ObjectPool_1_t1330 *, MethodInfo*))ObjectPool_1_get_countAll_m21657_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m24063(__this, ___value, method) (( void (*) (ObjectPool_1_t1330 *, int32_t, MethodInfo*))ObjectPool_1_set_countAll_m21659_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
#define ObjectPool_1_get_countActive_m24064(__this, method) (( int32_t (*) (ObjectPool_1_t1330 *, MethodInfo*))ObjectPool_1_get_countActive_m21661_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m24065(__this, method) (( int32_t (*) (ObjectPool_1_t1330 *, MethodInfo*))ObjectPool_1_get_countInactive_m21663_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
#define ObjectPool_1_Get_m6168(__this, method) (( List_1_t1368 * (*) (ObjectPool_1_t1330 *, MethodInfo*))ObjectPool_1_Get_m21665_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
#define ObjectPool_1_Release_m6169(__this, ___element, method) (( void (*) (ObjectPool_1_t1330 *, List_1_t1368 *, MethodInfo*))ObjectPool_1_Release_m21667_gshared)(__this, ___element, method)
