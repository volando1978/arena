﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.LineRenderer
struct LineRenderer_t743;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RayoScr
struct  RayoScr_t752  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject RayoScr::player
	GameObject_t144 * ___player_2;
	// UnityEngine.GameObject RayoScr::target
	GameObject_t144 * ___target_3;
	// UnityEngine.LineRenderer RayoScr::lr
	LineRenderer_t743 * ___lr_4;
	// System.Int32 RayoScr::timer
	int32_t ___timer_5;
	// UnityEngine.GameObject RayoScr::enemyController
	GameObject_t144 * ___enemyController_6;
	// UnityEngine.GameObject RayoScr::randomExplosion
	GameObject_t144 * ___randomExplosion_7;
};
