﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
struct  X509CertificateEnumerator_t2029  : public Object_t
{
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::enumerator
	Object_t * ___enumerator_0;
};
