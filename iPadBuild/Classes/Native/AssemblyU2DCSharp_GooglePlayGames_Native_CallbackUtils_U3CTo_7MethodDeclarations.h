﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Int32,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey18_3_t3736;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Int32,System.Object,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19885_gshared (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3736 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19885(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3736 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19885_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Int32,System.Object,System.Object>::<>m__9(T1,T2,T3)
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19886_gshared (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3736 * __this, int32_t ___val1, Object_t * ___val2, Object_t * ___val3, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19886(__this, ___val1, ___val2, ___val3, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey18_3_t3736 *, int32_t, Object_t *, Object_t *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19886_gshared)(__this, ___val1, ___val2, ___val3, method)
