﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.AudioClip
struct AudioClip_t753;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// bulletThinSrc
struct  bulletThinSrc_t768  : public MonoBehaviour_t26
{
	// System.Single bulletThinSrc::speed
	float ___speed_2;
	// System.Single bulletThinSrc::angle
	float ___angle_3;
	// System.Single bulletThinSrc::step
	float ___step_4;
	// UnityEngine.Vector3 bulletThinSrc::p
	Vector3_t758  ___p_5;
	// UnityEngine.GameObject bulletThinSrc::enemyController
	GameObject_t144 * ___enemyController_6;
	// UnityEngine.GameObject bulletThinSrc::paquete
	GameObject_t144 * ___paquete_7;
	// UnityEngine.GameObject bulletThinSrc::coin
	GameObject_t144 * ___coin_8;
	// UnityEngine.GameObject bulletThinSrc::movingText
	GameObject_t144 * ___movingText_9;
	// UnityEngine.GameObject bulletThinSrc::movingTextKills
	GameObject_t144 * ___movingTextKills_10;
	// UnityEngine.GameObject bulletThinSrc::explosion
	GameObject_t144 * ___explosion_11;
	// UnityEngine.GameObject bulletThinSrc::dust
	GameObject_t144 * ___dust_12;
	// UnityEngine.AudioClip bulletThinSrc::shotSnd
	AudioClip_t753 * ___shotSnd_13;
};
