﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>
struct Func_2_t699;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager
struct  RealtimeManager_t564  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.PInvoke.RealtimeManager::mGameServices
	GameServices_t534 * ___mGameServices_0;
};
struct RealtimeManager_t564_StaticFields{
	// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr> GooglePlayGames.Native.PInvoke.RealtimeManager::<>f__am$cache1
	Func_2_t699 * ___U3CU3Ef__amU24cache1_1;
};
