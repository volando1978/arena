﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1485;
// UnityEngine.AssetBundle
struct AssetBundle_t1487;

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m6348 (AssetBundleCreateRequest_t1485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t1487 * AssetBundleCreateRequest_get_assetBundle_m6349 (AssetBundleCreateRequest_t1485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m6350 (AssetBundleCreateRequest_t1485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
