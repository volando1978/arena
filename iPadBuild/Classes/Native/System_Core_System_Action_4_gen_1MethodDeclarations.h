﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct Action_4_t4167;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_4__ctor_m25522_gshared (Action_4_t4167 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_4__ctor_m25522(__this, ___object, ___method, method) (( void (*) (Action_4_t4167 *, Object_t *, IntPtr_t, MethodInfo*))Action_4__ctor_m25522_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
extern "C" void Action_4_Invoke_m25523_gshared (Action_4_t4167 * __this, Object_t * ___arg1, Object_t * ___arg2, Object_t * ___arg3, Object_t * ___arg4, MethodInfo* method);
#define Action_4_Invoke_m25523(__this, ___arg1, ___arg2, ___arg3, ___arg4, method) (( void (*) (Action_4_t4167 *, Object_t *, Object_t *, Object_t *, Object_t *, MethodInfo*))Action_4_Invoke_m25523_gshared)(__this, ___arg1, ___arg2, ___arg3, ___arg4, method)
// System.IAsyncResult System.Action`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_4_BeginInvoke_m25524_gshared (Action_4_t4167 * __this, Object_t * ___arg1, Object_t * ___arg2, Object_t * ___arg3, Object_t * ___arg4, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_4_BeginInvoke_m25524(__this, ___arg1, ___arg2, ___arg3, ___arg4, ___callback, ___object, method) (( Object_t * (*) (Action_4_t4167 *, Object_t *, Object_t *, Object_t *, Object_t *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_4_BeginInvoke_m25524_gshared)(__this, ___arg1, ___arg2, ___arg3, ___arg4, ___callback, ___object, method)
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void Action_4_EndInvoke_m25525_gshared (Action_4_t4167 * __this, Object_t * ___result, MethodInfo* method);
#define Action_4_EndInvoke_m25525(__this, ___result, method) (( void (*) (Action_4_t4167 *, Object_t *, MethodInfo*))Action_4_EndInvoke_m25525_gshared)(__this, ___result, method)
