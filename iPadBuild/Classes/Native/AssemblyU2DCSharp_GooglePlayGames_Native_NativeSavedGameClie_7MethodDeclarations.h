﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48
struct U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628;
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
struct SnapshotSelectUIResponse_t705;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48::.ctor()
extern "C" void U3CShowSelectSavedGameUIU3Ec__AnonStorey48__ctor_m2518 (U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey48::<>m__4C(GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse)
extern "C" void U3CShowSelectSavedGameUIU3Ec__AnonStorey48_U3CU3Em__4C_m2519 (U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628 * __this, SnapshotSelectUIResponse_t705 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
