﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t861;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct  Predicate_1_t3685  : public MulticastDelegate_t22
{
};
