﻿#pragma once
#include <stdint.h>
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>
struct Func_2_t577;
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Func_2_t578;
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>
struct Func_2_t352;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_9.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState
struct  ActiveState_t586  : public MessagingEnabledState_t580
{
};
struct ActiveState_t586_StaticFields{
	// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache0
	Func_2_t577 * ___U3CU3Ef__amU24cache0_8;
	// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache1
	Func_2_t578 * ___U3CU3Ef__amU24cache1_9;
	// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache2
	Func_2_t352 * ___U3CU3Ef__amU24cache2_10;
	// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache3
	Func_2_t352 * ___U3CU3Ef__amU24cache3_11;
};
