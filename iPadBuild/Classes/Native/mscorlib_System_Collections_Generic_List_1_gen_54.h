﻿#pragma once
#include <stdint.h>
// System.IntPtr[]
struct IntPtrU5BU5D_t859;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.IntPtr>
struct  List_1_t3807  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.IntPtr>::_items
	IntPtrU5BU5D_t859* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::_version
	int32_t ____version_3;
};
struct List_1_t3807_StaticFields{
	// T[] System.Collections.Generic.List`1<System.IntPtr>::EmptyArray
	IntPtrU5BU5D_t859* ___EmptyArray_4;
};
