﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t563;
// GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener
struct RealTimeMultiplayerListener_t573;
// System.String[]
struct StringU5BU5D_t169;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::.ctor(GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void OnGameThreadForwardingListener__ctor_m2364 (OnGameThreadForwardingListener_t563 * __this, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::RoomSetupProgress(System.Single)
extern "C" void OnGameThreadForwardingListener_RoomSetupProgress_m2365 (OnGameThreadForwardingListener_t563 * __this, float ___percent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::RoomConnected(System.Boolean)
extern "C" void OnGameThreadForwardingListener_RoomConnected_m2366 (OnGameThreadForwardingListener_t563 * __this, bool ___success, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::LeftRoom()
extern "C" void OnGameThreadForwardingListener_LeftRoom_m2367 (OnGameThreadForwardingListener_t563 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::PeersConnected(System.String[])
extern "C" void OnGameThreadForwardingListener_PeersConnected_m2368 (OnGameThreadForwardingListener_t563 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::PeersDisconnected(System.String[])
extern "C" void OnGameThreadForwardingListener_PeersDisconnected_m2369 (OnGameThreadForwardingListener_t563 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::RealTimeMessageReceived(System.Boolean,System.String,System.Byte[])
extern "C" void OnGameThreadForwardingListener_RealTimeMessageReceived_m2370 (OnGameThreadForwardingListener_t563 * __this, bool ___isReliable, String_t* ___senderId, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::ParticipantLeft(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" void OnGameThreadForwardingListener_ParticipantLeft_m2371 (OnGameThreadForwardingListener_t563 * __this, Participant_t340 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::<LeftRoom>m__32()
extern "C" void OnGameThreadForwardingListener_U3CLeftRoomU3Em__32_m2372 (OnGameThreadForwardingListener_t563 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
