﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A
struct U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631;
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
struct FetchAllResponse_t702;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A::.ctor()
extern "C" void U3CFetchAllSavedGamesU3Ec__AnonStorey4A__ctor_m2522 (U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey4A::<>m__4E(GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse)
extern "C" void U3CFetchAllSavedGamesU3Ec__AnonStorey4A_U3CU3Em__4E_m2523 (U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631 * __this, FetchAllResponse_t702 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
