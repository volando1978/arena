﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// movingTextKillsScr
struct movingTextKillsScr_t817;

// System.Void movingTextKillsScr::.ctor()
extern "C" void movingTextKillsScr__ctor_m3577 (movingTextKillsScr_t817 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextKillsScr::Start()
extern "C" void movingTextKillsScr_Start_m3578 (movingTextKillsScr_t817 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextKillsScr::Update()
extern "C" void movingTextKillsScr_Update_m3579 (movingTextKillsScr_t817 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextKillsScr::OnGUI()
extern "C" void movingTextKillsScr_OnGUI_m3580 (movingTextKillsScr_t817 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
