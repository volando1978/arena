﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Byte[]>
struct Enumerator_t4086;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1550;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m24612(__this, ___l, method) (( void (*) (Enumerator_t4086 *, List_1_t1550 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24613(__this, method) (( Object_t * (*) (Enumerator_t4086 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::Dispose()
#define Enumerator_Dispose_m24614(__this, method) (( void (*) (Enumerator_t4086 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::VerifyState()
#define Enumerator_VerifyState_m24615(__this, method) (( void (*) (Enumerator_t4086 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte[]>::MoveNext()
#define Enumerator_MoveNext_m24616(__this, method) (( bool (*) (Enumerator_t4086 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Byte[]>::get_Current()
#define Enumerator_get_Current_m24617(__this, method) (( ByteU5BU5D_t350* (*) (Enumerator_t4086 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
