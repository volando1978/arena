﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>
struct Enumerator_t3660;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3655;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18982_gshared (Enumerator_t3660 * __this, Dictionary_2_t3655 * ___host, MethodInfo* method);
#define Enumerator__ctor_m18982(__this, ___host, method) (( void (*) (Enumerator_t3660 *, Dictionary_2_t3655 *, MethodInfo*))Enumerator__ctor_m18982_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18983_gshared (Enumerator_t3660 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18983(__this, method) (( Object_t * (*) (Enumerator_t3660 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18983_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::Dispose()
extern "C" void Enumerator_Dispose_m18984_gshared (Enumerator_t3660 * __this, MethodInfo* method);
#define Enumerator_Dispose_m18984(__this, method) (( void (*) (Enumerator_t3660 *, MethodInfo*))Enumerator_Dispose_m18984_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18985_gshared (Enumerator_t3660 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m18985(__this, method) (( bool (*) (Enumerator_t3660 *, MethodInfo*))Enumerator_MoveNext_m18985_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m18986_gshared (Enumerator_t3660 * __this, MethodInfo* method);
#define Enumerator_get_Current_m18986(__this, method) (( Object_t * (*) (Enumerator_t3660 *, MethodInfo*))Enumerator_get_Current_m18986_gshared)(__this, method)
