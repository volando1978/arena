﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Reward>
struct Enumerator_t206;
// System.Object
struct Object_t;
// Soomla.Reward
struct Reward_t55;
// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m16900(__this, ___l, method) (( void (*) (Enumerator_t206 *, List_1_t56 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16901(__this, method) (( Object_t * (*) (Enumerator_t206 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::Dispose()
#define Enumerator_Dispose_m16902(__this, method) (( void (*) (Enumerator_t206 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::VerifyState()
#define Enumerator_VerifyState_m16903(__this, method) (( void (*) (Enumerator_t206 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::MoveNext()
#define Enumerator_MoveNext_m936(__this, method) (( bool (*) (Enumerator_t206 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::get_Current()
#define Enumerator_get_Current_m935(__this, method) (( Reward_t55 * (*) (Enumerator_t206 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
