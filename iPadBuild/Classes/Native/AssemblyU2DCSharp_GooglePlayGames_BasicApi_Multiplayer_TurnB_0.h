﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_0.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
struct  MatchTurnStatus_t349 
{
	// System.Int32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus::value__
	int32_t ___value___1;
};
