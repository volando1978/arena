﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Object,System.Byte>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3725;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Object,System.Byte>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m19760_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3725 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m19760(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3725 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m19760_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Object,System.Byte>::<>m__78()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m19761_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3725 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m19761(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3725 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m19761_gshared)(__this, method)
