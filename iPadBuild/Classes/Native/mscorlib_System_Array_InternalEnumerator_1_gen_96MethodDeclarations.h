﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.MonoType>
struct InternalEnumerator_1_t4239;
// System.Object
struct Object_t;
// System.MonoType
struct MonoType_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.MonoType>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m25998(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4239 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.MonoType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25999(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4239 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.MonoType>::Dispose()
#define InternalEnumerator_1_Dispose_m26000(__this, method) (( void (*) (InternalEnumerator_1_t4239 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.MonoType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m26001(__this, method) (( bool (*) (InternalEnumerator_1_t4239 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.MonoType>::get_Current()
#define InternalEnumerator_1_get_Current_m26002(__this, method) (( MonoType_t * (*) (InternalEnumerator_1_t4239 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
