﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>
struct Func_2_t934;
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeAchievement
struct NativeAchievement_t673;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.UIntPtr,System.Object>
#include "System_Core_System_Func_2_gen_43MethodDeclarations.h"
#define Func_2__ctor_m3890(__this, ___object, ___method, method) (( void (*) (Func_2_t934 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20391_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>::Invoke(T)
#define Func_2_Invoke_m20392(__this, ___arg1, method) (( NativeAchievement_t673 * (*) (Func_2_t934 *, UIntPtr_t , MethodInfo*))Func_2_Invoke_m20393_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20394(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t934 *, UIntPtr_t , AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20395_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20396(__this, ___result, method) (( NativeAchievement_t673 * (*) (Func_2_t934 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20397_gshared)(__this, ___result, method)
