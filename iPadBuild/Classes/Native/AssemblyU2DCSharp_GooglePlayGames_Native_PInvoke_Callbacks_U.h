﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Object,System.Byte>
struct Action_2_t912;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Byte>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724  : public Object_t
{
	// System.Action`2<T1,T2> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Byte>::toInvokeOnGameThread
	Action_2_t912 * ___toInvokeOnGameThread_0;
};
