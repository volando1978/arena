﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<cabezaScr/Frame>
struct List_1_t772;
// cabezaScr/Frame
struct Frame_t770;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>
struct  Enumerator_t3828 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::l
	List_1_t772 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::current
	Frame_t770 * ___current_3;
};
