﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<ObjectKvp>
struct IList_1_t3473;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<ObjectKvp>
struct  ReadOnlyCollection_1_t3474  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ObjectKvp>::list
	Object_t* ___list_0;
};
