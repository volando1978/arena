﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// ADInterstitial
#include "AssemblyU2DCSharp_ADInterstitial.h"
#ifndef _MSC_VER
#else
#endif
// ADInterstitial
#include "AssemblyU2DCSharp_ADInterstitialMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.iOS.ADInterstitialAd
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.iOS.ADInterstitialAd
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAdMethodDeclarations.h"


// System.Void ADInterstitial::.ctor()
extern "C" void ADInterstitial__ctor_m1322 (ADInterstitial_t332 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ADInterstitial::.cctor()
extern "C" void ADInterstitial__cctor_m1323 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ADInterstitial::Start()
extern TypeInfo* ADInterstitialAd_t331_il2cpp_TypeInfo_var;
extern TypeInfo* ADInterstitial_t332_il2cpp_TypeInfo_var;
extern "C" void ADInterstitial_Start_m1324 (ADInterstitial_t332 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ADInterstitialAd_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		ADInterstitial_t332_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(596);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		ADInterstitialAd_t331 * L_0 = (ADInterstitialAd_t331 *)il2cpp_codegen_object_new (ADInterstitialAd_t331_il2cpp_TypeInfo_var);
		ADInterstitialAd__ctor_m3688(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitial_t332_il2cpp_TypeInfo_var);
		((ADInterstitial_t332_StaticFields*)ADInterstitial_t332_il2cpp_TypeInfo_var->static_fields)->___fullscreenAd_2 = L_0;
		return;
	}
}
// System.Void ADInterstitial::OnFullscreenLoaded()
extern "C" void ADInterstitial_OnFullscreenLoaded_m1325 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ADInterstitial::Update()
extern "C" void ADInterstitial_Update_m1326 (ADInterstitial_t332 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// GooglePlayGames.BasicApi.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achievement.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_AchievementMethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Void GooglePlayGames.BasicApi.Achievement::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Achievement__ctor_m1327 (Achievement_t333 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mId_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mDescription_6 = L_1;
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mName_7 = L_2;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Achievement::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t189_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Achievement_ToString_m1328 (Achievement_t333 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Int32_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t208* G_B2_1 = {0};
	ObjectU5BU5D_t208* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t208* G_B1_1 = {0};
	ObjectU5BU5D_t208* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t208* G_B3_2 = {0};
	ObjectU5BU5D_t208* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	{
		ObjectU5BU5D_t208* L_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 8));
		String_t* L_1 = (__this->___mId_0);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t208* L_2 = L_0;
		String_t* L_3 = (__this->___mName_7);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t208* L_4 = L_2;
		String_t* L_5 = (__this->___mDescription_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_5;
		ObjectU5BU5D_t208* L_6 = L_4;
		bool L_7 = (__this->___mIsIncremental_1);
		G_B1_0 = 3;
		G_B1_1 = L_6;
		G_B1_2 = L_6;
		G_B1_3 = (String_t*) &_stringLiteral334;
		if (!L_7)
		{
			G_B2_0 = 3;
			G_B2_1 = L_6;
			G_B2_2 = L_6;
			G_B2_3 = (String_t*) &_stringLiteral334;
			goto IL_003d;
		}
	}
	{
		G_B3_0 = (String_t*) &_stringLiteral335;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0042;
	}

IL_003d:
	{
		G_B3_0 = (String_t*) &_stringLiteral336;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0042:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1)) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t208* L_8 = G_B3_3;
		bool L_9 = (__this->___mIsRevealed_2);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 4)) = (Object_t *)L_11;
		ObjectU5BU5D_t208* L_12 = L_8;
		bool L_13 = (__this->___mIsUnlocked_3);
		bool L_14 = L_13;
		Object_t * L_15 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 5)) = (Object_t *)L_15;
		ObjectU5BU5D_t208* L_16 = L_12;
		int32_t L_17 = (__this->___mCurrentSteps_4);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 6);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 6)) = (Object_t *)L_19;
		ObjectU5BU5D_t208* L_20 = L_16;
		int32_t L_21 = (__this->___mTotalSteps_5);
		int32_t L_22 = L_21;
		Object_t * L_23 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 7);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 7)) = (Object_t *)L_23;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Format_m3689(NULL /*static, unused*/, G_B3_4, L_20, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsIncremental()
extern "C" bool Achievement_get_IsIncremental_m1329 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mIsIncremental_1);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsIncremental(System.Boolean)
extern "C" void Achievement_set_IsIncremental_m1330 (Achievement_t333 * __this, bool ___value, MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___mIsIncremental_1 = L_0;
		return;
	}
}
// System.Int32 GooglePlayGames.BasicApi.Achievement::get_CurrentSteps()
extern "C" int32_t Achievement_get_CurrentSteps_m1331 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mCurrentSteps_4);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_CurrentSteps(System.Int32)
extern "C" void Achievement_set_CurrentSteps_m1332 (Achievement_t333 * __this, int32_t ___value, MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mCurrentSteps_4 = L_0;
		return;
	}
}
// System.Int32 GooglePlayGames.BasicApi.Achievement::get_TotalSteps()
extern "C" int32_t Achievement_get_TotalSteps_m1333 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mTotalSteps_5);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_TotalSteps(System.Int32)
extern "C" void Achievement_set_TotalSteps_m1334 (Achievement_t333 * __this, int32_t ___value, MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mTotalSteps_5 = L_0;
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsUnlocked()
extern "C" bool Achievement_get_IsUnlocked_m1335 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mIsUnlocked_3);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsUnlocked(System.Boolean)
extern "C" void Achievement_set_IsUnlocked_m1336 (Achievement_t333 * __this, bool ___value, MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___mIsUnlocked_3 = L_0;
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsRevealed()
extern "C" bool Achievement_get_IsRevealed_m1337 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mIsRevealed_2);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsRevealed(System.Boolean)
extern "C" void Achievement_set_IsRevealed_m1338 (Achievement_t333 * __this, bool ___value, MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___mIsRevealed_2 = L_0;
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Achievement::get_Id()
extern "C" String_t* Achievement_get_Id_m1339 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mId_0);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_Id(System.String)
extern "C" void Achievement_set_Id_m1340 (Achievement_t333 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mId_0 = L_0;
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Achievement::get_Description()
extern "C" String_t* Achievement_get_Description_m1341 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mDescription_6);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_Description(System.String)
extern "C" void Achievement_set_Description_m1342 (Achievement_t333 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mDescription_6 = L_0;
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Achievement::get_Name()
extern "C" String_t* Achievement_get_Name_m1343 (Achievement_t333 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mName_7);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.Achievement::set_Name(System.String)
extern "C" void Achievement_set_Name_m1344 (Achievement_t333 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mName_7 = L_0;
		return;
	}
}
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSourceMethodDeclarations.h"



// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatus.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatusMethodDeclarations.h"



// GooglePlayGames.BasicApi.UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatusMethodDeclarations.h"



// GooglePlayGames.BasicApi.DummyClient
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DummyClient.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.DummyClient
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DummyClientMethodDeclarations.h"

// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_List_1_gen_13.h"
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
#include "mscorlib_System_Action_1_gen_6.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceive.h"
// GooglePlayGames.BasicApi.Multiplayer.Invitation
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit_0.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
#include "mscorlib_System_Action_1_gen_6MethodDeclarations.h"
// GooglePlayGames.OurUtils.Logger
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_LoggerMethodDeclarations.h"


// System.Void GooglePlayGames.BasicApi.DummyClient::.ctor()
extern "C" void DummyClient__ctor_m1345 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C" void DummyClient_Authenticate_m1346 (DummyClient_t337 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.DummyClient::IsAuthenticated()
extern "C" bool DummyClient_IsAuthenticated_m1347 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::SignOut()
extern "C" void DummyClient_SignOut_m1348 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserId()
extern "C" String_t* DummyClient_GetUserId_m1349 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (String_t*) &_stringLiteral337;
	}
}
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserDisplayName()
extern "C" String_t* DummyClient_GetUserDisplayName_m1350 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (String_t*) &_stringLiteral338;
	}
}
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserImageUrl()
extern "C" String_t* DummyClient_GetUserImageUrl_m1351 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (String_t*)NULL;
	}
}
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement> GooglePlayGames.BasicApi.DummyClient::GetAchievements()
extern TypeInfo* List_1_t841_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3690_MethodInfo_var;
extern "C" List_1_t841 * DummyClient_GetAchievements_m1352 (DummyClient_t337 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t841_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(598);
		List_1__ctor_m3690_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		s_Il2CppMethodIntialized = true;
	}
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t841_il2cpp_TypeInfo_var);
		List_1_t841 * L_0 = (List_1_t841 *)il2cpp_codegen_object_new (List_1_t841_il2cpp_TypeInfo_var);
		List_1__ctor_m3690(L_0, /*hidden argument*/List_1__ctor_m3690_MethodInfo_var);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.BasicApi.DummyClient::GetAchievement(System.String)
extern "C" Achievement_t333 * DummyClient_GetAchievement_m1353 (DummyClient_t337 * __this, String_t* ___achId, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (Achievement_t333 *)NULL;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::UnlockAchievement(System.String,System.Action`1<System.Boolean>)
extern "C" void DummyClient_UnlockAchievement_m1354 (DummyClient_t337 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_0012:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::RevealAchievement(System.String,System.Action`1<System.Boolean>)
extern "C" void DummyClient_RevealAchievement_m1355 (DummyClient_t337 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_0012:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern "C" void DummyClient_IncrementAchievement_m1356 (DummyClient_t337 * __this, String_t* ___achId, int32_t ___steps, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_0012:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void DummyClient_ShowAchievementsUI_m1357 (DummyClient_t337 * __this, Action_1_t530 * ___callback, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t530 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t530 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::Invoke(!0) */, L_1, ((int32_t)-4));
	}

IL_0013:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void DummyClient_ShowLeaderboardUI_m1358 (DummyClient_t337 * __this, String_t* ___lbId, Action_1_t530 * ___callback, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t530 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t530 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::Invoke(!0) */, L_1, ((int32_t)-4));
	}

IL_0013:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::SubmitScore(System.String,System.Int64,System.Action`1<System.Boolean>)
extern "C" void DummyClient_SubmitScore_m1359 (DummyClient_t337 * __this, String_t* ___lbId, int64_t ___score, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_0012:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern TypeInfo* OnStateLoadedListener_t842_il2cpp_TypeInfo_var;
extern "C" void DummyClient_LoadState_m1360 (DummyClient_t337 * __this, int32_t ___slot, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnStateLoadedListener_t842_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		s_Il2CppMethodIntialized = true;
	}
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t * L_0 = ___listener;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Object_t * L_1 = ___listener;
		int32_t L_2 = ___slot;
		NullCheck(L_1);
		InterfaceActionInvoker3< bool, int32_t, ByteU5BU5D_t350* >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.OnStateLoadedListener::OnStateLoaded(System.Boolean,System.Int32,System.Byte[]) */, OnStateLoadedListener_t842_il2cpp_TypeInfo_var, L_1, 0, L_2, (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void DummyClient_UpdateState_m1361 (DummyClient_t337 * __this, int32_t ___slot, ByteU5BU5D_t350* ___data, Object_t * ___listener, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.BasicApi.DummyClient::GetRtmpClient()
extern "C" Object_t * DummyClient_GetRtmpClient_m1362 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.BasicApi.DummyClient::GetTbmpClient()
extern "C" Object_t * DummyClient_GetTbmpClient_m1363 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}
}
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.BasicApi.DummyClient::GetSavedGameClient()
extern "C" Object_t * DummyClient_GetSavedGameClient_m1364 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}
}
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.BasicApi.DummyClient::GetEventsClient()
extern "C" Object_t * DummyClient_GetEventsClient_m1365 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}
}
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.BasicApi.DummyClient::GetQuestsClient()
extern "C" Object_t * DummyClient_GetQuestsClient_m1366 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern "C" void DummyClient_RegisterInvitationDelegate_m1367 (DummyClient_t337 * __this, InvitationReceivedDelegate_t363 * ___deleg, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Invitation GooglePlayGames.BasicApi.DummyClient::GetInvitationFromNotification()
extern "C" Invitation_t341 * DummyClient_GetInvitationFromNotification_m1368 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (Invitation_t341 *)NULL;
	}
}
// System.Boolean GooglePlayGames.BasicApi.DummyClient::HasInvitationFromNotification()
extern "C" bool DummyClient_HasInvitationFromNotification_m1369 (DummyClient_t337 * __this, MethodInfo* method)
{
	{
		DummyClient_LogUsage_m1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Void GooglePlayGames.BasicApi.DummyClient::LogUsage()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void DummyClient_LogUsage_m1370 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral339, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Events.EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_EventVisib.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Events.EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_EventVisibMethodDeclarations.h"



// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_InvitMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.Invitation
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit_0MethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.Participant
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti_0.h"


// System.Void GooglePlayGames.BasicApi.Multiplayer.Invitation::.ctor(GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType,System.String,GooglePlayGames.BasicApi.Multiplayer.Participant,System.Int32)
extern "C" void Invitation__ctor_m1371 (Invitation_t341 * __this, int32_t ___invType, String_t* ___invId, Participant_t340 * ___inviter, int32_t ___variant, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___invType;
		__this->___mInvitationType_0 = L_0;
		String_t* L_1 = ___invId;
		__this->___mInvitationId_1 = L_1;
		Participant_t340 * L_2 = ___inviter;
		__this->___mInviter_2 = L_2;
		int32_t L_3 = ___variant;
		__this->___mVariant_3 = L_3;
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType GooglePlayGames.BasicApi.Multiplayer.Invitation::get_InvitationType()
extern "C" int32_t Invitation_get_InvitationType_m1372 (Invitation_t341 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mInvitationType_0);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Invitation::get_InvitationId()
extern "C" String_t* Invitation_get_InvitationId_m1373 (Invitation_t341 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mInvitationId_1);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.Invitation::get_Inviter()
extern "C" Participant_t340 * Invitation_get_Inviter_m1374 (Invitation_t341 * __this, MethodInfo* method)
{
	{
		Participant_t340 * L_0 = (__this->___mInviter_2);
		return L_0;
	}
}
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Invitation::get_Variant()
extern "C" int32_t Invitation_get_Variant_m1375 (Invitation_t341 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mVariant_3);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Invitation::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* InvType_t339_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t189_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Invitation_ToString_m1376 (Invitation_t341 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		InvType_t339_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		Int32_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t208* L_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		int32_t L_1 = Invitation_get_InvitationType_m1372(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(InvType_t339_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t208* L_4 = L_0;
		String_t* L_5 = Invitation_get_InvitationId_m1373(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t208* L_6 = L_4;
		Participant_t340 * L_7 = Invitation_get_Inviter_m1374(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)L_7;
		ObjectU5BU5D_t208* L_8 = L_6;
		int32_t L_9 = Invitation_get_Variant_m1375(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_11;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m3689(NULL /*static, unused*/, (String_t*) &_stringLiteral340, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_MatchMethodDeclarations.h"



// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_0MethodDeclarations.h"

// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_3.h"
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_3MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"


// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::.ctor()
extern TypeInfo* List_1_t43_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t343_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t344_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m883_MethodInfo_var;
extern MethodInfo* Dictionary_2__ctor_m3691_MethodInfo_var;
extern MethodInfo* Dictionary_2__ctor_m3692_MethodInfo_var;
extern "C" void MatchOutcome__ctor_m1377 (MatchOutcome_t345 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		Dictionary_2_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		Dictionary_2_t344_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		List_1__ctor_m883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		Dictionary_2__ctor_m3691_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483815);
		Dictionary_2__ctor_m3692_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483816);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t43_il2cpp_TypeInfo_var);
		List_1_t43 * L_0 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m883(L_0, /*hidden argument*/List_1__ctor_m883_MethodInfo_var);
		__this->___mParticipantIds_1 = L_0;
		Dictionary_2_t343 * L_1 = (Dictionary_2_t343 *)il2cpp_codegen_object_new (Dictionary_2_t343_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3691(L_1, /*hidden argument*/Dictionary_2__ctor_m3691_MethodInfo_var);
		__this->___mPlacements_2 = L_1;
		Dictionary_2_t344 * L_2 = (Dictionary_2_t344 *)il2cpp_codegen_object_new (Dictionary_2_t344_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3692(L_2, /*hidden argument*/Dictionary_2__ctor_m3692_MethodInfo_var);
		__this->___mResults_3 = L_2;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::SetParticipantResult(System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult,System.UInt32)
extern "C" void MatchOutcome_SetParticipantResult_m1378 (MatchOutcome_t345 * __this, String_t* ___participantId, int32_t ___result, uint32_t ___placement, MethodInfo* method)
{
	{
		List_1_t43 * L_0 = (__this->___mParticipantIds_1);
		String_t* L_1 = ___participantId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t43 * L_3 = (__this->___mParticipantIds_1);
		String_t* L_4 = ___participantId;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_3, L_4);
	}

IL_001d:
	{
		Dictionary_2_t343 * L_5 = (__this->___mPlacements_2);
		String_t* L_6 = ___participantId;
		uint32_t L_7 = ___placement;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, uint32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::set_Item(!0,!1) */, L_5, L_6, L_7);
		Dictionary_2_t344 * L_8 = (__this->___mResults_3);
		String_t* L_9 = ___participantId;
		int32_t L_10 = ___result;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Item(!0,!1) */, L_8, L_9, L_10);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::SetParticipantResult(System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult)
extern "C" void MatchOutcome_SetParticipantResult_m1379 (MatchOutcome_t345 * __this, String_t* ___participantId, int32_t ___result, MethodInfo* method)
{
	{
		String_t* L_0 = ___participantId;
		int32_t L_1 = ___result;
		MatchOutcome_SetParticipantResult_m1378(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::SetParticipantResult(System.String,System.UInt32)
extern "C" void MatchOutcome_SetParticipantResult_m1380 (MatchOutcome_t345 * __this, String_t* ___participantId, uint32_t ___placement, MethodInfo* method)
{
	{
		String_t* L_0 = ___participantId;
		uint32_t L_1 = ___placement;
		MatchOutcome_SetParticipantResult_m1378(__this, L_0, (-1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::get_ParticipantIds()
extern "C" List_1_t43 * MatchOutcome_get_ParticipantIds_m1381 (MatchOutcome_t345 * __this, MethodInfo* method)
{
	{
		List_1_t43 * L_0 = (__this->___mParticipantIds_1);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::GetResultFor(System.String)
extern "C" int32_t MatchOutcome_GetResultFor_m1382 (MatchOutcome_t345 * __this, String_t* ___participantId, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Dictionary_2_t344 * L_0 = (__this->___mResults_3);
		String_t* L_1 = ___participantId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Dictionary_2_t344 * L_3 = (__this->___mResults_3);
		String_t* L_4 = ___participantId;
		NullCheck(L_3);
		int32_t L_5 = (int32_t)VirtFuncInvoker1< int32_t, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Item(!0) */, L_3, L_4);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = (-1);
	}

IL_0023:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::GetPlacementFor(System.String)
extern "C" uint32_t MatchOutcome_GetPlacementFor_m1383 (MatchOutcome_t345 * __this, String_t* ___participantId, MethodInfo* method)
{
	uint32_t G_B3_0 = 0;
	{
		Dictionary_2_t343 * L_0 = (__this->___mPlacements_2);
		String_t* L_1 = ___participantId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Dictionary_2_t343 * L_3 = (__this->___mPlacements_2);
		String_t* L_4 = ___participantId;
		NullCheck(L_3);
		uint32_t L_5 = (uint32_t)VirtFuncInvoker1< uint32_t, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::get_Item(!0) */, L_3, L_4);
		G_B3_0 = L_5;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((uint32_t)(0));
	}

IL_0023:
	{
		return G_B3_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::ToString()
extern TypeInfo* ParticipantResult_t342_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t235_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t214_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* List_1_GetEnumerator_m969_MethodInfo_var;
extern MethodInfo* Enumerator_get_Current_m970_MethodInfo_var;
extern MethodInfo* Enumerator_MoveNext_m971_MethodInfo_var;
extern "C" String_t* MatchOutcome_ToString_m1384 (MatchOutcome_t345 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticipantResult_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		UInt32_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(170);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enumerator_t214_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		List_1_GetEnumerator_m969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483755);
		Enumerator_get_Current_m970_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483756);
		Enumerator_MoveNext_m971_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483757);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	Enumerator_t214  V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*) &_stringLiteral341;
		List_1_t43 * L_0 = (__this->___mParticipantIds_1);
		NullCheck(L_0);
		Enumerator_t214  L_1 = List_1_GetEnumerator_m969(L_0, /*hidden argument*/List_1_GetEnumerator_m969_MethodInfo_var);
		V_2 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0049;
		}

IL_0017:
		{
			String_t* L_2 = Enumerator_get_Current_m970((&V_2), /*hidden argument*/Enumerator_get_Current_m970_MethodInfo_var);
			V_1 = L_2;
			String_t* L_3 = V_0;
			String_t* L_4 = V_1;
			String_t* L_5 = V_1;
			int32_t L_6 = MatchOutcome_GetResultFor_m1382(__this, L_5, /*hidden argument*/NULL);
			int32_t L_7 = L_6;
			Object_t * L_8 = Box(ParticipantResult_t342_il2cpp_TypeInfo_var, &L_7);
			String_t* L_9 = V_1;
			uint32_t L_10 = MatchOutcome_GetPlacementFor_m1383(__this, L_9, /*hidden argument*/NULL);
			uint32_t L_11 = L_10;
			Object_t * L_12 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_11);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Format_m3693(NULL /*static, unused*/, (String_t*) &_stringLiteral342, L_4, L_8, L_12, /*hidden argument*/NULL);
			String_t* L_14 = String_Concat_m856(NULL /*static, unused*/, L_3, L_13, /*hidden argument*/NULL);
			V_0 = L_14;
		}

IL_0049:
		{
			bool L_15 = Enumerator_MoveNext_m971((&V_2), /*hidden argument*/Enumerator_MoveNext_m971_MethodInfo_var);
			if (L_15)
			{
				goto IL_0017;
			}
		}

IL_0055:
		{
			IL2CPP_LEAVE(0x66, FINALLY_005a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_005a;
	}

FINALLY_005a:
	{ // begin finally (depth: 1)
		Enumerator_t214  L_16 = V_2;
		Enumerator_t214  L_17 = L_16;
		Object_t * L_18 = Box(Enumerator_t214_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_18);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_18);
		IL2CPP_END_FINALLY(90)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(90)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0066:
	{
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m856(NULL /*static, unused*/, L_19, (String_t*) &_stringLiteral44, /*hidden argument*/NULL);
		return L_20;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_PartiMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.Participant
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti_0MethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.Player
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Playe.h"
// System.Enum
#include "mscorlib_System_Enum.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
// GooglePlayGames.BasicApi.Multiplayer.Player
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_PlayeMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"


// System.Void GooglePlayGames.BasicApi.Multiplayer.Participant::.ctor(System.String,System.String,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Player,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Participant__ctor_m1385 (Participant_t340 * __this, String_t* ___displayName, String_t* ___participantId, int32_t ___status, Player_t347 * ___player, bool ___connectedToRoom, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mDisplayName_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mParticipantId_1 = L_1;
		__this->___mStatus_2 = 7;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___displayName;
		__this->___mDisplayName_0 = L_2;
		String_t* L_3 = ___participantId;
		__this->___mParticipantId_1 = L_3;
		int32_t L_4 = ___status;
		__this->___mStatus_2 = L_4;
		Player_t347 * L_5 = ___player;
		__this->___mPlayer_3 = L_5;
		bool L_6 = ___connectedToRoom;
		__this->___mIsConnectedToRoom_4 = L_6;
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Participant::get_DisplayName()
extern "C" String_t* Participant_get_DisplayName_m1386 (Participant_t340 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mDisplayName_0);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Participant::get_ParticipantId()
extern "C" String_t* Participant_get_ParticipantId_m1387 (Participant_t340 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mParticipantId_1);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus GooglePlayGames.BasicApi.Multiplayer.Participant::get_Status()
extern "C" int32_t Participant_get_Status_m1388 (Participant_t340 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStatus_2);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Player GooglePlayGames.BasicApi.Multiplayer.Participant::get_Player()
extern "C" Player_t347 * Participant_get_Player_m1389 (Participant_t340 * __this, MethodInfo* method)
{
	{
		Player_t347 * L_0 = (__this->___mPlayer_3);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Participant::get_IsConnectedToRoom()
extern "C" bool Participant_get_IsConnectedToRoom_m1390 (Participant_t340 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mIsConnectedToRoom_4);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Participant::get_IsAutomatch()
extern "C" bool Participant_get_IsAutomatch_m1391 (Participant_t340 * __this, MethodInfo* method)
{
	{
		Player_t347 * L_0 = (__this->___mPlayer_3);
		return ((((Object_t*)(Player_t347 *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Participant::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* ParticipantStatus_t346_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Participant_ToString_m1392 (Participant_t340 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		ParticipantStatus_t346_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t208* G_B2_1 = {0};
	ObjectU5BU5D_t208* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t208* G_B1_1 = {0};
	ObjectU5BU5D_t208* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t208* G_B3_2 = {0};
	ObjectU5BU5D_t208* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	{
		ObjectU5BU5D_t208* L_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 5));
		String_t* L_1 = (__this->___mDisplayName_0);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t208* L_2 = L_0;
		String_t* L_3 = (__this->___mParticipantId_1);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t208* L_4 = L_2;
		int32_t L_5 = (__this->___mStatus_2);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(ParticipantStatus_t346_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_8;
		ObjectU5BU5D_t208* L_9 = L_4;
		Player_t347 * L_10 = (__this->___mPlayer_3);
		G_B1_0 = 3;
		G_B1_1 = L_9;
		G_B1_2 = L_9;
		G_B1_3 = (String_t*) &_stringLiteral343;
		if (L_10)
		{
			G_B2_0 = 3;
			G_B2_1 = L_9;
			G_B2_2 = L_9;
			G_B2_3 = (String_t*) &_stringLiteral343;
			goto IL_0047;
		}
	}
	{
		G_B3_0 = (String_t*) &_stringLiteral344;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0052;
	}

IL_0047:
	{
		Player_t347 * L_11 = (__this->___mPlayer_3);
		NullCheck(L_11);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String GooglePlayGames.BasicApi.Multiplayer.Player::ToString() */, L_11);
		G_B3_0 = L_12;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0052:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1)) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t208* L_13 = G_B3_3;
		bool L_14 = (__this->___mIsConnectedToRoom_4);
		bool L_15 = L_14;
		Object_t * L_16 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4)) = (Object_t *)L_16;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m3689(NULL /*static, unused*/, G_B3_4, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Participant::CompareTo(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" int32_t Participant_CompareTo_m1393 (Participant_t340 * __this, Participant_t340 * ___other, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mParticipantId_1);
		Participant_t340 * L_1 = ___other;
		NullCheck(L_1);
		String_t* L_2 = (L_1->___mParticipantId_1);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)VirtFuncInvoker1< int32_t, String_t* >::Invoke(22 /* System.Int32 System.String::CompareTo(System.String) */, L_0, L_2);
		return L_3;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Participant::Equals(System.Object)
extern const Il2CppType* Participant_t340_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Participant_t340_il2cpp_TypeInfo_var;
extern "C" bool Participant_Equals_m1394 (Participant_t340 * __this, Object_t * ___obj, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Participant_t340_0_0_0_var = il2cpp_codegen_type_from_index(606);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		Participant_t340_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(606);
		s_Il2CppMethodIntialized = true;
	}
	Participant_t340 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		bool L_2 = Object_ReferenceEquals_m3694(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		Object_t * L_3 = ___obj;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m932(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(Participant_t340_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_002d;
		}
	}
	{
		return 0;
	}

IL_002d:
	{
		Object_t * L_6 = ___obj;
		V_0 = ((Participant_t340 *)Castclass(L_6, Participant_t340_il2cpp_TypeInfo_var));
		String_t* L_7 = (__this->___mParticipantId_1);
		Participant_t340 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = (L_8->___mParticipantId_1);
		NullCheck(L_7);
		bool L_10 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_7, L_9);
		return L_10;
	}
}
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Participant::GetHashCode()
extern "C" int32_t Participant_GetHashCode_m1395 (Participant_t340 * __this, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___mParticipantId_1);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = (__this->___mParticipantId_1);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.String::GetHashCode() */, L_1);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.BasicApi.Multiplayer.Player::.ctor(System.String,System.String,System.String)
extern "C" void Player__ctor_m1396 (Player_t347 * __this, String_t* ___displayName, String_t* ___playerId, String_t* ___avatarUrl, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___displayName;
		__this->___mDisplayName_0 = L_0;
		String_t* L_1 = ___playerId;
		__this->___mPlayerId_1 = L_1;
		String_t* L_2 = ___avatarUrl;
		__this->___mAvatarUrl_2 = L_2;
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_DisplayName()
extern "C" String_t* Player_get_DisplayName_m1397 (Player_t347 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mDisplayName_0);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_PlayerId()
extern "C" String_t* Player_get_PlayerId_m1398 (Player_t347 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mPlayerId_1);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_AvatarURL()
extern "C" String_t* Player_get_AvatarURL_m1399 (Player_t347 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mAvatarUrl_2);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Player_ToString_m1400 (Player_t347 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___mDisplayName_0);
		String_t* L_1 = (__this->___mPlayerId_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral345, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Player::Equals(System.Object)
extern const Il2CppType* Player_t347_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Player_t347_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool Player_Equals_m1401 (Player_t347 * __this, Object_t * ___obj, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Player_t347_0_0_0_var = il2cpp_codegen_type_from_index(607);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		Player_t347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Player_t347 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		bool L_2 = Object_ReferenceEquals_m3694(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		Object_t * L_3 = ___obj;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m932(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(Player_t347_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_002d;
		}
	}
	{
		return 0;
	}

IL_002d:
	{
		Object_t * L_6 = ___obj;
		V_0 = ((Player_t347 *)Castclass(L_6, Player_t347_il2cpp_TypeInfo_var));
		String_t* L_7 = (__this->___mPlayerId_1);
		Player_t347 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = (L_8->___mPlayerId_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m835(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Player::GetHashCode()
extern "C" int32_t Player_GetHashCode_m1402 (Player_t347 * __this, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___mPlayerId_1);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = (__this->___mPlayerId_1);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.String::GetHashCode() */, L_1);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnBMethodDeclarations.h"



// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_0MethodDeclarations.h"



// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_1MethodDeclarations.h"

// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_gen_14.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>
#include "System_Core_System_Func_2_gen_0.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_gen_14MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9MethodDeclarations.h"
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>
#include "System_Core_System_Func_2_gen_0MethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
struct Enumerable_t219;
struct IEnumerable_1_t170;
struct IEnumerable_1_t902;
struct Func_2_t352;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t219;
struct IEnumerable_1_t221;
struct Func_2_t903;
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C" Object_t* Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t903 * p1, MethodInfo* method);
#define Enumerable_Select_TisObject_t_TisObject_t_m3696(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t903 *, MethodInfo*))Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisParticipant_t340_TisString_t_m3695(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t352 *, MethodInfo*))Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t219;
struct StringU5BU5D_t169;
struct IEnumerable_1_t170;
struct Enumerable_t219;
struct ObjectU5BU5D_t208;
struct IEnumerable_1_t221;
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" ObjectU5BU5D_t208* Enumerable_ToArray_TisObject_t_m3698_gshared (Object_t * __this /* static, unused */, Object_t* p0, MethodInfo* method);
#define Enumerable_ToArray_TisObject_t_m3698(__this /* static, unused */, p0, method) (( ObjectU5BU5D_t208* (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToArray_TisObject_t_m3698_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m3697(__this /* static, unused */, p0, method) (( StringU5BU5D_t169* (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToArray_TisObject_t_m3698_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::.ctor(System.String,System.Byte[],System.Boolean,System.String,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>,System.UInt32,System.String,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus,System.UInt32,System.UInt32)
extern MethodInfo* List_1_Sort_m3699_MethodInfo_var;
extern "C" void TurnBasedMatch__ctor_m1403 (TurnBasedMatch_t353 * __this, String_t* ___matchId, ByteU5BU5D_t350* ___data, bool ___canRematch, String_t* ___selfParticipantId, List_1_t351 * ___participants, uint32_t ___availableAutomatchSlots, String_t* ___pendingParticipantId, int32_t ___turnStatus, int32_t ___matchStatus, uint32_t ___variant, uint32_t ___version, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_Sort_m3699_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483817);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___matchId;
		__this->___mMatchId_0 = L_0;
		ByteU5BU5D_t350* L_1 = ___data;
		__this->___mData_1 = L_1;
		bool L_2 = ___canRematch;
		__this->___mCanRematch_2 = L_2;
		String_t* L_3 = ___selfParticipantId;
		__this->___mSelfParticipantId_4 = L_3;
		List_1_t351 * L_4 = ___participants;
		__this->___mParticipants_5 = L_4;
		List_1_t351 * L_5 = (__this->___mParticipants_5);
		NullCheck(L_5);
		List_1_Sort_m3699(L_5, /*hidden argument*/List_1_Sort_m3699_MethodInfo_var);
		uint32_t L_6 = ___availableAutomatchSlots;
		__this->___mAvailableAutomatchSlots_3 = L_6;
		String_t* L_7 = ___pendingParticipantId;
		__this->___mPendingParticipantId_6 = L_7;
		int32_t L_8 = ___turnStatus;
		__this->___mTurnStatus_7 = L_8;
		int32_t L_9 = ___matchStatus;
		__this->___mMatchStatus_8 = L_9;
		uint32_t L_10 = ___variant;
		__this->___mVariant_9 = L_10;
		uint32_t L_11 = ___version;
		__this->___mVersion_10 = L_11;
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_MatchId()
extern "C" String_t* TurnBasedMatch_get_MatchId_m1404 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mMatchId_0);
		return L_0;
	}
}
// System.Byte[] GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Data()
extern "C" ByteU5BU5D_t350* TurnBasedMatch_get_Data_m1405 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		ByteU5BU5D_t350* L_0 = (__this->___mData_1);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_CanRematch()
extern "C" bool TurnBasedMatch_get_CanRematch_m1406 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mCanRematch_2);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_SelfParticipantId()
extern "C" String_t* TurnBasedMatch_get_SelfParticipantId_m1407 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mSelfParticipantId_4);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Self()
extern "C" Participant_t340 * TurnBasedMatch_get_Self_m1408 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mSelfParticipantId_4);
		Participant_t340 * L_1 = TurnBasedMatch_GetParticipant_m1409(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::GetParticipant(System.String)
extern TypeInfo* Enumerator_t904_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* List_1_GetEnumerator_m3700_MethodInfo_var;
extern MethodInfo* Enumerator_get_Current_m3701_MethodInfo_var;
extern MethodInfo* Enumerator_MoveNext_m3702_MethodInfo_var;
extern "C" Participant_t340 * TurnBasedMatch_GetParticipant_m1409 (TurnBasedMatch_t353 * __this, String_t* ___participantId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		List_1_GetEnumerator_m3700_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483818);
		Enumerator_get_Current_m3701_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483819);
		Enumerator_MoveNext_m3702_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483820);
		s_Il2CppMethodIntialized = true;
	}
	Participant_t340 * V_0 = {0};
	Enumerator_t904  V_1 = {0};
	Participant_t340 * V_2 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t351 * L_0 = (__this->___mParticipants_5);
		NullCheck(L_0);
		Enumerator_t904  L_1 = List_1_GetEnumerator_m3700(L_0, /*hidden argument*/List_1_GetEnumerator_m3700_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0011:
		{
			Participant_t340 * L_2 = Enumerator_get_Current_m3701((&V_1), /*hidden argument*/Enumerator_get_Current_m3701_MethodInfo_var);
			V_0 = L_2;
			Participant_t340 * L_3 = V_0;
			NullCheck(L_3);
			String_t* L_4 = Participant_get_ParticipantId_m1387(L_3, /*hidden argument*/NULL);
			String_t* L_5 = ___participantId;
			NullCheck(L_4);
			bool L_6 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_4, L_5);
			if (!L_6)
			{
				goto IL_0031;
			}
		}

IL_002a:
		{
			Participant_t340 * L_7 = V_0;
			V_2 = L_7;
			IL2CPP_LEAVE(0x60, FINALLY_0042);
		}

IL_0031:
		{
			bool L_8 = Enumerator_MoveNext_m3702((&V_1), /*hidden argument*/Enumerator_MoveNext_m3702_MethodInfo_var);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		Enumerator_t904  L_9 = V_1;
		Enumerator_t904  L_10 = L_9;
		Object_t * L_11 = Box(Enumerator_t904_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_11);
		IL2CPP_END_FINALLY(66)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_004e:
	{
		String_t* L_12 = ___participantId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral346, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return (Participant_t340 *)NULL;
	}

IL_0060:
	{
		Participant_t340 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Participants()
extern "C" List_1_t351 * TurnBasedMatch_get_Participants_m1410 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		List_1_t351 * L_0 = (__this->___mParticipants_5);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_PendingParticipantId()
extern "C" String_t* TurnBasedMatch_get_PendingParticipantId_m1411 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mPendingParticipantId_6);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_PendingParticipant()
extern "C" Participant_t340 * TurnBasedMatch_get_PendingParticipant_m1412 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	Participant_t340 * G_B3_0 = {0};
	{
		String_t* L_0 = (__this->___mPendingParticipantId_6);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((Participant_t340 *)(NULL));
		goto IL_001d;
	}

IL_0011:
	{
		String_t* L_1 = (__this->___mPendingParticipantId_6);
		Participant_t340 * L_2 = TurnBasedMatch_GetParticipant_m1409(__this, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_TurnStatus()
extern "C" int32_t TurnBasedMatch_get_TurnStatus_m1413 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mTurnStatus_7);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Status()
extern "C" int32_t TurnBasedMatch_get_Status_m1414 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mMatchStatus_8);
		return L_0;
	}
}
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Variant()
extern "C" uint32_t TurnBasedMatch_get_Variant_m1415 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mVariant_9);
		return L_0;
	}
}
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Version()
extern "C" uint32_t TurnBasedMatch_get_Version_m1416 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mVersion_10);
		return L_0;
	}
}
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_AvailableAutomatchSlots()
extern "C" uint32_t TurnBasedMatch_get_AvailableAutomatchSlots_m1417 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mAvailableAutomatchSlots_3);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::ToString()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern TypeInfo* TurnBasedMatch_t353_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t352_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MatchTurnStatus_t349_il2cpp_TypeInfo_var;
extern TypeInfo* MatchStatus_t348_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t235_il2cpp_TypeInfo_var;
extern MethodInfo* TurnBasedMatch_U3CToStringU3Em__0_m1419_MethodInfo_var;
extern MethodInfo* Func_2__ctor_m3703_MethodInfo_var;
extern MethodInfo* Enumerable_Select_TisParticipant_t340_TisString_t_m3695_MethodInfo_var;
extern MethodInfo* Enumerable_ToArray_TisString_t_m3697_MethodInfo_var;
extern "C" String_t* TurnBasedMatch_ToString_m1418 (TurnBasedMatch_t353 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		TurnBasedMatch_t353_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		Func_2_t352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		MatchTurnStatus_t349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(611);
		MatchStatus_t348_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(612);
		UInt32_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(170);
		TurnBasedMatch_U3CToStringU3Em__0_m1419_MethodInfo_var = il2cpp_codegen_method_info_from_index(173);
		Func_2__ctor_m3703_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483822);
		Enumerable_Select_TisParticipant_t340_TisString_t_m3695_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483823);
		Enumerable_ToArray_TisString_t_m3697_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483824);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t351 * G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	int32_t G_B2_2 = 0;
	ObjectU5BU5D_t208* G_B2_3 = {0};
	ObjectU5BU5D_t208* G_B2_4 = {0};
	String_t* G_B2_5 = {0};
	List_1_t351 * G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	int32_t G_B1_2 = 0;
	ObjectU5BU5D_t208* G_B1_3 = {0};
	ObjectU5BU5D_t208* G_B1_4 = {0};
	String_t* G_B1_5 = {0};
	{
		ObjectU5BU5D_t208* L_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, ((int32_t)10)));
		String_t* L_1 = (__this->___mMatchId_0);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t208* L_2 = L_0;
		ByteU5BU5D_t350* L_3 = (__this->___mData_1);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t208* L_4 = L_2;
		bool L_5 = (__this->___mCanRematch_2);
		bool L_6 = L_5;
		Object_t * L_7 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_7;
		ObjectU5BU5D_t208* L_8 = L_4;
		String_t* L_9 = (__this->___mSelfParticipantId_4);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_9;
		ObjectU5BU5D_t208* L_10 = L_8;
		List_1_t351 * L_11 = (__this->___mParticipants_5);
		Func_2_t352 * L_12 = ((TurnBasedMatch_t353_StaticFields*)TurnBasedMatch_t353_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheB_11;
		G_B1_0 = L_11;
		G_B1_1 = (String_t*) &_stringLiteral41;
		G_B1_2 = 4;
		G_B1_3 = L_10;
		G_B1_4 = L_10;
		G_B1_5 = (String_t*) &_stringLiteral347;
		if (L_12)
		{
			G_B2_0 = L_11;
			G_B2_1 = (String_t*) &_stringLiteral41;
			G_B2_2 = 4;
			G_B2_3 = L_10;
			G_B2_4 = L_10;
			G_B2_5 = (String_t*) &_stringLiteral347;
			goto IL_005a;
		}
	}
	{
		IntPtr_t L_13 = { TurnBasedMatch_U3CToStringU3Em__0_m1419_MethodInfo_var };
		Func_2_t352 * L_14 = (Func_2_t352 *)il2cpp_codegen_object_new (Func_2_t352_il2cpp_TypeInfo_var);
		Func_2__ctor_m3703(L_14, NULL, L_13, /*hidden argument*/Func_2__ctor_m3703_MethodInfo_var);
		((TurnBasedMatch_t353_StaticFields*)TurnBasedMatch_t353_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheB_11 = L_14;
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
		G_B2_4 = G_B1_4;
		G_B2_5 = G_B1_5;
	}

IL_005a:
	{
		Func_2_t352 * L_15 = ((TurnBasedMatch_t353_StaticFields*)TurnBasedMatch_t353_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheB_11;
		Object_t* L_16 = Enumerable_Select_TisParticipant_t340_TisString_t_m3695(NULL /*static, unused*/, G_B2_0, L_15, /*hidden argument*/Enumerable_Select_TisParticipant_t340_TisString_t_m3695_MethodInfo_var);
		StringU5BU5D_t169* L_17 = Enumerable_ToArray_TisString_t_m3697(NULL /*static, unused*/, L_16, /*hidden argument*/Enumerable_ToArray_TisString_t_m3697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Join_m3704(NULL /*static, unused*/, G_B2_1, L_17, /*hidden argument*/NULL);
		NullCheck(G_B2_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B2_3, G_B2_2);
		ArrayElementTypeCheck (G_B2_3, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B2_3, G_B2_2)) = (Object_t *)L_18;
		ObjectU5BU5D_t208* L_19 = G_B2_4;
		String_t* L_20 = (__this->___mPendingParticipantId_6);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 5);
		ArrayElementTypeCheck (L_19, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 5)) = (Object_t *)L_20;
		ObjectU5BU5D_t208* L_21 = L_19;
		int32_t L_22 = (__this->___mTurnStatus_7);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(MatchTurnStatus_t349_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 6);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 6)) = (Object_t *)L_24;
		ObjectU5BU5D_t208* L_25 = L_21;
		int32_t L_26 = (__this->___mMatchStatus_8);
		int32_t L_27 = L_26;
		Object_t * L_28 = Box(MatchStatus_t348_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 7);
		ArrayElementTypeCheck (L_25, L_28);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 7)) = (Object_t *)L_28;
		ObjectU5BU5D_t208* L_29 = L_25;
		uint32_t L_30 = (__this->___mVariant_9);
		uint32_t L_31 = L_30;
		Object_t * L_32 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 8);
		ArrayElementTypeCheck (L_29, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, 8)) = (Object_t *)L_32;
		ObjectU5BU5D_t208* L_33 = L_29;
		uint32_t L_34 = (__this->___mVersion_10);
		uint32_t L_35 = L_34;
		Object_t * L_36 = Box(UInt32_t235_il2cpp_TypeInfo_var, &L_35);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)9));
		ArrayElementTypeCheck (L_33, L_36);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, ((int32_t)9))) = (Object_t *)L_36;
		String_t* L_37 = String_Format_m3689(NULL /*static, unused*/, G_B2_5, L_33, /*hidden argument*/NULL);
		return L_37;
	}
}
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::<ToString>m__0(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* TurnBasedMatch_U3CToStringU3Em__0_m1419 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method)
{
	{
		Participant_t340 * L_0 = ___p;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String GooglePlayGames.BasicApi.Multiplayer.Participant::ToString() */, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Advertisin.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_AdvertisinMethodDeclarations.h"

// GooglePlayGames.OurUtils.Misc
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_MiscMethodDeclarations.h"
struct Misc_t391;
struct String_t;
// GooglePlayGames.OurUtils.Misc
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Misc.h"
struct Misc_t391;
struct Object_t;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(!!0)
extern "C" Object_t * Misc_CheckNotNull_TisObject_t_m3706_gshared (Object_t * __this /* static, unused */, Object_t * p0, MethodInfo* method);
#define Misc_CheckNotNull_TisObject_t_m3706(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(!!0)
#define Misc_CheckNotNull_TisString_t_m3705(__this /* static, unused */, p0, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.BasicApi.Nearby.AdvertisingResult::.ctor(GooglePlayGames.BasicApi.ResponseStatus,System.String)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void AdvertisingResult__ctor_m1420 (AdvertisingResult_t354 * __this, int32_t ___status, String_t* ___localEndpointName, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___status;
		__this->___mStatus_0 = L_0;
		String_t* L_1 = ___localEndpointName;
		String_t* L_2 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mLocalEndpointName_1 = L_2;
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Succeeded()
extern "C" bool AdvertisingResult_get_Succeeded_m1421 (AdvertisingResult_t354 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStatus_0);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Status()
extern "C" int32_t AdvertisingResult_get_Status_m1422 (AdvertisingResult_t354 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStatus_0);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_LocalEndpointName()
extern "C" String_t* AdvertisingResult_get_LocalEndpointName_m1423 (AdvertisingResult_t354 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mLocalEndpointName_1);
		return L_0;
	}
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.AdvertisingResult
void AdvertisingResult_t354_marshal(const AdvertisingResult_t354& unmarshaled, AdvertisingResult_t354_marshaled& marshaled)
{
	marshaled.___mStatus_0 = unmarshaled.___mStatus_0;
	marshaled.___mLocalEndpointName_1 = il2cpp_codegen_marshal_string(unmarshaled.___mLocalEndpointName_1);
}
void AdvertisingResult_t354_marshal_back(const AdvertisingResult_t354_marshaled& marshaled, AdvertisingResult_t354& unmarshaled)
{
	unmarshaled.___mStatus_0 = marshaled.___mStatus_0;
	unmarshaled.___mLocalEndpointName_1 = il2cpp_codegen_marshal_string_result(marshaled.___mLocalEndpointName_1);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.AdvertisingResult
void AdvertisingResult_t354_marshal_cleanup(AdvertisingResult_t354_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___mLocalEndpointName_1);
	marshaled.___mLocalEndpointName_1 = NULL;
}
// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_ConnectionMethodDeclarations.h"

// GooglePlayGames.BasicApi.Nearby.EndpointDetails
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_EndpointDe.h"
// GooglePlayGames.BasicApi.Nearby.EndpointDetails
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_EndpointDeMethodDeclarations.h"
struct Misc_t391;
struct ByteU5BU5D_t350;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Byte[]>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Byte[]>(!!0)
#define Misc_CheckNotNull_TisByteU5BU5D_t350_m3707(__this /* static, unused */, p0, method) (( ByteU5BU5D_t350* (*) (Object_t * /* static, unused */, ByteU5BU5D_t350*, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionRequest::.ctor(System.String,System.String,System.String,System.String,System.Byte[])
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var;
extern "C" void ConnectionRequest__ctor_m1424 (ConnectionRequest_t355 * __this, String_t* ___remoteEndpointId, String_t* ___remoteDeviceId, String_t* ___remoteEndpointName, String_t* ___serviceId, ByteU5BU5D_t350* ___payload, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483826);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral348, /*hidden argument*/NULL);
		String_t* L_0 = ___remoteEndpointId;
		String_t* L_1 = ___remoteDeviceId;
		String_t* L_2 = ___remoteEndpointName;
		String_t* L_3 = ___serviceId;
		EndpointDetails_t356  L_4 = {0};
		EndpointDetails__ctor_m1457(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->___mRemoteEndpoint_0 = L_4;
		ByteU5BU5D_t350* L_5 = ___payload;
		ByteU5BU5D_t350* L_6 = Misc_CheckNotNull_TisByteU5BU5D_t350_m3707(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var);
		__this->___mPayload_1 = L_6;
		return;
	}
}
// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_RemoteEndpoint()
extern "C" EndpointDetails_t356  ConnectionRequest_get_RemoteEndpoint_m1425 (ConnectionRequest_t355 * __this, MethodInfo* method)
{
	{
		EndpointDetails_t356  L_0 = (__this->___mRemoteEndpoint_0);
		return L_0;
	}
}
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_Payload()
extern "C" ByteU5BU5D_t350* ConnectionRequest_get_Payload_m1426 (ConnectionRequest_t355 * __this, MethodInfo* method)
{
	{
		ByteU5BU5D_t350* L_0 = (__this->___mPayload_1);
		return L_0;
	}
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionRequest
void ConnectionRequest_t355_marshal(const ConnectionRequest_t355& unmarshaled, ConnectionRequest_t355_marshaled& marshaled)
{
	EndpointDetails_t356_marshal(unmarshaled.___mRemoteEndpoint_0, marshaled.___mRemoteEndpoint_0);
	marshaled.___mPayload_1 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___mPayload_1);
}
extern TypeInfo* Byte_t237_il2cpp_TypeInfo_var;
void ConnectionRequest_t355_marshal_back(const ConnectionRequest_t355_marshaled& marshaled, ConnectionRequest_t355& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	EndpointDetails_t356_marshal_back(marshaled.___mRemoteEndpoint_0, unmarshaled.___mRemoteEndpoint_0);
	unmarshaled.___mPayload_1 = (ByteU5BU5D_t350*)il2cpp_codegen_marshal_array_result(Byte_t237_il2cpp_TypeInfo_var, marshaled.___mPayload_1, 1);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionRequest
void ConnectionRequest_t355_marshal_cleanup(ConnectionRequest_t355_marshaled& marshaled)
{
	EndpointDetails_t356_marshal_cleanup(marshaled.___mRemoteEndpoint_0);
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_0MethodDeclarations.h"



// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_1MethodDeclarations.h"



// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.ctor(System.Int64,System.String,GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status,System.Byte[])
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var;
extern "C" void ConnectionResponse__ctor_m1427 (ConnectionResponse_t358 * __this, int64_t ___localClientId, String_t* ___remoteEndpointId, int32_t ___code, ByteU5BU5D_t350* ___payload, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483826);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___localClientId;
		__this->___mLocalClientId_1 = L_0;
		String_t* L_1 = ___remoteEndpointId;
		String_t* L_2 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mRemoteEndpointId_2 = L_2;
		int32_t L_3 = ___code;
		__this->___mResponseStatus_3 = L_3;
		ByteU5BU5D_t350* L_4 = ___payload;
		ByteU5BU5D_t350* L_5 = Misc_CheckNotNull_TisByteU5BU5D_t350_m3707(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisByteU5BU5D_t350_m3707_MethodInfo_var);
		__this->___mPayload_4 = L_5;
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.cctor()
extern TypeInfo* ByteU5BU5D_t350_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectionResponse_t358_il2cpp_TypeInfo_var;
extern "C" void ConnectionResponse__cctor_m1428 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		ConnectionResponse_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ConnectionResponse_t358_StaticFields*)ConnectionResponse_t358_il2cpp_TypeInfo_var->static_fields)->___EmptyPayload_0 = ((ByteU5BU5D_t350*)SZArrayNew(ByteU5BU5D_t350_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_LocalClientId()
extern "C" int64_t ConnectionResponse_get_LocalClientId_m1429 (ConnectionResponse_t358 * __this, MethodInfo* method)
{
	{
		int64_t L_0 = (__this->___mLocalClientId_1);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_RemoteEndpointId()
extern "C" String_t* ConnectionResponse_get_RemoteEndpointId_m1430 (ConnectionResponse_t358 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mRemoteEndpointId_2);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_ResponseStatus()
extern "C" int32_t ConnectionResponse_get_ResponseStatus_m1431 (ConnectionResponse_t358 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mResponseStatus_3);
		return L_0;
	}
}
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_Payload()
extern "C" ByteU5BU5D_t350* ConnectionResponse_get_Payload_m1432 (ConnectionResponse_t358 * __this, MethodInfo* method)
{
	{
		ByteU5BU5D_t350* L_0 = (__this->___mPayload_4);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Rejected(System.Int64,System.String)
extern TypeInfo* ConnectionResponse_t358_il2cpp_TypeInfo_var;
extern "C" ConnectionResponse_t358  ConnectionResponse_Rejected_m1433 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConnectionResponse_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___localClientId;
		String_t* L_1 = ___remoteEndpointId;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_t358_il2cpp_TypeInfo_var);
		ByteU5BU5D_t350* L_2 = ((ConnectionResponse_t358_StaticFields*)ConnectionResponse_t358_il2cpp_TypeInfo_var->static_fields)->___EmptyPayload_0;
		ConnectionResponse_t358  L_3 = {0};
		ConnectionResponse__ctor_m1427(&L_3, L_0, L_1, 1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::NetworkNotConnected(System.Int64,System.String)
extern TypeInfo* ConnectionResponse_t358_il2cpp_TypeInfo_var;
extern "C" ConnectionResponse_t358  ConnectionResponse_NetworkNotConnected_m1434 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConnectionResponse_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___localClientId;
		String_t* L_1 = ___remoteEndpointId;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_t358_il2cpp_TypeInfo_var);
		ByteU5BU5D_t350* L_2 = ((ConnectionResponse_t358_StaticFields*)ConnectionResponse_t358_il2cpp_TypeInfo_var->static_fields)->___EmptyPayload_0;
		ConnectionResponse_t358  L_3 = {0};
		ConnectionResponse__ctor_m1427(&L_3, L_0, L_1, 3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::InternalError(System.Int64,System.String)
extern TypeInfo* ConnectionResponse_t358_il2cpp_TypeInfo_var;
extern "C" ConnectionResponse_t358  ConnectionResponse_InternalError_m1435 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConnectionResponse_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___localClientId;
		String_t* L_1 = ___remoteEndpointId;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_t358_il2cpp_TypeInfo_var);
		ByteU5BU5D_t350* L_2 = ((ConnectionResponse_t358_StaticFields*)ConnectionResponse_t358_il2cpp_TypeInfo_var->static_fields)->___EmptyPayload_0;
		ConnectionResponse_t358  L_3 = {0};
		ConnectionResponse__ctor_m1427(&L_3, L_0, L_1, 2, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::EndpointNotConnected(System.Int64,System.String)
extern TypeInfo* ConnectionResponse_t358_il2cpp_TypeInfo_var;
extern "C" ConnectionResponse_t358  ConnectionResponse_EndpointNotConnected_m1436 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConnectionResponse_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___localClientId;
		String_t* L_1 = ___remoteEndpointId;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_t358_il2cpp_TypeInfo_var);
		ByteU5BU5D_t350* L_2 = ((ConnectionResponse_t358_StaticFields*)ConnectionResponse_t358_il2cpp_TypeInfo_var->static_fields)->___EmptyPayload_0;
		ConnectionResponse_t358  L_3 = {0};
		ConnectionResponse__ctor_m1427(&L_3, L_0, L_1, 4, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Accepted(System.Int64,System.String,System.Byte[])
extern "C" ConnectionResponse_t358  ConnectionResponse_Accepted_m1437 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, ByteU5BU5D_t350* ___payload, MethodInfo* method)
{
	{
		int64_t L_0 = ___localClientId;
		String_t* L_1 = ___remoteEndpointId;
		ByteU5BU5D_t350* L_2 = ___payload;
		ConnectionResponse_t358  L_3 = {0};
		ConnectionResponse__ctor_m1427(&L_3, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::AlreadyConnected(System.Int64,System.String)
extern TypeInfo* ConnectionResponse_t358_il2cpp_TypeInfo_var;
extern "C" ConnectionResponse_t358  ConnectionResponse_AlreadyConnected_m1438 (Object_t * __this /* static, unused */, int64_t ___localClientId, String_t* ___remoteEndpointId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConnectionResponse_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___localClientId;
		String_t* L_1 = ___remoteEndpointId;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_t358_il2cpp_TypeInfo_var);
		ByteU5BU5D_t350* L_2 = ((ConnectionResponse_t358_StaticFields*)ConnectionResponse_t358_il2cpp_TypeInfo_var->static_fields)->___EmptyPayload_0;
		ConnectionResponse_t358  L_3 = {0};
		ConnectionResponse__ctor_m1427(&L_3, L_0, L_1, 5, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionResponse
void ConnectionResponse_t358_marshal(const ConnectionResponse_t358& unmarshaled, ConnectionResponse_t358_marshaled& marshaled)
{
	marshaled.___mLocalClientId_1 = unmarshaled.___mLocalClientId_1;
	marshaled.___mRemoteEndpointId_2 = il2cpp_codegen_marshal_string(unmarshaled.___mRemoteEndpointId_2);
	marshaled.___mResponseStatus_3 = unmarshaled.___mResponseStatus_3;
	marshaled.___mPayload_4 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___mPayload_4);
}
extern TypeInfo* Byte_t237_il2cpp_TypeInfo_var;
void ConnectionResponse_t358_marshal_back(const ConnectionResponse_t358_marshaled& marshaled, ConnectionResponse_t358& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.___mLocalClientId_1 = marshaled.___mLocalClientId_1;
	unmarshaled.___mRemoteEndpointId_2 = il2cpp_codegen_marshal_string_result(marshaled.___mRemoteEndpointId_2);
	unmarshaled.___mResponseStatus_3 = marshaled.___mResponseStatus_3;
	unmarshaled.___mPayload_4 = (ByteU5BU5D_t350*)il2cpp_codegen_marshal_array_result(Byte_t237_il2cpp_TypeInfo_var, marshaled.___mPayload_4, 1);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionResponse
void ConnectionResponse_t358_marshal_cleanup(ConnectionResponse_t358_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___mRemoteEndpointId_2);
	marshaled.___mRemoteEndpointId_2 = NULL;
}
// GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_DummyNearb.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_DummyNearbMethodDeclarations.h"

// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"
// System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>
#include "mscorlib_System_Action_1_gen_7.h"
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>
#include "mscorlib_System_Action_1_gen_8.h"
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>
#include "mscorlib_System_Action_1_gen_9.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>
#include "mscorlib_System_Action_1_gen_7MethodDeclarations.h"
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"


// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::.ctor()
extern "C" void DummyNearbyConnectionClient__ctor_m1439 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::MaxUnreliableMessagePayloadLength()
extern "C" int32_t DummyNearbyConnectionClient_MaxUnreliableMessagePayloadLength_m1440 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		return ((int32_t)1168);
	}
}
// System.Int32 GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::MaxReliableMessagePayloadLength()
extern "C" int32_t DummyNearbyConnectionClient_MaxReliableMessagePayloadLength_m1441 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		return ((int32_t)4096);
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::SendReliable(System.Collections.Generic.List`1<System.String>,System.Byte[])
extern "C" void DummyNearbyConnectionClient_SendReliable_m1442 (DummyNearbyConnectionClient_t359 * __this, List_1_t43 * ___recipientEndpointIds, ByteU5BU5D_t350* ___payload, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral349, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::SendUnreliable(System.Collections.Generic.List`1<System.String>,System.Byte[])
extern "C" void DummyNearbyConnectionClient_SendUnreliable_m1443 (DummyNearbyConnectionClient_t359 * __this, List_1_t43 * ___recipientEndpointIds, ByteU5BU5D_t350* ___payload, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral350, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StartAdvertising(System.String,System.Collections.Generic.List`1<System.String>,System.Nullable`1<System.TimeSpan>,System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>,System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DummyNearbyConnectionClient_StartAdvertising_m1444 (DummyNearbyConnectionClient_t359 * __this, String_t* ___name, List_1_t43 * ___appIdentifiers, Nullable_1_t377  ___advertisingDuration, Action_1_t845 * ___resultCallback, Action_1_t846 * ___connectionRequestCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	AdvertisingResult_t354  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		AdvertisingResult__ctor_m1420((&V_0), (-1), L_0, /*hidden argument*/NULL);
		Action_1_t845 * L_1 = ___resultCallback;
		AdvertisingResult_t354  L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< AdvertisingResult_t354  >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::Invoke(!0) */, L_1, L_2);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StopAdvertising()
extern "C" void DummyNearbyConnectionClient_StopAdvertising_m1445 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral351, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::SendConnectionRequest(System.String,System.String,System.Byte[],System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>,GooglePlayGames.BasicApi.Nearby.IMessageListener)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectionResponse_t358_il2cpp_TypeInfo_var;
extern "C" void DummyNearbyConnectionClient_SendConnectionRequest_m1446 (DummyNearbyConnectionClient_t359 * __this, String_t* ___name, String_t* ___remoteEndpointId, ByteU5BU5D_t350* ___payload, Action_1_t847 * ___responseCallback, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ConnectionResponse_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	ConnectionResponse_t358  V_0 = {0};
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral352, /*hidden argument*/NULL);
		Action_1_t847 * L_0 = ___responseCallback;
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_t358_il2cpp_TypeInfo_var);
		ConnectionResponse_t358  L_2 = ConnectionResponse_Rejected_m1433(NULL /*static, unused*/, (((int64_t)0)), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Action_1_t847 * L_3 = ___responseCallback;
		ConnectionResponse_t358  L_4 = V_0;
		NullCheck(L_3);
		VirtActionInvoker1< ConnectionResponse_t358  >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::Invoke(!0) */, L_3, L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::AcceptConnectionRequest(System.String,System.Byte[],GooglePlayGames.BasicApi.Nearby.IMessageListener)
extern "C" void DummyNearbyConnectionClient_AcceptConnectionRequest_m1447 (DummyNearbyConnectionClient_t359 * __this, String_t* ___remoteEndpointId, ByteU5BU5D_t350* ___payload, Object_t * ___listener, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral353, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StartDiscovery(System.String,System.Nullable`1<System.TimeSpan>,GooglePlayGames.BasicApi.Nearby.IDiscoveryListener)
extern "C" void DummyNearbyConnectionClient_StartDiscovery_m1448 (DummyNearbyConnectionClient_t359 * __this, String_t* ___serviceId, Nullable_1_t377  ___advertisingTimeout, Object_t * ___listener, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral354, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StopDiscovery(System.String)
extern "C" void DummyNearbyConnectionClient_StopDiscovery_m1449 (DummyNearbyConnectionClient_t359 * __this, String_t* ___serviceId, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral355, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::RejectConnectionRequest(System.String)
extern "C" void DummyNearbyConnectionClient_RejectConnectionRequest_m1450 (DummyNearbyConnectionClient_t359 * __this, String_t* ___requestingEndpointId, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral356, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::DisconnectFromEndpoint(System.String)
extern "C" void DummyNearbyConnectionClient_DisconnectFromEndpoint_m1451 (DummyNearbyConnectionClient_t359 * __this, String_t* ___remoteEndpointId, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral357, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::StopAllConnections()
extern "C" void DummyNearbyConnectionClient_StopAllConnections_m1452 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		Debug_LogError_m910(NULL /*static, unused*/, (String_t*) &_stringLiteral358, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::LocalEndpointId()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* DummyNearbyConnectionClient_LocalEndpointId_m1453 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::LocalDeviceId()
extern "C" String_t* DummyNearbyConnectionClient_LocalDeviceId_m1454 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		return (String_t*) &_stringLiteral359;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::GetAppBundleId()
extern "C" String_t* DummyNearbyConnectionClient_GetAppBundleId_m1455 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		return (String_t*) &_stringLiteral360;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient::GetServiceId()
extern "C" String_t* DummyNearbyConnectionClient_GetServiceId_m1456 (DummyNearbyConnectionClient_t359 * __this, MethodInfo* method)
{
	{
		return (String_t*) &_stringLiteral361;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.BasicApi.Nearby.EndpointDetails::.ctor(System.String,System.String,System.String,System.String)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void EndpointDetails__ctor_m1457 (EndpointDetails_t356 * __this, String_t* ___endpointId, String_t* ___deviceId, String_t* ___name, String_t* ___serviceId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___endpointId;
		String_t* L_1 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mEndpointId_0 = L_1;
		String_t* L_2 = ___deviceId;
		String_t* L_3 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mDeviceId_1 = L_3;
		String_t* L_4 = ___name;
		String_t* L_5 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mName_2 = L_5;
		String_t* L_6 = ___serviceId;
		String_t* L_7 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_6, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mServiceId_3 = L_7;
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_EndpointId()
extern "C" String_t* EndpointDetails_get_EndpointId_m1458 (EndpointDetails_t356 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mEndpointId_0);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_DeviceId()
extern "C" String_t* EndpointDetails_get_DeviceId_m1459 (EndpointDetails_t356 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mDeviceId_1);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_Name()
extern "C" String_t* EndpointDetails_get_Name_m1460 (EndpointDetails_t356 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mName_2);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_ServiceId()
extern "C" String_t* EndpointDetails_get_ServiceId_m1461 (EndpointDetails_t356 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mServiceId_3);
		return L_0;
	}
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.EndpointDetails
void EndpointDetails_t356_marshal(const EndpointDetails_t356& unmarshaled, EndpointDetails_t356_marshaled& marshaled)
{
	marshaled.___mEndpointId_0 = il2cpp_codegen_marshal_string(unmarshaled.___mEndpointId_0);
	marshaled.___mDeviceId_1 = il2cpp_codegen_marshal_string(unmarshaled.___mDeviceId_1);
	marshaled.___mName_2 = il2cpp_codegen_marshal_string(unmarshaled.___mName_2);
	marshaled.___mServiceId_3 = il2cpp_codegen_marshal_string(unmarshaled.___mServiceId_3);
}
void EndpointDetails_t356_marshal_back(const EndpointDetails_t356_marshaled& marshaled, EndpointDetails_t356& unmarshaled)
{
	unmarshaled.___mEndpointId_0 = il2cpp_codegen_marshal_string_result(marshaled.___mEndpointId_0);
	unmarshaled.___mDeviceId_1 = il2cpp_codegen_marshal_string_result(marshaled.___mDeviceId_1);
	unmarshaled.___mName_2 = il2cpp_codegen_marshal_string_result(marshaled.___mName_2);
	unmarshaled.___mServiceId_3 = il2cpp_codegen_marshal_string_result(marshaled.___mServiceId_3);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.EndpointDetails
void EndpointDetails_t356_marshal_cleanup(EndpointDetails_t356_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___mEndpointId_0);
	marshaled.___mEndpointId_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___mDeviceId_1);
	marshaled.___mDeviceId_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___mName_2);
	marshaled.___mName_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___mServiceId_3);
	marshaled.___mServiceId_3 = NULL;
}
// GooglePlayGames.BasicApi.Nearby.InitializationStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Initializa.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Nearby.InitializationStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_InitializaMethodDeclarations.h"



// GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_NearbyConn.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_NearbyConnMethodDeclarations.h"

// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
#include "mscorlib_System_Action_1_gen_10.h"
struct Misc_t391;
struct Action_1_t361;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>>(!!0)
#define Misc_CheckNotNull_TisAction_1_t361_m3708(__this /* static, unused */, p0, method) (( Action_1_t361 * (*) (Object_t * /* static, unused */, Action_1_t361 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::.ctor(System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>,System.Int64)
extern MethodInfo* Misc_CheckNotNull_TisAction_1_t361_m3708_MethodInfo_var;
extern "C" void NearbyConnectionConfiguration__ctor_m1462 (NearbyConnectionConfiguration_t362 * __this, Action_1_t361 * ___callback, int64_t ___localClientId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisAction_1_t361_m3708_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483827);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t361 * L_0 = ___callback;
		Action_1_t361 * L_1 = Misc_CheckNotNull_TisAction_1_t361_m3708(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t361_m3708_MethodInfo_var);
		__this->___mInitializationCallback_2 = L_1;
		int64_t L_2 = ___localClientId;
		__this->___mLocalClientId_3 = L_2;
		return;
	}
}
// System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_LocalClientId()
extern "C" int64_t NearbyConnectionConfiguration_get_LocalClientId_m1463 (NearbyConnectionConfiguration_t362 * __this, MethodInfo* method)
{
	{
		int64_t L_0 = (__this->___mLocalClientId_3);
		return L_0;
	}
}
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_InitializationCallback()
extern "C" Action_1_t361 * NearbyConnectionConfiguration_get_InitializationCallback_m1464 (NearbyConnectionConfiguration_t362 * __this, MethodInfo* method)
{
	{
		Action_1_t361 * L_0 = (__this->___mInitializationCallback_2);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCoMethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1.h"
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceiveMethodDeclarations.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1MethodDeclarations.h"
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0MethodDeclarations.h"
struct Misc_t391;
struct InvitationReceivedDelegate_t363;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.InvitationReceivedDelegate>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.InvitationReceivedDelegate>(!!0)
#define Misc_CheckNotNull_TisInvitationReceivedDelegate_t363_m3709(__this /* static, unused */, p0, method) (( InvitationReceivedDelegate_t363 * (*) (Object_t * /* static, unused */, InvitationReceivedDelegate_t363 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct MatchDelegate_t364;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Multiplayer.MatchDelegate>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Multiplayer.MatchDelegate>(!!0)
#define Misc_CheckNotNull_TisMatchDelegate_t364_m3710(__this /* static, unused */, p0, method) (( MatchDelegate_t364 * (*) (Object_t * /* static, unused */, MatchDelegate_t364 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::.ctor()
extern TypeInfo* Builder_t365_il2cpp_TypeInfo_var;
extern TypeInfo* InvitationReceivedDelegate_t363_il2cpp_TypeInfo_var;
extern TypeInfo* MatchDelegate_t364_il2cpp_TypeInfo_var;
extern MethodInfo* Builder_U3CmInvitationDelegateU3Em__1_m1475_MethodInfo_var;
extern MethodInfo* Builder_U3CmMatchDelegateU3Em__2_m1476_MethodInfo_var;
extern "C" void Builder__ctor_m1465 (Builder_t365 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Builder_t365_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(617);
		InvitationReceivedDelegate_t363_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		MatchDelegate_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(619);
		Builder_U3CmInvitationDelegateU3Em__1_m1475_MethodInfo_var = il2cpp_codegen_method_info_from_index(180);
		Builder_U3CmMatchDelegateU3Em__2_m1476_MethodInfo_var = il2cpp_codegen_method_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	Builder_t365 * G_B2_0 = {0};
	Builder_t365 * G_B1_0 = {0};
	Builder_t365 * G_B4_0 = {0};
	Builder_t365 * G_B3_0 = {0};
	{
		InvitationReceivedDelegate_t363 * L_0 = ((Builder_t365_StaticFields*)Builder_t365_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { Builder_U3CmInvitationDelegateU3Em__1_m1475_MethodInfo_var };
		InvitationReceivedDelegate_t363 * L_2 = (InvitationReceivedDelegate_t363 *)il2cpp_codegen_object_new (InvitationReceivedDelegate_t363_il2cpp_TypeInfo_var);
		InvitationReceivedDelegate__ctor_m3672(L_2, NULL, L_1, /*hidden argument*/NULL);
		((Builder_t365_StaticFields*)Builder_t365_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		InvitationReceivedDelegate_t363 * L_3 = ((Builder_t365_StaticFields*)Builder_t365_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		NullCheck(G_B2_0);
		G_B2_0->___mInvitationDelegate_2 = L_3;
		MatchDelegate_t364 * L_4 = ((Builder_t365_StaticFields*)Builder_t365_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B3_0 = __this;
		if (L_4)
		{
			G_B4_0 = __this;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_5 = { Builder_U3CmMatchDelegateU3Em__2_m1476_MethodInfo_var };
		MatchDelegate_t364 * L_6 = (MatchDelegate_t364 *)il2cpp_codegen_object_new (MatchDelegate_t364_il2cpp_TypeInfo_var);
		MatchDelegate__ctor_m3676(L_6, NULL, L_5, /*hidden argument*/NULL);
		((Builder_t365_StaticFields*)Builder_t365_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_6;
		G_B4_0 = G_B3_0;
	}

IL_003c:
	{
		MatchDelegate_t364 * L_7 = ((Builder_t365_StaticFields*)Builder_t365_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		NullCheck(G_B4_0);
		G_B4_0->___mMatchDelegate_3 = L_7;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::EnableSavedGames()
extern "C" Builder_t365 * Builder_EnableSavedGames_m1466 (Builder_t365 * __this, MethodInfo* method)
{
	{
		__this->___mEnableSaveGames_0 = 1;
		return __this;
	}
}
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::EnableDeprecatedCloudSave()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" Builder_t365 * Builder_EnableDeprecatedCloudSave_m1467 (Builder_t365 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral362, /*hidden argument*/NULL);
		__this->___mEnableDeprecatedCloudSave_1 = 1;
		return __this;
	}
}
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::WithInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern MethodInfo* Misc_CheckNotNull_TisInvitationReceivedDelegate_t363_m3709_MethodInfo_var;
extern "C" Builder_t365 * Builder_WithInvitationDelegate_m1468 (Builder_t365 * __this, InvitationReceivedDelegate_t363 * ___invitationDelegate, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisInvitationReceivedDelegate_t363_m3709_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483830);
		s_Il2CppMethodIntialized = true;
	}
	{
		InvitationReceivedDelegate_t363 * L_0 = ___invitationDelegate;
		InvitationReceivedDelegate_t363 * L_1 = Misc_CheckNotNull_TisInvitationReceivedDelegate_t363_m3709(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisInvitationReceivedDelegate_t363_m3709_MethodInfo_var);
		__this->___mInvitationDelegate_2 = L_1;
		return __this;
	}
}
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::WithMatchDelegate(GooglePlayGames.BasicApi.Multiplayer.MatchDelegate)
extern MethodInfo* Misc_CheckNotNull_TisMatchDelegate_t364_m3710_MethodInfo_var;
extern "C" Builder_t365 * Builder_WithMatchDelegate_m1469 (Builder_t365 * __this, MatchDelegate_t364 * ___matchDelegate, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisMatchDelegate_t364_m3710_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483831);
		s_Il2CppMethodIntialized = true;
	}
	{
		MatchDelegate_t364 * L_0 = ___matchDelegate;
		MatchDelegate_t364 * L_1 = Misc_CheckNotNull_TisMatchDelegate_t364_m3710(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisMatchDelegate_t364_m3710_MethodInfo_var);
		__this->___mMatchDelegate_3 = L_1;
		return __this;
	}
}
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::HasEnableSaveGames()
extern "C" bool Builder_HasEnableSaveGames_m1470 (Builder_t365 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mEnableSaveGames_0);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::HasEnableDeprecatedCloudSave()
extern "C" bool Builder_HasEnableDeprecatedCloudSave_m1471 (Builder_t365 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mEnableDeprecatedCloudSave_1);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::GetMatchDelegate()
extern "C" MatchDelegate_t364 * Builder_GetMatchDelegate_m1472 (Builder_t365 * __this, MethodInfo* method)
{
	{
		MatchDelegate_t364 * L_0 = (__this->___mMatchDelegate_3);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::GetInvitationDelegate()
extern "C" InvitationReceivedDelegate_t363 * Builder_GetInvitationDelegate_m1473 (Builder_t365 * __this, MethodInfo* method)
{
	{
		InvitationReceivedDelegate_t363 * L_0 = (__this->___mInvitationDelegate_2);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::Build()
extern "C" PlayGamesClientConfiguration_t366  Builder_Build_m1474 (Builder_t365 * __this, MethodInfo* method)
{
	{
		PlayGamesClientConfiguration_t366  L_0 = {0};
		PlayGamesClientConfiguration__ctor_m1477(&L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<mInvitationDelegate>m__1(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C" void Builder_U3CmInvitationDelegateU3Em__1_m1475 (Object_t * __this /* static, unused */, Invitation_t341 * p0, bool p1, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<mMatchDelegate>m__2(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C" void Builder_U3CmMatchDelegateU3Em__2_m1476 (Object_t * __this /* static, unused */, TurnBasedMatch_t353 * p0, bool p1, MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder)
extern "C" void PlayGamesClientConfiguration__ctor_m1477 (PlayGamesClientConfiguration_t366 * __this, Builder_t365 * ___builder, MethodInfo* method)
{
	{
		Builder_t365 * L_0 = ___builder;
		NullCheck(L_0);
		bool L_1 = Builder_HasEnableSaveGames_m1470(L_0, /*hidden argument*/NULL);
		__this->___mEnableSavedGames_1 = L_1;
		Builder_t365 * L_2 = ___builder;
		NullCheck(L_2);
		bool L_3 = Builder_HasEnableDeprecatedCloudSave_m1471(L_2, /*hidden argument*/NULL);
		__this->___mEnableDeprecatedCloudSave_2 = L_3;
		Builder_t365 * L_4 = ___builder;
		NullCheck(L_4);
		InvitationReceivedDelegate_t363 * L_5 = Builder_GetInvitationDelegate_m1473(L_4, /*hidden argument*/NULL);
		__this->___mInvitationDelegate_3 = L_5;
		Builder_t365 * L_6 = ___builder;
		NullCheck(L_6);
		MatchDelegate_t364 * L_7 = Builder_GetMatchDelegate_m1472(L_6, /*hidden argument*/NULL);
		__this->___mMatchDelegate_4 = L_7;
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration::.cctor()
extern TypeInfo* Builder_t365_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var;
extern "C" void PlayGamesClientConfiguration__cctor_m1478 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Builder_t365_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(617);
		PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(620);
		s_Il2CppMethodIntialized = true;
	}
	{
		Builder_t365 * L_0 = (Builder_t365 *)il2cpp_codegen_object_new (Builder_t365_il2cpp_TypeInfo_var);
		Builder__ctor_m1465(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		PlayGamesClientConfiguration_t366  L_1 = Builder_Build_m1474(L_0, /*hidden argument*/NULL);
		((PlayGamesClientConfiguration_t366_StaticFields*)PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var->static_fields)->___DefaultConfiguration_0 = L_1;
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_EnableSavedGames()
extern "C" bool PlayGamesClientConfiguration_get_EnableSavedGames_m1479 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mEnableSavedGames_1);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_EnableDeprecatedCloudSave()
extern "C" bool PlayGamesClientConfiguration_get_EnableDeprecatedCloudSave_m1480 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mEnableDeprecatedCloudSave_2);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_InvitationDelegate()
extern "C" InvitationReceivedDelegate_t363 * PlayGamesClientConfiguration_get_InvitationDelegate_m1481 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method)
{
	{
		InvitationReceivedDelegate_t363 * L_0 = (__this->___mInvitationDelegate_3);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_MatchDelegate()
extern "C" MatchDelegate_t364 * PlayGamesClientConfiguration_get_MatchDelegate_m1482 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method)
{
	{
		MatchDelegate_t364 * L_0 = (__this->___mMatchDelegate_4);
		return L_0;
	}
}
// GooglePlayGames.BasicApi.Quests.QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestState.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Quests.QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestStateMethodDeclarations.h"



// GooglePlayGames.BasicApi.Quests.MilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_MilestoneS.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Quests.MilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_MilestoneSMethodDeclarations.h"



// GooglePlayGames.BasicApi.Quests.QuestFetchFlags
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestFetch.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Quests.QuestFetchFlags
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestFetchMethodDeclarations.h"



// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestAccep.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestAccepMethodDeclarations.h"



// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestClaim.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestClaimMethodDeclarations.h"



// GooglePlayGames.BasicApi.Quests.QuestUiResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestUiRes.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.Quests.QuestUiResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestUiResMethodDeclarations.h"



// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_ConflicMethodDeclarations.h"



// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGaMethodDeclarations.h"



// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SelectU.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SelectUMethodDeclarations.h"



// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_0MethodDeclarations.h"

// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_genMethodDeclarations.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1MethodDeclarations.h"


// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedDescription(System.String)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" Builder_t376  Builder_WithUpdatedDescription_m1483 (Builder_t376 * __this, String_t* ___description, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___description;
		String_t* L_1 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		__this->___mNewDescription_1 = L_1;
		__this->___mDescriptionUpdated_0 = 1;
		return (*(Builder_t376 *)__this);
	}
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPngCoverImage(System.Byte[])
extern "C" Builder_t376  Builder_WithUpdatedPngCoverImage_m1484 (Builder_t376 * __this, ByteU5BU5D_t350* ___newPngCoverImage, MethodInfo* method)
{
	{
		__this->___mCoverImageUpdated_2 = 1;
		ByteU5BU5D_t350* L_0 = ___newPngCoverImage;
		__this->___mNewPngCoverImage_3 = L_0;
		return (*(Builder_t376 *)__this);
	}
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPlayedTime(System.TimeSpan)
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern MethodInfo* Nullable_1__ctor_m3713_MethodInfo_var;
extern "C" Builder_t376  Builder_WithUpdatedPlayedTime_m1485 (Builder_t376 * __this, TimeSpan_t190  ___newPlayedTime, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		Nullable_1__ctor_m3713_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483832);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = TimeSpan_get_TotalMilliseconds_m3711((&___newPlayedTime), /*hidden argument*/NULL);
		if ((!(((double)L_0) > ((double)(1.8446744073709552E+19)))))
		{
			goto IL_0020;
		}
	}
	{
		InvalidOperationException_t905 * L_1 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_1, (String_t*) &_stringLiteral363, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0020:
	{
		TimeSpan_t190  L_2 = ___newPlayedTime;
		Nullable_1_t377  L_3 = {0};
		Nullable_1__ctor_m3713(&L_3, L_2, /*hidden argument*/Nullable_1__ctor_m3713_MethodInfo_var);
		__this->___mNewPlayedTime_4 = L_3;
		return (*(Builder_t376 *)__this);
	}
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::Build()
extern "C" SavedGameMetadataUpdate_t378  Builder_Build_m1486 (Builder_t376 * __this, MethodInfo* method)
{
	{
		SavedGameMetadataUpdate_t378  L_0 = {0};
		SavedGameMetadataUpdate__ctor_m1487(&L_0, (*(Builder_t376 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::.ctor(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder)
extern "C" void SavedGameMetadataUpdate__ctor_m1487 (SavedGameMetadataUpdate_t378 * __this, Builder_t376  ___builder, MethodInfo* method)
{
	{
		bool L_0 = ((&___builder)->___mDescriptionUpdated_0);
		__this->___mDescriptionUpdated_0 = L_0;
		String_t* L_1 = ((&___builder)->___mNewDescription_1);
		__this->___mNewDescription_1 = L_1;
		bool L_2 = ((&___builder)->___mCoverImageUpdated_2);
		__this->___mCoverImageUpdated_2 = L_2;
		ByteU5BU5D_t350* L_3 = ((&___builder)->___mNewPngCoverImage_3);
		__this->___mNewPngCoverImage_3 = L_3;
		Nullable_1_t377  L_4 = ((&___builder)->___mNewPlayedTime_4);
		__this->___mNewPlayedTime_4 = L_4;
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsDescriptionUpdated()
extern "C" bool SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1488 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mDescriptionUpdated_0);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedDescription()
extern "C" String_t* SavedGameMetadataUpdate_get_UpdatedDescription_m1489 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mNewDescription_1);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsCoverImageUpdated()
extern "C" bool SavedGameMetadataUpdate_get_IsCoverImageUpdated_m1490 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___mCoverImageUpdated_2);
		return L_0;
	}
}
// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPngCoverImage()
extern "C" ByteU5BU5D_t350* SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m1491 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method)
{
	{
		ByteU5BU5D_t350* L_0 = (__this->___mNewPngCoverImage_3);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsPlayedTimeUpdated()
extern MethodInfo* Nullable_1_get_HasValue_m3714_MethodInfo_var;
extern "C" bool SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_m1492 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Nullable_1_get_HasValue_m3714_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483833);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t377  V_0 = {0};
	{
		Nullable_1_t377  L_0 = (__this->___mNewPlayedTime_4);
		V_0 = L_0;
		bool L_1 = Nullable_1_get_HasValue_m3714((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m3714_MethodInfo_var);
		return L_1;
	}
}
// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPlayedTime()
extern "C" Nullable_1_t377  SavedGameMetadataUpdate_get_UpdatedPlayedTime_m1493 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method)
{
	{
		Nullable_1_t377  L_0 = (__this->___mNewPlayedTime_4);
		return L_0;
	}
}
// GooglePlayGames.GameInfo
#include "AssemblyU2DCSharp_GooglePlayGames_GameInfo.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.GameInfo
#include "AssemblyU2DCSharp_GooglePlayGames_GameInfoMethodDeclarations.h"



// System.Boolean GooglePlayGames.GameInfo::ApplicationIdInitialized()
extern "C" bool GameInfo_ApplicationIdInitialized_m1494 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		String_t* L_0 = GameInfo_ToEscapedToken_m1496(NULL /*static, unused*/, (String_t*) &_stringLiteral365, /*hidden argument*/NULL);
		NullCheck((String_t*) &_stringLiteral364);
		bool L_1 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, (String_t*) &_stringLiteral364, L_0);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean GooglePlayGames.GameInfo::IosClientIdInitialized()
extern "C" bool GameInfo_IosClientIdInitialized_m1495 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		String_t* L_0 = GameInfo_ToEscapedToken_m1496(NULL /*static, unused*/, (String_t*) &_stringLiteral367, /*hidden argument*/NULL);
		NullCheck((String_t*) &_stringLiteral366);
		bool L_1 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, (String_t*) &_stringLiteral366, L_0);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.String GooglePlayGames.GameInfo::ToEscapedToken(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* GameInfo_ToEscapedToken_m1496 (Object_t * __this /* static, unused */, String_t* ___token, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___token;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m3715(NULL /*static, unused*/, (String_t*) &_stringLiteral368, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.PlayGamesAchievement
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesAchievement.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.PlayGamesAchievement
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesAchievementMethodDeclarations.h"

// System.DateTime
#include "mscorlib_System_DateTime.h"
// GooglePlayGames.PlayGamesPlatform
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatform.h"
// GooglePlayGames.ReportProgress
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgress.h"
// GooglePlayGames.PlayGamesPlatform
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatformMethodDeclarations.h"
// GooglePlayGames.ReportProgress
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgressMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"


// System.Void GooglePlayGames.PlayGamesAchievement::.ctor()
extern TypeInfo* PlayGamesPlatform_t382_il2cpp_TypeInfo_var;
extern TypeInfo* ReportProgress_t380_il2cpp_TypeInfo_var;
extern "C" void PlayGamesAchievement__ctor_m1497 (PlayGamesAchievement_t381 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesPlatform_t382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		ReportProgress_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(623);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t382 * L_0 = PlayGamesPlatform_get_Instance_m1525(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayGamesPlatform_t382 * L_1 = L_0;
		IntPtr_t L_2 = { GetVirtualMethodInfo(L_1, 5) };
		ReportProgress_t380 * L_3 = (ReportProgress_t380 *)il2cpp_codegen_object_new (ReportProgress_t380_il2cpp_TypeInfo_var);
		ReportProgress__ctor_m3684(L_3, L_1, L_2, /*hidden argument*/NULL);
		PlayGamesAchievement__ctor_m1498(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::.ctor(GooglePlayGames.ReportProgress)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void PlayGamesAchievement__ctor_m1498 (PlayGamesAchievement_t381 * __this, ReportProgress_t380 * ___progressCallback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mId_1 = L_0;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		ReportProgress_t380 * L_1 = ___progressCallback;
		__this->___mProgressCallback_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::.cctor()
extern TypeInfo* PlayGamesAchievement_t381_il2cpp_TypeInfo_var;
extern "C" void PlayGamesAchievement__cctor_m1499 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesAchievement_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(624);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t48  L_0 = {0};
		DateTime__ctor_m3716(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		((PlayGamesAchievement_t381_StaticFields*)PlayGamesAchievement_t381_il2cpp_TypeInfo_var->static_fields)->___Thesentinel_3 = L_0;
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::ReportProgress(System.Action`1<System.Boolean>)
extern "C" void PlayGamesAchievement_ReportProgress_m1500 (PlayGamesAchievement_t381 * __this, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		ReportProgress_t380 * L_0 = (__this->___mProgressCallback_0);
		String_t* L_1 = (__this->___mId_1);
		double L_2 = (__this->___mPercentComplete_2);
		Action_1_t98 * L_3 = ___callback;
		NullCheck(L_0);
		VirtActionInvoker3< String_t*, double, Action_1_t98 * >::Invoke(10 /* System.Void GooglePlayGames.ReportProgress::Invoke(System.String,System.Double,System.Action`1<System.Boolean>) */, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesAchievement::get_id()
extern "C" String_t* PlayGamesAchievement_get_id_m1501 (PlayGamesAchievement_t381 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mId_1);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::set_id(System.String)
extern "C" void PlayGamesAchievement_set_id_m1502 (PlayGamesAchievement_t381 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mId_1 = L_0;
		return;
	}
}
// System.Double GooglePlayGames.PlayGamesAchievement::get_percentCompleted()
extern "C" double PlayGamesAchievement_get_percentCompleted_m1503 (PlayGamesAchievement_t381 * __this, MethodInfo* method)
{
	{
		double L_0 = (__this->___mPercentComplete_2);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::set_percentCompleted(System.Double)
extern "C" void PlayGamesAchievement_set_percentCompleted_m1504 (PlayGamesAchievement_t381 * __this, double ___value, MethodInfo* method)
{
	{
		double L_0 = ___value;
		__this->___mPercentComplete_2 = L_0;
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_completed()
extern "C" bool PlayGamesAchievement_get_completed_m1505 (PlayGamesAchievement_t381 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_hidden()
extern "C" bool PlayGamesAchievement_get_hidden_m1506 (PlayGamesAchievement_t381 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.DateTime GooglePlayGames.PlayGamesAchievement::get_lastReportedDate()
extern TypeInfo* PlayGamesAchievement_t381_il2cpp_TypeInfo_var;
extern "C" DateTime_t48  PlayGamesAchievement_get_lastReportedDate_m1507 (PlayGamesAchievement_t381 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesAchievement_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(624);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesAchievement_t381_il2cpp_TypeInfo_var);
		DateTime_t48  L_0 = ((PlayGamesAchievement_t381_StaticFields*)PlayGamesAchievement_t381_il2cpp_TypeInfo_var->static_fields)->___Thesentinel_3;
		return L_0;
	}
}
// GooglePlayGames.PlayGamesLocalUser
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUser.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.PlayGamesLocalUser
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUserMethodDeclarations.h"

#include "UnityEngine_ArrayTypes.h"
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// GooglePlayGames.PlayGamesUserProfile
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserProfileMethodDeclarations.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"


// System.Void GooglePlayGames.PlayGamesLocalUser::.ctor(GooglePlayGames.PlayGamesPlatform)
extern "C" void PlayGamesLocalUser__ctor_m1508 (PlayGamesLocalUser_t385 * __this, PlayGamesPlatform_t382 * ___plaf, MethodInfo* method)
{
	{
		PlayGamesUserProfile__ctor_m1579(__this, /*hidden argument*/NULL);
		PlayGamesPlatform_t382 * L_0 = ___plaf;
		__this->___mPlatform_0 = L_0;
		__this->___mAvatarUrl_1 = (WWW_t383 *)NULL;
		__this->___mImage_2 = (Texture2D_t384 *)NULL;
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::Authenticate(System.Action`1<System.Boolean>)
extern "C" void PlayGamesLocalUser_Authenticate_m1509 (PlayGamesLocalUser_t385 * __this, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		PlayGamesPlatform_t382 * L_0 = (__this->___mPlatform_0);
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_0);
		PlayGamesPlatform_Authenticate_m1537(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C" void PlayGamesLocalUser_Authenticate_m1510 (PlayGamesLocalUser_t385 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method)
{
	{
		PlayGamesPlatform_t382 * L_0 = (__this->___mPlatform_0);
		Action_1_t98 * L_1 = ___callback;
		bool L_2 = ___silent;
		NullCheck(L_0);
		PlayGamesPlatform_Authenticate_m1538(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::LoadFriends(System.Action`1<System.Boolean>)
extern "C" void PlayGamesLocalUser_LoadFriends_m1511 (PlayGamesLocalUser_t385 * __this, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.IUserProfile[] GooglePlayGames.PlayGamesLocalUser::get_friends()
extern TypeInfo* IUserProfileU5BU5D_t850_il2cpp_TypeInfo_var;
extern "C" IUserProfileU5BU5D_t850* PlayGamesLocalUser_get_friends_m1512 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IUserProfileU5BU5D_t850_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(625);
		s_Il2CppMethodIntialized = true;
	}
	{
		return ((IUserProfileU5BU5D_t850*)SZArrayNew(IUserProfileU5BU5D_t850_il2cpp_TypeInfo_var, 0));
	}
}
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_authenticated()
extern "C" bool PlayGamesLocalUser_get_authenticated_m1513 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	{
		PlayGamesPlatform_t382 * L_0 = (__this->___mPlatform_0);
		NullCheck(L_0);
		bool L_1 = PlayGamesPlatform_IsAuthenticated_m1540(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_underage()
extern "C" bool PlayGamesLocalUser_get_underage_m1514 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
// System.String GooglePlayGames.PlayGamesLocalUser::get_userName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesLocalUser_get_userName_m1515 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean GooglePlayGames.PlayGamesLocalUser::get_authenticated() */, __this);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		PlayGamesPlatform_t382 * L_1 = (__this->___mPlatform_0);
		NullCheck(L_1);
		String_t* L_2 = PlayGamesPlatform_GetUserDisplayName_m1545(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_3;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.String GooglePlayGames.PlayGamesLocalUser::get_id()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesLocalUser_get_id_m1516 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean GooglePlayGames.PlayGamesLocalUser::get_authenticated() */, __this);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		PlayGamesPlatform_t382 * L_1 = (__this->___mPlatform_0);
		NullCheck(L_1);
		String_t* L_2 = PlayGamesPlatform_GetUserId_m1543(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_3;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_isFriend()
extern "C" bool PlayGamesLocalUser_get_isFriend_m1517 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
// UnityEngine.SocialPlatforms.UserState GooglePlayGames.PlayGamesLocalUser::get_state()
extern "C" int32_t PlayGamesLocalUser_get_state_m1518 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// UnityEngine.Texture2D GooglePlayGames.PlayGamesLocalUser::LoadImage()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t383_il2cpp_TypeInfo_var;
extern "C" Texture2D_t384 * PlayGamesLocalUser_LoadImage_m1519 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		WWW_t383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(627);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		PlayGamesPlatform_t382 * L_0 = (__this->___mPlatform_0);
		NullCheck(L_0);
		String_t* L_1 = PlayGamesPlatform_GetUserImageUrl_m1546(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m899(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_008b;
		}
	}
	{
		WWW_t383 * L_4 = (__this->___mAvatarUrl_1);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		WWW_t383 * L_5 = (__this->___mAvatarUrl_1);
		NullCheck(L_5);
		String_t* L_6 = WWW_get_url_m3717(L_5, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Inequality_m831(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}

IL_0038:
	{
		String_t* L_9 = V_0;
		WWW_t383 * L_10 = (WWW_t383 *)il2cpp_codegen_object_new (WWW_t383_il2cpp_TypeInfo_var);
		WWW__ctor_m3718(L_10, L_9, /*hidden argument*/NULL);
		__this->___mAvatarUrl_1 = L_10;
		__this->___mImage_2 = (Texture2D_t384 *)NULL;
	}

IL_004b:
	{
		Texture2D_t384 * L_11 = (__this->___mImage_2);
		bool L_12 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_11, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0063;
		}
	}
	{
		Texture2D_t384 * L_13 = (__this->___mImage_2);
		return L_13;
	}

IL_0063:
	{
		WWW_t383 * L_14 = (__this->___mAvatarUrl_1);
		NullCheck(L_14);
		bool L_15 = WWW_get_isDone_m3720(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008b;
		}
	}
	{
		WWW_t383 * L_16 = (__this->___mAvatarUrl_1);
		NullCheck(L_16);
		Texture2D_t384 * L_17 = WWW_get_texture_m3721(L_16, /*hidden argument*/NULL);
		__this->___mImage_2 = L_17;
		Texture2D_t384 * L_18 = (__this->___mImage_2);
		return L_18;
	}

IL_008b:
	{
		return (Texture2D_t384 *)NULL;
	}
}
// UnityEngine.Texture2D GooglePlayGames.PlayGamesLocalUser::get_image()
extern "C" Texture2D_t384 * PlayGamesLocalUser_get_image_m1520 (PlayGamesLocalUser_t385 * __this, MethodInfo* method)
{
	{
		Texture2D_t384 * L_0 = PlayGamesLocalUser_LoadImage_m1519(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>
#include "mscorlib_System_Action_1_gen_11.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
#include "mscorlib_System_Action_1_gen_12.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
#include "mscorlib_System_Action_1_gen_13.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
#include "mscorlib_System_Action_1_gen_14.h"
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
#include "mscorlib_System_Action_1_gen_15.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
// System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>
#include "mscorlib_System_Action_1_gen_11MethodDeclarations.h"
// UnityEngine.Social
#include "UnityEngine_UnityEngine_SocialMethodDeclarations.h"
// GooglePlayGames.PlayGamesClientFactory
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesClientFactoryMethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
#include "mscorlib_System_Action_1_gen_12MethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
#include "mscorlib_System_Action_1_gen_13MethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
#include "mscorlib_System_Action_1_gen_14MethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
#include "mscorlib_System_Action_1_gen_15MethodDeclarations.h"
struct Misc_t391;
struct IPlayGamesClient_t387;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.IPlayGamesClient>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.IPlayGamesClient>(!!0)
#define Misc_CheckNotNull_TisIPlayGamesClient_t387_m3722(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.PlayGamesPlatform::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern TypeInfo* Dictionary_2_t165_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesLocalUser_t385_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2__ctor_m911_MethodInfo_var;
extern "C" void PlayGamesPlatform__ctor_m1521 (PlayGamesPlatform_t382 * __this, PlayGamesClientConfiguration_t366  ___configuration, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		PlayGamesLocalUser_t385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(628);
		Dictionary_2__ctor_m911_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t165 * L_0 = (Dictionary_2_t165 *)il2cpp_codegen_object_new (Dictionary_2_t165_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m911(L_0, /*hidden argument*/Dictionary_2__ctor_m911_MethodInfo_var);
		__this->___mIdMap_7 = L_0;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		PlayGamesLocalUser_t385 * L_1 = (PlayGamesLocalUser_t385 *)il2cpp_codegen_object_new (PlayGamesLocalUser_t385_il2cpp_TypeInfo_var);
		PlayGamesLocalUser__ctor_m1508(L_1, __this, /*hidden argument*/NULL);
		__this->___mLocalUser_2 = L_1;
		PlayGamesClientConfiguration_t366  L_2 = ___configuration;
		__this->___mConfiguration_1 = L_2;
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::.ctor(GooglePlayGames.BasicApi.IPlayGamesClient)
extern TypeInfo* Dictionary_2_t165_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesLocalUser_t385_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2__ctor_m911_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisIPlayGamesClient_t387_m3722_MethodInfo_var;
extern "C" void PlayGamesPlatform__ctor_m1522 (PlayGamesPlatform_t382 * __this, Object_t * ___client, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		PlayGamesLocalUser_t385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(628);
		PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(620);
		Dictionary_2__ctor_m911_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		Misc_CheckNotNull_TisIPlayGamesClient_t387_m3722_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483834);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t165 * L_0 = (Dictionary_2_t165 *)il2cpp_codegen_object_new (Dictionary_2_t165_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m911(L_0, /*hidden argument*/Dictionary_2__ctor_m911_MethodInfo_var);
		__this->___mIdMap_7 = L_0;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		Object_t * L_1 = ___client;
		Object_t * L_2 = Misc_CheckNotNull_TisIPlayGamesClient_t387_m3722(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisIPlayGamesClient_t387_m3722_MethodInfo_var);
		__this->___mClient_3 = L_2;
		PlayGamesLocalUser_t385 * L_3 = (PlayGamesLocalUser_t385 *)il2cpp_codegen_object_new (PlayGamesLocalUser_t385_il2cpp_TypeInfo_var);
		PlayGamesLocalUser__ctor_m1508(L_3, __this, /*hidden argument*/NULL);
		__this->___mLocalUser_2 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var);
		PlayGamesClientConfiguration_t366  L_4 = ((PlayGamesClientConfiguration_t366_StaticFields*)PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var->static_fields)->___DefaultConfiguration_0;
		__this->___mConfiguration_1 = L_4;
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::.cctor()
extern "C" void PlayGamesPlatform__cctor_m1523 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::InitializeInstance(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern TypeInfo* PlayGamesPlatform_t382_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_InitializeInstance_m1524 (Object_t * __this /* static, unused */, PlayGamesClientConfiguration_t366  ___configuration, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesPlatform_t382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t382 * L_0 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		il2cpp_codegen_memory_barrier();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral370, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		PlayGamesClientConfiguration_t366  L_1 = ___configuration;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t382 * L_2 = (PlayGamesPlatform_t382 *)il2cpp_codegen_object_new (PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform__ctor_m1521(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_2;
		return;
	}
}
// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesPlatform::get_Instance()
extern TypeInfo* PlayGamesPlatform_t382_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var;
extern "C" PlayGamesPlatform_t382 * PlayGamesPlatform_get_Instance_m1525 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesPlatform_t382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(620);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t382 * L_0 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral369, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var);
		PlayGamesClientConfiguration_t366  L_1 = ((PlayGamesClientConfiguration_t366_StaticFields*)PlayGamesClientConfiguration_t366_il2cpp_TypeInfo_var->static_fields)->___DefaultConfiguration_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_InitializeInstance_m1524(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t382 * L_2 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		il2cpp_codegen_memory_barrier();
		return L_2;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::InitializeNearby(System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>)
extern TypeInfo* PlayGamesPlatform_t382_il2cpp_TypeInfo_var;
extern TypeInfo* DummyNearbyConnectionClient_t359_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_InitializeNearby_m1526 (Object_t * __this /* static, unused */, Action_1_t852 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesPlatform_t382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		DummyNearbyConnectionClient_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m869(NULL /*static, unused*/, (String_t*) &_stringLiteral371, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		Object_t * L_0 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyConnectionClient_5;
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		DummyNearbyConnectionClient_t359 * L_1 = (DummyNearbyConnectionClient_t359 *)il2cpp_codegen_object_new (DummyNearbyConnectionClient_t359_il2cpp_TypeInfo_var);
		DummyNearbyConnectionClient__ctor_m1439(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		il2cpp_codegen_memory_barrier();
		((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyConnectionClient_5 = L_1;
		Action_1_t852 * L_2 = ___callback;
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t852 * L_3 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		Object_t * L_4 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyConnectionClient_5;
		il2cpp_codegen_memory_barrier();
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>::Invoke(!0) */, L_3, L_4);
	}

IL_0035:
	{
		goto IL_0066;
	}

IL_003a:
	{
		Action_1_t852 * L_5 = ___callback;
		if (!L_5)
		{
			goto IL_005c;
		}
	}
	{
		Debug_Log_m869(NULL /*static, unused*/, (String_t*) &_stringLiteral372, /*hidden argument*/NULL);
		Action_1_t852 * L_6 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		Object_t * L_7 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyConnectionClient_5;
		il2cpp_codegen_memory_barrier();
		NullCheck(L_6);
		VirtActionInvoker1< Object_t * >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>::Invoke(!0) */, L_6, L_7);
		goto IL_0066;
	}

IL_005c:
	{
		Debug_Log_m869(NULL /*static, unused*/, (String_t*) &_stringLiteral373, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient GooglePlayGames.PlayGamesPlatform::get_Nearby()
extern TypeInfo* PlayGamesPlatform_t382_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_get_Nearby_m1527 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesPlatform_t382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		Object_t * L_0 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyConnectionClient_5;
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		bool L_1 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyInitializePending_4;
		il2cpp_codegen_memory_barrier();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		il2cpp_codegen_memory_barrier();
		((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyInitializePending_4 = 1;
		PlayGamesPlatform_InitializeNearby_m1526(NULL /*static, unused*/, (Action_1_t852 *)NULL, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		Object_t * L_2 = ((PlayGamesPlatform_t382_StaticFields*)PlayGamesPlatform_t382_il2cpp_TypeInfo_var->static_fields)->___sNearbyConnectionClient_5;
		il2cpp_codegen_memory_barrier();
		return L_2;
	}
}
// System.Boolean GooglePlayGames.PlayGamesPlatform::get_DebugLogEnabled()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" bool PlayGamesPlatform_get_DebugLogEnabled_m1528 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		bool L_0 = Logger_get_DebugLogEnabled_m1587(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::set_DebugLogEnabled(System.Boolean)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_set_DebugLogEnabled_m1529 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_set_DebugLogEnabled_m1588(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.PlayGamesPlatform::get_RealTime()
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_get_RealTime_m1530 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(15 /* GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.BasicApi.IPlayGamesClient::GetRtmpClient() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.PlayGamesPlatform::get_TurnBased()
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_get_TurnBased_m1531 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(16 /* GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.BasicApi.IPlayGamesClient::GetTbmpClient() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.PlayGamesPlatform::get_SavedGame()
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_get_SavedGame_m1532 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(17 /* GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.BasicApi.IPlayGamesClient::GetSavedGameClient() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.PlayGamesPlatform::get_Events()
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_get_Events_m1533 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(18 /* GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.BasicApi.IPlayGamesClient::GetEventsClient() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.PlayGamesPlatform::get_Quests()
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_get_Quests_m1534 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(19 /* GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.BasicApi.IPlayGamesClient::GetQuestsClient() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesPlatform::Activate()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesPlatform_t382_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" PlayGamesPlatform_t382 * PlayGamesPlatform_Activate_m1535 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		PlayGamesPlatform_t382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral374, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t382 * L_0 = PlayGamesPlatform_get_Instance_m1525(NULL /*static, unused*/, /*hidden argument*/NULL);
		Social_set_Active_m3723(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Object_t * L_1 = Social_get_Active_m3724(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral375, L_1, /*hidden argument*/NULL);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		PlayGamesPlatform_t382 * L_3 = PlayGamesPlatform_get_Instance_m1525(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::AddIdMapping(System.String,System.String)
extern "C" void PlayGamesPlatform_AddIdMapping_m1536 (PlayGamesPlatform_t382 * __this, String_t* ___fromId, String_t* ___toId, MethodInfo* method)
{
	{
		Dictionary_2_t165 * L_0 = (__this->___mIdMap_7);
		String_t* L_1 = ___fromId;
		String_t* L_2 = ___toId;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_Authenticate_m1537 (PlayGamesPlatform_t382 * __this, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = ___callback;
		PlayGamesPlatform_Authenticate_m1538(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_Authenticate_m1538 (PlayGamesPlatform_t382 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral376, /*hidden argument*/NULL);
		PlayGamesClientConfiguration_t366  L_1 = (__this->___mConfiguration_1);
		Object_t * L_2 = PlayGamesClientFactory_GetPlatformPlayGamesClient_m3133(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___mClient_3 = L_2;
	}

IL_0026:
	{
		Object_t * L_3 = (__this->___mClient_3);
		Action_1_t98 * L_4 = ___callback;
		bool L_5 = ___silent;
		NullCheck(L_3);
		InterfaceActionInvoker2< Action_1_t98 *, bool >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::Authenticate(System.Action`1<System.Boolean>,System.Boolean) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_3, L_4, L_5);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_Authenticate_m1539 (PlayGamesPlatform_t382 * __this, Object_t * ___unused, Action_1_t98 * ___callback, MethodInfo* method)
{
	{
		Action_1_t98 * L_0 = ___callback;
		PlayGamesPlatform_Authenticate_m1538(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesPlatform::IsAuthenticated()
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" bool PlayGamesPlatform_IsAuthenticated_m1540 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Object_t * L_0 = (__this->___mClient_3);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Object_t * L_1 = (__this->___mClient_3);
		NullCheck(L_1);
		bool L_2 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean GooglePlayGames.BasicApi.IPlayGamesClient::IsAuthenticated() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::SignOut()
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_SignOut_m1541 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Object_t * L_1 = (__this->___mClient_3);
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(2 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::SignOut() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IUserProfileU5BU5D_t850_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_LoadUsers_m1542 (PlayGamesPlatform_t382 * __this, StringU5BU5D_t169* ___userIDs, Action_1_t853 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IUserProfileU5BU5D_t850_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(625);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral377, /*hidden argument*/NULL);
		Action_1_t853 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Action_1_t853 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< IUserProfileU5BU5D_t850* >::Invoke(10 /* System.Void System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::Invoke(!0) */, L_1, ((IUserProfileU5BU5D_t850*)SZArrayNew(IUserProfileU5BU5D_t850_il2cpp_TypeInfo_var, 0)));
	}

IL_001c:
	{
		return;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetUserId()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesPlatform_GetUserId_m1543 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral378, /*hidden argument*/NULL);
		return (String_t*) &_stringLiteral379;
	}

IL_001b:
	{
		Object_t * L_1 = (__this->___mClient_3);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetUserId() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}
}
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.PlayGamesPlatform::GetAchievement(System.String)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" Achievement_t333 * PlayGamesPlatform_GetAchievement_m1544 (PlayGamesPlatform_t382 * __this, String_t* ___achievementId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral380, /*hidden argument*/NULL);
		return (Achievement_t333 *)NULL;
	}

IL_0017:
	{
		Object_t * L_1 = (__this->___mClient_3);
		String_t* L_2 = ___achievementId;
		NullCheck(L_1);
		Achievement_t333 * L_3 = (Achievement_t333 *)InterfaceFuncInvoker1< Achievement_t333 *, String_t* >::Invoke(6 /* GooglePlayGames.BasicApi.Achievement GooglePlayGames.BasicApi.IPlayGamesClient::GetAchievement(System.String) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_1, L_2);
		return L_3;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetUserDisplayName()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesPlatform_GetUserDisplayName_m1545 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral381, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_001b:
	{
		Object_t * L_2 = (__this->___mClient_3);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetUserDisplayName() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_2);
		return L_3;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetUserImageUrl()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesPlatform_GetUserImageUrl_m1546 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral382, /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_0017:
	{
		Object_t * L_1 = (__this->___mClient_3);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(5 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetUserImageUrl() */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t234_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t189_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_ReportProgress_m1547 (PlayGamesPlatform_t382 * __this, String_t* ___achievementID, double ___progress, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Double_t234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(166);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		Int32_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Achievement_t333 * V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* G_B10_0 = {0};
	String_t* G_B9_0 = {0};
	String_t* G_B11_0 = {0};
	String_t* G_B11_1 = {0};
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral383, /*hidden argument*/NULL);
		Action_1_t98 * L_1 = ___callback;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_2, 0);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		ObjectU5BU5D_t208* L_3 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, (String_t*) &_stringLiteral384);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0)) = (Object_t *)(String_t*) &_stringLiteral384;
		ObjectU5BU5D_t208* L_4 = L_3;
		String_t* L_5 = ___achievementID;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t208* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, (String_t*) &_stringLiteral385);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)(String_t*) &_stringLiteral385;
		ObjectU5BU5D_t208* L_7 = L_6;
		double L_8 = ___progress;
		double L_9 = L_8;
		Object_t * L_10 = Box(Double_t234_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m976(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___achievementID;
		String_t* L_13 = PlayGamesPlatform_MapId_m1568(__this, L_12, /*hidden argument*/NULL);
		___achievementID = L_13;
		double L_14 = ___progress;
		if ((!(((double)L_14) < ((double)(1.0E-06)))))
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral386, /*hidden argument*/NULL);
		Object_t * L_15 = (__this->___mClient_3);
		String_t* L_16 = ___achievementID;
		Action_1_t98 * L_17 = ___callback;
		NullCheck(L_15);
		InterfaceActionInvoker2< String_t*, Action_1_t98 * >::Invoke(8 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::RevealAchievement(System.String,System.Action`1<System.Boolean>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_15, L_16, L_17);
		return;
	}

IL_0080:
	{
		V_0 = 0;
		V_1 = 0;
		V_2 = 0;
		Object_t * L_18 = (__this->___mClient_3);
		String_t* L_19 = ___achievementID;
		NullCheck(L_18);
		Achievement_t333 * L_20 = (Achievement_t333 *)InterfaceFuncInvoker1< Achievement_t333 *, String_t* >::Invoke(6 /* GooglePlayGames.BasicApi.Achievement GooglePlayGames.BasicApi.IPlayGamesClient::GetAchievement(System.String) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_18, L_19);
		V_3 = L_20;
		Achievement_t333 * L_21 = V_3;
		if (L_21)
		{
			goto IL_00ba;
		}
	}
	{
		String_t* L_22 = ___achievementID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral387, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral388, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_012b;
	}

IL_00ba:
	{
		Achievement_t333 * L_24 = V_3;
		NullCheck(L_24);
		bool L_25 = Achievement_get_IsIncremental_m1329(L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		Achievement_t333 * L_26 = V_3;
		NullCheck(L_26);
		int32_t L_27 = Achievement_get_CurrentSteps_m1331(L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		Achievement_t333 * L_28 = V_3;
		NullCheck(L_28);
		int32_t L_29 = Achievement_get_TotalSteps_m1333(L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		bool L_30 = V_0;
		G_B9_0 = (String_t*) &_stringLiteral389;
		if (!L_30)
		{
			G_B10_0 = (String_t*) &_stringLiteral389;
			goto IL_00e4;
		}
	}
	{
		G_B11_0 = (String_t*) &_stringLiteral335;
		G_B11_1 = G_B9_0;
		goto IL_00e9;
	}

IL_00e4:
	{
		G_B11_0 = (String_t*) &_stringLiteral336;
		G_B11_1 = G_B10_0;
	}

IL_00e9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m856(NULL /*static, unused*/, G_B11_1, G_B11_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = V_0;
		if (!L_32)
		{
			goto IL_012b;
		}
	}
	{
		ObjectU5BU5D_t208* L_33 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 0);
		ArrayElementTypeCheck (L_33, (String_t*) &_stringLiteral390);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, 0)) = (Object_t *)(String_t*) &_stringLiteral390;
		ObjectU5BU5D_t208* L_34 = L_33;
		int32_t L_35 = V_1;
		int32_t L_36 = L_35;
		Object_t * L_37 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 1);
		ArrayElementTypeCheck (L_34, L_37);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_34, 1)) = (Object_t *)L_37;
		ObjectU5BU5D_t208* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 2);
		ArrayElementTypeCheck (L_38, (String_t*) &_stringLiteral391);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_38, 2)) = (Object_t *)(String_t*) &_stringLiteral391;
		ObjectU5BU5D_t208* L_39 = L_38;
		int32_t L_40 = V_2;
		int32_t L_41 = L_40;
		Object_t * L_42 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 3);
		ArrayElementTypeCheck (L_39, L_42);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_39, 3)) = (Object_t *)L_42;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m976(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
	}

IL_012b:
	{
		bool L_44 = V_0;
		if (!L_44)
		{
			goto IL_01ff;
		}
	}
	{
		double L_45 = ___progress;
		double L_46 = L_45;
		Object_t * L_47 = Box(Double_t234_il2cpp_TypeInfo_var, &L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m995(NULL /*static, unused*/, (String_t*) &_stringLiteral392, L_47, (String_t*) &_stringLiteral393, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		double L_49 = ___progress;
		if ((!(((double)L_49) >= ((double)(0.0)))))
		{
			goto IL_0183;
		}
	}
	{
		double L_50 = ___progress;
		if ((!(((double)L_50) <= ((double)(1.0)))))
		{
			goto IL_0183;
		}
	}
	{
		double L_51 = ___progress;
		double L_52 = L_51;
		Object_t * L_53 = Box(Double_t234_il2cpp_TypeInfo_var, &L_52);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = String_Concat_m995(NULL /*static, unused*/, (String_t*) &_stringLiteral392, L_53, (String_t*) &_stringLiteral394, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
	}

IL_0183:
	{
		double L_55 = ___progress;
		int32_t L_56 = V_2;
		V_4 = (((int32_t)((double)((double)((double)((double)L_55/(double)(100.0)))*(double)(((double)L_56))))));
		int32_t L_57 = V_4;
		int32_t L_58 = V_1;
		V_5 = ((int32_t)((int32_t)L_57-(int32_t)L_58));
		ObjectU5BU5D_t208* L_59 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 0);
		ArrayElementTypeCheck (L_59, (String_t*) &_stringLiteral395);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_59, 0)) = (Object_t *)(String_t*) &_stringLiteral395;
		ObjectU5BU5D_t208* L_60 = L_59;
		int32_t L_61 = V_4;
		int32_t L_62 = L_61;
		Object_t * L_63 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 1);
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, 1)) = (Object_t *)L_63;
		ObjectU5BU5D_t208* L_64 = L_60;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 2);
		ArrayElementTypeCheck (L_64, (String_t*) &_stringLiteral396);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_64, 2)) = (Object_t *)(String_t*) &_stringLiteral396;
		ObjectU5BU5D_t208* L_65 = L_64;
		int32_t L_66 = V_1;
		int32_t L_67 = L_66;
		Object_t * L_68 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, 3);
		ArrayElementTypeCheck (L_65, L_68);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_65, 3)) = (Object_t *)L_68;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = String_Concat_m976(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		int32_t L_70 = V_5;
		int32_t L_71 = L_70;
		Object_t * L_72 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_71);
		String_t* L_73 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral397, L_72, /*hidden argument*/NULL);
		Logger_d_m1591(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		int32_t L_74 = V_5;
		if ((((int32_t)L_74) <= ((int32_t)0)))
		{
			goto IL_01fa;
		}
	}
	{
		Object_t * L_75 = (__this->___mClient_3);
		String_t* L_76 = ___achievementID;
		int32_t L_77 = V_5;
		Action_1_t98 * L_78 = ___callback;
		NullCheck(L_75);
		InterfaceActionInvoker3< String_t*, int32_t, Action_1_t98 * >::Invoke(9 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_75, L_76, L_77, L_78);
	}

IL_01fa:
	{
		goto IL_0254;
	}

IL_01ff:
	{
		double L_79 = ___progress;
		if ((!(((double)L_79) >= ((double)(100.0)))))
		{
			goto IL_023a;
		}
	}
	{
		double L_80 = ___progress;
		double L_81 = L_80;
		Object_t * L_82 = Box(Double_t234_il2cpp_TypeInfo_var, &L_81);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_83 = String_Concat_m995(NULL /*static, unused*/, (String_t*) &_stringLiteral392, L_82, (String_t*) &_stringLiteral398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		Object_t * L_84 = (__this->___mClient_3);
		String_t* L_85 = ___achievementID;
		Action_1_t98 * L_86 = ___callback;
		NullCheck(L_84);
		InterfaceActionInvoker2< String_t*, Action_1_t98 * >::Invoke(7 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::UnlockAchievement(System.String,System.Action`1<System.Boolean>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_84, L_85, L_86);
		goto IL_0254;
	}

IL_023a:
	{
		double L_87 = ___progress;
		double L_88 = L_87;
		Object_t * L_89 = Box(Double_t234_il2cpp_TypeInfo_var, &L_88);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_90 = String_Concat_m995(NULL /*static, unused*/, (String_t*) &_stringLiteral392, L_89, (String_t*) &_stringLiteral399, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
	}

IL_0254:
	{
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t189_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_IncrementAchievement_m1548 (PlayGamesPlatform_t382 * __this, String_t* ___achievementID, int32_t ___steps, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Int32_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral400, /*hidden argument*/NULL);
		Action_1_t98 * L_1 = ___callback;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_2, 0);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		ObjectU5BU5D_t208* L_3 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, (String_t*) &_stringLiteral401);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0)) = (Object_t *)(String_t*) &_stringLiteral401;
		ObjectU5BU5D_t208* L_4 = L_3;
		String_t* L_5 = ___achievementID;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t208* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, (String_t*) &_stringLiteral402);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)(String_t*) &_stringLiteral402;
		ObjectU5BU5D_t208* L_7 = L_6;
		int32_t L_8 = ___steps;
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m976(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___achievementID;
		String_t* L_13 = PlayGamesPlatform_MapId_m1568(__this, L_12, /*hidden argument*/NULL);
		___achievementID = L_13;
		Object_t * L_14 = (__this->___mClient_3);
		String_t* L_15 = ___achievementID;
		int32_t L_16 = ___steps;
		Action_1_t98 * L_17 = ___callback;
		NullCheck(L_14);
		InterfaceActionInvoker3< String_t*, int32_t, Action_1_t98 * >::Invoke(9 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_14, L_15, L_16, L_17);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IAchievementDescriptionU5BU5D_t906_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_LoadAchievementDescriptions_m1549 (PlayGamesPlatform_t382 * __this, Action_1_t854 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IAchievementDescriptionU5BU5D_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(631);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral403, /*hidden argument*/NULL);
		Action_1_t854 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Action_1_t854 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< IAchievementDescriptionU5BU5D_t906* >::Invoke(10 /* System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::Invoke(!0) */, L_1, ((IAchievementDescriptionU5BU5D_t906*)SZArrayNew(IAchievementDescriptionU5BU5D_t906_il2cpp_TypeInfo_var, 0)));
	}

IL_001c:
	{
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IAchievementU5BU5D_t732_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_LoadAchievements_m1550 (PlayGamesPlatform_t382 * __this, Action_1_t855 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IAchievementU5BU5D_t732_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(633);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral404, /*hidden argument*/NULL);
		Action_1_t855 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Action_1_t855 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< IAchievementU5BU5D_t732* >::Invoke(10 /* System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::Invoke(!0) */, L_1, ((IAchievementU5BU5D_t732*)SZArrayNew(IAchievementU5BU5D_t732_il2cpp_TypeInfo_var, 0)));
	}

IL_001c:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.IAchievement GooglePlayGames.PlayGamesPlatform::CreateAchievement()
extern TypeInfo* PlayGamesAchievement_t381_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_CreateAchievement_m1551 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesAchievement_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(624);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesAchievement_t381_il2cpp_TypeInfo_var);
		PlayGamesAchievement_t381 * L_0 = (PlayGamesAchievement_t381 *)il2cpp_codegen_object_new (PlayGamesAchievement_t381_il2cpp_TypeInfo_var);
		PlayGamesAchievement__ctor_m1497(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t233_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_ReportScore_m1552 (PlayGamesPlatform_t382 * __this, int64_t ___score, String_t* ___board, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Int64_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral405, /*hidden argument*/NULL);
		Action_1_t98 * L_1 = ___callback;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_2, 0);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		ObjectU5BU5D_t208* L_3 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, (String_t*) &_stringLiteral406);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0)) = (Object_t *)(String_t*) &_stringLiteral406;
		ObjectU5BU5D_t208* L_4 = L_3;
		int64_t L_5 = ___score;
		int64_t L_6 = L_5;
		Object_t * L_7 = Box(Int64_t233_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t208* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, (String_t*) &_stringLiteral407);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)(String_t*) &_stringLiteral407;
		ObjectU5BU5D_t208* L_9 = L_8;
		String_t* L_10 = ___board;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 3)) = (Object_t *)L_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m976(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___board;
		String_t* L_13 = PlayGamesPlatform_MapId_m1568(__this, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		Object_t * L_14 = (__this->___mClient_3);
		String_t* L_15 = V_0;
		int64_t L_16 = ___score;
		Action_1_t98 * L_17 = ___callback;
		NullCheck(L_14);
		InterfaceActionInvoker3< String_t*, int64_t, Action_1_t98 * >::Invoke(12 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::SubmitScore(System.String,System.Int64,System.Action`1<System.Boolean>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_14, L_15, L_16, L_17);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* IScoreU5BU5D_t907_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_LoadScores_m1553 (PlayGamesPlatform_t382 * __this, String_t* ___leaderboardID, Action_1_t857 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		IScoreU5BU5D_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral408, /*hidden argument*/NULL);
		Action_1_t857 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Action_1_t857 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< IScoreU5BU5D_t907* >::Invoke(10 /* System.Void System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::Invoke(!0) */, L_1, ((IScoreU5BU5D_t907*)SZArrayNew(IScoreU5BU5D_t907_il2cpp_TypeInfo_var, 0)));
	}

IL_001c:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard GooglePlayGames.PlayGamesPlatform::CreateLeaderboard()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayGamesPlatform_CreateLeaderboard_m1554 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral409, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowAchievementsUI()
extern "C" void PlayGamesPlatform_ShowAchievementsUI_m1555 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	{
		PlayGamesPlatform_ShowAchievementsUI_m1556(__this, (Action_1_t530 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_ShowAchievementsUI_m1556 (PlayGamesPlatform_t382 * __this, Action_1_t530 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral410, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		Action_1_t530 * L_1 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral411, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Object_t * L_3 = (__this->___mClient_3);
		Action_1_t530 * L_4 = ___callback;
		NullCheck(L_3);
		InterfaceActionInvoker1< Action_1_t530 * >::Invoke(10 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_3, L_4);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_ShowLeaderboardUI_m1557 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral412, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___mDefaultLbUi_6);
		String_t* L_1 = PlayGamesPlatform_MapId_m1568(__this, L_0, /*hidden argument*/NULL);
		PlayGamesPlatform_ShowLeaderboardUI_m1559(__this, L_1, (Action_1_t530 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI(System.String)
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_ShowLeaderboardUI_m1558 (PlayGamesPlatform_t382 * __this, String_t* ___lbId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___lbId;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = ___lbId;
		String_t* L_2 = PlayGamesPlatform_MapId_m1568(__this, L_1, /*hidden argument*/NULL);
		___lbId = L_2;
	}

IL_000f:
	{
		Object_t * L_3 = (__this->___mClient_3);
		String_t* L_4 = ___lbId;
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, Action_1_t530 * >::Invoke(11 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_3, L_4, (Action_1_t530 *)NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_ShowLeaderboardUI_m1559 (PlayGamesPlatform_t382 * __this, String_t* ___lbId, Action_1_t530 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral413, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		ObjectU5BU5D_t208* L_1 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, (String_t*) &_stringLiteral414);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)(String_t*) &_stringLiteral414;
		ObjectU5BU5D_t208* L_2 = L_1;
		String_t* L_3 = ___lbId;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t208* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, (String_t*) &_stringLiteral415);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)(String_t*) &_stringLiteral415;
		ObjectU5BU5D_t208* L_5 = L_4;
		Action_1_t530 * L_6 = ___callback;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3)) = (Object_t *)L_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m976(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Object_t * L_8 = (__this->___mClient_3);
		String_t* L_9 = ___lbId;
		Action_1_t530 * L_10 = ___callback;
		NullCheck(L_8);
		InterfaceActionInvoker2< String_t*, Action_1_t530 * >::Invoke(11 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_8, L_9, L_10);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::SetDefaultLeaderboardForUI(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_SetDefaultLeaderboardForUI_m1560 (PlayGamesPlatform_t382 * __this, String_t* ___lbid, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___lbid;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral416, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___lbid;
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_3 = ___lbid;
		String_t* L_4 = PlayGamesPlatform_MapId_m1568(__this, L_3, /*hidden argument*/NULL);
		___lbid = L_4;
	}

IL_001f:
	{
		String_t* L_5 = ___lbid;
		__this->___mDefaultLbUi_6 = L_5;
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_LoadFriends_m1561 (PlayGamesPlatform_t382 * __this, Object_t * ___user, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral417, /*hidden argument*/NULL);
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_0017:
	{
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_LoadScores_m1562 (PlayGamesPlatform_t382 * __this, Object_t * ___board, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral408, /*hidden argument*/NULL);
		Action_1_t98 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t98 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, 0);
	}

IL_0017:
	{
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern "C" bool PlayGamesPlatform_GetLoading_m1563 (PlayGamesPlatform_t382 * __this, Object_t * ___board, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* OnStateLoadedListener_t842_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_LoadState_m1564 (PlayGamesPlatform_t382 * __this, int32_t ___slot, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		OnStateLoadedListener_t842_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral418, /*hidden argument*/NULL);
		Object_t * L_1 = ___listener;
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Object_t * L_2 = ___listener;
		int32_t L_3 = ___slot;
		NullCheck(L_2);
		InterfaceActionInvoker3< bool, int32_t, ByteU5BU5D_t350* >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.OnStateLoadedListener::OnStateLoaded(System.Boolean,System.Int32,System.Byte[]) */, OnStateLoadedListener_t842_il2cpp_TypeInfo_var, L_2, 0, L_3, (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL);
	}

IL_0024:
	{
		return;
	}

IL_0025:
	{
		Object_t * L_4 = (__this->___mClient_3);
		int32_t L_5 = ___slot;
		Object_t * L_6 = ___listener;
		NullCheck(L_4);
		InterfaceActionInvoker2< int32_t, Object_t * >::Invoke(13 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_4, L_5, L_6);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* OnStateLoadedListener_t842_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_UpdateState_m1565 (PlayGamesPlatform_t382 * __this, int32_t ___slot, ByteU5BU5D_t350* ___data, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		OnStateLoadedListener_t842_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m1540(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral419, /*hidden argument*/NULL);
		Object_t * L_1 = ___listener;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Object_t * L_2 = ___listener;
		int32_t L_3 = ___slot;
		NullCheck(L_2);
		InterfaceActionInvoker2< bool, int32_t >::Invoke(2 /* System.Void GooglePlayGames.BasicApi.OnStateLoadedListener::OnStateSaved(System.Boolean,System.Int32) */, OnStateLoadedListener_t842_il2cpp_TypeInfo_var, L_2, 0, L_3);
	}

IL_0023:
	{
		return;
	}

IL_0024:
	{
		Object_t * L_4 = (__this->___mClient_3);
		int32_t L_5 = ___slot;
		ByteU5BU5D_t350* L_6 = ___data;
		Object_t * L_7 = ___listener;
		NullCheck(L_4);
		InterfaceActionInvoker3< int32_t, ByteU5BU5D_t350*, Object_t * >::Invoke(14 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7);
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser GooglePlayGames.PlayGamesPlatform::get_localUser()
extern "C" Object_t * PlayGamesPlatform_get_localUser_m1566 (PlayGamesPlatform_t382 * __this, MethodInfo* method)
{
	{
		PlayGamesLocalUser_t385 * L_0 = (__this->___mLocalUser_2);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern TypeInfo* IPlayGamesClient_t387_il2cpp_TypeInfo_var;
extern "C" void PlayGamesPlatform_RegisterInvitationDelegate_m1567 (PlayGamesPlatform_t382 * __this, InvitationReceivedDelegate_t363 * ___deleg, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPlayGamesClient_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mClient_3);
		InvitationReceivedDelegate_t363 * L_1 = ___deleg;
		NullCheck(L_0);
		InterfaceActionInvoker1< InvitationReceivedDelegate_t363 * >::Invoke(20 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate) */, IPlayGamesClient_t387_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::MapId(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesPlatform_MapId_m1568 (PlayGamesPlatform_t382 * __this, String_t* ___id, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = ___id;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		Dictionary_2_t165 * L_1 = (__this->___mIdMap_7);
		String_t* L_2 = ___id;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_1, L_2);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		Dictionary_2_t165 * L_4 = (__this->___mIdMap_7);
		String_t* L_5 = ___id;
		NullCheck(L_4);
		String_t* L_6 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_4, L_5);
		V_0 = L_6;
		String_t* L_7 = ___id;
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1025(NULL /*static, unused*/, (String_t*) &_stringLiteral420, L_7, (String_t*) &_stringLiteral421, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_10 = V_0;
		return L_10;
	}

IL_003e:
	{
		String_t* L_11 = ___id;
		return L_11;
	}
}
// GooglePlayGames.PlayGamesScore
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesScore.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.PlayGamesScore
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesScoreMethodDeclarations.h"

// System.Int64
#include "mscorlib_System_Int64MethodDeclarations.h"


// System.Void GooglePlayGames.PlayGamesScore::.ctor()
extern "C" void PlayGamesScore__ctor_m1569 (PlayGamesScore_t389 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesScore::ReportScore(System.Action`1<System.Boolean>)
extern TypeInfo* PlayGamesPlatform_t382_il2cpp_TypeInfo_var;
extern "C" void PlayGamesScore_ReportScore_m1570 (PlayGamesScore_t389 * __this, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesPlatform_t382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t382_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t382 * L_0 = PlayGamesPlatform_get_Instance_m1525(NULL /*static, unused*/, /*hidden argument*/NULL);
		int64_t L_1 = (__this->___mValue_1);
		String_t* L_2 = (__this->___mLbId_0);
		Action_1_t98 * L_3 = ___callback;
		NullCheck(L_0);
		VirtActionInvoker3< int64_t, String_t*, Action_1_t98 * >::Invoke(7 /* System.Void GooglePlayGames.PlayGamesPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>) */, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesScore::get_leaderboardID()
extern "C" String_t* PlayGamesScore_get_leaderboardID_m1571 (PlayGamesScore_t389 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mLbId_0);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesScore::set_leaderboardID(System.String)
extern "C" void PlayGamesScore_set_leaderboardID_m1572 (PlayGamesScore_t389 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mLbId_0 = L_0;
		return;
	}
}
// System.Int64 GooglePlayGames.PlayGamesScore::get_value()
extern "C" int64_t PlayGamesScore_get_value_m1573 (PlayGamesScore_t389 * __this, MethodInfo* method)
{
	{
		int64_t L_0 = (__this->___mValue_1);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesScore::set_value(System.Int64)
extern "C" void PlayGamesScore_set_value_m1574 (PlayGamesScore_t389 * __this, int64_t ___value, MethodInfo* method)
{
	{
		int64_t L_0 = ___value;
		__this->___mValue_1 = L_0;
		return;
	}
}
// System.DateTime GooglePlayGames.PlayGamesScore::get_date()
extern "C" DateTime_t48  PlayGamesScore_get_date_m1575 (PlayGamesScore_t389 * __this, MethodInfo* method)
{
	{
		DateTime_t48  L_0 = {0};
		DateTime__ctor_m3725(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String GooglePlayGames.PlayGamesScore::get_formattedValue()
extern "C" String_t* PlayGamesScore_get_formattedValue_m1576 (PlayGamesScore_t389 * __this, MethodInfo* method)
{
	{
		int64_t* L_0 = &(__this->___mValue_1);
		String_t* L_1 = Int64_ToString_m3726(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.PlayGamesScore::get_userID()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesScore_get_userID_m1577 (PlayGamesScore_t389 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.Int32 GooglePlayGames.PlayGamesScore::get_rank()
extern "C" int32_t PlayGamesScore_get_rank_m1578 (PlayGamesScore_t389 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
// GooglePlayGames.PlayGamesUserProfile
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserProfile.h"
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.PlayGamesUserProfile::.ctor()
extern "C" void PlayGamesUserProfile__ctor_m1579 (PlayGamesUserProfile_t386 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesUserProfile::get_userName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesUserProfile_get_userName_m1580 (PlayGamesUserProfile_t386 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.String GooglePlayGames.PlayGamesUserProfile::get_id()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayGamesUserProfile_get_id_m1581 (PlayGamesUserProfile_t386 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesUserProfile::get_isFriend()
extern "C" bool PlayGamesUserProfile_get_isFriend_m1582 (PlayGamesUserProfile_t386 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// UnityEngine.SocialPlatforms.UserState GooglePlayGames.PlayGamesUserProfile::get_state()
extern "C" int32_t PlayGamesUserProfile_get_state_m1583 (PlayGamesUserProfile_t386 * __this, MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// UnityEngine.Texture2D GooglePlayGames.PlayGamesUserProfile::get_image()
extern "C" Texture2D_t384 * PlayGamesUserProfile_get_image_m1584 (PlayGamesUserProfile_t386 * __this, MethodInfo* method)
{
	{
		return (Texture2D_t384 *)NULL;
	}
}
// GooglePlayGames.OurUtils.Logger
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger.h"
#ifndef _MSC_VER
#else
#endif



// System.Void GooglePlayGames.OurUtils.Logger::.ctor()
extern "C" void Logger__ctor_m1585 (Logger_t390 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::.cctor()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void Logger__cctor_m1586 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___warningLogEnabled_1 = 1;
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.Logger::get_DebugLogEnabled()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" bool Logger_get_DebugLogEnabled_m1587 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___debugLogEnabled_0;
		return L_0;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::set_DebugLogEnabled(System.Boolean)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void Logger_set_DebugLogEnabled_m1588 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___debugLogEnabled_0 = L_0;
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.Logger::get_WarningLogEnabled()
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" bool Logger_get_WarningLogEnabled_m1589 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___warningLogEnabled_1;
		return L_0;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::set_WarningLogEnabled(System.Boolean)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void Logger_set_WarningLogEnabled_m1590 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___warningLogEnabled_1 = L_0;
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::d(System.String)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Logger_d_m1591 (Object_t * __this /* static, unused */, String_t* ___msg, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___debugLogEnabled_0;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_2 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		String_t* L_3 = Logger_ToLogMessage_m1595(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral422, L_2, /*hidden argument*/NULL);
		Debug_Log_m869(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::w(System.String)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void Logger_w_m1592 (Object_t * __this /* static, unused */, String_t* ___msg, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___warningLogEnabled_1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_1 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		String_t* L_2 = Logger_ToLogMessage_m1595(NULL /*static, unused*/, (String_t*) &_stringLiteral423, (String_t*) &_stringLiteral424, L_1, /*hidden argument*/NULL);
		Debug_LogWarning_m904(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::e(System.String)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void Logger_e_m1593 (Object_t * __this /* static, unused */, String_t* ___msg, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t390_StaticFields*)Logger_t390_il2cpp_TypeInfo_var->static_fields)->___warningLogEnabled_1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_1 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		String_t* L_2 = Logger_ToLogMessage_m1595(NULL /*static, unused*/, (String_t*) &_stringLiteral425, (String_t*) &_stringLiteral426, L_1, /*hidden argument*/NULL);
		Debug_LogWarning_m904(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.String GooglePlayGames.OurUtils.Logger::describe(System.Byte[])
extern TypeInfo* Int32_t189_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Logger_describe_m1594 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___b, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		ByteU5BU5D_t350* L_0 = ___b;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = (String_t*) &_stringLiteral427;
		goto IL_0027;
	}

IL_0010:
	{
		ByteU5BU5D_t350* L_1 = ___b;
		NullCheck(L_1);
		int32_t L_2 = (((int32_t)(((Array_t *)L_1)->max_length)));
		Object_t * L_3 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m995(NULL /*static, unused*/, (String_t*) &_stringLiteral428, L_3, (String_t*) &_stringLiteral44, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0027:
	{
		return G_B3_0;
	}
}
// System.String GooglePlayGames.OurUtils.Logger::ToLogMessage(System.String,System.String,System.String)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t48_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Logger_ToLogMessage_m1595 (Object_t * __this /* static, unused */, String_t* ___prefix, String_t* ___logType, String_t* ___msg, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		DateTime_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t48  V_0 = {0};
	{
		ObjectU5BU5D_t208* L_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = ___prefix;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t208* L_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t48_il2cpp_TypeInfo_var);
		DateTime_t48  L_3 = DateTime_get_Now_m924(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = DateTime_ToString_m3727((&V_0), (String_t*) &_stringLiteral430, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_4;
		ObjectU5BU5D_t208* L_5 = L_2;
		String_t* L_6 = ___logType;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2)) = (Object_t *)L_6;
		ObjectU5BU5D_t208* L_7 = L_5;
		String_t* L_8 = ___msg;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m3689(NULL /*static, unused*/, (String_t*) &_stringLiteral429, L_7, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"


// System.Boolean GooglePlayGames.OurUtils.Misc::BuffersAreIdentical(System.Byte[],System.Byte[])
extern "C" bool Misc_BuffersAreIdentical_m1596 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___a, ByteU5BU5D_t350* ___b, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t350* L_0 = ___a;
		ByteU5BU5D_t350* L_1 = ___b;
		if ((!(((Object_t*)(ByteU5BU5D_t350*)L_0) == ((Object_t*)(ByteU5BU5D_t350*)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return 1;
	}

IL_0009:
	{
		ByteU5BU5D_t350* L_2 = ___a;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		ByteU5BU5D_t350* L_3 = ___b;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return 0;
	}

IL_0017:
	{
		ByteU5BU5D_t350* L_4 = ___a;
		NullCheck(L_4);
		ByteU5BU5D_t350* L_5 = ___b;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)(((Array_t *)L_4)->max_length)))) == ((int32_t)(((int32_t)(((Array_t *)L_5)->max_length))))))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}

IL_0024:
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_002b:
	{
		ByteU5BU5D_t350* L_6 = ___a;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		ByteU5BU5D_t350* L_9 = ___b;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_6, L_8))) == ((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_11)))))
		{
			goto IL_0038;
		}
	}
	{
		return 0;
	}

IL_0038:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_13 = V_0;
		ByteU5BU5D_t350* L_14 = ___a;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)(((Array_t *)L_14)->max_length))))))
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}
}
// System.Byte[] GooglePlayGames.OurUtils.Misc::GetSubsetBytes(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t909_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t350_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t350* Misc_GetSubsetBytes_m1597 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___array, int32_t ___offset, int32_t ___length, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentOutOfRangeException_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(638);
		ByteU5BU5D_t350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t350* V_0 = {0};
	{
		ByteU5BU5D_t350* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t908 * L_1 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_1, (String_t*) &_stringLiteral431, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___offset;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = ___offset;
		ByteU5BU5D_t350* L_4 = ___array;
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)(((Array_t *)L_4)->max_length))))))
		{
			goto IL_002c;
		}
	}

IL_0021:
	{
		ArgumentOutOfRangeException_t909 * L_5 = (ArgumentOutOfRangeException_t909 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t909_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3729(L_5, (String_t*) &_stringLiteral432, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002c:
	{
		int32_t L_6 = ___length;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		ByteU5BU5D_t350* L_7 = ___array;
		NullCheck(L_7);
		int32_t L_8 = ___offset;
		int32_t L_9 = ___length;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))-(int32_t)L_8))) >= ((int32_t)L_9)))
		{
			goto IL_0049;
		}
	}

IL_003e:
	{
		ArgumentOutOfRangeException_t909 * L_10 = (ArgumentOutOfRangeException_t909 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t909_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3729(L_10, (String_t*) &_stringLiteral433, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0049:
	{
		int32_t L_11 = ___offset;
		if (L_11)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_12 = ___length;
		ByteU5BU5D_t350* L_13 = ___array;
		NullCheck(L_13);
		if ((!(((uint32_t)L_12) == ((uint32_t)(((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_005a;
		}
	}
	{
		ByteU5BU5D_t350* L_14 = ___array;
		return L_14;
	}

IL_005a:
	{
		int32_t L_15 = ___length;
		V_0 = ((ByteU5BU5D_t350*)SZArrayNew(ByteU5BU5D_t350_il2cpp_TypeInfo_var, L_15));
		ByteU5BU5D_t350* L_16 = ___array;
		int32_t L_17 = ___offset;
		ByteU5BU5D_t350* L_18 = V_0;
		int32_t L_19 = ___length;
		Array_Copy_m3730(NULL /*static, unused*/, (Array_t *)(Array_t *)L_16, L_17, (Array_t *)(Array_t *)L_18, 0, L_19, /*hidden argument*/NULL);
		ByteU5BU5D_t350* L_20 = V_0;
		return L_20;
	}
}
// GooglePlayGames.OurUtils.PlayGamesHelperObject
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGamesHelperOb.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.OurUtils.PlayGamesHelperObject
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGamesHelperObMethodDeclarations.h"

// System.Collections.Generic.List`1<System.Action>
#include "mscorlib_System_Collections_Generic_List_1_gen_15.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Action`1<System.Action>
#include "mscorlib_System_Action_1_gen_16.h"
// System.Collections.Generic.List`1<System.Action>
#include "mscorlib_System_Collections_Generic_List_1_gen_15MethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// System.Action`1<System.Action>
#include "mscorlib_System_Action_1_gen_16MethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
struct GameObject_t144;
struct PlayGamesHelperObject_t392;
struct GameObject_t144;
struct Component_t230;
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Component>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Component>()
extern "C" Component_t230 * GameObject_AddComponent_TisComponent_t230_m1035_gshared (GameObject_t144 * __this, MethodInfo* method);
#define GameObject_AddComponent_TisComponent_t230_m1035(__this, method) (( Component_t230 * (*) (GameObject_t144 *, MethodInfo*))GameObject_AddComponent_TisComponent_t230_m1035_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<GooglePlayGames.OurUtils.PlayGamesHelperObject>()
// !!0 UnityEngine.GameObject::AddComponent<GooglePlayGames.OurUtils.PlayGamesHelperObject>()
#define GameObject_AddComponent_TisPlayGamesHelperObject_t392_m3731(__this, method) (( PlayGamesHelperObject_t392 * (*) (GameObject_t144 *, MethodInfo*))GameObject_AddComponent_TisComponent_t230_m1035_gshared)(__this, method)


// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.ctor()
extern "C" void PlayGamesHelperObject__ctor_m1598 (PlayGamesHelperObject_t392 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.cctor()
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t393_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3732_MethodInfo_var;
extern "C" void PlayGamesHelperObject__cctor_m1599 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		List_1_t393_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		List_1__ctor_m3732_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483835);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___instance_2 = (PlayGamesHelperObject_t392 *)NULL;
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sIsDummy_3 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t393_il2cpp_TypeInfo_var);
		List_1_t393 * L_0 = (List_1_t393 *)il2cpp_codegen_object_new (List_1_t393_il2cpp_TypeInfo_var);
		List_1__ctor_m3732(L_0, /*hidden argument*/List_1__ctor_m3732_MethodInfo_var);
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueue_4 = L_0;
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueueEmpty_5 = 1;
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sPauseCallback_6 = (Action_1_t98 *)NULL;
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sFocusCallback_7 = (Action_1_t98 *)NULL;
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::CreateObject()
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t144_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_AddComponent_TisPlayGamesHelperObject_t392_m3731_MethodInfo_var;
extern "C" void PlayGamesHelperObject_CreateObject_m1600 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		GameObject_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		GameObject_AddComponent_TisPlayGamesHelperObject_t392_m3731_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483836);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t144 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_t392 * L_0 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_1 = Object_op_Inequality_m3719(NULL /*static, unused*/, L_0, (Object_t187 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		bool L_2 = Application_get_isPlaying_m3733(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		GameObject_t144 * L_3 = (GameObject_t144 *)il2cpp_codegen_object_new (GameObject_t144_il2cpp_TypeInfo_var);
		GameObject__ctor_m1036(L_3, (String_t*) &_stringLiteral434, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t144 * L_4 = V_0;
		Object_DontDestroyOnLoad_m854(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GameObject_t144 * L_5 = V_0;
		NullCheck(L_5);
		PlayGamesHelperObject_t392 * L_6 = GameObject_AddComponent_TisPlayGamesHelperObject_t392_m3731(L_5, /*hidden argument*/GameObject_AddComponent_TisPlayGamesHelperObject_t392_m3731_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___instance_2 = L_6;
		goto IL_004c;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_t392 * L_7 = (PlayGamesHelperObject_t392 *)il2cpp_codegen_object_new (PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject__ctor_m1598(L_7, /*hidden argument*/NULL);
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___instance_2 = L_7;
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sIsDummy_3 = 1;
	}

IL_004c:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Awake()
extern "C" void PlayGamesHelperObject_Awake_m1601 (PlayGamesHelperObject_t392 * __this, MethodInfo* method)
{
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m854(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnDisable()
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern "C" void PlayGamesHelperObject_OnDisable_m1602 (PlayGamesHelperObject_t392 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_t392 * L_0 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_1 = Object_op_Equality_m848(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___instance_2 = (PlayGamesHelperObject_t392 *)NULL;
	}

IL_0016:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunOnGameThread(System.Action)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern "C" void PlayGamesHelperObject_RunOnGameThread_m1603 (Object_t * __this /* static, unused */, Action_t588 * ___action, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t393 * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Action_t588 * L_0 = ___action;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t908 * L_1 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_1, (String_t*) &_stringLiteral435, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		bool L_2 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sIsDummy_3;
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		List_1_t393 * L_3 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueue_4;
		V_0 = L_3;
		List_1_t393 * L_4 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		List_1_t393 * L_5 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueue_4;
		Action_t588 * L_6 = ___action;
		NullCheck(L_5);
		VirtActionInvoker1< Action_t588 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Action>::Add(!0) */, L_5, L_6);
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueueEmpty_5 = 0;
		IL2CPP_LEAVE(0x47, FINALLY_0040);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		List_1_t393 * L_7 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Update()
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t393_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t394_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3732_MethodInfo_var;
extern MethodInfo* List_1_AddRange_m3736_MethodInfo_var;
extern MethodInfo* PlayGamesHelperObject_U3CUpdateU3Em__3_m1609_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3737_MethodInfo_var;
extern MethodInfo* List_1_ForEach_m3738_MethodInfo_var;
extern "C" void PlayGamesHelperObject_Update_m1604 (PlayGamesHelperObject_t392 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		List_1_t393_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		Action_1_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(642);
		List_1__ctor_m3732_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483835);
		List_1_AddRange_m3736_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483837);
		PlayGamesHelperObject_U3CUpdateU3Em__3_m1609_MethodInfo_var = il2cpp_codegen_method_info_from_index(190);
		Action_1__ctor_m3737_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483839);
		List_1_ForEach_m3738_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483840);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t393 * V_0 = {0};
	List_1_t393 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t393 * G_B8_0 = {0};
	List_1_t393 * G_B7_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sIsDummy_3;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		bool L_1 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueueEmpty_5;
		il2cpp_codegen_memory_barrier();
		if (!L_1)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t393_il2cpp_TypeInfo_var);
		List_1_t393 * L_2 = (List_1_t393 *)il2cpp_codegen_object_new (List_1_t393_il2cpp_TypeInfo_var);
		List_1__ctor_m3732(L_2, /*hidden argument*/List_1__ctor_m3732_MethodInfo_var);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		List_1_t393 * L_3 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueue_4;
		V_1 = L_3;
		List_1_t393 * L_4 = V_1;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		List_1_t393 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		List_1_t393 * L_6 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueue_4;
		NullCheck(L_5);
		List_1_AddRange_m3736(L_5, L_6, /*hidden argument*/List_1_AddRange_m3736_MethodInfo_var);
		List_1_t393 * L_7 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueue_4;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Action>::Clear() */, L_7);
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sQueueEmpty_5 = 1;
		IL2CPP_LEAVE(0x52, FINALLY_004b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		List_1_t393 * L_8 = V_1;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(75)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0052:
	{
		List_1_t393 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		Action_1_t394 * L_10 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_8;
		G_B7_0 = L_9;
		if (L_10)
		{
			G_B8_0 = L_9;
			goto IL_006b;
		}
	}
	{
		IntPtr_t L_11 = { PlayGamesHelperObject_U3CUpdateU3Em__3_m1609_MethodInfo_var };
		Action_1_t394 * L_12 = (Action_1_t394 *)il2cpp_codegen_object_new (Action_1_t394_il2cpp_TypeInfo_var);
		Action_1__ctor_m3737(L_12, NULL, L_11, /*hidden argument*/Action_1__ctor_m3737_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_8 = L_12;
		G_B8_0 = G_B7_0;
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		Action_1_t394 * L_13 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_8;
		NullCheck(G_B8_0);
		List_1_ForEach_m3738(G_B8_0, L_13, /*hidden argument*/List_1_ForEach_m3738_MethodInfo_var);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationFocus(System.Boolean)
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern "C" void PlayGamesHelperObject_OnApplicationFocus_m1605 (PlayGamesHelperObject_t392 * __this, bool ___focused, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		Action_1_t98 * L_0 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sFocusCallback_7;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		Action_1_t98 * L_1 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sFocusCallback_7;
		bool L_2 = ___focused;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, L_2);
	}

IL_0015:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationPause(System.Boolean)
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern "C" void PlayGamesHelperObject_OnApplicationPause_m1606 (PlayGamesHelperObject_t392 * __this, bool ___paused, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		Action_1_t98 * L_0 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sPauseCallback_6;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		Action_1_t98 * L_1 = ((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sPauseCallback_6;
		bool L_2 = ___paused;
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_1, L_2);
	}

IL_0015:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::SetFocusCallback(System.Action`1<System.Boolean>)
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern "C" void PlayGamesHelperObject_SetFocusCallback_m1607 (Object_t * __this /* static, unused */, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t98 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sFocusCallback_7 = L_0;
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::SetPauseCallback(System.Action`1<System.Boolean>)
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern "C" void PlayGamesHelperObject_SetPauseCallback_m1608 (Object_t * __this /* static, unused */, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t98 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t392_StaticFields*)PlayGamesHelperObject_t392_il2cpp_TypeInfo_var->static_fields)->___sPauseCallback_6 = L_0;
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::<Update>m__3(System.Action)
extern "C" void PlayGamesHelperObject_U3CUpdateU3Em__3_m1609 (Object_t * __this /* static, unused */, Action_t588 * ___a, MethodInfo* method)
{
	{
		Action_t588 * L_0 = ___a;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_0);
		return;
	}
}
// GooglePlayGames.Native.CallbackUtils
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUtils.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.CallbackUtils
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUtilsMethodDeclarations.h"



// GooglePlayGames.Native.ConversionUtils
#include "AssemblyU2DCSharp_GooglePlayGames_Native_ConversionUtils.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.ConversionUtils
#include "AssemblyU2DCSharp_GooglePlayGames_Native_ConversionUtilsMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"


// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.Native.ConversionUtils::ConvertResponseStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern TypeInfo* ResponseStatus_t409_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t ConversionUtils_ConvertResponseStatus_m1610 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ResponseStatus_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 0)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 1)
		{
			goto IL_003e;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 2)
		{
			goto IL_0038;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 3)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 4)
		{
			goto IL_0036;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 5)
		{
			goto IL_0041;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 6)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 7)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_0041;
	}

IL_002f:
	{
		return (int32_t)(1);
	}

IL_0031:
	{
		return (int32_t)(2);
	}

IL_0033:
	{
		return (int32_t)(((int32_t)-2));
	}

IL_0036:
	{
		return (int32_t)((-1));
	}

IL_0038:
	{
		return (int32_t)(((int32_t)-3));
	}

IL_003b:
	{
		return (int32_t)(((int32_t)-5));
	}

IL_003e:
	{
		return (int32_t)(((int32_t)-4));
	}

IL_0041:
	{
		int32_t L_2 = ___status;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(ResponseStatus_t409_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral436, L_4, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_6 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_6, L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}
}
// GooglePlayGames.BasicApi.UIStatus GooglePlayGames.Native.ConversionUtils::ConvertUIStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t ConversionUtils_ConvertUIStatus_m1611 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 0)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 1)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 2)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 3)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 4)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 5)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 6)
		{
			goto IL_0056;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 7)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 8)
		{
			goto IL_0053;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 9)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 10)
		{
			goto IL_004a;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 11)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 12)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 13)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_005c;
	}

IL_0048:
	{
		return (int32_t)(1);
	}

IL_004a:
	{
		return (int32_t)(((int32_t)-2));
	}

IL_004d:
	{
		return (int32_t)(((int32_t)-3));
	}

IL_0050:
	{
		return (int32_t)(((int32_t)-5));
	}

IL_0053:
	{
		return (int32_t)(((int32_t)-4));
	}

IL_0056:
	{
		return (int32_t)(((int32_t)-6));
	}

IL_0059:
	{
		return (int32_t)(((int32_t)-12));
	}

IL_005c:
	{
		int32_t L_2 = ___status;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(UIStatus_t412_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral436, L_4, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_6 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_6, L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}
}
// GooglePlayGames.Native.Cwrapper.Types/DataSource GooglePlayGames.Native.ConversionUtils::AsDataSource(GooglePlayGames.BasicApi.DataSource)
extern TypeInfo* DataSource_t334_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" int32_t ConversionUtils_AsDataSource_m1612 (Object_t * __this /* static, unused */, int32_t ___source, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSource_t334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___source;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0018;
	}

IL_0014:
	{
		return (int32_t)(1);
	}

IL_0016:
	{
		return (int32_t)(2);
	}

IL_0018:
	{
		int32_t L_3 = ___source;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(DataSource_t334_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral437, L_5, /*hidden argument*/NULL);
		InvalidOperationException_t905 * L_7 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_7);
	}
}
// GooglePlayGames.Native.Cwrapper.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_AchievemenMethodDeclarations.h"

// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi_0.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi.h"


// System.UInt32 GooglePlayGames.Native.Cwrapper.Achievement::Achievement_TotalSteps(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL Achievement_TotalSteps(void*);}
extern "C" uint32_t Achievement_Achievement_TotalSteps_m1613 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_TotalSteps;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_TotalSteps'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Achievement::Achievement_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Achievement_Description(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Achievement_Achievement_Description_m1614 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_Description;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_Description'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Achievement::Achievement_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL Achievement_Dispose(void*);}
extern "C" void Achievement_Achievement_Dispose_m1615 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Achievement::Achievement_UnlockedIconUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Achievement_UnlockedIconUrl(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Achievement_Achievement_UnlockedIconUrl_m1616 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_UnlockedIconUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_UnlockedIconUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.Achievement::Achievement_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Achievement_LastModifiedTime(void*);}
extern "C" uint64_t Achievement_Achievement_LastModifiedTime_m1617 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_LastModifiedTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_LastModifiedTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Achievement::Achievement_RevealedIconUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Achievement_RevealedIconUrl(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Achievement_Achievement_RevealedIconUrl_m1618 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_RevealedIconUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_RevealedIconUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.Achievement::Achievement_CurrentSteps(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL Achievement_CurrentSteps(void*);}
extern "C" uint32_t Achievement_Achievement_CurrentSteps_m1619 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_CurrentSteps;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_CurrentSteps'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/AchievementState GooglePlayGames.Native.Cwrapper.Achievement::Achievement_State(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL Achievement_State(void*);}
extern "C" int32_t Achievement_Achievement_State_m1620 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_State;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_State'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.Achievement::Achievement_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL Achievement_Valid(void*);}
extern "C" bool Achievement_Achievement_Valid_m1621 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.Achievement::Achievement_LastModified(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Achievement_LastModified(void*);}
extern "C" uint64_t Achievement_Achievement_LastModified_m1622 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_LastModified;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_LastModified'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.Achievement::Achievement_XP(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Achievement_XP(void*);}
extern "C" uint64_t Achievement_Achievement_XP_m1623 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_XP;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_XP'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/AchievementType GooglePlayGames.Native.Cwrapper.Achievement::Achievement_Type(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL Achievement_Type(void*);}
extern "C" int32_t Achievement_Achievement_Type_m1624 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_Type;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_Type'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Achievement::Achievement_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Achievement_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Achievement_Achievement_Id_m1625 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Achievement::Achievement_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Achievement_Name(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Achievement_Achievement_Name_m1626 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Achievement_Name;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Achievement_Name'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_0MethodDeclarations.h"

// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchAllCallback__ctor_m1627 (FetchAllCallback_t398 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchAllCallback_Invoke_m1628 (FetchAllCallback_t398 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchAllCallback_Invoke_m1628((FetchAllCallback_t398 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchAllCallback_t398(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchAllCallback_BeginInvoke_m1629 (FetchAllCallback_t398 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchAllCallback_EndInvoke_m1630 (FetchAllCallback_t398 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchCallback__ctor_m1631 (FetchCallback_t399 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchCallback_Invoke_m1632 (FetchCallback_t399 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchCallback_Invoke_m1632((FetchCallback_t399 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchCallback_t399(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchCallback_BeginInvoke_m1633 (FetchCallback_t399 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchCallback_EndInvoke_m1634 (FetchCallback_t399 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void ShowAllUICallback__ctor_m1635 (ShowAllUICallback_t400 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C" void ShowAllUICallback_Invoke_m1636 (ShowAllUICallback_t400 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ShowAllUICallback_Invoke_m1636((ShowAllUICallback_t400 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ShowAllUICallback_t400(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * ShowAllUICallback_BeginInvoke_m1637 (ShowAllUICallback_t400 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t412_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::EndInvoke(System.IAsyncResult)
extern "C" void ShowAllUICallback_EndInvoke_m1638 (ShowAllUICallback_t400 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.AchievementManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.AchievementManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL AchievementManager_FetchAll(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void AchievementManager_AchievementManager_FetchAll_m1639 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchAllCallback_t398 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchAll;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchAll'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Reveal(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {void DEFAULT_CALL AchievementManager_Reveal(void*, char*);}
extern "C" void AchievementManager_AchievementManager_Reveal_m1640 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_Reveal;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_Reveal'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___achievement_id' to native representation
	char* ____achievement_id_marshaled = { 0 };
	____achievement_id_marshaled = il2cpp_codegen_marshal_string(___achievement_id);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____achievement_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___achievement_id' native representation
	il2cpp_codegen_marshal_free(____achievement_id_marshaled);
	____achievement_id_marshaled = NULL;

}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Unlock(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {void DEFAULT_CALL AchievementManager_Unlock(void*, char*);}
extern "C" void AchievementManager_AchievementManager_Unlock_m1641 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_Unlock;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_Unlock'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___achievement_id' to native representation
	char* ____achievement_id_marshaled = { 0 };
	____achievement_id_marshaled = il2cpp_codegen_marshal_string(___achievement_id);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____achievement_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___achievement_id' native representation
	il2cpp_codegen_marshal_free(____achievement_id_marshaled);
	____achievement_id_marshaled = NULL;

}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_ShowAllUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL AchievementManager_ShowAllUI(void*, methodPointerType, IntPtr_t);}
extern "C" void AchievementManager_AchievementManager_ShowAllUI_m1642 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ShowAllUICallback_t400 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_ShowAllUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_ShowAllUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_SetStepsAtLeast(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32)
extern "C" {void DEFAULT_CALL AchievementManager_SetStepsAtLeast(void*, char*, uint32_t);}
extern "C" void AchievementManager_AchievementManager_SetStepsAtLeast_m1643 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, uint32_t ___steps, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_SetStepsAtLeast;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_SetStepsAtLeast'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___achievement_id' to native representation
	char* ____achievement_id_marshaled = { 0 };
	____achievement_id_marshaled = il2cpp_codegen_marshal_string(___achievement_id);

	// Marshaling of parameter '___steps' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____achievement_id_marshaled, ___steps);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___achievement_id' native representation
	il2cpp_codegen_marshal_free(____achievement_id_marshaled);
	____achievement_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___steps' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Increment(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32)
extern "C" {void DEFAULT_CALL AchievementManager_Increment(void*, char*, uint32_t);}
extern "C" void AchievementManager_AchievementManager_Increment_m1644 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, uint32_t ___steps, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_Increment;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_Increment'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___achievement_id' to native representation
	char* ____achievement_id_marshaled = { 0 };
	____achievement_id_marshaled = il2cpp_codegen_marshal_string(___achievement_id);

	// Marshaling of parameter '___steps' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____achievement_id_marshaled, ___steps);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___achievement_id' native representation
	il2cpp_codegen_marshal_free(____achievement_id_marshaled);
	____achievement_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___steps' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL AchievementManager_Fetch(void*, int32_t, char*, methodPointerType, IntPtr_t);}
extern "C" void AchievementManager_AchievementManager_Fetch_m1645 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___achievement_id, FetchCallback_t399 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_Fetch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_Fetch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___achievement_id' to native representation
	char* ____achievement_id_marshaled = { 0 };
	____achievement_id_marshaled = il2cpp_codegen_marshal_string(___achievement_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____achievement_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___achievement_id' native representation
	il2cpp_codegen_marshal_free(____achievement_id_marshaled);
	____achievement_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL AchievementManager_FetchAllResponse_Dispose(void*);}
extern "C" void AchievementManager_AchievementManager_FetchAllResponse_Dispose_m1646 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchAllResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchAllResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL AchievementManager_FetchAllResponse_GetStatus(void*);}
extern "C" int32_t AchievementManager_AchievementManager_FetchAllResponse_GetStatus_m1647 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchAllResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchAllResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL AchievementManager_FetchAllResponse_GetData_Length(void*);}
extern "C" UIntPtr_t  AchievementManager_AchievementManager_FetchAllResponse_GetData_Length_m1648 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchAllResponse_GetData_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchAllResponse_GetData_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL AchievementManager_FetchAllResponse_GetData_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t AchievementManager_AchievementManager_FetchAllResponse_GetData_GetElement_m1649 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchAllResponse_GetData_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchAllResponse_GetData_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL AchievementManager_FetchResponse_Dispose(void*);}
extern "C" void AchievementManager_AchievementManager_FetchResponse_Dispose_m1650 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL AchievementManager_FetchResponse_GetStatus(void*);}
extern "C" int32_t AchievementManager_AchievementManager_FetchResponse_GetStatus_m1651 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL AchievementManager_FetchResponse_GetData(void*);}
extern "C" IntPtr_t AchievementManager_AchievementManager_FetchResponse_GetData_m1652 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)AchievementManager_FetchResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'AchievementManager_FetchResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_OnMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/LogLevel
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_LogL.h"


// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnLogCallback__ctor_m1653 (OnLogCallback_t402 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/LogLevel,System.String,System.IntPtr)
extern "C" void OnLogCallback_Invoke_m1654 (OnLogCallback_t402 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnLogCallback_Invoke_m1654((OnLogCallback_t402 *)__this->___prev_9,___arg0, ___arg1, ___arg2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnLogCallback_t402(Il2CppObject* delegate, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, char*, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation
	char* ____arg1_marshaled = { 0 };
	____arg1_marshaled = il2cpp_codegen_marshal_string(___arg1);

	// Marshaling of parameter '___arg2' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ____arg1_marshaled, ___arg2);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation
	il2cpp_codegen_marshal_free(____arg1_marshaled);
	____arg1_marshaled = NULL;

	// Marshaling cleanup of parameter '___arg2' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/LogLevel,System.String,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* LogLevel_t503_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnLogCallback_BeginInvoke_m1655 (OnLogCallback_t402 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogLevel_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(646);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(LogLevel_t503_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = ___arg1;
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnLogCallback_EndInvoke_m1656 (OnLogCallback_t402 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_0MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"


// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnAuthActionStartedCallback__ctor_m1657 (OnAuthActionStartedCallback_t403 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr)
extern "C" void OnAuthActionStartedCallback_Invoke_m1658 (OnAuthActionStartedCallback_t403 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnAuthActionStartedCallback_Invoke_m1658((OnAuthActionStartedCallback_t403 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnAuthActionStartedCallback_t403(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* AuthOperation_t504_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnAuthActionStartedCallback_BeginInvoke_m1659 (OnAuthActionStartedCallback_t403 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthOperation_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(647);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AuthOperation_t504_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnAuthActionStartedCallback_EndInvoke_m1660 (OnAuthActionStartedCallback_t403 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_1MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0.h"


// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnAuthActionFinishedCallback__ctor_m1661 (OnAuthActionFinishedCallback_t404 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr)
extern "C" void OnAuthActionFinishedCallback_Invoke_m1662 (OnAuthActionFinishedCallback_t404 * __this, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnAuthActionFinishedCallback_Invoke_m1662((OnAuthActionFinishedCallback_t404 *)__this->___prev_9,___arg0, ___arg1, ___arg2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnAuthActionFinishedCallback_t404(Il2CppObject* delegate, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Marshaling of parameter '___arg2' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1, ___arg2);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

	// Marshaling cleanup of parameter '___arg2' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* AuthOperation_t504_il2cpp_TypeInfo_var;
extern TypeInfo* AuthStatus_t411_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnAuthActionFinishedCallback_BeginInvoke_m1663 (OnAuthActionFinishedCallback_t404 * __this, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthOperation_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(647);
		AuthStatus_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(AuthOperation_t504_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(AuthStatus_t411_il2cpp_TypeInfo_var, &___arg1);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnAuthActionFinishedCallback_EndInvoke_m1664 (OnAuthActionFinishedCallback_t404 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_2MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"


// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnMultiplayerInvitationEventCallback__ctor_m1665 (OnMultiplayerInvitationEventCallback_t405 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
extern "C" void OnMultiplayerInvitationEventCallback_Invoke_m1666 (OnMultiplayerInvitationEventCallback_t405 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnMultiplayerInvitationEventCallback_Invoke_m1666((OnMultiplayerInvitationEventCallback_t405 *)__this->___prev_9,___arg0, ___arg1, ___arg2, ___arg3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2, ___arg3,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2, ___arg3,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnMultiplayerInvitationEventCallback_t405(Il2CppObject* delegate, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, char*, IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation
	char* ____arg1_marshaled = { 0 };
	____arg1_marshaled = il2cpp_codegen_marshal_string(___arg1);

	// Marshaling of parameter '___arg2' to native representation

	// Marshaling of parameter '___arg3' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ____arg1_marshaled, ___arg2, ___arg3);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation
	il2cpp_codegen_marshal_free(____arg1_marshaled);
	____arg1_marshaled = NULL;

	// Marshaling cleanup of parameter '___arg2' native representation

	// Marshaling cleanup of parameter '___arg3' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* MultiplayerEvent_t518_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnMultiplayerInvitationEventCallback_BeginInvoke_m1667 (OnMultiplayerInvitationEventCallback_t405 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerEvent_t518_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(649);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(MultiplayerEvent_t518_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = ___arg1;
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	__d_args[3] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg3);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnMultiplayerInvitationEventCallback_EndInvoke_m1668 (OnMultiplayerInvitationEventCallback_t405 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnTurnBasedMatchEventCallback__ctor_m1669 (OnTurnBasedMatchEventCallback_t406 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
extern "C" void OnTurnBasedMatchEventCallback_Invoke_m1670 (OnTurnBasedMatchEventCallback_t406 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnTurnBasedMatchEventCallback_Invoke_m1670((OnTurnBasedMatchEventCallback_t406 *)__this->___prev_9,___arg0, ___arg1, ___arg2, ___arg3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2, ___arg3,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2, ___arg3,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnTurnBasedMatchEventCallback_t406(Il2CppObject* delegate, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, char*, IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation
	char* ____arg1_marshaled = { 0 };
	____arg1_marshaled = il2cpp_codegen_marshal_string(___arg1);

	// Marshaling of parameter '___arg2' to native representation

	// Marshaling of parameter '___arg3' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ____arg1_marshaled, ___arg2, ___arg3);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation
	il2cpp_codegen_marshal_free(____arg1_marshaled);
	____arg1_marshaled = NULL;

	// Marshaling cleanup of parameter '___arg2' native representation

	// Marshaling cleanup of parameter '___arg3' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* MultiplayerEvent_t518_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnTurnBasedMatchEventCallback_BeginInvoke_m1671 (OnTurnBasedMatchEventCallback_t406 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerEvent_t518_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(649);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(MultiplayerEvent_t518_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = ___arg1;
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	__d_args[3] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg3);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnTurnBasedMatchEventCallback_EndInvoke_m1672 (OnTurnBasedMatchEventCallback_t406 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_4MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnQuestCompletedCallback__ctor_m1673 (OnQuestCompletedCallback_t407 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void OnQuestCompletedCallback_Invoke_m1674 (OnQuestCompletedCallback_t407 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnQuestCompletedCallback_Invoke_m1674((OnQuestCompletedCallback_t407 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnQuestCompletedCallback_t407(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnQuestCompletedCallback_BeginInvoke_m1675 (OnQuestCompletedCallback_t407 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnQuestCompletedCallback_EndInvoke_m1676 (OnQuestCompletedCallback_t407 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.Builder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Builder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_BuilderMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnAuthActionStarted(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetOnAuthActionStarted(void*, methodPointerType, IntPtr_t);}
extern "C" void Builder_GameServices_Builder_SetOnAuthActionStarted_m1677 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnAuthActionStartedCallback_t403 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetOnAuthActionStarted;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetOnAuthActionStarted'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_AddOauthScope(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {void DEFAULT_CALL GameServices_Builder_AddOauthScope(void*, char*);}
extern "C" void Builder_GameServices_Builder_AddOauthScope_m1678 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___scope, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_AddOauthScope;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_AddOauthScope'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___scope' to native representation
	char* ____scope_marshaled = { 0 };
	____scope_marshaled = il2cpp_codegen_marshal_string(___scope);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____scope_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___scope' native representation
	il2cpp_codegen_marshal_free(____scope_marshaled);
	____scope_marshaled = NULL;

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetLogging(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback,System.IntPtr,GooglePlayGames.Native.Cwrapper.Types/LogLevel)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetLogging(void*, methodPointerType, IntPtr_t, int32_t);}
extern "C" void Builder_GameServices_Builder_SetLogging_m1679 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnLogCallback_t402 * ___callback, IntPtr_t ___callback_arg, int32_t ___min_level, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetLogging;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetLogging'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Marshaling of parameter '___min_level' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg, ___min_level);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

	// Marshaling cleanup of parameter '___min_level' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_Construct()
extern "C" {IntPtr_t DEFAULT_CALL GameServices_Builder_Construct();}
extern "C" IntPtr_t Builder_GameServices_Builder_Construct_m1680 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_Construct;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_Construct'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_EnableSnapshots(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL GameServices_Builder_EnableSnapshots(void*);}
extern "C" void Builder_GameServices_Builder_EnableSnapshots_m1681 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_EnableSnapshots;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_EnableSnapshots'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnLog(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback,System.IntPtr,GooglePlayGames.Native.Cwrapper.Types/LogLevel)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetOnLog(void*, methodPointerType, IntPtr_t, int32_t);}
extern "C" void Builder_GameServices_Builder_SetOnLog_m1682 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnLogCallback_t402 * ___callback, IntPtr_t ___callback_arg, int32_t ___min_level, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetOnLog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetOnLog'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Marshaling of parameter '___min_level' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg, ___min_level);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

	// Marshaling cleanup of parameter '___min_level' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetDefaultOnLog(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/LogLevel)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetDefaultOnLog(void*, int32_t);}
extern "C" void Builder_GameServices_Builder_SetDefaultOnLog_m1683 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___min_level, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetDefaultOnLog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetDefaultOnLog'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___min_level' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___min_level);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___min_level' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnAuthActionFinished(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetOnAuthActionFinished(void*, methodPointerType, IntPtr_t);}
extern "C" void Builder_GameServices_Builder_SetOnAuthActionFinished_m1684 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnAuthActionFinishedCallback_t404 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetOnAuthActionFinished;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetOnAuthActionFinished'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnTurnBasedMatchEvent(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetOnTurnBasedMatchEvent(void*, methodPointerType, IntPtr_t);}
extern "C" void Builder_GameServices_Builder_SetOnTurnBasedMatchEvent_m1685 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnTurnBasedMatchEventCallback_t406 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetOnTurnBasedMatchEvent;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetOnTurnBasedMatchEvent'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnQuestCompleted(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetOnQuestCompleted(void*, methodPointerType, IntPtr_t);}
extern "C" void Builder_GameServices_Builder_SetOnQuestCompleted_m1686 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnQuestCompletedCallback_t407 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetOnQuestCompleted;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetOnQuestCompleted'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnMultiplayerInvitationEvent(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL GameServices_Builder_SetOnMultiplayerInvitationEvent(void*, methodPointerType, IntPtr_t);}
extern "C" void Builder_GameServices_Builder_SetOnMultiplayerInvitationEvent_m1687 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnMultiplayerInvitationEventCallback_t405 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_SetOnMultiplayerInvitationEvent;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_SetOnMultiplayerInvitationEvent'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_Create(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {IntPtr_t DEFAULT_CALL GameServices_Builder_Create(void*, IntPtr_t);}
extern "C" IntPtr_t Builder_GameServices_Builder_Create_m1688 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___platform, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_Create;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_Create'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___platform' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___platform);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___platform' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL GameServices_Builder_Dispose(void*);}
extern "C" void Builder_GameServices_Builder_Dispose_m1689 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Builder_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Builder_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/FlushStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/FlushStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_2MethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0MethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErroMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_4MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_5.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_5MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_6.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_6MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.CommonErrorStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_7.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_7MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Event
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Event.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Event
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Even.h"


// System.UInt64 GooglePlayGames.Native.Cwrapper.Event::Event_Count(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Event_Count(void*);}
extern "C" uint64_t Event_Event_Count_m1690 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Count;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Count'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Event_Description(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Event_Event_Description_m1691 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Description;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Description'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_ImageUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Event_ImageUrl(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Event_Event_ImageUrl_m1692 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_ImageUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_ImageUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/EventVisibility GooglePlayGames.Native.Cwrapper.Event::Event_Visibility(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL Event_Visibility(void*);}
extern "C" int32_t Event_Event_Visibility_m1693 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Visibility;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Visibility'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Event_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Event_Event_Id_m1694 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.Event::Event_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL Event_Valid(void*);}
extern "C" bool Event_Event_Valid_m1695 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Event::Event_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL Event_Dispose(void*);}
extern "C" void Event_Event_Dispose_m1696 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Copy(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL Event_Copy(void*);}
extern "C" IntPtr_t Event_Event_Copy_m1697 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Copy;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Copy'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Event_Name(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Event_Event_Name_m1698 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Event_Name;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Event_Name'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManagMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchAllCallback__ctor_m1699 (FetchAllCallback_t419 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchAllCallback_Invoke_m1700 (FetchAllCallback_t419 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchAllCallback_Invoke_m1700((FetchAllCallback_t419 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchAllCallback_t419(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchAllCallback_BeginInvoke_m1701 (FetchAllCallback_t419 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchAllCallback_EndInvoke_m1702 (FetchAllCallback_t419 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchCallback__ctor_m1703 (FetchCallback_t420 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchCallback_Invoke_m1704 (FetchCallback_t420 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchCallback_Invoke_m1704((FetchCallback_t420 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchCallback_t420(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchCallback_BeginInvoke_m1705 (FetchCallback_t420 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchCallback_EndInvoke_m1706 (FetchCallback_t420 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.EventManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.EventManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL EventManager_FetchAll(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void EventManager_EventManager_FetchAll_m1707 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchAllCallback_t419 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_FetchAll;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_FetchAll'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL EventManager_Fetch(void*, int32_t, char*, methodPointerType, IntPtr_t);}
extern "C" void EventManager_EventManager_Fetch_m1708 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___event_id, FetchCallback_t420 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_Fetch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_Fetch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___event_id' to native representation
	char* ____event_id_marshaled = { 0 };
	____event_id_marshaled = il2cpp_codegen_marshal_string(___event_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____event_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___event_id' native representation
	il2cpp_codegen_marshal_free(____event_id_marshaled);
	____event_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_Increment(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32)
extern "C" {void DEFAULT_CALL EventManager_Increment(void*, char*, uint32_t);}
extern "C" void EventManager_EventManager_Increment_m1709 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___event_id, uint32_t ___steps, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_Increment;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_Increment'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___event_id' to native representation
	char* ____event_id_marshaled = { 0 };
	____event_id_marshaled = il2cpp_codegen_marshal_string(___event_id);

	// Marshaling of parameter '___steps' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____event_id_marshaled, ___steps);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___event_id' native representation
	il2cpp_codegen_marshal_free(____event_id_marshaled);
	____event_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___steps' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL EventManager_FetchAllResponse_Dispose(void*);}
extern "C" void EventManager_EventManager_FetchAllResponse_Dispose_m1710 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_FetchAllResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_FetchAllResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL EventManager_FetchAllResponse_GetStatus(void*);}
extern "C" int32_t EventManager_EventManager_FetchAllResponse_GetStatus_m1711 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_FetchAllResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_FetchAllResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_GetData(System.Runtime.InteropServices.HandleRef,System.IntPtr[],System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL EventManager_FetchAllResponse_GetData(void*, IntPtr_t*, UIntPtr_t );}
extern "C" UIntPtr_t  EventManager_EventManager_FetchAllResponse_GetData_m1712 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtrU5BU5D_t859* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_FetchAllResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_FetchAllResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	IntPtr_t* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_array<IntPtr_t>((Il2CppCodeGenArray*)___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___out_arg' native representation

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL EventManager_FetchResponse_Dispose(void*);}
extern "C" void EventManager_EventManager_FetchResponse_Dispose_m1713 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_FetchResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_FetchResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL EventManager_FetchResponse_GetStatus(void*);}
extern "C" int32_t EventManager_EventManager_FetchResponse_GetStatus_m1714 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_FetchResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_FetchResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL EventManager_FetchResponse_GetData(void*);}
extern "C" IntPtr_t EventManager_EventManager_FetchResponse_GetData_m1715 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EventManager_FetchResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EventManager_FetchResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_GameServic.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_GameServicMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FlushCallback__ctor_m1716 (FlushCallback_t422 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/FlushStatus,System.IntPtr)
extern "C" void FlushCallback_Invoke_m1717 (FlushCallback_t422 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FlushCallback_Invoke_m1717((FlushCallback_t422 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FlushCallback_t422(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/FlushStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* FlushStatus_t410_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FlushCallback_BeginInvoke_m1718 (FlushCallback_t422 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlushStatus_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(650);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FlushStatus_t410_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::EndInvoke(System.IAsyncResult)
extern "C" void FlushCallback_EndInvoke_m1719 (FlushCallback_t422 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.GameServices
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_GameServic_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.GameServices
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_GameServic_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_Flush(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL GameServices_Flush(void*, methodPointerType, IntPtr_t);}
extern "C" void GameServices_GameServices_Flush_m1720 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, FlushCallback_t422 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Flush;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Flush'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Boolean GooglePlayGames.Native.Cwrapper.GameServices::GameServices_IsAuthorized(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL GameServices_IsAuthorized(void*);}
extern "C" bool GameServices_GameServices_IsAuthorized_m1721 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_IsAuthorized;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_IsAuthorized'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL GameServices_Dispose(void*);}
extern "C" void GameServices_GameServices_Dispose_m1722 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_SignOut(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL GameServices_SignOut(void*);}
extern "C" void GameServices_GameServices_SignOut_m1723 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_SignOut;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_SignOut'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_StartAuthorizationUI(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL GameServices_StartAuthorizationUI(void*);}
extern "C" void GameServices_GameServices_StartAuthorizationUI_m1724 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GameServices_StartAuthorizationUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GameServices_StartAuthorizationUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.InternalHooks
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_InternalHo.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.InternalHooks
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_InternalHoMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.InternalHooks::InternalHooks_ConfigureForUnityPlugin(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL InternalHooks_ConfigureForUnityPlugin(void*);}
extern "C" void InternalHooks_InternalHooks_ConfigureForUnityPlugin_m1725 (Object_t * __this /* static, unused */, HandleRef_t657  ___builder, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)InternalHooks_ConfigureForUnityPlugin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'InternalHooks_ConfigureForUnityPlugin'"));
		}
	}

	// Marshaling of parameter '___builder' to native representation
	void* ____builder_marshaled = { 0 };
	____builder_marshaled = ___builder.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____builder_marshaled);

	// Marshaling cleanup of parameter '___builder' native representation

}
// GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_IosPlatfor.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_IosPlatforMethodDeclarations.h"



// System.IntPtr GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Construct()
extern "C" {IntPtr_t DEFAULT_CALL IosPlatformConfiguration_Construct();}
extern "C" IntPtr_t IosPlatformConfiguration_IosPlatformConfiguration_Construct_m1726 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)IosPlatformConfiguration_Construct;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'IosPlatformConfiguration_Construct'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL IosPlatformConfiguration_Dispose(void*);}
extern "C" void IosPlatformConfiguration_IosPlatformConfiguration_Dispose_m1727 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)IosPlatformConfiguration_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'IosPlatformConfiguration_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Boolean GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL IosPlatformConfiguration_Valid(void*);}
extern "C" bool IosPlatformConfiguration_IosPlatformConfiguration_Valid_m1728 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)IosPlatformConfiguration_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'IosPlatformConfiguration_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_SetClientID(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {void DEFAULT_CALL IosPlatformConfiguration_SetClientID(void*, char*);}
extern "C" void IosPlatformConfiguration_IosPlatformConfiguration_SetClientID_m1729 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___client_id, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)IosPlatformConfiguration_SetClientID;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'IosPlatformConfiguration_SetClientID'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___client_id' to native representation
	char* ____client_id_marshaled = { 0 };
	____client_id_marshaled = il2cpp_codegen_marshal_string(___client_id);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____client_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___client_id' native representation
	il2cpp_codegen_marshal_free(____client_id_marshaled);
	____client_id_marshaled = NULL;

}
// GooglePlayGames.Native.Cwrapper.Leaderboard
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Leaderboard
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_LeaderboarMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead.h"


// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Leaderboard_Name(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Leaderboard_Leaderboard_Name_m1730 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Leaderboard_Name;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Leaderboard_Name'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Leaderboard_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Leaderboard_Leaderboard_Id_m1731 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Leaderboard_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Leaderboard_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_IconUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Leaderboard_IconUrl(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Leaderboard_Leaderboard_IconUrl_m1732 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Leaderboard_IconUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Leaderboard_IconUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL Leaderboard_Dispose(void*);}
extern "C" void Leaderboard_Leaderboard_Dispose_m1733 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Leaderboard_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Leaderboard_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Boolean GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL Leaderboard_Valid(void*);}
extern "C" bool Leaderboard_Leaderboard_Valid_m1734 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Leaderboard_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Leaderboard_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Order(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL Leaderboard_Order(void*);}
extern "C" int32_t Leaderboard_Leaderboard_Order_m1735 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Leaderboard_Order;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Leaderboard_Order'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchCallback__ctor_m1736 (FetchCallback_t427 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchCallback_Invoke_m1737 (FetchCallback_t427 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchCallback_Invoke_m1737((FetchCallback_t427 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchCallback_t427(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchCallback_BeginInvoke_m1738 (FetchCallback_t427 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchCallback_EndInvoke_m1739 (FetchCallback_t427 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchAllCallback__ctor_m1740 (FetchAllCallback_t428 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchAllCallback_Invoke_m1741 (FetchAllCallback_t428 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchAllCallback_Invoke_m1741((FetchAllCallback_t428 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchAllCallback_t428(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchAllCallback_BeginInvoke_m1742 (FetchAllCallback_t428 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchAllCallback_EndInvoke_m1743 (FetchAllCallback_t428 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchScorePageCallback__ctor_m1744 (FetchScorePageCallback_t429 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchScorePageCallback_Invoke_m1745 (FetchScorePageCallback_t429 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchScorePageCallback_Invoke_m1745((FetchScorePageCallback_t429 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchScorePageCallback_t429(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchScorePageCallback_BeginInvoke_m1746 (FetchScorePageCallback_t429 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchScorePageCallback_EndInvoke_m1747 (FetchScorePageCallback_t429 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchScoreSummaryCallback__ctor_m1748 (FetchScoreSummaryCallback_t430 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchScoreSummaryCallback_Invoke_m1749 (FetchScoreSummaryCallback_t430 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchScoreSummaryCallback_Invoke_m1749((FetchScoreSummaryCallback_t430 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchScoreSummaryCallback_t430(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchScoreSummaryCallback_BeginInvoke_m1750 (FetchScoreSummaryCallback_t430 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchScoreSummaryCallback_EndInvoke_m1751 (FetchScoreSummaryCallback_t430 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_4MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchAllScoreSummariesCallback__ctor_m1752 (FetchAllScoreSummariesCallback_t431 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchAllScoreSummariesCallback_Invoke_m1753 (FetchAllScoreSummariesCallback_t431 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchAllScoreSummariesCallback_Invoke_m1753((FetchAllScoreSummariesCallback_t431 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchAllScoreSummariesCallback_t431(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchAllScoreSummariesCallback_BeginInvoke_m1754 (FetchAllScoreSummariesCallback_t431 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchAllScoreSummariesCallback_EndInvoke_m1755 (FetchAllScoreSummariesCallback_t431 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_5.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_5MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void ShowAllUICallback__ctor_m1756 (ShowAllUICallback_t432 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C" void ShowAllUICallback_Invoke_m1757 (ShowAllUICallback_t432 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ShowAllUICallback_Invoke_m1757((ShowAllUICallback_t432 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ShowAllUICallback_t432(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * ShowAllUICallback_BeginInvoke_m1758 (ShowAllUICallback_t432 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t412_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::EndInvoke(System.IAsyncResult)
extern "C" void ShowAllUICallback_EndInvoke_m1759 (ShowAllUICallback_t432 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_6.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_6MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void ShowUICallback__ctor_m1760 (ShowUICallback_t433 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C" void ShowUICallback_Invoke_m1761 (ShowUICallback_t433 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ShowUICallback_Invoke_m1761((ShowUICallback_t433 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ShowUICallback_t433(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * ShowUICallback_BeginInvoke_m1762 (ShowUICallback_t433 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t412_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::EndInvoke(System.IAsyncResult)
extern "C" void ShowUICallback_EndInvoke_m1763 (ShowUICallback_t433 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.LeaderboardManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_7.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.LeaderboardManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Leaderboar_7MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_0.h"


// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchAll(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchAll_m1764 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchAllCallback_t428 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAll;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAll'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummary(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan,GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchScoreSummary(void*, int32_t, char*, int32_t, int32_t, methodPointerType, IntPtr_t);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchScoreSummary_m1765 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___leaderboard_id, int32_t ___time_span, int32_t ___collection, FetchScoreSummaryCallback_t430 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, int32_t, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScoreSummary;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScoreSummary'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___leaderboard_id' to native representation
	char* ____leaderboard_id_marshaled = { 0 };
	____leaderboard_id_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id);

	// Marshaling of parameter '___time_span' to native representation

	// Marshaling of parameter '___collection' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____leaderboard_id_marshaled, ___time_span, ___collection, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___leaderboard_id' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id_marshaled);
	____leaderboard_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___time_span' native representation

	// Marshaling cleanup of parameter '___collection' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ScorePageToken(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart,GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan,GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection)
extern "C" {IntPtr_t DEFAULT_CALL LeaderboardManager_ScorePageToken(void*, char*, int32_t, int32_t, int32_t);}
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_ScorePageToken_m1766 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___leaderboard_id, int32_t ___start, int32_t ___time_span, int32_t ___collection, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, int32_t, int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_ScorePageToken;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_ScorePageToken'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___leaderboard_id' to native representation
	char* ____leaderboard_id_marshaled = { 0 };
	____leaderboard_id_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id);

	// Marshaling of parameter '___start' to native representation

	// Marshaling of parameter '___time_span' to native representation

	// Marshaling of parameter '___collection' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____leaderboard_id_marshaled, ___start, ___time_span, ___collection);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___leaderboard_id' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id_marshaled);
	____leaderboard_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___start' native representation

	// Marshaling cleanup of parameter '___time_span' native representation

	// Marshaling cleanup of parameter '___collection' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ShowAllUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL LeaderboardManager_ShowAllUI(void*, methodPointerType, IntPtr_t);}
extern "C" void LeaderboardManager_LeaderboardManager_ShowAllUI_m1767 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ShowAllUICallback_t432 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_ShowAllUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_ShowAllUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePage(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.IntPtr,System.UInt32,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchScorePage(void*, int32_t, IntPtr_t, uint32_t, methodPointerType, IntPtr_t);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchScorePage_m1768 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, IntPtr_t ___token, uint32_t ___max_results, FetchScorePageCallback_t429 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, IntPtr_t, uint32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScorePage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScorePage'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___token' to native representation

	// Marshaling of parameter '___max_results' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ___token, ___max_results, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___token' native representation

	// Marshaling cleanup of parameter '___max_results' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummaries(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchAllScoreSummaries(void*, int32_t, char*, methodPointerType, IntPtr_t);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchAllScoreSummaries_m1769 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___leaderboard_id, FetchAllScoreSummariesCallback_t431 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllScoreSummaries;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllScoreSummaries'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___leaderboard_id' to native representation
	char* ____leaderboard_id_marshaled = { 0 };
	____leaderboard_id_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____leaderboard_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___leaderboard_id' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id_marshaled);
	____leaderboard_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ShowUI(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL LeaderboardManager_ShowUI(void*, char*, methodPointerType, IntPtr_t);}
extern "C" void LeaderboardManager_LeaderboardManager_ShowUI_m1770 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___leaderboard_id, ShowUICallback_t433 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_ShowUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_ShowUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___leaderboard_id' to native representation
	char* ____leaderboard_id_marshaled = { 0 };
	____leaderboard_id_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____leaderboard_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___leaderboard_id' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id_marshaled);
	____leaderboard_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL LeaderboardManager_Fetch(void*, int32_t, char*, methodPointerType, IntPtr_t);}
extern "C" void LeaderboardManager_LeaderboardManager_Fetch_m1771 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___leaderboard_id, FetchCallback_t427 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_Fetch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_Fetch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___leaderboard_id' to native representation
	char* ____leaderboard_id_marshaled = { 0 };
	____leaderboard_id_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____leaderboard_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___leaderboard_id' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id_marshaled);
	____leaderboard_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_SubmitScore(System.Runtime.InteropServices.HandleRef,System.String,System.UInt64,System.String)
extern "C" {void DEFAULT_CALL LeaderboardManager_SubmitScore(void*, char*, uint64_t, char*);}
extern "C" void LeaderboardManager_LeaderboardManager_SubmitScore_m1772 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___leaderboard_id, uint64_t ___score, String_t* ___metadata, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint64_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_SubmitScore;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_SubmitScore'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___leaderboard_id' to native representation
	char* ____leaderboard_id_marshaled = { 0 };
	____leaderboard_id_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id);

	// Marshaling of parameter '___score' to native representation

	// Marshaling of parameter '___metadata' to native representation
	char* ____metadata_marshaled = { 0 };
	____metadata_marshaled = il2cpp_codegen_marshal_string(___metadata);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____leaderboard_id_marshaled, ___score, ____metadata_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___leaderboard_id' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id_marshaled);
	____leaderboard_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___score' native representation

	// Marshaling cleanup of parameter '___metadata' native representation
	il2cpp_codegen_marshal_free(____metadata_marshaled);
	____metadata_marshaled = NULL;

}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchResponse_Dispose(void*);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchResponse_Dispose_m1773 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL LeaderboardManager_FetchResponse_GetStatus(void*);}
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchResponse_GetStatus_m1774 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL LeaderboardManager_FetchResponse_GetData(void*);}
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchResponse_GetData_m1775 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchAllResponse_Dispose(void*);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchAllResponse_Dispose_m1776 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL LeaderboardManager_FetchAllResponse_GetStatus(void*);}
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchAllResponse_GetStatus_m1777 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL LeaderboardManager_FetchAllResponse_GetData_Length(void*);}
extern "C" UIntPtr_t  LeaderboardManager_LeaderboardManager_FetchAllResponse_GetData_Length_m1778 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllResponse_GetData_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllResponse_GetData_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL LeaderboardManager_FetchAllResponse_GetData_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchAllResponse_GetData_GetElement_m1779 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllResponse_GetData_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllResponse_GetData_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchScorePageResponse_Dispose(void*);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchScorePageResponse_Dispose_m1780 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScorePageResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScorePageResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL LeaderboardManager_FetchScorePageResponse_GetStatus(void*);}
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchScorePageResponse_GetStatus_m1781 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScorePageResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScorePageResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL LeaderboardManager_FetchScorePageResponse_GetData(void*);}
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchScorePageResponse_GetData_m1782 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScorePageResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScorePageResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchScoreSummaryResponse_Dispose(void*);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_Dispose_m1783 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScoreSummaryResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScoreSummaryResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL LeaderboardManager_FetchScoreSummaryResponse_GetStatus(void*);}
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_GetStatus_m1784 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScoreSummaryResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScoreSummaryResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL LeaderboardManager_FetchScoreSummaryResponse_GetData(void*);}
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_GetData_m1785 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchScoreSummaryResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchScoreSummaryResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_Dispose(void*);}
extern "C" void LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_Dispose_m1786 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllScoreSummariesResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllScoreSummariesResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus(void*);}
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus_m1787 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length(void*);}
extern "C" UIntPtr_t  LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length_m1788 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement_m1789 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.MultiplayerInvitation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Multiplaye.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.MultiplayerInvitation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_MultiplayeMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0.h"


// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL MultiplayerInvitation_AutomatchingSlotsAvailable(void*);}
extern "C" uint32_t MultiplayerInvitation_MultiplayerInvitation_AutomatchingSlotsAvailable_m1790 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_AutomatchingSlotsAvailable;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_AutomatchingSlotsAvailable'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_InvitingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL MultiplayerInvitation_InvitingParticipant(void*);}
extern "C" IntPtr_t MultiplayerInvitation_MultiplayerInvitation_InvitingParticipant_m1791 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_InvitingParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_InvitingParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL MultiplayerInvitation_Variant(void*);}
extern "C" uint32_t MultiplayerInvitation_MultiplayerInvitation_Variant_m1792 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_Variant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_Variant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL MultiplayerInvitation_CreationTime(void*);}
extern "C" uint64_t MultiplayerInvitation_MultiplayerInvitation_CreationTime_m1793 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_CreationTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_CreationTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL MultiplayerInvitation_Participants_Length(void*);}
extern "C" UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Participants_Length_m1794 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_Participants_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_Participants_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL MultiplayerInvitation_Participants_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t MultiplayerInvitation_MultiplayerInvitation_Participants_GetElement_m1795 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_Participants_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_Participants_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL MultiplayerInvitation_Valid(void*);}
extern "C" bool MultiplayerInvitation_MultiplayerInvitation_Valid_m1796 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Type(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL MultiplayerInvitation_Type(void*);}
extern "C" int32_t MultiplayerInvitation_MultiplayerInvitation_Type_m1797 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_Type;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_Type'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL MultiplayerInvitation_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Id_m1798 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL MultiplayerInvitation_Dispose(void*);}
extern "C" void MultiplayerInvitation_MultiplayerInvitation_Dispose_m1799 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerInvitation_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerInvitation_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.MultiplayerParticipant
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Multiplaye_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.MultiplayerParticipant
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Multiplaye_0MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Imag.h"
// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc.h"


// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Status(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL MultiplayerParticipant_Status(void*);}
extern "C" int32_t MultiplayerParticipant_MultiplayerParticipant_Status_m1800 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_Status;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_Status'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchRank(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL MultiplayerParticipant_MatchRank(void*);}
extern "C" uint32_t MultiplayerParticipant_MultiplayerParticipant_MatchRank_m1801 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_MatchRank;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_MatchRank'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_IsConnectedToRoom(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL MultiplayerParticipant_IsConnectedToRoom(void*);}
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_IsConnectedToRoom_m1802 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_IsConnectedToRoom;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_IsConnectedToRoom'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_DisplayName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL MultiplayerParticipant_DisplayName(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_DisplayName_m1803 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_DisplayName;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_DisplayName'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasPlayer(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL MultiplayerParticipant_HasPlayer(void*);}
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_HasPlayer_m1804 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_HasPlayer;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_HasPlayer'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL MultiplayerParticipant_AvatarUrl(void*, int32_t, char*, UIntPtr_t );}
extern "C" UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_AvatarUrl_m1805 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___resolution, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_AvatarUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_AvatarUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___resolution' to native representation

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___resolution, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___resolution' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchResult(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL MultiplayerParticipant_MatchResult(void*);}
extern "C" int32_t MultiplayerParticipant_MultiplayerParticipant_MatchResult_m1806 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_MatchResult;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_MatchResult'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Player(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL MultiplayerParticipant_Player(void*);}
extern "C" IntPtr_t MultiplayerParticipant_MultiplayerParticipant_Player_m1807 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_Player;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_Player'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL MultiplayerParticipant_Dispose(void*);}
extern "C" void MultiplayerParticipant_MultiplayerParticipant_Dispose_m1808 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL MultiplayerParticipant_Valid(void*);}
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_Valid_m1809 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasMatchResult(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL MultiplayerParticipant_HasMatchResult(void*);}
extern "C" bool MultiplayerParticipant_MultiplayerParticipant_HasMatchResult_m1810 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_HasMatchResult;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_HasMatchResult'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL MultiplayerParticipant_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_Id_m1811 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)MultiplayerParticipant_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'MultiplayerParticipant_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.ParticipantResults
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Participan.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.ParticipantResults
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_ParticipanMethodDeclarations.h"



// System.IntPtr GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_WithResult(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32,GooglePlayGames.Native.Cwrapper.Types/MatchResult)
extern "C" {IntPtr_t DEFAULT_CALL ParticipantResults_WithResult(void*, char*, uint32_t, int32_t);}
extern "C" IntPtr_t ParticipantResults_ParticipantResults_WithResult_m1812 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___participant_id, uint32_t ___placing, int32_t ___result, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ParticipantResults_WithResult;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ParticipantResults_WithResult'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___participant_id' to native representation
	char* ____participant_id_marshaled = { 0 };
	____participant_id_marshaled = il2cpp_codegen_marshal_string(___participant_id);

	// Marshaling of parameter '___placing' to native representation

	// Marshaling of parameter '___result' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____participant_id_marshaled, ___placing, ___result);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___participant_id' native representation
	il2cpp_codegen_marshal_free(____participant_id_marshaled);
	____participant_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___placing' native representation

	// Marshaling cleanup of parameter '___result' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL ParticipantResults_Valid(void*);}
extern "C" bool ParticipantResults_ParticipantResults_Valid_m1813 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ParticipantResults_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ParticipantResults_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_MatchResultForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {int32_t DEFAULT_CALL ParticipantResults_MatchResultForParticipant(void*, char*);}
extern "C" int32_t ParticipantResults_ParticipantResults_MatchResultForParticipant_m1814 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___participant_id, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ParticipantResults_MatchResultForParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ParticipantResults_MatchResultForParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___participant_id' to native representation
	char* ____participant_id_marshaled = { 0 };
	____participant_id_marshaled = il2cpp_codegen_marshal_string(___participant_id);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____participant_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___participant_id' native representation
	il2cpp_codegen_marshal_free(____participant_id_marshaled);
	____participant_id_marshaled = NULL;

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_PlaceForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {uint32_t DEFAULT_CALL ParticipantResults_PlaceForParticipant(void*, char*);}
extern "C" uint32_t ParticipantResults_ParticipantResults_PlaceForParticipant_m1815 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___participant_id, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ParticipantResults_PlaceForParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ParticipantResults_PlaceForParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___participant_id' to native representation
	char* ____participant_id_marshaled = { 0 };
	____participant_id_marshaled = il2cpp_codegen_marshal_string(___participant_id);

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____participant_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___participant_id' native representation
	il2cpp_codegen_marshal_free(____participant_id_marshaled);
	____participant_id_marshaled = NULL;

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_HasResultsForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {int8_t DEFAULT_CALL ParticipantResults_HasResultsForParticipant(void*, char*);}
extern "C" bool ParticipantResults_ParticipantResults_HasResultsForParticipant_m1816 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___participant_id, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ParticipantResults_HasResultsForParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ParticipantResults_HasResultsForParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___participant_id' to native representation
	char* ____participant_id_marshaled = { 0 };
	____participant_id_marshaled = il2cpp_codegen_marshal_string(___participant_id);

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____participant_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___participant_id' native representation
	il2cpp_codegen_marshal_free(____participant_id_marshaled);
	____participant_id_marshaled = NULL;

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL ParticipantResults_Dispose(void*);}
extern "C" void ParticipantResults_ParticipantResults_Dispose_m1817 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ParticipantResults_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ParticipantResults_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.Player
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Player.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Player
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMethodDeclarations.h"



// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_CurrentLevel(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL Player_CurrentLevel(void*);}
extern "C" IntPtr_t Player_Player_CurrentLevel_m1818 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_CurrentLevel;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_CurrentLevel'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Player_Name(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Player_Player_Name_m1819 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_Name;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_Name'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Player::Player_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL Player_Dispose(void*);}
extern "C" void Player_Player_Dispose_m1820 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Player_AvatarUrl(void*, int32_t, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Player_Player_AvatarUrl_m1821 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___resolution, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_AvatarUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_AvatarUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___resolution' to native representation

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___resolution, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___resolution' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_LastLevelUpTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Player_LastLevelUpTime(void*);}
extern "C" uint64_t Player_Player_LastLevelUpTime_m1822 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_LastLevelUpTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_LastLevelUpTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Title(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Player_Title(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Player_Player_Title_m1823 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_Title;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_Title'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_CurrentXP(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Player_CurrentXP(void*);}
extern "C" uint64_t Player_Player_CurrentXP_m1824 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_CurrentXP;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_CurrentXP'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL Player_Valid(void*);}
extern "C" bool Player_Player_Valid_m1825 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_HasLevelInfo(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL Player_HasLevelInfo(void*);}
extern "C" bool Player_Player_HasLevelInfo_m1826 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_HasLevelInfo;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_HasLevelInfo'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_NextLevel(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL Player_NextLevel(void*);}
extern "C" IntPtr_t Player_Player_NextLevel_m1827 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_NextLevel;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_NextLevel'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Player_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Player_Player_Id_m1828 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Player_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Player_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerManaMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchSelfCallback__ctor_m1829 (FetchSelfCallback_t439 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchSelfCallback_Invoke_m1830 (FetchSelfCallback_t439 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchSelfCallback_Invoke_m1830((FetchSelfCallback_t439 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchSelfCallback_t439(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchSelfCallback_BeginInvoke_m1831 (FetchSelfCallback_t439 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchSelfCallback_EndInvoke_m1832 (FetchSelfCallback_t439 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchCallback__ctor_m1833 (FetchCallback_t440 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchCallback_Invoke_m1834 (FetchCallback_t440 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchCallback_Invoke_m1834((FetchCallback_t440 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchCallback_t440(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchCallback_BeginInvoke_m1835 (FetchCallback_t440 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchCallback_EndInvoke_m1836 (FetchCallback_t440 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchListCallback__ctor_m1837 (FetchListCallback_t441 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchListCallback_Invoke_m1838 (FetchListCallback_t441 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchListCallback_Invoke_m1838((FetchListCallback_t441 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchListCallback_t441(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchListCallback_BeginInvoke_m1839 (FetchListCallback_t441 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchListCallback_EndInvoke_m1840 (FetchListCallback_t441 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.PlayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.PlayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchInvitable(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL PlayerManager_FetchInvitable(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void PlayerManager_PlayerManager_FetchInvitable_m1841 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchListCallback_t441 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchInvitable;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchInvitable'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchConnected(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL PlayerManager_FetchConnected(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void PlayerManager_PlayerManager_FetchConnected_m1842 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchListCallback_t441 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchConnected;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchConnected'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL PlayerManager_Fetch(void*, int32_t, char*, methodPointerType, IntPtr_t);}
extern "C" void PlayerManager_PlayerManager_Fetch_m1843 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___player_id, FetchCallback_t440 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_Fetch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_Fetch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___player_id' to native representation
	char* ____player_id_marshaled = { 0 };
	____player_id_marshaled = il2cpp_codegen_marshal_string(___player_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____player_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___player_id' native representation
	il2cpp_codegen_marshal_free(____player_id_marshaled);
	____player_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchRecentlyPlayed(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL PlayerManager_FetchRecentlyPlayed(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void PlayerManager_PlayerManager_FetchRecentlyPlayed_m1844 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchListCallback_t441 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchRecentlyPlayed;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchRecentlyPlayed'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelf(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL PlayerManager_FetchSelf(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void PlayerManager_PlayerManager_FetchSelf_m1845 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchSelfCallback_t439 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchSelf;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchSelf'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL PlayerManager_FetchSelfResponse_Dispose(void*);}
extern "C" void PlayerManager_PlayerManager_FetchSelfResponse_Dispose_m1846 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchSelfResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchSelfResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL PlayerManager_FetchSelfResponse_GetStatus(void*);}
extern "C" int32_t PlayerManager_PlayerManager_FetchSelfResponse_GetStatus_m1847 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchSelfResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchSelfResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL PlayerManager_FetchSelfResponse_GetData(void*);}
extern "C" IntPtr_t PlayerManager_PlayerManager_FetchSelfResponse_GetData_m1848 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchSelfResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchSelfResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL PlayerManager_FetchResponse_Dispose(void*);}
extern "C" void PlayerManager_PlayerManager_FetchResponse_Dispose_m1849 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL PlayerManager_FetchResponse_GetStatus(void*);}
extern "C" int32_t PlayerManager_PlayerManager_FetchResponse_GetStatus_m1850 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL PlayerManager_FetchResponse_GetData(void*);}
extern "C" IntPtr_t PlayerManager_PlayerManager_FetchResponse_GetData_m1851 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL PlayerManager_FetchListResponse_Dispose(void*);}
extern "C" void PlayerManager_PlayerManager_FetchListResponse_Dispose_m1852 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchListResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchListResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL PlayerManager_FetchListResponse_GetStatus(void*);}
extern "C" int32_t PlayerManager_PlayerManager_FetchListResponse_GetStatus_m1853 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchListResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchListResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL PlayerManager_FetchListResponse_GetData_Length(void*);}
extern "C" UIntPtr_t  PlayerManager_PlayerManager_FetchListResponse_GetData_Length_m1854 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchListResponse_GetData_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchListResponse_GetData_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL PlayerManager_FetchListResponse_GetData_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t PlayerManager_PlayerManager_FetchListResponse_GetData_GetElement_m1855 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)PlayerManager_FetchListResponse_GetData_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'PlayerManager_FetchListResponse_GetData_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Quest
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Quest.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Quest
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques.h"


// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Quest_Description(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Quest_Quest_Description_m1856 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_Description;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_Description'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_BannerUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Quest_BannerUrl(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Quest_Quest_BannerUrl_m1857 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_BannerUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_BannerUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Int64 GooglePlayGames.Native.Cwrapper.Quest::Quest_ExpirationTime(System.Runtime.InteropServices.HandleRef)
extern "C" {int64_t DEFAULT_CALL Quest_ExpirationTime(void*);}
extern "C" int64_t Quest_Quest_ExpirationTime_m1858 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_ExpirationTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_ExpirationTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_IconUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Quest_IconUrl(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Quest_Quest_IconUrl_m1859 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_IconUrl;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_IconUrl'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/QuestState GooglePlayGames.Native.Cwrapper.Quest::Quest_State(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL Quest_State(void*);}
extern "C" int32_t Quest_Quest_State_m1860 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_State;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_State'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Copy(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL Quest_Copy(void*);}
extern "C" IntPtr_t Quest_Quest_Copy_m1861 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_Copy;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_Copy'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.Quest::Quest_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL Quest_Valid(void*);}
extern "C" bool Quest_Quest_Valid_m1862 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Int64 GooglePlayGames.Native.Cwrapper.Quest::Quest_StartTime(System.Runtime.InteropServices.HandleRef)
extern "C" {int64_t DEFAULT_CALL Quest_StartTime(void*);}
extern "C" int64_t Quest_Quest_StartTime_m1863 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_StartTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_StartTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Quest::Quest_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL Quest_Dispose(void*);}
extern "C" void Quest_Quest_Dispose_m1864 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_CurrentMilestone(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL Quest_CurrentMilestone(void*);}
extern "C" IntPtr_t Quest_Quest_CurrentMilestone_m1865 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_CurrentMilestone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_CurrentMilestone'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Int64 GooglePlayGames.Native.Cwrapper.Quest::Quest_AcceptedTime(System.Runtime.InteropServices.HandleRef)
extern "C" {int64_t DEFAULT_CALL Quest_AcceptedTime(void*);}
extern "C" int64_t Quest_Quest_AcceptedTime_m1866 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_AcceptedTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_AcceptedTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Quest_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Quest_Quest_Id_m1867 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Quest_Name(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Quest_Quest_Name_m1868 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Quest_Name;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Quest_Name'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManagMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchCallback__ctor_m1869 (FetchCallback_t444 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchCallback_Invoke_m1870 (FetchCallback_t444 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchCallback_Invoke_m1870((FetchCallback_t444 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchCallback_t444(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchCallback_BeginInvoke_m1871 (FetchCallback_t444 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchCallback_EndInvoke_m1872 (FetchCallback_t444 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchListCallback__ctor_m1873 (FetchListCallback_t445 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchListCallback_Invoke_m1874 (FetchListCallback_t445 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchListCallback_Invoke_m1874((FetchListCallback_t445 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchListCallback_t445(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchListCallback_BeginInvoke_m1875 (FetchListCallback_t445 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchListCallback_EndInvoke_m1876 (FetchListCallback_t445 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::.ctor(System.Object,System.IntPtr)
extern "C" void AcceptCallback__ctor_m1877 (AcceptCallback_t446 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void AcceptCallback_Invoke_m1878 (AcceptCallback_t446 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AcceptCallback_Invoke_m1878((AcceptCallback_t446 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AcceptCallback_t446(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * AcceptCallback_BeginInvoke_m1879 (AcceptCallback_t446 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::EndInvoke(System.IAsyncResult)
extern "C" void AcceptCallback_EndInvoke_m1880 (AcceptCallback_t446 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
