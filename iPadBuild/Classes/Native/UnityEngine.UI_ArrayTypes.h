﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct  BaseInputModuleU5BU5D_t3863  : public Array_t
{
};
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct  IEventSystemHandlerU5BU5D_t3870  : public Array_t
{
};
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct  RaycastResultU5BU5D_t3888  : public Array_t
{
};
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct  BaseRaycasterU5BU5D_t3900  : public Array_t
{
};
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct  EntryU5BU5D_t3907  : public Array_t
{
};
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct  PointerEventDataU5BU5D_t3917  : public Array_t
{
};
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct  ButtonStateU5BU5D_t3933  : public Array_t
{
};
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct  ICanvasElementU5BU5D_t3945  : public Array_t
{
};
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct  TextU5BU5D_t3952  : public Array_t
{
};
struct TextU5BU5D_t3952_StaticFields{
};
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct  GraphicU5BU5D_t3983  : public Array_t
{
};
struct GraphicU5BU5D_t3983_StaticFields{
};
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct  IndexedSet_1U5BU5D_t3988  : public Array_t
{
};
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct  ContentTypeU5BU5D_t1363  : public Array_t
{
};
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct  SelectableU5BU5D_t4013  : public Array_t
{
};
struct SelectableU5BU5D_t4013_StaticFields{
};
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct  MatEntryU5BU5D_t4027  : public Array_t
{
};
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct  ToggleU5BU5D_t4034  : public Array_t
{
};
