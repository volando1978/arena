﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// gameControl
struct gameControl_t794;
// System.Object
#include "mscorlib_System_Object.h"
// gameControl/<startGame>c__Iterator7
struct  U3CstartGameU3Ec__Iterator7_t795  : public Object_t
{
	// System.Boolean gameControl/<startGame>c__Iterator7::<start>__0
	bool ___U3CstartU3E__0_0;
	// System.Int32 gameControl/<startGame>c__Iterator7::<index>__1
	int32_t ___U3CindexU3E__1_1;
	// System.Int32 gameControl/<startGame>c__Iterator7::<r>__2
	int32_t ___U3CrU3E__2_2;
	// System.Int32 gameControl/<startGame>c__Iterator7::<rB>__3
	int32_t ___U3CrBU3E__3_3;
	// System.Int32 gameControl/<startGame>c__Iterator7::<rR>__4
	int32_t ___U3CrRU3E__4_4;
	// System.Int32 gameControl/<startGame>c__Iterator7::$PC
	int32_t ___U24PC_5;
	// System.Object gameControl/<startGame>c__Iterator7::$current
	Object_t * ___U24current_6;
	// gameControl gameControl/<startGame>c__Iterator7::<>f__this
	gameControl_t794 * ___U3CU3Ef__this_7;
};
