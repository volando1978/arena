﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
struct Enumerator_t3967;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1267;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22928_gshared (Enumerator_t3967 * __this, List_1_t1267 * ___l, MethodInfo* method);
#define Enumerator__ctor_m22928(__this, ___l, method) (( void (*) (Enumerator_t3967 *, List_1_t1267 *, MethodInfo*))Enumerator__ctor_m22928_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22929_gshared (Enumerator_t3967 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22929(__this, method) (( Object_t * (*) (Enumerator_t3967 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22929_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m22930_gshared (Enumerator_t3967 * __this, MethodInfo* method);
#define Enumerator_Dispose_m22930(__this, method) (( void (*) (Enumerator_t3967 *, MethodInfo*))Enumerator_Dispose_m22930_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m22931_gshared (Enumerator_t3967 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m22931(__this, method) (( void (*) (Enumerator_t3967 *, MethodInfo*))Enumerator_VerifyState_m22931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22932_gshared (Enumerator_t3967 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m22932(__this, method) (( bool (*) (Enumerator_t3967 *, MethodInfo*))Enumerator_MoveNext_m22932_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t1265  Enumerator_get_Current_m22933_gshared (Enumerator_t3967 * __this, MethodInfo* method);
#define Enumerator_get_Current_m22933(__this, method) (( UIVertex_t1265  (*) (Enumerator_t3967 *, MethodInfo*))Enumerator_get_Current_m22933_gshared)(__this, method)
