﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>
struct Func_2_t954;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
struct FetchListResponse_t689;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.IntPtr,System.Object>
#include "System_Core_System_Func_2_gen_41MethodDeclarations.h"
#define Func_2__ctor_m3956(__this, ___object, ___method, method) (( void (*) (Func_2_t954 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20368_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>::Invoke(T)
#define Func_2_Invoke_m20686(__this, ___arg1, method) (( FetchListResponse_t689 * (*) (Func_2_t954 *, IntPtr_t, MethodInfo*))Func_2_Invoke_m20370_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20687(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t954 *, IntPtr_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20372_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20688(__this, ___result, method) (( FetchListResponse_t689 * (*) (Func_2_t954 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20374_gshared)(__this, ___result, method)
