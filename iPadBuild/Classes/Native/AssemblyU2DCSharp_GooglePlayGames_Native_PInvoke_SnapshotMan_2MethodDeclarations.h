﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
struct ReadResponse_t704;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::.ctor(System.IntPtr)
extern "C" void ReadResponse__ctor_m3033 (ReadResponse_t704 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::ResponseStatus()
extern "C" int32_t ReadResponse_ResponseStatus_m3034 (ReadResponse_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::RequestSucceeded()
extern "C" bool ReadResponse_RequestSucceeded_m3035 (ReadResponse_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::Data()
extern "C" ByteU5BU5D_t350* ReadResponse_Data_m3036 (ReadResponse_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ReadResponse_CallDispose_m3037 (ReadResponse_t704 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::FromPointer(System.IntPtr)
extern "C" ReadResponse_t704 * ReadResponse_FromPointer_m3038 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::<Data>m__9F(System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  ReadResponse_U3CDataU3Em__9F_m3039 (ReadResponse_t704 * __this, ByteU5BU5D_t350* ___out_bytes, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
