﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.SnapshotMetadata
struct SnapshotMetadata_t479;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void SnapshotMetadata_SnapshotMetadata_Dispose_m2116 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_CoverImageURL(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  SnapshotMetadata_SnapshotMetadata_CoverImageURL_m2117 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  SnapshotMetadata_SnapshotMetadata_Description_m2118 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_IsOpen(System.Runtime.InteropServices.HandleRef)
extern "C" bool SnapshotMetadata_SnapshotMetadata_IsOpen_m2119 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_FileName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  SnapshotMetadata_SnapshotMetadata_FileName_m2120 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool SnapshotMetadata_SnapshotMetadata_Valid_m2121 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C" int64_t SnapshotMetadata_SnapshotMetadata_PlayedTime_m2122 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C" int64_t SnapshotMetadata_SnapshotMetadata_LastModifiedTime_m2123 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
