﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t3450;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void DefaultComparer__ctor_m15765_gshared (DefaultComparer_t3450 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m15765(__this, method) (( void (*) (DefaultComparer_t3450 *, MethodInfo*))DefaultComparer__ctor_m15765_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m15766_gshared (DefaultComparer_t3450 * __this, KeyValuePair_2_t3407  ___x, KeyValuePair_2_t3407  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m15766(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3450 *, KeyValuePair_2_t3407 , KeyValuePair_2_t3407 , MethodInfo*))DefaultComparer_Compare_m15766_gshared)(__this, ___x, ___y, method)
