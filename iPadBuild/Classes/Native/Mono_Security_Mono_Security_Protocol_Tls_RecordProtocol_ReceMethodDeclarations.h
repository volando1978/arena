﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t2263;
// System.IO.Stream
struct Stream_t1685;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t135;
// System.Threading.WaitHandle
struct WaitHandle_t2308;
// System.AsyncCallback
struct AsyncCallback_t20;

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m9483 (ReceiveRecordAsyncResult_t2263 * __this, AsyncCallback_t20 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t350* ___initialBuffer, Stream_t1685 * ___record, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t1685 * ReceiveRecordAsyncResult_get_Record_m9484 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t350* ReceiveRecordAsyncResult_get_ResultingBuffer_m9485 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t350* ReceiveRecordAsyncResult_get_InitialBuffer_m9486 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m9487 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t135 * ReceiveRecordAsyncResult_get_AsyncException_m9488 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m9489 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t2308 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m9490 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m9491 (ReceiveRecordAsyncResult_t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m9492 (ReceiveRecordAsyncResult_t2263 * __this, Exception_t135 * ___ex, ByteU5BU5D_t350* ___resultingBuffer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m9493 (ReceiveRecordAsyncResult_t2263 * __this, Exception_t135 * ___ex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m9494 (ReceiveRecordAsyncResult_t2263 * __this, ByteU5BU5D_t350* ___resultingBuffer, MethodInfo* method) IL2CPP_METHOD_ATTR;
