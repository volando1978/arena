﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t2397;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Security.Cryptography.RSA
struct RSA_t2130;
// System.Security.Cryptography.DSA
struct DSA_t2129;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor()
extern "C" void PrivateKeyInfo__ctor_m11013 (PrivateKeyInfo_t2397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor(System.Byte[])
extern "C" void PrivateKeyInfo__ctor_m11014 (PrivateKeyInfo_t2397 * __this, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_PrivateKey()
extern "C" ByteU5BU5D_t350* PrivateKeyInfo_get_PrivateKey_m11015 (PrivateKeyInfo_t2397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Decode(System.Byte[])
extern "C" void PrivateKeyInfo_Decode_m11016 (PrivateKeyInfo_t2397 * __this, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::RemoveLeadingZero(System.Byte[])
extern "C" ByteU5BU5D_t350* PrivateKeyInfo_RemoveLeadingZero_m11017 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___bigInt, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Normalize(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t350* PrivateKeyInfo_Normalize_m11018 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___bigInt, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeRSA(System.Byte[])
extern "C" RSA_t2130 * PrivateKeyInfo_DecodeRSA_m11019 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___keypair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeDSA(System.Byte[],System.Security.Cryptography.DSAParameters)
extern "C" DSA_t2129 * PrivateKeyInfo_DecodeDSA_m11020 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___privateKey, DSAParameters_t2149  ___dsaParameters, MethodInfo* method) IL2CPP_METHOD_ATTR;
