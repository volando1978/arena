﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.String
struct String_t;

// System.Void GooglePlayGames.BasicApi.Achievement::.ctor()
extern "C" void Achievement__ctor_m1327 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::ToString()
extern "C" String_t* Achievement_ToString_m1328 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsIncremental()
extern "C" bool Achievement_get_IsIncremental_m1329 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsIncremental(System.Boolean)
extern "C" void Achievement_set_IsIncremental_m1330 (Achievement_t333 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Achievement::get_CurrentSteps()
extern "C" int32_t Achievement_get_CurrentSteps_m1331 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_CurrentSteps(System.Int32)
extern "C" void Achievement_set_CurrentSteps_m1332 (Achievement_t333 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Achievement::get_TotalSteps()
extern "C" int32_t Achievement_get_TotalSteps_m1333 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_TotalSteps(System.Int32)
extern "C" void Achievement_set_TotalSteps_m1334 (Achievement_t333 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsUnlocked()
extern "C" bool Achievement_get_IsUnlocked_m1335 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsUnlocked(System.Boolean)
extern "C" void Achievement_set_IsUnlocked_m1336 (Achievement_t333 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsRevealed()
extern "C" bool Achievement_get_IsRevealed_m1337 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsRevealed(System.Boolean)
extern "C" void Achievement_set_IsRevealed_m1338 (Achievement_t333 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_Id()
extern "C" String_t* Achievement_get_Id_m1339 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_Id(System.String)
extern "C" void Achievement_set_Id_m1340 (Achievement_t333 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_Description()
extern "C" String_t* Achievement_get_Description_m1341 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_Description(System.String)
extern "C" void Achievement_set_Description_m1342 (Achievement_t333 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_Name()
extern "C" String_t* Achievement_get_Name_m1343 (Achievement_t333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_Name(System.String)
extern "C" void Achievement_set_Name_m1344 (Achievement_t333 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
