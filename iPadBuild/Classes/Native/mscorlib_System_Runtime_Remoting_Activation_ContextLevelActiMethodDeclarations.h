﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t2586;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2582;

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator__ctor_m12506 (ContextLevelActivator_t2586 * __this, Object_t * ___next, MethodInfo* method) IL2CPP_METHOD_ATTR;
