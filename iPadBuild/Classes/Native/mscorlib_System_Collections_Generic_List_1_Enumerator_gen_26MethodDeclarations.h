﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.IntPtr>
struct Enumerator_t3808;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.IntPtr>
struct List_1_t3807;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m20779_gshared (Enumerator_t3808 * __this, List_1_t3807 * ___l, MethodInfo* method);
#define Enumerator__ctor_m20779(__this, ___l, method) (( void (*) (Enumerator_t3808 *, List_1_t3807 *, MethodInfo*))Enumerator__ctor_m20779_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20780_gshared (Enumerator_t3808 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20780(__this, method) (( Object_t * (*) (Enumerator_t3808 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20780_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::Dispose()
extern "C" void Enumerator_Dispose_m20781_gshared (Enumerator_t3808 * __this, MethodInfo* method);
#define Enumerator_Dispose_m20781(__this, method) (( void (*) (Enumerator_t3808 *, MethodInfo*))Enumerator_Dispose_m20781_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::VerifyState()
extern "C" void Enumerator_VerifyState_m20782_gshared (Enumerator_t3808 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m20782(__this, method) (( void (*) (Enumerator_t3808 *, MethodInfo*))Enumerator_VerifyState_m20782_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.IntPtr>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20783_gshared (Enumerator_t3808 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m20783(__this, method) (( bool (*) (Enumerator_t3808 *, MethodInfo*))Enumerator_MoveNext_m20783_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.IntPtr>::get_Current()
extern "C" IntPtr_t Enumerator_get_Current_m20784_gshared (Enumerator_t3808 * __this, MethodInfo* method);
#define Enumerator_get_Current_m20784(__this, method) (( IntPtr_t (*) (Enumerator_t3808 *, MethodInfo*))Enumerator_get_Current_m20784_gshared)(__this, method)
