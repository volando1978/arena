﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt64>
struct InternalEnumerator_1_t4202;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25819_gshared (InternalEnumerator_1_t4202 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25819(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4202 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25819_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25820_gshared (InternalEnumerator_1_t4202 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25820(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4202 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25820_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25821_gshared (InternalEnumerator_1_t4202 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25821(__this, method) (( void (*) (InternalEnumerator_1_t4202 *, MethodInfo*))InternalEnumerator_1_Dispose_m25821_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25822_gshared (InternalEnumerator_1_t4202 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25822(__this, method) (( bool (*) (InternalEnumerator_1_t4202 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25822_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C" uint64_t InternalEnumerator_1_get_Current_m25823_gshared (InternalEnumerator_1_t4202 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25823(__this, method) (( uint64_t (*) (InternalEnumerator_1_t4202 *, MethodInfo*))InternalEnumerator_1_get_Current_m25823_gshared)(__this, method)
