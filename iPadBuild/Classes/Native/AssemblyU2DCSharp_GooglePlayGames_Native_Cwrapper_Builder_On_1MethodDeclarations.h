﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback
struct OnAuthActionFinishedCallback_t404;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0.h"

// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnAuthActionFinishedCallback__ctor_m1661 (OnAuthActionFinishedCallback_t404 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr)
extern "C" void OnAuthActionFinishedCallback_Invoke_m1662 (OnAuthActionFinishedCallback_t404 * __this, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnAuthActionFinishedCallback_t404(Il2CppObject* delegate, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnAuthActionFinishedCallback_BeginInvoke_m1663 (OnAuthActionFinishedCallback_t404 * __this, int32_t ___arg0, int32_t ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnAuthActionFinishedCallback_EndInvoke_m1664 (OnAuthActionFinishedCallback_t404 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
